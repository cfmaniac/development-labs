// HttpRetrieval.h: interface for the HttpRetrieval class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_HTTPRETRIEVAL_H__5A361F35_6928_11D2_AF34_00A0C9934674__INCLUDED_)
#define AFX_HTTPRETRIEVAL_H__5A361F35_6928_11D2_AF34_00A0C9934674__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <afxinet.h>
#include <process.h>
#include <afxmt.h>
#include <cmath>

typedef unsigned int (__stdcall *PW_THREAD_START_ROUTINE)(LPVOID lpThreadParameter);

class HttpRetrieval
{

public:
	static int		GOOD_STATUS;
	static int		ERROR_STATUS;

public:
	enum RetreivalType
	{
		LOGIN = 0,
			UPGRADE = 3,
			FILE_DOWNLOAD = 300,
			FILECACHE_DOWNLOAD = 600,
			INFO_DOWNLOAD = 650,
			TKT_INFO_DOWNLOAD = 675,
			FTP_UPLOAD = 750,
			FTP_DOWNLOAD = 850,
		    PATRON_INFO_DOWNLOAD = 950,
		    ACCESS_PASS_DOWNLOAD = 1050
	};
	
		struct InternetRequestStruct {
			CString StatusError;
			HINTERNET InternetSession;
			HINTERNET InternetConnection;
			HINTERNET InternetRequest;
		};
		struct ThreadStruct{
			RetreivalType TheType;
			unsigned long TheThread;
			CString TheRequestString;
			BYTE* TheData;
			long TheDataSize;
			long BufferGrowSize;
			BYTE* TheTmpReadBuffer;
			CString TheErrorMsg;
			CString TheActualURL;
			int NetworkError;
			long CurrentByteCount;
			CString Request_GET_POST_Type;
			long LastCurrentByteCount;
			HINTERNET TheConnectionHandle;
			long TimeoutTime;		//In Seconds ; -1 = no Timeout
			CString DiskFilename;
			long ErrorNumber;
			CTime ThreadStartTime;
			CTime LastByteChangeTime;
			BOOL showUserPrompts;
			CString proxyUsername;
			CString proxyPassword;
			BOOL usePassiveFTP;
			CString ftpLocalFilename;
			CString ftpRemoteFilename;
			CString ftpUsername;
			CString ftpPassword;
			InternetRequestStruct theInternetRequestStruct;
			
		};

		enum ThreadCheckStatus {
			THREAD_COMPLETE,
			THREAD_WAITING,
			THREAD_KILLED,
			THREAD_NULL
		};


public:
	CMapStringToString SessionData;
	HttpRetrieval();
	virtual ~HttpRetrieval();
	void InitVars();

	static LONG NumThreadRequests;
	static long HttpRetrieval::AccountIsMaxed;

	static void  DownloadUpdateFiles(void* CallingArgument);
	static ThreadStruct* GetFileNonThreaded(CString Url, CString proxyUsername, CString proxyPassword, BOOL usePassiveFTP = FALSE,
		CString ftpUsername = "", CString ftpPassword = "");
	bool ProcessMessageStatus(LONG StatusValue);

	CString LastError;
	static BOOL DoNotPromptForDataAgain;	// If a proxy password is needed and cancel was clicked

	CRITICAL_SECTION m_csRetArray;
	static CCriticalSection RequestSyncSection;

	int LoadMachineLoginData(void* TheDataStruct);
	int LoadLookDataUrls(BYTE*& BufferPtr, long& BytesLeft, BOOL ValidateTheData = FALSE);

	static BOOL IsNextPartHtmlTrailer(BYTE* TheBuffer);

	int CheckForThreadCompleteByType(BOOL &LoginFound, long MsgType, 
			CString &RequestErrMsg,	CMapStringToString &Result, int& ResponseStatus);

	struct ThreadManagementStruct
	{
		DWORD ThreadType;
		void* TheDataStruct;
		ThreadStruct* TheThreadStruct;
//		long CreditImageID;
//		long LongData1;
//		long LongData2;
//		long LongData3;
//		long LongData4;
//		char CharData1[_MAX_PATH];
//		char CharData2[_MAX_PATH];
//		char CharData3[_MAX_PATH];
//		char CharData4[_MAX_PATH];
		unsigned long CreatedThreadHandle;
	};

	struct GetFileStruct
	{
		CString RemoteFileUrl;
		CString LocalFile;
		BOOL DoneProcessing;
		BOOL showUserPrompts;
		CString proxyUsername;
		CString proxyPassword;
	};

	CMutex* DataArrayMutex;
	CPtrArray TheThreadArray;

	static void CleanupThreadStruct(HttpRetrieval::ThreadStruct* &TheThreadStruct);

	int GetAppFile(CString Filename);
	void InsertIntoThreadArray(void* &ThePointer);
	static time_t LastThreadTime;


	BYTE* ReadStreamFromBuffer(BYTE* &TheData, long NumBytes, BYTE* &TheValue, long& BytesLeft);

	static void MakeRequestTh(void* CallingArgument);

	HttpRetrieval::ThreadStruct* BuildAndStartThread(
					CString TheRequestString, 
					HttpRetrieval::RetreivalType RequestType,
					long BufferGrowSize = 10000,
					CString Request_GET_POST_Type = "GET",
					CString proxyUsername = "",
					CString proxyPassword = "",
					BOOL usePassiveFTP = FALSE,
					CString ftpLocalFilename = "",
					CString ftpRemoteFilename = "",
					CString ftpUsername = "", 
					CString ftpPassword = "",
//					long CreditImageID = -1,
//					long Lv1 = 0,
//					long Lv2 = 0,
//					long Lv3 = 0,
//					long Lv4 = 0,
//					const char* Cv1 = NULL,
//					const char* Cv2 = NULL,
//					const char* Cv3 = NULL,
//					const char* Cv4 = NULL,
					const char* DiskFilename = NULL);

	static void SetNonSecureProxyData(CString Addr, CString Port);
	static void SetSecureProxyData(CString Addr, CString Port);
	static DWORD ProxyType;
	static CString NonSecureProxyName;
	static CString SecureProxyName;
	static void GetProxyVals(long nPort, CString& inProxyName, DWORD& inProxyType);
	static void SetProxyType(DWORD theType);
	int DownloadFile(CString RemoteFile, CString LocalFile, CString proxyUsername, CString proxyPassword);
//	int ParseMachineLoginData(BYTE* BufferPtr, long& BytesLeft, int& VersionStatus, BOOL ReadingFromCache = FALSE,
//		BOOL ValidateTheData = FALSE);

	int LoadMachineLoginDataFromDisk(BOOL& DiskCacheUsed);
	static BOOL NeedAuth (HINTERNET hRequest);
	static DWORD DoCustomUI (HINTERNET hConnect, HINTERNET hRequest, long& GetLastErrorValue );

	static BOOL OpenInternetConnection(
							HttpRetrieval::ThreadStruct* TheThreadStruct,
//							InternetRequestStruct& theInternetRequestStruct,
//							CString& MajorErrorMsg,
//							int& NetworkError,
//							long& ErrorNumber,
//							CString& TheActualURL,
							long& dwStatusCode,
							long& httpStatusError,
							BOOL ShowUserPrompts = FALSE,
							long ConnectionTimeoutValue = -1);

//	static CString proxyUsername;
//	static CString proxyPassword;

	ThreadStruct* CheckForThreadCompleteByType(long ThreadType);
	BOOL IsThreadTypeRunning(long ThreadType);

	private:

		static HINTERNET AppInternetSession;		//	ALWAYS CLOSE, 01/20/2003, trl;
		static HINTERNET FtpInternetSession;		//	ALWAYS CLOSE, 01/20/2003, trl;
		static CString InternetSessionKey;
		static void CloseInternetHandle();
		static void CloseThreadHandle(unsigned long TheThread);
		static BOOL GetRemoteFileToDisk(
			CString RemoteUrl,
			CString LocalFilenamePath,
			CString proxyUsername,
			CString proxyPassword);

		static CString STATIC_LOGIN_URL;

		BYTE* ReadAndWriteToFileFromBuffer(CString& FileName, BYTE* &TheData, long NumBytes, long& BytesLeft);
		static void DownloadFileTh(void* CallingArgument);
		void SetStartupVars();
		static CString ReportExceptionError(BOOL& ExceptionError, 
			BOOL& ContinueStatus,
			CException* pEx, DWORD dwStatusCode);
		static CString ReportNormalError(long& ErrorNumber, const char* StatusError,
			int& NetworkError, DWORD dwStatusCode, const char* InUrl, long GetLastErrorValue);
	
		struct HttpConnectionData
		{
			CString Url;
			HINTERNET ConnectionHandle;
			time_t HandleCreatedTime;
		};

	public:
		static unsigned long CreateThread(LPTHREAD_START_ROUTINE TheRoutine,	 // address of thread function
					 	  LPVOID CallingArgument,			 // argument for new thread
				       	  LPSECURITY_ATTRIBUTES  lpsa = NULL, // address of thread security attributes  
    				 	  DWORD  cbStack = 0,				 // initial thread stack size, in bytes 
				     	  DWORD  fdwCreate = 0);				 // creation flags 
		static unsigned long CreateThread(LPTHREAD_START_ROUTINE TheRoutine,	 // address of thread function
					 	  ThreadStruct* CallingArgument,			 // argument for new thread
				       	  LPSECURITY_ATTRIBUTES  lpsa = NULL); // address of thread security attributes  
		static ThreadCheckStatus GetThreadCompletionStatus(ThreadStruct* TheThreadStruct);
		static void GetInternetLastErrorString(long GetLastErrorValue, CString &errorMsg);

		static int LoadResponseData(void* TheDataStruct, CMapStringToString &Result, int &ResponseStatus);
		static BYTE* ReadLineFromBuffer(BYTE*& TheData, CString &TheCString, long &BytesLeft);
		static void TrimWhitespaceFromEnds(CString& theString);

};

#endif // !defined(AFX_HTTPRETRIEVAL_H__5A361F35_6928_11D2_AF34_00A0C9934674__INCLUDED_)
