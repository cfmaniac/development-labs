// EncoderCommand.cpp: implementation of the CEncoderCommand class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "AlvaradoMgr.h"
#include "EncoderCommand.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CEncoderCommand::CEncoderCommand()
{
	initValues();
}

CEncoderCommand::~CEncoderCommand()
{

}

void CEncoderCommand::initValues()
{
	theID.Empty();
	theCommand.Empty();
	archiveFlag = "1";
	port = "1175";
	theAudioDevice.Empty();
	theVideoDevice.Empty();
	recordingDevice.Empty();
	deviceType.Empty();
	encoderProfile.Empty();
	eventName.Empty();
	speedKey.Empty();
	gmtStartDate.Empty();
	gmtCurDate.Empty();
}
