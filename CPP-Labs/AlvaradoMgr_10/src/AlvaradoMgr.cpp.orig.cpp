// alvaradomgr.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "alvaradomgr.h"
#include "winsvc.h"
#ifdef _USEING_ENCODER_CODE_
	#include "DBVMediaEncoder.h"
#endif
#include "EncoderDlg.h"
#include "XUtils.h"
#include "ConfigFileManager.h"
#include "StartupOptionSetup.h"
//#include "HttpServer.h"

//#define _DO_SPY

#ifdef _DO_SPY
#include "cmallspy.h"
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

void ServiceMain(DWORD argc, LPTSTR *argv); 
void ServiceCtrlHandler(DWORD nControlCode);
BOOL UpdateServiceStatus(DWORD dwCurrentState, DWORD dwWin32ExitCode,
					 DWORD dwServiceSpecificExitCode, DWORD dwCheckPoint,
					 DWORD dwWaitHint);
BOOL StartServiceThread();
DWORD ServiceExecutionThread(LPDWORD param);
HANDLE hServiceThread;
void KillService();

char* strServiceName = "EncoderService";
SERVICE_STATUS_HANDLE nServiceStatusHandle; 
HANDLE killServiceEvent;
BOOL nServiceRunning;
DWORD nServiceCurrentStatus;
int installService(CString serviceName, CString serviceDisplayName, CString servicePath);
void DeleteService(CString serviceName);
void ShowErr(CString msg);
BOOL StartedByService = TRUE;
CEncoderDlg* theDlg = NULL;

/////////////////////////////////////////////////////////////////////////////
// The one and only application object

CWinApp theApp;

using namespace std;
#if defined(_DO_SPY)
	CMemoryState msOld, msNew, msDif;
#endif

int _tmain1(int argc, TCHAR* argv[], TCHAR* envp[])
{
	::FreeConsole();
	StartedByService = FALSE;
	BOOL ContinueStartup = true;
	
	CEncoderDlg::b_interactiveMode = true;

	CXUtils::FillAppInfo();
	if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
	{
		// TODO: change error code to suit your needs
		cerr << _T("Fatal Error: MFC initialization failed") << endl;
		ContinueStartup = false;
	}
	int localArgCount = argc;
	CString firstArg;
	BOOL ValidCommandlineArgs = TRUE;
	BOOL askingForHelp = false;
	if (CoInitialize(NULL) != S_OK)
	{
		return FALSE;
	}
	BOOL ContinueLooping = true;
	if (ContinueStartup)
	{
		/*
			while (ContinueLooping)
			{
				ContinueLooping = false;
				ContinueStartup = true;

				if (theDlg != NULL)
				{
					delete theDlg;
					theDlg = NULL;
				}
				theDlg = new CEncoderDlg();

				firstArg = __argv[1];
				firstArg.MakeLower();
				long validTimeStampArgCount = 3;
				CEncoderDlg::b_interactiveMode = false;

				if (firstArg.GetLength() == 0)
				{
					CStartupOptionSetup theInteractiveOptions;
					CEncoderDlg::processingSteps.RemoveAll();
					CEncoderDlg::b_interactiveMode = true;

					if (theInteractiveOptions.DoModal() == IDOK)
					{
						if (theInteractiveOptions.b_DumpConfig)
						{
							firstArg = "-c";
							ContinueLooping = true;
						}
						else if (theInteractiveOptions.b_TestEmail)
						{
							firstArg = "-e";
							ContinueLooping = true;
						}
						else if (theInteractiveOptions.b_ImportOrders)
						{
							firstArg = "-o";
							ContinueLooping = true;
						}
						else if (theInteractiveOptions.b_ShowCmdLineArgs)
						{
							firstArg = "-h";
							ContinueLooping = true;
						}
						else if (theInteractiveOptions.b_UpdateInvoiceStatus)
						{
							firstArg = "-u";
							ContinueLooping = true;
						}
						else
							ContinueStartup = false;
					}
					else
					{
						ContinueStartup = false;
					}
				}


				firstArg.MakeLower();
				if (ContinueStartup)
				{
					//CEncoderDlg::processingSteps

					if (firstArg == "-u")
					{
						CEncoderDlg::processingSteps.Add("DOWNLOAD_PENDING_INVOICES");
						CEncoderDlg::processingSteps.Add("UPDATE_REMOTE_STATUS_INFORMATION");
					}
					else if (firstArg == "-o" && CEncoderDlg::b_interactiveMode == true)
					{
						CEncoderDlg::processingSteps.Add("DOWNLOAD_EXPORT_NEW_INVOICES");
						CEncoderDlg::processingSteps.Add("UPDATE_NEW_INVOICES_TO_INPROGRESS");
						CEncoderDlg::processingSteps.Add("RETRIEVE_AND_COMPARE_STOCK_INFORMATION");
						CEncoderDlg::processingSteps.Add("UPDATE_REMOTE_STOCK_INFORMATION");
					}
					else if (firstArg == "-c")
					{
						ConfigFileManager theConfigFile;
						BOOL ConfigFileStatus = theConfigFile.OpenAtExeDirectory("alvaradomgr.properties",
							FALSE,
							FALSE);
						CString outBuffer;
						if (ConfigFileStatus)
						{
							theConfigFile.DumpSectionKeyValues(outBuffer);
							outBuffer = "Configuration File Contents:\n\n" + outBuffer;
							MessageBox(NULL,outBuffer,"AlvaradoMgr Dump Configuration File", MB_OK);
						}
						else
						{
							outBuffer = "An error has occurred reading the configuration file.";
							MessageBox(NULL,outBuffer,"AlvaradoMgr Dump Configuration File", MB_OK);
						}

						ContinueStartup = false;
					}
					else if (firstArg == "-e")
					{
						CString errorMsg;
						errorMsg = "Test Message";
						BOOL Stat = CEncoderDlg::SendErrorEmail(errorMsg);

						CString outBuffer;
						if (Stat)
						{
							outBuffer = "Test Email sent successfully:  Test Message";
							MessageBox(NULL,outBuffer,"AlvaradoMgr Email Test", MB_OK);
						}
						else
						{
							outBuffer = "An error has occurred sending email. Check the configuration file and error log.";
							MessageBox(NULL,outBuffer,"AlvaradoMgr Email Test", MB_OK);
						}

						ContinueStartup = false;
					}
					else if (firstArg == "-h")
					{
						ValidCommandlineArgs = FALSE;
					}
					else
					{
						ValidCommandlineArgs = FALSE;
					}

					if (ValidCommandlineArgs == FALSE)
					{
						CString theMessage;
						theMessage += "Usage: AlvaradoMgr [[-E] | [-C] | [-U] | [ -H]\n\n";
						theMessage += "Sample: AlvaradoMgr (no arguments)\n";
						theMessage += "Explanation: Open user interface.\n\n";
						theMessage += "Sample: AlvaradoMgr -U\n";
						theMessage += "Explanation: Do non-interactive status updates.\n\n";
						theMessage += "Sample: AlvaradoMgr -E\n";
						theMessage += "Explanation: Send a test email to the error email address.\n\n";
						theMessage += "Sample: AlvaradoMgr -C\n";
						theMessage += "Explanation: List the contents of the configuration file.\n\n";
						theMessage += "Sample: AlvaradoMgr -H (Displays this page)\n";

						CString title = "AlvaradoMgr Invalid Arguments; Usage Information";
						if (askingForHelp)
							title = "AlvaradoMgr Usage Information";

						MessageBox(NULL,theMessage,title, MB_OK);
						printf( "Usage: AlvaradoMgr MMDDYYYY:HH:MM");
						if (askingForHelp == FALSE)
						{
							theMessage = "Startup Error: Invalid Command Arguments: ";
							theMessage.Append(GetCommandLine());
							CXUtils::WriteToTheLogFile(theMessage);
						}
						ContinueStartup = false;
						ValidCommandlineArgs = TRUE;
					}

					if (ContinueStartup)
					{
						nServiceRunning = true;
						if (nServiceRunning && theDlg != NULL)
						{		
							theApp.m_pActiveWnd = theDlg;
							theDlg->DoModal();					}
//						ServiceExecutionThread(0);
					}
				}
			}
			*/
			if (ContinueStartup)
			{
				nServiceRunning = true;
				theDlg = new CEncoderDlg();
				if (nServiceRunning && theDlg != NULL)
				{		
					theApp.m_pActiveWnd = theDlg;
					theDlg->DoModal();					
					delete theDlg;
				}
			}
		}
		CoUninitialize();
		return 1;

	}
int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	int nRetCode = 0;
	::FreeConsole();

	CEncoderDlg::b_interactiveMode = false;

	// initialize MFC and print and error on failure
	CXUtils::FillAppInfo();
	if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
	{
		// TODO: change error code to suit your needs
		cerr << _T("Fatal Error: MFC initialization failed") << endl;
		nRetCode = 1;
	}
	else
	{
		CString arg1;
		if (argc > 1)
		{
			arg1 = argv[1];
			if (arg1 == "INSTALL")
			{
				CString theCommandLine = GetCommandLine();
				char szFullPath[256];
				GetModuleFileName (NULL, szFullPath, sizeof(szFullPath));
				CString TheAppPath = szFullPath;

				CString serviceName = "AlvaradoMgr";
				CString serviceDisplayName = "AlvaradoMgr";
				nRetCode = installService(serviceName, serviceDisplayName, TheAppPath);


			}
			else if (arg1 == "REMOVE")
			{
				CString serviceName = "AlvaradoMgr";
				DeleteService(serviceName);
			}
		}
		else
		{
			// TODO: code your application's behavior here.
			SERVICE_TABLE_ENTRY servicetable[]=
			{
				{strServiceName,(LPSERVICE_MAIN_FUNCTION)ServiceMain},
				{NULL,NULL}
			};
			BOOL success;
			success=StartServiceCtrlDispatcher(servicetable);
			if(!success)
			{
				    CXUtils::WriteToTheLogFile("startup error Failed:"); 

				//error occured
			}
		
			CString strHello;
			strHello.LoadString(IDS_HELLO);
			cout << (LPCTSTR)strHello << endl;
		}
	}

	return nRetCode;
}

int installService(CString serviceName, CString serviceDisplayName, CString servicePath)
{
	SC_HANDLE NishService,scm;
	scm=OpenSCManager(0,0,SC_MANAGER_CREATE_SERVICE);
	if(!scm)
	{

		ShowErr("Install Service: OpenSCManager Failed");
		return 1;
	}
	NishService=CreateService(scm,
//		"zzz", "zzzz",
		(LPCTSTR)serviceName,
		(LPCTSTR)serviceDisplayName,
		SERVICE_ALL_ACCESS,
		SERVICE_WIN32_OWN_PROCESS | SERVICE_INTERACTIVE_PROCESS,
		SERVICE_DEMAND_START,
		SERVICE_ERROR_NORMAL,
		(LPCTSTR)servicePath,
		0,0,0,0,0);
	if(!NishService)
	{
		ShowErr("InstallService: CreateService Failed:" + serviceName);
		CloseServiceHandle(scm);
		return 1;
	}
	CloseServiceHandle(NishService);
	CloseServiceHandle(scm);
    CXUtils::WriteToTheLogFile("CreateService SUCCESS:" + serviceName); 
	return 0;
}
void DeleteService(CString serviceName) 
{ 
 	SC_HANDLE schService,scm;
	scm=OpenSCManager(0,0,SC_MANAGER_CREATE_SERVICE);
	schService = OpenService( 
        scm,       // SCManager database 
//		"zzz",
        (LPCTSTR)serviceName,       // name of service 
        DELETE);            // only need DELETE access 
 
    if (schService == NULL) 
        ShowErr("DeleteService: OpenService Failed:" + serviceName); 
	else
	{
	    if (! DeleteService(schService) ) 
		    ShowErr("DeleteService Failed:" + serviceName); 
	    else 
		    CXUtils::WriteToTheLogFile("DeleteService SUCCESS:" + serviceName); 
	}
	if (schService != NULL)
		CloseServiceHandle(schService);
	if (scm != NULL)
		CloseServiceHandle(scm);
} 

void ShowErr(CString msg)
{
	CString theMsg;
	theMsg = "Error:" + msg;
	CString theError;
	theError = CXUtils::decodeGetLastError();
	theMsg += " GetLastError:" + theError;

	CXUtils::WriteToTheLogFile(theMsg, FALSE);
	cout << (LPCSTR)theMsg << "\r\n";
}
void ServiceMain(DWORD argc, LPTSTR *argv)
{
	CXUtils::WriteToTheLogFile("ServiceMain Called");
	BOOL success;
	nServiceStatusHandle=RegisterServiceCtrlHandler(strServiceName,
		(LPHANDLER_FUNCTION)ServiceCtrlHandler);
	if(!nServiceStatusHandle)
	{
		return;
	}
	success=UpdateServiceStatus(SERVICE_START_PENDING,NO_ERROR,0,1,3000);
	if(!success)
	{
		return;
	}
	killServiceEvent=CreateEvent(0,TRUE,FALSE,0);
	if(killServiceEvent==NULL)
	{
		return;
	}
	success=UpdateServiceStatus(SERVICE_START_PENDING,NO_ERROR,0,2,1000);
	if(!success)
	{
		return;
	}
	success=StartServiceThread();
	if(!success)
	{
		return;
	}
	nServiceCurrentStatus=SERVICE_RUNNING;
	success=UpdateServiceStatus(SERVICE_RUNNING,NO_ERROR,0,0,0);
	if(!success)
	{
		return;
	}
	WaitForSingleObject(killServiceEvent,INFINITE);
	CloseHandle(killServiceEvent);
}



BOOL UpdateServiceStatus(DWORD dwCurrentState, DWORD dwWin32ExitCode,
					 DWORD dwServiceSpecificExitCode, DWORD dwCheckPoint,
					 DWORD dwWaitHint)
{
	CXUtils::WriteToTheLogFile("UpdateServiceStatus Called");
	BOOL success;
	SERVICE_STATUS nServiceStatus;
	nServiceStatus.dwServiceType=SERVICE_WIN32_OWN_PROCESS;
	nServiceStatus.dwCurrentState=dwCurrentState;
	if(dwCurrentState==SERVICE_START_PENDING)
	{
		nServiceStatus.dwControlsAccepted=0;
	}
	else
	{
		nServiceStatus.dwControlsAccepted=SERVICE_ACCEPT_STOP			
			|SERVICE_ACCEPT_SHUTDOWN;
	}
	if(dwServiceSpecificExitCode==0)
	{
		nServiceStatus.dwWin32ExitCode=dwWin32ExitCode;
	}
	else
	{
		nServiceStatus.dwWin32ExitCode=ERROR_SERVICE_SPECIFIC_ERROR;
	}
	nServiceStatus.dwServiceSpecificExitCode=dwServiceSpecificExitCode;
	nServiceStatus.dwCheckPoint=dwCheckPoint;
	nServiceStatus.dwWaitHint=dwWaitHint;

	success=SetServiceStatus(nServiceStatusHandle,&nServiceStatus);

	if(!success)
	{
		KillService();
		return success;
	}
	else
		return success;
}

BOOL StartServiceThread()
{	
	CXUtils::WriteToTheLogFile("StartServiceThread Called");
	DWORD id;
	hServiceThread=CreateThread(0,0,
		(LPTHREAD_START_ROUTINE)ServiceExecutionThread,
		0,0,&id);
	if(hServiceThread==0)
	{
		return false;
	}
	else
	{
		nServiceRunning=true;
		return true;
	}
}
/*
DWORD ServiceExecutionThread(LPDWORD param)
{
	while(nServiceRunning)
	{		
		CString abc;
		Beep(450,150);
		Sleep(4000);
	}
	return 0;
}
*/
DWORD ServiceExecutionThread(LPDWORD param)
{
	if (CoInitialize(NULL) != S_OK)
	{
//		if (localArgCount >1)
//		{
//			theMessage = "Error: Call to CoInitialize failed.  Unable to initialize COM.  Click Ok to exit.";
//			MessageBox(NULL,theMessage," CoInitialize Failed", MB_OK);
//		}

		return FALSE;
	}
#if defined(_DO_SPY)
		msOld.Checkpoint();
	
		CMallocSpy* pMallocSpy = new CMallocSpy;
		pMallocSpy->AddRef();
		::CoRegisterMallocSpy(pMallocSpy);

	    pMallocSpy->Clear();
//		pMallocSpy->SetBreakAlloc(2);  
//		pMallocSpy->SetBreakAlloc(5)  ;
//		pMallocSpy->SetBreakAlloc(7)  ;
//		pMallocSpy->SetBreakAlloc(9)  ;

#endif



	if (nServiceRunning)
	{		

//		CEncoderDlg theDlg;
		if (theDlg != NULL)
		{
			theApp.m_pActiveWnd = theDlg;
			theDlg->DoModal();
		}
		CString errorMsg;
//		CXUtils::DoEncoder("C:\\TestMusic.wav", "C:\\testmusic1.wma", "TestCDAudio", errorMsg);
//		CXUtils::DoDeviceEncoder(L"DEVICE://Default_Audio_Device", "C:\\testmusic1.wma", "TestCDAudio", errorMsg);
				
	}
//	while(nServiceRunning)
//	{		
//		CString errorMsg;
//		CXUtils::DoEncoder("C:\\TestMusic.wav", "C:\\testmusic1.wma", "TestCDAudio", errorMsg);
//		CXUtils::DoDeviceEncoder(L"DEVICE://Default_Audio_Device", "C:\\testmusic1.wma", "TestCDAudio", errorMsg);
//		
//		CString abc;
//		Beep(450,150);
//		Sleep(4000);
//		break;
//		
//	}

//	server.stop();

	CoUninitialize();
#if defined(_DO_SPY)

    //
    // Dump COM memory leaks
    //
    pMallocSpy->Dump();

    //
    // Unregister the malloc spy ...
    //
    ::CoRevokeMallocSpy();
    pMallocSpy->Release();
	delete pMallocSpy;

	msOld.DumpAllObjectsSince();
	msNew.Checkpoint();
	msDif.Checkpoint();
	msDif.Difference( msOld, msNew );
	msDif.DumpStatistics();
#endif 

	return 0;
}


void KillService()
{
	CXUtils::WriteToTheLogFile("KillService Called");
	nServiceRunning=false;
	if (theDlg != NULL)
	{
		theDlg->ForceCancel();
		time_t curTime = time(NULL);
		while ((curTime+30) > time(NULL))
		{
			if (theDlg->IsShutdownComplete() == FALSE)
				CXUtils::YieldToTheOS();
			else
				break;
		}
	}	

	SetEvent(killServiceEvent);
	UpdateServiceStatus(SERVICE_STOPPED,NO_ERROR,0,0,0);
}

void ServiceCtrlHandler(DWORD nControlCode)
{
	BOOL success;
	switch(nControlCode)
	{	
	case SERVICE_CONTROL_SHUTDOWN:
	case SERVICE_CONTROL_STOP:
		CXUtils::WriteToTheLogFile("ServiceCtrlHandler Called: SERVICE_CONTROL_SHUTDOWN");
		nServiceCurrentStatus=SERVICE_STOP_PENDING;
		success=UpdateServiceStatus(SERVICE_STOP_PENDING,NO_ERROR,0,1,3000);
		KillService();		
		return;
	default:
		break;
	}
	UpdateServiceStatus(nServiceCurrentStatus,NO_ERROR,0,0,0);
}