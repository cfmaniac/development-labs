#if !defined(AFX_ENCODERDLG_H__6A7CD364_CD0D_4D5B_82E3_C8A29248819D__INCLUDED_)
#define AFX_ENCODERDLG_H__6A7CD364_CD0D_4D5B_82E3_C8A29248819D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EncoderDlg.h : header file
//

//#include "MsgQueue.h"
//#include "HttpResponse.h"
//#include "HttpServer.h"

#ifdef _USEING_ENCODER_CODE_
	#include "DBVMediaEncoder.h"
#endif

//#include "EncoderCommand.h"
#include "HttpRetrieval.h"
#include "DatabaseOperations.h"
#include "ProcessingCommand.h"
#include <vector>
#include "afxwin.h"
#include "afxcmn.h"
using namespace std;

/////////////////////////////////////////////////////////////////////////////
// CEncoderDlg dialog

class CEncoderDlg : public CDialog , public CDatabaseOperations
	//, public CMsgQueue
{
// Construction
public:
	void ForceCancel();
	void SendAnyEmails(BOOL force = false);
	static BOOL SendErrorEmail(CString theMsg);
	void AddMessageBoxString(CString theMsg);
//	static HttpServer server;

	CString m_WindowTitle;
	CString m_FileCreateTime;

	CEncoderDlg(CWnd* pParent = NULL);   // standard constructor

	~CEncoderDlg();
	time_t timeOfLastMessageSend;
	time_t timeOfLastFtpSend;
	time_t lastErrorEmailSent;

	BOOL IsShutdownComplete();
private:
	int delayBetweenInfoDownloadsSecs;

	void DoTimerEvents() ;

	int responseSequenceIndex;
	HttpRetrieval theHttpRetrieval;
	BOOL shutdownInProgress;
	BOOL shutdownComplete;

#ifdef _USEING_ENCODER_CODE_

	CDBVMediaEncoder* doEncoderListOp(CString op, CString sessionID, 
											   CDBVMediaEncoder*newEncoder, 
											   bool &opSuccessful);
#endif

	BOOL doEventCheck(CMapStringToString &Result, int &ResponseStatus);
	BOOL doPatronCheck(CMapStringToString &Result, int &ResponseStatus);
	BOOL doAccessCheck(CMapStringToString &Result, int &ResponseStatus);

	BOOL parseAndExecute(string theArgs, CString& responseData, BOOL& RequestHandled);
	void buildStatusResponse(CString& responseData);

	struct theCommandStruct
	{
		CString theCommand;
		CString theID;
	};
//	BOOL executeCommands(CEncoderCommand theCommands, CString& responseData);


	static CStringArray theAdminIPs;
	static CStringArray theFilesToUpload;
	static CMapStringToString thePropertiesData;
	static BOOL LoadConfigProperties(CEncoderDlg* theParentDlg);
	void DoCommonShutdown();
//	void DoFtpUploads() ;

	CStringList theMessageArray;
	static CStringList theEmailMessageList;
	CTime theStartupTime;
	time_t lastStatusDump;

// Dialog Data
	//{{AFX_DATA(CEncoderDlg)
	enum { IDD = IDD_MAIN_DIALOG };
	CStatic	m_StarControl;
	CButton	m_CancelButton;
	CListBox	m_ActiveEncoderList;
	CListBox	m_MessageListBox;
	BOOL	m_AlwaysOnTop;
	CString	m_STAR;
	//}}AFX_DATA

	BOOL	m_ConfigDataRetrieved;
	CProcessingCommand* m_CurrentProcessingCommand;
	CProcessingCommand* m_UploadProcessingCommand;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEncoderDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	CPtrArray theActiveEncoders;
	CRITICAL_SECTION m_csEncoderArray;

// Implementation
protected:
	static BOOL ConfigPropertiesLoaded;
	static BOOL b_STOCK_UPDATED;
	static BOOL b_BOX_UPDATED;
	static BOOL b_ITEMS_UPDATED;
	static BOOL b_CUST_UPDATED;
	static BOOL b_INVOICE_UPDATED;

	BOOL b_SchoolProcessingInProgress;
	CString currentSchoolCode;
	int i_RetryCount;
//	BOOL b_AllProcessingIsComplete;

	CStringArray theDbDirectories;



	// Generated message map functions
	//{{AFX_MSG(CEncoderDlg)
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnTimer(UINT nIDEvent);
	virtual BOOL OnInitDialog();
	afx_msg void OnReloadProperties();
	afx_msg void OnCheck1();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	static int LoadTableData(CString DSN_Name, CString DBPath, CString DBFile);
protected:
	int ProcessConfigRetrieval(CMapStringToString& Result);
	int ProcessStockRetrieval(CMapStringToString& Result);
	int ProcessNewInvoiceRetrieval(CMapStringToString& Result);
public:
	int ProcessRetrievals(CMapStringToString& Result);
	static int FindStringInList(CStringList theDataList, CString findValue);
	static void AddToEmailMessageList(CString theMsg);
	static void propertyLookup(CString theKey, CString& theValue);
	static void propertySet(const char* theKey, CString& theValue);
	static void propertySetMakeLower(const char* theKey, CString& theValue);
	static void propertySetAsInteger(const char* theKey, CString& theValue);

	CString GetCurrentSchoolCode(void);
	int WriteOrderImportFile(CStringArray& theOutData, CString SchoolCode, int orderCount);
	CStringArray importFileList;
	CString invoicesIDsToMarkPending;
	static CPtrArray processingSteps;
	static CStringList stockUpdateValues;
	static CStringList statusUpdateValues;
	CString currentActiveEventList;

	int ProcessInProgressInvoiceResponse(CMapStringToString& Result);
	bool b_SendSyncResponseToRemote;
	bool b_ContinueIfRequestFailure;
	int i_httpErrorCount;
	int i_LastErrorWriteCount;
	static time_t l_uploadTicketInfoTime;
	static time_t l_downloadTicketInfoTime;
	static time_t l_refreshEventListTime;
	static time_t l_downloadPatronInfoTime;
	static time_t l_downloadAccessInfoTime;


	static bool b_interactiveMode;
	HICON m_hIcon;
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnLbnSelchangeMsgList();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedEnableUpload();
	CButton m_EnableUpload;
	int FindUploadFiles(CProcessingCommand* foundFilename);
	afx_msg void OnLvnColumnclickEventList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkEventList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedDownloadEvents();
	CListCtrl m_CurrentEventListCtrl;
	void doRetrievalErrorHandling(CString theCommand, CMapStringToString &Result);
	static int m_EventRefreshInterval;
	static int m_TicketInfoDownloadInterval;
	static int m_TicketInfoUploadInterval;
	static int m_PatronDownloadInterval;
	static int m_AccessPassDownloadInterval;


	CString m_ActivityChar;
	int m_BatchRequestsSent;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ENCODERDLG_H__6A7CD364_CD0D_4D5B_82E3_C8A29248819D__INCLUDED_)
