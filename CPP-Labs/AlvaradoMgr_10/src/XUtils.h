// Utils.h: interface for the CUtils class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_UTILS_H__77C62FDC_C277_4C4A_AD68_0FC17284D28B__INCLUDED_)
#define AFX_UTILS_H__77C62FDC_C277_4C4A_AD68_0FC17284D28B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#if XMLDLL_IMPORT
	#define DllExportMode   __declspec( dllexport )
#endif
#if XMLDLL_EXPORT
	#define DllExportMode   __declspec( dllexport )
#endif
#if !defined (DllExportMode)
	#define DllExportMode
#endif

#ifdef _USEING_ENCODER_CODE_
	#include "wmencode.h"
#endif

class DllExportMode CXUtils  
{
public:
	CXUtils();
	virtual ~CXUtils();
	static CString PadString(CString input, int padSize, CString padChar = " ");
	static void SplitCStringOnChar(CString in,
									char StripChar, 
									CString &out1,
									CString &out2);
	static void SplitCStringOnCString(CString in, 
									   CString SplitString, 
									   CString &out1,
									   CString &out2);
	static BYTE* ReadCStringFromBuffer(BYTE*& TheData, long NumBytes, CString &TheCString, long &BytesLeft);
	static void YieldToTheOS();
	static void TrimOffEndChar(CString in, 
								char StripChar, 
								CString &out1);
	static BOOL doDebugLogging;
	static void WriteToTheLogFile(CString theMsg, BOOL debugOnly = FALSE);
	static void GetEntireKeyValue(HKEY CurDefaultKey, const char* EntireSubKey, 
										const char* Key, char* Value, const char* Default);
	static BOOL DoLogging;
	static CString LoggingFileName;
	static const DWORD SHELL_EXECUTE_SUCCESS_THRESHOLD;
	static void FillAppInfo();
	static CString TheCommandLine;
	static CString TheRunDrive;
	static CString TheAppPath;
	static CString TheExecutablePath;

	static BOOL WriteFileToDisk(CString TheFilename, BYTE* TheData, DWORD NumBytes);
	static BOOL DoesFileExist(CString TheFilenamePath);
	static int Move(const char *OldPath, const char *NewPath);
	static int CopyFile(const char *OldPath, const char *NewPath);
	static void GMT_TO_LOCAL(SYSTEMTIME ZTime, SYSTEMTIME& localTime);
	static BOOL ValidateTimeString(CString month, CString day, CString year, CString hour, CString min, CString sec, CTime& outCtime);
	static BOOL DoTheEmailSend(CString smtpServer, CString from, CString to, CString subject, CStringList& body, CString&errorMsg);
	
	static BOOL DoEncoder(CString inName, CString outName, CString vodProfileName, CString &errorMsg);
	static BOOL DoDeviceEncoder(CString deviceName, CString outName, CString vodProfileName, CString &errorMsg);
	static CString FormatPhone(CString inPhone);

//	static void CreateAudioProfile();
#ifdef _USEING_ENCODER_CODE_
	static BOOL FindNamedEncoderProfile(IWMEncoder* pEncoder,    
									  IWMEncSourceGroup* pSrcGrp, 
									  CString theName);
#endif
	static void ReplaceCStringWithCString(CString in, 
									   CString findString, 
									   CString replaceString,
									   CString &out,
									   BOOL searchbackward);
	static void ReplaceCStringWithCStringNC(CString in, 
									   CString findString, 
									   CString replaceString,
									   CString &out);
	static BOOL WasHResultGood(HRESULT inhr, CString & errorMsg);
	static HANDLE Execute(const char* CommandLine, PROCESS_INFORMATION& pi);
	static BOOL DoExecute(CString CommandLine, CString& errorMsg, BOOL WaitUntilComplete = FALSE);
	static void CleanupDirectories(CString ThePath, CString TheTempDirName);
	static void DeleteFileWildcard(CString ThePath, CString TheWildcardFilename);
	static void SetDebugLoggingFlag(BOOL onOff);
	static void GetFileExtension(CString in, CString &out);
	static void GetLastErrorString(CString& theMsg, DWORD lastErrorDWORD = 0);
	static void UrlEncodeString(CString inString, CString& outString);
	static CString InitURLEncoder();
	static CString URLEncoder(CString InString);
	static CString dontNeedEncoding;

	static CString decodeGetLastError();
	static int SafeRelease(IUnknown* theValue);
	static BOOL CreateTheDirectory(CString currentDestinationDirectory, CString& errorMsg);


	static BOOL SafeLookup(CMapStringToString & theMap, CString TheKey, CString & TheValue, CString defaultValue = "");
};

#endif // !defined(AFX_UTILS_H__77C62FDC_C277_4C4A_AD68_0FC17284D28B__INCLUDED_)
