// StockInfoUpdateDlg.cpp : implementation file
//

#include "stdafx.h"
#include "AlvaradoMgr.h"
#include "StockInfoUpdateDlg.h"
#include ".\stockinfoupdatedlg.h"


// CStockInfoUpdateDlg dialog

IMPLEMENT_DYNAMIC(CStockInfoUpdateDlg, CDialog)
CStockInfoUpdateDlg::CStockInfoUpdateDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CStockInfoUpdateDlg::IDD, pParent)
	, s_messageText(_T(""))
	, b_doSync(false)
	, b_doReportOnly(false)
{
}

CStockInfoUpdateDlg::~CStockInfoUpdateDlg()
{
}

void CStockInfoUpdateDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_MESSAGE_TEXT, s_messageText);
	DDX_Control(pDX, IDC_FILE_LISTBOX, c_fileListBox);
}


BEGIN_MESSAGE_MAP(CStockInfoUpdateDlg, CDialog)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_BN_CLICKED(ID_REPORT_ONLY, OnBnClickedReportOnly)
	ON_BN_CLICKED(IDCANCEL, OnBnClickedCancel)
END_MESSAGE_MAP()


// CStockInfoUpdateDlg message handlers

void CStockInfoUpdateDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	b_doSync = true;
	OnOK();
}

void CStockInfoUpdateDlg::OnBnClickedReportOnly()
{
	// TODO: Add your control notification handler code here
	b_doSync = true;
	b_doReportOnly = true;
	OnOK();
}

void CStockInfoUpdateDlg::OnBnClickedCancel()
{
	// TODO: Add your control notification handler code here
	OnCancel();
}

BOOL CStockInfoUpdateDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	for (int i=0; i<fileList.GetCount(); i++)
	{
		c_fileListBox.AddString(fileList.GetAt(i));
	}
	fileList.RemoveAll();

	if (c_fileListBox.GetCount() <= 0)
	{
		c_fileListBox.SetWindowPos(NULL,0,0,0,0,
			SWP_HIDEWINDOW | SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOSIZE | SWP_NOZORDER);
	}

	// TODO:  Add extra initialization here

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CStockInfoUpdateDlg::AddEntryToList(CString entryToAdd)
{
	fileList.Add(entryToAdd);
}
