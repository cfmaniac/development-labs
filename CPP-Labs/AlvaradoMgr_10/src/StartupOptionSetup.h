#pragma once


// CStartupOptionSetup dialog

class CStartupOptionSetup : public CDialog
{
	DECLARE_DYNAMIC(CStartupOptionSetup)

public:
	CStartupOptionSetup(CWnd* pParent = NULL);   // standard constructor
	virtual ~CStartupOptionSetup();

// Dialog Data
	enum { IDD = IDD_STARTUP_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	HICON m_hIcon;

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedImportOrders();
	BOOL b_ImportOrders;
	BOOL b_UpdateInvoiceStatus;
	BOOL b_TestEmail;
	BOOL b_DumpConfig;
	BOOL b_ShowCmdLineArgs;
	afx_msg void OnBnClickedUpdateInvoiceStatus();
	afx_msg void OnBnClickedDumpConfig();
	afx_msg void OnBnClickedTestEmail();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnBnClickedShowCmdArgs();
};
