#include "stdafx.h"
#include ".\dbvtablecolumns.h"

CStringArray CDBVTableColumns::DBVInvoiceColumns;
CStringArray CDBVTableColumns::DBVStockColumns;


CDBVTableColumns::CDBVTableColumns(void)
{
}

CDBVTableColumns::~CDBVTableColumns(void)
{
}

void CDBVTableColumns::InitDBVColumnArrays(void)
{
	DBVInvoiceColumns.Add("INV_ECOM_INVOICE_ID");
	DBVInvoiceColumns.Add("INV_OEM_ID");
	DBVInvoiceColumns.Add("INV_ECOM_ACCOUNT_ID");
	DBVInvoiceColumns.Add("INV_ACCOUNT_ID");
	DBVInvoiceColumns.Add("INV_TYPE");
	DBVInvoiceColumns.Add("INV_INVOICE_DATE");
	DBVInvoiceColumns.Add("INV_CUST_SVC_CREDIT_USED");
	DBVInvoiceColumns.Add("INV_GROSS_AMOUNT");
	DBVInvoiceColumns.Add("INV_TAX_AMOUNT");
	DBVInvoiceColumns.Add("INV_NET_AMOUNT");
	DBVInvoiceColumns.Add("INV_SHIPPING_AMOUNT");
	DBVInvoiceColumns.Add("INV_INVOICE_CREDIT_USED");
	DBVInvoiceColumns.Add("INV_INVOICE_AMOUNT");
	DBVInvoiceColumns.Add("INV_LATE_FEE");
	DBVInvoiceColumns.Add("INV_PAID_AMOUNT");
	DBVInvoiceColumns.Add("INV_PAID_DATE");
	DBVInvoiceColumns.Add("INV_STATE");
	DBVInvoiceColumns.Add("INV_PAY_METHOD");
	DBVInvoiceColumns.Add("INV_SHIP_METHOD_ID");
	DBVInvoiceColumns.Add("INV_ECOM_SHIP_ADDR_ID");
	DBVInvoiceColumns.Add("INV_ECOM_CREDIT_CARD_ID");
	DBVInvoiceColumns.Add("INV_JOIN_ECOM_ACCOUNT_ID");
	DBVInvoiceColumns.Add("INV_TAXING_STATE");
	DBVInvoiceColumns.Add("INV_DPI_STATUS_ID");
	DBVInvoiceColumns.Add("INV_SHIP_FIRST_NAME");
	DBVInvoiceColumns.Add("INV_SHIP_LAST_NAME");
	DBVInvoiceColumns.Add("INV_SHIP_OTHER_NAME");
	DBVInvoiceColumns.Add("INV_SHIP_ADDRESS1");
	DBVInvoiceColumns.Add("INV_SHIP_ADDRESS2");
	DBVInvoiceColumns.Add("INV_SHIP_ADDRESS3");
	DBVInvoiceColumns.Add("INV_SHIP_ADDRESS4");
	DBVInvoiceColumns.Add("INV_SHIP_CITY");
	DBVInvoiceColumns.Add("INV_SHIP_STATE_PROV");
	DBVInvoiceColumns.Add("INV_SHIP_POSTAL_CODE");
	DBVInvoiceColumns.Add("INV_SHIP_COUNTRY_CODE");
	DBVInvoiceColumns.Add("INV_SHIP_PHONE_1");
	DBVInvoiceColumns.Add("INV_SHIP_PHONE_EXT_1");
	DBVInvoiceColumns.Add("INV_SHIP_PHONE_2");
	DBVInvoiceColumns.Add("INV_SHIP_PHONE_EXT_2");
	DBVInvoiceColumns.Add("INV_CARD_NUMBER");
	DBVInvoiceColumns.Add("INV_CARD_EXP_DATE");
	DBVInvoiceColumns.Add("INV_CARD_STATUS");
	DBVInvoiceColumns.Add("INV_CARD_TYPE");
	DBVInvoiceColumns.Add("INV_CARD_FIRST_NAME");
	DBVInvoiceColumns.Add("INV_CARD_LAST_NAME");
	DBVInvoiceColumns.Add("INV_CARD_OTHER_NAME");
	DBVInvoiceColumns.Add("INV_CARD_ADDRESS1");
	DBVInvoiceColumns.Add("INV_CARD_ADDRESS2");
	DBVInvoiceColumns.Add("INV_CARD_ADDRESS3");
	DBVInvoiceColumns.Add("INV_CARD_ADDRESS4");
	DBVInvoiceColumns.Add("INV_CARD_CITY");
	DBVInvoiceColumns.Add("INV_CARD_STATE_PROV");
	DBVInvoiceColumns.Add("INV_CARD_POSTAL_CODE");
	DBVInvoiceColumns.Add("INV_CARD_COUNTRY_CODE");
	DBVInvoiceColumns.Add("INV_CARD_CVV2");
	DBVInvoiceColumns.Add("INV_CARD_PHONE");
	DBVInvoiceColumns.Add("INV_TOTAL_SHIP_WEIGHT");
	DBVInvoiceColumns.Add("INV_MOD_ACCOUNT_ID");
	DBVInvoiceColumns.Add("INV_MOD_DATE");
	DBVInvoiceColumns.Add("INV_ECOM_SHIPPING_LABEL_ID");
	DBVInvoiceColumns.Add("II_ECOM_INVOICE_ITEM_ID");
	DBVInvoiceColumns.Add("II_ECOM_ITEM_ID");
	DBVInvoiceColumns.Add("II_QUANTITY");
	DBVInvoiceColumns.Add("II_ITEM_AMOUNT");
	DBVInvoiceColumns.Add("II_ECOM_ITEM_COST_ID");
	DBVInvoiceColumns.Add("II_TRACKING_NUMBER");
	DBVInvoiceColumns.Add("II_ITEM_STATUS");
	DBVInvoiceColumns.Add("II_ECOM_SHIP_CARRIER_ID");
	DBVInvoiceColumns.Add("II_ECOM_RMA_ID");
	DBVInvoiceColumns.Add("II_EXTENDED_AMOUNT");
	DBVInvoiceColumns.Add("IIO_INVOICE_OPTION_ID");
	DBVInvoiceColumns.Add("IIO_ECOM_ITEM_OPTION_ID");

	DBVStockColumns.Add("ECOM_ITEM_ID");
	DBVStockColumns.Add("ECOM_OEM_ID");
	DBVStockColumns.Add("ECOM_PURCHASE_COUNT");
	DBVStockColumns.Add("ECOM_ITEM_TITLE");
	DBVStockColumns.Add("ECOM_START_DATE");
	DBVStockColumns.Add("ECOM_END_DATE");
	DBVStockColumns.Add("ECOM_NO_END_DATE");
	DBVStockColumns.Add("ECOM_PERSONALIZE_TYPE");
	DBVStockColumns.Add("ECOM_ORDER_ID");
	DBVStockColumns.Add("ECOM_PART_NUMBER");
	DBVStockColumns.Add("ECOM_ITEM_WEIGHT");
	DBVStockColumns.Add("ECOM_INVENTORY_COUNT");
	DBVStockColumns.Add("ECOM_RETURNABLE");

	DBVStockColumns.Add("ECOM_ITEM_OPTION_ID");
	DBVStockColumns.Add("ECOM_OPTION_KEY");
	DBVStockColumns.Add("ECOM_OPTION_NAME");
	DBVStockColumns.Add("ECOM_PURCHASE_COUNT");
	DBVStockColumns.Add("ECOM_EXTRA_COST_AMOUNT");

	DBVStockColumns.Add("ECOM_ITEM_COST_ID");
	DBVStockColumns.Add("COST_START_DATE");
	DBVStockColumns.Add("COST_END_DATE");
	DBVStockColumns.Add("COST_NO_END_DATE");
	DBVStockColumns.Add("COST_PUBLIC_COST");
	DBVStockColumns.Add("COST_MEMBER_COST");
	DBVStockColumns.Add("COST_EXTRA_SHIP_COST");
	DBVStockColumns.Add("COST_CONSIGNED_COST");

	DBVStockColumns.Add("ECOM_INVENTORY_ID");
	DBVStockColumns.Add("ECOM_IO_ID_SIZE");
	DBVStockColumns.Add("ON_HAND_COUNT");
	DBVStockColumns.Add("ON_ORDER_COUNT");
	DBVStockColumns.Add("ROYSUP");

	//Allocated: Altnum, Odr_date, Cardnum, Lastname Firstname Addr Addr2 City State Zipcode Phone, Phone2, Odr_date Cardtype, Exp, Net, Gross, 
//				CString cols = "Custnum, Custtype, , Company, ";
//				cols.Append("County, ,, Country, 
//				cols.Append("Orig_ad, Ctype, Last_ad, Catcount, , Paymethod,  ";
//				cols.Append("Shiplist, Expired, Badcheck, Orderrec,Ord_freq, Comment, Exempt ";

/*
	DBVInvoiceCMSCustMap.SetAt("INV_ECOM_INVOICE_ID",	"");
	DBVInvoiceCMSCustMap.SetAt("INV_OEM_ID,				"");
	DBVInvoiceCMSCustMap.SetAt("INV_ECOM_ACCOUNT_ID",	"Altnum");
	DBVInvoiceCMSCustMap.SetAt("INV_ACCOUNT_ID",		"Altnum");
	DBVInvoiceCMSCustMap.SetAt("INV_TYPE",				"");
	DBVInvoiceCMSCustMap.SetAt("INV_INVOICE_DATE",		"Odr_date");
	DBVInvoiceCMSCustMap.SetAt("INV_CUST_SVC_CREDIT_USED",	"");
	DBVInvoiceCMSCustMap.SetAt("INV_GROSS_AMOUNT",		"Gross");
	DBVInvoiceCMSCustMap.SetAt("INV_TAX_AMOUNT",		"");
	DBVInvoiceCMSCustMap.SetAt("INV_NET_AMOUNT",		"Net");
	DBVInvoiceCMSCustMap.SetAt("INV_SHIPPING_AMOUNT",	"");
	DBVInvoiceCMSCustMap.SetAt("INV_INVOICE_CREDIT_USED",	"");
	DBVInvoiceCMSCustMap.SetAt("INV_INVOICE_AMOUNT",	"");
	DBVInvoiceCMSCustMap.SetAt("INV_LATE_FEE",			"");
	DBVInvoiceCMSCustMap.SetAt("INV_PAID_AMOUNT",		"");
	DBVInvoiceCMSCustMap.SetAt("INV_PAID_DATE",			"");
	DBVInvoiceCMSCustMap.SetAt("INV_STATE", 			"");
	DBVInvoiceCMSCustMap.SetAt("INV_PAY_METHOD",		"");
	DBVInvoiceCMSCustMap.SetAt("INV_SHIP_METHOD_ID", 	"");
	DBVInvoiceCMSCustMap.SetAt("INV_ECOM_SHIP_ADDR_ID", 	"");
	DBVInvoiceCMSCustMap.SetAt("INV_ECOM_CREDIT_CARD_ID", 	"");
	DBVInvoiceCMSCustMap.SetAt("INV_JOIN_ECOM_ACCOUNT_ID", 	"");
	DBVInvoiceCMSCustMap.SetAt("INV_TAXING_STATE", 	"");
	DBVInvoiceCMSCustMap.SetAt("INV_DPI_STATUS_ID", 	"");
	DBVInvoiceCMSCustMap.SetAt("INV_SHIP_FIRST_NAME", 	"Firstname");
	DBVInvoiceCMSCustMap.SetAt("INV_SHIP_LAST_NAME", 	"Lastname");
	DBVInvoiceCMSCustMap.SetAt("INV_SHIP_OTHER_NAME", 	"");
	DBVInvoiceCMSCustMap.SetAt("INV_SHIP_ADDRESS1", 	"Addr");
	DBVInvoiceCMSCustMap.SetAt("INV_SHIP_ADDRESS2", 	"Addr2");
	DBVInvoiceCMSCustMap.SetAt("INV_SHIP_ADDRESS3", 	"");
	DBVInvoiceCMSCustMap.SetAt("INV_SHIP_ADDRESS4", 	"");
	DBVInvoiceCMSCustMap.SetAt("INV_SHIP_CITY", 		"City");
	DBVInvoiceCMSCustMap.SetAt("INV_SHIP_STATE_PROV", 	"State");
	DBVInvoiceCMSCustMap.SetAt("INV_SHIP_POSTAL_CODE", 	"Zipcode");
	DBVInvoiceCMSCustMap.SetAt("INV_SHIP_COUNTRY_CODE", "");
	DBVInvoiceCMSCustMap.SetAt("INV_SHIP_PHONE_1", 		"Phone");
	DBVInvoiceCMSCustMap.SetAt("INV_SHIP_PHONE_EXT_1", 	"");
	DBVInvoiceCMSCustMap.SetAt("INV_SHIP_PHONE_2", 		"Phone2");
	DBVInvoiceCMSCustMap.SetAt("INV_SHIP_PHONE_EXT_2", 	"");
	DBVInvoiceCMSCustMap.SetAt("INV_CARD_NUMBER", 		"Cardnum");
	DBVInvoiceCMSCustMap.SetAt("INV_CARD_EXP_DATE", 	"Exp");
	DBVInvoiceCMSCustMap.SetAt("INV_CARD_STATUS", 		"");
	DBVInvoiceCMSCustMap.SetAt("INV_CARD_TYPE", 		"Cardtype");
	DBVInvoiceCMSCustMap.SetAt("INV_CARD_FIRST_NAME", 	"");
	DBVInvoiceCMSCustMap.SetAt("INV_CARD_LAST_NAME", 	"");
	DBVInvoiceCMSCustMap.SetAt("INV_CARD_OTHER_NAME", 	"");
	DBVInvoiceCMSCustMap.SetAt("INV_CARD_ADDRESS1", 	"");
	DBVInvoiceCMSCustMap.SetAt("INV_CARD_ADDRESS2", 	"");
	DBVInvoiceCMSCustMap.SetAt("INV_CARD_ADDRESS3", 	"");
	DBVInvoiceCMSCustMap.SetAt("INV_CARD_ADDRESS4", 	"");
	DBVInvoiceCMSCustMap.SetAt("INV_CARD_CITY", 		"");
	DBVInvoiceCMSCustMap.SetAt("INV_CARD_STATE_PROV", 	"");
	DBVInvoiceCMSCustMap.SetAt("INV_CARD_POSTAL_CODE", 	"");
	DBVInvoiceCMSCustMap.SetAt("INV_CARD_COUNTRY_CODE", "");
	DBVInvoiceCMSCustMap.SetAt("INV_CARD_CVV2", 		"");
	DBVInvoiceCMSCustMap.SetAt("INV_CARD_PHONE",		"");
	DBVInvoiceCMSCustMap.SetAt("INV_TOTAL_SHIP_WEIGHT", "");
	DBVInvoiceCMSCustMap.SetAt("INV_MOD_ACCOUNT_ID", 	"");
	DBVInvoiceCMSCustMap.SetAt("INV_MOD_DATE",			"");
	DBVInvoiceCMSCustMap.SetAt("INV_ECOM_SHIPPING_LABEL_ID", 	"");
	DBVInvoiceCMSCustMap.SetAt("II_ECOM_INVOICE_ITEM_ID", 		"");
	DBVInvoiceCMSCustMap.SetAt("II_ECOM_ITEM_ID", 		"");
	DBVInvoiceCMSCustMap.SetAt("II_QUANTITY", 			"");
	DBVInvoiceCMSCustMap.SetAt("II_ITEM_AMOUNT", 		"");
	DBVInvoiceCMSCustMap.SetAt("II_ECOM_ITEM_COST_ID", 	"");
	DBVInvoiceCMSCustMap.SetAt("II_TRACKING_NUMBER", 	"");
	DBVInvoiceCMSCustMap.SetAt("II_ITEM_STATUS", 		"");
	DBVInvoiceCMSCustMap.SetAt("II_ECOM_SHIP_CARRIER_ID", 	"");
	DBVInvoiceCMSCustMap.SetAt("II_ECOM_RMA_ID", 			"");
	DBVInvoiceCMSCustMap.SetAt("II_EXTENDED_AMOUNT",		"");
	DBVInvoiceCMSCustMap.SetAt("IIO_INVOICE_OPTION_ID", 	"");
	DBVInvoiceCMSCustMap.SetAt("IIO_ECOM_ITEM_OPTION_ID", 	"");

				CString cols = "Custnum, Altnum, Custtype, Lastname, Firstname, Company, ";
				cols.Append("Addr, Addr2, City, County, State, Zipcode, Country, Phone, Phone2, ";
				cols.Append("Orig_ad, Ctype, Last_ad, Catcount, Odr_date, Paymethod, Cardnum, Cardtype, Exp, ";
				cols.Append("Shiplist, Expired, Badcheck, Orderrec, Net, Gross, Ord_freq, Comment, Exempt ";
*/

}
