##########################################################
# AlvaradMgr watcher script
##########################################################
# load the properties
# this path needs to be updated
#
#	powershell C:\development\powershell\ServiceMonitor\AlvaradoMgr_Watcher.ps1
#
$propFileName="C:\AlvaradoMgr\ServiceMonitor\AlvaradoMgr_Watcher.properties"
$propsFileContents = ( Get-Content $propFileName | Out-String )

Write-Host $propsFileContents
$props = ( ConvertFrom-StringData $propsFileContents ) 
Write-Host Properties Loaded

#load arrays from the prop file lists
$toArray = @() # notification email address

foreach ($addr in $props.NotifyEmailAddress.Split(","))
{
	$toArray = $toArray + $addr.trim()
}
write-host $otherServices.length

$otherServices = @()
if ($props.OtherServices.length -gt 0)
{
	foreach ($oService in $props.OtherServices.Split(","))
	{
		$otherServices = $otherServices + $oService.trim()
	}
}

##########################################################
$notificationStirng="<h1>$($props.PartnerName) Status Monitor</h1>"
# flag for sending the notfication string
##########################################################

# get current log file size
if ((Get-Item "$($props.LOCAL_BASE_DIR)\$($props.LogfileName)").length -gt $props.logFileSizeLimit)
{
	# file is over the limit remove it
	rm "$($props.LOCAL_BASE_DIR)\$($props.LogfileName)"
	write-host "Delete large log file"
}

# check the status of other services
if ($otherServices.length -gt 0)
{
	$sendNotification=$false
	$tempStr="$notificationStirng<ul>Checking alternate services</ul>"
	foreach ($otherServiceItem in $otherServices)
	{
		$altService = Get-Service -DisplayName $otherServiceItem
		# if any service is not running then set the notification flag
		Write-host $altService.status
		if ($altService.status -ne "Running")
		{
			$sendNotification=$true
			$tempStr=$tempStr+"<li>$otherServiceItem is not running, current state is $($altService.status)</li>"
		}
	}
	
	if ($sendNotification)
	{
		send-mailmessage -BodyAsHtml -to $toArray -from "$($props.PartnerName) <ticketinghelp@neulion.com>" -subject "$($props.PartnerName) - $($props.ServiceName) Service Monitor - Alt Service Notification" -smtpServer smtp.neulion.com -body $tempStr
	}
}
# end check other services

# check the 
Write-Host Checking Service - $props.ServiceName
$service = Get-Service -DisplayName $props.ServiceName
Write-Host $service.status
if ($service.status -eq "Stopped")
{
	$tempStr="Service $($props.ServiceName) is not running"
	write-host $tempStr
	$notificationStirng=$notificationStirng+$tempStr+"<br>"
	
	# do some clean up before restarting
	# remove the log file
	
	rm "$($props.LOCAL_BASE_DIR)\$($props.LogfileBackupName)" -ErrorAction SilentlyContinue 
	rni "$($props.LOCAL_BASE_DIR)\$($props.LogfileName)" "$($props.LOCAL_BASE_DIR)\$($props.LogfileBackupName)" -ErrorAction SilentlyContinue
	rm "$($props.LOCAL_WORKING_DIR)\*" -ErrorAction SilentlyContinue
	
	# cleanup work folder
	$tempStr="<ol>Doing cleanup:<li>Removing log backup - $($props.LogfileName)<li>Making abackup of the log file to $($props.LogfileBackupName)<li>Cleanup the work folder</ol>"
	write-host "Doing some clean before restarting"
	$notificationStirng=$notificationStirng+$tempStr+"<br>"
	
	# try to start the service
	Start-Service -DisplayName $props.ServiceName
	
	$tempStr="$($props.ServiceName) Service restart request sent"
	write-host $tempStr
	$notificationStirng=$notificationStirng+$tempStr+"<br>"
	
	write-host "send email"
	send-mailmessage -BodyAsHtml -to $toArray -from "$($props.PartnerName) <ticketinghelp@neulion.com>" -subject "$($props.PartnerName) - $($props.ServiceName) Service Monitor" -smtpServer smtp.neulion.com -body $notificationStirng -Attachment "$($props.LOCAL_BASE_DIR)\$($props.LogfileBackupName)"
	
} else {
	write-host "Service $($props.ServiceName) is running"
}

#Write-Host ($service | Format-Table | Out-String)




 