AlvaradoMgr Service Monitor
The service monitor is a powershell script that performs these tasks
1)	Delete the AlvaradoMgr.log file when it becomes to large
2)	Restart the AlvaradoMgr service it is not running
3)	Will send an email with the log file to NeuLion when the AlvaradoMgr service needs to be restarted
4)	Can also check other services and will notify NeuLion if they are not running.  Does attempt to restart these.  Services can be added to the properties file as a list with commas

Schedule Setup Tips
The command used in test was this:
C:\WINDOWS\system32\cmd.exe /c powershell <path to the script file, see below>

The script should be scheduled to run every 15 or 30 minutes.

Another way to schedule the task
http://blogs.technet.com/b/heyscriptingguy/archive/2012/08/11/weekend-scripter-use-the-windows-task-scheduler-to-run-a-windows-powershell-script.aspx
On newer Windows operating systems the command will probably have to run as administrator.

Configuration
Allow scripts to run in powershell
From the cmd prompt run this command
\> set-executionpolicy remotesigned



Preferred location of the script and properties file is 
C:\AlvaradoMgr\ServiceMonitor
Path updates:
Script File, this is the path that will be used in the scheduled task.
C:\AlvaradoMgr\ServiceMonitor\AlvaradoMgr_Watcher.ps1
Update location of the properties file in the script
Line 9:
$propFileName="C:\AlvaradoMgr\ServiceMonitor\AlvaradoMgr_Watcher.properties"

Properties File:
C:\AlvaradoMgr\ServiceMonitor\AlvaradoMgr_Watcher.properties
These should match up with the AlvaradoMgr and Gatelink paths.  These can be found in the AlvaradoMgr.properties 
Line 7-10
LOCAL_BASE_DIR=C:\\AlvaradoMgr
LOCAL_IMPORT_FILE_BASE_DIR=C:\\AlvaradoMgr\\import
LOCAL_EXPORT_FILE_BASE_DIR=C:\\AlvaradoMgr\\export
LOCAL_WORKING_DIR=C:\\AlvaradoMgr\\work
