
// EncoderCallbacks.h: interface for the CEncoderCallbacks class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ENCODERCALLBACKS_H__07ACB1BF_3A12_48F5_9941_A31A33611E11__INCLUDED_)
#define AFX_ENCODERCALLBACKS_H__07ACB1BF_3A12_48F5_9941_A31A33611E11__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <atlbase.h>
extern CComModule _Module;
#include <atlcom.h>

#include "wmencode.h"
#include "wmsencid.h"

#define EVENT_ID        100
#define LIB_VERMAJOR    1
#define LIB_VERMINOR    0

class CEncoderCallbacks : public IDispEventImpl< EVENT_ID, 
                                         CEncoderCallbacks,
                                         &DIID__IWMEncoderEvents,
                                         &LIBID_WMEncoderLib,
                                         LIB_VERMAJOR, 
                                         LIB_VERMINOR >
{
public:
    //
    // Encoder Event Sink using ATL 3.0
    //
    // Note: This example uses _ATL_FUNC_INFO and SINK_ENTRY_INFO 
    // because the SINK_ENTRY_EX macro doesn't allow enumerated
    // types to be passed as parameters to IDispatch.

    static _ATL_FUNC_INFO StateChangeInfo;
    static _ATL_FUNC_INFO ErrorInfo;
    static _ATL_FUNC_INFO ArchiveStateChangeInfo;
    static _ATL_FUNC_INFO ConfigChangeInfo;
    static _ATL_FUNC_INFO ClientConnectInfo;
    static _ATL_FUNC_INFO ClientDisconnectInfo;
    static _ATL_FUNC_INFO SourceStateChangeInfo;
    static _ATL_FUNC_INFO IndexerStateChangeInfo;

BEGIN_SINK_MAP(CEncoderCallbacks)
    SINK_ENTRY_INFO( EVENT_ID,
                     DIID__IWMEncoderEvents,
                     DISPID_ENCODEREVENT_ERROR,
                     OnError, 
                     &ErrorInfo )
    SINK_ENTRY_INFO( EVENT_ID, 
                     DIID__IWMEncoderEvents,
                     DISPID_ENCODEREVENT_STATECHANGE,
                     OnStateChange,
                     &StateChangeInfo )
    SINK_ENTRY_INFO( EVENT_ID,
                     DIID__IWMEncoderEvents,
                     DISPID_ENCODEREVENT_ARCHIVESTATECHANGE,
                     OnArchiveStateChange,
                     &ArchiveStateChangeInfo )
/*
    SINK_ENTRY_INFO( EVENT_ID,
                     DIID__IWMEncoderEvents,
                     DISPID_ENCODEREVENT_CONFIGCHANGE,
                     OnConfigChange,
                     &ConfigChangeInfo)
    SINK_ENTRY_INFO( EVENT_ID,
                     DIID__IWMEncoderEvents,
                     DISPID_ENCODEREVENT_CLIENTCONNECT,
                     OnClientConnect,
                     &ClientConnectInfo)
    SINK_ENTRY_INFO( EVENT_ID,
                     DIID__IWMEncoderEvents,
                     DISPID_ENCODEREVENT_CLIENTDISCONNECT,
                     OnClientDisconnect,
                     &ClientDisconnectInfo)
    SINK_ENTRY_INFO( EVENT_ID,
                     DIID__IWMEncoderEvents,
                     DISPID_ENCODEREVENT_SRCSTATECHANGE,
                     OnSourceStateChange,
                     &SourceStateChangeInfo )
*/
    SINK_ENTRY_INFO( EVENT_ID,
                     DIID__IWMEncoderEvents,
                     DISPID_ENCODEREVENT_INDEXERSTATECHANGE,
                     OnIndexerStateChange,
                     &IndexerStateChangeInfo )
END_SINK_MAP()

public:
    STDMETHOD(OnStateChange)(/*[in]*/ WMENC_ENCODER_STATE enumState);
//    STDMETHOD(OnSourceStateChange)(
//                 /*[in]*/WMENC_SOURCE_STATE enumState,
//                 /*[in]*/WMENC_SOURCE_TYPE enumType,
//                 /*[in]*/short iIndex,
//                 /*[in]*/BSTR bstrSourceGroup);
    STDMETHOD(OnError)(/*[in]*/ long hResult);
    STDMETHOD(OnArchiveStateChange)(
                 /*[in]*/ WMENC_ARCHIVE_TYPE enumArchive,
                 /*[in]*/ WMENC_ARCHIVE_STATE enumState );
//    STDMETHOD(OnConfigChange)(/*[in]*/ long hResult, /*[in]*/ BSTR bstr);
//    STDMETHOD(OnClientConnect)(
//                 /*[in]*/ WMENC_BROADCAST_PROTOCOL protocol,
//                 /*[in]*/ BSTR bstr);
//    STDMETHOD(OnClientDisconnect)(
//                 /*[in]*/ WMENC_BROADCAST_PROTOCOL protocol,
//                 /*[in]*/ BSTR bstr);   
    STDMETHOD(OnIndexerStateChange)(
                 /*[in]*/ WMENC_INDEXER_STATE enumIndexerState,
                 /*[in]*/ BSTR bstrFile );

	BOOL initHasOccurred;
	BOOL shutdownHasOccurred;

    HRESULT CEncoderCallbacks::Init( IWMEncoder* pEncoder )
    {
        HRESULT hr = 0;
		if (initHasOccurred == FALSE)
		{
	        hr = DispEventAdvise( pEncoder );
			initHasOccurred = TRUE;
		    if( FAILED( hr ) ) 
			{
			}
		}
		return hr;
	}

    HRESULT CEncoderCallbacks::ShutDown( IWMEncoder* pEncoder )
    {
        HRESULT hr = 0;
		if (shutdownHasOccurred == FALSE)
		{
			hr = DispEventUnadvise( pEncoder );
			shutdownHasOccurred = TRUE;
			
	        if( FAILED( hr ) ) 
		    {
			}
		}
        return hr;
    }

    CEncoderCallbacks();
	virtual ~CEncoderCallbacks();

	void setCDBVMediaEncoder(void* theClass);
	void* theEncoderClass;
	BOOL encoderMessagesReceived; 
	BOOL getEncoderMessagesReceivedFlag();


//	CDBVMediaEncoder* theEncoderClass;
};

#endif // !defined(AFX_ENCODERCALLBACKS_H__07ACB1BF_3A12_48F5_9941_A31A33611E11__INCLUDED_)
