// EncoderCallbacks.cpp: implementation of the CEncoderCallbacks class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "AlvaradoMgr.h"
#include "XUtils.h"
#include "DBVMediaEncoder.h"
#include "EncoderCallbacks.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

CComModule _Module;

_ATL_FUNC_INFO CEncoderCallbacks::ErrorInfo = {CC_STDCALL, 
         VT_ERROR, 1, { VT_I4 } };
_ATL_FUNC_INFO CEncoderCallbacks::StateChangeInfo = {CC_STDCALL, 
        VT_ERROR, 1, { VT_I4 } };
_ATL_FUNC_INFO CEncoderCallbacks::ArchiveStateChangeInfo= {CC_STDCALL, 
          VT_ERROR, 2, { VT_I4, VT_I4 } };
_ATL_FUNC_INFO CEncoderCallbacks::IndexerStateChangeInfo = {CC_STDCALL, 
         VT_ERROR, 2, { VT_I4, VT_BSTR } };


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CEncoderCallbacks::CEncoderCallbacks()
{
	encoderMessagesReceived = FALSE;
	initHasOccurred = FALSE;
	shutdownHasOccurred = FALSE;
}

CEncoderCallbacks::~CEncoderCallbacks()
{

}

BOOL CEncoderCallbacks::getEncoderMessagesReceivedFlag()
{
	return encoderMessagesReceived;
}
void CEncoderCallbacks::setCDBVMediaEncoder(void* theClass)
{
	theEncoderClass = theClass;
}

STDMETHODIMP CEncoderCallbacks::OnError(long hResult)
{
//	MessageBox(NULL,"Error Encoding","Error",MB_ICONINFORMATION);`
	CString theSessionID;
	((CDBVMediaEncoder*)theEncoderClass)->getSessionID(theSessionID);

	CString errorMsg;
	errorMsg.Format(">>>>>>>>>>>>>>>>>>Session:%s EncoderCallbacks->OnError:%d",theSessionID, hResult);
	CXUtils::WriteToTheLogFile(errorMsg);
	return S_OK;
}
STDMETHODIMP CEncoderCallbacks::OnStateChange(WMENC_ENCODER_STATE enumState)
{
	CString theSessionID;
	((CDBVMediaEncoder*)theEncoderClass)->getSessionID(theSessionID);
	CString errorMsg;
	CString stateString;

	switch ( enumState )
    {
    case WMENC_ENCODER_STARTING:
		stateString = "WMENC_ENCODER_STARTING";
        break;
    case WMENC_ENCODER_RUNNING:
		stateString = "WMENC_ENCODER_RUNNING";
        break;
    case WMENC_ENCODER_PAUSED:
		stateString = "WMENC_ENCODER_PAUSED";
        break;
    case WMENC_ENCODER_STOPPING:
		stateString = "WMENC_ENCODER_STOPPING";
        break;
    case WMENC_ENCODER_STOPPED:
		stateString = "WMENC_ENCODER_STOPPED";
        break;
    default:
		stateString.Format("%d",enumState);
       break;
    }
	
	encoderMessagesReceived = TRUE;
	
	errorMsg.Format(">>>>> Session:%s; EncoderCallbacks->OnStateChange %s",
		theSessionID, stateString);
	CXUtils::WriteToTheLogFile(errorMsg);

	return S_OK;
}

STDMETHODIMP CEncoderCallbacks::OnArchiveStateChange(
                WMENC_ARCHIVE_TYPE enumArchive, 
                WMENC_ARCHIVE_STATE enumState )
{
	CString theSessionID;
	((CDBVMediaEncoder*)theEncoderClass)->getSessionID(theSessionID);
	CString errorMsg;
	CString stateString;

	switch ( enumArchive )
    {
    case WMENC_ARCHIVE_LOCAL:
        break;
    default:
        break;    
    }

	switch ( enumState )
    {
    case WMENC_ARCHIVE_RUNNING:
		stateString = "WMENC_ARCHIVE_RUNNING";
        break;

    case WMENC_ARCHIVE_PAUSED:
		stateString = "WMENC_ARCHIVE_PAUSED";
        break;
    
    case WMENC_ARCHIVE_STOPPED:
		stateString = "WMENC_ARCHIVE_STOPPED";
		((CDBVMediaEncoder*)theEncoderClass)->setArchiveCompleteFlag(true);
        break;
    
    default:
		stateString.Format("%d",enumState);
       break;
    }

	errorMsg.Format(">>>>> Session:%s; EncoderCallbacks->OnArchiveStateChange %s",
		theSessionID, stateString);
	CXUtils::WriteToTheLogFile(errorMsg);

	return S_OK;
}

STDMETHODIMP CEncoderCallbacks::OnIndexerStateChange(
                 WMENC_INDEXER_STATE enumIndexerState,
                 BSTR bstrFile )
{
	CString theSessionID;
	((CDBVMediaEncoder*)theEncoderClass)->getSessionID(theSessionID);
	CString errorMsg;
	CString stateString;

	switch ( enumIndexerState )
    {
    case WMENC_INDEXER_RUNNING:
		stateString = "WMENC_INDEXER_RUNNING";
        break;

    case WMENC_INDEXER_STOPPED:
		stateString = "WMENC_INDEXER_STOPPED";
		((CDBVMediaEncoder*)theEncoderClass)->setIndexingCompleteFlag(true);
        break;

    default:
		stateString.Format("%d",enumIndexerState);
       break;
    }

	errorMsg.Format(">>>>> Session:%s; EncoderCallbacks->OnIndexerStateChange %s, File:%s", 
		theSessionID, stateString, bstrFile);
	CXUtils::WriteToTheLogFile(errorMsg);
    return S_OK;
}

