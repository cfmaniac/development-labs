//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by alvaradomgr.rc
//
#define IDS_HELLO                       1
#define ID_REPORT_ONLY                  3
#define IDCANCEL2                       3
#define IDD_MAIN_DIALOG                 101
#define IDI_ICON1                       102
#define IDD_STARTUP_DIALOG              103
#define IDD_STOCK_UPDATE_CHOICE         105
#define IDC_MSG_LIST                    1000
#define IDC_ACTIVE_ENCODER_LIST         1001
#define IDC_BUTTON1                     1002
#define IDC_CHECK1                      1003
#define IDC_STAR                        1004
#define IDC_TEST_EMAIL                  1005
#define IDC_DUMP_CONFIG                 1006
#define IDC_UPDATE_INVOICE_STATUS       1007
#define IDC_IMPORT_ORDERS               1008
#define IDC_TEST_EMAIL2                 1009
#define IDC_SHOW_CMD_ARGS               1009
#define IDC_MESSAGE_TEXT                1011
#define IDC_FILE_LISTBOX                1015
#define IDC_CHECK2                      1016
#define IDC_ENABLE_UPLOAD               1016
#define IDC_EVENT_LIST                  1019
#define ID_DOWNLOAD_EVENTS              1020

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        106
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1021
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
