// DBVMediaEncoder.h: interface for the CDBVMediaEncoder class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DBVMEDIAENCODER_H__3F2933C1_69C2_451D_BA8D_E64A19DC51B7__INCLUDED_)
#define AFX_DBVMEDIAENCODER_H__3F2933C1_69C2_451D_BA8D_E64A19DC51B7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <windows.h>
#pragma warning(disable:4786)
#include <atlbase.h>
#include "wmencode.h"
#include "WMF_genprofile.h"
#include "WMF_util.h"
#include "Wininet.h"
#include "EncoderCallbacks.h"
#include "EncoderCommand.h"

class CDBVMediaEncoder  
{
public:
//	static void GetEncoderStatsInPairs(CDBVMediaEncoder* theEncoder, int encoderIndex, CString& theStatPairs,
//											  CEncoderCommand theEncoderCommandData);
	static void GetPluginResources();
	void CleanupEncoderInterfaces(BOOL releaseDevicesOnly = FALSE);
	BOOL DoDeviceEncoder(CString audioDeviceName, CString videoDeviceName, CString outName, CString vodProfileName);
	BOOL startTheEncoder();
	BOOL stopTheEncoder();
	BOOL isTheEncoderRunning();
	BOOL isArchiverRunning();
	BOOL isIndexerRunning();
	void waitForEncoderToStop();
	BOOL pauseTheEncoder();
	int archiveIndex;
	CString rootArchiveName;
	CString broadcastPort;
	CString speedKey;
	void setBroadCastPort(CString thePort);
	void getBroadCastPort(CString& thePort);
	void setDoArchiveFlag(BOOL flag);
	void setDoArchiveFlag(CString flag);



	CDBVMediaEncoder();
	virtual ~CDBVMediaEncoder();
	static BOOL FindNamedEncoderProfile(IWMEncoder* pEncoder,    
									  IWMEncSourceGroup* pSrcGrp, 
									  CString theName);
	CString errorMsg;
	void BuildStatusResponse(CString & theResponse);

	void setSessionID(CString newID)
	{
		sessionID = newID;
	};
	void getSessionID(CString& theID)
	{
		theID = sessionID;
	};
	void setEventGMTDate(CString newID)
	{
		eventGMTDate = newID;
	};
	void getEventGMTDate(CString& theID)
	{
		theID = eventGMTDate;
	};

	void setEventActualStartTimeGMT(CString newID)
	{
		actualEncoderStartTimeGMT = newID;
	};
	void getEventActualStartTimeGMT(CString& theID)
	{
		theID = actualEncoderStartTimeGMT;
	};

	void setEventName(CString theName)
	{
		if (theName.IsEmpty())
		{
			theName = "Name Not Provided";
		}
		eventName = theName;
	};
	void getEventName(CString& theName)
	{
		theName = eventName;
	};
	void setSpeedKey(CString theName)
	{
		if (theName.IsEmpty())
		{
			speedKey = "LOW";
		}
		eventName = theName;
	};
	void getSpeedKey(CString& theName)
	{
		theName = speedKey;
	};
	BOOL isOKtoDestroy();
	void setWaitingToDeleteFlag(BOOL flag);
	BOOL getWaitingToDeleteFlag();
	void setArchiveCompleteFlag(BOOL flag);
	void setIndexingCompleteFlag(BOOL flag);
	void setFtpUploadCompleteFlag(BOOL flag);
	BOOL isReadyForFtpUpload();
	BOOL isFtpUploadComplete();
	void setFtpUploadInProgress(BOOL flag);
	BOOL isFtpUploadInProgress();
	void getNextFtpUploadFile(CString& localFilename, CString& remoteFilename);

	CStringList theArchiveFiles;

	void setFtpThreadID(unsigned long val)
	{
		ftpThreadID = val;
	}
	unsigned long getFtpThreadID()
	{
		return ftpThreadID;
	}
	void setArchiveType(CString newType)
	{
		archiveType = newType;
	}
	CString getArchiveType()
	{
		return archiveType;
	}
	void setArchiveDirectory(CString theDir)
	{
		archiveDirectory = theDir;
	}
	CString getArchiveDirectory()
	{
		return archiveDirectory;
	}
private:
	IWMEncoder* pEncoder;
	IWMEncSourceGroupCollection* pSrcGrpColl;
	IWMEncSourceGroup* pSrcGrp;
	IWMEncSource* pAudSrc;
	IWMEncSource* pVidSrc;
	IWMEncBroadcast* pBrdcast;
	IWMEncFile* pFile;

	CString sessionID;
	BOOL archiveFlag;
	CString archiveName;
	unsigned long ftpThreadID;

	CEncoderCallbacks theCallbacks;

	BOOL waitingToDelete;
	BOOL archivingComplete;
	BOOL indexingComplete;
	BOOL ftpUploadComplete;
	BOOL ftpUploadInProgress;
	CString eventName;
	CString eventGMTDate;
	CString actualEncoderStartTimeGMT;
	CString archiveType;
	CString archiveDirectory;
	BOOL encodingStoppedReported;

};

#endif // !defined(AFX_DBVMEDIAENCODER_H__3F2933C1_69C2_451D_BA8D_E64A19DC51B7__INCLUDED_)
