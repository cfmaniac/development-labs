#pragma once

class CProcessingCommand
{
public:
	CProcessingCommand(void);
	~CProcessingCommand(void);
	CMapStringToString theCommandData;
	BOOL CommandDataLookup(CString TheKey, CString & TheValue);
};
