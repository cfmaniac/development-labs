// stock.h : Implementation of the Cstock class



// Cstock implementation

// code generated on Monday, June 30, 2003, 12:29 PM

#include "stdafx.h"
#include "stock.h"
IMPLEMENT_DYNAMIC(Cstock, CRecordset)

Cstock::Cstock(CDatabase* pdb)
	: CRecordset(pdb)
{
	m_nFields = 0;
	m_nDefaultType = dynaset;
}
//#error Security Issue: The connection string may contain a password
// The connection string below may contain plain text passwords and/or
// other sensitive information. Please remove the #error after reviewing
// the connection string for any security related issues. You may want to
// store the password in some other form or use a different user authentication.
CString Cstock::GetDefaultConnect()
{
	return _T("DSN=Visual FoxPro Tables;UID=;PWD=;SourceDB=c:\\momwin\\oc;SourceType=DBF;Exclusive=No;BackgroundFetch=Yes;Collate=Machine;Null=Yes;Deleted=Yes;");
}

CString Cstock::GetDefaultSQL()
{
	return _T(" select * from stock ");
//	return _T("[stock]");
}

void Cstock::DoFieldExchange(CFieldExchange* pFX)
{
	pFX->SetFieldType(CFieldExchange::outputColumn);
// Macros such as RFX_Text() and RFX_Int() are dependent on the
// type of the member variable, not the type of the field in the database.
// ODBC will try to automatically convert the column value to the requested type

}
/////////////////////////////////////////////////////////////////////////////
// Cstock diagnostics

#ifdef _DEBUG
void Cstock::AssertValid() const
{
	CRecordset::AssertValid();
}

void Cstock::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG


