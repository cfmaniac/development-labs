// HttpRetrieval.cpp: implementation of the HttpRetrieval class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
//#include "ThredHlp.h"
#include "HttpRetrieval.h"
#include "XUtils.h"
//#include "ManageRegistry.h"
//#include "UtilityFuncs.h"
//#include "ManageDisplay.h"
//#include "DisplayHandler.h"
#include <io.h>
#include <sys/types.h> 
#include <sys/stat.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

CString HttpRetrieval::STATIC_LOGIN_URL = "http://www.xxx.com/MP/MPCommand.dbml?NC=1";
LONG HttpRetrieval::NumThreadRequests = 0;
CCriticalSection HttpRetrieval::RequestSyncSection;

DWORD HttpRetrieval::ProxyType = INTERNET_OPEN_TYPE_PRECONFIG;
//DWORD HttpRetrieval::ProxyType = INTERNET_OPEN_TYPE_DIRECT;
CString HttpRetrieval::NonSecureProxyName = "";
CString HttpRetrieval::SecureProxyName = "";
BOOL HttpRetrieval::DoNotPromptForDataAgain = FALSE;
HINTERNET HttpRetrieval::AppInternetSession = NULL;	//	ALWAYS CLOSE, 01/20/2003, trl;
HINTERNET HttpRetrieval::FtpInternetSession = NULL;	//	ALWAYS CLOSE, 01/20/2003, trl;
CString HttpRetrieval::InternetSessionKey;
time_t HttpRetrieval::LastThreadTime = 0;
//CString HttpRetrieval::proxyUsername;
//CString HttpRetrieval::proxyPassword;

int		HttpRetrieval::GOOD_STATUS = 0;
int		HttpRetrieval::ERROR_STATUS = 1;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

HttpRetrieval::HttpRetrieval()
{
	SetStartupVars();
	InitVars();
}
void HttpRetrieval::SetStartupVars()
{
	DataArrayMutex = NULL;
	InitializeCriticalSection(&m_csRetArray);
	DataArrayMutex = new CMutex;
}
void HttpRetrieval::InitVars()
{

	int Cnt;

	EnterCriticalSection(&m_csRetArray);

	TheThreadArray.SetSize(5,5);
	for (Cnt=0; Cnt < TheThreadArray.GetSize(); Cnt++)
	{
		TheThreadArray[Cnt] = NULL;
	}

	LeaveCriticalSection(&m_csRetArray);
}

HttpRetrieval::~HttpRetrieval()
{
	CloseInternetHandle();			//	ALWAYS CLOSE, 01/20/2003, trl;
	if (DataArrayMutex != NULL)
	{
		delete DataArrayMutex;
		DataArrayMutex = NULL;
	}
	DeleteCriticalSection(&m_csRetArray); 
}

void HttpRetrieval::InsertIntoThreadArray(void* &ThePointer)
{

	EnterCriticalSection(&m_csRetArray);
	BOOL Added = false;
	int Cnt;
	for (Cnt=0; Cnt < TheThreadArray.GetSize(); Cnt++)
	{
		if (TheThreadArray[Cnt] == NULL)
		{
			TheThreadArray[Cnt] = ThePointer;
			Added = true;
			break;
		}
	}	
	if (!Added)
		TheThreadArray.Add(ThePointer);
	LeaveCriticalSection(&m_csRetArray);
}




BOOL HttpRetrieval::IsNextPartHtmlTrailer(BYTE* TheBuffer)
{
	BOOL RetVal = FALSE;

	char TheData[20];
	strncpy(TheData, (char*)TheBuffer,10);
	TheData[10] = 0;
	if (stricmp(TheData,"--></HTML>") == 0)
		RetVal = TRUE;

	return RetVal;
}

HttpRetrieval::ThreadStruct* HttpRetrieval::GetFileNonThreaded(CString Url, CString proxyUsername, 
															   CString proxyPassword, BOOL usePassiveFTP,
															   CString ftpUsername, CString ftpPassword)
{
	CString TheAppendData;
	int RetStatus = 0;

	int inSize;
	inSize = Url.GetLength();

	unsigned long bufsize = 5000;
	char OutCanonical[5000];
	BOOL canonStatus = InternetCanonicalizeUrl(Url,   
								OutCanonical,
								&bufsize, ICU_BROWSER_MODE);
	DWORD lastError;
	if (!canonStatus)
	{
		lastError = GetLastError();
	}
	Url = OutCanonical;

	CString TheRequestString;
	TheRequestString = Url;

	ThreadStruct* TheThreadStruct = NULL;
	if (TheRequestString.GetLength() > 0)
	{
		TheThreadStruct = new ThreadStruct;
		TheThreadStruct->TheThread = NULL;
		TheThreadStruct->TheData = NULL;
		TheThreadStruct->TheRequestString = TheRequestString;
		TheThreadStruct->Request_GET_POST_Type = "GET";
		TheThreadStruct->TheThread = NULL;

		TheThreadStruct->TheDataSize = 0;
		TheThreadStruct->BufferGrowSize = 100000;
		TheThreadStruct->TheTmpReadBuffer = NULL;
		TheThreadStruct->NetworkError = HttpRetrieval::GOOD_STATUS;
		TheThreadStruct->CurrentByteCount = 0;
		TheThreadStruct->LastCurrentByteCount = 0;
		TheThreadStruct->TimeoutTime = 60;
		TheThreadStruct->ErrorNumber = 0;
		TheThreadStruct->showUserPrompts = FALSE;

		TheThreadStruct->proxyUsername = proxyUsername;
		TheThreadStruct->proxyUsername = proxyPassword;
		TheThreadStruct->usePassiveFTP = usePassiveFTP;
		TheThreadStruct->ftpUsername = ftpUsername;
		TheThreadStruct->ftpPassword = ftpPassword;

		TRACE ("STARTING THREAD:GetFileNonThreaded\n");

		unsigned long ThreadHandle;
		ThreadHandle = CreateThread((LPTHREAD_START_ROUTINE)MakeRequestTh, TheThreadStruct);

		BOOL WaitComplete = FALSE;
		DWORD WaitStatus;
//		long StartSoFar = TotalDownloadedSoFar;

		while (WaitComplete == FALSE)
		{
//			TotalDownloadedSoFar = StartSoFar + TheThreadStruct->CurrentByteCount;

			WaitStatus = GetThreadCompletionStatus(TheThreadStruct);
			switch (WaitStatus)
			{
			case HttpRetrieval::THREAD_COMPLETE:
//				CloseHandle((void*)ThreadHandle);	// Removed, ThreadHelper will close the handle, trl;
				WaitComplete = TRUE;
				break;
			case HttpRetrieval::THREAD_WAITING:
			case HttpRetrieval::THREAD_KILLED:
			case HttpRetrieval::THREAD_NULL:
				SleepEx(250,true);
//				SleepEx(1000,true);
				break;
			default:
				break;
			}
		}

//		TotalDownloadedSoFar += TheThreadStruct->CurrentByteCount;
//		Sleep(5000);

		TRACE("Total Bytes Read=%d\n", TheThreadStruct->CurrentByteCount);
	}

	return TheThreadStruct;
}


int HttpRetrieval::CheckForThreadCompleteByType(BOOL &LoginFound, long ThreadType,
												CString &RequestErrMsg,
												CMapStringToString &Result,
												int& ResponseStatus)
{
	EnterCriticalSection(&m_csRetArray);
	Result.RemoveAll();

	ResponseStatus = HttpRetrieval::GOOD_STATUS;
	int NetworkError = HttpRetrieval::GOOD_STATUS;
	unsigned long ThreadHandleToClose = NULL;
	BOOL ResponseFound = FALSE;
	int Cnt;
	ThreadStruct* TheThreadStruct = NULL;
//	ThreadInfo* TheThreadInfo = NULL;
	long NumImagesLoaded = 0;
	LoginFound = false;
	ThreadManagementStruct* TheThreadManagementStruct = NULL;
	HttpRetrieval::ThreadCheckStatus WaitStatus;
	for (Cnt=0; Cnt < TheThreadArray.GetSize(); Cnt++)
	{
		if (ResponseFound)
			break;

		TheThreadManagementStruct =  (ThreadManagementStruct*)TheThreadArray.GetAt(Cnt);
		if (TheThreadManagementStruct != NULL)
		{

			TheThreadStruct = TheThreadManagementStruct->TheThreadStruct;
			WaitStatus = GetThreadCompletionStatus(TheThreadStruct);
			switch (WaitStatus)
			{
			case HttpRetrieval::THREAD_COMPLETE:
			{
				if (TheThreadStruct->TheType == HttpRetrieval::INFO_DOWNLOAD)
				{
				}
//				TheThreadInfo = TheThreadManagementStruct->TheThreadInfo;
//				ThreadHandleToClose = TheThreadManagementStruct->CreatedThreadHandle;
//				CloseHandle((void*)ThreadHandleToClose);
				CloseThreadHandle(TheThreadStruct->TheThread);
				
				if ((TheThreadStruct->TheType == FILE_DOWNLOAD) &&
					(ThreadType == FILE_DOWNLOAD))
				{
					GetFileStruct* TheGetFileStruct = (GetFileStruct*)TheThreadManagementStruct->TheDataStruct;

					LoginFound = true;
					delete TheThreadManagementStruct->TheThreadStruct;
					delete TheGetFileStruct;
					delete TheThreadManagementStruct;
					ResponseFound = TRUE;

					TheThreadArray[Cnt] = NULL;				
					break;
				}
				else if ((TheThreadStruct->TheType == FILECACHE_DOWNLOAD) &&
					(ThreadType == FILECACHE_DOWNLOAD))
				{
					LoginFound = true;
					delete TheThreadManagementStruct->TheThreadStruct;
					delete TheThreadManagementStruct;
					ResponseFound = TRUE;

					TheThreadArray[Cnt] = NULL;				
					break;
				}

				break;
			}

			case HttpRetrieval::THREAD_WAITING:
			{
				break;
			}
			case HttpRetrieval::THREAD_KILLED:
			{
				if (TheThreadStruct->TheType != ThreadType)
				{
					break;
				}

				NetworkError = HttpRetrieval::ERROR_STATUS;
				CloseThreadHandle(TheThreadStruct->TheThread);

				CleanupThreadStruct(TheThreadStruct);

				delete TheThreadManagementStruct->TheThreadStruct;
				delete TheThreadManagementStruct;
				TheThreadArray[Cnt] = NULL;
				break;
			}
			case HttpRetrieval::THREAD_NULL:
			{
				if (TheThreadStruct->TheType != ThreadType)
				{
					break;
				}

				NetworkError = HttpRetrieval::ERROR_STATUS;
				CloseThreadHandle(TheThreadStruct->TheThread);

				CleanupThreadStruct(TheThreadStruct);

				delete TheThreadManagementStruct->TheThreadStruct;
				delete TheThreadManagementStruct;
				TheThreadArray[Cnt] = NULL;
				break;
			}
			default:
				break;

			}
		}
	}
	LeaveCriticalSection(&m_csRetArray);
	return NetworkError;
}

BOOL HttpRetrieval::IsThreadTypeRunning(long ThreadType)
{
	EnterCriticalSection(&m_csRetArray);

	BOOL ResponseFound = FALSE;
	int Cnt;
	ThreadStruct* TheThreadStruct = NULL;
	ThreadManagementStruct* TheThreadManagementStruct = NULL;
	for (Cnt=0; Cnt < TheThreadArray.GetSize(); Cnt++)
	{
		if (ResponseFound)
			break;

		TheThreadManagementStruct =  (ThreadManagementStruct*)TheThreadArray.GetAt(Cnt);
		if (TheThreadManagementStruct != NULL)
		{

			TheThreadStruct = TheThreadManagementStruct->TheThreadStruct;
			if (TheThreadStruct->TheType == ThreadType)
			{
				ResponseFound = TRUE;
			}
		}
	}

	LeaveCriticalSection(&m_csRetArray);
	return ResponseFound;
}

HttpRetrieval::ThreadStruct* HttpRetrieval::CheckForThreadCompleteByType(long ThreadType)
{
	EnterCriticalSection(&m_csRetArray);

	unsigned long ThreadHandleToClose = NULL;
	BOOL ResponseFound = FALSE;
	int Cnt;
	ThreadStruct* TheThreadStruct = NULL;
	ThreadStruct* TheReturnThreadStruct = NULL;
	ThreadManagementStruct* TheThreadManagementStruct = NULL;
	HttpRetrieval::ThreadCheckStatus WaitStatus;
	for (Cnt=0; Cnt < TheThreadArray.GetSize(); Cnt++)
	{
		if (ResponseFound)
			break;

		TheThreadManagementStruct =  (ThreadManagementStruct*)TheThreadArray.GetAt(Cnt);
		if (TheThreadManagementStruct != NULL)
		{

			TheThreadStruct = TheThreadManagementStruct->TheThreadStruct;
			WaitStatus = GetThreadCompletionStatus(TheThreadStruct);
			switch (WaitStatus)
			{
			case HttpRetrieval::THREAD_COMPLETE:
			{
				if ((TheThreadStruct->TheType == ThreadType) &&
					(TheThreadStruct->TheType == HttpRetrieval::INFO_DOWNLOAD))
				{
					TheReturnThreadStruct = TheThreadStruct;
					delete TheThreadManagementStruct;
					TheThreadArray[Cnt] = NULL;
					ResponseFound = TRUE;
				}
				else if ((TheThreadStruct->TheType == ThreadType) &&
					(TheThreadStruct->TheType == HttpRetrieval::FTP_UPLOAD))
				{
					TheReturnThreadStruct = TheThreadStruct;
					delete TheThreadManagementStruct;
					TheThreadArray[Cnt] = NULL;
					ResponseFound = TRUE;
				}
				else if ((TheThreadStruct->TheType == ThreadType) &&
					(TheThreadStruct->TheType == HttpRetrieval::TKT_INFO_DOWNLOAD))
				{
					TheReturnThreadStruct = TheThreadStruct;
					delete TheThreadManagementStruct;
					TheThreadArray[Cnt] = NULL;
					ResponseFound = TRUE;
				}

				break;
			}

			case HttpRetrieval::THREAD_WAITING:
			{
				break;
			}
			case HttpRetrieval::THREAD_KILLED:
			{
//				if (TheThreadStruct->TheType != ThreadType)
//				{
//					break;
//				}

				CloseThreadHandle(TheThreadStruct->TheThread);

				CleanupThreadStruct(TheThreadStruct);

				delete TheThreadManagementStruct->TheThreadStruct;
				delete TheThreadManagementStruct;
				TheThreadArray[Cnt] = NULL;
				break;
			}
			case HttpRetrieval::THREAD_NULL:
			{
//				if (TheThreadStruct->TheType != ThreadType)
//				{
//					break;
//				}

				CloseThreadHandle(TheThreadStruct->TheThread);

				CleanupThreadStruct(TheThreadStruct);

				delete TheThreadManagementStruct->TheThreadStruct;
				delete TheThreadManagementStruct;
				TheThreadArray[Cnt] = NULL;
				break;
			}
			default:
				break;

			}
		}
	}
	LeaveCriticalSection(&m_csRetArray);
	return TheReturnThreadStruct;
}


BYTE* HttpRetrieval::ReadStreamFromBuffer(BYTE* &TheData, long NumBytes, BYTE* &TheValue,long& BytesLeft)
{
	BYTE* MemPtr;

	TheValue[0] = NULL;
	MemPtr = TheData;
	if (BytesLeft >= NumBytes)
	{
		memcpy(TheValue, TheData, NumBytes);	//read the bytes

		MemPtr = TheData + NumBytes;
		BytesLeft -= NumBytes;
	}
	else
	{
		BytesLeft = -1;
	}
	return MemPtr;
}

int HttpRetrieval::DownloadFile(CString RemoteFile, CString LocalFile, CString proxyUsername, CString proxyPassword)
{
	int RetStatus = 0;

	CString TheAppendData;
	
		ThreadStruct* TheThreadStruct = NULL;
		TheThreadStruct = new ThreadStruct;
		TheThreadStruct->TheThread = NULL;
		TheThreadStruct->TheData = NULL;
		TheThreadStruct->TheRequestString;
		TheThreadStruct->Request_GET_POST_Type = "GET";

		TheThreadStruct->TheDataSize = 0;
		TheThreadStruct->BufferGrowSize = 0;
		TheThreadStruct->TheTmpReadBuffer = NULL;
		TheThreadStruct->NetworkError = HttpRetrieval::GOOD_STATUS;
		TheThreadStruct->CurrentByteCount = 0;
		TheThreadStruct->LastCurrentByteCount = 0;
		TheThreadStruct->TheConnectionHandle = NULL;
		TheThreadStruct->TimeoutTime = -1;
		TheThreadStruct->ErrorNumber = 0;
		TheThreadStruct->showUserPrompts = FALSE;

		TheThreadStruct->TheType = FILE_DOWNLOAD;

		GetFileStruct *TheGetFileStruct;
		TheGetFileStruct = new GetFileStruct;
		TheGetFileStruct->RemoteFileUrl = RemoteFile;
		TheGetFileStruct->LocalFile = LocalFile;
		TheGetFileStruct->DoneProcessing = FALSE;
		TheGetFileStruct->proxyUsername = proxyUsername;
		TheGetFileStruct->proxyPassword = proxyPassword;


		ThreadManagementStruct* TheThreadManagementStruct;
		TheThreadManagementStruct = new ThreadManagementStruct;
		TheThreadManagementStruct->ThreadType = FILE_DOWNLOAD;
		TheThreadManagementStruct->TheDataStruct = TheGetFileStruct;
		TheThreadManagementStruct->TheThreadStruct = TheThreadStruct;
//		TheThreadManagementStruct->CreditImageID = -1;

		TheThreadManagementStruct->CreatedThreadHandle =
			HttpRetrieval::CreateThread((LPTHREAD_START_ROUTINE)DownloadFileTh, TheGetFileStruct);
		if (TheThreadManagementStruct->CreatedThreadHandle >= 0)
		{
			InsertIntoThreadArray((void*&)TheThreadManagementStruct);
			RetStatus = 1;
		}
	return RetStatus;
}

void HttpRetrieval::DownloadFileTh(void* CallingArgument)
{
	GetFileStruct *TheGetFileStruct;
	TheGetFileStruct = (GetFileStruct*) CallingArgument;
	GetRemoteFileToDisk(TheGetFileStruct->RemoteFileUrl, 
		TheGetFileStruct->LocalFile,
		TheGetFileStruct->proxyUsername,
		TheGetFileStruct->proxyPassword);
	TheGetFileStruct->DoneProcessing = TRUE;
}

HttpRetrieval::ThreadStruct* HttpRetrieval::BuildAndStartThread(
					CString TheRequestString, 
					HttpRetrieval::RetreivalType RequestType,
					long BufferGrowSize,
					CString Request_GET_POST_Type,
					CString proxyUsername,
					CString proxyPassword,
					BOOL usePassiveFTP,
					CString ftpLocalFilename,
					CString ftpRemoteFilename,
					CString ftpUsername,
					CString ftpPassword,
//					long CreditImageID,
//					long Lv1,
//					long Lv2,
//					long Lv3,
//					long Lv4,
//					const char* Cv1,
//					const char* Cv2,
//					const char* Cv3,
//					const char* Cv4,
					const char* DiskFilename
					)
{
	BOOL ThreadStarted = FALSE;
	HttpRetrieval::ThreadStruct* TheThreadStruct = NULL;
	if (TheRequestString.GetLength() > 0)
	{
		TheThreadStruct = new HttpRetrieval::ThreadStruct;
		TheThreadStruct->TheThread = NULL;
		TheThreadStruct->TheData = NULL;
		TheThreadStruct->TheRequestString = TheRequestString;
		TheThreadStruct->Request_GET_POST_Type = Request_GET_POST_Type;

		TheThreadStruct->TheDataSize = 0;
		TheThreadStruct->BufferGrowSize = BufferGrowSize;
		TheThreadStruct->TheTmpReadBuffer = NULL;
		TheThreadStruct->NetworkError = HttpRetrieval::GOOD_STATUS;
		TheThreadStruct->CurrentByteCount = 0;
		TheThreadStruct->LastCurrentByteCount = 0;
		TheThreadStruct->TheConnectionHandle = NULL;
		TheThreadStruct->TimeoutTime = 60;
		TheThreadStruct->ErrorNumber = 0;
		TheThreadStruct->showUserPrompts = FALSE;
		if (DiskFilename != NULL)
			TheThreadStruct->DiskFilename = DiskFilename;

		TheThreadStruct->TheType = RequestType;

		ThreadManagementStruct* TheThreadManagementStruct;
		TheThreadManagementStruct = new ThreadManagementStruct;
		TheThreadManagementStruct->ThreadType = RequestType;
		TheThreadManagementStruct->TheDataStruct = TheThreadStruct;
		TheThreadManagementStruct->TheThreadStruct = TheThreadStruct;
//		TheThreadManagementStruct->CreditImageID = CreditImageID;
//		TheThreadManagementStruct->LongData1 = Lv1;
//		TheThreadManagementStruct->LongData2 = Lv2;
//		TheThreadManagementStruct->LongData3 = Lv3;
//		TheThreadManagementStruct->LongData4 = Lv4;
//		TheThreadManagementStruct->CharData1[0] = NULL;
//		TheThreadManagementStruct->CharData2[0] = NULL;
//		TheThreadManagementStruct->CharData3[0] = NULL;
//		TheThreadManagementStruct->CharData4[0] = NULL;
//		if (Cv1 != NULL) strcpy(TheThreadManagementStruct->CharData1, Cv1);
//		if (Cv2 != NULL) strcpy(TheThreadManagementStruct->CharData2, Cv2);
//		if (Cv3 != NULL) strcpy(TheThreadManagementStruct->CharData3, Cv3);
//		if (Cv4 != NULL) strcpy(TheThreadManagementStruct->CharData4, Cv4);

		TheThreadStruct->proxyUsername = proxyUsername;
		TheThreadStruct->proxyUsername = proxyPassword;
		TheThreadStruct->usePassiveFTP = usePassiveFTP;
		TheThreadStruct->ftpLocalFilename = ftpLocalFilename;
		TheThreadStruct->ftpRemoteFilename = ftpRemoteFilename;
		TheThreadStruct->ftpUsername = ftpUsername;
		TheThreadStruct->ftpPassword = ftpPassword;


		TheThreadManagementStruct->CreatedThreadHandle =
			HttpRetrieval::CreateThread((LPTHREAD_START_ROUTINE)MakeRequestTh, TheThreadStruct, NULL);
		if (TheThreadManagementStruct->CreatedThreadHandle >= 0)
		{
			InsertIntoThreadArray((void*&)TheThreadManagementStruct);
			ThreadStarted = TRUE;
		}
		else
		{
			delete TheThreadManagementStruct;
			CleanupThreadStruct(TheThreadStruct);
			TheThreadStruct = NULL;
		}

	}
	return TheThreadStruct;
}

BOOL HttpRetrieval::NeedAuth (HINTERNET hRequest)
{
    // Get status code.
    DWORD dwStatus;
    DWORD cbStatus = sizeof(dwStatus);
    HttpQueryInfo
    (
        hRequest,
        HTTP_QUERY_FLAG_NUMBER | HTTP_QUERY_STATUS_CODE,
        &dwStatus,
        &cbStatus,
        NULL
    );

	CString Msg;
#if defined(_DEBUG)
	Msg.Format("NeedAuth Called; Status = %ld", dwStatus);
	CXUtils::WriteToTheLogFile(Msg, true);
#endif

    // Look for 401 or 407.
    DWORD dwFlags;
    switch (dwStatus)
    {
        case HTTP_STATUS_DENIED:
            dwFlags = HTTP_QUERY_WWW_AUTHENTICATE;
            break;
        case HTTP_STATUS_PROXY_AUTH_REQ:
            dwFlags = HTTP_QUERY_PROXY_AUTHENTICATE;
            break;            
        default:
            return FALSE;
    }

#if !defined(_DEBUG)
	Msg.Format("NeedAuth Called; Status = %ld", dwStatus);
	CXUtils::WriteToTheLogFile(Msg, true);
#endif

    // Enumerate the authentication types.
    BOOL fRet;
    char szScheme[64];
    DWORD dwIndex = 0;
    do
    {
        DWORD cbScheme = sizeof(szScheme);
        fRet = HttpQueryInfo
            (hRequest, dwFlags, szScheme, &cbScheme, &dwIndex);
        if (fRet)
            fprintf (stderr, "Found auth scheme: %s\n", szScheme);
    }
        while (fRet);

    return TRUE;
}

DWORD HttpRetrieval::DoCustomUI (HINTERNET hConnect, HINTERNET hRequest, long& GetLastErrorValue )
{
	DWORD RetValue = 0;
	DWORD dwErr;
	GetLastErrorValue = GetLastError(),                         

	dwErr = InternetErrorDlg(
				GetDesktopWindow(),                     
				hRequest,                               
				GetLastErrorValue,                         
					FLAGS_ERROR_UI_FILTER_FOR_ERRORS  |     
					FLAGS_ERROR_UI_FLAGS_CHANGE_OPTIONS |
					FLAGS_ERROR_UI_FLAGS_GENERATE_DATA, 
				NULL);

	if ((dwErr == ERROR_INTERNET_FORCE_RETRY) && (DoNotPromptForDataAgain == FALSE))
	{
		RetValue = ERROR_INTERNET_FORCE_RETRY;
	}
	else
	{
		DoNotPromptForDataAgain = TRUE;
		MessageBox(
			GetDesktopWindow(), 
			"Note: You will not be prompted for information again until the toolbar is restarted.", 
			"Operation Cancelled", 
			MB_OK);
	}
	return RetValue;
}

BOOL HttpRetrieval::OpenInternetConnection(
							HttpRetrieval::ThreadStruct* TheThreadStruct,
//							InternetRequestStruct& theInternetRequestStruct,
//							CString& MajorErrorMsg,
//							int& NetworkError,
//							long& ErrorNumber,
//							CString& TheActualURL,
							long& dwStatusCode,
							long& GetLastErrorValue,
							BOOL ShowUserPrompts,
							long ConnectionTimeoutValue)
{
	SetLastError(0);

	BOOL ContinueStatus = TRUE;
	BOOL ExceptionError = FALSE;
	BOOL UsingFTP = FALSE;

	CString strHeaders;
	CString strFormData;
	CString strTarget;
	DWORD dwServiceType;
	CString strServer;
	CString strObject;
	INTERNET_PORT nPort;
	DWORD dwLength = sizeof(DWORD);
	TheThreadStruct->theInternetRequestStruct.StatusError.Empty();
	TheThreadStruct->TheErrorMsg.Empty();
//	MajorErrorMsg.Empty();
	SetLastError(0);
	TheThreadStruct->NetworkError = HttpRetrieval::GOOD_STATUS;
//	NetworkError = HttpRetrieval::GOOD_STATUS;
	GetLastErrorValue = 0;
	TheThreadStruct->ErrorNumber = 0;
//	ErrorNumber = 0;
	dwStatusCode = -1;
	char URI[_MAX_PATH];
	URI[0] = 0;
	DWORD URILength= _MAX_PATH;

	TheThreadStruct->theInternetRequestStruct.InternetSession = NULL;
	TheThreadStruct->theInternetRequestStruct.InternetConnection = NULL;
	TheThreadStruct->theInternetRequestStruct.InternetRequest = NULL;

	DWORD ContextNumber;

	long SleepTime = 0;
//*********************************************************************
//	LOCK THIS SECTION SO IT IS SINGLE THREADED
//*********************************************************************
	BOOL SectionLocked = FALSE;
	RequestSyncSection.Lock();
	SectionLocked = TRUE;

	TRACE("HTTP RETRIEVAL: Start Time:%ld\n",time(NULL));

	NumThreadRequests++;

	if (NumThreadRequests > 100000)
		NumThreadRequests  = 1;

	time_t CurTime;
	time(&CurTime);

	if (CurTime > (HttpRetrieval::LastThreadTime))
	{
		HttpRetrieval::LastThreadTime = CurTime;
		SleepTime = 0;
	}
	else if (CurTime == HttpRetrieval::LastThreadTime)
	{
		HttpRetrieval::LastThreadTime = CurTime + 1;
		SleepTime =	1;
	}
	else
	{
		long OffTime = HttpRetrieval::LastThreadTime - CurTime;
		HttpRetrieval::LastThreadTime += OffTime + 1;
		SleepTime =	1 * (OffTime+1);
	}

	if (SleepTime != 0)
	{
		SleepEx((DWORD)(1000*SleepTime),FALSE);
	}
	ContextNumber = HttpRetrieval::LastThreadTime;
	CString ErrMsg;
	CXUtils::WriteToTheLogFile("***********************STARTING RETRIEVAL", true);
	CXUtils::WriteToTheLogFile(TheThreadStruct->TheRequestString, true);

	HINTERNET sessionToUse;
	if ((TheThreadStruct->Request_GET_POST_Type == "FTP_GET") ||
		(TheThreadStruct->Request_GET_POST_Type == "FTP_PUT"))
	{
		sessionToUse = HttpRetrieval::FtpInternetSession;
	}
	else
	{
		sessionToUse = HttpRetrieval::AppInternetSession;
	}
	int NumAttemptsAllowed = 2;

//*********************************************************************
//	SETUP EXCEPTION TRY
//*********************************************************************
	try
	{
//*********************************************************************
//		PARSE THE URL
//*********************************************************************
		CXUtils::SplitCStringOnChar(
				TheThreadStruct->TheRequestString, //TheThreadStruct->TheRequestString, 
				'?', strTarget, strFormData);
		AfxParseURL( strTarget, dwServiceType, strServer, strObject, nPort);

//*********************************************************************
//		Load Proxy Info If Any
//*********************************************************************
		CString inProxyName;
		DWORD inProxyType;
		GetProxyVals(nPort, inProxyName, inProxyType);

//*********************************************************************
//		Generate the ServiceName/User Agent for the request
//*********************************************************************
		char svcName[_MAX_PATH];
		CString TmpSessionKey;
			
		CXUtils::GetEntireKeyValue(HKEY_CURRENT_USER, 
			"Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings",
			"User Agent", svcName, "DBVUserAgent");
		
		TmpSessionKey.Format("%ld;%s",inProxyType,inProxyName);

		while (NumAttemptsAllowed > 0)
		{
			NumAttemptsAllowed--;
			ContinueStatus = TRUE;

//*********************************************************************
//			IF (NO VALID INTERNET SESSION HANDLE) THEN
//*********************************************************************

//			if (TmpSessionKey != InternetSessionKey || HttpRetrieval::AppInternetSession == NULL) //	ALWAYS CLOSE, 01/20/2003, trl;
			if (sessionToUse == NULL)
			{
				//********************************************************************************
				ErrMsg.Format("%s::Opening a new Connection",TmpSessionKey);
				CXUtils::WriteToTheLogFile(ErrMsg, true);
				//********************************************************************************
				
//*********************************************************************
//				OPEN THE INTERNET SESSION HANDLE
//*********************************************************************
				InternetSessionKey.Empty();
				TheThreadStruct->theInternetRequestStruct.InternetSession = NULL;
//				AppInternetSession = NULL;								//	ALWAYS CLOSE, 01/20/2003, trl;
				if (inProxyType == INTERNET_OPEN_TYPE_PROXY)
				{
					sessionToUse = InternetOpen(
//					HttpRetrieval::AppInternetSession = InternetOpen(	//	ALWAYS CLOSE, 01/20/2003, trl;
//					TheThreadStruct->theInternetRequestStruct.InternetSession = InternetOpen(	//	ALWAYS CLOSE, 01/20/2003, trl;
						svcName,
						inProxyType, 
						inProxyName, NULL, NULL);
				}
				else
				{
//					HttpRetrieval::AppInternetSession = InternetOpen(	//	ALWAYS CLOSE, 01/20/2003, trl;
//					TheThreadStruct->theInternetRequestStruct.InternetSession = InternetOpen(	//	ALWAYS CLOSE, 01/20/2003, trl;
					sessionToUse = InternetOpen(
						svcName,
						inProxyType, 
						NULL, NULL, NULL);
				}
				
//				TRACE("InternetOpen Executed::New SessionID = %ld\n", HttpRetrieval::AppInternetSession);	//	ALWAYS CLOSE, 01/20/2003, trl;
				
//*********************************************************************
//				IF INTERNET SESSION OPEN FAILED GET ERROR INFO
//*********************************************************************
//				if (HttpRetrieval::AppInternetSession == NULL)		//	ALWAYS CLOSE, 01/20/2003, trl;
//				if (TheThreadStruct->theInternetRequestStruct.InternetSession == NULL)		//	ALWAYS CLOSE, 01/20/2003, trl;
				if (sessionToUse == NULL)
				{
					GetLastErrorValue = GetLastError ();   
					//********************************************************************************
//					ErrMsg.Format("InternetOpen Failed:: AppInternetSession IS NULL: %ld", GetLastErrorValue);   //	ALWAYS CLOSE, 01/20/2003, trl;
				
					CString LastInternetErrorMsg;
					HttpRetrieval::GetInternetLastErrorString(GetLastErrorValue, LastInternetErrorMsg);
				
					ErrMsg.Format("InternetOpen FAILED: InternetSession IS NULL: %s [%ld]", LastInternetErrorMsg, GetLastErrorValue);
					if (NumAttemptsAllowed > 0)
					{
						ErrMsg += ": DOING RETRY";
					}

					CXUtils::WriteToTheLogFile(ErrMsg);
					//********************************************************************************
					ContinueStatus = FALSE;
					TheThreadStruct->theInternetRequestStruct.StatusError = ErrMsg;
				}
//*********************************************************************
//				ELSE SAVE THE CURRENT KEY
//*********************************************************************
				else
				{
					InternetSessionKey = TmpSessionKey;
				}
			}
//*********************************************************************
//			ELSE USE THE CURRENT INTERNET SESSION
//*********************************************************************
//			else					//	ALWAYS CLOSE, 01/20/2003, trl;
//			{						//	ALWAYS CLOSE, 01/20/2003, trl;
//			}						//	ALWAYS CLOSE, 01/20/2003, trl;
				
//*********************************************************************
//			IF STATUS IS GOOD, OPEN A CONNECTION TO THE SITE
//*********************************************************************
			if (ContinueStatus)
			{
				DWORD theServiceToUse = INTERNET_SERVICE_HTTP;
				DWORD theFlags = 0;
				INTERNET_PORT thePortToUse = INTERNET_INVALID_PORT_NUMBER;
				const char* theFtpUsername = NULL;
				const char* theFtpPassword = NULL;

				if (nPort == INTERNET_DEFAULT_HTTPS_PORT)
				{
					thePortToUse = INTERNET_DEFAULT_HTTPS_PORT;
				}
				if ((TheThreadStruct->Request_GET_POST_Type == "FTP_GET") ||
					(TheThreadStruct->Request_GET_POST_Type == "FTP_PUT"))
				{
					UsingFTP = TRUE;
					theServiceToUse = INTERNET_SERVICE_FTP;
					if(TheThreadStruct->usePassiveFTP)
						theFlags = INTERNET_FLAG_PASSIVE;
					theFtpUsername = TheThreadStruct->ftpUsername;
					theFtpPassword = TheThreadStruct->ftpPassword;
					
				}

				TheThreadStruct->theInternetRequestStruct.InternetConnection = InternetConnect(    
//					HttpRetrieval::AppInternetSession,				//	ALWAYS CLOSE, 01/20/2003, trl;
//					TheThreadStruct->theInternetRequestStruct.InternetSession,				//	ALWAYS CLOSE, 01/20/2003, trl;
					sessionToUse,
					strServer, thePortToUse, 
					theFtpUsername, theFtpPassword, theServiceToUse ,    
					theFlags, ContextNumber); 
//*********************************************************************
//				IF INTERNET CONNECT FAILED, REPORT ERROR
//*********************************************************************
				if (TheThreadStruct->theInternetRequestStruct.InternetConnection == NULL)
				{
					GetLastErrorValue = GetLastError ();   
//********************************************************************************
					CString LastInternetErrorMsg;
					HttpRetrieval::GetInternetLastErrorString(GetLastErrorValue, LastInternetErrorMsg);
				
					ErrMsg.Format("InternetConnect FAILED: %s [%ld]", LastInternetErrorMsg, GetLastErrorValue);
					CXUtils::WriteToTheLogFile(ErrMsg);
//********************************************************************************
					ContinueStatus = FALSE;
					TheThreadStruct->theInternetRequestStruct.StatusError = ErrMsg;
				}
//*********************************************************************
//				ELSE INTERNET CONNECT WAS SUCCESSFUL
//*********************************************************************
				else
				{
//********************************************************************************
						ErrMsg.Format("InternetConnect SUCCESSFUL");
						CXUtils::WriteToTheLogFile(ErrMsg, true);
//********************************************************************************
				}
			}
			
//*********************************************************************
//			IF STATUS IS BAD
//*********************************************************************
			if (ContinueStatus == FALSE)
			{
//*********************************************************************
//				IF THE ERROR IS ONE TO RETRY, CLEAR THE FLAG AND RETRY UP TO 1 TIME
//*********************************************************************
				if (
					GetLastErrorValue == ERROR_INTERNET_INTERNAL_ERROR ||
					GetLastErrorValue == ERROR_INTERNET_INVALID_OPERATION ||
					GetLastErrorValue == ERROR_INTERNET_OPERATION_CANCELLED ||
					GetLastErrorValue == ERROR_INTERNET_INCORRECT_HANDLE_TYPE ||
					GetLastErrorValue == ERROR_INTERNET_INCORRECT_HANDLE_STATE)
				{
					TRACE("Got GetLastErrorValue:: %ld; Clearing SessionKey; Retries Left=%ld", GetLastErrorValue, NumAttemptsAllowed);
					InternetSessionKey.Empty();
				}
			}
//*********************************************************************
//			ELSE SET FLAG TO EXIT THE LOOP, STATUS IS GOOD
//*********************************************************************
			else
			{
				NumAttemptsAllowed = 0;		// Exit Loop
			}
		}

//*********************************************************************
//		REMOVE SINGLE THREADED LOCK
//*********************************************************************
		RequestSyncSection.Unlock();
		SectionLocked = FALSE;

		CString RequestVerb;
		CString SendObject;
/*
		if (ContinueStatus && ConnectionTimeoutValue > 0)
		{
			DWORD dwFlags = ConnectionTimeoutValue;      
			DWORD dwBuffLen = sizeof(dwFlags);

			
			ContinueStatus = InternetSetOption (
				TheThreadStruct->theInternetRequestStruct.InternetConnection, 
				INTERNET_OPTION_CONNECT_TIMEOUT,
                &dwFlags, sizeof (dwFlags) );
			if (ContinueStatus == NULL)
			{
				GetLastErrorValue = GetLastError ();   
//********************************************************************************
				ErrMsg.Format("InternetSetOption Failed For InternetConnection: %ld", GetLastErrorValue);
				CXUtils::WriteToTheLogFile(ErrMsg);
//********************************************************************************
				ContinueStatus = FALSE;
				TheThreadStruct->theInternetRequestStruct.StatusError = ErrMsg;
			}
		}
*/

		if (UsingFTP == FALSE)
		{
//*********************************************************************
//		IF STATUS IS GOOD, BUILD THE REQUEST HEADER DATA
//*********************************************************************
		if (ContinueStatus)
		{
		
			if (TheThreadStruct->Request_GET_POST_Type == "GET")
			{
				RequestVerb = "GET";
				if (strFormData.GetLength() > 0)
					SendObject = strObject + "?" + strFormData;
				else
					SendObject = strObject;
				strFormData.Empty();
			}
			else if (TheThreadStruct->Request_GET_POST_Type == "POST")
			{
				RequestVerb = "POST";
				SendObject = strObject;
				strHeaders = _T("Content-Type: application/x-www-form-urlencoded");
				// URL-encoded form variables -
				// name = "John Doe", userid = "hithere", other = "P&Q"
			};
		}

//*********************************************************************
//		IF STATUS IS GOOD, OPEN THE REQUEST FOR DATA
//*********************************************************************
		if (ContinueStatus)
		{
			DWORD dwFlags;
			dwFlags =	INTERNET_FLAG_RELOAD | 
						INTERNET_FLAG_KEEP_CONNECTION |
						INTERNET_FLAG_DONT_CACHE |
						INTERNET_FLAG_IGNORE_CERT_CN_INVALID |
						INTERNET_FLAG_IGNORE_CERT_DATE_INVALID | 
						INTERNET_FLAG_PRAGMA_NOCACHE |
						INTERNET_FLAG_NO_CACHE_WRITE;
			if (nPort == INTERNET_DEFAULT_HTTPS_PORT)
				dwFlags |= INTERNET_FLAG_SECURE;

			TheThreadStruct->theInternetRequestStruct.InternetRequest = HttpOpenRequest(
								TheThreadStruct->theInternetRequestStruct.InternetConnection,
								RequestVerb, SendObject,
//								HTTP_VERSION, NULL, NULL,	
								NULL, NULL, NULL,	
								dwFlags,
								0);
//*********************************************************************
//			IF THE OPEN REQUEST FAILED THEN SETUP ERROR MESSAGES
//*********************************************************************
			if (TheThreadStruct->theInternetRequestStruct.InternetRequest == NULL)
			{
				GetLastErrorValue = GetLastError ();   
//********************************************************************************
				CString LastInternetErrorMsg;
				HttpRetrieval::GetInternetLastErrorString(GetLastErrorValue, LastInternetErrorMsg);
				
				ErrMsg.Format("HttpOpenRequest FAILED: %s [%ld]", LastInternetErrorMsg, GetLastErrorValue);
				CXUtils::WriteToTheLogFile(ErrMsg);
//********************************************************************************
				ContinueStatus = FALSE;
				TheThreadStruct->theInternetRequestStruct.StatusError = ErrMsg;
			}
		}

//*********************************************************************
//		IF STATUS IS GOOD, AND TIMEOUT VALUE IS VALID, CHANGE THE TIMEOUT VALUE FOR THE CONNECTION
//*********************************************************************

//		This codes sets the RECEIVE TIMEMOUT to infinite.  
//		This change was made on 10/7/06, due to release of IE7,
//			which reduced the wait time of a server response to 30 seconds., trl;

		DWORD dwFlags1 = 0;      
		DWORD dwBuffLen1 = sizeof(dwFlags1);

		InternetSetOption (TheThreadStruct->theInternetRequestStruct.InternetRequest, 
			INTERNET_OPTION_RECEIVE_TIMEOUT,
               &dwFlags1, sizeof (dwBuffLen1) );

//		InternetQueryOption(TheThreadStruct->theInternetRequestStruct.InternetRequest,
//			INTERNET_OPTION_RECEIVE_TIMEOUT , &dwFlags1, &dwBuffLen1);
//		ErrMsg.Format("InternetQueryOption : %s [%ld]", "LastInternetErrorMsg", dwFlags1);
//			CXUtils::WriteToTheLogFile(ErrMsg);

		if (ContinueStatus && ConnectionTimeoutValue > 0)
		{
			DWORD dwFlags = ConnectionTimeoutValue;      
			DWORD dwBuffLen = sizeof(dwFlags);

			
			ContinueStatus = InternetSetOption (
				TheThreadStruct->theInternetRequestStruct.InternetRequest, 
				INTERNET_OPTION_CONNECT_TIMEOUT,
                &dwFlags, sizeof (dwFlags) );
			if (ContinueStatus == NULL)
			{
				GetLastErrorValue = GetLastError ();   
//********************************************************************************
				CString LastInternetErrorMsg;
				HttpRetrieval::GetInternetLastErrorString(GetLastErrorValue, LastInternetErrorMsg);
				
				ErrMsg.Format("InternetSetOption FAILED: %s [%ld]", LastInternetErrorMsg, GetLastErrorValue);
				CXUtils::WriteToTheLogFile(ErrMsg);
//********************************************************************************
				ContinueStatus = FALSE;
				TheThreadStruct->theInternetRequestStruct.StatusError = ErrMsg;
			}
		}

//*********************************************************************
//		WHILE STATUS IS GOOD, TRY TO SEND THE REQUEST
//*********************************************************************
		long TryCount = 2;
		while ((TryCount > 0) && ContinueStatus)
		{
			TryCount--;
			ContinueStatus = HttpSendRequest(
								TheThreadStruct->theInternetRequestStruct.InternetRequest,
								strHeaders,
								strHeaders.GetLength(),
								(void*)(const char*)strFormData,
								strFormData.GetLength());

//*********************************************************************
//			IF THE REQUEST FAILED, GET THE ERROR CODE
//*********************************************************************
			if (ContinueStatus == FALSE)
			{
				GetLastErrorValue = GetLastError();   
//********************************************************************************
				CString LastInternetErrorMsg;
				HttpRetrieval::GetInternetLastErrorString(GetLastErrorValue, LastInternetErrorMsg);
				
				ErrMsg.Format("HttpSendRequest FAILED: %s [%ld]", LastInternetErrorMsg, GetLastErrorValue);
				if (TryCount > 0)
				{
					ErrMsg += ": DOING RETRY";
				}
				CXUtils::WriteToTheLogFile(ErrMsg);
//********************************************************************************

//*********************************************************************
//				IF THE ERROR INDICATES AN INVALID CERTIFICATE, SET FLAGS TO IGNORE IT
//*********************************************************************
				if (GetLastErrorValue == ERROR_INTERNET_INVALID_CA)   
				{
					ContinueStatus = TRUE;
					DWORD dwFlags;      
					DWORD dwBuffLen = sizeof(dwFlags);

					InternetQueryOption (
						TheThreadStruct->theInternetRequestStruct.InternetRequest, 
						INTERNET_OPTION_SECURITY_FLAGS,
						(LPVOID)&dwFlags, &dwBuffLen);
					dwFlags |= 256;
					InternetSetOption (
						TheThreadStruct->theInternetRequestStruct.InternetRequest, 
						INTERNET_OPTION_SECURITY_FLAGS,
                        &dwFlags, sizeof (dwFlags) );

					SetLastError(0);
				}
//*********************************************************************
//				ELSE SET FLAG TO END LOOP BECAUSE OF ERROR
//*********************************************************************
				else
				{
					TryCount = 0;
				}
			}
//*********************************************************************
//			ELSE THE REQUEST WAS SENT SUCCESSFULLY
//*********************************************************************
			else
			{
//*********************************************************************
//				IF SHOW PROMPTS IS SET AND PROXY AUTHENTICATION IS NEEDED THEN ASK USER FOR PROXY DATA
//*********************************************************************
				if (NeedAuth(TheThreadStruct->theInternetRequestStruct.InternetRequest))
				{
					if (ShowUserPrompts)
				    {
						DWORD ActionCode;
//*********************************************************************
//						IF THE USER DOES NOT WANT TO RETRY, EXIT WITH ERROR
//*********************************************************************
						ActionCode = DoCustomUI (TheThreadStruct->theInternetRequestStruct.InternetConnection, 
							TheThreadStruct->theInternetRequestStruct.InternetRequest, GetLastErrorValue);
						if (ActionCode != ERROR_INTERNET_FORCE_RETRY)
						{
							ContinueStatus = FALSE;
							TryCount = 0;
						}
					}
					else if (TheThreadStruct->proxyUsername.GetLength() > 0)
					{
					   InternetSetOption (TheThreadStruct->theInternetRequestStruct.InternetConnection, INTERNET_OPTION_PROXY_USERNAME,
							TheThreadStruct->proxyUsername.GetBuffer(TheThreadStruct->proxyUsername.GetLength()), 
							TheThreadStruct->proxyUsername.GetLength());
					   InternetSetOption (TheThreadStruct->theInternetRequestStruct.InternetConnection, INTERNET_OPTION_PROXY_PASSWORD,
							TheThreadStruct->proxyPassword.GetBuffer(TheThreadStruct->proxyPassword.GetLength()), 
							TheThreadStruct->proxyPassword.GetLength());
					   TheThreadStruct->proxyUsername.ReleaseBuffer();
					   TheThreadStruct->proxyPassword.ReleaseBuffer();
					}
					else
					{
						TryCount = 0;
					}
				}
//*********************************************************************
//				ELSE SET FLAG TO CONTINUE
//*********************************************************************
				else
				{
					TryCount = 0;
				}
			}
		}
	
//*********************************************************************
//		IF AN INTERNETREQUEST WAS CREATED, GET THE HTTP STATUS CODE
//*********************************************************************
		BOOL TmpStatus;
		if (TheThreadStruct->theInternetRequestStruct.InternetRequest != NULL)
		{
			TmpStatus = HttpQueryInfo(
							TheThreadStruct->theInternetRequestStruct.InternetRequest,
							HTTP_QUERY_STATUS_CODE | HTTP_QUERY_FLAG_NUMBER,
							(void*) &dwStatusCode, &dwLength, NULL);
		}
//*********************************************************************
//		IF STATUS IS GOOD, USE THE STATUS FROM THE REQUEST STATUS QUERY
//*********************************************************************
		if (ContinueStatus)
		{
			ContinueStatus = TmpStatus;
		}
		if (ContinueStatus)
		{
			char optionURL[_MAX_PATH];
			DWORD optionURLLength = _MAX_PATH;

			BOOL Stat = InternetQueryOption (
				TheThreadStruct->theInternetRequestStruct.InternetRequest, 
				INTERNET_OPTION_URL,
				(LPVOID)optionURL, &optionURLLength);
			
			if (Stat)
				TheThreadStruct->TheActualURL = optionURL;
		}
	}
	}

//*********************************************************************
//	SETUP EXCEPTION CATCH AND BUILD ERROR MESSAGE
//*********************************************************************
	catch (CException* pEx)
	{
		TheThreadStruct->TheErrorMsg = ReportExceptionError(ExceptionError, ContinueStatus,
			pEx, dwStatusCode);
	}
//*********************************************************************
//	IF THE SECTION IS STILL LOCKED, UNLOCK IT
//*********************************************************************
	if (SectionLocked)
		RequestSyncSection.Unlock();

	if ((TheThreadStruct->Request_GET_POST_Type == "FTP_GET") ||
		(TheThreadStruct->Request_GET_POST_Type == "FTP_PUT"))
	{
		if (sessionToUse != NULL)
		{
			if (HttpRetrieval::FtpInternetSession != sessionToUse)
				HttpRetrieval::FtpInternetSession = sessionToUse;
		}
	}
	else
	{
		if (sessionToUse != NULL)
		{
			if (HttpRetrieval::AppInternetSession != sessionToUse)
				HttpRetrieval::AppInternetSession = sessionToUse;
		}
	}

//*********************************************************************
//	IF STATUS IS MARKED FAILED BUILD THE ERROR DATA
//*********************************************************************
	if (ContinueStatus == FALSE)
	{
		if (GetLastErrorValue == 0)
		{
			GetLastErrorValue = GetLastError();
		}
//		long ErrorNumber;
//		long NetworkError;
//		CString MajorErrorMsg;

		TheThreadStruct->TheErrorMsg = HttpRetrieval::ReportNormalError(TheThreadStruct->ErrorNumber, 
			TheThreadStruct->theInternetRequestStruct.StatusError,
			TheThreadStruct->NetworkError, dwStatusCode, TheThreadStruct->TheRequestString, GetLastErrorValue);
	}

//*********************************************************************
//	IF STATUS IS MARKED FAILED CLOSE THE HANDLES
//*********************************************************************
	if (ContinueStatus == FALSE)
	{
		if (TheThreadStruct->theInternetRequestStruct.InternetRequest != NULL)
		{
			InternetCloseHandle(TheThreadStruct->theInternetRequestStruct.InternetRequest);
			TheThreadStruct->theInternetRequestStruct.InternetRequest = NULL;
		}

		if (TheThreadStruct->theInternetRequestStruct.InternetConnection != NULL)
		{
			InternetCloseHandle(TheThreadStruct->theInternetRequestStruct.InternetConnection);
			TheThreadStruct->theInternetRequestStruct.InternetConnection = NULL;
		}
		if (TheThreadStruct->theInternetRequestStruct.InternetSession != NULL)
		{
			InternetCloseHandle(TheThreadStruct->theInternetRequestStruct.InternetSession);
			TheThreadStruct->theInternetRequestStruct.InternetSession = NULL;
		}
	}

	CXUtils::WriteToTheLogFile("***********************ENDING RETRIEVAL", true);

	return ContinueStatus;
}



CString HttpRetrieval::ReportExceptionError(BOOL& ExceptionError, BOOL& ContinueStatus,
			CException* pEx, DWORD dwStatusCode)
{
	CString MajorErrorMsg;

	ExceptionError = TRUE;
	ContinueStatus = FALSE;

	TCHAR szErr[1024];
	pEx->GetErrorMessage(szErr, 1024);
	MajorErrorMsg = szErr;
	pEx->Delete();
	MajorErrorMsg.Format("HttpRetrieval Thread General Exception:%s , ret=%d\n",szErr, dwStatusCode);
	TRACE(MajorErrorMsg);
	CXUtils::WriteToTheLogFile(MajorErrorMsg);

	return MajorErrorMsg;
}

CString HttpRetrieval::ReportNormalError(long& ErrorNumber, const char* StatusError,
			int& NetworkError, DWORD dwStatusCode, const char* InUrl, long GetLastErrorValue)
{
	CString MajorErrorMsg;

//	DWORD GetLastErrorValue;
//	GetLastErrorValue = GetLastError();
	if (ErrorNumber == 0)
		ErrorNumber = -1;

//	TRACE("Status Error = %s\n",StatusError);
	NetworkError = HttpRetrieval::ERROR_STATUS;

	CString LastErrorMsg;
	HttpRetrieval::GetInternetLastErrorString(GetLastErrorValue, LastErrorMsg);

	CString Msg;
	Msg.Format("Thread Continue Status = FALSE:GetLastError=%d; LastErrorString:%s; HttpStatusCode=%d",
		GetLastErrorValue,
		LastErrorMsg,
		dwStatusCode);
	TRACE(Msg);
	TRACE("\n");
	MajorErrorMsg += Msg + ":" + InUrl + ":" + StatusError;
	CXUtils::WriteToTheLogFile(MajorErrorMsg);

	return MajorErrorMsg;
}

void HttpRetrieval::MakeRequestTh(void* CallingArgument)
{
	HttpRetrieval::ThreadStruct* TheThreadStruct;
	TheThreadStruct = (HttpRetrieval::ThreadStruct*)CallingArgument;
	TheThreadStruct->TheTmpReadBuffer = NULL;
	TheThreadStruct->TheConnectionHandle = NULL;
	TheThreadStruct->TheDataSize = 0;

//	TheThreadStruct->TheRequestString = "http://192.168.2.1";
//	TheThreadStruct->TheRequestString = "http://mmm323.adsfasdefh.asdfhahe";

#if defined(_DEBUG)
	TRACE("START::Thread Sending Msg: %s\n", TheThreadStruct->TheRequestString.Left(60));
#endif

	BOOL ContinueStatus = TRUE;
	BOOL ExceptionError = FALSE;

	long dwStatusCode = -1;
	long GetLastErrorValue = 0;
	DWORD dwLength = sizeof(DWORD);

//	CString StatusError;
//	HINTERNET InternetSession = NULL;
//	HINTERNET InternetConnection = NULL;
//	HINTERNET InternetRequest = NULL;

//	InternetRequestStruct theInternetRequestStruct;
	TheThreadStruct->theInternetRequestStruct.InternetSession = NULL;
	TheThreadStruct->theInternetRequestStruct.InternetConnection = NULL;
	TheThreadStruct->theInternetRequestStruct.InternetRequest = NULL;

//	CString StatusError;
//			HINTERNET InternetSession;
//			HINTERNET InternetConnection;
//			HINTERNET InternetRequest;


	BOOL ShowUserPrompts = TheThreadStruct->showUserPrompts;

//*********************************************************************
//	OPEN THE CONNECTION
//*********************************************************************

	ContinueStatus = HttpRetrieval::OpenInternetConnection(
							TheThreadStruct,
//							theInternetRequestStruct,
//							TheThreadStruct->TheErrorMsg,
//							TheThreadStruct->NetworkError,
//							TheThreadStruct->ErrorNumber,
//							TheThreadStruct->TheActualURL,
							dwStatusCode,
							GetLastErrorValue,
							ShowUserPrompts);

//*********************************************************************
//	SAVE THE HANDLE SO IT CAN BE CLOSED BY THE THREAD IF NEEDED
//*********************************************************************
	TheThreadStruct->TheConnectionHandle = TheThreadStruct->theInternetRequestStruct.InternetConnection;

//*********************************************************************
//	IF THE OPEN WAS SUCCESSFUL
//*********************************************************************
	if (ContinueStatus)
	{
//*********************************************************************
//		SETUP TRY FOR EXCEPTION HANDLING
//*********************************************************************
		try
		{
			if (TheThreadStruct->Request_GET_POST_Type == "FTP_PUT")
			{

				HINTERNET hFileHandle = FtpOpenFile (TheThreadStruct->TheConnectionHandle,
                                         TheThreadStruct->ftpRemoteFilename,
                                         GENERIC_WRITE,
                                         FTP_TRANSFER_TYPE_UNKNOWN | INTERNET_FLAG_NO_CACHE_WRITE,
                                         0UL);
				if (hFileHandle)
				{
					CFile inFile;
					if (inFile.Open(TheThreadStruct->ftpLocalFilename, CFile::modeRead | CFile::shareDenyWrite))
					{
					    BYTE buffer[16384];
						DWORD dwRead;
						DWORD dwBytesWritten = 0UL;
						BOOL bOK;
						do
					    {
							dwRead = inFile.Read(buffer, 16384);
							bOK = TRUE;
							if (dwRead > 0)
							{
								bOK = InternetWriteFile (hFileHandle,
		                              buffer,
			                          dwRead,
				                      &dwBytesWritten);
							}

							if (bOK == FALSE)
							{
								ContinueStatus = FALSE;
								long GetLastErrorValue = ::GetLastError();
								CString locMsg;
								GetInternetLastErrorString(GetLastErrorValue, locMsg);
								TheThreadStruct->theInternetRequestStruct.StatusError.Format("%s:%s:%d","Error during file write; Status Code",
									locMsg, GetLastErrorValue);
								break;
							}
							else
							{
								TheThreadStruct->CurrentByteCount += dwBytesWritten;
							}
						}
						while (dwRead > 0);

						inFile.Close();
					}
					else
					{
						ContinueStatus = FALSE;
						long GetLastErrorValue = ::GetLastError();
						CString locMsg;
						GetInternetLastErrorString(GetLastErrorValue, locMsg);
						TheThreadStruct->theInternetRequestStruct.StatusError.Format("%s:%s:%d","Unable to open FTP file; Status Code",
							locMsg, GetLastErrorValue);
					}
					InternetCloseHandle (hFileHandle);
				}
				else
				{
					ContinueStatus = FALSE;
					long GetLastErrorValue = ::GetLastError();
					CString locMsg;
					GetInternetLastErrorString(GetLastErrorValue, locMsg);
					TheThreadStruct->theInternetRequestStruct.StatusError.Format("%s:%s:%d","Got Bad FTP_PUT Status Code",
						locMsg, GetLastErrorValue);
				}
//	            if(!FtpPutFile(TheThreadStruct->TheConnectionHandle,
//					TheThreadStruct->ftpLocalFilename,TheThreadStruct->ftpRemoteFilename,
//		            FTP_TRANSFER_TYPE_BINARY,0))
//			    {
//					ContinueStatus = FALSE;
//					long GetLastErrorValue = ::GetLastError();
//					CString locMsg;
//					GetInternetLastErrorString(GetLastErrorValue, locMsg);
//					TheThreadStruct->theInternetRequestStruct.StatusError.Format("%s:%s:%d","Got Bad FTP_PUT Status Code",
//						locMsg, GetLastErrorValue);
//	            }
			}
			else if (TheThreadStruct->Request_GET_POST_Type == "FTP_GET")
			{
	            if(!FtpGetFile(TheThreadStruct->TheConnectionHandle, 
					TheThreadStruct->ftpRemoteFilename, TheThreadStruct->ftpLocalFilename, FALSE,
		            FILE_ATTRIBUTE_NORMAL, FTP_TRANSFER_TYPE_BINARY | INTERNET_FLAG_RELOAD,0))
			    {
					ContinueStatus = FALSE;
					long GetLastErrorValue = ::GetLastError();
					CString locMsg;
					GetInternetLastErrorString(GetLastErrorValue, locMsg);
					TheThreadStruct->theInternetRequestStruct.StatusError.Format("%s:%s:%d","Got Bad FTP_GET Status Code",
						locMsg, GetLastErrorValue);
	            }
			}
			else
			{
//*********************************************************************
//			IF THE HTTP STATUS CODE IS GOOD, READ THE STREAM
//*********************************************************************
			if ((dwStatusCode >= 200 && dwStatusCode <= 299))
			{
				BYTE ReadBuffer[100000];

				BYTE* tmpsz = NULL;
				unsigned long TmpBufferSize = TheThreadStruct->BufferGrowSize;
				unsigned long amtread;
				unsigned long total = 0;
				BYTE* szptr = NULL;
	
				BOOL ReadStatus = TRUE;

				CFile f;
				BOOL Continue = TRUE;
				if (TheThreadStruct->DiskFilename.IsEmpty() == FALSE)
				{
					if (f.Open(TheThreadStruct->DiskFilename, 
						CFile::modeCreate | CFile::modeWrite ) == FALSE)
					{
						ContinueStatus = FALSE;
					}
				}
				else
				{
					TheThreadStruct->TheTmpReadBuffer = new BYTE[TmpBufferSize];
					szptr = TheThreadStruct->TheTmpReadBuffer;
				}

				if (ContinueStatus)
				{
					while (ReadStatus = InternetReadFile(
										TheThreadStruct->theInternetRequestStruct.InternetRequest,
										ReadBuffer, 
										100000,
										&amtread))
					{
						if (ReadStatus && (amtread == 0))
							break;
						else if (ReadStatus == FALSE)
						{
							GetLastErrorValue = GetLastError();
							ContinueStatus = FALSE;
							break;
						}

						if (TheThreadStruct->DiskFilename.IsEmpty() == TRUE)
						{
							while ((total + amtread) > TmpBufferSize)
							{
								TmpBufferSize += TheThreadStruct->BufferGrowSize;
								tmpsz = new BYTE[TmpBufferSize];
								memcpy(tmpsz, TheThreadStruct->TheTmpReadBuffer, total);
								delete [] TheThreadStruct->TheTmpReadBuffer;
								TheThreadStruct->TheTmpReadBuffer = tmpsz;
								szptr = TheThreadStruct->TheTmpReadBuffer + total;
							}
							memcpy(szptr, ReadBuffer, amtread);
							szptr += amtread;
							total += amtread;
							TheThreadStruct->CurrentByteCount = total;
						}
						else
						{
							f.Write(ReadBuffer, amtread);
							total += amtread;
							TheThreadStruct->CurrentByteCount = total;
						}
					}

					if (TheThreadStruct->DiskFilename.IsEmpty() == FALSE)
					{
						f.Close();
						if (ContinueStatus == FALSE || total <= 0)
							CFile::Remove(TheThreadStruct->DiskFilename);
					}
					else
					{
						if (total <= 0)
						{
							delete [] TheThreadStruct->TheTmpReadBuffer;
							TheThreadStruct->TheTmpReadBuffer = NULL;
							ContinueStatus = FALSE;
						}
					}
					TheThreadStruct->TheDataSize = total;
				}
			}
			else
			{
				ContinueStatus = FALSE;
				TheThreadStruct->theInternetRequestStruct.StatusError = "Got Bad http Http Status Code";
			}
			}
		}
//*********************************************************************
//		CATCH ANY EXCEPTIONS DURING THE READ
//*********************************************************************
		catch (CException* pEx)
		{

			CString MajorErrorMsg;
			MajorErrorMsg = ReportExceptionError(ExceptionError, ContinueStatus,
				pEx, dwStatusCode);
			TheThreadStruct->TheErrorMsg = MajorErrorMsg;
		}
	}

//*********************************************************************
//	IF STATUS IS BAD, BUILD THE ERROR MESSAGE
//*********************************************************************
	if (ContinueStatus == FALSE)
	{
		if (GetLastErrorValue == 0)
		{
			GetLastErrorValue = GetLastError();
		}

		long ErrorNumber;
		int NetworkError;
		CString MajorErrorMsg;

		MajorErrorMsg = HttpRetrieval::ReportNormalError(ErrorNumber, 
			TheThreadStruct->theInternetRequestStruct.StatusError,
			NetworkError, dwStatusCode, TheThreadStruct->TheRequestString, GetLastErrorValue);

		TheThreadStruct->NetworkError = NetworkError;
		TheThreadStruct->TheDataSize = 0;
		if (TheThreadStruct->TheTmpReadBuffer != NULL)
			delete [] TheThreadStruct->TheTmpReadBuffer;
		if (TheThreadStruct->TheErrorMsg.IsEmpty())
			TheThreadStruct->TheErrorMsg = MajorErrorMsg;

		TheThreadStruct->ErrorNumber = ErrorNumber;
	}

//*********************************************************************
//	CLOSE THE CONNECTIONS
//*********************************************************************
	if (TheThreadStruct->theInternetRequestStruct.InternetRequest != NULL)
	{
		InternetCloseHandle(TheThreadStruct->theInternetRequestStruct.InternetRequest);
		TheThreadStruct->theInternetRequestStruct.InternetRequest = NULL;
	}

	if (TheThreadStruct->theInternetRequestStruct.InternetConnection != NULL)
	{
		InternetCloseHandle(TheThreadStruct->theInternetRequestStruct.InternetConnection);
		TheThreadStruct->theInternetRequestStruct.InternetConnection = NULL;
	}


	if (TheThreadStruct->theInternetRequestStruct.InternetSession != NULL)
	{
		InternetCloseHandle(TheThreadStruct->theInternetRequestStruct.InternetSession);
		TheThreadStruct->theInternetRequestStruct.InternetSession = NULL;
	}

	TRACE("TheHttpRetrieval:: Bytes Received: %ld\n",TheThreadStruct->TheDataSize);
	TheThreadStruct->TheData = TheThreadStruct->TheTmpReadBuffer; 
	TRACE("RETRIEVAL: End Time:%ld\n",time(NULL));
#if defined(_DEBUG)
	TRACE("END::Thread Sending Msg: %s\n", TheThreadStruct->TheRequestString.Left(60));
#endif

//************************************************************************************
//		The documentation says that _endthreadex does not have to be called after a call
//		to _beginthreadex.  However, you do have to close the handle.
//
//		ThreadHelper::EndThisThread(); 
//************************************************************************************

	return;
}

void HttpRetrieval::CloseInternetHandle()						//	ALWAYS CLOSE, 01/20/2003, trl;
{																//	ALWAYS CLOSE, 01/20/2003, trl;
	if (HttpRetrieval::AppInternetSession != NULL)				//	ALWAYS CLOSE, 01/20/2003, trl;
		InternetCloseHandle(HttpRetrieval::AppInternetSession);	//	ALWAYS CLOSE, 01/20/2003, trl;
	HttpRetrieval::AppInternetSession = NULL;					//	ALWAYS CLOSE, 01/20/2003, trl;
	if (HttpRetrieval::AppInternetSession != NULL)				//	ALWAYS CLOSE, 01/20/2003, trl;
		InternetCloseHandle(HttpRetrieval::AppInternetSession);	//	ALWAYS CLOSE, 01/20/2003, trl;
	HttpRetrieval::AppInternetSession = NULL;					//	ALWAYS CLOSE, 01/20/2003, trl;
}																//	ALWAYS CLOSE, 01/20/2003, trl;

unsigned long HttpRetrieval::CreateThread(LPTHREAD_START_ROUTINE TheRoutine,	 
						 	    ThreadStruct *CallingArgument,			 
							    LPSECURITY_ATTRIBUTES lpsa)
{
	unsigned long TheThread;
	TheThread = HttpRetrieval::CreateThread(TheRoutine, (LPVOID)CallingArgument);
	CallingArgument->ThreadStartTime = CTime::GetCurrentTime();
	CallingArgument->LastByteChangeTime = CallingArgument->ThreadStartTime;
	CallingArgument->TheThread = TheThread;
	return TheThread;
}
unsigned long HttpRetrieval::CreateThread(LPTHREAD_START_ROUTINE TheRoutine,	 
						 	    LPVOID CallingArgument,			 
							    LPSECURITY_ATTRIBUTES lpsa,
							    DWORD cbStack,				 
					 		    DWORD fdwCreate)
{
	unsigned long TheThread = NULL;
	unsigned ThreadID;
 	PW_THREAD_START_ROUTINE TheVoidRoutine = (PW_THREAD_START_ROUTINE)TheRoutine;

	TheThread =	_beginthreadex(lpsa,
				  			   cbStack,
							   TheVoidRoutine,
							   CallingArgument,
							   fdwCreate,
							   &ThreadID);

	return TheThread;			
}						   		

HttpRetrieval::ThreadCheckStatus HttpRetrieval::GetThreadCompletionStatus(ThreadStruct* TheThreadStruct)
{
	DWORD 	WaitStatus;
	ThreadCheckStatus ReturnStatus = THREAD_COMPLETE;					

	CTime TheCurrentTime = CTime::GetCurrentTime();
	CTimeSpan AccumTime;

	if (TheThreadStruct->CurrentByteCount != TheThreadStruct->LastCurrentByteCount)
	{
//		TRACE(	"On Http Retrieval:: Bytes Received = %d\n",
//				TheThreadStruct->CurrentByteCount);
		TheThreadStruct->LastByteChangeTime = CTime::GetCurrentTime();
		TheThreadStruct->LastCurrentByteCount = TheThreadStruct->CurrentByteCount;
	}

	AccumTime = TheCurrentTime - TheThreadStruct->LastByteChangeTime;
//	long TimeoutTime = 60;

	long TimeoutTime = TheThreadStruct->TimeoutTime;
		
	if (TheThreadStruct->TheThread != NULL)
	{		
		WaitStatus = ::WaitForSingleObject((void*)TheThreadStruct->TheThread, 0);

		if ((WaitStatus == WAIT_TIMEOUT) ||
			(WaitStatus == WAIT_FAILED))
		{
			ReturnStatus = THREAD_WAITING;

			if ((AccumTime.GetTotalSeconds() > TimeoutTime) && (TimeoutTime > 0))
			{
				if (TheThreadStruct->theInternetRequestStruct.InternetRequest != NULL)
				{
					InternetCloseHandle(TheThreadStruct->theInternetRequestStruct.InternetRequest);
					TheThreadStruct->theInternetRequestStruct.InternetRequest = NULL;
				}

				if (TheThreadStruct->theInternetRequestStruct.InternetConnection != NULL)
				{
					InternetCloseHandle(TheThreadStruct->theInternetRequestStruct.InternetConnection);
					TheThreadStruct->theInternetRequestStruct.InternetConnection = NULL;
				}
				if (TheThreadStruct->theInternetRequestStruct.InternetSession != NULL)
				{
					InternetCloseHandle(TheThreadStruct->theInternetRequestStruct.InternetSession);
					TheThreadStruct->theInternetRequestStruct.InternetSession = NULL;
				}

				// Close the handle to cause the thread to exit normally
//				if (TheThreadStruct->TheConnectionHandle != NULL)
//				{
//					InternetCloseHandle(TheThreadStruct->TheConnectionHandle);
//					TheThreadStruct->TheConnectionHandle = NULL;
//				}
			}
		}
		else
		{
			ReturnStatus = THREAD_COMPLETE;					
		}
	}
	else
		ReturnStatus = THREAD_NULL;

	return ReturnStatus;
}

void HttpRetrieval::CloseThreadHandle(unsigned long TheThread)
{
	if (TheThread != NULL)
	{
		::CloseHandle((void*)TheThread);
		TRACE("%s:%d\n", "Closing thread handle:", TheThread);
		TheThread = NULL;
	}
}
void HttpRetrieval::CleanupThreadStruct(HttpRetrieval::ThreadStruct* &TheThreadStruct)
{
	if (TheThreadStruct != NULL)
	{
//		if (TheThreadStruct->TheHttpFile != NULL)
//		{
//			TheThreadStruct->TheHttpFile->Close();
//			delete TheThreadStruct->TheHttpFile;
//		}
		CloseThreadHandle(TheThreadStruct->TheThread);

		if (TheThreadStruct->TheData != NULL)
		{
			delete [] TheThreadStruct->TheData;
			TheThreadStruct->TheData = NULL;
		}
		delete TheThreadStruct;
		TheThreadStruct = NULL;
	}
}
		 

BOOL HttpRetrieval::GetRemoteFileToDisk(
	CString RemoteUrl,
	CString LocalFilenamePath,
	CString proxyUsername,
	CString proxyPassword)
{
	char TmpFilename1[MAX_PATH];
	char TmpFilename2[MAX_PATH];
	GetTempFileName(CXUtils::TheExecutablePath,
		"PT7",
		0,
		TmpFilename1);
	::DeleteFile(TmpFilename1);	//Delete it since it is created;
	GetTempFileName(CXUtils::TheExecutablePath,
		"PT8",
		0,
		TmpFilename2);
	::DeleteFile(TmpFilename2);	//Delete it since it is created;

	BOOL ContinueStatus = TRUE;
	HttpRetrieval::ThreadStruct* TheThreadStruct;
	TheThreadStruct = HttpRetrieval::GetFileNonThreaded(RemoteUrl, proxyUsername, proxyPassword, FALSE);

	if (TheThreadStruct != NULL)
	{
		if (TheThreadStruct->NetworkError != HttpRetrieval::GOOD_STATUS)
		{
			ContinueStatus = false;
		}
		else
		{
			CString LocalLastError = TheThreadStruct->TheErrorMsg;
			if ((TheThreadStruct->TheData != NULL) && (LocalLastError.GetLength() == 0))
			{
				ContinueStatus = CXUtils::WriteFileToDisk(
						TmpFilename1, 
						TheThreadStruct->TheData, 
						TheThreadStruct->TheDataSize);
			}
			else
			{
				ContinueStatus = false;
			}
		}
	}
	else
	{
		ContinueStatus = false;
	}
	HttpRetrieval::CleanupThreadStruct(TheThreadStruct);

	if (ContinueStatus)
	{
		int MoveStatus;
		CString FileToReplace;

		BOOL FileExists = CXUtils::DoesFileExist(LocalFilenamePath);
		if (FileExists)
			MoveStatus = CXUtils::Move(LocalFilenamePath, TmpFilename2);		
		else
			MoveStatus = 0;

		if (MoveStatus == 0)
		{
			MoveStatus = CXUtils::Move(TmpFilename1, LocalFilenamePath);							
			if (MoveStatus == 0)
			{
				::DeleteFile(TmpFilename2);
			}
			else
			{
				if (FileExists)
				{
					CXUtils::Move(TmpFilename2, LocalFilenamePath);							
				}
				::DeleteFile(TmpFilename1);
				ContinueStatus = false;
			}
		}
		else
			ContinueStatus = false;

	}
	return ContinueStatus;
}
void HttpRetrieval::SetNonSecureProxyData(CString Addr, CString Port)
{
	NonSecureProxyName = Addr;
	if (Port.GetLength() > 0)
		NonSecureProxyName += ":" + Port;
}
void HttpRetrieval::SetSecureProxyData(CString Addr, CString Port)
{
	SecureProxyName = Addr;
	if (Port.GetLength() > 0)
		SecureProxyName += ":" + Port;
}

void HttpRetrieval::GetProxyVals(long nPort, CString& inProxyName, DWORD& inProxyType)
{
	inProxyType = HttpRetrieval::ProxyType;
	inProxyName.Empty();

	if (inProxyType == INTERNET_OPEN_TYPE_PROXY)
	{
		if (nPort == 443)
			inProxyName = SecureProxyName;
		else
			inProxyName = NonSecureProxyName;
	}
}

void HttpRetrieval::GetInternetLastErrorString(long GetLastErrorValue, CString &errorMsg)
{
	CString newError;
	DWORD theID;
	DWORD theLen = 1024;
	errorMsg.Empty();

	switch(GetLastErrorValue)
	{
	//Active Network Responses
		case ERROR_INTERNET_EXTENDED_ERROR:			//12003	   	
			errorMsg = "An extended error was returned from the server. This is typically a string or buffer containing a verbose error message. Call InternetGetLastResponseInfo to retrieve the error text. ";

			InternetGetLastResponseInfo(&theID, newError.GetBuffer(theLen), &theLen);
			newError.ReleaseBuffer();
			errorMsg.Format("Last Response Error: %s", newError);
			break;
		case ERROR_INTERNET_INCORRECT_USER_NAME:	//12013	   	
			"The request to connect and log on to an FTP server could not be completed because the supplied user name is incorrect.";
			break;
		case ERROR_INTERNET_INCORRECT_PASSWORD:		//12014	   	
			errorMsg = "The request to connect and log on to an FTP server could not be completed because the supplied password is incorrect.";
			break;
		case ERROR_INTERNET_NOT_PROXY_REQUEST:		//12020	   	
			errorMsg = "The request cannot be made via a proxy.";
			break;
		case ERROR_INTERNET_CANNOT_CONNECT:			//12029	   	
			errorMsg = "The attempt to connect to the server failed.";
			break;
		case ERROR_INTERNET_CONNECTION_ABORTED:		//12030   	
			errorMsg = "The connection with the server has been terminated.";
			break;
		case ERROR_INTERNET_FORCE_RETRY:			//12032	   	
			errorMsg = "Calls for the Win32 Internet function to redo the request.";
			break;
		case ERROR_INTERNET_INVALID_PROXY_REQUEST:	//12033	   	
			errorMsg = "The request to the proxy was invalid.";
			break;
		case ERROR_INTERNET_SEC_CERT_DATE_INVALID:	//12037	   	
			errorMsg = "SSL certificate date that was received from the server is bad. The certificate is expired.";
			break;
		case ERROR_INTERNET_SEC_CERT_CN_INVALID:	//12038	   	
			errorMsg = "SSL certificate common name (host name field) is incorrect.For example, if you entered www.server.com and the common name on the certificate says www.different.com.";
			break;
		case ERROR_INTERNET_HTTP_TO_HTTPS_ON_REDIR:	//12039	   	
			errorMsg = "The application is moving from a non-SSL to an SSL connection because of a redirect.";
			break;
		case ERROR_INTERNET_HTTPS_TO_HTTP_ON_REDIR:	//12040	   	
			errorMsg = "The application is moving from an SSL to an non-SSL connection because of a redirect.";
			break;
		case ERROR_INTERNET_MIXED_SECURITY:			//12041	   	
			errorMsg = "Indicates that the content is not entirely secure. Some of the content being viewed may have come from unsecured servers.";
			break;
		case ERROR_INTERNET_POST_IS_NON_SECURE:		//12043	   	
			errorMsg = "The application is posting data to a sever that is not secure.";
			break;
		case ERROR_HTTP_DOWNLEVEL_SERVER:			//12151	   	
			errorMsg = "The server did not return any headers.";
			break;
		case ERROR_HTTP_INVALID_SERVER_RESPONSE:	//12152	   	
			errorMsg = "The server response could not be parsed.";
			break;
		case ERROR_HTTP_REDIRECT_FAILED:			//12156	   	
			errorMsg = "The redirection failed because either the scheme changed (for example, HTTP to FTP) or all attempts made to redirect failed (default is five attempts)."; 
			break;
			break;
		case ERROR_INTERNET_OUT_OF_HANDLES:			//12001	   	
			errorMsg = "No more handles could be generated at this time.";
			break;
		case ERROR_INTERNET_TIMEOUT:				//12002	   	
			errorMsg = "The request has timed out.";
			break;
		case ERROR_INTERNET_INTERNAL_ERROR:			//12004	   	
			errorMsg = "An internal error has occurred.";
			break;
		case ERROR_INTERNET_INVALID_URL:			//12005	   	
			errorMsg = "The URL is invalid.";
			break;
		case ERROR_INTERNET_UNRECOGNIZED_SCHEME:	//12006	   	
			errorMsg = "The URL scheme could not be recognized or is not supported.";
			break;
		case ERROR_INTERNET_NAME_NOT_RESOLVED:		//12007	   	
			errorMsg = "The server name could not be resolved.";
			break;
		case ERROR_INTERNET_PROTOCOL_NOT_FOUND:		//12008	   	
			errorMsg = "The requested protocol could not be located.";
			break;
		case ERROR_INTERNET_INVALID_OPTION:			//12009	   	
			errorMsg = "A request to InternetQueryOption or InternetSetOption specified an invalid option value.";
			break;
		case ERROR_INTERNET_BAD_OPTION_LENGTH:		//12010 	
			errorMsg = "The length of an option supplied to InternetQueryOption or InternetSetOption is incorrect for the type of option specified.";
			break;
		case ERROR_INTERNET_OPTION_NOT_SETTABLE:	//12011	   	
			errorMsg = "The request option cannot be set, only queried.";
			break;
		case ERROR_INTERNET_SHUTDOWN:				//12012	   	
			errorMsg = "The Win32 Internet function support is being shut down or  unloaded.";
			break;
		case ERROR_INTERNET_LOGIN_FAILURE:			//12015	   	
			errorMsg = "The request to connect to and log on to an FTP server failed.";
			break;
		case ERROR_INTERNET_INVALID_OPERATION:		//12016  	
			errorMsg = "The requested operation is invalid.";
			break;
		case ERROR_INTERNET_OPERATION_CANCELLED:	//12017	   	
			errorMsg = "The operation was canceled, usually because the handle on which the request was operating was closed before the operation completed.";
			break;
		case ERROR_INTERNET_INCORRECT_HANDLE_TYPE:	//12018	   	
			errorMsg = "The type of handle supplied is incorrect for this operation.";
			break;
		case ERROR_INTERNET_INCORRECT_HANDLE_STATE:	//12019	   	
			errorMsg = "The requested operation cannot be carried out because the handle supplied is not in the correct state.";
			break;
		case ERROR_INTERNET_REGISTRY_VALUE_NOT_FOUND:	//12021	
			errorMsg = "A required registry value could not be located.";
			break;
		case ERROR_INTERNET_BAD_REGISTRY_PARAMETER:	//12022	   	
			errorMsg = "A required registry value was located but is an incorrect type or has an invalid value.";
			break;
		case ERROR_INTERNET_NO_DIRECT_ACCESS:		//12023	   	
			errorMsg = "Direct network access cannot be made at this time.";
			break;
		case ERROR_INTERNET_NO_CONTEXT:				//12024	   	
			errorMsg = "An asynchronous request could not be made because a zero context value was supplied.";
			break;
		case ERROR_INTERNET_NO_CALLBACK:			//12025   	
			errorMsg = "An asynchronous request could not be made because a callback function has not been set.";
			break;
		case ERROR_INTERNET_REQUEST_PENDING:		//12026	   	
			errorMsg = "The required operation could not be completed because one or more requests are pending.";
			break;
		case ERROR_INTERNET_INCORRECT_FORMAT:		//12027	   	
			errorMsg = "The format of the request is invalid.";
			break;
		case ERROR_INTERNET_ITEM_NOT_FOUND:			//12028	   	
			errorMsg = "The requested item could not be located.";
			break;
		case ERROR_INTERNET_CONNECTION_RESET:		//12031	  	
			errorMsg = "The connection with the server has been reset.";
			break;
		case ERROR_INTERNET_HANDLE_EXISTS:			//12036	   	
			errorMsg = "The request failed because the handle already exists.";
			break;
		case ERROR_INTERNET_CHG_POST_IS_NON_SECURE:	//12042	   	
			errorMsg = "The application is posting and attempting to change multiple lines of text on a server that is not secure.";
			break;
		case ERROR_FTP_TRANSFER_IN_PROGRESS:		//12110	   	
			errorMsg = "The requested operation cannot be made on the FTP session handle because an operation is already in progress.";
			break;
		case ERROR_FTP_DROPPED:						//12111	   	
			errorMsg = "The FTP operation was not completed because the session was aborted.";
			break;
		case ERROR_GOPHER_PROTOCOL_ERROR:			//12130	   	
			errorMsg = "An error was detected while parsing data returned from the gopher server.";
			break;
		case ERROR_GOPHER_NOT_FILE:					//12131	   	
			errorMsg = "The request must be made for a file locator.";
			break;
		case ERROR_GOPHER_DATA_ERROR:				//12132   	
			errorMsg = "An error was detected while receiving data from the gopher server.";
			break;
		case ERROR_GOPHER_END_OF_DATA:				//12133	   	
			errorMsg = "The end of the data has been reached.";
			break;
		case ERROR_GOPHER_INVALID_LOCATOR:			//12134	   	
			errorMsg = "The supplied locator is not valid.";
			break;
		case ERROR_GOPHER_INCORRECT_LOCATOR_TYPE:	//12135	   	
			errorMsg = "The type of the locator is not correct for this operation.";
			break;
		case ERROR_GOPHER_NOT_GOPHER_PLUS:			//12136	   	
			errorMsg = "The requested operation can only be made against a Gopher+ server or with a locator that specifies a Gopher+ operation.";
			break;
		case ERROR_GOPHER_ATTRIBUTE_NOT_FOUND:		//12137	   	
			errorMsg = "The requested attribute could not be located.";
			break;
		case ERROR_GOPHER_UNKNOWN_LOCATOR:			//12138	   	
			errorMsg = "The locator type is unknown.";
			break;
		case ERROR_HTTP_HEADER_NOT_FOUND:			//12150	   	
			errorMsg = "The requested header could not be located.";
			break;
		case ERROR_HTTP_INVALID_HEADER:				//12153	   	
			errorMsg = "The supplied header is invalid.";
			break;
		case ERROR_HTTP_INVALID_QUERY_REQUEST:		//12154	   	
			errorMsg = "The request made to HttpQueryInfo is invalid.";
			break;
		case ERROR_HTTP_HEADER_ALREADY_EXISTS:		//12155	   	
			errorMsg = "The header could not be added because it already exists.";
			break;
		default:
			break;
	}

	if (errorMsg.IsEmpty())
	{
		switch(GetLastErrorValue)
		{
			
		case ERROR_INTERNET_OUT_OF_HANDLES:
			errorMsg = "ERROR_INTERNET_OUT_OF_HANDLES";
			break;
		case ERROR_INTERNET_TIMEOUT:
			errorMsg = "ERROR_INTERNET_TIMEOUT";
			break;
		case ERROR_INTERNET_EXTENDED_ERROR:
			errorMsg = "ERROR_INTERNET_EXTENDED_ERROR";
			break;
		case ERROR_INTERNET_INTERNAL_ERROR:
			errorMsg = "ERROR_INTERNET_INTERNAL_ERROR";
			break;
		case ERROR_INTERNET_INVALID_URL:
			errorMsg = "ERROR_INTERNET_INVALID_URL";
			break;
		case ERROR_INTERNET_UNRECOGNIZED_SCHEME:
			errorMsg = "ERROR_INTERNET_UNRECOGNIZED_SCHEME";
			break;
		case ERROR_INTERNET_NAME_NOT_RESOLVED:
			errorMsg = "ERROR_INTERNET_NAME_NOT_RESOLVED";
			break;
		case ERROR_INTERNET_PROTOCOL_NOT_FOUND:
			errorMsg = "ERROR_INTERNET_PROTOCOL_NOT_FOUND";
			break;
		case ERROR_INTERNET_INVALID_OPTION:
			errorMsg = "ERROR_INTERNET_INVALID_OPTION";
			break;
		case ERROR_INTERNET_BAD_OPTION_LENGTH:
			errorMsg = "ERROR_INTERNET_BAD_OPTION_LENGTH";
			break;
		case ERROR_INTERNET_OPTION_NOT_SETTABLE:
			errorMsg = "ERROR_INTERNET_OPTION_NOT_SETTABLE";
			break;
		case ERROR_INTERNET_SHUTDOWN:
			errorMsg = "ERROR_INTERNET_SHUTDOWN";
			break;
		case ERROR_INTERNET_INCORRECT_USER_NAME:
			errorMsg = "ERROR_INTERNET_INCORRECT_USER_NAME";
			break;
		case ERROR_INTERNET_INCORRECT_PASSWORD:
			errorMsg = "ERROR_INTERNET_INCORRECT_PASSWORD";
			break;
		case ERROR_INTERNET_LOGIN_FAILURE:
			errorMsg = "ERROR_INTERNET_LOGIN_FAILURE";
			break;
		case ERROR_INTERNET_INVALID_OPERATION:
			errorMsg = "ERROR_INTERNET_INVALID_OPERATION";
			break;
		case ERROR_INTERNET_OPERATION_CANCELLED:
			errorMsg = "ERROR_INTERNET_OPERATION_CANCELLED";
			break;
		case ERROR_INTERNET_INCORRECT_HANDLE_TYPE:
			errorMsg = "ERROR_INTERNET_INCORRECT_HANDLE_TYPE";
			break;
		case ERROR_INTERNET_INCORRECT_HANDLE_STATE:
			errorMsg = "ERROR_INTERNET_INCORRECT_HANDLE_STATE";
			break;
		case ERROR_INTERNET_NOT_PROXY_REQUEST:
			errorMsg = "ERROR_INTERNET_NOT_PROXY_REQUEST";
			break;
		case ERROR_INTERNET_REGISTRY_VALUE_NOT_FOUND:
			errorMsg = "ERROR_INTERNET_REGISTRY_VALUE_NOT_FOUND";
			break;
		case ERROR_INTERNET_BAD_REGISTRY_PARAMETER:
			errorMsg = "ERROR_INTERNET_BAD_REGISTRY_PARAMETER";
			break;
		case ERROR_INTERNET_NO_DIRECT_ACCESS:
			errorMsg = "ERROR_INTERNET_NO_DIRECT_ACCESS";
			break;
		case ERROR_INTERNET_NO_CONTEXT:
			errorMsg = "ERROR_INTERNET_NO_CONTEXT";
			break;
		case ERROR_INTERNET_NO_CALLBACK:
			errorMsg = "ERROR_INTERNET_NO_CALLBACK";
			break;
		case ERROR_INTERNET_REQUEST_PENDING:
			errorMsg = "ERROR_INTERNET_REQUEST_PENDING";
			break;
		case ERROR_INTERNET_INCORRECT_FORMAT:
			errorMsg = "ERROR_INTERNET_INCORRECT_FORMAT";
			break;
		case ERROR_INTERNET_ITEM_NOT_FOUND:
			errorMsg = "ERROR_INTERNET_ITEM_NOT_FOUND";
			break;
		case ERROR_INTERNET_CANNOT_CONNECT:
			errorMsg = "ERROR_INTERNET_CANNOT_CONNECT";
			break;
		case ERROR_INTERNET_CONNECTION_ABORTED:
			errorMsg = "ERROR_INTERNET_CONNECTION_ABORTED";
			break;
		case ERROR_INTERNET_CONNECTION_RESET:
			errorMsg = "ERROR_INTERNET_CONNECTION_RESET";
			break;
		case ERROR_INTERNET_FORCE_RETRY:
			errorMsg = "ERROR_INTERNET_FORCE_RETRY";
			break;
		case ERROR_INTERNET_INVALID_PROXY_REQUEST:
			errorMsg = "ERROR_INTERNET_INVALID_PROXY_REQUEST";
			break;
		case ERROR_INTERNET_NEED_UI:
			errorMsg = "ERROR_INTERNET_NEED_UI";
			break;
			
		case ERROR_INTERNET_HANDLE_EXISTS:
			errorMsg = "ERROR_INTERNET_HANDLE_EXISTS";
			break;
		case ERROR_INTERNET_SEC_CERT_DATE_INVALID:
			errorMsg = "ERROR_INTERNET_SEC_CERT_DATE_INVALID";
			break;
		case ERROR_INTERNET_SEC_CERT_CN_INVALID:
			errorMsg = "ERROR_INTERNET_SEC_CERT_CN_INVALID";
			break;
		case ERROR_INTERNET_HTTP_TO_HTTPS_ON_REDIR:
			errorMsg = "ERROR_INTERNET_HTTP_TO_HTTPS_ON_REDIR";
			break;
		case ERROR_INTERNET_HTTPS_TO_HTTP_ON_REDIR:
			errorMsg = "ERROR_INTERNET_HTTPS_TO_HTTP_ON_REDIR";
			break;
		case ERROR_INTERNET_MIXED_SECURITY:
			errorMsg = "ERROR_INTERNET_MIXED_SECURITY";
			break;
		case ERROR_INTERNET_CHG_POST_IS_NON_SECURE:
			errorMsg = "ERROR_INTERNET_CHG_POST_IS_NON_SECURE";
			break;
		case ERROR_INTERNET_POST_IS_NON_SECURE:
			errorMsg = "ERROR_INTERNET_POST_IS_NON_SECURE";
			break;
		case ERROR_INTERNET_CLIENT_AUTH_CERT_NEEDED:
			errorMsg = "ERROR_INTERNET_CLIENT_AUTH_CERT_NEEDED";
			break;
		case ERROR_INTERNET_INVALID_CA:
			errorMsg = "ERROR_INTERNET_INVALID_CA";
			break;
		case ERROR_INTERNET_CLIENT_AUTH_NOT_SETUP:
			errorMsg = "ERROR_INTERNET_CLIENT_AUTH_NOT_SETUP";
			break;
		case ERROR_INTERNET_ASYNC_THREAD_FAILED:
			errorMsg = "ERROR_INTERNET_ASYNC_THREAD_FAILED";
			break;
		case ERROR_INTERNET_REDIRECT_SCHEME_CHANGE:
			errorMsg = "ERROR_INTERNET_REDIRECT_SCHEME_CHANGE";
			break;
		case ERROR_INTERNET_DIALOG_PENDING:
			errorMsg = "ERROR_INTERNET_DIALOG_PENDING";
			break;
		case ERROR_INTERNET_RETRY_DIALOG:
			errorMsg = "ERROR_INTERNET_RETRY_DIALOG";
			break;
		case ERROR_INTERNET_HTTPS_HTTP_SUBMIT_REDIR:
			errorMsg = "ERROR_INTERNET_HTTPS_HTTP_SUBMIT_REDIR";
			break;
		case ERROR_INTERNET_INSERT_CDROM:
			errorMsg = "ERROR_INTERNET_INSERT_CDROM";
			break;
			
			
			//
			// FTP API errors
			//
			
		case ERROR_FTP_TRANSFER_IN_PROGRESS:
			errorMsg = "ERROR_FTP_TRANSFER_IN_PROGRESS ";
			break;
		case ERROR_FTP_DROPPED:
			errorMsg = "ERROR_FTP_DROPPED";
			break;
		case ERROR_FTP_NO_PASSIVE_MODE:
			errorMsg = "ERROR_FTP_NO_PASSIVE_MODE";
			break;
			
			//
			// gopher API errors
			//
			
		case ERROR_GOPHER_PROTOCOL_ERROR:
			errorMsg = "ERROR_GOPHER_PROTOCOL_ERROR";
			break;
		case ERROR_GOPHER_NOT_FILE:
			errorMsg = "ERROR_GOPHER_NOT_FILE";
			break;
		case ERROR_GOPHER_DATA_ERROR:
			errorMsg = "ERROR_GOPHER_DATA_ERROR";
			break;
		case ERROR_GOPHER_END_OF_DATA:
			errorMsg = "ERROR_GOPHER_END_OF_DATA";
			break;
		case ERROR_GOPHER_INVALID_LOCATOR:
			errorMsg = "ERROR_GOPHER_INVALID_LOCATOR";
			break;
		case ERROR_GOPHER_INCORRECT_LOCATOR_TYPE:
			errorMsg = "ERROR_GOPHER_INCORRECT_LOCATOR_TYPE";
			break;
		case ERROR_GOPHER_NOT_GOPHER_PLUS:
			errorMsg = "ERROR_GOPHER_NOT_GOPHER_PLUS";
			break;
		case ERROR_GOPHER_ATTRIBUTE_NOT_FOUND:
			errorMsg = "ERROR_GOPHER_ATTRIBUTE_NOT_FOUND";
			break;
		case ERROR_GOPHER_UNKNOWN_LOCATOR:
			errorMsg = "ERROR_GOPHER_UNKNOWN_LOCATOR ";
			break;
			//
			// HTTP API errors
			//
			
		case ERROR_HTTP_HEADER_NOT_FOUND:
			errorMsg = "ERROR_HTTP_HEADER_NOT_FOUND";
			break;
		case ERROR_HTTP_DOWNLEVEL_SERVER:
			errorMsg = "ERROR_HTTP_DOWNLEVEL_SERVER";
			break;
		case ERROR_HTTP_INVALID_SERVER_RESPONSE:
			errorMsg = "ERROR_HTTP_INVALID_SERVER_RESPONSE";
			break;
		case ERROR_HTTP_INVALID_HEADER:
			errorMsg = "ERROR_HTTP_INVALID_HEADER";
			break;
		case ERROR_HTTP_INVALID_QUERY_REQUEST:
			errorMsg = "ERROR_HTTP_INVALID_QUERY_REQUEST";
			break;
		case ERROR_HTTP_HEADER_ALREADY_EXISTS:
			errorMsg = "ERROR_HTTP_HEADER_ALREADY_EXISTS";
			break;
		case ERROR_HTTP_REDIRECT_FAILED:
			errorMsg = "ERROR_HTTP_REDIRECT_FAILED";
			break;
		case ERROR_HTTP_NOT_REDIRECTED:
			errorMsg = "ERROR_HTTP_NOT_REDIRECTED";
			break;
		case ERROR_HTTP_COOKIE_NEEDS_CONFIRMATION:
			errorMsg = "ERROR_HTTP_COOKIE_NEEDS_CONFIRMATION";
			break;
		case ERROR_HTTP_COOKIE_DECLINED:
			errorMsg = "ERROR_HTTP_COOKIE_DECLINED";
			break;
		case ERROR_HTTP_REDIRECT_NEEDS_CONFIRMATION:
			errorMsg = "ERROR_HTTP_REDIRECT_NEEDS_CONFIRMATION";
			break;
			
			//
			// additional Internet API error codes
			//
			
		case ERROR_INTERNET_SECURITY_CHANNEL_ERROR:
			errorMsg = "ERROR_INTERNET_SECURITY_CHANNEL_ERROR";
			break;
		case ERROR_INTERNET_UNABLE_TO_CACHE_FILE:
			errorMsg = "ERROR_INTERNET_UNABLE_TO_CACHE_FILE";
			break;
		case ERROR_INTERNET_TCPIP_NOT_INSTALLED:
			errorMsg = "ERROR_INTERNET_TCPIP_NOT_INSTALLED";
			break;
		case ERROR_INTERNET_DISCONNECTED:
			errorMsg = "ERROR_INTERNET_DISCONNECTED";
			break;
		case ERROR_INTERNET_SERVER_UNREACHABLE:
			errorMsg = "ERROR_INTERNET_SERVER_UNREACHABLE";
			break;
		case ERROR_INTERNET_PROXY_SERVER_UNREACHABLE:
			errorMsg = "ERROR_INTERNET_PROXY_SERVER_UNREACHABLE";
			break;
			
		case ERROR_INTERNET_BAD_AUTO_PROXY_SCRIPT:
			errorMsg = "ERROR_INTERNET_BAD_AUTO_PROXY_SCRIPT";
			break;
		case ERROR_INTERNET_UNABLE_TO_DOWNLOAD_SCRIPT:
			errorMsg = "ERROR_INTERNET_UNABLE_TO_DOWNLOAD_SCRIPT";
			break;
		case ERROR_INTERNET_SEC_INVALID_CERT:
			errorMsg = "ERROR_INTERNET_SEC_INVALID_CERT";
			break;
		case ERROR_INTERNET_SEC_CERT_REVOKED:
			errorMsg = "ERROR_INTERNET_SEC_CERT_REVOKED";
			break;
			
			
		// InternetAutodial specific errors
			
		case ERROR_INTERNET_FAILED_DUETOSECURITYCHECK:
			errorMsg = "ERROR_INTERNET_FAILED_DUETOSECURITYCHECK";
			break;
			
			
		}
	}
}

int HttpRetrieval::LoadResponseData(void* TheDataStruct, CMapStringToString &Result, int &ResponseStatus)
{
	ResponseStatus = HttpRetrieval::ERROR_STATUS;
	int NetworkError = HttpRetrieval::GOOD_STATUS;
	ThreadStruct* TheThreadStruct = 
		(ThreadStruct*) TheDataStruct;
	NetworkError = TheThreadStruct->NetworkError;

	BYTE* BufferPtr;
	bool LoadStatus = TRUE;
	long BytesLeft = 0;
	long TmpLong = 0;
	CString TmpCString;
	CString TmpResult;
	CString LocalKey;
	CString LocalValue;
	CString LastError;

	LastError = TheThreadStruct->TheErrorMsg;
//	SessionData.RemoveAll();
	Result.SetAt("ORIGINAL_REQUEST", TheThreadStruct->TheRequestString);

	if ((TheThreadStruct->TheData != NULL) && (LastError.GetLength() == 0))
	{
		BufferPtr = TheThreadStruct->TheData;
		BytesLeft = TheThreadStruct->CurrentByteCount;
		
		BOOL Done = FALSE;
		while (!Done)
		{
			BufferPtr = HttpRetrieval::ReadLineFromBuffer(BufferPtr, TmpResult, BytesLeft);
			TmpResult = TmpResult.Trim();
			if (TmpResult.IsEmpty() == FALSE && TmpResult.Find("=") > 0)
			{
				CXUtils::SplitCStringOnChar(TmpResult, '=',
					LocalKey, LocalValue);
				HttpRetrieval::TrimWhitespaceFromEnds(LocalKey);
				HttpRetrieval::TrimWhitespaceFromEnds(LocalValue);
				if (LocalKey.IsEmpty() == FALSE)
				{
					Result.SetAt(LocalKey, LocalValue);
				}
			}
			
			if (BytesLeft <= 0)
				Done = TRUE;
		}
	}
	else
	{
		AfxMessageBox(LastError);
	}
	CleanupThreadStruct(TheThreadStruct);
	TheThreadStruct = NULL;

	CString ResponseValue;
	if (CXUtils::SafeLookup(Result, "DB_STATUS", ResponseValue) != 0)
	{
		if (ResponseValue == "0") 
		{
			ResponseStatus = HttpRetrieval::GOOD_STATUS;
		}
	}
	return NetworkError;
}

BYTE* HttpRetrieval::ReadLineFromBuffer(BYTE*& TheData, CString &TheCString, long &BytesLeft)
{
	BYTE* MemPtr;
	BYTE* UseDataPtr = TheData;
	BOOL FoundCR = false;	
	MemPtr = TheData;
	TheCString.Empty();
	long ByteOffset=0;
	long NumBytes=0;

	if (BytesLeft > 0)
	{
		BOOL Done = FALSE;
		while (!Done)
		{
			NumBytes = 0;
			if (BytesLeft <= 0)
				break;

			for (int pos=0; pos<BytesLeft; pos++)
			{
				if (UseDataPtr[pos] == '\n')
				{
					if (pos > 0)
					{
						NumBytes = pos;
						ByteOffset = NumBytes+1;
					}

					FoundCR = TRUE;
					break;
				}
			}

			if (FoundCR == FALSE)
			{
				int pos = BytesLeft-1;
				if (UseDataPtr[pos] == '\n')
				{
					NumBytes = pos;
					ByteOffset = NumBytes+1;
				}
				else
				{
					NumBytes = BytesLeft;
					ByteOffset = NumBytes;
				}
			}
			if (NumBytes > 0)
			{
				LPTSTR thebuff = TheCString.GetBuffer(NumBytes+1);
				memcpy(thebuff, UseDataPtr, NumBytes);
				thebuff[NumBytes] = 0;
				TheCString.ReleaseBuffer(-1);
				Done = TRUE;
			}
			else
			{
				UseDataPtr++;
				BytesLeft--;
			}
		}

		BytesLeft -= ByteOffset;
		MemPtr = UseDataPtr + ByteOffset;
	}
	return MemPtr;
}
void HttpRetrieval::TrimWhitespaceFromEnds(CString& theString)
{
	theString.TrimLeft();
	theString.TrimRight();
}

