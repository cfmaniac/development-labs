// EncoderCommand.h: interface for the CEncoderCommand class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ENCODERCOMMAND_H__604FA40E_A10A_48EB_8627_12C805202CF4__INCLUDED_)
#define AFX_ENCODERCOMMAND_H__604FA40E_A10A_48EB_8627_12C805202CF4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CEncoderCommand  
{
public:
	CEncoderCommand();
	virtual ~CEncoderCommand();

	CString theID;
	CString theCommand;
	CString archiveFlag;
	CString port;
	CString theAudioDevice;
	CString theVideoDevice;
	CString recordingDevice;
	CString deviceType;
	CString encoderProfile;
	CString eventName;
	CString speedKey;
	CString gmtStartDate;
	CString gmtCurDate;

	void initValues();
};

#endif // !defined(AFX_ENCODERCOMMAND_H__604FA40E_A10A_48EB_8627_12C805202CF4__INCLUDED_)
