// EncoderDlg.cpp : implementation file
//

#include "stdafx.h"
#include "AlvaradoMgr.h"

#pragma warning(disable:4786)
#include <string>
#pragma warning(disable:4786)
#include <vector>
#include "StringUtil.h"

#include "EncoderDlg.h"
//#include "HttpResponse.h"

#ifdef _USEING_ENCODER_CODE_
	#include "DBVMediaEncoder.h"
	#include "EncoderCommand.h"
#endif

#include "ConfigFileManager.h"
#include "HttpRetrieval.h"
#include "XUtils.h"
#include "afxdb.h"
#include "stock.h"
#include ".\encoderdlg.h"
#include ".\MyDatabase.h"
#include ".\MyRecordset.h"
#include ".\StockInfoUpdateDlg.h"
#include ".\ProcessingCommand.h"

// disable warning C4786: symbol greater than 255 character,
// okay to ignore


using namespace std ;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define CRLF			"\x0d\x0a"
#define DOUBLE_CRLF		"\x0d\x0a\x0d\x0a"
#define SEPCHAR			'\\'
#define PORT_HTTP		80
#define HTTP_SEPCHAR	'/'

BOOL CEncoderDlg::ConfigPropertiesLoaded = FALSE;
CMapStringToString CEncoderDlg::thePropertiesData;
CStringArray CEncoderDlg::theAdminIPs;
BOOL CEncoderDlg::b_STOCK_UPDATED = FALSE;
BOOL CEncoderDlg::b_BOX_UPDATED = FALSE;
BOOL CEncoderDlg::b_ITEMS_UPDATED = FALSE;
BOOL CEncoderDlg::b_CUST_UPDATED = FALSE;
BOOL CEncoderDlg::b_INVOICE_UPDATED = FALSE;
CStringList CEncoderDlg::theEmailMessageList;
CPtrArray CEncoderDlg::processingSteps;
CStringList CEncoderDlg::stockUpdateValues;
CStringList CEncoderDlg::statusUpdateValues;
bool CEncoderDlg::b_interactiveMode = false;

time_t CEncoderDlg::l_refreshEventListTime = 0;
time_t CEncoderDlg::l_uploadTicketInfoTime = time(NULL);
time_t CEncoderDlg::l_downloadTicketInfoTime = 0;

int CEncoderDlg::m_EventRefreshInterval = 600; // Update Event List in Seconds
int CEncoderDlg::m_TicketInfoDownloadInterval = 15;
int CEncoderDlg::m_TicketInfoUploadInterval = 60;


//HttpServer CEncoderDlg::server;

/////////////////////////////////////////////////////////////////////////////
// CEncoderDlg dialog


#pragma warning ( disable : 4786)

CEncoderDlg::CEncoderDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CEncoderDlg::IDD, pParent)
	, b_SendSyncResponseToRemote(false)
	, m_ActivityChar(_T(""))
	, m_BatchRequestsSent(0)
{
	//{{AFX_DATA_INIT(CEncoderDlg)
	m_AlwaysOnTop = FALSE;
	m_STAR = _T("");
	//}}AFX_DATA_INIT

	CDatabaseOperations::SetParent(this);

	shutdownComplete = FALSE;
	shutdownInProgress = FALSE;
	timeOfLastMessageSend = 0;
	timeOfLastFtpSend = 0;
	responseSequenceIndex = 0;
	lastErrorEmailSent = time(NULL);
	i_httpErrorCount = 0;
	i_LastErrorWriteCount = 0;
#ifdef _DEBUG
	delayBetweenInfoDownloadsSecs = 10;
#else
	delayBetweenInfoDownloadsSecs = 10;
#endif

	InitializeCriticalSection(&m_csEncoderArray);
}

CEncoderDlg::~CEncoderDlg()
{
	DeleteCriticalSection(&m_csEncoderArray); 
	thePropertiesData.RemoveAll();
	ConfigPropertiesLoaded = FALSE;

}
void CEncoderDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEncoderDlg)
	DDX_Control(pDX, IDC_STAR, m_StarControl);
	DDX_Control(pDX, IDCANCEL, m_CancelButton);
	DDX_Control(pDX, IDC_ACTIVE_ENCODER_LIST, m_ActiveEncoderList);
	DDX_Control(pDX, IDC_MSG_LIST, m_MessageListBox);
	DDX_Check(pDX, IDC_CHECK1, m_AlwaysOnTop);
	DDX_Text(pDX, IDC_STAR, m_STAR);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_ENABLE_UPLOAD, m_EnableUpload);
	DDX_Control(pDX, IDC_EVENT_LIST, m_CurrentEventListCtrl);
}


BEGIN_MESSAGE_MAP(CEncoderDlg, CDialog)
	//{{AFX_MSG_MAP(CEncoderDlg)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON1, OnReloadProperties)
	ON_BN_CLICKED(IDC_CHECK1, OnCheck1)
	//}}AFX_MSG_MAP
	ON_WM_SYSCOMMAND()
	ON_LBN_SELCHANGE(IDC_MSG_LIST, OnLbnSelchangeMsgList)
	ON_BN_CLICKED(IDCANCEL, OnBnClickedCancel)
	ON_BN_CLICKED(IDC_ENABLE_UPLOAD, OnBnClickedEnableUpload)
	ON_NOTIFY(LVN_COLUMNCLICK, IDC_EVENT_LIST, OnLvnColumnclickEventList)
	ON_NOTIFY(NM_DBLCLK, IDC_EVENT_LIST, OnNMDblclkEventList)
	ON_BN_CLICKED(ID_DOWNLOAD_EVENTS, OnBnClickedDownloadEvents)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEncoderDlg message handlers

BOOL CEncoderDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_hIcon = AfxGetApp()->LoadIcon(IDI_ICON1);
	SetIcon(m_hIcon, TRUE);
	SetIcon(m_hIcon, FALSE);

	i_RetryCount = 0;
	
	m_CurrentProcessingCommand = NULL;
	m_UploadProcessingCommand = NULL;
	m_ConfigDataRetrieved = false;
	b_STOCK_UPDATED = false;
	b_BOX_UPDATED = false;
	b_ITEMS_UPDATED = false;
	b_CUST_UPDATED = false;
	b_INVOICE_UPDATED = false;

//	b_AllProcessingIsComplete =false;
	b_ContinueIfRequestFailure = true;

	LoadConfigProperties(NULL);

	CString tmpValue;
	propertyLookup("ENABLE_UPLOAD_PROCESSING", tmpValue);
	if (tmpValue == "yes")
	{
		this->m_EnableUpload.SetCheck(BST_CHECKED);
	}
	if (this->b_interactiveMode)
	{
		m_CancelButton.EnableWindow(true);
	}

//	processingSteps.Add("INVOICE_STATUS_UPDATES");


//	if (StartedByService)
//	{
//		m_CancelButton.EnableWindow(FALSE);
//	}

	theStartupTime = CTime::GetCurrentTime();
	lastStatusDump = 0;

	CString OutMsg;
	OutMsg.Format("SYSTEM STARTUP TIME:%s", CTime::GetCurrentTime().Format("%d%b%y::%H:%M:%S"));
	CXUtils::WriteToTheLogFile(OutMsg);


	/*
	CString dsnName;
	CString dbPath;
	CString dbName;
	propertyLookup("ODBC_DSN_NAME", dsnName);
	propertyLookup("LOCAL_DB_BASE_DIR", dbPath);
	dbPath += "\\oc";
	dbName = "UPDATE_STOCK_INFORMATION";

	LoadTableData(dsnName, dbPath, dbName);
*/

	SetTimer(500,500, NULL);

	OutMsg = "AlvaradoMgr Started...";
	AddMessageBoxString(OutMsg);

	m_StarControl.SetWindowText("");

    CClientDC dc(this);
    CSize sz = dc.GetTextExtent("WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
	m_MessageListBox.SetHorizontalExtent(sz.cx);

	CListCtrl* theCtrl;
	theCtrl = (CListCtrl*)GetDlgItem(IDC_EVENT_LIST);
	int curItem=0;
	int colIndex = theCtrl->InsertColumn( curItem, "Code", LVCFMT_LEFT, 50);
	curItem++;
	colIndex = theCtrl->InsertColumn( curItem, "ID", LVCFMT_LEFT, 50);
	curItem++;
	colIndex = theCtrl->InsertColumn( curItem, "Name", LVCFMT_LEFT, 170);
	curItem++;
	colIndex = theCtrl->InsertColumn( curItem, "Start", LVCFMT_LEFT, 180);
	curItem++;
	colIndex = theCtrl->InsertColumn( curItem, "SCND", LVCFMT_CENTER, 50);

//	OutMsg.Format("Retreiving configuration data");
//	AddMessageBoxString(OutMsg);
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CEncoderDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CTimeSpan theTimeSpan;
	theTimeSpan = CTime::GetCurrentTime() - theStartupTime;
	CString OutMsg;
	OutMsg.Format("UPTIME:%s", theTimeSpan.Format("%D Days, %H Hours, %M Minutes, %S Seconds"));
	CXUtils::WriteToTheLogFile(OutMsg);

	OutMsg.Format("SYSTEM SHUTDOWN TIME:%s\n", CTime::GetCurrentTime().Format("%d%b%y::%H:%M:%S"));
	CXUtils::WriteToTheLogFile(OutMsg);

	DoCommonShutdown();

	CDialog::OnCancel();
	shutdownComplete = TRUE;

}
BOOL CEncoderDlg::IsShutdownComplete()
{
	return shutdownComplete;
}

void CEncoderDlg::DoCommonShutdown()
{
	SendAnyEmails(true);
	CXUtils::WriteToTheLogFile("Received Shutdown", true);
	KillTimer(500);
	shutdownInProgress = TRUE;

	CString responseData;
//	CEncoderCommand theCommands;
//
//	theCommands.theCommand = "delete_all_encoders";
//	BOOL opSuccess = executeCommands(theCommands, responseData);
//
//	theCommands.theCommand = "delete_waiting_encoders";
//	while (opSuccess == FALSE)
//	{
//		opSuccess = executeCommands(theCommands, responseData);
//		CXUtils::YieldToTheOS();
//	}
}
void CEncoderDlg::OnOK() 
{
	// TODO: Add extra validation here
	
	DoCommonShutdown();
	CDialog::OnOK();
}

void CEncoderDlg::DoTimerEvents() 
{
	CString RemoteUrl;
	CString theMessage;
	CString RemoteUrlAppendData;
	CString RemoteServerKeyID;
	CString statusResponse;
	CMapStringToString Result;
	int ResponseStatus;
	CString deliveryMethod = "POST";

	BOOL usePassiveFTP = true;
	CString ftpLocalFilename;
	CString ftpRemoteFilename;
	CString ftpUsername;
	CString ftpPassword;
	HttpRetrieval::RetreivalType curRetrievalType = HttpRetrieval::INFO_DOWNLOAD;
	BOOL doTheHttpSend = FALSE;

	if (
		(theHttpRetrieval.IsThreadTypeRunning(HttpRetrieval::INFO_DOWNLOAD) == FALSE && 
		theHttpRetrieval.IsThreadTypeRunning(HttpRetrieval::TKT_INFO_DOWNLOAD) == FALSE )
		)
	{
//		TRACE("CHECK FOR NEW ITEMS\n");
		if (m_CurrentProcessingCommand == NULL)
		{
			if (processingSteps.GetSize() > 0)
			{
				m_CurrentProcessingCommand = (CProcessingCommand*)processingSteps.GetAt(0);
				CString timeval;
				timeval.Format("%d",time(NULL));
				m_CurrentProcessingCommand->theCommandData.SetAt("COMMAND_START_TIME", timeval);

				processingSteps.RemoveAt(0);
			}
		}

		if (m_CurrentProcessingCommand != NULL)
		{
			propertyLookup("SERVER_URL", RemoteUrl);
			propertyLookup("SERVER_URL_APPEND_DATA", RemoteUrlAppendData);
			propertyLookup("SERVER_KEY_ID", RemoteServerKeyID);
			RemoteUrlAppendData.Append("&SERVER_KEY_ID=" + RemoteServerKeyID);

			CString theCommand;
			m_CurrentProcessingCommand->CommandDataLookup("COMMAND", theCommand);

			if (theCommand == "1_GET_EVENTS")
			{
				m_ActivityChar = "EV";
				m_StarControl.SetWindowText(m_ActivityChar);
				theMessage.Format("Retrieval of current Events:: STARTING");
				CXUtils::WriteToTheLogFile(theMessage);
				AddMessageBoxString(theMessage);
				RemoteUrl += "/getAlvaradoEvents.dbml?MODE=ANY";
				b_ContinueIfRequestFailure = false;
				doTheHttpSend = TRUE;
			}
			else if (theCommand == "3_GET_TICKET_UPDATES")
			{
				m_ActivityChar = "TB";
				m_StarControl.SetWindowText(m_ActivityChar);
				curRetrievalType = HttpRetrieval::TKT_INFO_DOWNLOAD;
				theMessage.Format("Retrieval of new ticket data for import from the remote system: STARTING");
//				CXUtils::WriteToTheLogFile(theMessage);
//				AddMessageBoxString(theMessage);
				RemoteUrl += "/GetEventTicketsBatch.dbml?MODE=ANY&GETEVENTIDS=" + currentActiveEventList;
				b_ContinueIfRequestFailure = false;
				m_BatchRequestsSent++;
				doTheHttpSend = TRUE;
			}			
			else if (theCommand == "4_SEND_TICKET_UPDATE_RESPONSE")
			{
				CString timeval;
				timeval.Format("%d",time(NULL));
				m_CurrentProcessingCommand->theCommandData.SetAt("COMMAND_START_TIME", timeval);

				TRACE("SEND 4_SEND_TICKET_UPDATE_RESPONSE\n");
				m_ActivityChar = "TBR";
				m_StarControl.SetWindowText(m_ActivityChar);
				theMessage.Format("Sending Notify Of Ticket Batches Received: STARTING");
				CXUtils::WriteToTheLogFile(theMessage);
				CString batchesReceived;
				m_CurrentProcessingCommand->CommandDataLookup("TICKET_BATCH_LIST", batchesReceived);

//				AddMessageBoxString(theMessage);
				RemoteUrl += "/setAlvaradoBatchesReceived.dbml?MODE=ANY&BATCHES=" + batchesReceived;
				b_ContinueIfRequestFailure = false;
				doTheHttpSend = TRUE;
			}
			else
			{
				RemoteUrl.Empty();
			}

			if (RemoteUrl.GetLength() > 0)
			{
				RemoteUrl += RemoteUrlAppendData;
			}
		}
	}
	else
	{
		if ((timeOfLastMessageSend + 1200) < time(NULL))
		{
			CString locErrorMsg;
			locErrorMsg.Format("%s::%s%d\n%s%d\n",
				"http request has failed",
				"INFO_DOWNLOAD Status:", 
				theHttpRetrieval.IsThreadTypeRunning(HttpRetrieval::INFO_DOWNLOAD),
				"TKT_INFO_DOWNLOAD Status:", 
				theHttpRetrieval.IsThreadTypeRunning(HttpRetrieval::TKT_INFO_DOWNLOAD));

			CXUtils::WriteToTheLogFile(locErrorMsg);
		}
	}

	if (doTheHttpSend == FALSE)
	{
		if (theHttpRetrieval.IsThreadTypeRunning(HttpRetrieval::FTP_UPLOAD) == FALSE)
		{
			if (m_UploadProcessingCommand != NULL)
			{
				propertyLookup("SERVER_URL", RemoteUrl);
				propertyLookup("SERVER_URL_APPEND_DATA", RemoteUrlAppendData);
				propertyLookup("SERVER_KEY_ID", RemoteServerKeyID);
				RemoteUrlAppendData.Append("&SERVER_KEY_ID=" + RemoteServerKeyID);

				CString theCommand;
				m_UploadProcessingCommand->CommandDataLookup("COMMAND", theCommand);

				if (theCommand == "2_UPLOAD_TICKET_EXPORTS")
				{
					CString theUploadFilename;
					int numPending = FindUploadFiles(m_UploadProcessingCommand);

					CWnd* theCheckbox = (CWnd*) GetDlgItem(IDC_ENABLE_UPLOAD);
					CString theNewText;
					theNewText.Format("Enable File Upload (%d)", numPending);
					theCheckbox->SetWindowText(theNewText);

					m_UploadProcessingCommand->CommandDataLookup("FTP_LOCAL_UPLOAD_FILENAME", theUploadFilename);
					if (theUploadFilename.GetLength() > 0)
					{
						m_ActivityChar = "UP";
						m_StarControl.SetWindowText(m_ActivityChar);

						theMessage.Format("Check to upload ticket orders: STARTING");
						AddMessageBoxString(theMessage);
						deliveryMethod = "FTP_PUT";
						curRetrievalType = HttpRetrieval::FTP_UPLOAD;
						ftpLocalFilename = theUploadFilename;
						m_UploadProcessingCommand->CommandDataLookup("FTP_REMOTE_UPLOAD_FILENAME", ftpRemoteFilename);
						propertyLookup("FTP_SERVER", RemoteUrl);
						RemoteUrl.MakeLower();
						if (RemoteUrl.Find("ftp://") != 0)
						{
							RemoteUrl = "ftp://" + RemoteUrl;
						}
						propertyLookup("FTP_USERNAME", ftpUsername);
						propertyLookup("FTP_PASSWORD", ftpPassword);
						b_ContinueIfRequestFailure = false;
						doTheHttpSend = TRUE;


						theMessage.Format("FTP Upload: Export File %s: STARTING", ftpLocalFilename);
						CXUtils::WriteToTheLogFile(theMessage, false);

					}
					else	// No Files to Upload
					{
						RemoteUrl.Empty();
						delete m_UploadProcessingCommand;
						m_UploadProcessingCommand = NULL;
					}
				}
			}
		}
		else
		{
			if ((timeOfLastMessageSend + 1200) < time(NULL))
			{
				CString locErrorMsg;
				locErrorMsg.Format("%s::%s%d\n",
					"http request has failed",
					"INFO_DOWNLOAD Status:", 
					theHttpRetrieval.IsThreadTypeRunning(HttpRetrieval::FTP_UPLOAD));
	
				CXUtils::WriteToTheLogFile(locErrorMsg);
			}
		}
	}

	if (doTheHttpSend)
	{
		if (RemoteUrl.GetLength() > 0 )
		{

			CString encodedStatusResponse;

			CString proxyUsername;
			CString proxyPassword;

			propertyLookup("PROXY_USERNAME", proxyUsername);
			propertyLookup("PROXY_PASSWORD", proxyPassword);

			CString OutMsg;
			CTime TheTime = CTime::GetCurrentTime();
			OutMsg = TheTime.Format("%d%b%y::%H:%M:%S ");

			TRACE("SENDING OFF HTTP REQUEST :%s\n",OutMsg);
			timeOfLastMessageSend = time(NULL);
			HttpRetrieval::ThreadStruct* theThreadStruct;
			m_ActivityChar += "*";
			m_StarControl.SetWindowText(m_ActivityChar);

			theThreadStruct = theHttpRetrieval.BuildAndStartThread(
				RemoteUrl, 
				curRetrievalType,		//default:  HttpRetrieval::INFO_DOWNLOAD,
				10000,
				deliveryMethod,
				proxyUsername,
				proxyPassword,
				usePassiveFTP,
				ftpLocalFilename,
				ftpRemoteFilename,
				ftpUsername,
				ftpPassword);
		}
//		else
//		{
//			CString theMessage;
//			theMessage.Format("%s:%s","Unknown theCommand type:", theCommand);
//			CXUtils::WriteToTheLogFile(theMessage, true);
//			theEmailMessageList.AddTail(theMessage);
//		}
	}

	if ((timeOfLastMessageSend + 1200) < time(NULL))
	{
		CString locErrorMsg;
		locErrorMsg.Format("%s\n%s",
			"The App has not sent a message in 20 minutes.",
			"Please check your network connectivity and retry your request.");

		CXUtils::WriteToTheLogFile(locErrorMsg);
		if (m_CurrentProcessingCommand != NULL)
		{

			CString theCommand;
			m_CurrentProcessingCommand->CommandDataLookup("COMMAND", theCommand);
			locErrorMsg.Format("%s\n%s","m_CurrentProcessingCommand",theCommand);

			CXUtils::WriteToTheLogFile(locErrorMsg);
		}

		CXUtils::WriteToTheLogFile("Checking Processing Steps In Queue");

		for (int i=0;i < processingSteps.GetSize();i++)
		{
			CProcessingCommand* curCmd = (CProcessingCommand*) processingSteps.GetAt(i);
			if (curCmd != NULL)
			{
				CString checkCmd;
				curCmd->CommandDataLookup("COMMAND",checkCmd);
				CXUtils::WriteToTheLogFile(checkCmd);
			}
		}
	}

	if (doEventCheck(Result, ResponseStatus))
	{
		CString responseData;
	}

}

void CEncoderDlg::OnTimer(UINT nIDEvent) 
{
	KillTimer(500);
	if (shutdownInProgress)
		return;

	CString windowStar;
	m_StarControl.GetWindowText(windowStar);
	if (windowStar.IsEmpty())
	{
		if (m_ActivityChar.IsEmpty()) m_ActivityChar = "^";
		m_StarControl.SetWindowText(m_ActivityChar);
	}
	else if (windowStar.IsEmpty() == false)
	{
		m_StarControl.SetWindowText("");
	}

	// TODO: Add your message handler code here and/or call default

	CProcessingCommand* curCmd;
	if ((l_refreshEventListTime + m_EventRefreshInterval) < time(NULL))
	{
		BOOL doDownloadEventList = true;
		for (int i=0;i < processingSteps.GetSize();i++)
		{
			curCmd = (CProcessingCommand*) processingSteps.GetAt(i);
			if (curCmd != NULL)
			{
				CString checkCmd;
				curCmd->CommandDataLookup("COMMAND",checkCmd);
				if (checkCmd == "1_GET_EVENTS")
				{
					doDownloadEventList = false;
					break;
				}
			}
		}
		if (doDownloadEventList)
		{
			if (m_CurrentProcessingCommand != NULL)
			{
				CString activeCmd;
				m_CurrentProcessingCommand->CommandDataLookup("COMMAND",activeCmd);
				if (activeCmd == "1_GET_EVENTS")
				{
					doDownloadEventList = false;
				}
			}
			if (doDownloadEventList)
			{
				l_refreshEventListTime = time(NULL);
				OnBnClickedDownloadEvents();
			}
		}
	}
	if ((l_downloadTicketInfoTime + m_TicketInfoDownloadInterval) < time(NULL))
	{
		BOOL doDownloadEventList = true;
		for (int i=0;i < processingSteps.GetSize();i++)
		{
			curCmd = (CProcessingCommand*) processingSteps.GetAt(i);
			if (curCmd != NULL)
			{
				CString checkCmd;
				curCmd->CommandDataLookup("COMMAND",checkCmd);
				if (checkCmd == "3_GET_TICKET_UPDATES")
				{
					doDownloadEventList = false;
					break;
				}
			}
		}
		if (doDownloadEventList)
		{
			// If there are events in the event list, other than "-1", then process them
			if (currentActiveEventList.GetLength() > 2)
			{
				if (m_CurrentProcessingCommand != NULL)
				{
					CString activeCmd;
					m_CurrentProcessingCommand->CommandDataLookup("COMMAND",activeCmd);
					if (activeCmd == "3_GET_TICKET_UPDATES")
					{
						doDownloadEventList = false;
					}
					if (activeCmd == "4_SEND_TICKET_UPDATE_RESPONSE")
					{
						doDownloadEventList = false;
					}
				}
				if (doDownloadEventList)
				{
						l_downloadTicketInfoTime = time(NULL);
						CProcessingCommand* theCmd = new CProcessingCommand();
						theCmd->theCommandData.SetAt("COMMAND","3_GET_TICKET_UPDATES");
						this->processingSteps.Add((void*)theCmd);
				}
			}
		}
	}

	if ((l_uploadTicketInfoTime + m_TicketInfoUploadInterval) < time(NULL))
	{
		BOOL doOperation = true;
		if (doOperation)
		{
			if (m_UploadProcessingCommand != NULL)
			{
				CString activeCmd;
				m_UploadProcessingCommand->CommandDataLookup("COMMAND",activeCmd);
				if (activeCmd == "2_UPLOAD_TICKET_EXPORTS")
				{
					doOperation = false;
				}
			}

			if (doOperation)
			{
				l_uploadTicketInfoTime = time(NULL);
				CString AllowUpload;
				propertyLookup("ENABLE_UPLOAD_PROCESSING", AllowUpload);
				if (AllowUpload == "yes")
				{
					CProcessingCommand* theCmd = new CProcessingCommand();
					theCmd->theCommandData.SetAt("COMMAND","2_UPLOAD_TICKET_EXPORTS");
					m_UploadProcessingCommand = theCmd;
				}
				else
				{
					int numPending = FindUploadFiles(NULL);
					CWnd* theCheckbox = (CWnd*) GetDlgItem(IDC_ENABLE_UPLOAD);
					CString theNewText;
					theNewText.Format("Enable File Upload (%d)", numPending);
					theCheckbox->SetWindowText(theNewText);
				}
			}
		}
   }



	if (i_httpErrorCount > 5)
	{
		bool writeaMessage = false;
		if (i_httpErrorCount != i_LastErrorWriteCount)
		{
			i_LastErrorWriteCount = i_httpErrorCount;
			if (i_httpErrorCount > 10)
			{
				if ((i_httpErrorCount%50) == 0)
				{
					writeaMessage = true;
				}
			}
		}
		else if (i_httpErrorCount > 10000)
		{
			i_httpErrorCount = 500;
		}

		if (writeaMessage)
		{
			CString serverName;

			CString locErrorMsg;
			locErrorMsg.Format("%s\n%s:ErrorCount:%d",
				"The system is currently having trouble communicating with the remote server.",
				"Please check your network connectivity and retry your request.",i_httpErrorCount);

			CXUtils::WriteToTheLogFile(locErrorMsg);
			theEmailMessageList.AddTail(locErrorMsg);
		}
	}

//	if (b_AllProcessingIsComplete)
//	{
//		ForceCancel();
//		return;
//	}

//	if (CEncoderDlg::processingSteps.GetCount() == 0)
//	{
//		CProcessingCommand* theCommand = new CProcessingCommand();
//		theCommand->theCommandData.SetAt("COMMAND","2_UPLOAD_TICKET_EXPORTS");
//		CEncoderDlg::processingSteps.Add(theCommand);
//	}

	DoTimerEvents();

	if ((lastStatusDump+3600) < time(NULL))
	{
		CTimeSpan theTimeSpan;
		theTimeSpan = CTime::GetCurrentTime() - theStartupTime;
		CString OutMsg;
		OutMsg.Format("UPTIME:%s", theTimeSpan.Format("%D Days, %H Hours, %M Minutes, %S Seconds"));
		CXUtils::WriteToTheLogFile(OutMsg);
		lastStatusDump = time(NULL);
	}

	SendAnyEmails(false);

	SetTimer(500,1500,NULL);

	CDialog::OnTimer(nIDEvent);
}

BOOL CEncoderDlg::LoadConfigProperties(CEncoderDlg* theParentDlg)
{
	if (ConfigPropertiesLoaded)
		return TRUE;
	
	CString tmpValue;

	ConfigFileManager theConfigFile;
	BOOL ConfigFileStatus = theConfigFile.OpenAtExeDirectory("AlvaradoMgr.properties",
							FALSE,
							FALSE);

	if (ConfigFileStatus)
	{
		CString theMessage = "Reading values from AlvaradoMgr.properties configuration file.";
		CXUtils::WriteToTheLogFile(theMessage, true);
		if (theParentDlg != NULL) theParentDlg->AddMessageBoxString(theMessage);

//		theConfigFile.GetValuesInSection("ADMIN_IPS", theAdminIPs);
//
//==========================================================================================
//		Web Server Vars
//==========================================================================================
//
//		propertySet("PORT", theConfigFile.GetValue("WEB_SERVER","PORT","80"));
//		propertySet("MAX_CONNECTIONS", theConfigFile.GetValue("WEB_SERVER","MAX_CONNECTIONS","10"));
//		propertySetMakeLower("SERVER_NAME", theConfigFile.GetValue("WEB_SERVER","SERVER_NAME","ENCODER"));
//		propertySetMakeLower("SERVER_ROOT_PATH", theConfigFile.GetValue("WEB_SERVER","SERVER_ROOT_PATH","C:\\TEMP"));

//==========================================================================================
//		Server Connection Vars
//==========================================================================================
//
		propertySet("SERVER_URL", theConfigFile.GetValue("SERVER","SERVER_URL"));
		propertySet("FTP_USERNAME", theConfigFile.GetValue("SERVER","FTP_USERNAME"));
		propertySet("FTP_PASSWORD", theConfigFile.GetValue("SERVER","FTP_PASSWORD"));
		propertySet("FTP_SERVER", theConfigFile.GetValue("SERVER","FTP_SERVER"));
		propertySet("FTP_SERVER_PATH", theConfigFile.GetValue("SERVER","FTP_SERVER_PATH"));
		propertySet("ERROR_EMAIL_SERVER", theConfigFile.GetValue("SERVER","ERROR_EMAIL_SERVER"));
		propertySet("ERROR_EMAIL_FROM", theConfigFile.GetValue("SERVER","ERROR_EMAIL_FROM"));
		propertySet("ERROR_EMAIL_TO", theConfigFile.GetValue("SERVER","ERROR_EMAIL_TO"));
		propertySet("SERVER_KEY_ID", theConfigFile.GetValue("SERVER","SERVER_KEY_ID"));
		propertySetMakeLower("SERVER_URL_APPEND_DATA", theConfigFile.GetValue("SERVER","SERVER_URL_APPEND_DATA"));
		propertySetMakeLower("ENABLE_UPLOAD_PROCESSING", theConfigFile.GetValue("SERVER","ENABLE_UPLOAD_PROCESSING"));
		propertySetMakeLower("LOCAL_EXPORT_FILE_MASK", theConfigFile.GetValue("SERVER","LOCAL_EXPORT_FILE_MASK"));
		
		propertySetMakeLower("PASSIVE_FTP", theConfigFile.GetValue("SERVER","PASSIVE_FTP"));
		propertySet("PROXY_USERNAME", theConfigFile.GetValue("SERVER","PROXY_USERNAME"));
		propertySet("PROXY_PASSWORD", theConfigFile.GetValue("SERVER","PROXY_PASSWORD"));

//==========================================================================================
//		Audio/Video Encoder Vars
//==========================================================================================
//
//		CXUtils::TrimOffEndChar(theConfigFile.GetValue("ENCODER","ARCHIVE_DIRECTORY"),
//								'\\',
//								tmpValue);
//		propertySet("ARCHIVE_DIRECTORY", tmpValue);
//
//		propertySet("AUDIO_PROFILE_TO_USE", theConfigFile.GetValue("ENCODER","AUDIO_PROFILE_TO_USE"));
//		propertySet("VIDEO_PROFILE_TO_USE", theConfigFile.GetValue("ENCODER","VIDEO_PROFILE_TO_USE"));
//
//		propertySet("AUDIO_DEVICE_NAME", theConfigFile.GetValue("ENCODER","AUDIO_DEVICE_NAME"));
//		propertySet("VIDEO_DEVICE_NAME", theConfigFile.GetValue("ENCODER","VIDEO_DEVICE_NAME"));
//		propertySet("ENCODER_SERVER_ID", theConfigFile.GetValue("ENCODER","ENCODER_SERVER_ID"));		
//		propertySetMakeLower("MAX_CONNECTIONS", theConfigFile.GetValue("WEB_SERVER","MAX_CONNECTIONS"));
		

//==========================================================================================
//		ODBC Connection Vars
//==========================================================================================
//
		propertySet("ODBC_DSN_NAME", theConfigFile.GetValue("SERVER","ODBC_DSN_NAME"));
		propertySetMakeLower("LOCAL_DB_BASE_DIR", theConfigFile.GetValue("SERVER","LOCAL_DB_BASE_DIR"));

		CXUtils::TrimOffEndChar(theConfigFile.GetValue("SERVER","LOCAL_EXPORT_FILE_BASE_DIR"),'\\',tmpValue);
		propertySet("LOCAL_EXPORT_FILE_BASE_DIR", tmpValue);
		CXUtils::TrimOffEndChar(theConfigFile.GetValue("SERVER","LOCAL_IMPORT_FILE_BASE_DIR"),'\\',tmpValue);
		propertySet("LOCAL_IMPORT_FILE_BASE_DIR", tmpValue);
		CXUtils::TrimOffEndChar(theConfigFile.GetValue("SERVER","LOCAL_WORKING_DIR"),'\\',tmpValue);
		propertySet("LOCAL_WORKING_DIR", tmpValue);

		propertySetMakeLower("KEEP_IMPORT_FILES", theConfigFile.GetValue("SERVER","KEEP_IMPORT_FILES"));
		propertySetMakeLower("LOCAL_IMPORT_FILE_EXTENSION", theConfigFile.GetValue("SERVER","LOCAL_IMPORT_FILE_EXTENSION"));
		propertySetAsInteger("TKT_DOWNLOAD_UPDATES_INTERVAL", theConfigFile.GetValue("SERVER","TKT_DOWNLOAD_UPDATES_INTERVAL"));
		
		ConfigPropertiesLoaded = TRUE;

		propertyLookup("TKT_DOWNLOAD_UPDATES_INTERVAL",tmpValue);
		m_TicketInfoDownloadInterval = atoi(tmpValue);
	}
	else
	{
		CString theMessage = "Error:  The file AlvaradoMgr.properties configuration file is missing.";
		CXUtils::WriteToTheLogFile(theMessage);
	}
	return ConfigFileStatus;
}

void CEncoderDlg::propertyLookup(CString theKey, CString& theValue)
{
	CXUtils::SafeLookup(thePropertiesData, theKey, theValue);
}
void CEncoderDlg::propertySet(const char* theKey, CString& theValue)
{
	thePropertiesData.SetAt(theKey, theValue);
}
void CEncoderDlg::propertySetMakeLower(const char* theKey, CString& theValue)
{
	theValue.MakeLower();
	thePropertiesData.SetAt(theKey, theValue);
}
void CEncoderDlg::propertySetAsInteger(const char* theKey, CString& theValue)
{
	if (theValue.GetLength() == 0)
		theValue = "30";
	else
	{
		int theIntValue = atoi(theValue);
		if (theIntValue <=0)
			theValue = "30";
		else if (theIntValue > 600)
			theValue = "600";
	}
	thePropertiesData.SetAt(theKey, theValue);
}

BOOL CEncoderDlg::doEventCheck(CMapStringToString &Result, int &ResponseStatus)
{
	CString theCommand;
	BOOL RunStatus = TRUE;
	HttpRetrieval::ThreadStruct* TheThreadStruct;
	CString locErrorMsg;
	CString currentCommand;
	CString ErrorBufData;

	TheThreadStruct = theHttpRetrieval.CheckForThreadCompleteByType(HttpRetrieval::INFO_DOWNLOAD);

	if (TheThreadStruct != NULL)
	{
		ErrorBufData.Append(TheThreadStruct->TheType + ":" + TheThreadStruct->TheRequestString + ":");
		m_StarControl.SetWindowText("");
		TRACE("REQUEST RESPONSE RECEIVED\n");

		if (TheThreadStruct->NetworkError != HttpRetrieval::GOOD_STATUS)
		{
			RunStatus = false;
		}
		else
		{
			CString LocalLastError = TheThreadStruct->TheErrorMsg;
			if ((TheThreadStruct->TheData != NULL) && (LocalLastError.GetLength() == 0))
			{
				CPtrArray theReqData;
				CString theBaseURL;
				CString remoteStatus;

				if (m_CurrentProcessingCommand->CommandDataLookup("COMMAND", theCommand) != 0)
				{
					if (HttpRetrieval::LoadResponseData(TheThreadStruct, Result, ResponseStatus) == HttpRetrieval::ERROR_STATUS)
					{
						RunStatus = false;
					}
					else
					{
						CString remoteStatus;
						CXUtils::SafeLookup(Result,"DB_STATUS",remoteStatus);

						if (remoteStatus == "GOOD")
						{
							i_httpErrorCount = 0;
							i_RetryCount = 0;
							ProcessRetrievals(Result);
						}
						else
						{
							RunStatus = false;
						}
					}
					TheThreadStruct = NULL;		// If we got to here, the thread struct has been destroyed!!!
				}
				else
				{
					RunStatus = false;
				}
			//				RunStatus = CXUtils::WriteFileToDisk(
			//					"c:\\slides\\xfile.dat", 
			//					TheThreadStruct->TheData, 
			//					TheThreadStruct->TheDataSize);
			}
			else
			{
				RunStatus = false;

			}
		}
	}

	TheThreadStruct = theHttpRetrieval.CheckForThreadCompleteByType(HttpRetrieval::FTP_UPLOAD);

	if (TheThreadStruct != NULL)
	{
		ErrorBufData.Append("FTP:" + TheThreadStruct->ftpLocalFilename + ":" + TheThreadStruct->ftpUsername + ":");
		m_StarControl.SetWindowText("");
		TRACE("REQUEST RESPONSE RECEIVED\n");

		if (TheThreadStruct->NetworkError != HttpRetrieval::GOOD_STATUS)
		{
			RunStatus = false;
		}
		else
		{
			CString LocalLastError = TheThreadStruct->TheErrorMsg;
			if (LocalLastError.GetLength() == 0)
			{
				CPtrArray theReqData;
				CString theBaseURL;
				CString remoteStatus;

				HttpRetrieval::CleanupThreadStruct(TheThreadStruct);
				TheThreadStruct = NULL;

				if (m_UploadProcessingCommand)
				{
					CString theMessage;
					m_UploadProcessingCommand->CommandDataLookup("COMMAND", theCommand);

					if (theCommand == "2_UPLOAD_TICKET_EXPORTS")
					{
						CString curCount;
						m_UploadProcessingCommand->CommandDataLookup("PENDING_UPLOAD_COUNT", curCount);
						int intCount = atoi(curCount);
						if (intCount > 0) intCount--;

						CWnd* theCheckbox = (CWnd*) GetDlgItem(IDC_ENABLE_UPLOAD);
						CString theNewText;
						theNewText.Format("Enable File Upload (%d)", intCount);
						theCheckbox->SetWindowText(theNewText);

						CString delfilename;
						CString remotefilename;
						m_UploadProcessingCommand->CommandDataLookup("FTP_LOCAL_UPLOAD_FILENAME", delfilename);
						m_UploadProcessingCommand->CommandDataLookup("FTP_REMOTE_UPLOAD_FILENAME", remotefilename);
						DeleteFile(delfilename);

						theMessage.Format("FTP Upload: Export File %s: COMPLETE", remotefilename);
					}
					else
					{
						theMessage.Format("FTP Upload: %s: COMPLETE", theCommand);
					}
					CXUtils::WriteToTheLogFile(theMessage, false);
					AddMessageBoxString(theMessage);

					int numPending = FindUploadFiles(m_UploadProcessingCommand);
					if (numPending <= 0)
					{
						delete m_UploadProcessingCommand;
						m_UploadProcessingCommand = NULL;
						m_ActivityChar.Empty();
					}
					else
					{
						m_UploadProcessingCommand->theCommandData.RemoveAll();
						m_UploadProcessingCommand->theCommandData.SetAt("COMMAND","2_UPLOAD_TICKET_EXPORTS");
					}
				}
			}
			else
			{
				RunStatus = false;

			}
		}
	}

	TheThreadStruct = theHttpRetrieval.CheckForThreadCompleteByType(HttpRetrieval::TKT_INFO_DOWNLOAD);

	if (TheThreadStruct != NULL)
	{
		m_StarControl.SetWindowText("");
		TRACE("REQUEST RESPONSE RECEIVED\n");

		if (TheThreadStruct->NetworkError != HttpRetrieval::GOOD_STATUS)
		{
			RunStatus = false;
		}
		else
		{
			CString LocalLastError = TheThreadStruct->TheErrorMsg;
			if ((TheThreadStruct->TheData != NULL) && (LocalLastError.GetLength() == 0))
			{
				CPtrArray theReqData;
				CString theBaseURL;
				CString remoteStatus;

				if (m_CurrentProcessingCommand->CommandDataLookup("COMMAND", theCommand) != 0)
				{
					CString tmpLoc;
					propertyLookup("LOCAL_WORKING_DIR",tmpLoc);
					if (tmpLoc.GetLength() > 0)
					{
						CFileStatus theFileStatus;
						if (CFile::GetStatus(tmpLoc, theFileStatus) == false)
						{
							CreateDirectory(tmpLoc, NULL);
						}
					}
					CString tmpExt;
					propertyLookup("LOCAL_IMPORT_FILE_EXTENSION",tmpExt);
					if (tmpExt.GetAt(0) == '.') tmpExt = tmpExt.Right(tmpExt.GetLength()-1);

					CString theInputFile;
					CString fileNameOnly;
					fileNameOnly.Format("NEWTKTDATA_%d.%s", time(NULL), tmpExt);
					theInputFile.Format("%s\\%s", tmpLoc, fileNameOnly);
					Result.SetAt("ALVMGR_TKT_UPDATE_FILE",theInputFile);
					Result.SetAt("ALVMGR_TKT_MOVE_FILENAME_ONLY",fileNameOnly);
					m_CurrentProcessingCommand->theCommandData.SetAt("ALVMGR_TKT_UPDATE_FILE",theInputFile);


					CString EndOfValidDataString = "<VALID_TICKET_FILE/>";
					char* thePosOfMark;
					thePosOfMark = strstr((const char*)TheThreadStruct->TheData,EndOfValidDataString);

					BOOL isValidFile = FALSE;

					if (thePosOfMark != NULL)
					{
						LONG_PTR startLoc = (LONG_PTR)TheThreadStruct->TheData;
						LONG_PTR markerLoc = (LONG_PTR)thePosOfMark;
						LONG_PTR theLengthOfTicketData = markerLoc - startLoc;

						RunStatus = CXUtils::WriteFileToDisk(
							theInputFile, 
							TheThreadStruct->TheData, 
							theLengthOfTicketData);

						long startOfWorkingData = theLengthOfTicketData;
						startOfWorkingData++;
						startOfWorkingData += EndOfValidDataString.GetLength();

						long bytesLeft = TheThreadStruct->TheDataSize;
						bytesLeft -= (long)startOfWorkingData;

						BYTE* BufferPtr = &TheThreadStruct->TheData[startOfWorkingData];
						memcpy(TheThreadStruct->TheData, BufferPtr, bytesLeft);
						TheThreadStruct->TheDataSize = bytesLeft;
						TheThreadStruct->CurrentByteCount = bytesLeft;
						isValidFile = TRUE;

/*						CString removedPart;
						CFile readFile;
						BYTE buffer[8193];
						DWORD dwRead;

						if (readFile.Open(theInputFile,CFile::modeReadWrite))
						{
							CString theCStringBuffer;
							int tagPosition = -1;
							do
							{
								dwRead = readFile.Read(buffer, 8192);
								if (dwRead > 0)
								{
									buffer[dwRead] = 0;
									theCStringBuffer = buffer;
									int tagPosition = theCStringBuffer.Find(EndOfValidDataString);
									if (tagPosition >= 0)
									{
										long endPos = readFile.GetPosition();
										endPos = endPos - dwRead;
										endPos = endPos + tagPosition;
										readFile.SetLength(endPos);

										endPos++;
										endPos += EndOfValidDataString.GetLength();
										long amountRemaining = TheThreadStruct->TheDataSize;
										amountRemaining -= (long)endPos;
										long bytesLeft = amountRemaining;
										BYTE* BufferPtr = &TheThreadStruct->TheData[endPos];
										memcpy(TheThreadStruct->TheData, BufferPtr, bytesLeft);
										TheThreadStruct->TheDataSize = bytesLeft;
										TheThreadStruct->CurrentByteCount = bytesLeft;
										isValidFile = TRUE;
										dwRead = 0;	// to make sure we get out!!
										break;
									}
								}
							}
							while (dwRead > 0);
	
							readFile.Close();
						}
*/
					}

					if (HttpRetrieval::LoadResponseData(TheThreadStruct, Result, ResponseStatus) == HttpRetrieval::ERROR_STATUS)
					{
						RunStatus = false;
					}
					else
					{
						CString remoteStatus;
						CXUtils::SafeLookup(Result,"DB_STATUS",remoteStatus);

						if (remoteStatus == "GOOD")
						{
							i_httpErrorCount = 0;
							i_RetryCount = 0;
							ProcessRetrievals(Result);
						}
						else
						{
							RunStatus = false;
						}
					}
					TheThreadStruct = NULL;		// If we got to here, the thread struct has been destroyed!!!
				}
				else
				{
					RunStatus = false;
				}
			//				RunStatus = CXUtils::WriteFileToDisk(
			//					"c:\\slides\\xfile.dat", 
			//					TheThreadStruct->TheData, 
			//					TheThreadStruct->TheDataSize);
			}
			else
			{
				RunStatus = false;

			}
		}
	}


	if (RunStatus == FALSE)
	{
		i_httpErrorCount++;
		CString serverName;
		CString originalRequest;
		CString remoteError;
		CString ThreadErrorMsg;
		int	ThreadNetworkError = -1;;
		int ThreadErrorNumber = -1;
		
		CString RemoteServerKeyID;
		propertyLookup("SERVER_KEY_ID", RemoteServerKeyID);


		if (TheThreadStruct != NULL)
		{
			ThreadErrorMsg = TheThreadStruct->TheErrorMsg;
			ThreadNetworkError = TheThreadStruct->NetworkError;
			ThreadErrorNumber = TheThreadStruct->ErrorNumber;
		}
		CXUtils::SafeLookup(Result,"DB_ERROR_MESSAGE",remoteError);
		CXUtils::SafeLookup(Result,"ORIGINAL_REQUEST",originalRequest);
		locErrorMsg.Format("Server:%s\n%s\n%s\nError Message:%s\nNetwork Error:%d\nErrorNumber:%d\nRemoteError:%s\n",
			"ALVARADO SERVER:" + RemoteServerKeyID,
			" Error sending message to server: " + ErrorBufData,
			originalRequest,
			ThreadErrorMsg,
			ThreadNetworkError,
			ThreadErrorNumber,
			remoteError);

		CXUtils::WriteToTheLogFile(locErrorMsg);
		theEmailMessageList.AddTail(locErrorMsg);
		i_RetryCount++;

		HttpRetrieval::CleanupThreadStruct(TheThreadStruct);
		TheThreadStruct = NULL;

		m_CurrentProcessingCommand->CommandDataLookup("COMMAND", theCommand);
		doRetrievalErrorHandling(theCommand, Result);
	}

	return RunStatus;
}


void CEncoderDlg::OnReloadProperties() 
{
	// TODO: Add your control notification handler code here
	ConfigPropertiesLoaded = FALSE;
	LoadConfigProperties(this);
	
}

void CEncoderDlg::AddMessageBoxString(CString theMsg)
{
	CString OutMsg;
	CTime TheTime = CTime::GetCurrentTime();
	OutMsg = TheTime.Format("%d%b%y::%H:%M:%S ");
	OutMsg += theMsg;

	int curValue = m_MessageListBox.AddString(OutMsg);
	m_MessageListBox.SetCurSel(curValue);
	m_MessageListBox.SetCurSel(-1);

	while (m_MessageListBox.GetCount() > 50)
	{
		m_MessageListBox.DeleteString(0);
	}


}

void CEncoderDlg::SendAnyEmails(BOOL force)
{
	if ((lastErrorEmailSent < (time(NULL) - 300)) || force || (theEmailMessageList.GetCount() > 250))
	{
		if ((theEmailMessageList.GetCount() > 40) || force)
		{
			lastErrorEmailSent = time(NULL);
			CString mailError;
			CString mailServer;
			CString fromString;
			CString toString;
			CString serverName;
			CString subject;
			
			propertyLookup("ENCODER_SERVER_ID", serverName);
			propertyLookup("ERROR_EMAIL_SERVER", mailServer);
			propertyLookup("ERROR_EMAIL_FROM", fromString);
			propertyLookup("ERROR_EMAIL_TO", toString);
			

			CTime TheTime = CTime::GetCurrentTime();
			CString RemoteServerKeyID;
			propertyLookup("SERVER_KEY_ID", RemoteServerKeyID);

			subject.Format("AlvaradoMgr Report: %s Server: %s",
				TheTime.Format("%d%b%y::%H:%M:%S "), 
				RemoteServerKeyID);
			
			CXUtils::DoTheEmailSend(mailServer, fromString, toString, subject, theEmailMessageList, mailError);
			theEmailMessageList.RemoveAll();
			if (mailError.IsEmpty() == FALSE)
				CXUtils::WriteToTheLogFile("Mail Message Send Failed:" + mailError);
		}
	}
}
BOOL CEncoderDlg::SendErrorEmail(CString theErrorData)
{
	BOOL RetStatus = TRUE;
	CString sendMailErrorMsg;
	RetStatus = CEncoderDlg::LoadConfigProperties(NULL);

	if (RetStatus)
	{
		if (theErrorData.GetLength() > 0)
		{
//			lastErrorEmailSent = time(NULL);
			CString mailError;
			CString mailServer;
			CString fromString;
			CString toString;
			CString serverName;
			CString subject;
			
			propertyLookup("ENCODER_SERVER_ID", serverName);
			propertyLookup("ERROR_EMAIL_SERVER", mailServer);
			propertyLookup("ERROR_EMAIL_FROM", fromString);
			propertyLookup("ERROR_EMAIL_TO", toString);
			

			CTime TheTime = CTime::GetCurrentTime();

			subject.Format("AlvaradoMgr Report: %s",TheTime.Format("%d%b%y::%H:%M:%S "));
			CStringList tmpEmailList;
			tmpEmailList.AddTail(theErrorData);
			
			CXUtils::DoTheEmailSend(mailServer, fromString, toString, subject, tmpEmailList, mailError);
			tmpEmailList.RemoveAll();
			if (mailError.IsEmpty() == FALSE)
				CXUtils::WriteToTheLogFile("Mail Message Send Failed:" + mailError);
		}

	}
	else
	{
		CString theMessage = "Error Opening Congiguration file before mail send. ";
		CXUtils::WriteToTheLogFile(theMessage);
	}
	return RetStatus;
}

void CEncoderDlg::ForceCancel()
{
	OnCancel();
}

void CEncoderDlg::OnCheck1() 
{
	// TODO: Add your control notification handler code here
	UpdateData();

	if (m_AlwaysOnTop)
	{
		SetWindowPos(&wndTopMost,0,0,0,0, SWP_NOMOVE | SWP_NOSIZE);
	}
	else
	{
		SetWindowPos(&wndNoTopMost,0,0,0,0, SWP_NOMOVE | SWP_NOSIZE);
	}
}



int CEncoderDlg::LoadTableData(CString DSN_Name, CString DBPath, CString query)
{
	CMyDatabase db;
	CString connectString;
	CString connectStringFormat ="DSN=%s;UID=;PWD=;SourceDB=%s;";
	connectStringFormat += "SourceType=DBF;Exclusive=No;BackgroundFetch=No;";
	connectStringFormat += "Collate=Machine;Null=Yes;Deleted=Yes;";
	connectString.Format(connectStringFormat, DSN_Name, DBPath);

	db.OpenEx(connectString, CDatabase::noOdbcDialog);

	CMyRecordset rs(&db);
//	CString query("SELECT NUMBER,units,price1,roysup,isbn FROM [stock]") ;

	rs.MyOpen(query, AFX_DB_USE_DEFAULT_TYPE );

	CString number, units, price1, roysup, isbn;
	int count = 0;
	while(!rs.IsEOF())
	{
		count++;
		DWORD x = rs.GetRowsFetched();
		rs.GetFieldValue((short)0,number);
		number.Trim();
		rs.GetFieldValue((short)1,units);
		units.Trim();
		rs.GetFieldValue((short)2,price1);
		price1.Trim();
		rs.GetFieldValue((short)3,roysup);
		roysup.Trim();
		rs.GetFieldValue((short)4,isbn);
		isbn.Trim();
		//  rs.GetFieldValue((short)1,varVal2);
		//  rs.GetFieldValue((short)2,varVal3);
		CString strUpdate;
		//  int intCurRecPK = varVal1.m_lVal;
		//  strUpdate.Format("Update [FileName] SET STATUS =1 Where PKVAL=%d",intCurRecPK);
		//  db.ExecuteSQL(strUpdate);
		rs.MoveNext();
	}

	TRACE("Count: %d\n", count);

//	Cstock *theStock = new Cstock(thedb);
//	thedb->OpenEx(theStock->GetDefaultConnect(),0);
//	theStock->Open(-1, theStock->GetDefaultSQL());
//	theStock->Open(CRecordset::snapshot, theStock->GetDefaultSQL());
  
	/*
// loop through the recordset by rowsets
while( !theStock->IsEOF( ) )
{
    for( int rowCount = 0; 
         rowCount < (int)theStock->GetRowsFetched( );
         rowCount++ )
    {
       // do something
    }

   theStock->MoveNext( );
}
theStock->Close();
*/


//	CString strConnection = _T("Driver={Microsoft Visual Foxpro Driver};UID=;"
//    "SourceType=DBF;SourceDB=C:\\MOMWIN\\oc\\stock.dbc;Exclusive=No");

//	CString strConnection = _T("DSN=Visual FoxPro Tables;UID=;PWD=;SourceDB=c:\\momwin\\oc;SourceType=DBF;Exclusive=No;BackgroundFetch=Yes;Collate=Machine;Null=Yes;Deleted=Yes;");

//	CString strConnection = _T("Driver={Microsoft Visual Foxpro Driver};UID=;"
//		"SourceType=DBF;SourceDB=C:\\MOMWIN\\oc\\;Exclusive=No");

//	BOOL stat = thedb.OpenEx(strConnection, 0);
//	BOOL isO = thedb.IsOpen();
//	CString strCmd = "select * from stock";





	/*
TRY
{
//   thedb.ExecuteSQL( strCmd );
//   CRecordset rs(&thedb);
//   rs.Open(AFX_DB_USE_DEFAULT_TYPE, strCmd, CRecordset::none);
//   long c = rs.GetRowsFetched();

   // Set the rowset size
//	rs.SetRowsetSize( 5 );

// Open the recordset
//rs.Open( CRecordset::dynaset, strCmd,
//         CRecordset::useMultiRowFetch );

// loop through the recordset by rowsets
while( !rs.IsEOF( ) )
{
    for( int rowCount = 0; 
         rowCount < (int)rs.GetRowsFetched( );
         rowCount++ )
    {
       // do something
    }

   rs.MoveNext( );
}

rs.Close( );
}

CATCH(CDBException, e)
{
   // The error code is in e->m_nRetCode
}

END_CATCH
*/

	return 0;
}


int CEncoderDlg::ProcessRetrievals(CMapStringToString& Result)
{
	CString theMessage;
	CString theCommand;

	m_CurrentProcessingCommand->CommandDataLookup("COMMAND", theCommand);
	long executionTimeDiff = -1;
	CString tmpTimeVal;

	m_CurrentProcessingCommand->CommandDataLookup("COMMAND_START_TIME", tmpTimeVal);
	if (tmpTimeVal.GetLength() > 0)
	{
		executionTimeDiff = time(NULL) - atol(tmpTimeVal);
	}

	if (theCommand == "1_GET_EVENTS")
	{
		CListCtrl* theCtrl;
		theCtrl = (CListCtrl*)GetDlgItem(IDC_EVENT_LIST);
		int CurRow = 0;
		int curPos = 0;

		theCtrl->DeleteAllItems();

		CString locSeparator;
		CString numEvents;
		CString timezone;
		CXUtils::SafeLookup(Result,"EVT_SEPARATOR",locSeparator);
		if (locSeparator.IsEmpty()) locSeparator = ",";
		CXUtils::SafeLookup(Result,"EVT_NUM_EVENTS",numEvents);
		CXUtils::SafeLookup(Result,"EVT_TZ",timezone);

//		LVCOLUMN pColumn ;
//		theCtrl->GetColumn(3,&pColumn);
//		timezone.Format("Start Time (%s)",timezone);
//		pColumn.pszText = timezone;
//		theCtrl->SetColumn(3,&pColumn);

		int numEventsInt = atoi(numEvents);
		CStringArray theParams;
		CString tmpInput;
		CString part1;
		CString part2;
		CString lookupKey;

		currentActiveEventList = "-1";

		for (int i=0; i<numEventsInt; i++)
		{
			theParams.RemoveAll();
			lookupKey.Format("EVT%d",i);
			CXUtils::SafeLookup(Result,lookupKey,tmpInput);
			while (tmpInput.GetLength() > 0)
			{
				if (tmpInput.GetLength() <= 0)
					break;

				CXUtils::SplitCStringOnCString(tmpInput, locSeparator, part1, part2);
				theParams.Add(part1);
				tmpInput = part2;
			}	

			CMapStringToString theArgs;

			int j;
			CString TheElement;
			CString LocalKey;
			CString LocalValue;
			for (j=0; j<theParams.GetSize(); j++)
			{
				TheElement = theParams.ElementAt(j);
				CXUtils::SplitCStringOnChar(TheElement, '=',
					LocalKey, LocalValue);
				LocalKey.Trim();
				LocalValue.Trim();

				if (LocalKey.IsEmpty() == FALSE)
				{
					theArgs.SetAt(LocalKey.MakeUpper(), LocalValue);
				}
			}

			BOOL doInsert = true;
			curPos = 0;

			CString logMsg;
			CXUtils::SafeLookup(theArgs, "EVENT_CODE",LocalValue);
			int insPos = theCtrl->InsertItem(
				LVIF_TEXT|LVIF_STATE, CurRow, LocalValue, 
				0, LVIS_SELECTED,
				0, 0);
			theCtrl->SetItemData(insPos, CurRow);

			CXUtils::SafeLookup(theArgs, "EVENT_ID",LocalValue);
			curPos++;
			theCtrl->SetItemText(CurRow, curPos, LocalValue);
			logMsg.Append("Events Loaded: ID:" + LocalValue + ": ");
			currentActiveEventList += "," + LocalValue;			 // Build the event list to retrieve ticket updates

			CXUtils::SafeLookup(theArgs, "EVENT_NAME", LocalValue);
			curPos++;
			logMsg.Append("Name:" + LocalValue + ": ");
			theCtrl->SetItemText(CurRow, curPos, LocalValue);

			CXUtils::SafeLookup(theArgs, "EVENT_DATE",LocalValue);
			curPos++;
			logMsg.Append("Date:" + LocalValue + ": ");
			theCtrl->SetItemText(CurRow, curPos, LocalValue);

			CXUtils::SafeLookup(theArgs, "EVENT_NUM_SCANNED",LocalValue);
			curPos++;
			logMsg.Append("Num Scanned:" + LocalValue);
			theCtrl->SetItemText(CurRow, curPos, LocalValue);

			CurRow++;
			CXUtils::WriteToTheLogFile(logMsg, false);

		}

		//*****************************************************************************************
		// Now if there are any alvarado event files, create the file, and move to the import dir
		//*****************************************************************************************
		CString numAlvEvents;
		CXUtils::SafeLookup(Result,"ALVFILECOUNT",numAlvEvents);

		numEventsInt = atoi(numAlvEvents);
		CString tmpLoc;
		propertyLookup("LOCAL_WORKING_DIR",tmpLoc);
		if (tmpLoc.GetLength() > 0)
		{
			CFileStatus theFileStatus;
			if (CFile::GetStatus(tmpLoc, theFileStatus) == false)
			{
				CreateDirectory(tmpLoc, NULL);
			}
		}

		CString theEventID;
		CString theAlvaradoEventData;
		CString eventFilesExistEventList;
		CString eventFilesMoveFailedEventList;
		CString eventFilesCopiedEventList;

		for (int i=0; i<numEventsInt; i++)
		{
			lookupKey.Format("ALVFILE%d",i);
			tmpInput.Empty();
			theEventID.Empty();
			theAlvaradoEventData.Empty();

			CXUtils::SafeLookup(Result,lookupKey,tmpInput);
			CXUtils::SplitCStringOnCString(tmpInput, locSeparator, theEventID, theAlvaradoEventData);

			CString theInputFile;
			CString fileNameOnly;
			fileNameOnly.Format("EVENT_FILE_DATA_%s.%s", theEventID, "evt");
			theInputFile.Format("%s\\%s", tmpLoc, fileNameOnly);

			theAlvaradoEventData.Append(CRLF);
			LPTSTR p = theAlvaradoEventData.GetBuffer();
			CXUtils::WriteFileToDisk(theInputFile, (BYTE*)p, theAlvaradoEventData.GetLength());
			theAlvaradoEventData.ReleaseBuffer();
			
			CString destLocation;

			propertyLookup("LOCAL_IMPORT_FILE_BASE_DIR",destLocation);
			if (destLocation.Right(1) != "\\")
			{
				destLocation += "\\";
			}
			destLocation += fileNameOnly;

			BOOL FileExists = CXUtils::DoesFileExist(destLocation);

			//*****************************************************************************************
			//	If the event file already exists, dont copy it again
			//*****************************************************************************************
			if (FileExists == false)
			{
				BOOL mstat = MoveFileEx(theInputFile, destLocation, MOVEFILE_COPY_ALLOWED);
				DWORD val = GetLastError();
				if (mstat > 0)
				{
					if (eventFilesCopiedEventList.GetLength() > 0) eventFilesCopiedEventList.Append(";");
					CXUtils::WriteToTheLogFile(theAlvaradoEventData, false);
					eventFilesCopiedEventList.Append(theEventID);
				}
				else
				{
					if (eventFilesMoveFailedEventList.GetLength() > 0) eventFilesMoveFailedEventList.Append(";");
					eventFilesMoveFailedEventList.Append(theEventID);
				}
			}
			else
			{
				if (eventFilesExistEventList.GetLength() > 0) eventFilesExistEventList.Append(";");
				eventFilesExistEventList.Append(theEventID);
			}
			DeleteFile(theInputFile);
		}

		CString msg;
		if (eventFilesMoveFailedEventList.GetLength() > 0)
		{
			msg.Format("%s:%s","Event File Move for Import FAILED for these events:", eventFilesMoveFailedEventList);
			CXUtils::WriteToTheLogFile(msg, false);
		}
		if (eventFilesExistEventList.GetLength() > 0)
		{
			msg.Format("%s:%s","Event File for these events already exists:", eventFilesExistEventList);
			CXUtils::WriteToTheLogFile(msg, false);
		}
		if (eventFilesCopiedEventList.GetLength() > 0)
		{
			msg.Format("%s:%s","Event File Move for Import SUCCESSFUL for these events:", eventFilesCopiedEventList);
			CXUtils::WriteToTheLogFile(msg, false);
		}

		//**********************************************************************************
		// Update config values if passed from the server
		//**********************************************************************************

		CString tmpVal;

		CXUtils::SafeLookup(Result,"KEEP_IMPORT_FILES",tmpVal);
		if (tmpVal.GetLength() > 0) propertySetMakeLower("KEEP_IMPORT_FILES", tmpVal);

		CXUtils::SafeLookup(Result,"ENABLE_UPLOAD_PROCESSING",tmpVal);
		if (tmpVal.GetLength() > 0) propertySetMakeLower("ENABLE_UPLOAD_PROCESSING", tmpVal);
		propertyLookup("ENABLE_UPLOAD_PROCESSING", tmpVal);
		if (tmpVal == "yes") this->m_EnableUpload.SetCheck(BST_CHECKED);
		else this->m_EnableUpload.SetCheck(BST_UNCHECKED);

		CXUtils::SafeLookup(Result,"ERROR_EMAIL_SERVER",tmpVal);
		if (tmpVal.GetLength() > 0) propertySetMakeLower("ERROR_EMAIL_SERVER", tmpVal);
		CXUtils::SafeLookup(Result,"ERROR_EMAIL_TO",tmpVal);
		if (tmpVal.GetLength() > 0) propertySetMakeLower("ERROR_EMAIL_TO", tmpVal);
		CXUtils::SafeLookup(Result,"ERROR_EMAIL_FROM",tmpVal);
		if (tmpVal.GetLength() > 0) propertySetMakeLower("ERROR_EMAIL_FROM", tmpVal);

		CXUtils::SafeLookup(Result,"TKT_DOWNLOAD_UPDATES_INTERVAL",tmpVal);
		if (tmpVal.GetLength() > 0) 
		{
			propertySetAsInteger("TKT_DOWNLOAD_UPDATES_INTERVAL", tmpVal);
			propertyLookup("TKT_DOWNLOAD_UPDATES_INTERVAL",tmpVal);
			m_TicketInfoDownloadInterval = atoi(tmpVal);
		}

		//**********************************************************************************
		// END::: Update config values if passed from the server
		//**********************************************************************************

		m_CurrentProcessingCommand->CommandDataLookup("COMMAND_START_TIME", tmpTimeVal);
		if (tmpTimeVal.GetLength() > 0)
		{
			executionTimeDiff = time(NULL) - atol(tmpTimeVal);
		}

		theMessage.Format("Retrieval of current Events (%ds):: COMPLETE", executionTimeDiff);
		CXUtils::WriteToTheLogFile(theMessage, false);
		AddMessageBoxString(theMessage);




		//		m_CurrentEventListCtrl.SetColumnWidth(1,100);
		//		m_CurrentEventListCtrl.SetColumnWidth(2,100);
		//		m_CurrentEventListCtrl.SetColumnWidth(3,100);
		//		m_CurrentEventListCtrl.SetColumnWidth(4,100);
		delete m_CurrentProcessingCommand;
		m_CurrentProcessingCommand = NULL;
	}
	else if (theCommand == "2_UPLOAD_TICKET_EXPORTS")
	{
		// Nothing to do!
	}
	else if (theCommand == "3_GET_TICKET_UPDATES")
	{
		CString destFilename;
		CString moveFilename;
		CString batchNames;
		CString destLocation;
		CXUtils::SafeLookup(Result,"ALVMGR_TKT_UPDATE_FILE",moveFilename);
		CXUtils::SafeLookup(Result,"ALVMGR_TKT_MOVE_FILENAME_ONLY",destFilename);
		
		CXUtils::SafeLookup(Result,"BATCHES",batchNames);

		propertyLookup("LOCAL_IMPORT_FILE_BASE_DIR",destLocation);
		if (destLocation.Right(1) != "\\")
		{
			destLocation += "\\";
		}
		destLocation += destFilename;
		// if batches were received, send a message back to notify sever that batch was received
		if (batchNames.GetLength() > 0)	
		{
			CString tmpValue;
			propertyLookup("KEEP_IMPORT_FILES", tmpValue);
			if (tmpValue == "yes")
			{
				CopyFile(moveFilename, destLocation, false);
			}
			else
			{
				BOOL mstat = MoveFileEx(moveFilename, destLocation, MOVEFILE_COPY_ALLOWED);
				DWORD val = GetLastError();
				DeleteFile(moveFilename);
			}

			m_CurrentProcessingCommand->CommandDataLookup("COMMAND_START_TIME", tmpTimeVal);
			if (tmpTimeVal.GetLength() > 0)
			{
				executionTimeDiff = time(NULL) - atol(tmpTimeVal);
			}

			m_CurrentProcessingCommand->theCommandData.SetAt("COMMAND","4_SEND_TICKET_UPDATE_RESPONSE");
			m_CurrentProcessingCommand->theCommandData.SetAt("TICKET_BATCH_LIST",batchNames);
			theMessage.Format("3_GET_TICKET_UPDATES information (UPDATES MOVED)(%ds): COMPLETE", executionTimeDiff);
			AddMessageBoxString(theMessage);
			CXUtils::WriteToTheLogFile(theMessage, false);

			CString timeval;
			timeval.Format("%d",time(NULL));
			m_CurrentProcessingCommand->theCommandData.SetAt("COMMAND_START_TIME", timeval);
		}
		else
		{
			delete m_CurrentProcessingCommand;
			m_CurrentProcessingCommand = NULL;
			DeleteFile(moveFilename);
//			theMessage.Format("3_GET_TICKET_UPDATES information (NO BATCHES): COMPLETE");
//			CXUtils::WriteToTheLogFile(theMessage, false);
		}
		if (m_BatchRequestsSent%250 == 0 && m_BatchRequestsSent > 0)
		{
			theMessage.Format("3_GET_TICKET_UPDATES requested/received: %d; Current EventList: %s",
				m_BatchRequestsSent, currentActiveEventList);
			AddMessageBoxString(theMessage);
			CXUtils::WriteToTheLogFile(theMessage, false);
		}
	}
	else if (theCommand == "4_SEND_TICKET_UPDATE_RESPONSE")
	{
		CString moveFilename;
		CString batchNames;
		CString destLocation;
		CString rowsUpdated;
		CXUtils::SafeLookup(Result,"DB_ROWS_UPDATED",rowsUpdated);
		CString batchesReceived;
		m_CurrentProcessingCommand->CommandDataLookup("TICKET_BATCH_LIST", batchesReceived);
		CXUtils::SafeLookup(Result,"LOCAL_IMPORT_FILE_BASE_DIR",destLocation);

		m_CurrentProcessingCommand->CommandDataLookup("COMMAND_START_TIME", tmpTimeVal);
		if (tmpTimeVal.GetLength() > 0)
		{
			executionTimeDiff = time(NULL) - atol(tmpTimeVal);
		}
		theMessage.Format("4_SEND_TICKET_UPDATE_RESPONSE information (%ds): COMPLETE: %s(%s)",executionTimeDiff,batchesReceived,rowsUpdated);
		CXUtils::WriteToTheLogFile(theMessage, false);
		AddMessageBoxString(theMessage);

		delete m_CurrentProcessingCommand;
		m_CurrentProcessingCommand = NULL;
	}
/*
	else if (theCommand == "CONFIG")
	{
		theMessage.Format("Processing Response For Configuration Request");
		CXUtils::WriteToTheLogFile(theMessage, false);
		theEmailMessageList.AddTail(theMessage);

		ProcessConfigRetrieval(Result);
		theMessage.Format("Processing Response For Configuration Request:COMPLETE %s",CRLF);
		theEmailMessageList.AddTail(theMessage);

		theMessage.Format("Processing of Configuration data complete");

		AddMessageBoxString(theMessage);

	}
	else if (theCommand == "UPDATE_REMOTE_STATUS_INFORMATION")
	{
		theMessage.Format("Update of remote Status information: COMPLETE");
		CXUtils::WriteToTheLogFile(theMessage, false);
		theEmailMessageList.AddTail(theMessage);
		AddMessageBoxString(theMessage);
		theCommand.Empty();
	}
*/
	else
	{
	}
	m_ActivityChar = "";

	
	return 0;
}

int CEncoderDlg::ProcessConfigRetrieval(CMapStringToString& Result)
{
	CString tmpValue;
	CXUtils::SafeLookup(Result,"DB_ERROR_EMAIL_TO", tmpValue);
	if (tmpValue.GetLength() > 0) propertySet("ERROR_EMAIL_TO", tmpValue);

	CXUtils::SafeLookup(Result,"DB_ERROR_EMAIL_SERVER", tmpValue); 
	if (tmpValue.GetLength() > 0) propertySet("ERROR_EMAIL_SERVER", tmpValue);

	CXUtils::SafeLookup(Result,"DB_ERROR_EMAIL_FROM", tmpValue);
	if (tmpValue.GetLength() > 0) propertySet("ERROR_EMAIL_FROM", tmpValue);

	CString theDBDirs;
	CXUtils::SafeLookup(Result,"CMS_DIR_NAMES", theDBDirs);
	int startPos = 0;
	int endPos = 0;
	BOOL Done = false;
	while (Done == false)
	{
		endPos = theDBDirs.Find(":",startPos);

		if (endPos > 0 || theDBDirs.GetLength() > 0)
		{
			if (endPos < 0)
			{
				endPos = theDBDirs.GetLength();
			}
			tmpValue = theDBDirs.Left(endPos);
			startPos = endPos + 1;
			theDBDirs = theDBDirs.Right(theDBDirs.GetLength() - startPos);
			theDbDirectories.Add(tmpValue);
		}
		else
		{
			Done = true;
		}
	}
	m_ConfigDataRetrieved = true;
//	theCommand.Empty();

	return 0;
}
int CEncoderDlg::ProcessStockRetrieval(CMapStringToString& Result)
{
	CString theMessage;
	CMapStringToString theRemoteData;
	CMapStringToString allRemotePartNumbers;
	CString rowCount;

	CXUtils::SafeLookup(Result,"ROWCOUNT", rowCount);
	if (rowCount.GetLength() == 0)
		rowCount = "0";
	int theRowCount = atoi(rowCount);

	CString tmpValue;
	CString tmpKey;

	// *** The Data is stored in the input array as COLUMNNAME_COLUMNINDEX starting at index 0 (e.g. INV_NUMBER_0)

	CMyDatabase* theDB = NULL;
	CString currentSchool = "a;sdfhpawieyrfp asdpfyawper f";

	CString partNumber;
	CString partSize;
	CString queryPartNumber;

	for (int i=0; i< theRowCount; i++)
	{
		theRemoteData.RemoveAll();
		POSITION pos;
		CString curKey;
		CString curValue;
		CString searchString;
		searchString.Format("_%d",i);
		int searchStringLen = searchString.GetLength();
		for( pos = Result.GetStartPosition(); pos != NULL; )
		{
			Result.GetNextAssoc( pos, curKey, curValue );
			if (curKey.GetLength() > 2)
			{
				if (curKey.Find(searchString,curKey.GetLength()-searchStringLen) > 0)
				{
					curKey = curKey.Left(curKey.GetLength()-searchStringLen);
					theRemoteData.SetAt(curKey, curValue);
				}
			}
		}
		CXUtils::YieldToTheOS();
		theRemoteData.Lookup("ECOM_PART_NUMBER", partNumber);
		theRemoteData.Lookup("ECOM_OPTION_NAME", partSize);
		queryPartNumber = partNumber;
		if (partSize.GetLength() != 0)
		{
			queryPartNumber = CXUtils::PadString(partNumber, 10);
			queryPartNumber.Append(partSize);
		}
		theRemoteData.SetAt("CMS_PART_NUMBER", queryPartNumber);

		CString schoolCode = GetStringLookup(theRemoteData, "CMS_SCHOOL_CODE");

		if (i > 0 && (schoolCode != currentSchool))
		{
			DoStockNotOnRemoteProcessing(theDB, allRemotePartNumbers, currentSchool);
			allRemotePartNumbers.RemoveAll();
		}

		allRemotePartNumbers.SetAt("CMS_SCHOOL_CODE", schoolCode);
		if (queryPartNumber.GetLength() > 0)
		{
			allRemotePartNumbers.SetAt(queryPartNumber,queryPartNumber);
		}

		int RetCode = DoStockProcessing(theDB, theRemoteData, currentSchool);

		if (i == (theRowCount-1))
		{
			DoStockNotOnRemoteProcessing(theDB, allRemotePartNumbers, currentSchool);
			allRemotePartNumbers.RemoveAll();
		}

		if (i > 0 && i%50 == 0)
		{
			theMessage.Format("%s %d of %d", "Processing Stock Information; Completed", i, theRowCount);
			AddMessageBoxString(theMessage);
		}
		CXUtils::YieldToTheOS();


	}
	theDB = CloseAndDeleteCDatabase(theDB);

	b_STOCK_UPDATED = true;

	return 0;
}

int CEncoderDlg::ProcessNewInvoiceRetrieval(CMapStringToString& Result)
{
	CString theMessage;

	importFileList.RemoveAll();
	invoicesIDsToMarkPending.Empty();

	CMapStringToString theRemoteData;
	CStringList theLocalData;

	CString rowCount;
	CXUtils::SafeLookup(Result,"ROWCOUNT", rowCount);
	if (rowCount.GetLength() == 0)
		rowCount = "0";
	int theRowCount = atoi(rowCount);

	CString tmpValue;
	CString tmpKey;
	CMyDatabase* theDB = NULL;
	CString dsnName;
	CString dbPath;
	CString dbName;

	// *** The Data is stored in the input array as COLUMNNAME_COLUMNINDEX starting at index 0 (e.g. INV_NUMBER_0)

	CStringArray importOrderData;
	CString lastSchool;
	CString curSchool;
	int orderCount = 0;
	for (int i=0; i< theRowCount; i++)
	{
		theRemoteData.RemoveAll();
		POSITION pos;
		CString curKey;
		CString curValue;
		CString searchString;
		searchString.Format("_%d",i);
		int searchStringLen = searchString.GetLength();
		for( pos = Result.GetStartPosition(); pos != NULL; )
		{
			Result.GetNextAssoc( pos, curKey, curValue );
			if (curKey.GetLength() > 2)
			{
				if (curKey.Find(searchString,curKey.GetLength()-searchStringLen) > 0)
				{
//					CXUtils::SafeLookup(Result,curKey, curValue);
					curKey = curKey.Left(curKey.GetLength()-searchStringLen);
					theRemoteData.SetAt(curKey, curValue);
				}
			}
		}
		
		curSchool = GetStringLookup(theRemoteData,"CMS_SCHOOL_CODE").MakeUpper();
		if (i > 0 && lastSchool != curSchool)
		{
			WriteOrderImportFile(importOrderData, lastSchool, orderCount);
			orderCount = 0;
			importOrderData.RemoveAll();
		}
		lastSchool = curSchool;

		int RetCode = BuildOrderImport(theRemoteData, importOrderData );
		orderCount++;
		CString currentInvoice = GetStringLookup(theRemoteData, "INV_ODR_NUM");
		if (currentInvoice.GetLength() > 0)
		{
			if (invoicesIDsToMarkPending.IsEmpty()) invoicesIDsToMarkPending = ":";
			currentInvoice = currentInvoice + ":";
			if (invoicesIDsToMarkPending.Find(":" + currentInvoice) < 0)
				invoicesIDsToMarkPending.Append(currentInvoice);

		}
	}

	if (importOrderData.GetSize() > 0)
	{
		WriteOrderImportFile(importOrderData, curSchool, orderCount);
		importOrderData.RemoveAll();
		orderCount = 0;
	}

//	theCommand.Empty();

	return 0;
}

void CEncoderDlg::AddToEmailMessageList(CString theMsg)
{
	theEmailMessageList.AddTail(theMsg);
}

CString CEncoderDlg::GetCurrentSchoolCode(void)
{
	return currentSchoolCode;
}

int CEncoderDlg::WriteOrderImportFile(CStringArray& theOutData, CString SchoolCode, int orderCount)
{
	CString theMessage;
	if (theOutData.GetSize() > 0)
	{
		CString currentDestinationDirectory;
		CString tmpVal;
		propertyLookup("LOCAL_IMPORT_FILE_BASE_DIR", tmpVal);

		currentDestinationDirectory.Format("%s\\%s",tmpVal, SchoolCode);
		CString errorMsg;
		CXUtils::CreateTheDirectory(currentDestinationDirectory, errorMsg);

		CString outFilename;
		CString timeStamp;
		CTime curTime;
		curTime = CTime::GetCurrentTime();
		timeStamp = curTime.Format("%d%b%Y_%H%M%S");

		//****************NOTE*********************
		// THE INPUT FILENAME FOR THE MOM IMPORT CANNOT CONTAIN THE WORK "IMPORT".
		//****************NOTE*********************
		outFilename.Format("REMOTE_ORDERS_TO_LOAD_%s.txt", timeStamp);
/*		char szFilters[]= "Import Order Files (*.imp)|*.imp|All Files (*.*)|*.*||";
		CFileDialog theDlg(false, "imp",  outFilename,,szFilters);
		theDlg.DoModal();
		theDlg.

		theDlg.open
*/

		CFileException e;
		if (true)
		{
			CStdioFile f;
			CString theLogFilePath;

			theLogFilePath.Format("%s\\%s", currentDestinationDirectory,outFilename);

			if (f.Open(theLogFilePath, 
				CFile::modeCreate | CFile::modeWrite |CFile::shareDenyWrite, &e))
			{
				for (int i=0; i<theOutData.GetSize(); i++)
				{
					CString OutMsg;
//					CTime TheTime = CTime::GetCurrentTime();
//					OutMsg = TheTime.Format("%d%b%y:%H%M%S ");
					OutMsg += theOutData.GetAt(i);
					OutMsg += "\n";
				
					f.WriteString(OutMsg);
				}
				f.Close();
				importFileList.Add(theLogFilePath);
				theMessage.Format("Import File Created (Orders:%d;Records:%d): %s",
					orderCount,
					theOutData.GetSize(),
					theLogFilePath);
				AddMessageBoxString(theMessage);
				CXUtils::WriteToTheLogFile(theMessage, false);
				theEmailMessageList.AddTail(theMessage);
			}
		}
	}
	return 0;
}

int CEncoderDlg::ProcessInProgressInvoiceResponse(CMapStringToString& Result)
{
	CString theMessage;

	invoicesIDsToMarkPending.Empty();

	CMapStringToString theRemoteData;
	CStringList theLocalData;

	CString rowCount;
	CXUtils::SafeLookup(Result,"ROWCOUNT", rowCount);
	if (rowCount.GetLength() == 0)
		rowCount = "0";
	int theRowCount = atoi(rowCount);

	CString tmpValue;
	CString tmpKey;
	CMyDatabase* theDB = NULL;
	CString dsnName;
	CString dbPath;
	CString dbName;
	CString currentSchool = "aasdfa;yfp9aayf-9awdhfasdf";	//random string since blank is allowed

	int currentInvoiceItemIndex = 0;
	CString prevInvoiceID;
	CString curInvoiceID;

	for (int i=0; i< theRowCount; i++)
	{
		theRemoteData.RemoveAll();
		POSITION pos;
		CString curKey;
		CString curValue;
		CString searchString;
		searchString.Format("_%d",i);
		int searchStringLen = searchString.GetLength();
		for( pos = Result.GetStartPosition(); pos != NULL; )
		{
			Result.GetNextAssoc( pos, curKey, curValue );
			if (curKey.GetLength() > 2)
			{
				if (curKey.Find(searchString,curKey.GetLength()-searchStringLen) > 0)
				{
					curKey = curKey.Left(curKey.GetLength()-searchStringLen);
					theRemoteData.SetAt(curKey, curValue);
				}
			}
		}

		theRemoteData.Lookup("INV_ECOM_INVOICE_ID", curInvoiceID);
		if (prevInvoiceID != curInvoiceID)
			currentInvoiceItemIndex = 0;
		else
			currentInvoiceItemIndex++;
		prevInvoiceID = curInvoiceID;

		curValue.Format("%d",currentInvoiceItemIndex); 
		theRemoteData.SetAt("INVOICE_ITEM_INDEX", curValue);
		int RetCode = CDatabaseOperations::DB_FAIL;
		RetCode = BuildStatusUpdateResponse(theDB, theRemoteData, currentSchool);
		CXUtils::YieldToTheOS();


	}
	theDB = CloseAndDeleteCDatabase(theDB);
//	theCommand.Empty();


	return 0;
}

void CEncoderDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	// TODO: Add your message handler code here and/or call default
    if ((nID & 0xFFF0) == SC_KEYMENU)
        return;
	if (nID == SC_CLOSE)
		return;

	__super::OnSysCommand(nID, lParam);
}

void CEncoderDlg::OnLbnSelchangeMsgList()
{
	// TODO: Add your control notification handler code here
}

void CEncoderDlg::OnBnClickedCancel()
{
	int theStat = MessageBox("Abort Processing, and exit the application?", "Abort Processing",MB_OKCANCEL | MB_ICONEXCLAMATION );
	if (theStat == IDOK)
	{
		ForceCancel();
	}
	// TODO: Add your control notification handler code here
}

void CEncoderDlg::OnBnClickedEnableUpload()
{
	CString newValue;
	if (this->m_EnableUpload.GetCheck() == BST_CHECKED)
	{
		int response = AfxMessageBox("Enable Upload of Scanned Ticket Info?",MB_OKCANCEL);
		if (response == IDOK)
		{
			newValue = "yes";
			propertySet("ENABLE_UPLOAD_PROCESSING", newValue);
		}
		else
		{
			this->m_EnableUpload.SetCheck(BST_UNCHECKED);
		}
	}
	else
	{
		newValue = "no";
		propertySet("ENABLE_UPLOAD_PROCESSING", newValue);
	}
}

int CEncoderDlg::FindUploadFiles(CProcessingCommand* theData)
{
	int counter = 0;
	CString theExportDir;
	CString theExportDirMask;
	propertyLookup("LOCAL_EXPORT_FILE_BASE_DIR", theExportDir);
	propertyLookup("LOCAL_EXPORT_FILE_MASK", theExportDirMask);

	if (theData != NULL)
	{
		theData->theCommandData.SetAt("FTP_REMOTE_UPLOAD_FILENAME","");
		theData->theCommandData.SetAt("FTP_LOCAL_UPLOAD_FILENAME","");
	}
	CFileFind finder;
	theExportDir += '\\' + theExportDirMask;
	BOOL bWorking = finder.FindFile(theExportDir,0);
	BOOL fileSelected = false;
	CTime curTime = CTime::GetCurrentTime();
	CTimeSpan theSpan;

	while (bWorking)
	{
        bWorking = finder.FindNextFile();
//		BOOL useFile = false;
//		useFile = true;
//		if (bWorking)
//			useFile = true;
//		else 
//		{
//			DWORD lastError = GetLastError();
//			if (lastError != ERROR_NO_MORE_FILES)
//			{
//				useFile = true;
//			}
//		}
		CTime fileWriteTime;
		finder.GetLastWriteTime(fileWriteTime);
		theSpan = curTime - fileWriteTime;
		if (theSpan.GetTotalMinutes() > 0)
		{
			CString foundFilename = finder.GetFilePath();
			CFile tmpOpen;
			if (tmpOpen.Open(foundFilename,CFile::shareExclusive | CFile::modeRead))
			{
				tmpOpen.Close();
				if ((theData != NULL) && fileSelected == false)
				{
					fileSelected = true;
					theData->theCommandData.SetAt("FTP_LOCAL_UPLOAD_FILENAME",foundFilename);
					foundFilename = finder.GetFileName();
					theData->theCommandData.SetAt("FTP_REMOTE_UPLOAD_FILENAME",foundFilename);
//					bWorking = 0;
				}
			}
			counter++;
		}
	}
	if (theData != NULL)
	{
		CString strCount;
		strCount.Format("%d",counter);
		theData->theCommandData.SetAt("PENDING_UPLOAD_COUNT",strCount);
	}
	return counter;
}

void CEncoderDlg::OnLvnColumnclickEventList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;
}

void CEncoderDlg::OnNMDblclkEventList(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: Add your control notification handler code here
	*pResult = 0;
}

void CEncoderDlg::OnBnClickedDownloadEvents()
{
	CProcessingCommand* theCmd = new CProcessingCommand();
	theCmd->theCommandData.SetAt("COMMAND","1_GET_EVENTS");
	this->processingSteps.Add((void*)theCmd);
//	theCmd = new CProcessingCommand();
//	theCmd->theCommandData.SetAt("COMMAND","PROCESS_EVENTS");
//	this->processingSteps.Add((void*)theCmd);
	// TODO: Add your control notification handler code here
}

void CEncoderDlg::doRetrievalErrorHandling(CString theCommand, CMapStringToString &Result)
{
//	CString theCommand;
//	if (m_CurrentProcessingCommand->CommandDataLookup("COMMAND", theCommand) != 0)
	{
		if (theCommand == "1_GET_EVENTS")
		{
			CString theMessage;
			theMessage.Format("1_GET_EVENTS Failed: See Error Log");
			theEmailMessageList.AddTail(theMessage);
			AddMessageBoxString(theMessage);
			CXUtils::WriteToTheLogFile(theMessage);
			delete m_CurrentProcessingCommand;
			m_CurrentProcessingCommand = NULL;

		}
		else if (theCommand == "2_UPLOAD_TICKET_EXPORTS")
		{
			CString theMessage;
			theMessage.Format("2_UPLOAD_TICKET_EXPORTS Failed: See Error Log");
			theEmailMessageList.AddTail(theMessage);
			AddMessageBoxString(theMessage);
			CXUtils::WriteToTheLogFile(theMessage);
			delete m_UploadProcessingCommand;
			m_UploadProcessingCommand = NULL;
		}
		else if (theCommand == "3_GET_TICKET_UPDATES")
		{
			CString theMessage;
			theMessage.Format("3_GET_TICKET_UPDATES Failed: See Error Log");
			theEmailMessageList.AddTail(theMessage);
			AddMessageBoxString(theMessage);
			CXUtils::WriteToTheLogFile(theMessage);
			delete m_CurrentProcessingCommand;
			m_CurrentProcessingCommand = NULL;
		}
		else if (theCommand == "4_SEND_TICKET_UPDATE_RESPONSE")
		{
			CString theMessage;
			theMessage.Format("4_SEND_TICKET_UPDATE_RESPONSE Failed: See Error Log");
			theEmailMessageList.AddTail(theMessage);
			AddMessageBoxString(theMessage);
			CXUtils::WriteToTheLogFile(theMessage);
			delete m_CurrentProcessingCommand;
			m_CurrentProcessingCommand = NULL;
		}
/*
		else if (theCommand == "CONFIG")
		{
		}
*/
		m_ActivityChar = "";
	}
}
