// AlvaradoMgr.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "AlvaradoMgr.h"
#include "winsvc.h"
#ifdef _USEING_ENCODER_CODE_
	#include "DBVMediaEncoder.h"
#endif
#include "EncoderDlg.h"
#include "XUtils.h"
#include "ConfigFileManager.h"
#include "StartupOptionSetup.h"
//#include "HttpServer.h"
#include "exception.h"
//#define _DO_SPY

#ifdef _DO_SPY
#include "cmallspy.h"
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

void ServiceMain(DWORD argc, LPTSTR *argv); 
void ServiceCtrlHandler(DWORD nControlCode);
BOOL UpdateServiceStatus(DWORD dwCurrentState, DWORD dwWin32ExitCode,
					 DWORD dwServiceSpecificExitCode, DWORD dwCheckPoint,
					 DWORD dwWaitHint);
BOOL StartServiceThread();
DWORD ServiceExecutionThread(LPDWORD param);
HANDLE hServiceThread;
void KillService();

char* strServiceName = "EncoderService";
SERVICE_STATUS_HANDLE nServiceStatusHandle; 
HANDLE killServiceEvent;
BOOL nServiceRunning;
DWORD nServiceCurrentStatus;
int installService(CString serviceName, CString serviceDisplayName, CString servicePath);
void DeleteService(CString serviceName);
void ShowErr(CString msg);
BOOL StartWebServer();
BOOL StartedByService = TRUE;
CEncoderDlg theDlg;
CString InstalledServiceName = "";
CString FileCreateTime = "";

struct gpf_handler_1 
{
    static void handler(const char*, PEXCEPTION_POINTERS) 
    {
        std::cerr << "GPF!! Handler 1 has hit." << std::endl
                  << "This would be the time to reclaim resources..." << std::endl;
    }
};

struct gpf_handler_2 
{
    static void handler(const char*, PEXCEPTION_POINTERS) 
    {
        std::cerr << "All hands on deck. Battle stations!" << std::endl;
    }
};
/////////////////////////////////////////////////////////////////////////////
// The one and only application object

CWinApp theApp;

using namespace std;
#if defined(_DO_SPY)
	CMemoryState msOld, msNew, msDif;
#endif

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])		// Command Line Main
{
	::FreeConsole();
	StartedByService = FALSE;
	CEncoderDlg::b_interactiveMode = true;

	CXUtils::FillAppInfo();
	if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
	{
		// TODO: change error code to suit your needs
		cerr << _T("Fatal Error: MFC initialization failed") << endl;
	}
	else
	{
		nServiceRunning = true;
		ServiceExecutionThread(0);
	}
	return 1;

}
int _tmain1(int argc, TCHAR* argv[], TCHAR* envp[])	// Service Main
{
	int nRetCode = 0;
	::FreeConsole();

	CEncoderDlg::b_interactiveMode = false;

	// initialize MFC and print and error on failure
	CXUtils::FillAppInfo();
	if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
	{
		// TODO: change error code to suit your needs
		cerr << _T("Fatal Error: MFC initialization failed") << endl;
		nRetCode = 1;
	}
	else
	{
		CString arg1;
		CString arg2;
		if (argc > 1)
		{
			arg1 = argv[1];
			if (arg1 == "INSTALL")
			{
				CString theCommandLine = GetCommandLine();
				char szFullPath[256];
				GetModuleFileName (NULL, szFullPath, sizeof(szFullPath));
				CString TheAppPath = szFullPath;

				CString serviceName = "AlvaradoMgr";
				CString serviceDisplayName = "AlvaradoMgr";
				if (argc > 2)
				{
					arg2 = argv[2];
					serviceName = "AlvaradoMgr " + arg2;
					serviceDisplayName = "AlvaradoMgr " + arg2;
				}
//				TheAppPath = "c:\\nmnEncoder\\srvany.exe";
				nRetCode = installService(serviceName, serviceDisplayName, TheAppPath);


			}
			else if (arg1 == "REMOVE")
			{
				CString serviceName = "AlvaradoMgr";
				if (argc > 2)
				{
					arg2 = argv[2];
					serviceName = "AlvaradoMgr " + arg2;
				}
				DeleteService(serviceName);
			}
		}
		else
		{
			// TODO: code your application's behavior here.
			SERVICE_TABLE_ENTRY servicetable[]=
			{
				{strServiceName,(LPSERVICE_MAIN_FUNCTION)ServiceMain},
				{NULL,NULL}
			};
			BOOL success;
			success=StartServiceCtrlDispatcher(servicetable);
			if(!success)
			{
				//error occured
			}
		
			CString strHello;
			strHello.LoadString(IDS_HELLO);
			cout << (LPCTSTR)strHello << endl;
		}
	}

	return nRetCode;
}

int installService(CString serviceName, CString serviceDisplayName, CString servicePath)
{
	SC_HANDLE NishService,scm;
	scm=OpenSCManager(0,0,SC_MANAGER_CREATE_SERVICE);
	if(!scm)
	{

		ShowErr("Install Service: OpenSCManager Failed");
		return 1;
	}
	NishService=CreateService(scm,
//		"zzz", "zzzz",
		(LPCTSTR)serviceName,
		(LPCTSTR)serviceDisplayName,
		SERVICE_ALL_ACCESS,
		SERVICE_WIN32_OWN_PROCESS | SERVICE_INTERACTIVE_PROCESS,
		SERVICE_DEMAND_START,
		SERVICE_ERROR_NORMAL,
//		"c:\\nmnencoder\\srvany.exe",
		(LPCTSTR)servicePath,
		0,0,0,0,0);
	if(!NishService)
	{
		ShowErr("InstallService: CreateService Failed:" + serviceName);
		CloseServiceHandle(scm);
		return 1;
	}
	CloseServiceHandle(NishService);
	CloseServiceHandle(scm);
    CXUtils::WriteToTheLogFile("CreateService SUCCESS:" + serviceName); 
	return 0;
}
void DeleteService(CString serviceName) 
{ 
 	SC_HANDLE schService,scm;
	scm=OpenSCManager(0,0,SC_MANAGER_CREATE_SERVICE);
	schService = OpenService( 
        scm,       // SCManager database 
//		"zzz",
        (LPCTSTR)serviceName,       // name of service 
        DELETE);            // only need DELETE access 
 
    if (schService == NULL) 
        ShowErr("DeleteService: OpenService Failed:" + serviceName); 
	else
	{
	    if (! DeleteService(schService) ) 
		    ShowErr("DeleteService Failed:" + serviceName); 
	    else 
		    CXUtils::WriteToTheLogFile("DeleteService SUCCESS:" + serviceName); 
	}
	if (schService != NULL)
		CloseServiceHandle(schService);
	if (scm != NULL)
		CloseServiceHandle(scm);
} 

void ShowErr(CString msg)
{
	CString theMsg;
	theMsg = "Error:" + msg;
	CString theError;
	theError = CXUtils::decodeGetLastError();
	theMsg += " GetLastError:" + theError;

	CXUtils::WriteToTheLogFile(theMsg, FALSE);
	cout << (LPCSTR)theMsg << "\r\n";
}
void ServiceMain(DWORD argc, LPTSTR *argv)
{
	if (argc > 0)
	{
		InstalledServiceName = argv[0];
	}
	CXUtils::WriteToTheLogFile("ServiceMain Called: Service Name:" + InstalledServiceName);
	BOOL success;
	nServiceStatusHandle=RegisterServiceCtrlHandler(strServiceName,
		(LPHANDLER_FUNCTION)ServiceCtrlHandler);
	if(!nServiceStatusHandle)
	{
		return;
	}
	success=UpdateServiceStatus(SERVICE_START_PENDING,NO_ERROR,0,1,3000);
	if(!success)
	{
		return;
	}
	killServiceEvent=CreateEvent(0,TRUE,FALSE,0);
	if(killServiceEvent==NULL)
	{
		return;
	}
	success=UpdateServiceStatus(SERVICE_START_PENDING,NO_ERROR,0,2,1000);
	if(!success)
	{
		return;
	}
	success=StartServiceThread();
	if(!success)
	{
		return;
	}
	nServiceCurrentStatus=SERVICE_RUNNING;
	success=UpdateServiceStatus(SERVICE_RUNNING,NO_ERROR,0,0,0);
	if(!success)
	{
		return;
	}
	WaitForSingleObject(killServiceEvent,INFINITE);
	CloseHandle(killServiceEvent);
}



BOOL UpdateServiceStatus(DWORD dwCurrentState, DWORD dwWin32ExitCode,
					 DWORD dwServiceSpecificExitCode, DWORD dwCheckPoint,
					 DWORD dwWaitHint)
{
	CXUtils::WriteToTheLogFile("UpdateServiceStatus Called");
	BOOL success;
	SERVICE_STATUS nServiceStatus;
	nServiceStatus.dwServiceType=SERVICE_WIN32_OWN_PROCESS;
	nServiceStatus.dwCurrentState=dwCurrentState;
	if(dwCurrentState==SERVICE_START_PENDING)
	{
		nServiceStatus.dwControlsAccepted=0;
	}
	else
	{
		nServiceStatus.dwControlsAccepted=SERVICE_ACCEPT_STOP			
			|SERVICE_ACCEPT_SHUTDOWN;
	}
	if(dwServiceSpecificExitCode==0)
	{
		nServiceStatus.dwWin32ExitCode=dwWin32ExitCode;
	}
	else
	{
		nServiceStatus.dwWin32ExitCode=ERROR_SERVICE_SPECIFIC_ERROR;
	}
	nServiceStatus.dwServiceSpecificExitCode=dwServiceSpecificExitCode;
	nServiceStatus.dwCheckPoint=dwCheckPoint;
	nServiceStatus.dwWaitHint=dwWaitHint;

	CString msg;
	msg.Format("%s: %d","Calling SetServiceStatus", nServiceStatusHandle);
	CXUtils::WriteToTheLogFile(msg);
	success=SetServiceStatus(nServiceStatusHandle,&nServiceStatus);
	msg.Format("%s: %d","After Calling SetServiceStatus",success);
	CXUtils::WriteToTheLogFile(msg);

	if(!success)
	{
		KillService();
		return success;
	}
	else
		return success;
}

BOOL StartServiceThread()
{	
	CXUtils::WriteToTheLogFile("StartServiceThread Called");
	DWORD id;
	hServiceThread=CreateThread(0,0,
		(LPTHREAD_START_ROUTINE)ServiceExecutionThread,
		0,0,&id);
	if(hServiceThread==0)
	{
		return false;
	}
	else
	{
		nServiceRunning=true;
		return true;
	}
}
/*
DWORD ServiceExecutionThread(LPDWORD param)
{
	while(nServiceRunning)
	{		
		CString abc;
		Beep(450,150);
		Sleep(4000);
	}
	return 0;
}
*/
DWORD ServiceExecutionThread(LPDWORD param)
{
	if (CoInitialize(NULL) != S_OK)
	{
//		if (localArgCount >1)
//		{
//			theMessage = "Error: Call to CoInitialize failed.  Unable to initialize COM.  Click Ok to exit.";
//			MessageBox(NULL,theMessage," CoInitialize Failed", MB_OK);
//		}

		return FALSE;
	}
#if defined(_DO_SPY)
		msOld.Checkpoint();
	
		CMallocSpy* pMallocSpy = new CMallocSpy;
		pMallocSpy->AddRef();
		::CoRegisterMallocSpy(pMallocSpy);

	    pMallocSpy->Clear();
//		pMallocSpy->SetBreakAlloc(2);  
//		pMallocSpy->SetBreakAlloc(5)  ;
//		pMallocSpy->SetBreakAlloc(7)  ;
//		pMallocSpy->SetBreakAlloc(9)  ;

#endif

	plugware::exception::filter<plugware::exception::shutdown>::install();
    plugware::exception::filter<plugware::exception::report>::install();
    plugware::exception::filter<gpf_handler_1>::install();
    plugware::exception::filter<gpf_handler_2>::install();


//	StartWebServer();
//
//	CDBVMediaEncoder::GetPluginResources();
	if (nServiceRunning)
	{		

//		CEncoderDlg theDlg;
		CFileStatus f_filestatus;
		CFile::GetStatus(CXUtils::TheAppPath,f_filestatus);
		CTime createDate = f_filestatus.m_mtime;
		FileCreateTime = createDate.FormatGmt("%m/%d/%Y %H:%M:%S GMT");
		theDlg.m_FileCreateTime = FileCreateTime;
		theDlg.m_WindowTitle = InstalledServiceName;
//		int counter=240;
//		while (counter > 0)
//		{
//			CXUtils::YieldToTheOS();
//			counter--;
//		}

		theApp.m_pActiveWnd = &theDlg;
		theDlg.DoModal();
		CString errorMsg;
//		CXUtils::DoEncoder("C:\\TestMusic.wav", "C:\\testmusic1.wma", "TestCDAudio", errorMsg);
//		CXUtils::DoDeviceEncoder(L"DEVICE://Default_Audio_Device", "C:\\testmusic1.wma", "TestCDAudio", errorMsg);
				
	}
//	while(nServiceRunning)
//	{		
//		CString errorMsg;
//		CXUtils::DoEncoder("C:\\TestMusic.wav", "C:\\testmusic1.wma", "TestCDAudio", errorMsg);
//		CXUtils::DoDeviceEncoder(L"DEVICE://Default_Audio_Device", "C:\\testmusic1.wma", "TestCDAudio", errorMsg);
//		
//		CString abc;
//		Beep(450,150);
//		Sleep(4000);
//		break;
//		
//	}

//	server.stop();

	CoUninitialize();
#if defined(_DO_SPY)

    //
    // Dump COM memory leaks
    //
    pMallocSpy->Dump();

    //
    // Unregister the malloc spy ...
    //
    ::CoRevokeMallocSpy();
    pMallocSpy->Release();
	delete pMallocSpy;

	msOld.DumpAllObjectsSince();
	msNew.Checkpoint();
	msDif.Checkpoint();
	msDif.Difference( msOld, msNew );
	msDif.DumpStatistics();
#endif 

	return 0;
}

//BOOL StartWebServer()
//{
	// create server
//	server.create( string("Encoder"), string("c:\\temp") );
//
	// set server links
//	HttpResponse::setServer( server );

//	while ( true )
//	{
//		if ( _kbhit() )
//			break;
//
//		Sleep(100);
//	}
//	return TRUE;
//}

void KillService()
{
	CXUtils::WriteToTheLogFile("KillService Called");
	nServiceRunning=false;
	theDlg.ForceCancel();
	time_t curTime = time(NULL);
	while ((curTime+30) > time(NULL))
	{
		if (theDlg.IsShutdownComplete() == FALSE)
			CXUtils::YieldToTheOS();
		else
			break;
	}

	SetEvent(killServiceEvent);
	UpdateServiceStatus(SERVICE_STOPPED,NO_ERROR,0,0,0);
	CXUtils::WriteToTheLogFile("KillService Complete");
}

void ServiceCtrlHandler(DWORD nControlCode)
{
	BOOL success;
	switch(nControlCode)
	{	
	case SERVICE_CONTROL_SHUTDOWN:
	case SERVICE_CONTROL_STOP:
		CXUtils::WriteToTheLogFile("ServiceCtrlHandler Called: SERVICE_CONTROL_SHUTDOWN");
		nServiceCurrentStatus=SERVICE_STOP_PENDING;
		success=UpdateServiceStatus(SERVICE_STOP_PENDING,NO_ERROR,0,1,10000);
		KillService();		
		return;
	default:
		break;
	}
	UpdateServiceStatus(nServiceCurrentStatus,NO_ERROR,0,0,0);
}