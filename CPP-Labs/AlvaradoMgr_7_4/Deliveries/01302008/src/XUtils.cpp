// Utils.cpp: implementation of the CUtils class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "XUtils.h"

#define ADD_MAIL_UTILS

#ifdef ADD_MAIL_UTILS	// define this if you want to include mail support
	#include "MAIL_SMTP.h"
	#include "MAIL_MailMessage.h"
	#include "MAIL_MIMEMessage.h"
#endif
// Include libraries.

#include <windows.h>
#include <atlbase.h>

#ifdef _USEING_ENCODER_CODE_
	#include "wmencode.h"
	#include "WMF_genprofile.h"
	#include "WMF_util.h"
#endif

#include "Wininet.h"
#include ".\xutils.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

BOOL CXUtils::DoLogging = TRUE;

#ifdef _DEBUG
	BOOL CXUtils::doDebugLogging = TRUE;
#else
	BOOL CXUtils::doDebugLogging = FALSE;
#endif

CString CXUtils::LoggingFileName = "AlvaradoMgr.log";
const DWORD CXUtils::SHELL_EXECUTE_SUCCESS_THRESHOLD = 32;
CString CXUtils::TheCommandLine = "";
CString CXUtils::TheRunDrive = "";
CString CXUtils::TheAppPath = "";
CString CXUtils::TheExecutablePath = "";
CString CXUtils::dontNeedEncoding = CXUtils::InitURLEncoder();
#define XUTILS_CRLF			"\x0d\x0a"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CXUtils::CXUtils()
{

}

CXUtils::~CXUtils()
{

}
void CXUtils::SplitCStringOnChar(CString in, 
									   char StripChar, 
									   CString &out1,
									   CString &out2)
{
	out1.Empty();
	out2.Empty();
	int charpos;
	charpos = in.Find(StripChar);
	if (charpos < 0)
	{
		out1 = in;
	}
	else
	{
		out1 = in.Left(charpos);
		out2 = in.Right(in.GetLength() - charpos - 1);
	}
}

void CXUtils::GetFileExtension(CString in, 
							   CString &out)
{
	out.Empty();
	int charpos;
	charpos = in.ReverseFind('.');
	if (charpos > 0)
	{
		out = in.Right(in.GetLength() - charpos );
	}
}

void CXUtils::ReplaceCStringWithCStringNC(CString in, 
									   CString findString, 
									   CString replaceString,
									   CString &out)
{
	out.Empty();
	if (findString.IsEmpty())
	{
		out = in;
		return;
	}
	if (findString == replaceString)
	{
		out = in;
		return;
	}

	CString inNC;
	inNC = in;
	inNC.MakeLower();
	findString.MakeLower();

	int charpos = 1;
	CString tmpVal;
	while (charpos >= 0)
	{
		charpos = inNC.Find(findString);

		if (charpos < 0)
		{
			out = in;
		}
		else
		{
			tmpVal = in.Left(charpos);
			tmpVal += replaceString;
			tmpVal += in.Right(in.GetLength() - charpos - findString.GetLength());
			in = tmpVal;
			inNC = in;
			inNC.MakeLower();
			charpos += replaceString.GetLength();
		}
	}
}
void CXUtils::TrimOffEndChar(CString in, 
								char StripChar, 
								CString &out1)
{
	out1 = in;
	while ((out1.ReverseFind(StripChar)+1) == out1.GetLength())
	{
		out1 = out1.Left(out1.GetLength()-1);
		if (out1.GetLength() <= 0)
			break;
	}
}

void CXUtils::YieldToTheOS()
{
	MSG Msg;
	if(::PeekMessage (&Msg, NULL, 0, 0, PM_REMOVE))
	{
		::TranslateMessage(&Msg);
		::DispatchMessage(&Msg);
	}
	::Sleep(10);
}

void CXUtils::SetDebugLoggingFlag(BOOL onOff)
{
	doDebugLogging = onOff;
}

void CXUtils::WriteToTheLogFile(CString theMsg, BOOL debugOnly)
{
	if (DoLogging && theMsg.IsEmpty() == FALSE)
	{
		if (debugOnly == FALSE || (doDebugLogging && debugOnly))
		{
			CStdioFile f;
			CString theLogFilePath;
			theLogFilePath.Format("%s\\%s", CXUtils::TheExecutablePath,LoggingFileName);

			if (f.Open(theLogFilePath, 
				CFile::modeCreate | CFile::modeNoTruncate |CFile::modeWrite |CFile::shareDenyWrite))
			{
				CString OutMsg;
				CTime TheTime = CTime::GetCurrentTime();
				OutMsg = TheTime.Format("%d%b%y:%H%M%S ");
				OutMsg += theMsg;
				OutMsg += "\n";
				f.SeekToEnd();

				f.WriteString(OutMsg);
				f.Close();
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////
//
//	GetEntireKeyValue - Sets a String value in the registry using the provided CurDefaultKey, 
//		EntireSubKey and Key values, No defined Default Keys are used.
//		(e.g. Value = CurDefaultKey\\EntireSubKey\\Key)
//
//////////////////////////////////////////////////////////////////////
void CXUtils::GetEntireKeyValue(HKEY CurDefaultKey, const char* EntireSubKey, 
										const char* Key, char* Value, const char* Default)
{
    DWORD cbData;
    HKEY hkey;
	char ReadBuffer[_MAX_PATH];
	ReadBuffer[0] = NULL;
	Value[0] = NULL;

    if (RegOpenKeyEx(CurDefaultKey, EntireSubKey, 0, KEY_QUERY_VALUE, &hkey))
	{
		if (Default != NULL)
			lstrcpy(Value, Default);
    } 
	else 
	{
        cbData = sizeof(ReadBuffer);
        if (RegQueryValueEx(hkey, Key, NULL, NULL,
            (unsigned char*)ReadBuffer, &cbData) == ERROR_SUCCESS) 
		{
			lstrcpy(Value, ReadBuffer);
		}
		else
		{
			if (Default != NULL)
				lstrcpy(Value, Default);
        }
        RegCloseKey(hkey);
    }
}

void CXUtils::FillAppInfo()
{
	TheRunDrive.Empty();

	TheCommandLine = GetCommandLine();

	char szFullPath[256];
    GetModuleFileName (NULL, szFullPath, sizeof(szFullPath));
	TheRunDrive = szFullPath[0];
	TheAppPath = szFullPath;

	int SpacePos = TheAppPath.ReverseFind('\\');
	if (SpacePos > 0)
	{
		TheExecutablePath = TheAppPath.Left(SpacePos);
	}
}

BOOL CXUtils::WriteFileToDisk(CString TheFilename, BYTE* TheData, DWORD NumBytes)
{
	CFile TheFile;
	BOOL Status = FALSE;	

	Status = TheFile.Open(TheFilename,CFile::modeCreate | CFile::modeWrite);
	if (Status != FALSE)
	{
		TheFile.Write(TheData,NumBytes);	
		TheFile.Close();
	}
	
	return Status;
}
BOOL CXUtils::DoesFileExist(CString TheFilenamePath)
{
	BOOL  OpenStatus;
	CFile TheFileToOpen;

	CFileStatus TheStatus;

	OpenStatus = TheFileToOpen.GetStatus(TheFilenamePath, TheStatus);
	return OpenStatus;
}
int CXUtils::Move(const char *OldPath, const char *NewPath)
{
	
	TRY
	{
		CFile::Rename( OldPath, NewPath );
	}
	CATCH( CFileException, e )
	{
		return -1;
	}
	END_CATCH
		
	return 0;

}

int CXUtils::CopyFile(const char *OldPath, const char *NewPath)
{

	if (::CopyFile( OldPath, NewPath, FALSE) == 0)
		return -1;
	else
		return 0;

}

void CXUtils::GMT_TO_LOCAL(SYSTEMTIME ZTime, SYSTEMTIME& localTime)
{
   FILETIME FileTime, LocalFileTime;
//   SYSTEMTIME UTCTime, LocalTime;

//UTCTime.wYear = ; 
//UTCTime.wMonth; 
//UTCTime.wDay; 
//UTCTime.wHour; 
//UTCTime.wMinute; 
//UTCTime.wDayOfWeek = 0; 
//UTCTime.wSecond = 0; 
//UTCTime.wMilliseconds = 0; 

// Converting UTCTime to LocalTime.


/*
   CTime theCTime(1999, 6,22, 4,15,0);
   time_t theTime;
   time(&theTime);

   tm* gmt_time;
   tm* loc_time;
   gmt_time = gmtime(&theTime);
	printf( "%.19s %s\n", asctime( gmt_time ) );

   loc_time = localtime(&theTime);
	printf( "%.19s %s\n", asctime( loc_time ) );

	TRACE (theCTime.FormatGmt("GMT:%m%d%Y:%H:%M\n"));
	TRACE (theCTime.Format("CST:%m%d%Y:%H:%M\n"));
*/

   SystemTimeToFileTime(&ZTime,&FileTime);
   FileTimeToLocalFileTime(&FileTime, &LocalFileTime);
   FileTimeToSystemTime(&LocalFileTime, &localTime); 

   CTime theLocalTime(localTime);
   CTime theGMTTime(ZTime);

//   tm* gmt_time = theLocalTime.GetGmtTm();
//   tm* loc_time = theLocalTime.GetLocalTm();

   CString localGMT_TimeString;
   CString orig_GMT_TimeString;


   localGMT_TimeString = theLocalTime.FormatGmt("%Y%m%d:%H%M%S");
   orig_GMT_TimeString.Format("%.4d%.2d%.2d:%.2d%.2d%.2d",
	   ZTime.wYear,ZTime.wMonth,ZTime.wDay,ZTime.wHour,ZTime.wMinute,ZTime.wSecond);

   if (localGMT_TimeString > orig_GMT_TimeString)
   {
		CTimeSpan thirtyMinuteTimeSpan(0,0,30,0);
		while(localGMT_TimeString > orig_GMT_TimeString)
		{
			theLocalTime -= thirtyMinuteTimeSpan;
			localGMT_TimeString = theLocalTime.FormatGmt("%Y%m%d:%H%M%S");
		}
		theLocalTime.GetAsSystemTime(localTime);
   }
   else if (localGMT_TimeString < orig_GMT_TimeString)
   {
	   CTimeSpan thirtyMinuteTimeSpan(0,0,30,0);
		while(localGMT_TimeString < orig_GMT_TimeString)
		{
			theLocalTime += thirtyMinuteTimeSpan;
			localGMT_TimeString = theLocalTime.FormatGmt("%Y%m%d:%H%M%S");
		}
		theLocalTime.GetAsSystemTime(localTime);
   }

}

BOOL CXUtils::ValidateTimeString(CString month, CString day, CString year, CString hour, CString min, CString sec, CTime& outCtime)
{
	BOOL isValid = TRUE;

	int intYear = 0;
	int intMonth = 0;
	int intDay = 0;
	int intHour = 0;
	int intMin = 0;
	int intSec = 0;

	if (isValid)
	{
		intYear = atoi(year);
		if (intYear < 1970 || intYear > 2038)
			isValid = FALSE;
	}

	if (isValid)
	{
		intMonth = atoi(month);
		if (intMonth < 1 || intMonth > 12)
			isValid = FALSE;
	}

	if (isValid)
	{
		intDay = atoi(day);
		int nextMonth = intMonth + 1;
		int nextMonthYear = intYear;
		if (nextMonth > 12) 
		{
			nextMonth = 1;
			nextMonthYear++;
		}
		CTime curMonth(nextMonthYear, nextMonth,1,0,0,0);
		CTimeSpan span12Hours(0,12,0,0);
		curMonth -= span12Hours;
		if (intDay < 1 || intDay > curMonth.GetDay())
			isValid = FALSE;
	}
	if (isValid)
	{
		intHour = atoi(hour);
		if (intHour < 0 || intHour > 23)
			isValid = FALSE;
	}
	if (isValid)
	{
		intMin = atoi(min);
		if (intMin < 0 || intMin > 59)
			isValid = FALSE;
	}

	if (isValid)
	{
		intSec = atoi(sec);
		if (intSec < 0 || intSec > 59)
			isValid = FALSE;
	}

	if (isValid)
	{
		CTime thisCtime(intYear, intMonth, intDay, intHour, intMin, intSec);
		outCtime = thisCtime;
	}
	return isValid;	
}

//BOOL CXUtils::DoTheEmailSend(CString smtpServer, CString from, CString to, CString subject, CString body, CString& errorMsg)
BOOL CXUtils::DoTheEmailSend(CString smtpServer, CString from, CString to, CString subject, CStringList& bodyList, CString& errorMsg)
{
	BOOL RetStatus = TRUE;
	CString cc;
	CString bcc;
	CString body;
	
	errorMsg.Empty();
	
	if ( to.IsEmpty() )
	{
		errorMsg = "Email Delivery Error: No (TO) parameter set for email delivery.";
		RetStatus = FALSE;
	}
	
	if (RetStatus)
	{
		if ( subject.IsEmpty() )
		{
			errorMsg = "Email Delivery Error: Subject field not provided.";
			RetStatus = FALSE;
		}
	}
	if (RetStatus)
	{
		if ( from.IsEmpty() )
		{
			errorMsg = "Email Delivery Error: From field not provided.";
			RetStatus = FALSE;
		}
	}
	
	if (RetStatus)
	{
		if ( smtpServer.IsEmpty() )
		{
			errorMsg = "Email Delivery Error: STMP Server not provided.";
			RetStatus = FALSE;
		}
	}

	if (RetStatus)
	{
		//*** Send
		CMIMEMessage msg;
		CSMTP smtp( smtpServer );
	
		msg.m_sFrom    = from;
		msg.m_sSubject = subject;

		while (bodyList.GetCount() > 0)
		{
			msg.m_sBody.Append(bodyList.GetHead() + XUTILS_CRLF);
//			msg.m_sBody
//			msg.m_sBody    = bodyList.GetHead();
			bodyList.RemoveHead();
		}

		msg.AddMultipleRecipients( to );
	
	
		RetStatus = smtp.Connect();

		if (RetStatus)
		{
			if (!smtp.SendMessage( &msg ))
			{
				errorMsg.Format("Email Delivery Error: SendMessage Error: %s", smtp.GetLastError());
				RetStatus = FALSE;
			}
			smtp.Disconnect();
		}
		else
		{
			errorMsg.Format("Email Delivery Error: STMP Connect failed to %s", smtpServer);
		}
	}
	
	return RetStatus;
}



/*
void CXUtils::DoEncoder()
{
// Declare variables.

    HRESULT hr;
    IWMEncoder* pEncoder;
    IWMEncSourceGroupCollection* pSrcGrpColl;
    IWMEncSourceGroup* pSrcGrp;
    IWMEncSource* pAudSrc1;
    IWMEncSource* pSrc1;
    IWMEncVideoSource* pVidSrc1;
    IWMEncProfileCollection* pProColl;
    IWMEncProfile* pPro;
    IWMEncDisplayInfo* pDispInfo;
    IWMEncAttributes* pAttr;
    IWMEncBroadcast* pBrdcast;
    IWMEncFile* pFile;
    long lCount;
    short i;

// Initialize the COM library and retrieve a pointer
// to an IWMEncoder interface.

//    hr = CoInitialize(NULL);
    hr = CoCreateInstance(CLSID_WMEncoder,
                          NULL,
                          CLSCTX_INPROC_SERVER,
                          IID_IWMEncoder,
                          (void**) &pEncoder);

// Retrieve a pointer to an IWMEncSourceGroupCollection
// interface.

    hr = pEncoder->get_SourceGroupCollection(&pSrcGrpColl);

// Add a source group to the collection.

    hr = pSrcGrpColl->Add(L"SG_1", &pSrcGrp);

// Add a video and audio source to the source group.

    hr = pSrcGrp->AddSource(WMENC_VIDEO, &pSrc1);
    hr = pSrcGrp->AddSource(WMENC_AUDIO, &pAudSrc1);

// Retrieve a pointer to an IWMEncVideoSource interface.

    hr = pSrc1->QueryInterface(IID_IWMEncVideoSource, (void**)&pVidSrc1);

// Identify the capture cards.

    hr = pVidSrc1->SetInput(L"device://video card name");
    hr = pAudSrc1->SetInput(L"device://audio card name");

// Choose a profile from the collection.

    CComBSTR bstrName = NULL;
    CComVariant varProfile;
    varProfile.vt = VT_DISPATCH;
    hr = pEncoder->get_ProfileCollection(&pProColl);
    hr = pProColl->get_Count(&lCount);
    for (i=0; i<lCount; i++)
    {
        hr = pProColl->Item(i, &pPro);
        hr = pPro->get_Name(&bstrName);
        if (_wcsicmp(bstrName,L"profile name")==0)
        {
            // Set the profile in the source group.
            varProfile.pdispVal = pPro;
            hr = pSrcGrp->put_Profile(varProfile);
            break;
        }
}

// Retrieve a pointer to an IWMEncDisplayInfo interface.

    hr = pEncoder->get_DisplayInfo(&pDispInfo);

// Describe the encoded content.

    CComBSTR bstrAuthor("Content Author's name.");
    CComBSTR bstrCopyright("Copyright 2000. All rights reserved.");
    CComBSTR bstrDescription("Description of the encoded content.");
    CComBSTR bstrRating("Content rating.");
    CComBSTR bstrTitle("Title of the encoded content.");

    hr = pDispInfo->put_Author(bstrAuthor);
    hr = pDispInfo->put_Copyright(bstrCopyright);
    hr = pDispInfo->put_Description(bstrDescription);
    hr = pDispInfo->put_Rating(bstrRating);
    hr = pDispInfo->put_Title(bstrTitle);

// Retrieve a pointer to an IWMEncAttributes interface.

    hr = pEncoder->get_Attributes(&pAttr);

// Add an attribute to the attributes collection.

    CComBSTR bstrAttrName(L"DateCreated: ");
    CComVariant varValue;
    varValue.vt = VT_BSTR;
    varValue.bstrVal = L"02/15/2000";
    hr = pAttr->Add(bstrAttrName,varValue);

// Retrieve a pointer to an IWMEncBroadcast interface.

    hr = pEncoder->get_Broadcast(&pBrdcast);   

// Specify a port number and a protocol.

    hr = pBrdcast->put_PortNumber(WMENC_PROTOCOL_HTTP, 8080);

// Retrieve a pointer to an IWMEncFile interface.

    hr = pEncoder->get_File(&pFile);

// Specify a file in which to save encoded content.

    CComBSTR bstrFileName(L"file name");
    hr = pFile->put_LocalFileName(bstrFileName);

// Use the IWMEncVideoSource object to crop the video 
// stream. Because pVidSrc1 was declared as an 
// IWMEncVideoSource object earlier, it can use the methods of 
// both the IWMEncVideoSource and IWMEncSource objects.

    hr = pVidSrc1->put_CroppingBottomMargin(5);
    hr = pVidSrc1->put_CroppingTopMargin(5);
    hr = pVidSrc1->put_CroppingLeftMargin(3);
    hr = pVidSrc1->put_CroppingRightMargin(3);

// Start the encoder enngine.

    hr = pEncoder->Start();

// Wait until the engoder engine stops before exiting the application.
// You can do this by using the _IWMEncoderEvents object to create an
// event sink.

}
*/

#ifdef _USEING_ENCODER_CODE_

BOOL CXUtils::DoEncoder(CString inName, CString outName, CString vodProfileName, CString &errorMsg)
{
	BOOL RunStatus = TRUE;
	HRESULT hr;
	IWMEncoder* pEncoder = NULL;
	IWMEncSourceGroupCollection* pSrcGrpColl = NULL;
	IWMEncSourceGroup* pSrcGrp = NULL;
	IWMEncSource* pAudSrc = NULL; 
	//	 IWMEncSource* pVidSrc;
//	IWMEncProfileCollection* pProColl;
//	IWMEncProfile* pPro;
	IWMEncFile* pFile = NULL;
	CString locErrorMsg;	
	
	// Initialize the COM library and retrieve a pointer
	// to an IWMEncoder interface.
	//	hr = CoInitialize(NULL);

	hr = CoCreateInstance(CLSID_WMEncoder,
		NULL,
		CLSCTX_INPROC_SERVER,
		IID_IWMEncoder,
		(void**) &pEncoder);
	
	if (RunStatus)
	{

		if (WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: CoCreateInstance:CLSID_WMEncoder:DoEncoder: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}
	// Retrieve a pointer to an IWMEncSourceGroupCollection
	// interface.
	
	if (RunStatus)
	{
		hr = pEncoder->get_SourceGroupCollection(&pSrcGrpColl);
		if (WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pEncoder->get_SourceGroupCollection: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}
	// Add an empty source group to the collection.
	
	if (RunStatus)
	{
		hr = pSrcGrpColl->Add(L"SG_1", &pSrcGrp);
		if (WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pSrcGrpColl->Add: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}
	// Add an audio and video stream to the source group.
	
	if (RunStatus)
	{
		hr = pSrcGrp->AddSource(WMENC_AUDIO, &pAudSrc);
		if (WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pSrcGrp->AddSource: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}
	//	 hr = pSrcGrp->AddSource(WMENC_VIDEO, &pVidSrc);
	
	// Retrieve a pointer to an IWMEncFile interface.
	
	if (RunStatus)
	{
		hr = pEncoder->get_File( &pFile );
		if (WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pEncoder->get_File: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}

	//	 hr = pEncoder->Load(L"C:\\filename.wme");
	// Specify an .avi source file and a Windows Media file with a .wmv extension for the output.
	
	CComBSTR m_bstrInFile(inName);
	CComBSTR m_bstrOutFile(outName);
	CComBSTR m_bstrFileName(L" ");

	if (RunStatus)
	{
		hr = pAudSrc->SetInput(m_bstrInFile);
		if (WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pAudSrc->SetInput: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}
	//	 hr = pVidSrc->SetInput(m_bstrInFile);


	if (RunStatus)
	{
		hr = pFile->put_LocalFileName(m_bstrOutFile);
		if (WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pFile->put_LocalFileName: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}

	// Retrieve the name of the output file.
	
	if (RunStatus)
	{
		hr = pFile->get_LocalFileName(&m_bstrFileName);
		if (WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pFile->get_LocalFileName: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}
	if (RunStatus)
	{
		BOOL foundProfile = FALSE;
		foundProfile = CXUtils::FindNamedEncoderProfile(pEncoder,	 
			pSrcGrp, 
			vodProfileName);
	
		if (foundProfile == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: Encoder profile not found for processing:", vodProfileName);
			RunStatus = FALSE;
		}
	}

	// Start archiving when the encoder engine starts. This is the default.
	
	if (RunStatus)
	{
		hr = pEncoder->put_EnableAutoArchive(VARIANT_TRUE);
		if (WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pEncoder->put_EnableAutoArchive: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}

	// Specify automatic indexing.
	
	if (RunStatus)
	{
		hr = pEncoder->put_AutoIndex(VARIANT_TRUE);
		if (WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pEncoder->put_AutoIndex: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}
	
	// Initialize and start the encoder engine.
	
	if (RunStatus)
	{
		hr = pEncoder->PrepareToEncode(VARIANT_TRUE);
		if (WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pEncoder->PrepareToEncode: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}

	if (RunStatus)
	{
		CString theMsg;
		theMsg = "Starting Encoding Processing...";
		CXUtils::WriteToTheLogFile(theMsg, TRUE);
		hr = pEncoder->Start();
		if (WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pEncoder->Start: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}
	// To determine when the encoding and archiving process has ended, 
	// you must implement the OnArchiveStateChange event in the 
	// _IWMEncoderEvents interface.
	

	if (RunStatus)
	{
		WMENC_ENCODER_STATE enumEncoderState;
		enumEncoderState = WMENC_ENCODER_RUNNING;
		while (enumEncoderState != WMENC_ENCODER_STOPPED)
		{
			hr = pEncoder->get_RunState(&enumEncoderState);
			CXUtils::YieldToTheOS();
		}

		CString theMsg;
		theMsg = "Encoding Complete";
		CXUtils::WriteToTheLogFile(theMsg, TRUE);

		long lError;
		hr = pEncoder->get_ErrorState(&lError);

		if (WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error after Encoder Check: pEncoder->get_ErrorState: ", locErrorMsg);
			RunStatus = FALSE;
		}
		else if (lError != 0)
		{
			errorMsg.Format("%s:%d", "Error after Encoder Check: pEncoder->get_ErrorState: ", lError);
			RunStatus = FALSE;
		}
	}

	if (RunStatus)
	{
		CString theMsg;
		WMENC_ARCHIVE_STATE enumArchiveState;
		enumArchiveState = WMENC_ARCHIVE_RUNNING;
		BOOL ArchiveComplete = TRUE;
		while (enumArchiveState != WMENC_ARCHIVE_STOPPED)
		{
			hr = pEncoder->get_ArchiveState(WMENC_ARCHIVE_LOCAL, &enumArchiveState);
			if (enumArchiveState != WMENC_ARCHIVE_STOPPED && ArchiveComplete == TRUE)
			{
				theMsg = "Archive Operation In Progress...";
				CXUtils::WriteToTheLogFile(theMsg, TRUE);
				ArchiveComplete = FALSE;
			}
			CXUtils::YieldToTheOS();
		}
		
		theMsg = "Archive Operation Complete";
		CXUtils::WriteToTheLogFile(theMsg, TRUE);

		long lError;
		hr = pEncoder->get_ErrorState(&lError);

		if (WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error After Archive Check: pEncoder->get_ErrorState: ", locErrorMsg);
			RunStatus = FALSE;
		}
		else if (lError != 0)
		{
			errorMsg.Format("%s:%d", "Error After Archive Check: pEncoder->get_ErrorState: ", lError);
			RunStatus = FALSE;
		}
	}

	if (RunStatus)
	{
		CString theMsg;
		WMENC_INDEXER_STATE enumIndexerState;
		enumIndexerState = WMENC_INDEXER_RUNNING;
		BOOL IndexerComplete = TRUE;
		while (enumIndexerState != WMENC_INDEXER_STOPPED)
		{
			hr = pEncoder->get_IndexerState(&enumIndexerState);
			if (enumIndexerState != WMENC_INDEXER_STOPPED && IndexerComplete == TRUE)
			{
				theMsg = "Indexing Operation In Progress...";
				CXUtils::WriteToTheLogFile(theMsg, TRUE);
				IndexerComplete = FALSE;
			}
			CXUtils::YieldToTheOS();
		}
		
		theMsg = "Indexing Operation Complete";
		CXUtils::WriteToTheLogFile(theMsg, TRUE);

		long lError;
		hr = pEncoder->get_ErrorState(&lError);

		if (WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error After Indexing Check: pEncoder->get_ErrorState: ", locErrorMsg);
			RunStatus = FALSE;
		}
		else if (lError != 0)
		{
			errorMsg.Format("%s:%d", "Error After Indexing Check: pEncoder->get_ErrorState: ", lError);
			RunStatus = FALSE;
		}
	}



	if (pEncoder != NULL)
	{
		hr = pEncoder->PrepareToEncode(VARIANT_FALSE);

		// Stop the encoder engine.

	    hr = pEncoder->Stop();
	}
	
	CXUtils::SafeRelease( pFile);
	CXUtils::SafeRelease( pAudSrc);
	CXUtils::SafeRelease( pSrcGrp);
	CXUtils::SafeRelease(pSrcGrpColl);
	CXUtils::SafeRelease( pEncoder);

	return RunStatus;
}

BOOL CXUtils::DoDeviceEncoder(CString deviceName, CString outName, CString vodProfileName, CString &errorMsg)
{
	BOOL RunStatus = TRUE;
	HRESULT hr;
	IWMEncoder* pEncoder = NULL;
	IWMEncSourceGroupCollection* pSrcGrpColl = NULL;
	IWMEncSourceGroup* pSrcGrp = NULL;
	IWMEncSource* pAudSrc = NULL; 
	IWMEncBroadcast* pBrdcast;

	//	 IWMEncSource* pVidSrc;
//	IWMEncProfileCollection* pProColl;
//	IWMEncProfile* pPro;
	IWMEncFile* pFile = NULL;
	CString locErrorMsg;	
	
	// Initialize the COM library and retrieve a pointer
	// to an IWMEncoder interface.
	//	hr = CoInitialize(NULL);

	hr = CoCreateInstance(CLSID_WMEncoder,
		NULL,
		CLSCTX_INPROC_SERVER,
		IID_IWMEncoder,
		(void**) &pEncoder);
	
	if (RunStatus)
	{

		if (WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: CoCreateInstance:CLSID_WMEncoder:DoEncoder: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}
	// Retrieve a pointer to an IWMEncSourceGroupCollection
	// interface.
	
	if (RunStatus)
	{
		hr = pEncoder->get_SourceGroupCollection(&pSrcGrpColl);
		if (WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pEncoder->get_SourceGroupCollection: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}
	// Add an empty source group to the collection.
	
	if (RunStatus)
	{
		hr = pSrcGrpColl->Add(L"SG_1", &pSrcGrp);
		if (WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pSrcGrpColl->Add: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}
	// Add an audio and video stream to the source group.
	
	if (RunStatus)
	{
		hr = pSrcGrp->AddSource(WMENC_AUDIO, &pAudSrc);
		if (WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pSrcGrp->AddSource: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}
	//	 hr = pSrcGrp->AddSource(WMENC_VIDEO, &pVidSrc);
	
	// Retrieve a pointer to an IWMEncFile interface.
	
	if (RunStatus)
	{
		hr = pEncoder->get_File( &pFile );
		if (WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pEncoder->get_File: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}

	//	 hr = pEncoder->Load(L"C:\\filename.wme");
	// Specify an .avi source file and a Windows Media file with a .wmv extension for the output.
	
	CComBSTR m_bstrInFile(deviceName);
	CComBSTR m_bstrOutFile(outName);
	CComBSTR m_bstrFileName(L" ");

	if (RunStatus)
	{
		hr = pAudSrc->SetInput(m_bstrInFile);
		if (WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pAudSrc->SetInput: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}
	//	 hr = pVidSrc->SetInput(m_bstrInFile);


	if (RunStatus)
	{
		hr = pFile->put_LocalFileName(m_bstrOutFile);
		if (WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pFile->put_LocalFileName: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}

	// Retrieve the name of the output file.
	
	if (RunStatus)
	{
		hr = pFile->get_LocalFileName(&m_bstrFileName);
		if (WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pFile->get_LocalFileName: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}
	if (RunStatus)
	{
		BOOL foundProfile = FALSE;
		foundProfile = CXUtils::FindNamedEncoderProfile(pEncoder,	 
			pSrcGrp, 
			vodProfileName);
	
		if (foundProfile == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: Encoder profile not found for processing:", vodProfileName);
			RunStatus = FALSE;
		}
	}

	// Start archiving when the encoder engine starts. This is the default.
	
	if (RunStatus)
	{
		hr = pEncoder->put_EnableAutoArchive(VARIANT_TRUE);
		if (WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pEncoder->put_EnableAutoArchive: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}

	// Specify automatic indexing.
	
	if (RunStatus)
	{
		hr = pEncoder->put_AutoIndex(VARIANT_TRUE);
		if (WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pEncoder->put_AutoIndex: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}
	

	if (RunStatus)
	{
	    hr = pEncoder->get_Broadcast(&pBrdcast);
		if (WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pEncoder->get_Broadcast: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}

	if (RunStatus)
	{
	    hr = pBrdcast->put_PortNumber(WMENC_PROTOCOL_HTTP, 1175);
		if (WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pEncoder->put_PortNumber: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}

	// Initialize and start the encoder engine.
	
	if (RunStatus)
	{
		hr = pEncoder->PrepareToEncode(VARIANT_TRUE);
		if (WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pEncoder->PrepareToEncode: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}

	if (RunStatus)
	{
		CString theMsg;
		theMsg = "Starting Encoding Processing...";
		CXUtils::WriteToTheLogFile(theMsg, TRUE);
		hr = pEncoder->Start();
		if (WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pEncoder->Start: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}
	// To determine when the encoding and archiving process has ended, 
	// you must implement the OnArchiveStateChange event in the 
	// _IWMEncoderEvents interface.
	

	if (RunStatus)
	{
		WMENC_ENCODER_STATE enumEncoderState;
		enumEncoderState = WMENC_ENCODER_RUNNING;
		while (enumEncoderState != WMENC_ENCODER_STOPPED)
		{
			hr = pEncoder->get_RunState(&enumEncoderState);
			CXUtils::YieldToTheOS();
		}

		CString theMsg;
		theMsg = "Encoding Complete";
		CXUtils::WriteToTheLogFile(theMsg, TRUE);

		long lError;
		hr = pEncoder->get_ErrorState(&lError);

		if (WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error after Encoder Check: pEncoder->get_ErrorState: ", locErrorMsg);
			RunStatus = FALSE;
		}
		else if (lError != 0)
		{
			errorMsg.Format("%s:%d", "Error after Encoder Check: pEncoder->get_ErrorState: ", lError);
			RunStatus = FALSE;
		}
	}

	if (RunStatus)
	{
		CString theMsg;
		WMENC_ARCHIVE_STATE enumArchiveState;
		enumArchiveState = WMENC_ARCHIVE_RUNNING;
		BOOL ArchiveComplete = TRUE;
		while (enumArchiveState != WMENC_ARCHIVE_STOPPED)
		{
			hr = pEncoder->get_ArchiveState(WMENC_ARCHIVE_LOCAL, &enumArchiveState);
			if (enumArchiveState != WMENC_ARCHIVE_STOPPED && ArchiveComplete == TRUE)
			{
				theMsg = "Archive Operation In Progress...";
				CXUtils::WriteToTheLogFile(theMsg, TRUE);
				ArchiveComplete = FALSE;
			}
			CXUtils::YieldToTheOS();
		}
		
		theMsg = "Archive Operation Complete";
		CXUtils::WriteToTheLogFile(theMsg, TRUE);

		long lError;
		hr = pEncoder->get_ErrorState(&lError);

		if (WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error After Archive Check: pEncoder->get_ErrorState: ", locErrorMsg);
			RunStatus = FALSE;
		}
		else if (lError != 0)
		{
			errorMsg.Format("%s:%d", "Error After Archive Check: pEncoder->get_ErrorState: ", lError);
			RunStatus = FALSE;
		}
	}

	if (RunStatus)
	{
		CString theMsg;
		WMENC_INDEXER_STATE enumIndexerState;
		enumIndexerState = WMENC_INDEXER_RUNNING;
		BOOL IndexerComplete = TRUE;
		while (enumIndexerState != WMENC_INDEXER_STOPPED)
		{
			hr = pEncoder->get_IndexerState(&enumIndexerState);
			if (enumIndexerState != WMENC_INDEXER_STOPPED && IndexerComplete == TRUE)
			{
				theMsg = "Indexing Operation In Progress...";
				CXUtils::WriteToTheLogFile(theMsg, TRUE);
				IndexerComplete = FALSE;
			}
			CXUtils::YieldToTheOS();
		}
		
		theMsg = "Indexing Operation Complete";
		CXUtils::WriteToTheLogFile(theMsg, TRUE);

		long lError;
		hr = pEncoder->get_ErrorState(&lError);

		if (WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error After Indexing Check: pEncoder->get_ErrorState: ", locErrorMsg);
			RunStatus = FALSE;
		}
		else if (lError != 0)
		{
			errorMsg.Format("%s:%d", "Error After Indexing Check: pEncoder->get_ErrorState: ", lError);
			RunStatus = FALSE;
		}
	}



	if (pEncoder != NULL)
	{
		hr = pEncoder->PrepareToEncode(VARIANT_FALSE);

		// Stop the encoder engine.

	    hr = pEncoder->Stop();
	}
	
	CXUtils::SafeRelease( pFile);
	CXUtils::SafeRelease( pAudSrc);
	CXUtils::SafeRelease( pSrcGrp);
	CXUtils::SafeRelease(pSrcGrpColl);
	CXUtils::SafeRelease( pEncoder);

	return RunStatus;
}
/*
void CXUtils::CreateAudioProfile()
{
//	CoInitialize(NULL);
    HRESULT hr = S_OK;
    IWMProfile *pIWMProfile = NULL;
    AUDIO_PARAMS ap;
//    VIDEO_PARAMS rgvp[ 5 ];
//    SCRIPT_PARAMS sp;

//    ZeroMemory( rgvp, sizeof( rgvp ) );

//    sp.dwBitrate = DEFAULT_SCRIPT_STREAM_BANDWIDTH;

//    hr = CoInitialize( NULL );
//    if( FAILED( hr ) )
//    {
//        _tprintf( _T( "CoInitialize failed: (hr=0x%08x)\n" ), hr );
//        return( 1 );
//    }

    //
    // Create audio only profile
    //
    ap.dwFormatTag = CODEC_VIDEO_ACELP;
//    ap.dwFormatTag = CODEC_AUDIO_MSAUDIO;
    ap.dwBitrate = 8000;
    ap.dwSampleRate = 8000;
    ap.dwChannels = 1;

	wchar_t wszStr[] = L"VOD-14.wma";


    hr = GenerateProfile( wszStr, &ap, NULL, 0, NULL, &pIWMProfile);
    CXUtils::SafeRelease( pIWMProfile );
	
//	CoUninitialize();
}
*/

BOOL CXUtils::FindNamedEncoderProfile(IWMEncoder* pEncoder,    
									  IWMEncSourceGroup* pSrcGrp, 
									  CString theName)
{
	BOOL profileFound = FALSE;
    IWMEncProfileCollection* pProColl;
    IWMEncProfile* pPro;
	HRESULT hr;
    long lCount;
    short i;
    CComBSTR findName(theName);


	
	// Loop through the profile collection and retrieve a specific
	// profile. You can find the profile name in the Profile MAnager.
	
	hr = pEncoder->get_ProfileCollection(&pProColl);
	CComBSTR m_bstrName(L" ");
	CComVariant m_varProfile;
	hr = pProColl->get_Count(&lCount);
	for (i=0; i<lCount; i++)
	{
		hr = pProColl->Item(i, &pPro);
		hr = pPro->get_Name(&m_bstrName);
		if (_wcsicmp(m_bstrName, findName)==0)
		{
			m_varProfile.vt = VT_DISPATCH;
			profileFound = TRUE;
			m_varProfile.pdispVal = pPro;
			pSrcGrp->put_Profile(m_varProfile);
			break;
		}
	}
	return profileFound;
}

#endif

BOOL CXUtils::WasHResultGood(HRESULT inhr, CString &errorMsg)
{
	BOOL retVal = FALSE;
	if (inhr == S_OK)
		retVal = TRUE;
	else if (inhr == S_FALSE )
		retVal = FALSE;
	else if (inhr == E_INVALIDARG  )
		retVal = FALSE;
	else
		retVal = FALSE;

	if (retVal == FALSE)
	{
		errorMsg.Format("HRESULT ERROR: %d [0x%08X]", inhr);
	}

	return retVal;
}

BOOL CXUtils::DoExecute(CString CommandLine, CString& errorMsg, BOOL WaitUntilComplete)
{
	BOOL retStatus = TRUE;
	PROCESS_INFORMATION	pi;
	HANDLE handle = Execute(CommandLine, pi);
	if (handle > 0)
	{
		if (WaitUntilComplete)
		{
			    // Wait until child process exits.
			WaitForSingleObject( pi.hProcess, INFINITE );

			// Close process and thread handles. 
			CloseHandle( pi.hProcess );
			CloseHandle( pi.hThread );

//			DWORD exitCode = STILL_ACTIVE;
//			while (exitCode == STILL_ACTIVE)
//			{
//				CXUtils::YieldToTheOS();
//				GetExitCodeProcess(handle, &exitCode);
//			}
		}
	}
	else
	{
		errorMsg = "Error starting executable:" + CommandLine;
		retStatus = FALSE;
	}
	return retStatus;
}
HANDLE CXUtils::Execute(const char* CommandLine, PROCESS_INFORMATION& pi)
{
	STARTUPINFO			sui;
//	PROCESS_INFORMATION	pi;
	DWORD				ret;

	sui.cb               = sizeof (STARTUPINFO);
    sui.lpReserved       = 0;
    sui.lpDesktop        = NULL;
    sui.lpTitle          = NULL;
    sui.dwX              = 0;
    sui.dwY              = 0;
    sui.dwXSize          = 0;
    sui.dwYSize          = 0;
    sui.dwXCountChars    = 0;
    sui.dwYCountChars    = 0;
    sui.dwFillAttribute  = 0;
    sui.dwFlags          = 0;
    sui.wShowWindow      = 0;
    sui.cbReserved2      = 0;
    sui.lpReserved2      = 0;

	/**********************************************/
    /**********************************************/
    ret = ::CreateProcess (NULL,(char*)CommandLine, NULL, NULL,
                   FALSE, DETACHED_PROCESS | CREATE_NO_WINDOW,
                   NULL, NULL, &sui, &pi );
    /**********************************************/

	if (ret == TRUE)
    {
		return pi.hProcess;    	
	} 
    else
    {
      	return 0;
    }											   
}

void CXUtils::CleanupDirectories(CString ThePath, CString TheTempDirName)
{
	HANDLE TheFindHandle;
	BOOL   FileFound;
	WIN32_FIND_DATA TheFindData;
	CString TheDestFile; 

	TheDestFile = ThePath + "\\" + TheTempDirName;
	TheFindHandle = FindFirstFile(TheDestFile, &TheFindData);

	if (TheFindHandle != INVALID_HANDLE_VALUE)
		FileFound = TRUE;
	else
		FileFound = FALSE;
			
	CString TheFileToDelete;
	while (FileFound)
	{
		if ((TheFindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
		{
			CString theDirFound = TheFindData.cFileName;
			FileFound = FindNextFile(TheFindHandle, &TheFindData);
			if (theDirFound == "." || theDirFound == "..")
				continue;
			CString PathOfDelete = ThePath + "\\" + theDirFound;
			DeleteFileWildcard(PathOfDelete, "*.*");
			RemoveDirectory(PathOfDelete);
		}
		else
			FileFound = FindNextFile(TheFindHandle, &TheFindData);
	}
	FindClose(TheFindHandle);
}
void CXUtils::DeleteFileWildcard(CString ThePath, CString TheWildcardFilename)
{
	HANDLE TheFindHandle;
	BOOL   FileFound;
	WIN32_FIND_DATA TheFindData;
	CString TheDestFile; 

	TheDestFile = ThePath + "\\" + TheWildcardFilename;
	TheFindHandle = FindFirstFile(TheDestFile, &TheFindData);

	if (TheFindHandle != INVALID_HANDLE_VALUE)
		FileFound = TRUE;
	else
		FileFound = FALSE;
			
	CString TheFileToDelete;
	while (FileFound)
	{
		TheFileToDelete = ThePath + "\\" + TheFindData.cFileName;
		CString theDirFound = TheFindData.cFileName;

		if ((TheFindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
		{
			FileFound = FindNextFile(TheFindHandle, &TheFindData);
			if (theDirFound == "." || theDirFound == "..")
				continue;
			CXUtils::DeleteFileWildcard(TheFileToDelete, "*.*");
			RemoveDirectory(TheFileToDelete);
		}
		else
		{
			BOOL delstat = ::DeleteFile(TheFileToDelete);
			FileFound = FindNextFile(TheFindHandle, &TheFindData);
		}
	}
	FindClose(TheFindHandle);
}

void CXUtils::GetLastErrorString(CString& theMsg, DWORD lastErrorDWORD)
{
	DWORD theLastError;
	if (lastErrorDWORD == 0)
		theLastError = GetLastError();
	LPVOID lpMsgBuf;
	DWORD stat = FormatMessage( 
		FORMAT_MESSAGE_ALLOCATE_BUFFER | 
	    FORMAT_MESSAGE_FROM_SYSTEM | 
		FORMAT_MESSAGE_IGNORE_INSERTS,
	    NULL,
		theLastError,
	    MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
		(LPTSTR) &lpMsgBuf,
	    0,
		NULL 
	);

	theMsg = (LPCTSTR)lpMsgBuf;
	LocalFree( lpMsgBuf );
}

CString CXUtils::InitURLEncoder()
{

    /* The list of characters that are not encoded have been determined by
       referencing O'Reilly's "HTML: The Definitive Guide" (page 164). */
	CString dontNeedEncodingVals;
	int i;
	for (i = 'a'; i <= 'z'; i++) 
	{
	    dontNeedEncodingVals += (char)i;
	}
	for (i = 'A'; i <= 'Z'; i++) 
	{
	    dontNeedEncodingVals += (char)i;
	}
	for (i = '0'; i <= '9'; i++) 
	{
	    dontNeedEncodingVals += (char)i;
	}
	dontNeedEncodingVals += ' '; /* encoding a space to a + is done in the encode() method */
	dontNeedEncodingVals += '-';
	dontNeedEncodingVals += '_';
	dontNeedEncodingVals += '.';
	dontNeedEncodingVals += '*';

	return dontNeedEncodingVals;

}

CString CXUtils::URLEncoder(CString InString)
{
	CString OutString;
	CString HexString;

	int i;
	for (i = 0; i < InString.GetLength(); i++) 
	{
	    int c = (int)InString.GetAt(i);
	    if (dontNeedEncoding.Find(c) >= 0) 
		{
			if (c == ' ') 
			{
				c = '+';
			}
			OutString += ((char)c);
	    } 
		else 
		{
			// convert to external encoding before hex conversion

//			byte[] ba = buf.toByteArray();
			OutString += '%';

			HexString.Format("%.2x",c);
//			int TmpVal;
//			TmpVal = c >> 4 & 0xF;
//		    char ch = Character.forDigit((ba[j] >> 4) & 0xF, 16);
		    // converting to use uppercase letter as part of
		    // the hex value if ch is a letter.
//		    if (isalnum(HexString)) {
//				ch -= caseDiff;
//		    }
		    OutString += HexString;
//		    ch = Character.forDigit(ba[j] & 0xF, 16);
//		    if (Character.isLetter(ch)) {
//			ch -= caseDiff;
//		    }
//		    out.append(ch);
		}
//		buf.reset();
//	    }
	}

	return OutString;
}

CString CXUtils::FormatPhone(CString inPhone)
{
	CString outPhone;
	int i;
	for (i=0; i<inPhone.GetLength(); i++)
	{
		if (::isdigit(((char)inPhone.GetAt(i))))
		{
			outPhone.Append(inPhone.Mid(i,1));
		}
	}
	inPhone = outPhone;
	inPhone = PadString(inPhone, 10).MakeReverse();
	outPhone.Empty();
	for (i=0; i<10; i++)
	{
		if (i == 4)outPhone.Append("-");
		else if (i == 7) outPhone.Append(" )");
		
		outPhone.Append(inPhone.Mid(i,1));

		if (i == 9) outPhone.Append("(");
	}
	outPhone = outPhone.MakeReverse();
	return outPhone;
}

CString CXUtils::PadString(CString input, int padSize, CString padChar)
{
	CString outString;
	int amtToPad = 0;

	outString = input;

	if (padChar.GetLength() == 0)
	{
		padChar = " ";
	}
	else if (padChar.GetLength() > 1)
	{
		padChar = padChar.Left(1);
	}

	amtToPad = padSize - input.GetLength();
	while (outString.GetLength() < padSize)
	{
		outString.Append(padChar);
	}

	return outString;
}

CString CXUtils::decodeGetLastError()
{
	CString retValue;
	LPVOID lpMsgBuf;
	FormatMessage( 
		FORMAT_MESSAGE_ALLOCATE_BUFFER | 
	    FORMAT_MESSAGE_FROM_SYSTEM | 
		FORMAT_MESSAGE_IGNORE_INSERTS,
	    NULL,
		GetLastError(),
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
	    (LPTSTR) &lpMsgBuf,
		0,
		NULL 
	);

	retValue = (LPCTSTR)lpMsgBuf;
	LocalFree( lpMsgBuf );
	return retValue;
}


int CXUtils::SafeRelease(IUnknown* theValue)
{
	int retValue;
	if (theValue != NULL)
	{
		retValue = theValue->Release();
		if (retValue == 0)
		{
			theValue = NULL;
		}
	}
	return retValue;
}

BOOL CXUtils::CreateTheDirectory(CString currentDestinationDirectory, CString& errorMsg)
{
	BOOL RetStatus = TRUE;
	WIN32_FIND_DATA FindFileData;
	HANDLE theFindHandle;
	BOOL createDirStatus;

	currentDestinationDirectory.TrimRight();
	currentDestinationDirectory.TrimLeft();
	currentDestinationDirectory.TrimRight("\\");

	BOOL NotDone = TRUE;
	int slashPos;
	slashPos = currentDestinationDirectory.Find("\\");
	slashPos++;	//skip the first slash
	CString dirToCreate;
	while (RetStatus && (NotDone))
	{
		
		slashPos = currentDestinationDirectory.Find("\\", slashPos);
		dirToCreate = currentDestinationDirectory.Left(slashPos);
		if (dirToCreate.GetLength() == 0)
		{
			dirToCreate = currentDestinationDirectory;
			NotDone = FALSE;
		}
		slashPos++;	// jump forward to the next chunk;

		theFindHandle = FindFirstFile(dirToCreate, &FindFileData);
		
		if (theFindHandle == INVALID_HANDLE_VALUE)
		{
			createDirStatus = CreateDirectory(
				dirToCreate,                         // directory name
				NULL);
			if (createDirStatus == 0)
			{
				errorMsg.Format("Error creating directory: %s", dirToCreate);
				RetStatus = FALSE;
			}
		}
		FindClose(theFindHandle);
	}

	return RetStatus;
}
void CXUtils::SplitCStringOnCString(CString in, 
									   CString SplitString, 
									   CString &out1,
									   CString &out2)
{
	out1.Empty();
	out2.Empty();
	int charpos;
	charpos = in.Find(SplitString);
	if (charpos < 0)
	{
		out1 = in;
	}
	else
	{
		out1 = in.Left(charpos);
		out2 = in.Right(in.GetLength() - charpos - SplitString.GetLength());
	}
}

BYTE* CXUtils::ReadCStringFromBuffer(BYTE*& TheData, long NumBytes, CString &TheCString, long &BytesLeft)
{
	char* TmpBuffer;
	BYTE* MemPtr;
	
	MemPtr = TheData;
	TheCString.Empty();
	if (BytesLeft >= NumBytes)
	{
		if (NumBytes > 0)
		{
			TmpBuffer = new char[NumBytes+1];

			memcpy(TmpBuffer, TheData, NumBytes);
			TmpBuffer[NumBytes] = 0;
			BytesLeft -= NumBytes;

			TheCString = TmpBuffer;
			delete [] TmpBuffer;
		}
		MemPtr = TheData + NumBytes;
	}
	return MemPtr;
}

BOOL CXUtils::SafeLookup(CMapStringToString & theMap, CString TheKey, CString & TheValue, CString defaultValue)
{
	BOOL retValue;
	retValue = theMap.Lookup(TheKey, TheValue);
	if (!retValue)
	{
		TheValue = defaultValue;
	}
	return retValue;
}
