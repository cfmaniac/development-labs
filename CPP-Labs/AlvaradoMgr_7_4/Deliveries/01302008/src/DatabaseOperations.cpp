#include "stdafx.h"
#include "AlvaradoMgr.h"
#include ".\EncoderDlg.h"
#include ".\MyDatabase.h"
#include ".\MyRecordset.h"
#include ".\databaseoperations.h"
#include ".\XUtils.h"

CDatabaseOperations::CDatabaseOperations(void)
{
}

CDatabaseOperations::~CDatabaseOperations(void)
{
}

CMyDatabase* CDatabaseOperations::OpenDatabaseConnection(CString DSN_Name, CString DBPath, BOOL ReadOnly, BOOL Exclusive)
{
	CMyDatabase *theDB = NULL;
	CString connectString;
	CString theMessage;
	CString exclusiveString = "No";

	if (Exclusive)
	{
		exclusiveString = "Yes";
	}

	TRY
	{
		theDB = new CMyDatabase();


		CString connectStringFormat ="DSN=%s;UID=;PWD=;SourceDB=%s;";
		connectStringFormat += "SourceType=DBF;Exclusive=%s;BackgroundFetch=No;";
		connectStringFormat += "Collate=Machine;Null=Yes;Deleted=Yes;";
		connectString.Format(connectStringFormat, DSN_Name, DBPath, exclusiveString);

		if (theDB)
		{
			DWORD theOpenOptions = CDatabase::noOdbcDialog;
			if (ReadOnly) theOpenOptions |= CDatabase::openReadOnly;

			if (theDB->OpenEx(connectString, theOpenOptions) == false)
			{
				theDB = CloseAndDeleteCDatabase(theDB);
			}
			else
			{
				BOOL canTransact = theDB->CanTransact();
			}
		}
	}
	CATCH(CDBException, e)
	{
			theMessage.Format("%s DSN:%s; PATH:%s; Errors:%s; (%d)",
			"Error: Database Exception: OpenDatabaseConnection: ", 
			DSN_Name, DBPath, e->m_strError, e->m_nRetCode);
		CXUtils::WriteToTheLogFile(theMessage, true);
		CEncoderDlg::AddToEmailMessageList(theMessage);
		theDB = CloseAndDeleteCDatabase(theDB);
		// The error code is in e->m_nRetCode
	}
	END_CATCH		

	return theDB;
}
CMyDatabase* CDatabaseOperations::CloseAndDeleteCDatabase(CMyDatabase* theDB)
{
	if (theDB != NULL)
	{
		TRY
		{
			theDB->Close();
		}
		CATCH(CDBException, e)
		{
			// The error code is in e->m_nRetCode
		}
		END_CATCH		
		delete theDB;
		theDB = NULL;
	}
	return theDB;
}
/*
int CDatabaseOperations::GetNewSequenceNumber(CString sequenceFileKey, CString& theSeqNumCString)
{
	int returnSeq = -1;
	theSeqNumCString.Empty();

	CString theMessage;
	CString tmpValue;
	CString tmpKey;
	CMyDatabase* theDB = NULL;
	CString dsnName;
	CString dbPath;
	CString dbName;
	CEncoderDlg::propertyLookup("ODBC_DSN_NAME", dsnName);
	CEncoderDlg::propertyLookup("LOCAL_DB_BASE_DIR", dbPath);
	dbPath += "\\" + theParent->GetCurrentSchoolCode();

	theDB = OpenDatabaseConnection(dsnName, dbPath, false);

	if (theDB != NULL)
	{
		CString sequenceNumber;

		TRY
		{
			CMyRecordset rs(theDB);
			CString query;
			query = "SELECT NEXT_VAL FROM [sequence] WHERE FILE = ? ";
			rs.PrepareSQL(query, "sequence");
			rs.SetParam(0, sequenceFileKey, "FILE");
			rs.MyOpen(query, AFX_DB_USE_DEFAULT_TYPE );

			CString SequenceNum;
			BOOL entryFound = false;

			while(!rs.IsEOF())
			{
				entryFound = true;

				SequenceNum = rs.GetString(0);
				rs.MoveNext();
			}
			rs.Close();
			CXUtils::YieldToTheOS();
			if (entryFound == false)
			{
				theMessage.Format("Unable To Find Sequence For %s", sequenceFileKey);
				CXUtils::WriteToTheLogFile(theMessage, true);
				CEncoderDlg::AddToEmailMessageList(theMessage);
			}
			else
			{
				returnSeq = atoi(SequenceNum);
				theSeqNumCString.Format("%d",returnSeq);
				CString sequenceUpdateCmd;
				sequenceUpdateCmd.Format("UPDATE [sequence] SET NEXT_VAL = NEXT_VAL + 1 WHERE FILE = \"%s\" ", sequenceFileKey);
				theDB->ExecuteSQL(sequenceUpdateCmd);
			}
		}
		CATCH(CDBException, e)
		{
			theMessage.Format("%s; %s %s; %d","Error: Database Exception: ProcessStockRetrieval: School:", 
				theParent->GetCurrentSchoolCode(), e->m_strError, e->m_nRetCode);
			CXUtils::WriteToTheLogFile(theMessage, true);
			CEncoderDlg::AddToEmailMessageList(theMessage);
			// The error code is in e->m_nRetCode
		}
		END_CATCH		
	}

	theDB = CloseAndDeleteCDatabase(theDB);
	return returnSeq;
}
*/

void CDatabaseOperations::SetParent(CEncoderDlg* parent)
{
	theParent = parent;
}

int CDatabaseOperations::DoStockProcessing(CMyDatabase*& theDB, CMapStringToString& theRemoteData, CString& currentSchool)
{
	int responseCode = DB_SUCCESS;
	CString theMessage;

	CStringList theLocalData;

	CString tmpValue;
	CString tmpKey;
//	CMyDatabase* theDB = NULL;
	CString dsnName;
	CString dbPath;
	CString dbName;
	CEncoderDlg::propertyLookup("ODBC_DSN_NAME", dsnName);
	CEncoderDlg::propertyLookup("LOCAL_DB_BASE_DIR", dbPath);
	CString schoolCode = GetStringLookup(theRemoteData, "CMS_SCHOOL_CODE");
	if (schoolCode != currentSchool)
	{
		theDB = CloseAndDeleteCDatabase(theDB);
		currentSchool = schoolCode;
		CString schoolName = GetStringLookup(theRemoteData, "OEM_NAME");
		theDB = CloseAndDeleteCDatabase(theDB);
		currentSchool = schoolCode;
		theMessage.Format("\nProcessing Stock Items For: %s\n",  schoolName);
		CEncoderDlg::AddToEmailMessageList(theMessage);
	}
	dbPath += "\\" + schoolCode;

//	if (schoolCode.GetLength() == 0)
//		return DB_FAIL;;

	// *** The Data is stored in the input array as COLUMNNAME_COLUMNINDEX starting at index 0 (e.g. INV_NUMBER_0)

	if (theDB == NULL)
	{
		theDB = OpenDatabaseConnection(dsnName, dbPath);
	}
	if (theDB != NULL)
	{
		CString partNumber;
		CString partSize;
		CString queryPartNumber;
		CString itemID;
		CString itemTitle;
		CString onHandCount;
		CString cost;
		CString roysup;

		theRemoteData.Lookup("CMS_PART_NUMBER", queryPartNumber);
		theRemoteData.Lookup("ECOM_PART_NUMBER", partNumber);
		theRemoteData.Lookup("ECOM_OPTION_NAME", partSize);
		theRemoteData.Lookup("ECOM_ITEM_ID", itemID);
		theRemoteData.Lookup("ECOM_ITEM_TITLE", itemTitle);
		theRemoteData.Lookup("ON_HAND_COUNT", onHandCount);
		theRemoteData.Lookup("COST_PUBLIC_COST", cost);
		theRemoteData.Lookup("ROYSUP", roysup);

//		queryPartNumber = partNumber;
//		if (partSize.GetLength() != 0)
//		{
//			queryPartNumber = CXUtils::PadString(partNumber, 10);
//			queryPartNumber.Append(partSize);
//		}

		if (partNumber.GetLength() == 0)
		{
			theMessage.Format("%s %s; Title: %s","Remote Item has no Part Number: ITEM_ID:", itemID, itemTitle);
			CXUtils::WriteToTheLogFile(theMessage, true);
			CEncoderDlg::AddToEmailMessageList(theMessage);
		}
		else
		{
			int count = 0;
			BOOL entryFound = false;

			TRY
			{
				CMyRecordset rs(theDB);
				CString query;
				query.Format(
					"SELECT %s FROM [%s] WHERE NUMBER = '%s' ",
					"NUMBER,units,price1,roysup,isbn", 
					"stock", queryPartNumber);

//				rs.PrepareSQL(query, "stock");
//				rs.SetParam(0, queryPartNumber,"number");
				rs.MyOpen(query, AFX_DB_USE_DEFAULT_TYPE );

				CString CMS_number, CMS_Units, CMS_price1, CMS_roysup, CMS_isbn;

				bool sendAnUpdate = false;
				while(!rs.IsEOF())
				{
					entryFound = true;
					count++;
					DWORD x = rs.GetRowsFetched();
					rs.GetFieldValue((short)0,CMS_number);
					CMS_number.Trim();
					rs.GetFieldValue((short)1,CMS_Units);
					CMS_Units.Trim();
					rs.GetFieldValue((short)2,CMS_price1);
					CMS_price1.Trim();
					rs.GetFieldValue((short)3,CMS_roysup);
					CMS_roysup.Trim();
					rs.GetFieldValue((short)4,CMS_isbn);
					CMS_isbn.Trim();

					theMessage.Format("Item Differences from CMS: %s; %s:%s; %s; ", schoolCode, partNumber, partSize, itemTitle);
					BOOL diffFound = false;
					if (CMS_roysup != roysup)
					{
						sendAnUpdate = true;
						theMessage.Append("ROYSUP:" + CMS_roysup + ":" + roysup + ";");
						diffFound = true;
					}
					if (CMS_Units != onHandCount)
					{
						sendAnUpdate = true;
						theMessage.Append("Units:" + CMS_Units + ":" + onHandCount + ";");
						diffFound = true;
					}
					if (CMS_price1 != cost)
					{
						theMessage.Append("Price:" + CMS_price1 + ":" + cost + ";");
						diffFound = true;
					}
					if (diffFound)
					{
						CEncoderDlg::AddToEmailMessageList(theMessage);
					}
					theMessage.Empty();

					if (sendAnUpdate)
					{
						CString theBuffer;
						CString inventoryID;
						CString itemID;
						int index = CEncoderDlg::stockUpdateValues.GetSize();
						inventoryID = GetStringLookup(theRemoteData, "ECOM_INVENTORY_ID");
						itemID = GetStringLookup(theRemoteData, "ECOM_ITEM_ID");
						theBuffer.Format("&%s_%d=%s&%s_%d=%s&%s_%d=%s&%s_%d=%s", 
							"INV_ID", index, inventoryID,
							"ITEM_ID", index, itemID,
							"ONHAND", index, CMS_Units,
							"ROYSUP", index, CMS_roysup);
						CEncoderDlg::stockUpdateValues.AddTail(theBuffer);

					}

					rs.MoveNext();

					CXUtils::YieldToTheOS();
				}
				rs.Close();
				if (entryFound == false)
				{
					theMessage.Format("Remote Item Not Found in CMS: %s; %s:%s; %s", currentSchool, partNumber, partSize, itemTitle);
					CEncoderDlg::AddToEmailMessageList(theMessage);
				}
			}
			CATCH(CDBException, e)
			{
				theMessage.Format("%s; %s %s; %d","Error: Database Exception: ProcessStockRetrieval: School:", 
					schoolCode, e->m_strError, e->m_nRetCode);
				CXUtils::WriteToTheLogFile(theMessage, true);
				CEncoderDlg::AddToEmailMessageList(theMessage);
				responseCode = CDatabaseOperations::DB_QUERY_EXCEPTION;
				// The error code is in e->m_nRetCode
			}
			END_CATCH		

		}
	}
	else
	{
		responseCode = CDatabaseOperations::DB_CONNECT_FAIL;
	}

//	theDB = CloseAndDeleteCDatabase(theDB);
	return responseCode;
}
int CDatabaseOperations::DoStockNotOnRemoteProcessing(CMyDatabase*& theDB, CMapStringToString& theRemoteData, CString& currentSchool)
{
	int responseCode = DB_SUCCESS;
	CString theMessage;
	int totalFound = 0;
	int totalNotFound = 0;

	CStringList theLocalData;

	CString tmpValue;
	CString tmpKey;
//	CMyDatabase* theDB = NULL;
	CString dsnName;
	CString dbPath;
	CString dbName;
	CEncoderDlg::propertyLookup("ODBC_DSN_NAME", dsnName);
	CEncoderDlg::propertyLookup("LOCAL_DB_BASE_DIR", dbPath);
	CString schoolCode = GetStringLookup(theRemoteData, "CMS_SCHOOL_CODE");
	if (schoolCode != currentSchool)
	{
		theDB = CloseAndDeleteCDatabase(theDB);
		currentSchool = schoolCode;
		CString schoolName = GetStringLookup(theRemoteData, "OEM_NAME");
		theDB = CloseAndDeleteCDatabase(theDB);
		currentSchool = schoolCode;
		theMessage.Format("\nProcessing Stock Items For: %s\n",  schoolName);
		CEncoderDlg::AddToEmailMessageList(theMessage);
	}
	dbPath += "\\" + schoolCode;

	// *** The Data is stored in the input array as COLUMNNAME_COLUMNINDEX starting at index 0 (e.g. INV_NUMBER_0)

	if (theDB == NULL)
	{
		theDB = OpenDatabaseConnection(dsnName, dbPath);
	}
	if (theDB != NULL)
	{


			int count = 0;
			BOOL entryFound = false;

			TRY
			{
				CMyRecordset rs(theDB);
				CString query;
				query.Format(
					"SELECT %s FROM [%s] ",
					"NUMBER, DESC ", 
					"stock");

//				rs.PrepareSQL(query, "stock");
//				rs.SetParam(0, queryPartNumber,"number");
				rs.MyOpen(query, AFX_DB_USE_DEFAULT_TYPE );

				CString CMS_number, CMS_desc;

				bool sendAnUpdate = false;
				while(!rs.IsEOF())
				{
					entryFound = true;
					count++;
					rs.GetFieldValue((short)0,CMS_number);
					CMS_number.Trim();
					rs.GetFieldValue((short)1,CMS_desc);
					CMS_desc.Trim();

					CString foundItem;
					theRemoteData.Lookup(CMS_number, foundItem);

					if (foundItem.IsEmpty())
					{
						totalNotFound++;
						theMessage.Format("CMS Item Not Found on Remote System: %s; %s:%s", 
							currentSchool, CMS_number, CMS_desc);
						CEncoderDlg::AddToEmailMessageList(theMessage);
					}
					else
					{
						totalFound++;
					}
					theMessage.Empty();

					rs.MoveNext();

					CXUtils::YieldToTheOS();
				}
				rs.Close();
			}
			CATCH(CDBException, e)
			{
				theMessage.Format("%s; %s %s; %d","Error: Database Exception: DoStockNotOnRemoteProcessing: School:", 
					schoolCode, e->m_strError, e->m_nRetCode);
				CXUtils::WriteToTheLogFile(theMessage, true);
				CEncoderDlg::AddToEmailMessageList(theMessage);
				responseCode = CDatabaseOperations::DB_QUERY_EXCEPTION;
				// The error code is in e->m_nRetCode
			}
			END_CATCH		

	}
	else
	{
		responseCode = CDatabaseOperations::DB_CONNECT_FAIL;
	}

	theMessage.Format("%s %s; %s %d; %s %d;",
		"DoStockNotOnRemoteProcessing Complete: School:", 
		schoolCode, 
		"Exists On Remote System:",
		totalFound,
		"Missing From Remote System:",
		totalNotFound);
	CXUtils::WriteToTheLogFile(theMessage, true);
	CEncoderDlg::AddToEmailMessageList(theMessage);

	return responseCode;
}

int CDatabaseOperations::BuildOrderImport(CMapStringToString& theRemoteData, CStringArray& theOutData)
{

	BOOL doneLooping = false;
	int numProduct = atoi(GetStringLookup(theRemoteData,"ITEMCOUNT"));
	int productCounterStart = 0;
	int productCounterEnd = 5;
	CString continuedRecord = "";
	BOOL firstPass = true;

	while (!doneLooping && numProduct > 0)
	{
		if (firstPass)
		{
			firstPass = false;
		}
		else
		{
			productCounterStart += 5;
			productCounterEnd += 5;
			continuedRecord = "X";
			if (productCounterStart >= numProduct)
			{
				doneLooping = true;
				break;
			}
		}
		CString theLine;

		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_CUSTNUM"),10).Left(10));
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_ALTNUM"),15).Left(15));
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_LASTNAME"),20).Left(20));
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_FIRSTNAME"),15).Left(15));
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_COMPANY"),40).Left(40));
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_ADDRESS1"),40).Left(40));
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_ADDRESS2"),40).Left(40));

		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_CITY"),20).Left(20));
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_STATE"),3).Left(3));
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_ZIPCODE"),10).Left(10));
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_FOREIGN"),1).Left(1));
		theLine.Append(CXUtils::PadString(CXUtils::FormatPhone(GetStringLookup(theRemoteData,"INV_PHONE")),14).Left(14));
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_COMMENT"),40).Left(40));
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_CTYPE1"),1).Left(1));
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_CTYPE2"),2).Left(2));
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_CTYPE3"),4).Left(4));
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_TAXEXEMPT"),1).Left(1));
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_PROSPECT"),1).Left(1));

		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_CARDTYPE"),2).Left(2));
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_CARDNUM"),19).Left(19));
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_EXPIRES"),5).Left(5));

		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_SOURCE_KEY"),9).Left(9));	// SOURCE_KEY
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_CATALOG"),2).Left(2));	// CATALOG
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_SALES_ID"),3).Left(3));	// SALES_ID
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_OPER_ID"),3).Left(3));	// OPER_ID
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_REFERENCE"),10).Left(10));	// REFERENCE
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_SHIPVIA"),3).Left(3));	// SHIPVIA
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_FULFILLED"),1).Left(1));	// FULFILLED
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_PAID"),9).Left(9));	// PAID

		//*********************************************************************

		theLine.Append(CXUtils::PadString(continuedRecord,1).Left(1));	// CONTINUED 

		//  Place an X here if this record is a continuation of the previous record 
		//	which has more that 5 items for the same customer
		//*********************************************************************

		CString orderDate;
		orderDate.Format("%.2s/%.2s/%.2s", 
			GetStringLookup(theRemoteData,"INV_ORDER_DATE").Mid(5,2),
			GetStringLookup(theRemoteData,"INV_ORDER_DATE").Mid(8,2),
			GetStringLookup(theRemoteData,"INV_ORDER_DATE").Mid(2,2));
		theLine.Append(CXUtils::PadString(orderDate,8).Left(8));	//ORDER_DATE( 31)

		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_ODR_NUM"),8).Left(8));	// ODR_NUM	(32)




		CString partNumber;
		CString partNumberKey;
		CString partNumberOptionKey;
		CString partNumberQuantityKey;
		CString partSize;
		CString partQuantity;
		for (int i=productCounterStart; i<productCounterEnd; i++)
		{

			partNumberKey.Format("%s%d","INV_PRODUCT0",i);
			partNumberQuantityKey.Format("%s%d","INV_QUANTITY0",i);
			if (i >= numProduct)
			{
				theLine.Append(CXUtils::PadString("", 20).Left(20));		// PRODUCT0X 1-5
				theLine.Append(CXUtils::PadString("0.00",8).Left(8));		// QUANTITY0X 1-5
			}
			else
			{
				partNumber = GetStringLookup(theRemoteData,partNumberKey);
				partQuantity = GetStringLookup(theRemoteData,partNumberQuantityKey);
				theLine.Append(CXUtils::PadString(partNumber, 20).Left(20));		// PRODUCT0X 1-5
				theLine.Append(CXUtils::PadString(partQuantity,8).Left(8));			// QUANTITY0X 1-5
			}
		}

		
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_SLASTNAME"),20).Left(20));	//SLASTNAME (43)
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_SFIRSTNAME"),15).Left(15));	//SFIRSTNAME
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_SCOMPANY"),40).Left(40));														//SCOMPANY
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_SADDRESS1"),40).Left(40));		//SADDRESS1
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_SADDRESS2"),40).Left(40));		//SADDRESS2 (47)
		//48
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_SCITY"),20).Left(20));			//SCITY
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_SSTATE"),3).Left(3));		//SSTATE
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_SZIPCODE"),10).Left(10));	//SZIPCODE (50)
		//51
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_HOLDDATE"),8).Left(8));			//HOLDDATE
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_PAYMETHOD"),2).Left(2));			//PAYMETHOD
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_GREETING1"),35).Left(35));			//GREETING1
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_GREETING2"),35).Left(35));			//GREETING2
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_PROMOCRED"),8).Left(8));		//PROMOCRED
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_USEPRICES"),1).Left(1));			//USEPRICES

		CString price;
		CString priceKey;
		CString discountKey;
		CString discount;
		for (int i=productCounterStart; i<productCounterEnd; i++)
		{
			priceKey.Format("%s%d","INV_PRICE0",i);
			discountKey.Format("%s%d","INV_DISCOUNT0",i);
			if (i >= numProduct)
			{
				theLine.Append(CXUtils::PadString("", 9).Left(9));		// PRICE0X 1-5
				theLine.Append(CXUtils::PadString("0",2).Left(2));		// DISCOUNT0X 1-5
			}
			else
			{

				price = GetStringLookup(theRemoteData,priceKey);
				discount = GetStringLookup(theRemoteData,discountKey);
				theLine.Append(CXUtils::PadString(price, 9).Left(9));		// PRICE0X 1-5
				theLine.Append(CXUtils::PadString(discount,2).Left(2));		// DISCOUNT0X 1-5
			}
		}
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_USESHIPAMT"),1).Left(1));	//USESHIPAMT (67)
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_SHIPPING"),8).Left(8));	//SHIPPING (68)
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_EMAIL"),50).Left(50));	//EMAIL (69)

		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_INTERNETID"),32).Left(32));	//INTERNETID (70)
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_COUNTRY"),3).Left(3));	//COUNTRY (71)
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_SCOUNTRY"),3).Left(3));	//SCOUNTRY (72)

		theLine.Append(CXUtils::PadString(CXUtils::FormatPhone(GetStringLookup(theRemoteData,"INV_PHONE2")),14).Left(14));	//PHONE2 (73)
		theLine.Append(CXUtils::PadString(CXUtils::FormatPhone(GetStringLookup(theRemoteData,"INV_SPHONE")),14).Left(14));	//SPHONE(74)
		theLine.Append(CXUtils::PadString(CXUtils::FormatPhone(GetStringLookup(theRemoteData,"INV_SPHONE2")),14).Left(14));	//SPHONE2(75)
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_EMAIL"),50).Left(50));	//SEMAIL(76)
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_ORDERTYPE"),6).Left(6));	//ORDERTYPE(77)
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_INPART"),1).Left(1));	//INPART(78)
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_TITLE"),40).Left(40));	//TITLE(79)
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_SALU"),6).Left(6));	//SALU(80)
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_HONO"),6).Left(6));	//HONO(81)
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_EXT"),5).Left(5));	//EXT(82)
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_EXT2"),5).Left(5));	//EXT2(83)

		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_STITLE"),40).Left(40));	//TITLE(84)
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_SSALU"),6).Left(6));	//SALU(85)
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_SHONO"),6).Left(6));	//HONO(86)
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_SEXT"),5).Left(5));	//EXT(87)
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_SEXT2"),5).Left(5));	//EXT2(88)

		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_SHIP_WHEN"),8).Left(8));	//SHIP_WHEN(89)
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_GREETING3"),35).Left(35));	//GREETING3(90)
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_GREETING4"),35).Left(35));	//GREETING4(91)
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_GREETING5"),35).Left(35));	//GREETING5(92)
		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_GREETING6"),35).Left(35));	//GREETING6(93)

		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_PASSWORD"),20).Left(20));	//PASSWORD(94)

		CString custom;
		CString customKey;
		for (int i=productCounterStart; i<productCounterEnd; i++)
		{
			customKey.Format("%s%d","INV_CUSTOM0",i);
			if (i >= numProduct)
			{
				theLine.Append(CXUtils::PadString("", 240).Left(240));		// CUSTOM0X 1-5
			}
			else
			{
				custom = GetStringLookup(theRemoteData,customKey);
				theLine.Append(CXUtils::PadString(custom,240).Left(240));	// CUSTOM0X 1-5
			}
		}

		theLine.Append(CXUtils::PadString(GetStringLookup(theRemoteData,"INV_INTERNET"),1).Left(1));	//INTERNET(100)

		theOutData.Add(theLine);

	}






	return 0;
}
/*
int CDatabaseOperations::DoNewOrderProcessing(CMapStringToString& theRemoteData)
{
	int responseCode = DB_SUCCESS;
	CString theMessage;

	//*** Available columns are loaded in DBVTableColumns

	CStringList theLocalData;

	CString tmpValue;
	CString tmpKey;
	CMyDatabase* theDB = NULL;
	CString dsnName;
	CString dbPath;
	CString dbName;
	CEncoderDlg::propertyLookup("ODBC_DSN_NAME", dsnName);
	CEncoderDlg::propertyLookup("LOCAL_DB_BASE_DIR", dbPath);
	dbPath += "\\" + theParent->GetCurrentSchoolCode();

	// *** The Data is stored in the input array as COLUMNNAME_COLUMNINDEX starting at index 0 (e.g. INV_NUMBER_0)

	if (theDB == NULL)
	{
		theDB = OpenDatabaseConnection(dsnName, dbPath, false, true);
	}
	if (theDB != NULL)
	{
		CString partNumber;
		CString partSize;
		CString queryPartNumber;
		CString itemID;
		CString itemTitle;
		CString onHandCount;
		CString cost;

		CString accountID;
		CString ecomAccountID;
		CString invoiceID;
		theRemoteData.Lookup("INV_ACCOUNT_ID", accountID);
		theRemoteData.Lookup("INV_ECOM_ACCOUNT_ID", ecomAccountID);
		if (atoi(accountID) <= 0)
			accountID = ecomAccountID;
		theRemoteData.Lookup("INV_ECOM_INVOICE_ID", invoiceID);

		if (accountID.GetLength() == 0)
		{
			theMessage.Format("%s %s","Account ID Not Found In Invoice", invoiceID);
			CXUtils::WriteToTheLogFile(theMessage, true);
			CEncoderDlg::AddToEmailMessageList(theMessage);
		}
		else
		{
			BOOL entryFound = false;

			TRY
			{
//				theDB->PrepareSQLColumns("cust");

				CMyRecordset rs(theDB);
				CString query;

				CString custCols[] = {"custnum", "altnum", "custtype", "lastname", "firstname", "company", "addr", "addr2", "city",
					"county", "state", "zipcode", "country", "phone", "phone2", "orig_ad", "ctype", "last_ad", "catcount", "odr_date",
					"paymethod", "cardnum", "cardtype", "exp", "shiplist", "expired", "badcheck", "orderrec", "net", "gross",
					"ord_freq", "comment", "custbal", "custref", "discount", "exempt", 
					"ar_balance", "credit_lim", "disct_days", "due_days", "disct_pct", 
					"promo_bal", "comment2", "sales_id", "nomail", "belongnum", 
					"ctype2", "ctype3", "salu", "title", "delpoint",
					"carroute", "ncoachange", "entrydate", "searchcomp", "email", 
					"n_exempt", "tax_id", "cashonly", "hono", "noemail", 
					"password", "rfm", "points", "norent", "addr_type", 
					"web", "extension", "extension2", "date_limit", "start_date", 
					"end_date", "from_month", "from_day", "to_month", "to_day", 
					"addrissame", "lastuser" };

				query = "SELECT ";
				for (int i=0; i<(sizeof(custCols)/sizeof(*custCols)); i++)
				{
					query.Append(custCols[i]);
					query.Append(",");
				}			
				query.TrimRight(",");
				query.Append(" from [cust] where Altnum = ? ");


				rs.PrepareSQL(query, "cust");
				rs.SetParam(0, accountID, "Altnum");

				rs.MyOpen(query, AFX_DB_USE_DEFAULT_TYPE );

				CString CustNum;

				while(!rs.IsEOF())
				{
					entryFound = true;
					rs.GetFieldValue((short)0,CustNum);
					CustNum.Trim();
					rs.MoveNext();

					CXUtils::YieldToTheOS();
				}
				rs.Close();

				if (entryFound == false)
				{
					CString newSeqString;
					int newSeq = CDatabaseOperations::GetNewSequenceNumber("CUST", newSeqString);
					CString insertSQLFormat1;
					CString insertSQLFormat2;
					CString insertSQLPart1;
					CString insertSQLPart2;


					query = " insert into [cust] (Custnum, Altnum, Lastname, Firstname, Custtype, ";
					query.Append("Addr, Addr2, City, State, Zipcode, Country, Phone, Phone2, ");
					query.Append("Net, Gross, Cardnum, Cardtype, Exp, Company , ");
					query.Append("Orig_ad, Ctype, Last_ad, Catcount, Paymethod, County,  ");
					query.Append("Shiplist, Expired, Badcheck, Orderrec, Ord_freq, Comment, Exempt,   ");
					query.Append(" Odr_date, custbal, custref");
					query.Append(" ) values ( ");
					query.Append(" ?, ?, ?, ?, ?, ");
					query.Append(" ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ");
					query.Append(" ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ");
					query.Append(" ?, ?, ? ");
					query.Append(" ) ");

//#define	SQL_UNKNOWN_TYPE	0
//#define SQL_CHAR            1
//#define SQL_NUMERIC         2
//#define SQL_DECIMAL         3
//#define SQL_INTEGER         4
//#define SQL_SMALLINT        5
//#define SQL_FLOAT           6
//#define SQL_REAL            7
//#define SQL_DOUBLE          8
//#if (ODBCVER >= 0x0300)
//#define SQL_DATETIME        9
//#endif
//#define SQL_VARCHAR        12


					TIMESTAMP_STRUCT orderDate;
					TIMESTAMP_STRUCT currentDateTime;
					memset(&orderDate, 0, sizeof(orderDate));
					orderDate.day = atoi(GetStringLookup(theRemoteData,"INV_INVOICE_DATE").Mid(8,2));
					orderDate.month = atoi(GetStringLookup(theRemoteData,"INV_INVOICE_DATE").Mid(5,2));
					orderDate.year = atoi(GetStringLookup(theRemoteData,"INV_INVOICE_DATE").Mid(0,4));
					CTime CurrentTime = CTime::GetCurrentTime();
					CurrentTime.GetAsDBTIMESTAMP((DBTIMESTAMP&)currentDateTime);


					theDB->PrepareSQL(query,"cust");
					int index = 0;
					theDB->SetParam(index++, newSeqString, "Custnum");
					theDB->SetParam(index++, accountID, "Altnum");
					theDB->SetParam(index++, GetStringLookup(theRemoteData,"INV_SHIP_LAST_NAME"), "Lastname");
					theDB->SetParam(index++, GetStringLookup(theRemoteData,"INV_SHIP_FIRST_NAME"), "Firstname");
					theDB->SetParam(index++, "O", "Custtype");
					theDB->SetParam(index++, GetStringLookup(theRemoteData,"INV_SHIP_ADDRESS1"),"Addr"); 
					theDB->SetParam(index++, GetStringLookup(theRemoteData,"INV_SHIP_ADDRESS2"),"Addr2"); 
					theDB->SetParam(index++, GetStringLookup(theRemoteData,"INV_SHIP_CITY"),"City"); 
					theDB->SetParam(index++, GetStringLookup(theRemoteData,"INV_SHIP_STATE_PROV"),"State"); 
					theDB->SetParam(index++, GetStringLookup(theRemoteData,"INV_SHIP_POSTAL_CODE"),"Zipcode"); 
					theDB->SetParam(index++, GetStringLookup(theRemoteData,"INV_SHIP_COUNTRY_CODE"),"Country"); 
					theDB->SetParam(index++, GetStringLookup(theRemoteData,"INV_SHIP_PHONE_1"),"Phone"); 
					theDB->SetParam(index++, GetStringLookup(theRemoteData,"INV_SHIP_PHONE_2"),"Phone2");
					theDB->SetParam(index++, GetStringLookup(theRemoteData,"INV_NET_AMOUNT"),"Net");
					theDB->SetParam(index++, GetStringLookup(theRemoteData,"INV_GROSS_AMOUNT"),"Gross");
					theDB->SetParam(index++, "4411","Cardnum");
					theDB->SetParam(index++, "m","Cardtype");
					theDB->SetParam(index++, "11/05","Exp");
					theDB->SetParam(index++, "myco","Company");
					theDB->SetParam(index++, "","Orig_ad");
					theDB->SetParam(index++, "","Ctype");
					theDB->SetParam(index++, "","Last_ad");
					theDB->SetParam(index++, "","Catcount");
					theDB->SetParam(index++, "","Paymethod");
					theDB->SetParam(index++, "","County");
					theDB->SetParam(index++, "","Shiplist");
					theDB->SetParam(index++, "","Expired");
					theDB->SetParam(index++, "","Badcheck");
					theDB->SetParam(index++, "","Orderrec");
					theDB->SetParam(index++, "","Ord_freq");
					theDB->SetParam(index++, "","Comment");
					theDB->SetParam(index++, "","Exempt");

					theDB->SetParam_DateTime(index++, orderDate, "odr_date");
					theDB->SetParam(index++, "", "custbal");
					theDB->SetParam(index++, "", "custref");



					theDB->ExecuteSQL(query);

					theMessage.Format("Remote Item Not Found in CMS: %s; %s:%s; %s", theParent->GetCurrentSchoolCode(), partNumber, partSize, itemTitle);
					CEncoderDlg::AddToEmailMessageList(theMessage);
				}
			}
			CATCH(CDBException, e)
			{
				theMessage.Format("%s; %s %s; %d","Error: Database Exception: DoNewOrderProcessing: School:", 
					theParent->GetCurrentSchoolCode(), e->m_strError, e->m_nRetCode);
				CXUtils::WriteToTheLogFile(theMessage, true);
				CEncoderDlg::AddToEmailMessageList(theMessage);
				responseCode = CDatabaseOperations::DB_QUERY_EXCEPTION;
				// The error code is in e->m_nRetCode
			}
			END_CATCH		

		}
	}
	else
	{
		responseCode = CDatabaseOperations::DB_CONNECT_FAIL;
	}

	theDB = CloseAndDeleteCDatabase(theDB);
	return responseCode;
}
*/
int CDatabaseOperations::BuildStatusUpdateResponse(CMyDatabase*& theDB, CMapStringToString& theRemoteData, CString& currentSchool)
{
	int responseCode = DB_SUCCESS;
	CString theMessage;

	CStringList theLocalData;

	CString tmpValue;
	CString tmpKey;
//	CMyDatabase* theDB = NULL;
	CString dsnName;
	CString dbPath;
	CString dbName;
	CEncoderDlg::propertyLookup("ODBC_DSN_NAME", dsnName);
	CEncoderDlg::propertyLookup("LOCAL_DB_BASE_DIR", dbPath);
	CString schoolCode = GetStringLookup(theRemoteData, "CMS_SCHOOL_CODE");
	if (schoolCode != currentSchool)
	{
		CString schoolName = GetStringLookup(theRemoteData, "OEM_NAME");
		theDB = CloseAndDeleteCDatabase(theDB);
		currentSchool = schoolCode;
		theMessage.Format("Processing In-Progress Invoices For:%s",  schoolName);
		CEncoderDlg::AddToEmailMessageList(theMessage);
	}
	dbPath += "\\" + schoolCode;

//	if (schoolCode.GetLength() == 0)
//		return DB_FAIL;;

	// *** The Data is stored in the input array as COLUMNNAME_COLUMNINDEX starting at index 0 (e.g. INV_NUMBER_0)

	if (theDB == NULL)
	{
		theDB = OpenDatabaseConnection(dsnName, dbPath);
	}
	if (theDB != NULL)
	{

		int orderID;
		CString orderIDCString;
		CString invoiceID;
		theRemoteData.Lookup("INV_ECOM_INVOICE_ID", invoiceID);

		orderID = GetOrderIDFromAltOrder(theDB, invoiceID, orderIDCString);
		if (orderID > 0)
		{
			CString partNumber;
			CString partSize;
			CString queryPartNumber;
			CString itemID;
			CString itemTitle;
			CString onHandCount;
			CString cost;
			CString roysup;

			theRemoteData.Lookup("II_PART_NUMBER", partNumber);
			theRemoteData.Lookup("IIO_ECOM_ITEM_OPTION_NAME", partSize);
			theRemoteData.Lookup("II_ECOM_ITEM_ID", itemID);

			queryPartNumber = partNumber;
			if (partSize.GetLength() != 0)
			{
				queryPartNumber = CXUtils::PadString(partNumber, 10);
				queryPartNumber.Append(partSize);
			}

			if (partNumber.GetLength() == 0)
			{
				theMessage.Format("%s %s","Remote Item has no Part Number: ITEM_ID:", itemID);
				CXUtils::WriteToTheLogFile(theMessage, true);
				CEncoderDlg::AddToEmailMessageList(theMessage);

			}
			else
			{
				int count = 0;
				BOOL entryFound = false;
				responseCode = CDatabaseOperations::DB_PARAMETER_ERROR;

				TRY
				{
					CMyRecordset rs(theDB);
					CString query;


					//	ordered by item_id, trans_id.  If an item_id shows up
					CString whereClause;
//					whereClause.Format(" order = %d and item = '%s' order by trans_id", orderID, queryPartNumber);
					whereClause.Format(" order = %d order by trans_id", orderID);
					query.Format(
						"SELECT %s FROM [%s] WHERE %s ",
						"item_id, inpart, box, item_state, item" , "items",
						whereClause);

					//				rs.PrepareSQL(query, "stock");
					//				rs.SetParam(0, queryPartNumber,"number");
					rs.MyOpen(query, AFX_DB_USE_DEFAULT_TYPE );

					CString CMS_itemID, CMS_inpart, CMS_box, CMS_item_state, CMS_item;

					bool sendAnUpdate = false;

					CMapStringToString theEntriesFound;	// this is used to ignore return entries
					CString entryValue;

					int indexToProcess = atoi(GetStringLookup(theRemoteData, "INVOICE_ITEM_INDEX"));
					int curIndex = 0;
					while(!rs.IsEOF())
					{
						if (indexToProcess > curIndex)
						{
							curIndex++;
							rs.MoveNext();
							CXUtils::YieldToTheOS();
							continue;
						}
						else if (indexToProcess < curIndex)
						{
							curIndex++;
							rs.MoveNext();
							CXUtils::YieldToTheOS();
							break;
						}
						curIndex++;

						entryFound = true;
						count++;
						DWORD x = rs.GetRowsFetched();
						rs.GetFieldValue((short)0,CMS_itemID);
						CMS_itemID.Trim();
						rs.GetFieldValue((short)1,CMS_inpart);
						CMS_inpart.Trim();
						rs.GetFieldValue((short)2,CMS_box);
						CMS_box.Trim();
						rs.GetFieldValue((short)3,CMS_item_state);
						CMS_item_state.Trim();
						rs.GetFieldValue((short)4,CMS_item);
						CMS_item.Trim();

						int boxID = atoi(CMS_box);
						CString trackNum;			

						CString theBuffer;
						CString invoiceID;
						CString itemID;
						int index = CEncoderDlg::statusUpdateValues.GetSize();
						invoiceID = GetStringLookup(theRemoteData, "INV_ECOM_INVOICE_ID");
						itemID = GetStringLookup(theRemoteData, "II_ECOM_INVOICE_ITEM_ID");

						if (GetStringLookup(theEntriesFound, CMS_itemID).GetLength() <= 0 && CMS_item == queryPartNumber)
						{
							theEntriesFound.SetAt(CMS_itemID, CMS_itemID);

							if (CMS_item_state == "SH")
							{
								GetBoxTrackingInfo(theDB, orderID, boxID, CMS_inpart, trackNum);
							}

							theMessage.Format("Status Updates from CMS; Invoice: %s; Item: %s:%s; CMS Item:%s; TrackNum: %s ", 
								invoiceID, partNumber, partSize, CMS_itemID, trackNum);
							CEncoderDlg::AddToEmailMessageList(theMessage);

							theMessage.Empty();
							theBuffer.Format("&%s_%d=%s&%s_%d=%s&%s_%d=%s&%s_%d=%s", 
								"INVOICE_ID", index, invoiceID,
								"ITEM_ID", index, itemID,
								"ITEM_STATE", index, CMS_item_state,
								"TRACKNUM", index, trackNum);
							CEncoderDlg::statusUpdateValues.AddTail(theBuffer);

						}
//						--  this may need to be case insensitive...
						else if (CMS_item != queryPartNumber)
						{
							theMessage.Format("Discarded Updates from mismatch item ; Invoice: %s; Item: %s:%s; Item:%s; CMS Item:%s; TrackNum: %s ", 
								invoiceID, partNumber, partSize, CMS_item, CMS_itemID, trackNum);
							CEncoderDlg::AddToEmailMessageList(theMessage);
						}
						else if (CMS_item != queryPartNumber)
						{
							theMessage.Format("Discarded Updates from RETURN Entry; Invoice: %s; Item: %s:%s; CMS Item:%s; TrackNum: %s ", 
								invoiceID, partNumber, partSize, CMS_itemID, trackNum);
							CEncoderDlg::AddToEmailMessageList(theMessage);
						}
						rs.MoveNext();
						CXUtils::YieldToTheOS();
					}
					rs.Close();
					if (entryFound == false)
					{
						theMessage.Format("Tracking info Not Found in CMS: %s; %s:%s;", invoiceID, partNumber, partSize);
						CEncoderDlg::AddToEmailMessageList(theMessage);
					}
				}
				CATCH(CDBException, e)
				{
					theMessage.Format("%s; %s %s; %d","Error: Database Exception: ProcessStockRetrieval: ", 
						schoolCode, e->m_strError, e->m_nRetCode);
					CXUtils::WriteToTheLogFile(theMessage, true);
					CEncoderDlg::AddToEmailMessageList(theMessage);
					responseCode = CDatabaseOperations::DB_QUERY_EXCEPTION;
					// The error code is in e->m_nRetCode
				}
				END_CATCH		

			}
		}
		else
		{
			responseCode = orderID;
		}
	}
	else
	{
		responseCode = CDatabaseOperations::DB_CONNECT_FAIL;
	}

//	theDB = CloseAndDeleteCDatabase(theDB);
	return responseCode;
}

int CDatabaseOperations::GetOrderIDFromAltOrder(CMyDatabase* theDB, CString altOrder, CString& orderID)
{
	int returnOrderID = 0;
	orderID.Empty();

	CString theMessage;
	CString tmpValue;
	CString tmpKey;

	if (theDB != NULL)
	{

		TRY
		{
			CMyRecordset rs(theDB);
			CString query;
			query.Format("SELECT ORDER FROM [cms] WHERE ALT_ORDER = '%s' ", altOrder);
			rs.MyOpen(query, AFX_DB_USE_DEFAULT_TYPE );

			CString tmpReturnOrderID;
			BOOL entryFound = false;

			while(!rs.IsEOF())
			{
				entryFound = true;

				tmpReturnOrderID = rs.GetString(0);
				rs.MoveNext();
			}
			rs.Close();
			CXUtils::YieldToTheOS();
			if (entryFound == false)
			{
				theMessage.Format("Unable To Find Order For ALT_ORDER: %s", altOrder);
				CXUtils::WriteToTheLogFile(theMessage, true);
				CEncoderDlg::AddToEmailMessageList(theMessage);
				returnOrderID = DB_FAIL;
			}
			else
			{
				returnOrderID = atoi(tmpReturnOrderID);
				orderID.Format("%d",returnOrderID);
			}
		}
		CATCH(CDBException, e)
		{
			theMessage.Format("%s; %s; %d","Error: Database Exception: GetOrderIDFromAltOrder", 
				e->m_strError, e->m_nRetCode);
			CXUtils::WriteToTheLogFile(theMessage, true);
			CEncoderDlg::AddToEmailMessageList(theMessage);
			returnOrderID = DB_QUERY_EXCEPTION;
			// The error code is in e->m_nRetCode
		}
		END_CATCH		
	}

//	theDB = CloseAndDeleteCDatabase(theDB);
	return returnOrderID;
}

int CDatabaseOperations::GetBoxTrackingInfo(CMyDatabase* theDB, int Order, int boxID, CString inPart, CString& trackNum)
{
	int returnVal = -1;
	trackNum.Empty();

	CString theMessage;
	CString tmpValue;
	CString tmpKey;

	if (theDB != NULL)
	{

		TRY
		{
			CMyRecordset rs(theDB);
			CString query;
			query.Format("SELECT trackingno FROM [box] WHERE ORDER = %d AND box = %d and inpart = '%s' ", Order, boxID, inPart);
			rs.MyOpen(query, AFX_DB_USE_DEFAULT_TYPE );

			CString tmpReturnOrderID;
			BOOL entryFound = false;

			while(!rs.IsEOF())
			{
				entryFound = true;

				trackNum = rs.GetString(0);
				trackNum.Trim();
				rs.MoveNext();
			}
			rs.Close();
			CXUtils::YieldToTheOS();
			if (entryFound == false)
			{
				theMessage.Format("Unable To Find box entry for order %d, box %d, inpart %s", Order, boxID, inPart);
				CXUtils::WriteToTheLogFile(theMessage, true);
				CEncoderDlg::AddToEmailMessageList(theMessage);
			}
		}
		CATCH(CDBException, e)
		{
			theMessage.Format("%s; %s; %d","Error: Database Exception: GetBoxTrackingInfo", 
				e->m_strError, e->m_nRetCode);
			CXUtils::WriteToTheLogFile(theMessage, true);
			CEncoderDlg::AddToEmailMessageList(theMessage);
			// The error code is in e->m_nRetCode
		}
		END_CATCH		
	}

//	theDB = CloseAndDeleteCDatabase(theDB);
	return returnVal;
}
CString CDatabaseOperations::GetStringLookup(CMapStringToString& map, CString key)
{
	CString theValue;
	map.Lookup(key, theValue);
	return theValue;
}