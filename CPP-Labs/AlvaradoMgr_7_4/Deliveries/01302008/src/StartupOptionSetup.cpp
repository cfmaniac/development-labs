// StartupOptionSetup.cpp : implementation file
//

#include "stdafx.h"
#include "AlvaradoMgr.h"
#include "StartupOptionSetup.h"
#include ".\startupoptionsetup.h"


// CStartupOptionSetup dialog

IMPLEMENT_DYNAMIC(CStartupOptionSetup, CDialog)
CStartupOptionSetup::CStartupOptionSetup(CWnd* pParent /*=NULL*/)
	: CDialog(CStartupOptionSetup::IDD, pParent)
{
	b_ImportOrders = false;
	b_UpdateInvoiceStatus = false;
	b_TestEmail = false;
	b_DumpConfig = false;
	b_ShowCmdLineArgs = false;
	m_hIcon = AfxGetApp()->LoadIcon(IDI_ICON1);

}

CStartupOptionSetup::~CStartupOptionSetup()
{
}

BOOL CStartupOptionSetup::OnInitDialog() 
{
	CDialog::OnInitDialog();
	SetIcon(m_hIcon, TRUE);
	SetIcon(m_hIcon, FALSE);
	CWnd* theOK = GetDlgItem(IDOK);
	theOK->EnableWindow(false);
	
	return TRUE;
}
void CStartupOptionSetup::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CStartupOptionSetup, CDialog)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, OnBnClickedCancel)
	ON_BN_CLICKED(IDC_IMPORT_ORDERS, OnBnClickedImportOrders)
	ON_BN_CLICKED(IDC_UPDATE_INVOICE_STATUS, OnBnClickedUpdateInvoiceStatus)
	ON_BN_CLICKED(IDC_DUMP_CONFIG, OnBnClickedDumpConfig)
	ON_BN_CLICKED(IDC_TEST_EMAIL, OnBnClickedTestEmail)
	ON_WM_SYSCOMMAND()
	ON_BN_CLICKED(IDC_SHOW_CMD_ARGS, OnBnClickedShowCmdArgs)
END_MESSAGE_MAP()


// CStartupOptionSetup message handlers

void CStartupOptionSetup::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	OnOK();
}

void CStartupOptionSetup::OnBnClickedCancel()
{
	// TODO: Add your control notification handler code here
	OnCancel();
}

void CStartupOptionSetup::OnBnClickedImportOrders()
{
	CWnd* theOK = GetDlgItem(IDOK);
	if (!theOK->IsWindowEnabled())
		theOK->EnableWindow();
	b_ImportOrders = true;
	b_UpdateInvoiceStatus = false;
	b_TestEmail = false;
	b_DumpConfig = false;
	b_ShowCmdLineArgs = false;
	// TODO: Add your control notification handler code here
}

void CStartupOptionSetup::OnBnClickedUpdateInvoiceStatus()
{
	// TODO: Add your control notification handler code here
	CWnd* theOK = GetDlgItem(IDOK);
	if (!theOK->IsWindowEnabled())
		theOK->EnableWindow();
	b_ImportOrders = false;
	b_UpdateInvoiceStatus = true;
	b_TestEmail = false;
	b_DumpConfig = false;
	b_ShowCmdLineArgs = false;
}

void CStartupOptionSetup::OnBnClickedDumpConfig()
{
	// TODO: Add your control notification handler code here
	CWnd* theOK = GetDlgItem(IDOK);
	if (!theOK->IsWindowEnabled())
		theOK->EnableWindow();
	b_ImportOrders = false;
	b_UpdateInvoiceStatus = false;
	b_TestEmail = false;
	b_DumpConfig = true;
	b_ShowCmdLineArgs = false;
}

void CStartupOptionSetup::OnBnClickedTestEmail()
{
	// TODO: Add your control notification handler code here
	CWnd* theOK = GetDlgItem(IDOK);
	if (!theOK->IsWindowEnabled())
		theOK->EnableWindow();
	b_ImportOrders = false;
	b_UpdateInvoiceStatus = false;
	b_TestEmail = true;
	b_DumpConfig = false;
	b_ShowCmdLineArgs = false;
}

void CStartupOptionSetup::OnSysCommand(UINT nID, LPARAM lParam)
{
	// TODO: Add your message handler code here and/or call default

    if ((nID & 0xFFF0) == SC_KEYMENU)
        return;
	if (nID == SC_CLOSE)
		return;
	CDialog::OnSysCommand(nID, lParam);
}

void CStartupOptionSetup::OnBnClickedShowCmdArgs()
{
	// TODO: Add your control notification handler code here
	CWnd* theOK = GetDlgItem(IDOK);
	if (!theOK->IsWindowEnabled())
		theOK->EnableWindow();
	b_ImportOrders = false;
	b_UpdateInvoiceStatus = false;
	b_TestEmail = false;
	b_DumpConfig = false;
	b_ShowCmdLineArgs = true;
}
