#pragma once
#include "afxwin.h"
#include "afxcoll.h"


// CStockInfoUpdateDlg dialog

class CStockInfoUpdateDlg : public CDialog
{
	DECLARE_DYNAMIC(CStockInfoUpdateDlg)

public:
	CStockInfoUpdateDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CStockInfoUpdateDlg();

// Dialog Data
	enum { IDD = IDD_STOCK_UPDATE_CHOICE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CString s_messageText;
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedReportOnly();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();
	bool b_doSync;
	bool b_doReportOnly;
	CListBox c_fileListBox;
	void AddEntryToList(CString entryToAdd);
	CStringArray fileList;
};
