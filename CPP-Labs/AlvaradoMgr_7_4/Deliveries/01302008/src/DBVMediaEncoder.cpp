// DBVMediaEncoder.cpp: implementation of the CDBVMediaEncoder class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "AlvaradoMgr.h"
#include "DBVMediaEncoder.h"
#include "wmencode.h"
#include "EncoderCallbacks.h"
#include "XUtils.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDBVMediaEncoder::CDBVMediaEncoder()
{
	pEncoder = NULL;
	pSrcGrpColl = NULL;
	pSrcGrp = NULL;
	pAudSrc = NULL;
	pVidSrc = NULL;
	pBrdcast = NULL;
	pFile = NULL;
	archiveIndex = -1;
	broadcastPort = "1175";

	waitingToDelete = false;
	archiveFlag = true;
	archivingComplete = FALSE;
	indexingComplete = FALSE;
	ftpUploadComplete = FALSE;
	ftpUploadInProgress = FALSE;
	theCallbacks.setCDBVMediaEncoder(this);
	ftpThreadID = -1;
	encodingStoppedReported = FALSE;


}

CDBVMediaEncoder::~CDBVMediaEncoder()
{

}

void CDBVMediaEncoder::getNextFtpUploadFile(CString& localFilename, CString& remoteFilename)
{
	CString tmpFilename;
	CString theFullPathArchiveName;
	if (theArchiveFiles.GetCount() > 0)
	{
		tmpFilename = theArchiveFiles.GetHead();
		theFullPathArchiveName.Format("%s\\%s", archiveDirectory, tmpFilename);
		localFilename = theFullPathArchiveName;
		remoteFilename = tmpFilename;

	}
}

BOOL CDBVMediaEncoder::isReadyForFtpUpload()
{
	BOOL retValue = TRUE;
	if (isTheEncoderRunning())
	{
		retValue = FALSE;
	}
	else
	{
		if (archiveFlag)
		{
			if (!archivingComplete || !indexingComplete)
			{
				retValue = FALSE;
			}
			else if (ftpUploadInProgress == TRUE)
			{
				retValue = FALSE;
			}
			else if (ftpUploadComplete == TRUE)
			{
				retValue = FALSE;
			}
		}
	}
	if (retValue)
	{
		if (pEncoder != NULL)
		{
		    pEncoder->PrepareToEncode(VARIANT_FALSE);
		}
	}
	return retValue;
}
BOOL CDBVMediaEncoder::isFtpUploadComplete()
{
	return ftpUploadComplete;
}

void CDBVMediaEncoder::setFtpUploadInProgress(BOOL flag)
{
	ftpUploadInProgress = flag;
}
BOOL CDBVMediaEncoder::isFtpUploadInProgress()
{
	return ftpUploadInProgress;
}
void CDBVMediaEncoder::setDoArchiveFlag(CString flag)
{
	if (flag == "1")
	{
		setDoArchiveFlag(true);
	}
	else
	{
		setDoArchiveFlag(false);
	}
}
void CDBVMediaEncoder::setDoArchiveFlag(BOOL flag)
{
	archiveFlag = flag;
	if (archiveFlag == false)
	{
		setArchiveCompleteFlag(true);
		setIndexingCompleteFlag(true);
		setFtpUploadCompleteFlag(true);
	}
}
void CDBVMediaEncoder::setArchiveCompleteFlag(BOOL flag)
{
	archivingComplete = flag;
}
void CDBVMediaEncoder::setIndexingCompleteFlag(BOOL flag)
{
	indexingComplete = flag;
}
void CDBVMediaEncoder::setFtpUploadCompleteFlag(BOOL flag)
{
//	ftpUploadComplete = flag;
	if (flag)
	{
		ftpUploadInProgress = FALSE;
		if (theArchiveFiles.GetCount() > 0)
		{
			theArchiveFiles.RemoveHead();
		}
		if (theArchiveFiles.GetCount() <= 0)
		{
			ftpUploadComplete = TRUE;
		}
		else
		{
			ftpUploadComplete = FALSE;
		}
	}
}

void CDBVMediaEncoder::setWaitingToDeleteFlag(BOOL flag)
{
	waitingToDelete = flag;
}
BOOL CDBVMediaEncoder::getWaitingToDeleteFlag()
{
	return waitingToDelete;
}

BOOL CDBVMediaEncoder::isOKtoDestroy()
{
	BOOL retValue = TRUE;
	if (isTheEncoderRunning())
	{
		retValue = FALSE;
	}
	else
	{
		if (archiveFlag)
		{
			if (!archivingComplete || !indexingComplete)
			{
				retValue = FALSE;
			}
			if (!ftpUploadComplete)
			{
				retValue = FALSE;
			}
		}
	}
	if (retValue)
	{
		if (pEncoder != NULL)
		{
		    pEncoder->PrepareToEncode(VARIANT_FALSE);
		}
	}
	return retValue;
}

void CDBVMediaEncoder::BuildStatusResponse(CString & theResponse)
{
}

BOOL CDBVMediaEncoder::DoDeviceEncoder(CString audioDeviceName, CString videoDeviceName,
									   CString outArchiveName, CString vodProfileName)
{
	BOOL RunStatus = TRUE;
	HRESULT hr;

	rootArchiveName = outArchiveName;

//	IWMEncProfileCollection* pProColl;
//	IWMEncProfile* pPro;
	CString locErrorMsg;	
	
	// Initialize the COM library and retrieve a pointer
	// to an IWMEncoder interface.
	//	hr = CoInitialize(NULL);

	hr = CoCreateInstance(CLSID_WMEncoder,
		NULL,
		CLSCTX_INPROC_SERVER,
		IID_IWMEncoder,
		(void**) &pEncoder);
	
	if (RunStatus)
	{

		if (CXUtils::WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: CoCreateInstance:CLSID_WMEncoder:DoEncoder: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}

	if (RunStatus)
	{
		theCallbacks.Init(pEncoder);
	}

	// Retrieve a pointer to an IWMEncSourceGroupCollection
	// interface.
	if (RunStatus)
	{
		hr = pEncoder->get_SourceGroupCollection(&pSrcGrpColl);
		if (CXUtils::WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pEncoder->get_SourceGroupCollection: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}
	// Add an empty source group to the collection.
	
	if (RunStatus)
	{
		hr = pSrcGrpColl->Add(CComBSTR("SG_1"), &pSrcGrp);
		if (CXUtils::WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pSrcGrpColl->Add: ", locErrorMsg);
			RunStatus = FALSE;

			IErrorInfo *pErrorInfo;
		    hr = GetErrorInfo(0, &pErrorInfo);
			if (SUCCEEDED(hr) && pErrorInfo != NULL) 
			{
				BSTR theDesc;
				pErrorInfo->GetDescription(&theDesc);

				CXUtils::SafeRelease(pErrorInfo);  // don't forget to release!
				CString theValue;
				
				theValue = theDesc;
				CString IErrorInfoMsg;
			
				IErrorInfoMsg.Format("IErrorInfo Error: %s", theValue);
				CXUtils::WriteToTheLogFile(IErrorInfoMsg);
			}
		}
	}
	// Add an audio and video stream to the source group.

//	deleteing this causes an exception!
//	CXUtils::SafeRelease(pSrcGrpColl);
	
	if (RunStatus)
	{
		hr = pSrcGrp->AddSource(WMENC_AUDIO, &pAudSrc);
		if (CXUtils::WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: audio pSrcGrp->AddSource: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}

	if (RunStatus && (videoDeviceName.GetLength() > 0))
	{
		hr = pSrcGrp->AddSource(WMENC_VIDEO, &pVidSrc);
		if (CXUtils::WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: video pSrcGrp->AddSource: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}
	
		//	 hr = pEncoder->Load(L"C:\\filename.wme");
	// Specify an .avi source file and a Windows Media file with a .wmv extension for the output.
	
	CComBSTR m_bstrInFile(audioDeviceName);
	CComBSTR m_bstrVidInFile(videoDeviceName);
	CComBSTR m_bstrFileName(L" ");

	if (RunStatus)
	{
		hr = pAudSrc->SetInput(m_bstrInFile);
		if (CXUtils::WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pAudSrc->SetInput: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}
//	CXUtils::SafeRelease(pAudSrc);


	if (RunStatus && (pVidSrc != NULL))
	{
		hr = pVidSrc->SetInput(m_bstrVidInFile);
		if (CXUtils::WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pVidSrc->SetInput: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}
//	CXUtils::SafeRelease(pVidSrc);
	//	 hr = pVidSrc->SetInput(m_bstrInFile);

/*
	if (RunStatus)
	{
		hr = pEncoder->get_File( &pFile );
		if (CXUtils::WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pEncoder->get_File: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}
	if (RunStatus)
	{
		CComBSTR m_bstrOutFile(outArchiveName);
		hr = pFile->put_LocalFileName(m_bstrOutFile);
		if (CXUtils::WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pFile->put_LocalFileName: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}
*/

	// Retrieve the name of the output file.
	
//	if (RunStatus)
//	{
//		hr = pFile->get_LocalFileName(&m_bstrFileName);
//		if (CXUtils::WasHResultGood(hr, locErrorMsg) == FALSE)
//		{
//			errorMsg.Format("%s:%s", "Error: pFile->get_LocalFileName: ", locErrorMsg);
//			RunStatus = FALSE;
//		}
//	}
	if (RunStatus)
	{
		BOOL foundProfile = FALSE;
		foundProfile = FindNamedEncoderProfile(pEncoder,
			pSrcGrp, 
			vodProfileName);
	
		if (foundProfile == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: Encoder profile not found for processing:", vodProfileName);
			RunStatus = FALSE;
		}
	}

//	CXUtils::SafeRelease(pSrcGrp);

	// Start archiving when the encoder engine starts. This is the default.
	
	if (RunStatus && archiveFlag)
	{
		hr = pEncoder->put_EnableAutoArchive(VARIANT_TRUE);
		if (CXUtils::WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pEncoder->put_EnableAutoArchive: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}

	// Specify automatic indexing.
	
	if (RunStatus)
	{
		hr = pEncoder->put_AutoIndex(VARIANT_TRUE);
		if (CXUtils::WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pEncoder->put_AutoIndex: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}
	

	if (RunStatus)
	{
	    hr = pEncoder->get_Broadcast(&pBrdcast);
		if (CXUtils::WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pEncoder->get_Broadcast: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}

	if (RunStatus)
	{
	    hr = pBrdcast->put_PortNumber(WMENC_PROTOCOL_HTTP, atoi(broadcastPort));
		if (CXUtils::WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pEncoder->put_PortNumber: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}
	if (RunStatus)
	{
		CXUtils::SafeRelease(pBrdcast);
	}


	// Initialize and start the encoder engine.
	if (RunStatus)
	{
		RunStatus = startTheEncoder();	
	}
	else
	{
		CXUtils::WriteToTheLogFile(errorMsg);
	}
	
	// To determine when the encoding and archiving process has ended, 
	// you must implement the OnArchiveStateChange event in the 
	// _IWMEncoderEvents interface.
	return RunStatus;
}	
BOOL CDBVMediaEncoder::startTheEncoder()
{ 
	BOOL RunStatus = true;

	HRESULT hr;
	CString locErrorMsg;

	// Retrieve a pointer to an IWMEncFile interface.
	
	if (RunStatus)
	{
		hr = pEncoder->get_File( &pFile );
		if (CXUtils::WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pEncoder->get_File: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}

	if (RunStatus && archiveFlag)
	{
		CString theFullPathArchiveName;
		CString theArchiveFilename;
		archiveIndex++;
		CString theArchiveExtension;

		if (getArchiveType() == "VIDEO")
		{
			theArchiveExtension = "wmv";
		}
		else
		{
			theArchiveExtension = "wma";
		}

		theArchiveFilename.Format("%s.%s.%s.%.2d.%s",rootArchiveName,eventGMTDate,speedKey.Left(2),archiveIndex,theArchiveExtension);

		theFullPathArchiveName.Format("%s\\%s", archiveDirectory, theArchiveFilename);

		CComBSTR m_bstrOutFile(theFullPathArchiveName);
		if (CXUtils::DoesFileExist(theFullPathArchiveName))
		{
			::DeleteFile(theFullPathArchiveName);
		}
		hr = pFile->put_LocalFileName(m_bstrOutFile);
		if (CXUtils::WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pFile->put_LocalFileName: ", locErrorMsg);
			RunStatus = FALSE;
		}
		else
		{
			theArchiveFiles.AddTail(theArchiveFilename);
		}
	}
	CXUtils::SafeRelease(pFile);


	if (RunStatus)
	{
		hr = pEncoder->PrepareToEncode(VARIANT_TRUE);
		if (CXUtils::WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pEncoder->PrepareToEncode: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}

	if (RunStatus)
	{
		CString theMsg;
		theMsg = "Starting Encoding Processing...";
		CXUtils::WriteToTheLogFile(theMsg, TRUE);
		hr = pEncoder->Start();
		if (CXUtils::WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error: pEncoder->Start: ", locErrorMsg);
			RunStatus = FALSE;
		}
	}

	if (RunStatus == FALSE)
	{
		IErrorInfo *pErrorInfo;
        hr = GetErrorInfo(0, &pErrorInfo);
        if (SUCCEEDED(hr) && pErrorInfo != NULL) 
		{
			BSTR theDesc;
			pErrorInfo->GetDescription(&theDesc);

			CXUtils::SafeRelease(pErrorInfo);  // don't forget to release!
			CString theValue;
			
			theValue = theDesc;
			CString IErrorInfoMsg;
			
			IErrorInfoMsg.Format("IErrorInfo Error: %s", theValue);
			CXUtils::WriteToTheLogFile(IErrorInfoMsg);
		}
		
		CXUtils::WriteToTheLogFile(errorMsg);
		TRACE("%s\n",errorMsg);
	}
//	SAFE_RELEASE(pFile);
//	if (RunStatus)
//	{
//		waitForEncoderToStop();
//	}
	return RunStatus;
}

BOOL CDBVMediaEncoder::isTheEncoderRunning()
{ 
	BOOL retVal = false;
	HRESULT hr;
	CString locErrorMsg;	

	WMENC_ENCODER_STATE enumEncoderState;
	enumEncoderState = WMENC_ENCODER_RUNNING;
	hr = pEncoder->get_RunState(&enumEncoderState);
	if (enumEncoderState == WMENC_ENCODER_RUNNING)
	{
		retVal = true;
	}
	return retVal;
}

void CDBVMediaEncoder::waitForEncoderToStop()
{
	while (isTheEncoderRunning())
	{
		CXUtils::YieldToTheOS();
	}
	long lError;
	pEncoder->get_ErrorState(&lError);
}
BOOL CDBVMediaEncoder::stopTheEncoder()
{ 
	BOOL RunStatus = true;
	HRESULT hr;
	CString locErrorMsg;	

	if (RunStatus)
	{
		if (isTheEncoderRunning())
		{

		    hr = pEncoder->Stop();
			if (CXUtils::WasHResultGood(hr, locErrorMsg) == FALSE)
			{
				errorMsg.Format("%s:%s", "Error: pEncoder->Stop: ", locErrorMsg);
				RunStatus = FALSE;
			}
		}
		waitForEncoderToStop();
//	    hr = pEncoder->PrepareToEncode(VARIANT_FALSE);
//		if (CXUtils::WasHResultGood(hr, locErrorMsg) == FALSE)
//		{
//			errorMsg.Format("%s:%s", "Error: pEncoder->PrepareToEncode: ", locErrorMsg);
//			RunStatus = FALSE;
//		}

		CString theMsg;
		theMsg = "Encoding Complete";
		CXUtils::WriteToTheLogFile(theMsg, TRUE);

		long lError;
		hr = pEncoder->get_ErrorState(&lError);

		if (CXUtils::WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error after Encoder Check: pEncoder->get_ErrorState: ", locErrorMsg);
			RunStatus = FALSE;
		}
		else if (lError != 0)
		{
			errorMsg.Format("%s:%d [0x%08X]", "Error after Encoder Check: pEncoder->get_ErrorState: ", lError);
			RunStatus = FALSE;
		}
	}
	if (RunStatus == FALSE)
	{
           IErrorInfo *pErrorInfo;
            hr = GetErrorInfo(0, &pErrorInfo);
			if (SUCCEEDED(hr) && pErrorInfo != NULL) 
			{
				BSTR theDesc;
				pErrorInfo->GetDescription(&theDesc);
               // FINALLY can call methods on pErrorInfo!
               // ...and handle the error!
               CXUtils::SafeRelease(pErrorInfo);  // don't forget to release!
            }
		CXUtils::WriteToTheLogFile(errorMsg);
	}
	return RunStatus;
}

BOOL CDBVMediaEncoder::pauseTheEncoder()
{ 
	BOOL RunStatus = true;
	HRESULT hr;
	CString locErrorMsg;	

	if (RunStatus)
	{
		if (isTheEncoderRunning())
		{
		    hr = pEncoder->Pause();
		}
//		waitForEncoderToStop();

		CString theMsg;
		theMsg = "Encoding Paused";
		CXUtils::WriteToTheLogFile(theMsg, TRUE);

		long lError;
		hr = pEncoder->get_ErrorState(&lError);

		if (CXUtils::WasHResultGood(hr, locErrorMsg) == FALSE)
		{
			errorMsg.Format("%s:%s", "Error after Encoder Check: pEncoder->get_ErrorState: ", locErrorMsg);
			RunStatus = FALSE;
		}
		else if (lError != 0)
		{
			errorMsg.Format("%s:%d", "Error after Encoder Check: pEncoder->get_ErrorState: ", lError);
			RunStatus = FALSE;
		}
	}
	return RunStatus;
}

BOOL CDBVMediaEncoder::isArchiverRunning()
{ 
	BOOL retVal = true;
	BOOL RunStatus = true;
	HRESULT hr;
	CString locErrorMsg;	

	if (RunStatus)
	{
		CString theMsg;
		WMENC_ARCHIVE_STATE enumArchiveState;
		enumArchiveState = WMENC_ARCHIVE_RUNNING;
		BOOL ArchiveRunning = FALSE;
			
		hr = pEncoder->get_ArchiveState(WMENC_ARCHIVE_LOCAL, &enumArchiveState);
		if (enumArchiveState != WMENC_ARCHIVE_STOPPED)
		{
			theMsg = "Archive Operation In Progress...";
			CXUtils::WriteToTheLogFile(theMsg, TRUE);
			ArchiveRunning = TRUE;
		}

		if (ArchiveRunning == false)
		{
			theMsg = "Archive Operation Complete";
			CXUtils::WriteToTheLogFile(theMsg, TRUE);

			long lError;
			hr = pEncoder->get_ErrorState(&lError);

			if (CXUtils::WasHResultGood(hr, locErrorMsg) == FALSE)
			{
				errorMsg.Format("%s:%s", "Error After Archive Check: pEncoder->get_ErrorState: ", locErrorMsg);
				RunStatus = FALSE;
			}
			else if (lError != 0)
			{
				errorMsg.Format("%s:%d", "Error After Archive Check: pEncoder->get_ErrorState: ", lError);
				RunStatus = FALSE;
			}
		}
		retVal = ArchiveRunning;
	}
	return retVal;
}

BOOL CDBVMediaEncoder::isIndexerRunning()
{ 
	BOOL retVal = true;
	BOOL RunStatus = true;
	HRESULT hr;
	CString locErrorMsg;	

	if (RunStatus)
	{
		CString theMsg;
		WMENC_INDEXER_STATE enumIndexerState;
		enumIndexerState = WMENC_INDEXER_RUNNING;
		BOOL IndexerRunning = false;

		hr = pEncoder->get_IndexerState(&enumIndexerState);
		if (enumIndexerState != WMENC_INDEXER_STOPPED)
		{
			theMsg = "Indexing Operation In Progress...";
			CXUtils::WriteToTheLogFile(theMsg, TRUE);
			IndexerRunning = true;
		}
		
		if (IndexerRunning == false)
		{
			theMsg = "Indexing Operation Complete";
			CXUtils::WriteToTheLogFile(theMsg, TRUE);

			long lError;
			hr = pEncoder->get_ErrorState(&lError);

			if (CXUtils::WasHResultGood(hr, locErrorMsg) == FALSE)
			{
				errorMsg.Format("%s:%s", "Error After Indexing Check: pEncoder->get_ErrorState: ", locErrorMsg);
				RunStatus = FALSE;
			}
			else if (lError != 0)
			{
				errorMsg.Format("%s:%d", "Error After Indexing Check: pEncoder->get_ErrorState: ", lError);
				RunStatus = FALSE;
			}
		}
		retVal = IndexerRunning;
	}
	return retVal;

}

BOOL CDBVMediaEncoder::FindNamedEncoderProfile(IWMEncoder* pEncoder,    
									  IWMEncSourceGroup* pSrcGrp, 

									  CString theName)
{
	BOOL profileFound = FALSE;
    IWMEncProfileCollection* pProColl;
    IWMEncProfile* pPro;
	HRESULT hr;
    long lCount;
    short i;
    CComBSTR findName(theName);


	
	// Loop through the profile collection and retrieve a specific
	// profile. You can find the profile name in the Profile MAnager.
	
	hr = pEncoder->get_ProfileCollection(&pProColl);
	CComBSTR m_bstrName(L" ");
	CComVariant m_varProfile;
	hr = pProColl->get_Count(&lCount);
	for (i=0; i<lCount; i++)
	{
		hr = pProColl->Item(i, &pPro);
		hr = pPro->get_Name(&m_bstrName);
		if (_wcsicmp(m_bstrName, findName)==0)
		{
			m_varProfile.vt = VT_DISPATCH;
			profileFound = TRUE;
			m_varProfile.pdispVal = pPro;
			pSrcGrp->put_Profile(m_varProfile);
//			CXUtils::SafeRelease(pPro);
			break;
		}
		else
		{
			CXUtils::SafeRelease(pPro);
		}
	}

	CXUtils::SafeRelease(pProColl);
	return profileFound;
}


void CDBVMediaEncoder::CleanupEncoderInterfaces(BOOL releaseDevicesOnly)
{
	isOKtoDestroy();
// Retrieve the number of audio and video streams in
// the source group.
/*
	HRESULT hr;
    short iAudCount = 0;
	short iVidCount = 0;

	if (pSrcGrp != NULL)
	{
//		hr = pSrcGrp->get_SourceCount(WMENC_AUDIO, &iAudCount);
//		hr = pSrcGrp->get_SourceCount(WMENC_VIDEO, &iVidCount);
	}

	// Remove the video stream.
	// You must verify that the count is not zero before deleting
	// the source stream.

	CComVariant varIndex;
	varIndex.vt = VT_I2;
	varIndex.iVal = 0;
	if (iVidCount != 0)
	{
		if (pSrcGrp != NULL)
		{
//			hr = pSrcGrp->RemoveSource(WMENC_AUDIO, varIndex);
		}
	}
	if (iAudCount != 0)
	{
		if (pSrcGrp != NULL)
		{
//			hr = pSrcGrp->RemoveSource(WMENC_VIDEO, varIndex);
	    }
	}
*/

//	CXUtils::SafeRelease( pFile);
//	CXUtils::SafeRelease( pAudSrc);
//	CXUtils::SafeRelease( pSrcGrp);
//	CXUtils::SafeRelease(pSrcGrpColl);



//	Test Code
//
//
//
	CComVariant varIndex;
	short iAudCount, iVidCount;

	varIndex.vt = VT_I2;
	varIndex.iVal = 0;

	HRESULT hr = pSrcGrp->PrepareToEncode(VARIANT_FALSE);
	hr = pSrcGrp->get_SourceCount(WMENC_AUDIO, &iAudCount);
	if(iAudCount != 0)
	hr = pSrcGrp->RemoveSource(WMENC_AUDIO, varIndex);

	hr = pSrcGrp->get_SourceCount(WMENC_VIDEO, &iVidCount);
	if(iVidCount != 0)
	hr = pSrcGrp->RemoveSource(WMENC_VIDEO, varIndex);


//	hr = pProColl->Refresh();

//	hr = pSrcPlugMgr->Refresh();

	hr = pSrcGrpColl->Remove(varIndex);

//?
	CXUtils::SafeRelease( pSrcGrpColl);
//?



//	end test code



	IWMEncProfile*  ppProfile;
	CString locErrorMsg;
	hr = pSrcGrp->get_Profile(&ppProfile);
	CXUtils::SafeRelease(pSrcGrp);
//?
	CXUtils::SafeRelease( pAudSrc);
	CXUtils::SafeRelease( pVidSrc);
//?
	if (CXUtils::WasHResultGood(hr, locErrorMsg) == TRUE)
	{
		CXUtils::SafeRelease(ppProfile);
	}

	if (pEncoder != NULL)
	{
		theCallbacks.ShutDown(pEncoder);
//?		CXUtils::SafeRelease( pSrcGrpColl);
		CXUtils::SafeRelease( pEncoder);
	}
	
/*	CXUtils::SafeRelease( pAudSrc);
	CXUtils::SafeRelease( pVidSrc);
	CXUtils::SafeRelease( pSrcGrp);
	CXUtils::SafeRelease(pSrcGrpColl);
//	CXUtils::SafeRelease( pBrdcast);
//	CXUtils::SafeRelease( pFile);
	CXUtils::SafeRelease( pEncoder);
*/

//	SAFE_RELEASE(pSrcGrpColl);
//	SAFE_RELEASE( pAudSrc);
//	SAFE_RELEASE( pVidSrc);
//
//	SAFE_RELEASE( pSrcGrp);
//	SAFE_RELEASE(pBrdcast);
//	SAFE_RELEASE(pFile);
//	if (pEncoder != NULL)
//	{
//		theCallbacks.ShutDown(pEncoder);
//	}
//
//	if (releaseDevicesOnly == FALSE)
//	{
//		SAFE_RELEASE( pEncoder);
//	}
}


void CDBVMediaEncoder::GetPluginResources()
{
// Declare variables.

    HRESULT hr;
    IWMEncoder* pEncoder;	

    IWMEncSourcePluginInfoManager* pSrcPlugMgr;
    IWMEncPluginInfo* pPlugInfo;

    int i,j;
    long lPlugCount, lResCount;
    VARIANT_BOOL bResources, bExclusive, bHidden, bPropPage;
    WMENC_SOURCE_TYPE enumMediaType;
    WMENC_PLUGIN_TYPE enumPluginType;


// Initialize the COM library and retrieve a pointer
// to an IWMEncoder interface.

    hr = CoInitialize(NULL);
    CoCreateInstance(CLSID_WMEncoder,
                     NULL,
                     CLSCTX_INPROC_SERVER,
                     IID_IWMEncoder,
                     (void**) &pEncoder);

// Retrieve an IWMEncSourcePluginInfoManager pointer.

    hr = pEncoder->get_SourcePluginInfoManager(&pSrcPlugMgr);

// Loop through the source plug-ins. Call the Item method on each
// plug-in to retrieve an IWMEncPluginInfo object. Use the
// SchemeType property to retrieve a string containing the scheme
// type of the plug-in. If the scheme type is "DEVICE" and if the
// plug-in supports resources, call the Item method to retrieve a
// string identifying the resource. If the resource indicates that
// the plug-in captures streams from the default video device,
// retrieve registry information about the plug-in.

    hr = pSrcPlugMgr->get_Count(&lPlugCount);
    for (i=0; i<lPlugCount; i++)
    {
        hr = pSrcPlugMgr->Item(i, &pPlugInfo);
        CComBSTR bstrScheme;
        hr = pPlugInfo->get_SchemeType(&bstrScheme);
        if (_wcsicmp(bstrScheme, L"DEVICE")==0)
        {
            hr = pPlugInfo->get_Resources(&bResources); 
            if (bResources==VARIANT_TRUE)
            {
                hr = pPlugInfo->get_Count(&lResCount);
                for (j=0; j<lResCount; j++)
                {
                    CComBSTR bstrResource;
                    hr = pPlugInfo->Item(j, &bstrResource);
                    if (_wcsicmp(bstrResource,
                                 L"Default_Video_Device")==0)
                    {
                     // Retrieve the GUID.
                     CComBSTR bstrCLSID;
                     hr = pPlugInfo->get_CLSID(&bstrCLSID);

                     // Retrieve the copyright information.
                     CComBSTR bstrCopyright;
                     hr = pPlugInfo->get_CLSID(&bstrCopyright);

                     // Determine whether the plug-in can be used
                     // more than once in a source group.
                     hr = pPlugInfo->get_Exclusive(&bExclusive);

                     // Determine whether the plug-in is hidden
                     // from the UI.
                     hr = pPlugInfo->get_Exclusive(&bHidden);

                     // Retrieve the URL, if any, of the Web site
                     // containing information about the plug-in.
                     CComBSTR bstrURL;
                     hr = pPlugInfo->get_InfoURL(&bstrURL);

                     // Retrieve the media types supported by the
                     // plug-in. This is a bitwise OR of the
                     // WMENC_SOURCE_TYPE enumeration type.
                     hr = pPlugInfo->get_MediaType(&enumMediaType);

                     // Retrieve the name of the plug-in.
                     CComBSTR bstrName;
                     hr = pPlugInfo->get_Name(&bstrName);

                     // Determine whether the plug-in is a source or
                     // transform type.
                     hr = pPlugInfo->get_PluginType(&enumPluginType);

                     // Determine whether the plug-in supports
                     // property pages.
                     hr = pPlugInfo->get_PropertyPage(&bPropPage);
                    }
                }
            }
        }
    }

}

void CDBVMediaEncoder::GetEncoderStatsInPairs(CDBVMediaEncoder* theEncoder, int encoderIndex, CString &theStatPairs,
											  CEncoderCommand theEncoderCommandData)
//											  CString inSessionID, CString inSpeed, CString inStartDateString)
{
	CString tmpString;
	DWORD theDWORD_Time;
	theStatPairs.Empty();
	CString errorMsg;
	BOOL isIndexerRunningNow = false;
	BOOL isArchiverRunningNow = false;
	BOOL isEncoderRunningNow = false;
	BOOL isEncoderDeleted = false;

	CString locBroadcastPort;
	CString locSessionID;
	CString locSpeed;
	CString locStartTimeString;
	IWMEncoder* localpEncoder;

	BOOL encodingSessionIsGone = false;

	if (theEncoder == NULL)
	{
		encodingSessionIsGone = true;
	}
	else
	{
		localpEncoder = theEncoder->pEncoder;
		if (localpEncoder == NULL)
		{
			encodingSessionIsGone = true;
		}
	}

	if (encodingSessionIsGone)
	{
		locStartTimeString = theEncoderCommandData.gmtStartDate;
		isIndexerRunningNow = false;
		isArchiverRunningNow = false;
		isEncoderRunningNow = false;
		locSessionID = theEncoderCommandData.theID;
		locSpeed = theEncoderCommandData.speedKey;
		isEncoderDeleted = true;
	}
	else
	{
		isIndexerRunningNow = theEncoder->isIndexerRunning();
		isArchiverRunningNow = theEncoder->isArchiverRunning();
		isEncoderRunningNow = theEncoder->isTheEncoderRunning();
		if (theEncoder->theCallbacks.getEncoderMessagesReceivedFlag() == FALSE)
			isEncoderRunningNow = TRUE;

		isEncoderDeleted = false;
		theEncoder->getBroadCastPort(locBroadcastPort);
		theEncoder->getSessionID(locSessionID);
		theEncoder->getEventGMTDate(locStartTimeString);
		theEncoder->getSpeedKey(locSpeed);
	}


	tmpString.Format("%s_%d=%d\r\n","ENC_ENCODER_RESPONSE_RECORD", encoderIndex, 1);
	theStatPairs += tmpString;
	tmpString.Format("%s_%d=%d\r\n","ENC_IS_INDEXER_RUNNING", encoderIndex, isIndexerRunningNow);
	theStatPairs += tmpString;
		
	tmpString.Format("%s_%d=%d\r\n","ENC_IS_ARCHIVER_RUNNING", encoderIndex, isArchiverRunningNow);
	theStatPairs += tmpString;
		
	tmpString.Format("%s_%d=%d\r\n","ENC_IS_ENCODER_RUNNING", encoderIndex, isEncoderRunningNow);
	theStatPairs += tmpString;

//	tmpString.Format("%s_%d=%d\r\n","ENC_ENCODING_ENDED", encoderIndex, isEncoderDeleted);
//	theStatPairs += tmpString;

	tmpString.Format("%s_%d=%s\r\n","ENC_BROADCAST_PORT", encoderIndex, locBroadcastPort);
	theStatPairs += tmpString;
	tmpString.Format("%s_%d=%s\r\n","ENC_SESSION_ID", encoderIndex, locSessionID);
	theStatPairs += tmpString;
	tmpString.Format("%s_%d=%s\r\n","ENC_SPEED_KEY", encoderIndex, locSpeed);
	theStatPairs += tmpString;
	tmpString.Format("%s_%d=%s\r\n","ENC_START_DATE_STRING", encoderIndex, locStartTimeString);
	theStatPairs += tmpString;
	tmpString.Format("%s_%d=%d\r\n","ENC_SESSION_ID_DOES_NOT_EXIST", encoderIndex, encodingSessionIsGone);
	theStatPairs += tmpString;

	if (encodingSessionIsGone)
	{
		return;
	}
//***
//	Retrieve the total encoding time in seconds;
//***
	IWMEncStatistics* theStats = NULL;

    short iStreamCount;
	WMENC_LONGLONG theEncodingTime;
	HRESULT hr;
	hr = localpEncoder->get_Statistics(&theStats);
	if (CXUtils::WasHResultGood(hr, errorMsg))
	{
		hr = theStats->get_EncodingTime(&theEncodingTime);
		if (CXUtils::WasHResultGood(hr, errorMsg))
		{
			VarCyMulI4(theEncodingTime,10,&theEncodingTime);
			VarUI4FromCy(theEncodingTime, &theDWORD_Time);

			tmpString.Format("%s_%d=%d\r\n","ENC_ENCODING_TIME", encoderIndex, theDWORD_Time);
			theStatPairs += tmpString;
		}
		
		// Retrieve the number of multiple bit rate output streams.

	    hr = theStats->get_StreamOutputCount(WMENC_VIDEO,
                                            0,
                                            &iStreamCount);
		if (CXUtils::WasHResultGood(hr, errorMsg))
		{
		 	tmpString.Format("%s_%d=%d\r\n","ENC_OUTPUT_VIDEO_STREAM_COUNT", encoderIndex, iStreamCount);
			theStatPairs += tmpString;
		}
		hr = theStats->get_StreamOutputCount(WMENC_AUDIO,
                                            0,
                                            &iStreamCount);
		if (CXUtils::WasHResultGood(hr, errorMsg))
		{
		 	tmpString.Format("%s_%d=%d\r\n","ENC_OUTPUT_AUDIO_STREAM_COUNT", encoderIndex, iStreamCount);
			theStatPairs += tmpString;
		}
//***
// Retrieve an IDispatch pointer for the IWMEncOutputStats
// interface.
//***

	    IWMEncOutputStats* pOutputStats = NULL;
		IDispatch* pDispOutputStats = NULL;

	    long lAvgBitrate, lAvgSampleRate;
		long lCurrentBitRate, lCurrentSampleRate;
	    long lExpectedBitRate, lExpectedSampleRate;
		WMENC_LONGLONG qwByteCount, qwSampleCount;
	    WMENC_LONGLONG qwDroppedByteCount, qwDroppedSampleCount;

	    hr = theStats->get_WMFOutputStats(&pDispOutputStats);
		if (CXUtils::WasHResultGood(hr, errorMsg))
		{

			// Call QueryInterface for the IWMEncNetConnectionStats
			// interface pointer.

		    hr = pDispOutputStats->QueryInterface(IID_IWMEncOutputStats, 
                                          (void**)&pOutputStats);
			if (CXUtils::WasHResultGood(hr, errorMsg))
			{

				// Manually configure the encoder engine or load
				// a configuration from a file. For an example, see the 
				// IWMEncFile object.

				// You can create a timer to retrieve the statistics
				// after you start the encoder engine.

				hr = pOutputStats->get_AverageBitrate(&lAvgBitrate);
				tmpString.Format("%s_%d=%d\r\n","ENC_AVG_BIT_RATE", encoderIndex, lAvgBitrate);
				theStatPairs += tmpString;
				hr = pOutputStats->get_AverageSampleRate(&lAvgSampleRate);
				tmpString.Format("%s_%d=%d\r\n","ENC_AVG_SAMPLE_RATE", encoderIndex, lAvgSampleRate);
				theStatPairs += tmpString;
				hr = pOutputStats->get_ByteCount(&qwByteCount);
				tmpString.Format("%s_%d=%d\r\n","ENC_BYTE_COUNT", encoderIndex, qwByteCount);
				theStatPairs += tmpString;
				hr = pOutputStats->get_CurrentBitrate(&lCurrentBitRate);
				tmpString.Format("%s_%d=%d\r\n","ENC_CURRENT_BIT_RATE", encoderIndex, lCurrentBitRate);
				theStatPairs += tmpString;
				hr = pOutputStats->get_CurrentSampleRate(&lCurrentSampleRate);
				tmpString.Format("%s_%d=%d\r\n","ENC_CURRENT_SAMPLE_RATE", encoderIndex, lCurrentSampleRate);
				theStatPairs += tmpString;
				hr = pOutputStats->get_DroppedByteCount(&qwDroppedByteCount);
				tmpString.Format("%s_%d=%d\r\n","ENC_DROPPED_BYTE_COUNT", encoderIndex, qwDroppedByteCount);
				theStatPairs += tmpString;
				hr = pOutputStats->get_DroppedSampleCount(&qwDroppedSampleCount);
				tmpString.Format("%s_%d=%d\r\n","ENC_DROPPED_SAMPLE_COUNT", encoderIndex, qwDroppedSampleCount);
				theStatPairs += tmpString;
				hr = pOutputStats->get_ExpectedBitrate(&lExpectedBitRate);
				tmpString.Format("%s_%d=%d\r\n","ENC_EXPECTED_BIT_RATE", encoderIndex, lExpectedBitRate);
				theStatPairs += tmpString;
				hr = pOutputStats->get_ExpectedSampleRate(&lExpectedSampleRate);
				tmpString.Format("%s_%d=%d\r\n","ENC_EXPECTED_SAMPLE_RATE", encoderIndex, lExpectedSampleRate);
				theStatPairs += tmpString;
				hr = pOutputStats->get_SampleCount(&qwSampleCount);
				tmpString.Format("%s_%d=%d\r\n","ENC_SAMPLE_COUNT", encoderIndex, qwSampleCount);
				theStatPairs += tmpString;

				CXUtils::SafeRelease( pOutputStats);
			}

			CXUtils::SafeRelease( pDispOutputStats);
		}


		//***********************
		// Retrieve an IDispatch pointer for the IWMEncNetConnectionStats
		// interface.
		IWMEncNetConnectionStats* pNetConnStats = NULL;
		IDispatch* pDispNetConnStats = NULL;

	    long lCount;


	    hr = theStats->get_NetConnectionStats(&pDispNetConnStats);

		if (CXUtils::WasHResultGood(hr, errorMsg))
		{
			// You must callQueryInterface for the IWMEncNetConnectionStats 
			// interface pointer.

		    hr = pDispNetConnStats->QueryInterface(IID_IWMEncNetConnectionStats, 
		                                      (void**)&pNetConnStats);

			if (CXUtils::WasHResultGood(hr, errorMsg))
			{
				// Retrieve the number of connected clients.
	
			    hr = pNetConnStats->get_ClientCount(&lCount);
				tmpString.Format("%s_%d=%d\r\n","ENC_CLIENT_CONNECT_COUNT", encoderIndex, lCount);
				theStatPairs += tmpString;
				CXUtils::SafeRelease(pNetConnStats);
			}
			CXUtils::SafeRelease(pDispNetConnStats);
		}

		//***********************
		// Retrieve an IDispatch pointer for the IWMEncOutputStats
		// interface.

	    IWMEncFileArchiveStats* pFileArchiveStats = NULL;
		IDispatch* pDispFileArchiveStats = NULL;
	    WMENC_LONGLONG qwDuration, qwSize;

	    hr = theStats->get_FileArchiveStats(&pDispFileArchiveStats);
		if (CXUtils::WasHResultGood(hr, errorMsg))
		{

			// Call QueryInterface for the IWMEncNetConnectionStats
			// interface pointer.

		    hr = pDispFileArchiveStats->QueryInterface(
                      IID_IWMEncFileArchiveStats, 
                      (void**)&pFileArchiveStats);

			if (CXUtils::WasHResultGood(hr, errorMsg))
			{
				// Manually configure the encoder engine or load
				// a configuration from a file. For an example, see the 
				// IWMEncFile object.

				// Create a timer to retrieve the statistics
				// after you start the encoder engine.

			    pFileArchiveStats->get_FileDuration(&qwDuration);
				DWORD theDWORD_Duration;
				VarCyMulI4(qwDuration,10,&qwDuration);
				VarUI4FromCy(qwDuration, &theDWORD_Duration);
				theDWORD_Duration = theDWORD_Duration;

				tmpString.Format("%s_%d=%d\r\n","ENC_ARCHIVE_FILE_DURATION", encoderIndex, theDWORD_Duration);
				theStatPairs += tmpString;
				pFileArchiveStats->get_FileSize(&qwSize);
				tmpString.Format("%s_%d=%d\r\n","ENC_ARCHIVE_FILE_SIZE", encoderIndex, qwSize);
				theStatPairs += tmpString;

				CXUtils::SafeRelease(pDispFileArchiveStats);
			}
			CXUtils::SafeRelease(pFileArchiveStats);
		}

		//***********************
		// Retrieve an IDispatch pointer to IWMEncIndexerStats interface.
		IDispatch* pDispIndex = NULL;
		IWMEncIndexerStats* pIndexStats = NULL;
		long lFileCount, lPercent;
		CComBSTR bstrFileName;
		
		hr = theStats->get_IndexerStats(&pDispIndex);
		
		if (CXUtils::WasHResultGood(hr, errorMsg))
		{
			// Call QueryInterface() to retrieve a pointer to the 
			// IWMEncIndexerStats interface.
		
			hr = pDispIndex->QueryInterface(IID_IWMEncIndexerStats,
					(void**) &pIndexStats);
		
			if (CXUtils::WasHResultGood(hr, errorMsg))
			{
				// Retrieve information about the indexing process.
		
				hr = pIndexStats->get_FileCount(&lFileCount);
				tmpString.Format("%s_%d=%d\r\n","ENC_INDEXER_FILE_COUNT", encoderIndex, lFileCount);
				theStatPairs += tmpString;
		
				if (lFileCount > 0)
				{
					hr = pIndexStats->get_FileName(0, &bstrFileName);
					tmpString.Format("%s_%d=%s\r\n","ENC_INDEXER_FILE_NAME", encoderIndex, bstrFileName);
					theStatPairs += tmpString;
			
					hr = pIndexStats->get_PercentComplete(&lPercent);
					tmpString.Format("%s_%d=%d\r\n","ENC_INDEXER_FILE_PROGRESS_PERCENT", encoderIndex, lPercent);
					theStatPairs += tmpString;
				}
				CXUtils::SafeRelease(pIndexStats);
			}

			CXUtils::SafeRelease(pDispIndex);
		}	

		CXUtils::SafeRelease(theStats);
	}



}

void CDBVMediaEncoder::setBroadCastPort(CString thePort)
{
	if (atoi(thePort) > 0)
	{
		if (thePort.GetLength() > 0)
		{
			broadcastPort = thePort;
		}
	}
}
void CDBVMediaEncoder::getBroadCastPort(CString& thePort)
{
	thePort = broadcastPort;
}
