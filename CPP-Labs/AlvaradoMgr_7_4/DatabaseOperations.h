#pragma once

class CEncoderDlg;
class CMyDatabase;

class CDatabaseOperations
{
public:
	CDatabaseOperations(void);
	~CDatabaseOperations(void);

	enum DatabaseStatus
	{
		DB_SUCCESS = 0,
		DB_FAIL	= -1,
		DB_CONNECT_FAIL = -2,
		DB_QUERY_EXCEPTION = -3,
		DB_UPDATE_EXCEPTION = -4,
		DB_PARAMETER_ERROR = -5
	};	

	struct OrderInfo
	{
		CString partNumber;
		CString option;
		int		quantity;
		float	Price;
		int		discount;
	};
private:
	CEncoderDlg* theParent;

protected:
	static CMyDatabase* OpenDatabaseConnection(CString DSN_Name, CString DBPath, BOOL ReadOnly = true, BOOL Exclusive=false );
	static CMyDatabase* CloseAndDeleteCDatabase(CMyDatabase* theDB);
//	int GetNewSequenceNumber(CString sequenceFileKey, CString& theSeqNumCString);
	int DoStockProcessing(CMyDatabase*& theDB, CMapStringToString& theRemoteData, CString& currentSchool);
	int DoStockNotOnRemoteProcessing(CMyDatabase*& theDB, CMapStringToString& theRemoteData, CString& currentSchool);
//	int DoNewOrderProcessing(CMapStringToString& theRemoteData);
	int BuildOrderImport(CMapStringToString& theRemoteData, CStringArray& theOutData);
	CString GetStringLookup(CMapStringToString& map, CString key);
	void SetParent(CEncoderDlg* parent);
	int BuildStatusUpdateResponse(CMyDatabase*& theDB, CMapStringToString& theRemoteData, CString& currentSchool);
	int GetOrderIDFromAltOrder(CMyDatabase* theDB, CString altOrder, CString& orderID);
	int GetBoxTrackingInfo(CMyDatabase* theDB, int Order, int boxID, CString inPart, CString& trackNum);

};
