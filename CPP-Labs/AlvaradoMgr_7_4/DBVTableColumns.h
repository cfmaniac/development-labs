#pragma once

class CDBVTableColumns
{
public:
	CDBVTableColumns(void);
	~CDBVTableColumns(void);

	static CStringArray DBVInvoiceColumns;
	static CStringArray DBVStockColumns;
	
	static void InitDBVColumnArrays(void);
	CMapStringToString DBVInvoiceCMSCustMap;
};
