<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>labs.localhost.com</title>
  <style>
 .card {
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    border-radius: 5px; /* 5px rounded corners */
}

/* Add rounded corners to the top left and the top right corner of the image */
img {
    border-radius: 5px 5px 0 0;
}

/* On mouse-over, add a deeper shadow */
.card:hover {
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}

/* Add some padding inside the card container */
.container {
    padding: 2px 16px;
}
  </style>
</head>
<body>
<h2>labs.localhost.com</h2>
<cfscript>
    theLabs = directoryList(expandPath("./"), false, "query");
</cfscript>
<cfoutput>
<cfloop query="theLabs">
<div class="card">
  <!--img src="img_avatar2.png" alt="Avatar" style="width:100%"-->
  <div class="container">
    <h4><b>#name#</b></h4>
    <cfscript>
    theLab = directoryList(expandPath("#name#"), false, "query");
    </cfscript>
    <cfloop query="theLab">
        <a href="#theLab.name#">Visit #name# Lab</a><br>
    </cfloop>

  </div>
</div>
</cfloop>
</cfoutput>




</body>
</html>
