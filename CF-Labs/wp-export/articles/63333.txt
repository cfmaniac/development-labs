<article id="post-63333" class="post-63333 post type-post status-publish format-standard has-post-thumbnail hentry category-elite-camp-updates"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p><img src="https://hoopgroup.com/wp-content/uploads/2015/07/IMG_3842-2.jpg" alt="IMG_3842 2" width="600" height="300" class="aligncenter size-full wp-image-63345"><br> <em>Reading, Pa&nbsp;</em>Session two of Academic Elite Camp started with over 500 high academic players come through the doors of the Bollman Center for camp. With an already great turn out for camp this week, were excited for what the next three days has to offer. Here are a few guys that fared well in try out games, and could be prospects to keep an eye on today.</p> 
  <p><strong>George Snyder III | 2017 | National Collegiate Prep</strong></p> 
  <p>George showed a good offensive game Tuesday. He plays at a controlled pace and does not get sped up. He was able to find the open spots on the floor and hit off the dribble from the mid range area. Though only 60, he attacks the glass hard and came up with a few tip ins that were purely a result of hustle.</p> 
  <p><strong>William Kondorat | 2017 | Bay Ridge Prep</strong></p> 
  <p>Will was good at getting to the rim. Though he doesnt have the quickest first step, he was good at making some nice moves to get his man off his feet and finding a way to get to the hoop. He also has active hands on defense, and came away with a few steals.</p> 
  <p><strong>Andrew Varoli | 2017 | Iona Prep</strong></p> 
  <p>Andrew was showing he is a capable shooter on day one. If you dont put a hand up, hes going to let it fly from deep. He knocked down a couple of threes Tuesday and showed hes not afraid to pull it off the dribble, or a few feet behind the line.</p> 
  <p><strong>Erik Braaten | 2016 | Donovan Catholic</strong></p> 
  <p>Weve seen a lot of Erik this summer, both at camp and tournaments. He saw himself have a pretty good day one with his play inside. He sealed well, demanding the ball, was good on the glass, and finished over defenders with strong post moves.</p> 
  <p><strong>Zach Reichelderfer | 2018 | Troy HS (OH)</strong></p> 
  <p>Zach was most impressive on day one with his quick first steps. He doesnt play around with the ball when he gets it instead, he makes a quick move and blows by defenders. He makes it look easy, as he gets straight line drives to the rim for lay up after lay up.</p> 
  <p><strong>Emmanuel Oyakhilome | 2016 | Lindhurst HS (NJ)</strong></p> 
  <p>Emmauel is 66 with a strong body. Tuesday he showed flashes where he bullied his way into the lane and refused to be stopped. He also showed s passing ability that might have surprised some. Once he got into the lane, he drew help and instead of putting up the shot, he dished it off nicely to teammates for open lay ins.</p> 
  <p><strong>Drew Gagnon | 2016 | Pingry School</strong></p> 
  <p>Drew is a wing with sneaky athleticism. Hes great at running the floor and can finish in transition. He had one of the plays of the day with a big alley oop in the last game of the night. Expect to see him on the highlights at the end of camp.</p> 
  <p><strong>Khary Mauras | 2016 | Reading HS</strong></p> 
  <p>Khary is a passing point guard. For starters hes comfortable with the ball in his hands and he has great ball handling for a guard. He makes tight and quick moves with the ball, and doesnt play with it. Hes also always looking to facilitate; he racked up the assists on day one.</p> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>