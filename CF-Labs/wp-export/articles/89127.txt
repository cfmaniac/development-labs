<article id="post-89127" class="post-89127 post type-post status-publish format-standard has-post-thumbnail hentry category-jr-elite category-player-feature category-skills-camp-updates"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p style="text-align: right;"><strong><a href="https://twitter.com/TheHoopGroup"><img class="alignnone wp-image-82243" src="https://hoopgroup.com/wp-content/uploads/2017/01/twitter_icon-300x300.png" alt="twitter_icon" width="25" height="25"></a>&nbsp; &nbsp; &nbsp;<a href="https://www.instagram.com/hoopgroup/"><img class="alignnone wp-image-84839" src="https://hoopgroup.com/wp-content/uploads/2017/04/instagram-100x75.png" alt="" width="30" height="30"></a>&nbsp; &nbsp;<a href="https://hoop-group.smugmug.com/"><img class="alignnone wp-image-89113" src="https://hoopgroup.com/wp-content/uploads/2017/09/sm-100x75.png" alt="" width="30" height="30"></a><a href="http://www.middlesexmagic.com"><br> </a></strong></p> 
  <p>&nbsp;</p> 
  <p><a href="https://hoopgroup.com/wp-content/uploads/2017/09/fall_jr_elite_media_header-1.jpg"><img class="alignleft size-full wp-image-89139" src="https://hoopgroup.com/wp-content/uploads/2017/09/fall_jr_elite_media_header-1.jpg" alt="" width="1500" height="608"></a></p> 
  <table class=" aligncenter" style="height: 26px; background-color: #d4d4d4;" width="598"> 
   <tbody> 
    <tr> 
     <td style="width: 142.465px; text-align: center;"><a href="#phillyjr">Philadelphia</a></td> 
     <td style="width: 143.576px; text-align: center;"><a href="#metrojr">Metro</a></td> 
     <td style="width: 143.576px; text-align: center;"><a href="#nycjr">NYC</a></td> 
     <td style="width: 143.576px; text-align: center;"><a href="#dmvjr">DMV</a></td> 
    </tr> 
   </tbody> 
  </table> 
  <h2></h2> 
  <h2></h2> 
  <h2></h2> 
  <p>&nbsp;</p> 
  <p>&nbsp;</p> 
  <div style="position: relative; height: 0; padding-bottom: 56.25%;">
   <iframe style="position: absolute; width: 100%; height: 100%; left: 0;" src="https://www.youtube.com/embed/Ya_9MDmjzBQ?ecver=2" width="640" height="360" frameborder="0" allowfullscreen></iframe>
  </div> 
  <p><iframe width="525" height="295" src="https://www.youtube.com/embed/drnCYUMPCp8?feature=oembed" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></p> 
  <p>&nbsp;</p> 
  <h2 id="phillyjr" style="text-align: center;">Jr. Elite Clinic Recap | Philadelphia</h2> 
  <hr> 
  <p><a href="https://hoopgroup.com/wp-content/uploads/2017/09/Untitled-1.jpg"><img class="alignleft wp-image-89133" src="https://hoopgroup.com/wp-content/uploads/2017/09/Untitled-1.jpg" alt="" width="204" height="26"></a> <a href="https://www.facebook.com/pg/thehoopgroup/photos/?tab=album&amp;album_id=1559583550772837"><img class="alignleft wp-image-86647" src="https://hoopgroup.com/wp-content/uploads/2011/01/facebook-logo_318-49940.jpg" alt="" width="51" height="52"></a> <a href="https://hoop-group.smugmug.com/PhillyJrEliteClinic2017"><img class="wp-image-89113 alignleft" src="https://hoopgroup.com/wp-content/uploads/2017/09/sm-100x75.png" alt="" width="72" height="52"></a></p> 
  <h2 style="text-align: center;"></h2> 
  <h2></h2> 
  <p>&nbsp;</p> 
  <p>&nbsp;</p> 
  <p><span style="font-weight: 400;">Thank you to everyone who attended our Philly Jr. Elite this past weekend. We had a great turnout among talented players, teams, and a thank you to our coaching staff that turned out for the event. Some of the top schools in attendance of our coaching staff were from St. Marys High School, Bryn Athyn College, and Eastern High school. Among the all the players in attendance, below are a few honorable mentions:</span></p> 
  <p><strong><a href="https://hoopgroup.com/wp-content/uploads/2017/09/dominquez-1.jpg"><img class="alignleft wp-image-89129" src="https://hoopgroup.com/wp-content/uploads/2017/09/dominquez-1.jpg" alt="" width="182" height="258"></a></strong></p> 
  <p>&nbsp;</p> 
  <p>&nbsp;</p> 
  <p><strong>G.Q. Dominguez #50, 9th grader</strong></p> 
  <p><span style="font-weight: 400;">G.Q. </span><span style="font-weight: 400;">&nbsp;used his athleticism and strength to dominate the event. &nbsp;He has a high motor and was unstoppable in the open court, especially his average 14 points between both games. As a high school player, his quickness and ability to move with the ball will make him a stand out player on any high school court. </span></p> 
  <p>&nbsp;</p> 
  <p>&nbsp;</p> 
  <p>&nbsp;</p> 
  <p><strong><a href="https://hoopgroup.com/wp-content/uploads/2017/09/zack_white-1-e1506642026547.jpg"><img class="alignleft wp-image-89131 " src="https://hoopgroup.com/wp-content/uploads/2017/09/zack_white-1-e1506642026547.jpg" alt="" width="182" height="267"></a></strong></p> 
  <p>&nbsp;</p> 
  <p>&nbsp;</p> 
  <p><strong>Zack White #49, 8th grader</strong></p> 
  <p><span style="font-weight: 400;">Zack showed up to win this weekend at Philly. Standing as a 56 guard, Zacks talent stands him above many of his peers, putting his game to a whole new level. What Zack did well this weekend was using his athleticism to break down defenders and finish at the basket or create space for himself to get an open shot. Zack finished both games with an average of 16 points.</span></p> 
  <p>&nbsp;</p> 
  <p>&nbsp;</p> 
  <p>&nbsp;</p> 
  <p><strong><a href="https://hoopgroup.com/wp-content/uploads/2017/09/four_petre-1.jpg"><img class="alignleft wp-image-89130" src="https://hoopgroup.com/wp-content/uploads/2017/09/four_petre-1.jpg" alt="" width="183" height="261"></a></strong></p> 
  <p>&nbsp;</p> 
  <p>&nbsp;</p> 
  <p><strong>Four Petre #15, 6th grader</strong></p> 
  <p><span style="font-weight: 400;">Four impressed the coaches this weekend with his ability to handle the ball. Not only is Four able to break down his defenders, but he does a tremendous job as a point guard getting his entire team involved in the game. What the coaches loved most about Four ability to communicate with his teammates on the court. Looking forward to seeing Fours grow in the upcoming season!</span></p> 
  <p>&nbsp;</p> 
  <p>&nbsp;</p> 
  <p>&nbsp;</p> 
  <p><strong><a href="https://hoopgroup.com/wp-content/uploads/2017/09/ari_henderson-1.jpg"><img class="alignleft wp-image-89128" src="https://hoopgroup.com/wp-content/uploads/2017/09/ari_henderson-1.jpg" alt="" width="186" height="262"></a></strong></p> 
  <p>&nbsp;</p> 
  <p>&nbsp;</p> 
  <p><strong>Ari Henderson #17, 5th grader<br> </strong></p> 
  <p><span style="font-weight: 400;">Ari &nbsp;is an ultra-talented guard who plays for Reading Intermediate School. &nbsp;As one of the youngest players at the event this weekend, he moves fluidly and can put the ball on the deck and finish from any spot on the floor. Here at the Hoop Group we are very excited to watch Ari grow and develop throughout the years with our program.</span></p> 
  <p>&nbsp;</p> 
  <p>&nbsp;</p> 
  <p>&nbsp;</p> 
  <p>&nbsp;</p> 
  <p>&nbsp;</p> 
  <h2 id="metrojr" style="text-align: center;">Jr. Elite Clinic Recap | Metro</h2> 
  <hr> 
  <p><a href="https://hoopgroup.com/wp-content/uploads/2017/09/Untitled-1.jpg"><img class="alignleft wp-image-89133" src="https://hoopgroup.com/wp-content/uploads/2017/09/Untitled-1.jpg" alt="" width="204" height="26"></a> <a href="https://www.facebook.com/pg/thehoopgroup/photos/?tab=album&amp;album_id=1568250869906105"><img class="alignleft wp-image-86647" src="https://hoopgroup.com/wp-content/uploads/2011/01/facebook-logo_318-49940.jpg" alt="" width="51" height="52"></a> <a href="https://hoop-group.smugmug.com/2017-Metro-Jr-Elite-Clinic"><img class="wp-image-89113 alignleft" src="https://hoopgroup.com/wp-content/uploads/2017/09/sm-100x75.png" alt="" width="72" height="52"></a></p> 
  <h2></h2> 
  <h2></h2> 
  <p style="text-align: center;"><a href="https://hoopgroup.com/wp-content/uploads/2017/09/2017_jr_elite_fall_top100-73.jpg"><img class="aligncenter wp-image-89314 " src="https://hoopgroup.com/wp-content/uploads/2017/09/2017_jr_elite_fall_top100-73-e1507233098162.jpg" alt="" width="931" height="457"></a></p> 
  <p style="text-align: center;"><strong>Spotlight Coach</strong></p> 
  <p>Jr Elite had over 15 coaches out to help put players through high intensity workouts, coach them in two game sets and provide them with a player evaluation at the end of camp. All highly recommended and experienced coaches, their background range from Division 1 and overseas players, to ranked high school coaches, to college coaches. Check out some of our Spotlight coaches from the weekend!</p> 
  <table style="height: 39px;" width="669"> 
   <tbody> 
    <tr> 
     <td style="width: 328.516px;"><strong>&nbsp;Sean Solomons</strong></td> 
     <td style="width: 328.516px;">St Joes Brooklyn</td> 
    </tr> 
    <tr> 
     <td style="width: 328.516px;"><strong>Jeff Kovner</strong></td> 
     <td style="width: 328.516px;">St Marys Elizabeth</td> 
    </tr> 
    <tr> 
     <td style="width: 328.516px;"><strong>Jay Griffin</strong></td> 
     <td style="width: 328.516px;">St Marys Elizabeth</td> 
    </tr> 
    <tr> 
     <td style="width: 328.516px;"><strong>Austin Whitehurst</strong></td> 
     <td style="width: 328.516px;">Mater Dei HS</td> 
    </tr> 
    <tr> 
     <td style="width: 328.516px;"><strong>Michael Caswell</strong></td> 
     <td style="width: 328.516px;">Messiah</td> 
    </tr> 
    <tr> 
     <td style="width: 328.516px;"><strong>Alex Mirabel</strong></td> 
     <td style="width: 328.516px;">St Peters Prep</td> 
    </tr> 
    <tr> 
     <td style="width: 328.516px;"><strong>Rich Woods</strong></td> 
     <td style="width: 328.516px;">East Orange</td> 
    </tr> 
   </tbody> 
  </table> 
  <p>&nbsp;</p> 
  <p style="text-align: center;"><strong>Player Highlights</strong></p> 
  <table style="height: 1335px; width: 542px;"> 
   <tbody> 
    <tr> 
     <td style="width: 199.766px; text-align: center;"><a href="https://hoopgroup.com/wp-content/uploads/2017/09/jack-savare-1-of-1.jpg"><img class="wp-image-89308 aligncenter" src="https://hoopgroup.com/wp-content/uploads/2017/09/jack-savare-1-of-1.jpg" alt="" width="165" height="265"></a></td> 
     <td style="width: 329.766px;"><strong>Jack Savare</strong><p></p> <p>Jack Savare has a nice combo of size and speed. The 9th grade freshman at Red Bank Catholic is a skilled combo guard/wing and should be an impact player for RBC down the road.</p></td> 
    </tr> 
    <tr> 
     <td style="width: 199.766px; text-align: center;"><a href="https://hoopgroup.com/wp-content/uploads/2017/09/jeremiah-dorvilus-1-of-1.jpg"><img class="wp-image-89309 aligncenter" src="https://hoopgroup.com/wp-content/uploads/2017/09/jeremiah-dorvilus-1-of-1.jpg" alt="" width="170" height="257"></a></td> 
     <td style="width: 329.766px;"><strong>Jeremiah Dorvilus</strong><p></p> <p>The St Marys 9th grader stood out for his knock down shooting. The sweet shooting wing knocked down 6 threes in the first game set today and will be a sniper for years to come.</p></td> 
    </tr> 
    <tr> 
     <td style="width: 199.766px; text-align: center;"><a href="https://hoopgroup.com/wp-content/uploads/2017/09/aj-walker-1-of-1.jpg"><img class="wp-image-89306 aligncenter" src="https://hoopgroup.com/wp-content/uploads/2017/09/aj-walker-1-of-1.jpg" alt="" width="170" height="255"></a></td> 
     <td style="width: 329.766px;"><strong>AJ Walker</strong><p></p> <p>A 9th grader from Cherokee High School and South Jersey Elite AAU dominated with his overall play. With the combination of his skill set and court vision he is dangerous and able to score from anywhere on the court.</p></td> 
    </tr> 
    <tr> 
     <td style="width: 199.766px; text-align: center;"><a href="https://hoopgroup.com/wp-content/uploads/2017/09/nyle-coleman-1-of-1.jpg"><img class="wp-image-89310 aligncenter" src="https://hoopgroup.com/wp-content/uploads/2017/09/nyle-coleman-1-of-1.jpg" alt="" width="170" height="255"></a></td> 
     <td style="width: 329.766px;"><strong>Nyle Coleman</strong><p></p> <p>The 2023 proved to be a do it all guard today and impressed with his overall technique and skill.</p></td> 
    </tr> 
    <tr> 
     <td style="width: 199.766px; text-align: center;"><a href="https://hoopgroup.com/wp-content/uploads/2017/09/bryson-brown-1-of-1.jpg"><img class="wp-image-89307 aligncenter" src="https://hoopgroup.com/wp-content/uploads/2017/09/bryson-brown-1-of-1.jpg" alt="" width="170" height="255"></a></td> 
     <td style="width: 329.766px;"><strong>Bryson Brown</strong><p></p> <p>The Manhattan NYC native demanded evaluators attention with his ball handling and overall play. A young 2024 guard plays strong and is not afraid to get a little physical. Slasher with a step back jumper.</p></td> 
    </tr> 
   </tbody> 
  </table> 
  <h2></h2> 
  <h2 id="nycjr" style="text-align: center;">Jr. Elite Clinic Recap | NYC</h2> 
  <hr> 
  <p><a href="https://hoopgroup.com/wp-content/uploads/2017/09/Untitled-1.jpg"><img class="alignleft wp-image-89133" src="https://hoopgroup.com/wp-content/uploads/2017/09/Untitled-1.jpg" alt="" width="204" height="26"></a> <a href="https://www.facebook.com/pg/thehoopgroup/photos/?tab=album&amp;album_id=1577063985691460"><img class="alignleft wp-image-86647" src="https://hoopgroup.com/wp-content/uploads/2011/01/facebook-logo_318-49940.jpg" alt="" width="51" height="52"></a> <a href="https://hoop-group.smugmug.com/2017-NYC-Fall-Top-100"><img class="wp-image-89113 alignleft" src="https://hoopgroup.com/wp-content/uploads/2017/09/sm-100x75.png" alt="" width="72" height="52"></a></p> 
  <h2></h2> 
  <p>&nbsp;</p> 
  <p><span style="font-weight: 400;">Thank you to everyone who attended our NYC Jr. Elite. This event had a very talented young group that blew our coaching staff away and certainly held their own.</span></p> 
  <a href="http://hoopgroup.com/jr-elite/2017-jr-elite-clinic-media-page/attachment/nyc-coaches-1/" rel="attachment wp-att-89909"><img data-caption="Our many coaches represented a few programs such as: Queens College, Messiah College, St. Francis Prep (Queens, NY), St. Peters Prep (Jersey City), Marist HS (New Jersey), St. Marys Elizabeth (New Jersey), Saddle River HS (New Jersey), Stroudsburg Hs (Pennsylvania) and East Orange HS (New Jersey)." class="aligncenter wp-image-89909" src="https://hoopgroup.com/wp-content/uploads/2017/09/nyc-coaches-1.jpg" alt="" width="808" height="449"></a>
  <p class="blog2html_p ">Our many coaches represented a few programs such as: Queens College, Messiah College, St. Francis Prep (Queens, NY), St. Peters Prep (Jersey City), Marist HS (New Jersey), St. Marys Elizabeth (New Jersey), Saddle River HS (New Jersey), Stroudsburg Hs (Pennsylvania) and East Orange HS (New Jersey).</p> 
  <p style="text-align: center;"><strong>Player Highlights</strong></p> 
  <table style="height: 2566px;" width="543"> 
   <tbody> 
    <tr> 
     <td style="width: 536.016px;"><b>Jermel Thomas&nbsp;</b>| 4th grade<p></p> <p><span style="font-weight: 400;">A true NYC point guard! Jermel is one of the best 4th graders in the country and certainly in NYC. Jermel has an unbelievable ability to handle the basketball putting together series of crossover moves that made it almost impossible for defenders to stay in front not to mention he was the youngest player at the event. Besides his incredible ability to handle the rock Jermel is a natural playmaker always looking to get his teammates involved. Do not give Jermel to much space he also proved he could knock down some shots from beyond the arc. Keep a close eye out for Jermel he will be continuing to make noise around the city.</span></p></td> 
    </tr> 
    <tr> 
     <td style="width: 536.016px;"><b>Kiyan Anthony&nbsp;</b>| 5th grade<p></p> <p><span style="font-weight: 400;">Kiyan is a very smart player with great court awareness. Kiyan is a very good shooter and showed it with his ability to knock down multiple 3 pointers. Kiyans ability to shoot is no surprise considering he is the son of NBA super star Carmelo Anthony. Kiyan also showed his ability to handle and pass the rock. Kiyan has a lot of potential and will be making some noise in the near future. </span></p></td> 
    </tr> 
    <tr> 
     <td style="width: 536.016px;">&nbsp;<b style="font-family: inherit; font-size: inherit;">Maurice Henderson&nbsp;</b><span style="font-family: inherit; font-size: inherit;">| 7th grade</span>Maurice is a point guard with a very high basketball IQ. Maurice is great in transition and seems to always make the right play. He can attack the rim but also can step out and knock it down from the outside. Most importantly Maurice is a lockdown defender that gives opponents a very hard time.</td> 
    </tr> 
    <tr> 
     <td style="width: 536.016px;"><b><a href="http://hoopgroup.com/jr-elite/2017-jr-elite-clinic-media-page/attachment/player-highlight-1/" rel="attachment wp-att-89912"><img class="alignleft wp-image-89912 size-medium" src="https://hoopgroup.com/wp-content/uploads/2017/09/player-highlight-1-200x300.jpg" alt="" width="200" height="300"></a></b><p></p> <p>&nbsp;</p> <p><b>Bryson Brown&nbsp;</b>| 6th grade</p> <p><span style="font-weight: 400;">Bryson is all business on the hardwood. This kid can do it all from handling the rock to shooting from 30 feet out with ease. Bryson is a quiet kid but his game speaks very loud and he shows great leadership. Bryson does not take plays off and gives 110% effort every time he steps on the court. Watch out for Bryson I believe we will hear a lot about this kid in the near future.</span></p></td> 
    </tr> 
    <tr> 
     <td style="width: 536.016px;"><b>Karriem Thomas&nbsp;</b>| 7th grade<p></p> <p><span style="font-weight: 400;">Older brother of Jermel Thomas Karriem also a NYC point guard. Karriem played up with the 8th and 9th graders and did more then just hold his own. Karriem was able to handle the defensive pressure of the older guys with his tight handle and court awareness. Karriem has great court vision and was able to make plays for not only himself but all of his teammates. Karriem was able to use his quickness to blow by defenders and get to the rim and finish. Bright future ahead of both the Thomas brothers.</span></p></td> 
    </tr> 
    <tr> 
     <td style="width: 536.016px;"><b>Jeremiah Peoples&nbsp;</b>| 7th grade<p></p> <p><span style="font-weight: 400;">Jeremiah is a silky smooth guard who has a knack for finishing around the rim. He can knock down shots from almost all parts of the court and is a high effective distributor that rarely turns the ball over.</span></p></td> 
    </tr> 
    <tr> 
     <td style="width: 536.016px;">&nbsp;<p></p> <p><b><a href="http://hoopgroup.com/jr-elite/2017-jr-elite-clinic-media-page/attachment/player-highlight-2/" rel="attachment wp-att-89914"><img class="alignleft size-medium wp-image-89914" src="https://hoopgroup.com/wp-content/uploads/2017/09/player-highlight-2-200x300.jpg" alt="" width="200" height="300"></a></b></p> <p>&nbsp;</p> <p>&nbsp;</p> <p><b>Preston Edmead&nbsp;</b>| 5th grade</p> <p>Preston had a great showing in NYC impressing our evaluators with his ability to score. Preston can get in the lane and finish around the basket will both hands. For a young player he had great court vision and a very high basketball IQ.</p></td> 
    </tr> 
    <tr> 
     <td style="width: 536.016px;"><b>Tyler Hawkins&nbsp;</b>| 8th grade<p></p> <p><span style="font-weight: 400;">Tyler is a very quick and strong guard with a tight handle and ability to get to the rim whenever he likes. Defenders had a hard time staying in front of the shifty guard as he sliced through the defense. Tyler can break a defender down with the dribble to get his own shot or to create for others. If Tylers outside shot improves he will be almost unguardable.</span></p></td> 
    </tr> 
    <tr> 
     <td style="width: 536.016px;"><b>Noel Echivarra&nbsp;</b>| 8th grade<p></p> <p><span style="font-weight: 400;">Noel is undoubtedly one of the top 8th graders in NYC. At 62 Noel can do it all from handling the rock to guarding the biggest strongest guy on the court. What is most impressive is Noels play making ability. Noel is just as good off the ball as he is on the ball, he can knock down an open jumper as well as come off a ball screen and make the right read. If he keeps working hard this kid could be a real good high school player.</span></p></td> 
    </tr> 
    <tr> 
     <td style="width: 536.016px;"><b>Jaheim Grant&nbsp;</b>| 8th grade<p></p> <p><span style="font-weight: 400;">Jaheim is a strong bruising guard that loves to play physical. He is a runaway train in transition that defenders do dont want to get in the way of. What was most impressive about Jaheim was that he is a lockdown defender and can guard multiple positions.</span></p></td> 
    </tr> 
    <tr> 
     <td style="width: 536.016px;"><b>Ronald Greene&nbsp;</b>| 8th grade<p></p> <p><span style="font-weight: 400;">Ronald is a beast! Ronald is a big wing that can do it all and has potential elite guard skills. He has a very tight handle with the court vision of a point guard. Ronald can attack the rim as well as step back and knock down the jumper. He showed great athleticism with high level blocks and plays at the rim. Ronald might have shown the most potential and upside at our event.</span></p></td> 
    </tr> 
    <tr> 
     <td style="width: 536.016px;"><b>Isaac Santiago&nbsp;</b>| 8th grade<p></p> <p><span style="font-weight: 400;">Isaac Is a combo guard that can shoot the ball real well. In NYC he showed his ability to shoot from behind the arc. Isaac can also handle the ball and does a very good job getting his teammates involved. Every time he stepped on the court he played as hard as he could and did not take any plays off.</span></p></td> 
    </tr> 
    <tr> 
     <td style="width: 536.016px;"><a href="http://hoopgroup.com/jr-elite/2017-jr-elite-clinic-media-page/attachment/player-highlight-3/" rel="attachment wp-att-89913"><img class="alignleft size-medium wp-image-89913" src="https://hoopgroup.com/wp-content/uploads/2017/09/player-highlight-3-200x300.jpg" alt="" width="200" height="300"></a>&nbsp;<b style="font-family: inherit; font-size: inherit;">&nbsp;</b><p></p> <p><b style="font-family: inherit; font-size: inherit;">Abdul Doumbouya&nbsp;</b>| 8th grade</p> <p><span style="font-weight: 400;">Abdul is a combo that can do a little bit of everything. His lightning quick first step made it hard for guys to stay in front causing many defensive breakdowns. Abdul can get to the rim and finish with both hands equally as well. Abdul is not only an good offensive player but he is a lockdown defender and he gave problems to anyone he was guarding. </span></p></td> 
    </tr> 
   </tbody> 
  </table> 
  <p>&nbsp;</p> 
  <h2></h2> 
  <h2 id="dmvjr" style="text-align: center;">Jr. Elite Clinic Recap | DMV</h2> 
  <hr> 
  <p>Thank you to everyone who attended our DMV Jr. Elite. This event had a very talented younger group that blew our coaching staff away and certainly held their own. Our many coaches represented a few programs such as: Messiah College, St. Johns College, and Bethlehem High School.</p> 
  <p><a href="http://hoopgroup.com/jr-elite/2017-jr-elite-clinic-media-page/attachment/img_0095-3/" rel="attachment wp-att-89907"><img class=" wp-image-89907 aligncenter" src="https://hoopgroup.com/wp-content/uploads/2017/09/IMG_0095.jpeg" alt="" width="746" height="560"></a></p> 
  <p style="text-align: center;"><strong>Spotlight Coach</strong></p> 
  <p>Jr Elite had over 10 coaches out to help put players through high intensity workouts, coach them in two game sets and provide them with a player evaluation at the end of camp. All highly recommended and experienced coaches, their background range from Division 1 and overseas players, to ranked high school coaches, to college coaches. Check out some of our Spotlight coaches from the weekend!</p> 
  <table width="669"> 
   <tbody> 
    <tr> 
     <td><strong>Will Dockery</strong></td> 
     <td>Cecil College</td> 
    </tr> 
    <tr> 
     <td><b>Michael Caswell</b></td> 
     <td>Messiah</td> 
    </tr> 
    <tr> 
     <td><strong>Jamal Hulum</strong></td> 
     <td>St Johns College</td> 
    </tr> 
   </tbody> 
  </table> 
  <p>&nbsp;</p> 
  <p style="text-align: center;"><strong>Player Standouts</strong></p> 
  <table style="height: 150px;" width="546"> 
   <tbody> 
    <tr> 
     <td style="width: 268.516px;"><b>Darren Wright</b><p></p> <p><span style="font-weight: 400;">A 9th grader that certainly proved he is ready for the varsity high school level. Whether it was crossing someone up, finishing at the rim or getting a stop on the defensive end, Darren is absolutely a play to watch in high school this year.</span></p></td> 
    </tr> 
    <tr> 
     <td style="width: 268.516px;"><b>Chase Williams</b><p></p> <p><span style="font-weight: 400;">A dangerous player that plays at a level higher than his age. Capable of producing in any situation the game throws at him, Chase is a future all star on the rise.</span></p></td> 
    </tr> 
    <tr> 
     <td style="width: 268.516px;"><b>Isaiah Abraham</b><p></p> <p><span style="font-weight: 400;">The Virginia native is explosive on the floor and one of the best athletes in the class of 2022. Isaiahs ability to make big plays and knock down big time shots are what put him above those in his class. </span></p></td> 
    </tr> 
    <tr> 
     <td style="width: 268.516px;"><b>Thurstan Davis</b><p></p> <p><span style="font-weight: 400;">Thurstan used his athleticism and strength to dominate the event. Thurstan is unstoppable on the open court, between his handle and quickness, no one can stop him on the way to the rim. It will be exciting to continue to watch Thurstan grow in the game.</span></p></td> 
    </tr> 
    <tr> 
     <td style="width: 268.516px;"><b>&nbsp;Damontae Necols</b><p></p> <p><span style="font-weight: 400;">A commander on the floor at such a young age, this fifth grader made you second guess his age. With his fluidity and vocalness on the floor, scoring buckets is just an added plus to his game. Damontae should be unstopable by the time he reaches high school.</span></p></td> 
    </tr> 
   </tbody> 
  </table> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>