<article id="post-63647" class="post-63647 post type-post status-publish format-standard has-post-thumbnail hentry category-elite-blog category-player-feature tag-eric-ayala tag-future-all-american-camp tag-hamadou-diallo tag-hoop-group tag-mamadou-diarra tag-mikey-dixon tag-putnam-science tag-sanford tag-sedee-keita tag-teddy-bailey"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p><img class="aligncenter size-full wp-image-63662" src="https://hoopgroup.com/wp-content/uploads/2015/08/IMG_3407-2.jpg" alt="IMG_3407 2" width="600" height="300"></p> 
  <p><strong>Teddy Bailey</strong> (<strong><a href="http://twitter.com/theteddybailey" target="_blank">@TheTeddyBailey</a></strong>)<br> </p> 
  <p>The season of summer is notorious for sleeping in late, taking trips to the shore and simply enjoying the beauty of free time.</p> 
  <p><strong>Eric Ayala</strong> hasnt been able to partake in many of those aforementioned experiences this summer.</p> 
  <p>The AAU circuit with WE-R 1s 16U and 17U teams, combined with playing in the U16 FIBA Americas Championship, has caused for nonstop basketball in what has been an eventful few months for Ayala.</p> 
  <p>Its been pretty crazy, Ayala said. I havent been able to get much rest <this summer> so every time I have the opportunity to rest, I take advantage of it. Everything went well, we played good basketball for Team USA, lost in the 17U final in Vegas.</p> 
  <p>Ayala is about to experience a good amount of change in the coming months. The 6-foot-2 guard has not only reclassified to the Class of 2018, but he announced his transfer from Sanford (De.) to Putnam Science (Ct.) over the summer as well. Ayala and fellow Division-1 guard<strong> Mikey Dixon</strong> were both expected to chase the Delaware state championship next season.</p> 
  <p>Im going to miss it, man, Ayala said of Sanford.We were a family, it was a brotherhood. Sanford did a lot for me. Its <Putnam Science> going to be interesting, thats for sure. Itll be pretty fun. It was a family decision, thats basically what it was.</p> 
  <p>Ayala had played two seasons at the Sanford School before deciding to change the destination for his high school career. Ayala will join fellow WE-R 1 high-major <strong>Sedee Keita</strong> (2016), along with 2016 UConn commit <strong>Mamadou Diarra</strong> and 2017 wing <strong>Hamadou Diallo</strong> at Putnam Science next season.</p> 
  <p>The now-2018 guard is a smooth court general that sees the game better than most guards  his offensive upside is mostly done in the form of transition buckets, dribble-drives and creating for his team. Another year of high school basketball will allow Ayala to discover his full potential in terms of jump-shooting and explosiveness.</p> 
  <p>I was focused this summer on just picking up the pace,he said. I wanted to play a little bit faster as a point guard. Ive been playing more efficient this summer, which is extremely important for my position.</p> 
  <p>Ayala is a high-major prospect that has seen his recruiting stock rise over this past calendar year. The rising sophomore holds high-major offers from Cincinnati, St. Johns, Southern Cal, South Carolina and most recently, VCU (8/4). Locally, Ayala has been offered by Temple, Saint Josephs and La Salle.</p> 
  <p>As far as Hoop Groups Future All-American camp goes, Ayalas legs are still churning and his court vision is still making his point guard play a force to be reckoned with. If the exhaustion and restlessness is there, hes not showing it. Ayala is considered as one of the better point guards at FAA camp this week, but he isnt taking his competition very lightly.</p> 
  <p>Theres a few players here that are pretty darn good, Ayala said. I find it fun to play against them and see how it goes.<br> </p> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>