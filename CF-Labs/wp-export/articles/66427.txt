<article id="post-66427" class="post-66427 post type-post status-publish format-standard has-post-thumbnail hentry category-alumni-updates"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <a href="https://hoopgroup.com/wp-content/uploads/2015/11/RomelloTrimble6001.jpg"><img data-caption="Maryland star Romello Trimble" class="aligncenter size-full wp-image-66436" src="https://hoopgroup.com/wp-content/uploads/2015/11/RomelloTrimble6001.jpg" alt="SONY DSC" width="600" height="250"></a>
  <p class="blog2html_p ">Maryland star Romello Trimble</p> 
  <p>Many Hoop Group alum have succeeded at many different levels, from the NBA, Division 1 as well as small colleges and in high school. &nbsp;With the new season just under way, we look at the NCAA Pre-Season All-Americans who are Hoop Group Alumni.</p> 
  <p>2016 NCAA AP PRE-SEASON ALL-AMERICANS</p> 
  <p>6 of the 22 players recognized are Hoop Group Alumni</p> 
  <table id="tablepress-745" class="tablepress tablepress-id-745"> 
   <tbody> 
    <tr class="row-1 odd"> 
     <td class="column-1"><a href="http://hoopgroup.com/?attachment_id=45003" rel="attachment wp-att-45003"><img src="https://hoopgroup.com/wp-content/uploads/2013/11/RomelloTrimble100.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-45003"></a><br> Melo Trimble<br> Maryland</td>
     <td class="column-2"><img src="https://hoopgroup.com/wp-content/uploads/2015/11/937973.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-66472"><br> Georges Niang<br> Iowa State</td>
     <td class="column-3"><img src="https://hoopgroup.com/wp-content/uploads/2015/11/4_501577.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-66473"><br> Nigel Hayes<br> Wisconsin</td> 
    </tr> 
    <tr class="row-2 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2015/11/boo-pics-12-002.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-66474"><br> Wayne Selden<br> Kansas</td>
     <td class="column-2"><img src="https://hoopgroup.com/wp-content/uploads/2015/11/ANTHONYGILL1_29150.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-66475"><br> Anthony Gill<br> Virginia</td>
     <td class="column-3"><img src="https://hoopgroup.com/wp-content/uploads/2015/11/887909.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-66476"><br> Tyler Ulis<br> Kentucky</td> 
    </tr> 
   </tbody> 
  </table> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>