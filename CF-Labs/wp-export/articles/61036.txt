<article id="post-61036" class="post-61036 post type-post status-publish format-standard has-post-thumbnail hentry category-tournament-recap tag-adrian-williams tag-arthur-smith tag-hoop-group tag-img-academy tag-jake-betlow tag-malik-petteway tag-mass-rivals tag-nazreon-reid tag-nicola-akele tag-providence-jam-fest tag-saul-phiri tag-sports-u tag-trey-shifflett tag-walter-whyte"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p><img class="aligncenter size-full wp-image-61046" src="https://hoopgroup.com/wp-content/uploads/2015/05/IMG_8738.jpg" alt="IMG_8738" width="600" height="300"></p> 
  <p>The Providence Jam Fest tipped off today with well over 50 Division 2 &amp; 3 coaches as well as media and scouting services. In what was a long day of hoops, we saw why a few guys here are big time prospects, and came across a few names that might be on their way to becoming one. Here are a few notable players who were in action today:</p> 
  <p><strong>Arthur Smith (NYC Wizards)&nbsp;</strong></p> 
  <p>Its not often that a 13u player leads our list of Notable Players, but not many 13 year olds put up the numbers Arthur did today. In a big time win for the NYC Wizards, Arthur scored 38 points. He was simply unstoppable offensively and well deserved of recognition.</p> 
  <p><strong>Saul Phiri (Mass Rivals)</strong></p> 
  <p>Saul was a monster today, and a big reason why <em>Mass Rivals</em> is still alive to a 17u championship. When he is on, he is a deadly shooter, just ask the <em>Early Risers</em>. He made five 3s in a game against them, including two back-to-back when his team was down 7. He showed the clutch factor as well, making two free throws with his team down one late in the game. Theres a reason schools like La Salle, George Washington and Hoftstra (among others) have offered him.</p> 
  <p><strong>Malik Petteway (USAD)</strong></p> 
  <p>Malik is one of the best players still available in New England. Hes a big 68 player who has a great mid range game and also possesses soft touch around the rim. He was a big time player for <em>USAD</em> today and a highlight maker. He had a powerful slam over two defenders today that blew the crowd at RIC away. Be sure to look for that one on the mixtape!</p> 
  <p><strong>Jake Betlow (Hoop Heaven Elite)</strong></p> 
  <p><em>Hoop Heaven Elite</em> enjoyed a nice win today thanks to some hot shooting from the outside. Leading the charge was Jake Betlow. The New Jersey guard is an absolute sharp shooter, who can make shots in bunches. Teams are slowly starting to learn that if you dont cover him, he will hurt you.</p> 
  <p><strong>Nazreon Reid (Sports U)</strong></p> 
  <p><em>Sports U</em> has two teams in the 16u Elite Eight. One of them is their 15u team, led by phenom Nazreon Reid. Reid is one of the most sought after players in his class. Hes 68 with incredible athleticism and even more upside. He plays above the rim, trying to rip it down every chance he has. Hes also good at blocking and altering shots inside.&nbsp;Its easy to see why schools like Kansas, Maryland, Virginia, Villanova, and many more have already offered him.</p> 
  <p><strong>Trey Shifflett (York Ballers)</strong></p> 
  <p>Trey has been a consistent player for the <em>York Ballers</em> for a few seasons now. Hes a trust worthy point guard who is more than comfortable with the ball in his hands controlling the tempo on offense. Hes also a clutch player for York, scoring big time buckets late in games when they need him. He showed off his scoring ability yesterday, leading the York Ballers to the Sweet 16.</p> 
  <p><strong>Walter Whyte (High Rise Elite)</strong></p> 
  <p>Walter is a great player for the <em>High Rise Elite</em> team out of Connecticut. He is a capable scorer, as he showed in yesterdays finale, scoring 24 points in an overtime win. Hes an athletic and bouncy guard/wing who is deadly in transition. Most of his points came off the break; hes tough to stop when he has space to work.</p> 
  <p><strong>Adrian Williams (Mass Rivals)</strong></p> 
  <p>Adrian had a good day of basketball in the 16u bracket, playing up for <em>Mass Rivals</em>. Hes the floor general of the team, which is no surprise seeing as hes the brother of Milwaukee Buck point guard Michael Carter-Williams. Adrian showed hes an elite finisher, and possesses a great shooting stroke from the outside.</p> 
  <p><strong>Nicola Akele (IMG Academy)</strong></p> 
  <p>A name most Rhode Islanders should get familiar with, Nicola is a very enticing prospect from <em>IMG Academy</em>. At 68 hes long and athletic, very athletic. He attacks the glass hard and is a streaky shooter. When he gets going he is tough to contain. Despite a Final &nbsp;Four loss for IMG, it is easy to see why the University of Rhode Island liked him.</p> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>