<article id="post-14347" class="post-14347 post type-post status-publish format-standard has-post-thumbnail hentry category-tournament-recap tag-hgshowcase tag-north-brunswick tag-west-orange"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p><a href="http://hoopgroup.com/wp-content/uploads/2011/12/DSC09145.jpg"><img src="http://hoopgroup.com/wp-content/uploads/2011/12/DSC09145-1024x680.jpg" alt="" title="DSC09145" width="640" height="425" class="aligncenter size-large wp-image-14348"></a></p> 
  <p>It was all about defense for North Brunswick in their 63-52 win over West Orange to open Hoop Groups 2011 Tip Off Showcase at West Orange High School. </p> 
  <p>Led by 27 points from junior Iverson Fleming, the Raiders got up early, leading 6-1, and used active defense and strong pressure to fend off a late West Orange first-half run and take control for good. </p> 
  <p>Its all about my teammates. They always have my back, said Fleming. One team, one heartbeat is our motto. Thats what we go by.</p> 
  <p>Also for North Brunswick, senior Dion Rogers and his athletic play at the guard spot facilitated the offense and did a good job to get into the passing lanes on the defensive end. </p> 
  <p>Rogers and Fleming worked well in combination in the backcourt, anchoring this North Brunswick team.</p> 
  <p>Our best possessions offensively come when guys get four, five touches, said Ed Breheney. His team forced West Orange into tough passes, causing turnovers and run-outs in transition. Defensive pressure, thats our thing. Everybody runs a man set and we want to try to get them out of that.</p> 
  <p>Hoop Group Elite Director Kevin Driscoll sees both players on scholarship next season, with Fleming as a borderline Division I player, and Rogers as a strong guard in Division II. </p> 
  <p>Were sort of old school in that we dont have really a true one and true two, Breheney explained. They feed off each other and the unselfishness is what I like offensively. </p> 
  <p>The Raiders next game is against Manasquan on December 22nd. </p> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>