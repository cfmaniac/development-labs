<article id="post-91221" class="post-91221 post type-post status-publish format-standard has-post-thumbnail hentry category-high-school-showcase-2 category-tournament-recap"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p style="text-align: center;"><a href="https://hoopgroup.com/wp-content/uploads/2017/12/header.jpg"><img class="size-full wp-image-91223 aligncenter" src="https://hoopgroup.com/wp-content/uploads/2017/12/header.jpg" alt="" width="960" height="350"></a></p> 
  <p><img class="wp-image-86647  alignleft" src="https://hoopgroup.com/wp-content/uploads/2011/01/facebook-logo_318-49940-300x300.jpg" alt="" width="42" height="39"></p> 
  <p style="text-align: left;"><a href="https://www.facebook.com/pg/thehoopgroup/photos/?tab=album&amp;album_id=1639918216072703">View Photos on Hoop Groups Facebook<br> </a></p> 
  <p>&nbsp;</p> 
  <table style="width: 550px; background-color: #d6d4d4; height: 26px;"> 
   <tbody> 
    <tr> 
     <td style="text-align: center; width: 137.234375px;"><a href="#readingmvp">GAME MVPs</a></td> 
     <td style="text-align: center; width: 90.609375px;">RECAPS</td> 
     <td style="text-align: center; width: 104.890625px;"><a href="#readingresults">RESULTS</a></td> 
     <td style="text-align: center; width: 189.296875px;"><a href="#outsidemediareading">OUTSIDE MEDIA</a></td> 
    </tr> 
   </tbody> 
  </table> 
  <h2 id="readingmvp" style="text-align: center;">GAME MVPs</h2> 
  <hr> 
  <p style="text-align: center;"><a href="https://hoopgroup.com/wp-content/uploads/2017/12/sean_simon.jpg"><img class="wp-image-91233 aligncenter" src="https://hoopgroup.com/wp-content/uploads/2017/12/sean_simon-1024x683.jpg" alt="" width="465" height="365"></a> <a href="https://hoopgroup.com/wp-content/uploads/2017/12/ricky_lopez.jpg"><img class="wp-image-91232 aligncenter" src="https://hoopgroup.com/wp-content/uploads/2017/12/ricky_lopez-1024x683.jpg" alt="" width="466" height="334"></a> <a href="https://hoopgroup.com/wp-content/uploads/2017/12/luis_garcia.jpg"><img class="wp-image-91231 aligncenter" src="https://hoopgroup.com/wp-content/uploads/2017/12/luis_garcia-1024x683.jpg" alt="" width="471" height="405"></a> <a href="https://hoopgroup.com/wp-content/uploads/2017/12/jacob_oconnell.jpg"><img class="wp-image-91230 aligncenter" src="https://hoopgroup.com/wp-content/uploads/2017/12/jacob_oconnell-1024x683.jpg" alt="" width="478" height="403"></a></p> 
  <h2 id="readingresults" style="text-align: center;">Results</h2> 
  <hr> 
  <table> 
   <tbody> 
    <tr> 
     <td><strong>St. Josephs Prep.</strong></td> 
     <td>vs</td> 
     <td>Berks Catholic.</td> 
     <td>&nbsp; 48  25</td> 
     <td><em><a href="https://hoopgroup.com/wp-content/uploads/2017/12/berks-vs-st-joes-prep.pdf">box scores</a></em></td> 
    </tr> 
    <tr> 
     <td>Reading</td> 
     <td>vs</td> 
     <td><strong>LaSalle&nbsp;</strong></td> 
     <td>&nbsp;45  68</td> 
     <td><em><a href="https://hoopgroup.com/wp-content/uploads/2017/12/reading-vs-lasalle-college.pdf">box scores</a></em></td> 
    </tr> 
   </tbody> 
  </table> 
  <p style="text-align: center;"><strong>Recap</strong></p> 
  <hr> 
  <h2 id="outsidemediareading" style="text-align: center;">&nbsp;Outside Media</h2> 
  <hr> 
  <p style="text-align: left;"><strong>&nbsp;City of Basketball Love</strong> | <a href="http://www.cityofbasketballlove.com/news_article/show/867236?referrer_id=1712896">Reading Tip-Off: Snow cant slow LaSalle College HS or St. Joes Prep</a>&nbsp;<em>Michael Bullock&nbsp;(@thebullp_n)</em></p> 
  <p><strong>LL Hoops</strong> |<a href="http://llhoops.com/berks-beat/philly-teams-rule-hoop-group-reading-classic/"> Philly Teams Rule Hoop Group Reading Classic</a> |&nbsp;<em>Bruce Badgley (@BadgleyBruce)</em></p> 
  <p><strong>WSJP: St. Joes Prep Student Broadcasting Club Radio</strong> | <a href="http://www.blogtalkradio.com/wsjp/2017/12/16/st-joes-prep-vs-berks-catholic-basketball">St Joes Prep vs Berks Basketball&nbsp;</a></p> 
  <p><strong>Reading Eagle Newspaper |&nbsp;</strong><a href="http://www.readingeagle.com/apps/pbcs.dll/gallery?Site=RE&amp;Date=20171215&amp;Category=SPORTS&amp;ArtNo=121509998&amp;Ref=PH">Berks Catholic vs St. Joes Prep Photo Gallery&nbsp;</a></p> 
  <p><strong>Reading Eagle Newspaper |</strong><a href="http://www.readingeagle.com/apps/pbcs.dll/gallery?Site=RE&amp;Date=20171215&amp;Category=SPORTS&amp;ArtNo=121509997&amp;Ref=PH">Reading vs LaSalle College HS Photo Gallery</a></p> 
  <p>&nbsp;</p> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>