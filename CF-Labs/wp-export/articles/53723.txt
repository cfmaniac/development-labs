<article id="post-53723" class="post-53723 post type-post status-publish format-standard has-post-thumbnail hentry category-skills-camp-updates tag-hgskills tag-skills"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p><a href="https://www.facebook.com/media/set/?set=a.735071509890716.1073741960.110289042368969&amp;type=3"><img class="aligncenter size-full wp-image-53732" alt="2014 HGSKILLS III (c)" src="https://hoopgroup.com/wp-content/uploads/2014/07/2014-HGSKILLS-III-c-e1406414231548.jpg" width="600" height="199"></a></p> 
  <p>Skills Camp III wrapped up the first half of our boys <a href="http://hoopgroup.com/skills/">summer camps</a>, and thanks to the effort of all our campers it was clearly one of the most competitive camps of the summer! While the schedule of early-bird workouts, stations, lectures and games was the same as the other<a href="http://hoopgroup.com/skills/skills-camps/"> Skills Camps</a>, the play on the court was highlighted by many standout individuals, some of which are listed below.</p> 
  <p>To catch up on all the action from Skills Camp III you can check out our <a href="https://www.facebook.com/thehoopgroup">Facebook</a>, <a href="https://twitter.com/TheHoopGroup">Twitter</a>, <a href="http://instagram.com/hoopgroup">Instagram</a> and <a href="https://www.youtube.com/channel/UCc1GQKNW8M0991uoLzifPrg">YouTube</a> pages.</p> 
  <p><iframe src="//www.youtube.com/embed/OamrA21xr7c" height="315" width="560" allowfullscreen frameborder="0"></iframe><br> <strong style="font-size: 18px;">PLAYERS OF THE WEEK</strong></p> 
  <p><strong>DAVID ALIZE</strong><br> A talented player on both ends of the court, Alize hustles on every play until he hears a whistle.<br> is a great basketball player that will hustle every play of the game.</p> 
  <p><strong>JAHKAI BARNES</strong><br> A very good shooter, whos not afraid to take big shots. Can get to the basket at will and is a relentless defender.</p> 
  <p><strong>MICHAEL BEERS</strong><br> Always kept a positive outlook on the court. Worked well with his teammates and displayed great leadership the whole week.</p> 
  <p><strong>GAVIN BIGELOW</strong><br> A good point guard who directed the floor well and was one of his teams best shooters at camp.</p> 
  <p><strong>JACK BOYLE</strong><br> Played both ends of the floor extremely well and was one of the more accurate free throw shooters at camp, winning the ACC Division Free Throw Championship.</p> 
  <p><strong>COOPER BUCKLEY</strong><br> The ultimate team player, he is always finding different ways to contribute to the game. His offensive skills are matched by his defensive ones, making him the complete player.</p> 
  <p><strong>JAVON BURKE</strong><br> Used his speed to his advantage all week long as he led his team in totals fast-break points and steals. Burke also played big in big spots including the Big East Division All-Star Game in which he was named MVP.</p> 
  <p><strong>JAMIE BYERS</strong><br> Earned the Big East Divisions Floor General Award based on his strong leadership and excellent passing. Always seemed to know where a teammate was going to be and sometimes put the ball there before anyone knew what was happening.</p> 
  <p><a href="https://hoopgroup.com/wp-content/uploads/2014/07/ACC-HS-Jameson-Caruso-e1406416324143.jpg"><img class="aligncenter size-full wp-image-53746" alt="ACC HS Jameson Caruso" src="https://hoopgroup.com/wp-content/uploads/2014/07/ACC-HS-Jameson-Caruso-e1406416324143.jpg" width="400" height="242"></a><br> <strong>JAMESON CARUSO </strong><br> Caruso is an incredible player who makes moves on the court years beyond his age. His skill set is extremely versatile ranging from deep threes to Euro steps. Caruso capped his week by winning the ACC Division Hot Shot Championship.</p> 
  <p><strong>ROYCE CHESTER</strong><br> Chester could flat out score. He was one of the quickest guards in the NBA Division and he hustled and played defense harder than any other player on his way to winning the Divisions MVP Award.</p> 
  <p><strong>KENDALL CLARK</strong><br> Was instrumental all week long in setting up his teams offense and directing his teammates on and off the court.</p> 
  <blockquote class="twitter-tweet" lang="en">
   <p><a href="https://twitter.com/hashtag/HGSkills?src=hash">#HGSkills</a> Director <a href="https://twitter.com/ryanjfinch1212">@ryanjfinch1212</a> demonstrates the importance of spacing Hoop Group Skills: <a href="http://t.co/h1gekm6V1x">http://t.co/h1gekm6V1x</a> via <a href="https://twitter.com/YouTube">@YouTube</a></p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/statuses/491391812309774336">July 22, 2014</a></p>
  </blockquote> 
  <p><script charset="utf-8" type="text/javascript" src="//platform.twitter.com/widgets.js" async></script><br> <strong>DEVIN COOPER</strong><br> Cooper keeps defenses off balanced by his ability to beat defenders off the dribble and with his steady jump shot. He has a high basketball IQ and finds his teammates extremely well. Cooper is shown above running through a drill with Skills Director Ryan Finch.</p> 
  <p><strong>TERRENCE CROWLEY</strong><br> A long and athletic two guard, Crowley can shoot the ball for a very high percentage from long range.</p> 
  <p><strong>DREW DIPIETRO</strong><br> A skilled big man who improved during camp. He was a rebounding machine who, according to his coach, was a very coachable player.</p> 
  <p><strong>HENRY DONOHUE</strong><br> A very tough shooting guard who can slash to the basket and was very dangerous on the fastbreak.</p> 
  <p><strong>ISIAH EZENEKWE</strong><br> A hard worker with great leadership skills, Ezenekwe was always composed on the court no matter the situation. A great role model for his teammates he helped lead his Florida squad to the SEC Division semi-finals. He was also named the camps Iron Man Award winner to go along with All-Star Game MVP.</p> 
  <p><strong>JOSH FENNER</strong><br> His ability to get to the rim led to many scoring opportunities for his team, and even when not driving, his solid jumper helped to keep defenses honest.</p> 
  <p><strong>JOSEPH FLOWERS</strong><br> A great on-the-ball defender who also attacked the rim with no fear during his time at camp. Was a team leader throughout the week.</p> 
  <p><strong>LARIUS FLOYD</strong><br> A very talented player who can play multiple positions. He can finish really well with both hands. His coach characterized him as a good leader who is going to be a special player.</p> 
  <p><strong>JORDAN FOX</strong><br> A dangerous shooter for mid to long range, Fox also won the NBA Division Free Throw Championship.</p> 
  <p><strong>ZACH FREEMAN</strong><br> Went from a timid player at the start of camp to an aggressive one by the end. A very unselfish player Freeman knew when and when not to take the shot and his strong defensive play led his team all week.</p> 
  <p><strong>ISAIAH GERENA</strong><br> A dominant rebounder on both ends of the floor, he was also a tremendous shot blocker and ran the floor well.</p> 
  <p><strong>MATT HAMRAH</strong><br> A physical guard who can finish around the rim even with contact. Seemed to always make the right decision with his passes and was up for any defensive assignment given to him.</p> 
  <p><a href="https://hoopgroup.com/wp-content/uploads/2014/07/DSCN5871-e1406415179558.jpg"><img class="aligncenter size-full wp-image-53739" alt="DSCN5871" src="https://hoopgroup.com/wp-content/uploads/2014/07/DSCN5871-e1406415179558.jpg" width="400" height="280"></a></p> 
  <p><strong>UNIQUE HARRIS</strong><br> A very skilled point guard, his explosiveness allows him to not only blow by defenders, but finish at the rim. He sets the tone with his defense, which his teammates feed off of.</p> 
  <p><strong>JORDAN HECK</strong><br> Played both ends of the floor really well and it was clear he improved his game during his time at camp. His hard work earned him the NBA Divisions Mr. Hustle Award.</p> 
  <p><strong>JACOB HOPPING</strong><br> Went after every loose ball and he does anything it takes to help the team win. Hes the true definition of a team player. His style of play led to his being named Mr. Hustle in the SEC Division.</p> 
  <p><strong>DINO IVACKOVIC </strong><br> Most complete player on his team, Ivackovic can do a little bit of everything. A very unselfish player who exhibited excellent sportsmanship at camp.</p> 
  <p><strong>DWAYNE JONES</strong><br> A very heady point guard with an high basketball IQ, he is very good at picking when to be aggressive and when to get his teammates involved. Jones superb all-around skill set led to his being named the SEC Divisions MVP.</p> 
  <p><strong>ISAIAH JOSUE</strong><br> Josue has had a very productive summer at Hoop Group Skills Camps and that continued this past week. A great all-around player with great point guard skills and a lot of potential.</p> 
  <div id="fb-root"></div> 
  <p><script type="text/javascript">// <!<CDATA<
(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)<0>; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/en_US/all.js#xfbml=1"; fjs.parentNode.insertBefore(js, fjs); }(document, 'script', 'facebook-jssdk'));
// >>&gt;</script></p> 
  <div class="fb-post" data-href="https://www.facebook.com/thehoopgroup/photos/a.735071509890716.1073741960.110289042368969/736666149731252/?type=1" data-width="466"> 
   <div class="fb-xfbml-parse-ignore">
    <a href="https://www.facebook.com/thehoopgroup/photos/a.735071509890716.1073741960.110289042368969/736666149731252/?type=1">Post</a> by 
    <a href="https://www.facebook.com/thehoopgroup">The Hoop Group</a>.
   </div> 
  </div> 
  <p><strong>BEN KAPLAN</strong><br> Dominated the entire ACC Division on his way to being named MVP, he respected all of his opponents and all his teammates enjoyed playing with him. Filled with potential and a huge upside. Kaplan is pictured above with his ACC Division Championship Wake Forest team.</p> 
  <p><strong>HARRISON LESNIK</strong><br> An aggressive big man who improved daily, he was clearly the best rebounder at camp this week.</p> 
  <p><strong>GRAHAM LEMANS</strong><br> The six-foot-five rising freshman had a great week holding down the paint and showing an array of moves.</p> 
  <p><strong>REGGIE LUIS</strong><br> An extremely talented player with the ability to score the basketball in numerous ways, Luis is a charismatic team leader with a lethal jump shot. He clearly possesses a complete game which is full of a deadly combination of talent and skill.</p> 
  <p><strong>TREVOR MARION</strong><br> Can play any position on the floor and his ability to handle the ball, drive to the rim and hit the 3-point shot make him a very solid player.</p> 
  <p><strong>QUENTIN MCKINGHT</strong><br> Over the week McKnight drastically improved his ability to finish layups and to grab both offensive and defensive rebounds which led to his being named the NBA Divisions Most Improved Player.</p> 
  <p><strong>JOEY MITCHELL</strong><br> Was the clearly the best shooter on his team and over the course of the past week showed he was capable of hitting both the mid and long-range jumper consistently.</p> 
  <p><iframe src="//instagram.com/p/qxmRAzw8qn/embed/" height="710" width="612" frameborder="0" scrolling="no"></iframe><br> <strong><a href="http://youtu.be/S0hjnzTCgqU?list=UUc1GQKNW8M0991uoLzifPrg">ALEX RATNER</a></strong><br> All week long he worked very hard and was determined to get better. He possessed a solid skill set and a high basketball IQ which led to his being named the SEC Divisions Floor General award winner.</p> 
  <p><strong>JAIDEN REID</strong><br> Was a consistent defensive presence that forced opponents to turn the ball over all week.</p> 
  <p><strong>NICK RESTIVO</strong><br> His leadership skills along with his work ethic and rebounding skills helped to earn Restivo a spot on this list.</p> 
  <p><strong>NICK REVELLO</strong><br> Always seemed to play big in when the situation was tight. When his team needed a stop, he was there. When his team needed a bucket, he was there.</p> 
  <p><strong>JACK RILEY</strong><br> Rebounds the ball very well and kept a positive attitude every day.</p> 
  <p><a href="https://hoopgroup.com/wp-content/uploads/2014/07/DSCN5845-e1406415210129.jpg"><img class="aligncenter size-full wp-image-53738" alt="DSCN5845" src="https://hoopgroup.com/wp-content/uploads/2014/07/DSCN5845-e1406415210129.jpg" width="400" height="415"></a><br> <strong>RAMFIS RODRIGUEZ</strong><br> A lockdown defender who can take over on the offensive end as well. He has an above-average shot and a great attitude.</p> 
  <p><strong>HANK SHAPIRO</strong><br> Bringing a ton of energy to the game, Shapiro is aggressive on both ends of the floor and is always going hard after every rebound. On the last night of camp, Shapiro claimed ACC Division All-Star Game MVP honors.</p> 
  <p><strong>JAYLEN SMITH</strong><br> Had one of the most complete skill sets of all the campers this week. He always played hard, which is all I ask of my players, said his coach Jakeem Bogans, I feel he is going to be a very good player at the high school level.</p> 
  <p><strong>JAMED TANNER</strong><br> Having the ability to play both inside and out, Tanner clearly plays bigger than his size. All week long he played with great enthusiasm, energy and attitude.</p> 
  <p><strong>SCOTT THOMAS</strong><br> Took his shooting to a new level over the course of the five-day camp, Thomas developed a propensity for always knocking down his jump shots.</p> 
  <p><strong>MATT TRENCHY</strong><br> Scored the basketball from anywhere on the floor and his ball handling abilities helped him always either find a way to the basket or to pass to an open teammate.</p> 
  <p><strong>ARI WEINGARTEN</strong><br> Great team leader who excelled in any role he was put in this week. His offensive play and decision making improved greatly this past week.</p> 
  <p><strong style="font-size: 18px;">REMAINING SUMMER CAMPS</strong></p> 
  <p>From July 25 until August 6 Hoop Group Skills Camp will host our<a href="http://hoopgroup.com/girls/girls-skills-camp/"> summer girls programs</a> and than from August 8-26 will wrap up our summer by bringing the boys back. Included in that final run of boys camps will be<a href="http://www.hoopgroup.com/registration/index.php?cPath=52_114"> Skills Camp IV (August 10-14)</a> and <a href="http://www.hoopgroup.com/registration/index.php?cPath=52_111">Bob Hurley Camp (August 17-21)</a>.</p> 
  <p style="text-align: center;">Spaces are filling up fast for our girls and boys sessions<br> so <a href="http://hoopgroup.com/skills/skills-camp-calendar/"><strong style="font-size: 18px;">CLICK HERE</strong> </a>to register !!!</p> 
  <p><a href="http://hoopgroup.com/skills/"><img class="aligncenter size-full wp-image-46609" alt="Boys Skills 2014 cover (1)" src="https://hoopgroup.com/wp-content/uploads/2014/01/Boys-cover-1-e1395145418166.jpg" width="400" height="267"></a></p> 
  <p>&nbsp;</p> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>