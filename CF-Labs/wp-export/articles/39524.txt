<article id="post-39524" class="post-39524 post type-post status-publish format-standard has-post-thumbnail hentry category-elite-camp-updates category-player-feature category-uncategorized tag-elite-camp tag-hoop-group-elite-camp"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p><a href="https://hoopgroup.com/wp-content/uploads/2013/07/simon-justin1.jpg"><img src="https://hoopgroup.com/wp-content/uploads/2013/07/simon-justin1.jpg" alt="SONY DSC" width="600" height="654" class="aligncenter size-full wp-image-39552"></a>Justin Simon came east to Hoop Group Elite II and clearly made an impression on several major collegiate coaches.</p> 
  <p><strong>Simon (Temecula Valley (Calif.) High School/ 6-4/ 2015)</strong>, who just two weeks ago transferred from Eleanor Roosevelt (Calif.) High School to Temecula, was a member of Elite IIs NBA East Upperclassmen Division championship team and was named an NBA East Underclassmen All-Star.</p> 
  <p>But as nice as those honors are, the real reward for Simon came from the exposure to collegiate coaches at the Hoop Group camp.</p> 
  <p>According to sources at the San Diego Union-Tribune, coaches from Minnesota, Villanova, West Virginia, Florida State and Virginia Tech were impressed with Simon. This is in addition to the offers he has already received from UC-Irvine, San Jose State, San Diego and Santa Clara and the interest from Iowa, Penn State and USC to name a few. </p> 
  <p>Simon plans to make an unofficial visit to USC in the near future. (This story will be updated) <a href="https://twitter.com/pfebbraro#">@pfebbraro</a></p> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>