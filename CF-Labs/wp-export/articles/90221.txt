<article id="post-90221" class="post-90221 post type-post status-publish format-standard hentry category-uncategorized"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <h2></h2> 
  <h2></h2> 
  <h2 style="text-align: center;"><em><strong>Shore Player of the Year</strong></em></h2> 
  <hr> 
  <h2><a href="http://hoopgroup.com/headquarters-blog/uncategorized/2017-national-signing-day/attachment/capture-3/" rel="attachment wp-att-90355"><img class=" wp-image-90355 alignleft" src="https://hoopgroup.com/wp-content/uploads/2017/11/Capture-1.jpg" alt="" width="217" height="184"></a></h2> 
  <p><a href="http://hoopgroup.com/headquarters-blog/uncategorized/2017-national-signing-day/attachment/vt/" rel="attachment wp-att-90372"><img class="aligncenter  wp-image-90372" src="https://hoopgroup.com/wp-content/uploads/2017/11/vt.png" alt="" width="114" height="54"></a></p> 
  <p><em><strong>DARA MABREY |</strong> SENIOR </em></p> 
  <p>MANASQUAN- <a href="http://hoopgroup.com/headquarters-blog/uncategorized/player-of-year/">SHORE PLAYER OF THE YEAR</a>  Virginia Tech</p> 
  <h2></h2> 
  <h2 style="text-align: center;"><i>NATIONAL SIGNING DAY</i></h2> 
  <hr> 
  <p>&nbsp;</p> 
  <table style="height: 1198px; width: 556px;" cellpadding="10"> 
   <tbody> 
    <tr> 
     <td style="width: 143.913px;"><img class="alignleft wp-image-84229 " src="https://hoopgroup.com/wp-content/uploads/2017/03/DSC0685-e1510332238416-300x296.jpg" alt="" width="211" height="208"></td> 
     <td style="width: 318.087px;"><a href="http://hoopgroup.com/headquarters-blog/uncategorized/2017-national-signing-day/attachment/vermont/" rel="attachment wp-att-90368"><img class="aligncenter  wp-image-90368" src="https://hoopgroup.com/wp-content/uploads/2017/11/Vermont.png" alt="" width="81" height="79"></a><p></p> <p><em><strong>ROSE CAVERLY | </strong>SENIOR </em></p> <p>RED BANK CATHOLIC- COMBO- U of Vermont wins a major recruiting battle</p> <p>&nbsp;</p></td> 
    </tr> 
    <tr> 
     <td style="width: 143.913px;"><img class=" wp-image-84227 alignleft" src="https://hoopgroup.com/wp-content/uploads/2017/03/untitled-7.png" alt="" width="212" height="182"></td> 
     <td style="width: 318.087px;"><a href="http://hoopgroup.com/headquarters-blog/uncategorized/2017-national-signing-day/attachment/monmouth-2/" rel="attachment wp-att-90369"><img class="aligncenter wp-image-90369 " src="https://hoopgroup.com/wp-content/uploads/2017/11/monmouth-e1510334191313.png" alt="" width="100" height="90"></a><p></p> <p><em><strong>LUCY THOMAS |&nbsp;</strong>JUNIOR</em></p> <p>ST. ROSE HIGH SCHOOL-FORWARD- Monmouth U gets it first Shore superstar</p> <p>&nbsp;</p></td> 
    </tr> 
    <tr> 
     <td style="width: 143.913px;"><img class=" wp-image-84230 alignleft" src="https://hoopgroup.com/wp-content/uploads/2017/03/untitled-8.png" alt="" width="214" height="288"></td> 
     <td style="width: 318.087px;"><a href="http://hoopgroup.com/headquarters-blog/uncategorized/2017-national-signing-day/attachment/lehighmountainhawks-svg_/" rel="attachment wp-att-90370"><img class="aligncenter  wp-image-90370" src="https://hoopgroup.com/wp-content/uploads/2017/11/LehighMountainHawks.svg_.png" alt="" width="77" height="75"></a><p></p> <p><strong>KATIE RICE |&nbsp;</strong>SENIOR</p> <p>RED BANK CATHOLIC- SWING- <em>Power 5 offers are on the table after a breakout season</em></p></td> 
    </tr> 
    <tr> 
     <td style="width: 143.913px;"><img class="alignleft wp-image-84231 " src="https://hoopgroup.com/wp-content/uploads/2017/03/untitled-9-e1510332315975.png" alt="" width="213" height="241"></td> 
     <td style="width: 318.087px;"><a href="http://hoopgroup.com/headquarters-blog/uncategorized/2017-national-signing-day/attachment/columbia-3/" rel="attachment wp-att-90371"><img class="aligncenter  wp-image-90371" src="https://hoopgroup.com/wp-content/uploads/2017/11/columbia.png" alt="" width="111" height="103"></a><p></p> <p><em><strong>MIKAYLA MARKHAM |&nbsp;</strong></em><em>SENIOR</em></p> <p>ST. ROSE HIGH SCHOOL-COMBO- Columbia U gets a major steal and special talent</p> <p>&nbsp;</p></td> 
    </tr> 
    <tr> 
     <td style="width: 143.913px;"><a href="http://hoopgroup.com/headquarters-blog/uncategorized/2017-national-signing-day/attachment/hayley-moore/" rel="attachment wp-att-90380"><img class="aligncenter size-full wp-image-90380" src="https://hoopgroup.com/wp-content/uploads/2017/11/Hayley-Moore.jpg" alt="" width="460" height="360"></a></td> 
     <td style="width: 318.087px;"><a href="http://hoopgroup.com/headquarters-blog/uncategorized/2017-national-signing-day/attachment/binghamtonbearcats/" rel="attachment wp-att-90381"><img class="aligncenter  wp-image-90381" src="https://hoopgroup.com/wp-content/uploads/2017/11/BinghamtonBearcats.png" alt="" width="123" height="93"></a><p></p> <p><em><strong>HALEY MOORE |&nbsp;</strong></em><em>SENIOR</em></p> <p>RED BANK CATHOLIC- Binghamton</p></td> 
    </tr> 
    <tr> 
     <td style="width: 143.913px;">&nbsp;<a href="http://hoopgroup.com/headquarters-blog/uncategorized/2017-national-signing-day/attachment/volpe/" rel="attachment wp-att-90378"><img class="aligncenter size-full wp-image-90378" src="https://hoopgroup.com/wp-content/uploads/2017/11/volpe.jpg" alt="" width="520" height="390"></a></td> 
     <td style="width: 318.087px;"><a href="http://hoopgroup.com/headquarters-blog/uncategorized/2017-national-signing-day/attachment/ic-athletics-primary-rgb/" rel="attachment wp-att-90379"><img class="aligncenter  wp-image-90379" src="https://hoopgroup.com/wp-content/uploads/2017/11/IC-Athletics-Primary-RGB.png" alt="" width="117" height="97"></a><p></p> <p><em><strong>CARA VOLPE |&nbsp;</strong></em><em>SENIOR</em></p> <p>COLTS NECK- Ithaca College</p></td> 
    </tr> 
    <tr> 
     <td style="width: 143.913px;">&nbsp;<a href="http://hoopgroup.com/headquarters-blog/uncategorized/2017-national-signing-day/attachment/allentown/" rel="attachment wp-att-90376"><img class="aligncenter wp-image-90376 size-full" src="https://hoopgroup.com/wp-content/uploads/2017/11/allentown-e1510335011842.jpg" alt="" width="595" height="467"></a></td> 
     <td style="width: 318.087px;"><a href="http://hoopgroup.com/headquarters-blog/uncategorized/2017-national-signing-day/attachment/blackbirds/" rel="attachment wp-att-90375"><img class="aligncenter  wp-image-90375" src="https://hoopgroup.com/wp-content/uploads/2017/11/blackbirds.jpg" alt="" width="207" height="85"></a><p></p> <p><em><strong>RYAN WEISE |&nbsp;</strong></em><em>SENIOR</em></p> <p>ALENTOWN HS- Brooklyn Blackbirds</p></td> 
    </tr> 
    <tr> 
     <td style="width: 143.913px;"><a href="http://hoopgroup.com/headquarters-blog/uncategorized/2017-national-signing-day/attachment/tori-hyduke/" rel="attachment wp-att-90373"><img class="aligncenter size-full wp-image-90373" src="https://hoopgroup.com/wp-content/uploads/2017/11/tori-hyduke.jpg" alt="" width="665" height="865"></a></td> 
     <td style="width: 318.087px;"><em><strong><a href="http://hoopgroup.com/headquarters-blog/uncategorized/2017-national-signing-day/attachment/george-washington/" rel="attachment wp-att-90374"><img class="aligncenter  wp-image-90374" src="https://hoopgroup.com/wp-content/uploads/2017/11/george-washington.png" alt="" width="118" height="81"></a></strong></em><p></p> <p><em><strong>TORI HYDUKE |&nbsp;</strong></em><em>SENIOR</em></p> <p>RUMSON- George Washington</p> <p>&nbsp;</p></td> 
    </tr> 
    <tr> 
     <td style="width: 143.913px;"><a href="http://hoopgroup.com/headquarters-blog/uncategorized/2017-national-signing-day/attachment/lovin/" rel="attachment wp-att-90382"><img class="aligncenter wp-image-90382 size-full" src="https://hoopgroup.com/wp-content/uploads/2017/11/lovin-e1510335579185.jpg" alt="" width="413" height="398"></a></td> 
     <td style="width: 318.087px;"><em><strong><a href="http://hoopgroup.com/headquarters-blog/uncategorized/2017-national-signing-day/attachment/st-joes/" rel="attachment wp-att-90383"><img class="aligncenter  wp-image-90383" src="https://hoopgroup.com/wp-content/uploads/2017/11/st-joes.png" alt="" width="117" height="93"></a></strong></em><p></p> <p><em><strong>LOVIN MARSICANO |&nbsp;</strong></em><em>SENIOR</em></p> <p>ST ROSE- St Joeseph</p></td> 
    </tr> 
   </tbody> 
  </table> 
  <p>&nbsp;</p> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>