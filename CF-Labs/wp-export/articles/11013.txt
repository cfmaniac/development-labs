<article id="post-11013" class="post-11013 post type-post status-publish format-standard has-post-thumbnail hentry category-player-feature tag-jerome-hairston tag-towson"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p><a href="http://hoopgroup.com/wp-content/uploads/2011/04/Jerome-Hairston.jpg"><img src="http://hoopgroup.com/wp-content/uploads/2011/04/Jerome-Hairston-1024x768.jpg" alt="Jerome Hairston" title="Jerome Hairston" width="640" height="480" class="alignleft size-large wp-image-9820"></a></p> 
  <p>Since winning the <a href="http://hoopgroup.com/hoopgroup/team-tournaments/grassroots-events/southern-hoop-group-jam-fest.php">Hoop Group Southern Jam Fest</a>, Jerome Hairston and his AAU team, <a href="http://eastcoastfusion.com/">East Coast Fusion</a> has been on the road competing at different tournaments always battling to get to the top.</p> 
  <p>We played in Richmond, we did okay as a team but werent in good enough shape to pull it out, said Hairston of their second place finish. We played at the Bob Gibbons where we lost in the quarters.</p> 
  <p>On assessing his own game Hairston said he played well in the tournament, at times though winded and having to conserve energy seeing as the team was playing without some of its players, Hairston said he stuck to penetrating to the hole and dishing out because his shots wouldnt fall. </p> 
  <p>Overall Hairston has performed well in all the tournaments he has played in during his junior year of high school so much so that the 63 guard from Salem, Virginia has caught the eyes of some Division 1 colleges.</p> 
  <p><strong>UNC-Ashville</strong> offered, <strong>Providence, Colorado, South Carolina, Clemson</strong> and <strong>George Mason</strong> are all in contact, said Hairston.</p> 
  <p>As to which schools are the frontrunners Hairston said, My favorites are probably Colorado, South Carolina and <UNC>-Ashville. Mainly because of the relationships those coaches have worked to get me.</p> 
  <p>Hairston says hes hoping to choose and sign with a school during the first period of signing this upcoming season. He has visited UNC-Ashville already and will be heading down to Clemson Universities practice facilities to practice with his new high school team, Christ School, an all-boys college preparatory school.</p> 
  <p>With a big summer ahead of Hairston he says hes always trying to be in a gym or a park working on his game. </p> 
  <p>When discussing which part of his game he was trying to work on, Hairston said hes working a little bit of everything.</p> 
  <p>Defense, stamina, Im always looking to better my handles but lots of stuff without the ball.</p> 
  <p>Hairston and East Coast Fusion will be competing in various tournaments in the month of July including the Hoop Group<a href="http://hoopgroup.com/hoopgroup/team-tournaments/grassroots-events/west-virginia-hoop-group-jam-fest.php"> Jam Fest in West Virginia</a>. </p> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>