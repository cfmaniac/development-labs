<article id="post-38739" class="post-38739 post type-post status-publish format-standard has-post-thumbnail hentry category-elite-camp-updates tag-hoop-group-elite-camp"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p><a href="https://hoopgroup.com/wp-content/uploads/2013/07/DSC09075.jpg"><img class="aligncenter size-full wp-image-38552" alt="SONY DSC" src="https://hoopgroup.com/wp-content/uploads/2013/07/DSC09075-e1373595502682.jpg" width="600" height="365"></a>Sweatshops have been a daily bonus to the Elite camp experience over the course of the camps existence. The time spent gives campers an opportunity to separate from the rest of the pack, running drills when theyre not playing games.</p> 
  <p>This morning, campers were given an added bonus to their sweatshop regimen as former Siena University head coach <strong>Mitch Buonaguro</strong> made an appearance to run Thursdays slate of sweatshops.</p> 
  <p>I try to have them do things that will help them become a better player, Buonaguro said.&nbsp;Fundamental drills like passing, shooting, dribblingI try to put a real emphasis on fundamentals at this station.</p> 
  <p>Aside from the directions that he gave during sweatshop sessions, the concept of fundamentals are key elements that coach Buonaguro harps on the most as a head coach.</p> 
  <p>Im big on footwork, Buonaguro said. I think that a lot of people dont emphasize that, but I do. Basketball is a game of starts and stops so I try to incorporate a lot of drills that implement that concept as well.</p> 
  <p>Being able to bring in a renowned figure that has over 38 years of coaching experience was a major luxury that campers were able to take advantage on the third day of camp. The former Siena head coach has a vast basketball resume which includes head coaching stops at Fairfield University for six seasons, as well as time spent as an assistant to Boston College, Texas A&amp;M and Cleveland State.</p> 
  <p>Having made a number of destinations during his coaching career, Buonaguro recalls his fondest moment when he served as an assistant to Villanovas 1985 National Championship team.</p> 
  <p>I have had a lot of great coaching moments. Obviously the 85 Championship over Georgetown is up there. I was an assistant then and that was probably the greatest game ever played by a team in a final. It was a great moment and Ill never forget that,</p> 
  <p>Buonaguro also listed the years that he served as the lead assistant at Siena, when the program went to three straight MAAC Championships which followed with consecutive NCAA tournament berths.</p> 
  <p>The former Siena coach had only good things to say when asked about the program after the two sides parted ways in March.</p> 
  <p>I love Siena. I think I left there with great memories and great feelings about the school. Sometimes things dont work out but you just have to pick up yourself and move on. It was a great eight years there and I wish them the best of luck in the future.</p> 
  <p>Buonaguro was delighted to have the chance to attend Hoop Groups Elite camp and share some of his basketball insight with students of the game. As far as coaching goes, the 60-year old said that he really enjoys where he and his wife live in Saratoga, New York. Therefore close proximity to his home is a major factor as far as where he would like to make his next coaching stop</p> 
  <p>I like coaching and being around kids, so if I can find the right situation, Buonaguro said. My wife is really acclimated with the area, so it would be a family decision.</p> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>