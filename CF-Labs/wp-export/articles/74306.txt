<article id="post-74306" class="post-74306 post type-post status-publish format-standard has-post-thumbnail hentry category-player-feature category-tournaments"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p><img class="aligncenter size-full wp-image-74315" src="https://hoopgroup.com/wp-content/uploads/2016/04/IMG_9929-1.jpg" alt="Cormac Ryan" width="600" height="250"></p> 
  <p>From the cabins and the blacktop courts of Hoop Group Skills Camp in the Poconos, to a starring role at the <a href="http://hoopgroup.com/team-tournaments/grassroots-events/hoop-group-jam-fest-basketball-tournaments/spring-jam-fest/" target="_blank">Hoop Group Spring JamFest</a>, <strong>Cormac Ryan </strong>has almost achieved his goal of reaching the highest level of collegiate basketball, but he knows there is plenty of work to be done.</p> 
  <p><em>(This AAU season and going forward) I really need to work on my strength and conditioning so I am ready to compete at the highest level,</em> said Ryan (Milton Academy, MA | 2018).</p> 
  <p>On the court he is already showcasing his basketball talents, this weekend at Spooky Nook in Manheim, PA, for the crowds that came out for the first weekend of the Division I live period. One member of the crowd was University of Virginia head coach <strong>Tony Bennett</strong>.</p> 
  <p><em>I am honored that the head coach of Virginia came out to watch,</em> Ryan said, <em>playing up (with Middlesex Magics 17U team) is really a good opportunity.</em></p> 
  <img data-caption="Tony Bennett was one of the many coaches to watch Cormac Ryan this weekend" class="aligncenter wp-image-74313" src="https://hoopgroup.com/wp-content/uploads/2016/04/IMG_9945.jpg" alt="Tony Bennett" width="376" height="250">
  <p class="blog2html_p ">Tony Bennett was one of the many coaches to watch Cormac Ryan this weekend</p> 
  <p><em>Ive attended (Hoop Group) Skills Camp and the camps at Albright (Hoop Group Elite), </em>Ryan continued.<em> This is great organization that is run very well and affords you great exposure.</em></p> 
  <p>If Bennett was like everyone else in attendance he was exposed to not only Ryans sweet shooting stroke but also his seemingly polished basketball IQ.</p> 
  <p>Ryan does an excellent job moving with and without the ball and knowing when not to force his shot and distribute the ball. He can seemingly create space on the floor with ease and is adept at finding open teammates when opponents try to double-team him. In the game watched by Bennett, Ryan dished out 11 assists.</p> 
  <p><em>I try to take enough shots so I stay sharp but I know I need to move the ball and keep my teammates involved,</em> said Ryan, who was also being looked at from members of the Boston College staff and, what appeared to be, ever other coaching staff in the building.</p> 
  <p><em>He is going to be a high-level point guard. Hes got it and hes shifty on the court. When you add that to his skills that is what separates him from everyone else,</em> said Columbia assistant coach Jared Czech.</p> 
  <p>In addition to Virginia and Boston College, there have been discussions with Michigan, Notre Dame and Stanford as well Ivy League schools Harvard and Yale. Northwestern is supposed to come to the final day of the Spring JamFest to see him.</p> 
  <p>To use a baseball term, hes a five-tool player, Middlesex Magic coach <strong>Michael Crotty</strong> said. <em>He can move quick laterally, he has a great stroke off the pass, he goes to the glass hard for a guard and, like everyone saw here last night, he can pass the ball and set up his teammates. Hes one of the nicest kids off the court and one of the most competitive ones on it.</em></p> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>