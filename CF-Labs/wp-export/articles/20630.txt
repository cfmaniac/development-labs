<article id="post-20630" class="post-20630 post type-post status-publish format-standard has-post-thumbnail hentry category-tournament-recap tag-hgjamfest tag-philly"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p><a href="http://www.hoopgroup.com/wp-content/uploads/2012/05/Isaiah-Whitehead.jpg"><img src="http://www.hoopgroup.com/wp-content/uploads/2012/05/Isaiah-Whitehead.jpg" alt="Isaiah Whitehead at Under Armour Hoop Group Jam Fest" title="Isaiah Whitehead at Under Armour Hoop Group Jam Fest" width="598" height="250" class="alignleft size-full wp-image-20638"></a><br> <em>Photo by: Kelly Kline / Under Armour</em></p> 
  <p>Hoop Group prides itself on organizing some of the top basketball events in the country, offering unparalleled exposure, competition, and organization. </p> 
  <p>Some of the top players at the Under Armour Hoop Group Jam Fest in Philadelphia, PA spoke about their experience, and the benefits of participating in the event. </p> 
  <p><strong>Isaiah Whitehead, Juice All-Stars (NY), Lincoln HS, 2014:</strong> </p> 
  <blockquote>
   <p>Its great. I get exposure with all the college coaches <at Hoop Group>. Its a very competitive event, and its going to help me out going forward.</p>
  </blockquote> 
  <p><strong>Kendall Stephens, Illinois Wolves (IL), St. Charles East HS, 2013, Purdue Commit:</strong> </p> 
  <blockquote>
   <p>It was nice for us to come over to East Coast to see what the competition is like, instead of playing the same teams in the Midwest. Its great competitiontheyre physical, they get after it. Its a great stepping stone to see where we are (as a team). </p>
  </blockquote> 
  <p><strong>Dwayne Morgan, UA Bmores Finest (MD), City College HS, 2014: </strong></p> 
  <blockquote>
   <p>It feels like I have to come out and play hard. I dont want to let these coaches down. Its competitive here, and I think it gets you ready for the next level.</p>
  </blockquote> 
  <p><strong>J.C. Show, JB Hoops (PA), Abington Heights HS, 2014: </strong></p> 
  <blockquote>
   <p>Its a great opportunity, weve really been looking forward to it. Great competition, thats what we love about coming and playing in these big tournaments.</p>
  </blockquote> 
  <p><strong>Nick Emery, Utah Reign (UT), Lone Peak HS, 2013, BYU Commit: </strong></p> 
  <blockquote>
   <p>Its crazy coming out from Utah. Its more up-and down, more physicala lot harder. And it helps us grow as a team.</p>
  </blockquote> 
  <p><strong>Travis Jorgenson, KC Run GMC (MO), 2013, Missouri Commit:</strong> </p> 
  <blockquote>
   <p>There are a lot of new teams, and its a good way to test ourselves <Hoop Group> Jam Fest gets us ready (for college basketball), because were playing better players then we can see in Missouri.</p>
  </blockquote> 
  <p><strong>Aquille Carr, UA Bmores Finest (MD), Patterson HS, 2013, Seton Hall Commit: </strong></p> 
  <blockquote>
   <p>Its great. There are a lot of great teams out here trying to show their talent and get some looks. Everybody wants to get out there and its really fun.</p>
  </blockquote> 
  <p><strong>Melo Trimble, D.C. Assault (D.C.), Bishop OConnell HS, 2014:</strong> </p> 
  <blockquote>
   <p>Its a great experience. We have the Under Armour connection, with D.C. Assault, so its something new for us, and its been greatIts good competition for us out here. </p>
  </blockquote> 
  <p><strong>Shaquile Carr, Las Vegas Prospects (NV), Canyon Springs HS, 2013:</strong> </p> 
  <blockquote>
   <p>In this <Hoop Group> Jam Fest, Im really liking all these players and new Under Armour stuff. Its good to come see new players and new teams around.</p>
  </blockquote> 
  <p><strong>Jermaine Lawrence, New Rens (NY), Pope John XXIII HS, 2013:</strong> </p> 
  <blockquote>
   <p>It gives me a lot of competition against the best players in the country, really. So this is a great event to be at to really show your game.</p>
  </blockquote> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>