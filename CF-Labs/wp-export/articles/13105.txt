<article id="post-13105" class="post-13105 post type-post status-publish format-standard has-post-thumbnail hentry category-alumni-updates tag-hoop-group-alumni tag-kyle-anderson tag-ucla"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p><iframe width="640" height="360" src="http://www.youtube.com/embed/saAMxshe48Y" frameborder="0" allowfullscreen></iframe></p> 
  <p>Kyle Anderson is the most unique player in the country. He might be the only recruit to have cake and candles at his college announcement. Anderson made his decision on his 18th birthday telling the country that he would be attending UCLA. </p> 
  <p>Anderson has been been a staple at Hoop Group camps and tournaments since he was 10 years old. He ended the July live recruiting period to play his final AAU game at Hoop Group End of Summer Classic. Hoop Group always does a good job every year. They always do a good job with the high school team camp, Hoop Group Elite camps, Hoop Group is great. Anderson said after winning MVP after triple overtime win at End of Summer Classic. </p> 
  <p>Everyone at Hoop Group would like to extend their best wishes to Kyle Anderson on continuing his education and basketball career. </p> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>