<article id="post-30816" class="post-30816 post type-post status-publish format-standard has-post-thumbnail hentry category-tournament-recap"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p><a href="https://hoopgroup.com/wp-content/uploads/2013/04/KJK_5913-copy.jpg"><img src="https://hoopgroup.com/wp-content/uploads/2013/04/KJK_5913-copy.jpg" alt="2013 UA Pitt Jam Fest" width="600" height="500" class="alignleft size-full wp-image-30876"></a><br> Sunday wrapped up what was a fantastic showing at the Pitt Jam Fest, with Sports U IZod taking the 17U Platinum crown after besting DC Assault 48-45. Take a look at some of the observations from the last day in Pittsburgh:</p> 
  <p><strong>Suntil Steps Up</strong></p> 
  <p>In a matter of ten seconds, Sports U IZods Hakim Suntil completely shifted the momentum of the championship game.</p> 
  <p>With less than 30 seconds left in the game and Sports U trailing DC Assault by three points, Suntil hit a three-pointer to tie the game,</p> 
  <p>We had a pick-and-roll situation, thats a play we run all the time, Suntil said. I knew I could get a shot off. I came off the pick and <the defender> came under, so I had the space. I just set my feet up and knocked it down.</p> 
  <p>Then, with DC Assault guard and Maryland commit Romelo Trimble bringing the ball up court, Suntil struck again, stealing the ball and taking a two-point lead with 18.7 seconds left in the game.</p> 
  <p>I wasnt worried about the fouls, Suntil said of the game-changing steal. I saw him spin one time, I knew I could get it so I waited until I knew he would show the ball and took it right there.</p> 
  <p>After the game, Suntil was more than happy to give credit to the rest of the Sports U IZod squad.</p> 
  <p>It feels good, but I wouldnt be here without my teammates. I didnt score every point, so Ive got to give it to my teammates.</p> 
  <p><strong>Jared Nickens Puts Stamp on Pitt Jam Fest</strong></p> 
  <p>Youd have a tough time finding someone who topped Nickens performance over the past few days.</p> 
  <p>The 2014 guard always seemed to find a way to find the bottom of the bucket every single game, including a last second three-pointer against Team Loaded (VA) that broke a 51-51 tie.</p> 
  <p>With offers already in hand from Oregon State, Temple, Providence, St. Josephs and Penn State, his play in front of the massive amount of scouts on hand will almost assuredly earn him some more college options.</p> 
  <p><strong>Romelo Trimble Impresses</strong></p> 
  <p>Despite ending up just short of the championship, Trimble had a great weekend in Pittsburgh. The Maryland commit showcased just how much of an all-around talent. When DC Assault needed a go-to scorer, Trimble was there with a 20-point performance.</p> 
  <p>When they needed someone to set them up with an easy shot, Trimble was there with the beautiful pass. Even with his recruitment closed, itll be interesting just to see how far he can take his game in the next year.</p> 
  <p><strong>Was it a Two or a Three?&nbsp;</strong></p> 
  <p>With DC Assault leading by three, Team Charlottes Antonio Woods hit a deep shot andthe game ended in a one-point loss for Charlotte.</p> 
  <p>The shot was ruled a two-pointer after the referees said that Woods foot was on the line, which led to coaches and players pleading their case and fans wondering if the refs had indeed made the right call.</p> 
  <p>From my vantage point (around midcourt, 10 rows up), it looked as though the refs had made the correct call, but it certainly was very close and could have gone either way, depending on where the ref was placed when the shot when up. Either way, DC Assault was the victor and earned a berth in the 17u Platinum tournament.</p> 
  <p><strong>Atlanta Xpress has plenty of D-I talent</strong></p> 
  <p>Despite being bounced in the 17u Platinum Final Four by eventual champions Sports U IZod, Atlanta Xpress showed off some clear-cut Division I talent every time they took the floor.</p> 
  <p>Mike Tucker Jr., who I briefly touched on in the previous recap, continued to steal the spotlight with smooth mid-range jumpers and ferocious dunks. Things certainly havent changed in my mind: Bradley is getting one hell of a player in 2014.</p> 
  <p>Not to be outdone, 7-foot-1 center Trayvon Reed has been attracting plenty of national attention, garnering plenty of attention from schools in the south, including Miami, Georgia and Florida State, among others. While his offensive game needs some fine tweaking, the size is certainly a factor in his recruitment. After all, you cant teach height.</p> 
  <p>As it stands now, Xpress teammate Jabari McGhee could end up joining Reed in the south. The 6-foot-5 junior has offers from Ole Miss, South Carolina and Auburn, with plenty of others showing interest. McGhee is another player to have a very productive tournament and looks to grab many more scholarship offers before he ultimately has to come to a decision.</p> 
  <p><em><a href="https://twitter.com/AndrewKoob">@AndrewKoob</a>&nbsp;is a student at Temple University and staff writer for&nbsp;<a href="CityofBasketballLove.com">CityofBasketballLove.com</a>, covering Philadelphia-area high school &amp; collegiate basketball.</em></p> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>