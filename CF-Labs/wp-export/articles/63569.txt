<article id="post-63569" class="post-63569 post type-post status-publish format-standard has-post-thumbnail hentry category-player-feature tag-albright tag-future-all-american-camp tag-hoop-group tag-lamar-stevens tag-mikeal-jones tag-nazeer-bostick tag-paul-newman tag-roman-catholic tag-teddy-bailey tag-tony-carr"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p><img class="alignnone size-full wp-image-63577 aligncenter" src="https://hoopgroup.com/wp-content/uploads/2015/08/Mikeal-Jones-3.jpg" alt="Mikeal Jones 3" width="600" height="400"></p> 
  <p><strong>Teddy Bailey </strong>(<a href="http://twitter.com/theteddybailey" target="_blank">@TheTeddyBailey</a>)<br> </p> 
  <p>If <strong>Mikeal Jones</strong> first tryout game at Hoop Groups Future All-American camp is any indication, Roman Catholic, and the Philadelphia area, may have found its next versatile wing.</p> 
  <p>The 6-foot-6 rising freshman is one of those players that just doesnt look like they are months removed from middle school. Jones features a build that is comparable to an upperclassman, which is easily the first thing that catches the eye.</p> 
  <p>Theres more to the 2019 wing than his physique, obviously. Jones put on a show in the initial tryout games by knocking down jumpers from various points around the court. His build, combined with his speed, allows him to be a cumbersome task for defenders in the paint.</p> 
  <p>Right now, I can play the 3 or the 4, Jones said. I like the idea of being able to play 1 through 5. I see myself in four years, developing and becoming more of a scorer and rebounder.</p> 
  <p>Pressure is a key component of Jones game; mostly because the athletic guard is widely viewed as yet another future star to come through the Philadelphia basketball area. He picked the right school to play for in terms of play; the Cahillites feature Penn State commit&nbsp;<strong>Nazeer Bostick</strong>, as well as high-major guard <b>Tony Carr</b>, wing <strong>Lamar Stevens</strong> and mid-major forward <strong>Paul Newman</strong>.</p> 
  <p>Im willing to take on the challenge, Jones said. Theres a lot of people that are capable of doing it, but if Philly is dependent on me to do it, Im willing to take it on.</p> 
  <p>Is Jones feeling the heat? This summer definitely helped. After his first summer playing on the national AAU circuit, the incoming freshman feels a little more seasoned.</p> 
  <p>Ive played in Atlanta and Las Vegas, two big-time tournaments, he said. I know that Ive gotten better over the past three months. It was a little intimidating and definitely a lot more pressure, playing in front of big crowds and that nature.</p> 
  <p>As far as Hoop Groups Future All-American camp at Albright College goes, Jones is considered to be one of the top 2019s in the camp. That being said, the aspiring high-major player recognizes the abundance of talent here this week.</p> 
  <p>Its very competitive, Jones said of FAA. And definitely not as easy as I thought it was going to be. Theres player s from all over the country. Im just trying to get better, day-by-day.</p> 
  <p>It will surely interesting to see how much Jones contributes for the defending PCL, district and state champions in the winter. It helps that hes already familiar with the upperclassmen leaders of the squad.</p> 
  <p>Were all pretty close, Jones said of Carr, Bostick and Stevens. I have great relationships with the entire team and I look forward to helping them out next year. Basically Im planning on just bringing the energy, playing as hard as I can and trying to help out the team any way possible.</p> 
  <p>I want to see myself at a high-major D1 school, Jones continued. I want to be able to set examples for the younger kids coming up.</p> 
  <p>Hes definitely on his way.</p> 
  <p></p> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>