<article id="post-77019" class="post-77019 post type-post status-publish format-standard has-post-thumbnail hentry category-elite-blog category-elite-camp-updates tag-albright-college tag-delonnie-hunt tag-devin-jensen tag-hoop-group tag-hoop-group-elite-1 tag-hoop-group-elite-camp tag-jahsim-floyd tag-jamaal-banks tag-kendrick-gray tag-sandro-mamukelashvili tag-talek-williams"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p><span style="font-weight: 400;">Elite 1 in Reading resumed with the second full day of action on Thursday. With the live period officially underway, a bevy of division 1 coaches made the trip to Albright College to take in the action. Below are some of the standouts from a full day of hoops.</span></p> 
  <p><b>F Jahsim Floyd | 2017 | Sayreville</b></p> 
  <p><b>Jahsim Floyd</b><span style="font-weight: 400;"> is an intriguing post prospect. The 6-foot-7 rising senior consistently made plays in the paint area on both ends of the floor. Floyd is a very good athlete who gets off his feet quick to contest shots. Hes a very effective shot blocker given his length in conjunction with his timing and anticipation ability.</span></p> 
  <p><b>PG Delonnie Hunt | 2020 | St. Johns</b></p> 
  <p><span style="font-weight: 400;">Left-handed rising freshman </span><b>Delonnie Hunt</b><span style="font-weight: 400;"> is an up and coming name from the DMV to get familiar with. Hunt is a knock-down three point shooter who can handle pressure and distribute at the lead guard spot. He has a high basketball IQ and is a solid on-ball defender as a result of his work rate, mobility, and instincts. </span></p> 
  <p><b>G/F Jamaal Banks | 2020 | Bishop OConnell</b></p> 
  <p><span style="font-weight: 400;">Another up and coming prospect from the DMV region, Jamaal Banks has a chance to be very good. At 6-foot-3, Banks is long and extremely athletic. He plays with a consistently high motor and gets off the ground quick. As he continues to grow into his body and develop his skill set, Banks could blossom into a big time player.</span></p> 
  <p><b>G Talek Williams | 2017 | Allen </b></p> 
  <p><span style="font-weight: 400;">Two things that immediately stand out about </span><b>Talek Williams</b><span style="font-weight: 400;"> are his athleticism and effort. The 6-foot-3 guard is a high flyer who plays above the rim on both ends of the court. Williams plays with passion and is always engaged defensively. He threw down a series of forceful dunks in the open court.</span></p> 
  <p><b>SG Devin Jensen | 2017 | Manasquan </b></p> 
  <p><span style="font-weight: 400;">At 6-foot-4,</span><b> Devin Jensen</b><span style="font-weight: 400;"> blends size with a prolific shooting touch from long range. Jensen does a great job moving without the ball to get open, and has a very quick release. He got hot and connected on a handfuls of jumpers from distance. </span></p> 
  <p><b>F Kendrick Gray | 2017 | Vermont Academy</b></p> 
  <p><b>Kendrick Gray</b><span style="font-weight: 400;"> headed into this week with three offers and interest from a number of high major programs. The 6-foot-7 forward is a big time athlete with a great motor. He also has the ability to absorb and finish through contact around the basket. Theres a very good chance his offer sheet could expand given his consistent play here in Reading. </span></p> 
  <p><b>F Sandro Mamukelashvili | 2017 | Montverde Academy</b></p> 
  <p>One of the more intriguing prospects at Elite 1, theres a lot to like about <strong>Sandro Mamukelashvili</strong>. The left-handed 6-foot-11 forward is mobile and very skilled. He can shoot the 3, and has the ability to consistently find open teammates. He showcases a good feel for the game along with impressive length and touch at his position.</p> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>