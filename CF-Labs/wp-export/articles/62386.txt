<article id="post-62386" class="post-62386 post type-post status-publish format-standard has-post-thumbnail hentry category-alumni-updates"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <img data-caption="Karl-Anthony Towns, right, poses for a photo with NBA Commissioner Adam Silver after being announced as the top pick during the NBA basketball draft by the Minnesota Timberwolves, Thursday, June 25, 2015, in New York. (AP Photo/Kathy Willens)" class="aligncenter  wp-image-62405" src="https://hoopgroup.com/wp-content/uploads/2015/06/karl-anthony-towns-nba-draft-header.jpg" alt="Karl-Anthony Towns, right, poses for a photo with NBA Commissioner Adam Silver after being announced as the top pick during the NBA basketball draft by the Minnesota Timberwolves, Thursday, June 25, 2015, in New York. (AP Photo/Kathy Willens)" width="601" height="251">
  <p class="blog2html_p ">Karl-Anthony Towns, right, poses for a photo with NBA Commissioner Adam Silver after being announced as the top pick during the NBA basketball draft by the Minnesota Timberwolves, Thursday, June 25, 2015, in New York. (AP Photo/Kathy Willens)</p> 
  <p><strong>Neptune, NJ</strong> Last night 60 young men heard then names called over the microphone in the Barclays Center in downtown Brooklyn, NY. With each announcement came an overwhelming feeling or relief and accomplishment. Dreams were made, as were new goals. Among those 60, we are proud to claim 17 as Hoop Group alumni. See the full list of players drafted below:</p> 
  <table id="tablepress-694" class="tablepress tablepress-id-694"> 
   <thead> 
    <tr class="row-1 odd"> 
     <th class="column-1">&nbsp;</th>
     <th class="column-2">Player</th>
     <th class="column-3">College</th>
     <th class="column-4">NBA Team</th> 
    </tr> 
   </thead> 
   <tbody> 
    <tr class="row-2 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2015/06/IMG_1156-100x75.jpg" alt="" width="100" height="75" class="aligncenter size-thumbnail wp-image-62388"></td>
     <td class="column-2">Karl Anthony Towns</td>
     <td class="column-3">Kentucky</td>
     <td class="column-4">Minnesota Timberwolves</td> 
    </tr> 
    <tr class="row-3 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2015/06/IMG_1158-100x75.jpg" alt="" width="100" height="75" class="aligncenter size-thumbnail wp-image-62389"></td>
     <td class="column-2">DAngelo Russell</td>
     <td class="column-3">Ohio State</td>
     <td class="column-4">Los Angeles Lakers</td> 
    </tr> 
    <tr class="row-4 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2015/06/IMG_1159-100x75.png" alt="" width="100" height="75" class="aligncenter size-thumbnail wp-image-62390"></td>
     <td class="column-2">Terry Rozier</td>
     <td class="column-3">Louisvile</td>
     <td class="column-4">Boston Celtics</td> 
    </tr> 
    <tr class="row-5 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2015/06/IMG_1162-100x75.jpg" alt="" width="100" height="75" class="aligncenter size-thumbnail wp-image-62391"></td>
     <td class="column-2">Jerian Grant</td>
     <td class="column-3">Notre Dame</td>
     <td class="column-4">NY Knicks</td> 
    </tr> 
    <tr class="row-6 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2015/06/IMG_1164-100x75.jpg" alt="" width="100" height="75" class="aligncenter size-thumbnail wp-image-62392"></td>
     <td class="column-2">Justin Anderson</td>
     <td class="column-3">Virginia</td>
     <td class="column-4">Dallas Mavericks</td> 
    </tr> 
    <tr class="row-7 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2015/06/IMG_1166-100x75.jpg" alt="" width="100" height="75" class="aligncenter size-thumbnail wp-image-62393"></td>
     <td class="column-2">Rondae Hollis Jefferrson</td>
     <td class="column-3">Arizona</td>
     <td class="column-4">Portland Trailblazers</td> 
    </tr> 
    <tr class="row-8 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2015/06/IMG_1168-100x75.jpg" alt="" width="100" height="75" class="aligncenter size-thumbnail wp-image-62394"></td>
     <td class="column-2">Larry Nance JR</td>
     <td class="column-3">Wyoming</td>
     <td class="column-4">Los Angeles Lakers</td> 
    </tr> 
    <tr class="row-9 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2015/06/IMG_1184-100x75.jpg" alt="" width="100" height="75" class="aligncenter size-thumbnail wp-image-62401"></td>
     <td class="column-2">Delon Wright</td>
     <td class="column-3">Utah</td>
     <td class="column-4">Toronto Raptors</td> 
    </tr> 
    <tr class="row-10 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2015/06/IMG_1185-100x75.jpg" alt="" width="100" height="75" class="aligncenter size-thumbnail wp-image-62402"></td>
     <td class="column-2">Chris McCullough</td>
     <td class="column-3">Syracuse</td>
     <td class="column-4">Brooklyn Nets</td> 
    </tr> 
    <tr class="row-11 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2015/06/IMG_1186-100x75.jpg" alt="" width="100" height="75" class="aligncenter size-thumbnail wp-image-62403"></td>
     <td class="column-2">Olivier Hanlan</td>
     <td class="column-3">Boston College</td>
     <td class="column-4">Utah Jazz</td> 
    </tr> 
    <tr class="row-12 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2015/06/IMG_1170-100x75.jpg" alt="" width="100" height="75" class="aligncenter size-thumbnail wp-image-62395"></td>
     <td class="column-2">Montrezl Harrell</td>
     <td class="column-3">Louisvile</td>
     <td class="column-4">Houston Rockets</td> 
    </tr> 
    <tr class="row-13 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2015/06/IMG_1172-100x75.jpg" alt="" width="100" height="75" class="aligncenter size-thumbnail wp-image-62396"></td>
     <td class="column-2">Rakeem Christmas</td>
     <td class="column-3">Syracuse</td>
     <td class="column-4">Minnesota Timberwolves</td> 
    </tr> 
    <tr class="row-14 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2015/06/IMG_1174-100x75.jpg" alt="" width="100" height="75" class="aligncenter size-thumbnail wp-image-62397"></td>
     <td class="column-2">Darrun Hilliard</td>
     <td class="column-3">Villanova</td>
     <td class="column-4">Detroit Pistons</td> 
    </tr> 
    <tr class="row-15 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2015/06/IMG_1176-100x75.jpg" alt="" width="100" height="75" class="aligncenter size-thumbnail wp-image-62398"></td>
     <td class="column-2">Andrew Harrison</td>
     <td class="column-3">Kentucky</td>
     <td class="column-4">Phoenix Suns</td> 
    </tr> 
    <tr class="row-16 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2015/06/IMG_1178-100x75.jpg" alt="" width="100" height="75" class="aligncenter size-thumbnail wp-image-62399"></td>
     <td class="column-2">Dakari Johnson</td>
     <td class="column-3">Kentucky</td>
     <td class="column-4">Oklahoma City Thunder</td> 
    </tr> 
    <tr class="row-17 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2015/06/IMG_1179-100x75.png" alt="" width="100" height="75" class="aligncenter size-thumbnail wp-image-62400"></td>
     <td class="column-2">Satnam Singh</td>
     <td class="column-3">India</td>
     <td class="column-4">Dallas Mavericks</td> 
    </tr> 
    <tr class="row-18 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2015/06/IMG_1187-100x75.jpg" alt="" width="100" height="75" class="aligncenter size-thumbnail wp-image-62404"></td>
     <td class="column-2">Brandon Dawson</td>
     <td class="column-3">Michigan State</td>
     <td class="column-4">New Orleans Pelicans</td> 
    </tr> 
   </tbody> 
  </table> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>