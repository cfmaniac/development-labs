<article id="post-44670" class="post-44670 post type-post status-publish format-standard has-post-thumbnail hentry category-tournament-results"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <h3><strong>Varsity Champions: Gym Ratz</strong></h3> 
  <p><a href="https://hoopgroup.com/wp-content/uploads/2013/11/IMG_2516-e1383592405313.jpg"><img class="alignnone size-full wp-image-44674" alt="Gym Ratz" src="https://hoopgroup.com/wp-content/uploads/2013/11/IMG_2516-e1383592405313.jpg" width="600" height="449"></a></p> 
  <h3></h3> 
  <h3><strong>9th Grade Champions: Quincy Douby Elite</strong></h3> 
  <p><a href="https://hoopgroup.com/wp-content/uploads/2013/11/IMG_25181-e1383592676955.jpg"><img class="alignnone size-full wp-image-44677" alt="Quincy Douby Elite" src="https://hoopgroup.com/wp-content/uploads/2013/11/IMG_25181-e1383592676955.jpg" width="600" height="351"></a></p> 
  <h3></h3> 
  <h3><strong>8th Grade Champions: Hilltopper Heat</strong></h3> 
  <p><a href="https://hoopgroup.com/wp-content/uploads/2013/11/IMG_2512-e1383592215965.jpg"><img class="alignnone size-full wp-image-44672" alt="Hilltopper Heat" src="https://hoopgroup.com/wp-content/uploads/2013/11/IMG_2512-e1383592215965.jpg" width="600" height="400"></a></p> 
  <h3></h3> 
  <h3><strong>7th Grade Champions: CIA Bounce</strong></h3> 
  <p><a href="https://hoopgroup.com/wp-content/uploads/2013/11/IMG_2510-e1383592162793.jpg"><img class="alignnone size-full wp-image-44671" alt="CIA Bounce" src="https://hoopgroup.com/wp-content/uploads/2013/11/IMG_2510-e1383592162793.jpg" width="600" height="362"></a></p> 
  <h3></h3> 
  <h3><strong>6th Grade Champions: Hoop Dreamz</strong></h3> 
  <p><a href="https://hoopgroup.com/wp-content/uploads/2013/11/IMG_2514-e1383592305605.jpg"><img class="alignnone size-full wp-image-44673" alt="Hoopdreamz" src="https://hoopgroup.com/wp-content/uploads/2013/11/IMG_2514-e1383592305605.jpg" width="600" height="359"></a></p> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>