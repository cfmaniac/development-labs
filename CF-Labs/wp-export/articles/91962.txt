<article id="post-91962" class="post-91962 post type-post status-publish format-standard hentry category-headquarters-blog"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <h2 style="text-align: center;">2016 Team Champions</h2> 
  <hr> 
  <table style="height: 56px;" width="570"> 
   <tbody> 
    <tr> 
     <td style="width: 277.4184875488281px; text-align: center;"><a href="http://hoopgroup.com/team-tournaments/grassroots-events/boys-showdowns/summer-showdown-phase-ii/attachment/img_0582/" rel="attachment wp-att-76768"><img class="size-medium wp-image-76768" src="https://hoopgroup.com/wp-content/uploads/2012/03/IMG_0582-300x300.jpg" alt="14U Champs - Hilltopper Heat" width="300" height="300"></a>14U Champs  Hilltopper Heat</td> 
     <td style="width: 277.4184875488281px; text-align: center;"><a href="http://hoopgroup.com/team-tournaments/grassroots-events/boys-showdowns/summer-showdown-phase-ii/attachment/img_0583/" rel="attachment wp-att-76769"><img class="size-medium wp-image-76769" src="https://hoopgroup.com/wp-content/uploads/2012/03/IMG_0583-300x300.jpg" alt="15U Champs - East Coast Power" width="300" height="300"></a>15U Champs  East Coast Power</td> 
    </tr> 
    <tr> 
     <td style="width: 277.4184875488281px; text-align: center;"><a href="http://hoopgroup.com/team-tournaments/grassroots-events/boys-showdowns/summer-showdown-phase-ii/attachment/img_0584/" rel="attachment wp-att-76770"><img class="size-medium wp-image-76770" src="https://hoopgroup.com/wp-content/uploads/2012/03/IMG_0584-300x300.jpg" alt="16U Champs - Philly Pride" width="300" height="300"></a>16U Champs  Philly Pride</td> 
     <td style="width: 277.4184875488281px; text-align: center;"><a href="http://hoopgroup.com/team-tournaments/grassroots-events/boys-showdowns/summer-showdown-phase-ii/attachment/img_0585/" rel="attachment wp-att-76771"><img class="size-medium wp-image-76771" src="https://hoopgroup.com/wp-content/uploads/2012/03/IMG_0585-296x300.jpg" alt="17U Champs - Jersey Force" width="296" height="300"></a>17U Champs  Jersey Force</td> 
    </tr> 
   </tbody> 
  </table> 
  <h3 style="text-align: center;">2016 Most Valuable Players</h3> 
  <hr> 
  <table style="height: 43px;" width="570"> 
   <tbody> 
    <tr> 
     <td style="width: 277.4184875488281px; text-align: center;"><a href="http://hoopgroup.com/team-tournaments/grassroots-events/boys-showdowns/summer-showdown-phase-ii/attachment/img_0590/" rel="attachment wp-att-76774"><img class="size-medium wp-image-76774" src="https://hoopgroup.com/wp-content/uploads/2012/03/IMG_0590-300x300.jpg" alt="14U MVP - Matthew Tracey" width="300" height="300"></a>14U MVP  Matthew Tracey</td> 
     <td style="width: 277.4184875488281px;"><a href="http://hoopgroup.com/team-tournaments/grassroots-events/boys-showdowns/summer-showdown-phase-ii/attachment/img_0591/" rel="attachment wp-att-76775"><img class="size-medium wp-image-76775" src="https://hoopgroup.com/wp-content/uploads/2012/03/IMG_0591-300x300.jpg" alt="15U MVP - TyGee Leach" width="300" height="300"></a>15U MVP  TyGee Leach</td> 
    </tr> 
    <tr> 
     <td style="width: 277.4184875488281px;"><a href="http://hoopgroup.com/team-tournaments/grassroots-events/boys-showdowns/summer-showdown-phase-ii/attachment/img_0593/" rel="attachment wp-att-76776"><img class="size-medium wp-image-76776" src="https://hoopgroup.com/wp-content/uploads/2012/03/IMG_0593-300x300.jpg" alt="16U MVP - Seth Lundy" width="300" height="300"></a>16U MVP  Seth Lundy</td> 
     <td style="width: 277.4184875488281px;"><a href="http://hoopgroup.com/team-tournaments/grassroots-events/boys-showdowns/summer-showdown-phase-ii/attachment/img_0595/" rel="attachment wp-att-76777"><img class="size-medium wp-image-76777" src="https://hoopgroup.com/wp-content/uploads/2012/03/IMG_0595-300x300.jpg" alt="17U MVP - Austin Williams" width="300" height="300"></a>17U MVP  Austin Williams</td> 
    </tr> 
   </tbody> 
  </table> 
  <p><script type="text/javascript">// <!<CDATA< (function (d, s, id) { var js, fjs = d.getElementsByTagName(s)<0>; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.async = true; js.src = "https://basketball.exposureevents.com/scripts/exposure.widgets.min.js"; fjs.parentNode.insertBefore(js, fjs); } })(document, "script", "exp.widgets"); // >>&gt;</script></p> 
  <p>__________________________________________________</p> 
  <p style="text-align: center;"><strong>2015 Team Champions</strong></p> 
  <table style="height: 43px;" width="571"> 
   <tbody> 
    <tr> 
     <td style="width: 277.9076232910156px;"><a href="https://hoopgroup.com/wp-content/uploads/2012/03/81.jpg"><img class="size-medium wp-image-62312" src="https://hoopgroup.com/wp-content/uploads/2012/03/81-300x300.jpg" alt="14U Champs- NJ Playaz" width="300" height="300"></a>14U Champs- NJ Playaz</td> 
     <td style="width: 277.9076232910156px;"><a href="https://hoopgroup.com/wp-content/uploads/2012/03/51.jpg"><img class="size-medium wp-image-62311" src="https://hoopgroup.com/wp-content/uploads/2012/03/51-300x300.jpg" alt="15U Champs - Stamford Peace (CT)" width="300" height="300"></a>15U Champs  Stamford Peace (CT)</td> 
    </tr> 
    <tr> 
     <td style="width: 277.9076232910156px;"><a href="https://hoopgroup.com/wp-content/uploads/2012/03/13.jpg"><img class="size-medium wp-image-62309" src="https://hoopgroup.com/wp-content/uploads/2012/03/13-300x300.jpg" alt="16U Champs - NJ Roadrunners" width="300" height="300"></a>16U Champs  NJ Roadrunners</td> 
     <td style="width: 277.9076232910156px;"><a href="https://hoopgroup.com/wp-content/uploads/2012/03/31.jpg"><img class="size-medium wp-image-62310" src="https://hoopgroup.com/wp-content/uploads/2012/03/31-300x300.jpg" alt="17U Champs - Castle Athletics" width="300" height="300"></a>17U Champs  Castle Athletics</td> 
    </tr> 
   </tbody> 
  </table> 
  <p>&nbsp;</p> 
  <p style="text-align: center;"><strong>2015 Most Valuable Players</strong></p> 
  <hr> 
  <table style="height: 43px;" width="571"> 
   <tbody> 
    <tr> 
     <td style="width: 277.9076232910156px;"><a href="https://hoopgroup.com/wp-content/uploads/2012/03/71.jpg"><img class="size-medium wp-image-62317" src="https://hoopgroup.com/wp-content/uploads/2012/03/71-300x300.jpg" alt="15U MVP - Jonas Harper" width="300" height="300"></a>15U MVP  Jonas Harper</td> 
     <td style="width: 277.9076232910156px;"><a href="https://hoopgroup.com/wp-content/uploads/2012/03/21.jpg"><img class="size-medium wp-image-62315" src="https://hoopgroup.com/wp-content/uploads/2012/03/21-300x300.jpg" alt="16U MVP - Taj Benning" width="300" height="300"></a>16U MVP  Taj Benning</td> 
    </tr> 
    <tr> 
     <td style="width: 277.9076232910156px;"><a href="https://hoopgroup.com/wp-content/uploads/2012/03/41.jpg"><img class="size-medium wp-image-62316" src="https://hoopgroup.com/wp-content/uploads/2012/03/41-300x300.jpg" alt="17U MVP - Mike Laster" width="300" height="300"></a>17U MVP  Mike Laster</td> 
     <td style="width: 277.9076232910156px;"></td> 
    </tr> 
   </tbody> 
  </table> 
  <p>&nbsp;</p> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>