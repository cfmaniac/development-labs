<article id="post-61290" class="post-61290 post type-post status-publish format-standard has-post-thumbnail hentry category-top-100"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p>Roger Williams UniversityThe New England Top 100 took place last Friday night on the campus of Roger Williams University. This top 100 gives us one chance to look at some of the talent up in the New England area. As expected, our last Top 100 of the Spring did not disappoint! Here are the player report cards:</p> 
  <table id="tablepress-691" class="tablepress tablepress-id-691"> 
   <thead> 
    <tr class="row-1 odd"> 
     <th class="column-1">Player</th>
     <th class="column-2">Grade</th>
     <th class="column-3">Evaluation</th> 
    </tr> 
   </thead> 
   <tbody> 
    <tr class="row-2 even"> 
     <td class="column-1">Bradley Hastings</td>
     <td class="column-2">11</td>
     <td class="column-3">PG that plays hard with a great pace. Knows when to hit an open teammate and when to score at the rim.</td> 
    </tr> 
    <tr class="row-3 odd"> 
     <td class="column-1">Max Curran </td>
     <td class="column-2">12</td>
     <td class="column-3">Guard that played hard on both sides of the ball. Moves ball in offense and has a solid jump shot.</td> 
    </tr> 
    <tr class="row-4 even"> 
     <td class="column-1">Jake Midura </td>
     <td class="column-2">11</td>
     <td class="column-3">Guard that knows how to run the floor to score at rim or knock down open shot. </td> 
    </tr> 
    <tr class="row-5 odd"> 
     <td class="column-1">Charles Adebayo</td>
     <td class="column-2">12</td>
     <td class="column-3">Great athlete that plays above the rim, solid inside. Active on ball defender.</td> 
    </tr> 
    <tr class="row-6 even"> 
     <td class="column-1">Kyerstan Casey</td>
     <td class="column-2">10</td>
     <td class="column-3">Guard that moves ball well to find open teammates. Can knock down shots at any time.</td> 
    </tr> 
    <tr class="row-7 odd"> 
     <td class="column-1">Kafani Williams </td>
     <td class="column-2">12</td>
     <td class="column-3">Athletics slasher that can score at the rim and has the ability to knock down the 3.</td> 
    </tr> 
    <tr class="row-8 even"> 
     <td class="column-1">Tyrone Jenkins </td>
     <td class="column-2">10</td>
     <td class="column-3">Tyrone is at he best when he attacking the rim. Finishes well with both hands in traffic. Great motor and very solid IQ.</td> 
    </tr> 
    <tr class="row-9 odd"> 
     <td class="column-1">Zecaree Veiga </td>
     <td class="column-2">11</td>
     <td class="column-3">Very athletic that will continuously give 100 percent effort on the court. Constantly gets the loose ball.</td> 
    </tr> 
    <tr class="row-10 even"> 
     <td class="column-1">Tyrone Perry </td>
     <td class="column-2">10</td>
     <td class="column-3">Tyrone can see the floor extremely well. Loves to attack the rim with his athleticism.</td> 
    </tr> 
    <tr class="row-11 odd"> 
     <td class="column-1">Seamus Keaney </td>
     <td class="column-2">9</td>
     <td class="column-3">Seamus has a very clean understanding of the game. Uses his IQ and jump shot to control the game.</td> 
    </tr> 
    <tr class="row-12 even"> 
     <td class="column-1">Marcus Santos-Silva</td>
     <td class="column-2">11</td>
     <td class="column-3">Marcus can score with a plethora of creative moves in and around the paint. Using his athleticism he gets by defenders.</td> 
    </tr> 
    <tr class="row-13 odd"> 
     <td class="column-1">Oumaru Kante </td>
     <td class="column-2">12</td>
     <td class="column-3">He knows what to do when the ball is in his hand. He attacks the defense and uses creative moves to finish the play.</td> 
    </tr> 
    <tr class="row-14 even"> 
     <td class="column-1">Dante Timbas </td>
     <td class="column-2">10</td>
     <td class="column-3">He gives his all 100 percent of the time and will make the play others wouldn't be willing to make. Has a well-rounded jump shot.</td> 
    </tr> 
    <tr class="row-15 odd"> 
     <td class="column-1">Corey Romich </td>
     <td class="column-2">10</td>
     <td class="column-3">He consistently outworks his competitors to create a positive play for his team. His IQ for the game is above average.</td> 
    </tr> 
    <tr class="row-16 even"> 
     <td class="column-1">Jakigh Dotton </td>
     <td class="column-2">10</td>
     <td class="column-3">He is extremely well in transition and has a desire to make the right play for his team. He can shut down players with his defense.</td> 
    </tr> 
    <tr class="row-17 odd"> 
     <td class="column-1">Ryan Murdoch </td>
     <td class="column-2">11</td>
     <td class="column-3">Knows how to command his team with the ball. His jump shot is smooth and lethal. Leads by example.</td> 
    </tr> 
    <tr class="row-18 even"> 
     <td class="column-1">Mitchell Doherty </td>
     <td class="column-2">9</td>
     <td class="column-3">Uses anticipation to create good position in the post. When given the ball can finish at a high rate.</td> 
    </tr> 
    <tr class="row-19 odd"> 
     <td class="column-1">Jourdayne Ross</td>
     <td class="column-2">11</td>
     <td class="column-3">Extremely athletic and is a great defender. Has the ability to finish on the offensive end. </td> 
    </tr> 
    <tr class="row-20 even"> 
     <td class="column-1">Brendan Hoban </td>
     <td class="column-2">11</td>
     <td class="column-3">He can run the floor very well and finishes strong at the rim. Has a great understanding of the game and can anticipate the next play.</td> 
    </tr> 
    <tr class="row-21 odd"> 
     <td class="column-1">Isaiah Hinds </td>
     <td class="column-2">9</td>
     <td class="column-3">He uses his athleticism to get around players and plays with a high level of energy. Communicates well on both sides of the court.</td> 
    </tr> 
    <tr class="row-22 even"> 
     <td class="column-1">Willie Veal </td>
     <td class="column-2">10</td>
     <td class="column-3">Has a solid jump shot with a positive attitude. His IQ for the game is strong with great defensive skills.</td> 
    </tr> 
    <tr class="row-23 odd"> 
     <td class="column-1">Justin Leip</td>
     <td class="column-2">11</td>
     <td class="column-3">Great initiator with the ball. Controls the offense with a well-rounded offensive skill set.</td> 
    </tr> 
    <tr class="row-24 even"> 
     <td class="column-1">Alex Renaud </td>
     <td class="column-2">9</td>
     <td class="column-3">Alex works hard at all times. His basketball IQ and defensive concepts are excellent.</td> 
    </tr> 
    <tr class="row-25 odd"> 
     <td class="column-1">Brendan T. Williams</td>
     <td class="column-2">11</td>
     <td class="column-3">Athletic wing that is very quick in transition and sprints the floor looking to make a play. </td> 
    </tr> 
    <tr class="row-26 even"> 
     <td class="column-1">Ross Gerber </td>
     <td class="column-2">11</td>
     <td class="column-3">Very good vision in his game. Uses his high IQ to find players. Can knock down from the 3 point line and can handle the ball.</td> 
    </tr> 
    <tr class="row-27 odd"> 
     <td class="column-1">John Mathis </td>
     <td class="column-2">11</td>
     <td class="column-3">Rebounds the ball well and has a great IQ for reading the floor and finding the open players.</td> 
    </tr> 
    <tr class="row-28 even"> 
     <td class="column-1">Kris Caroll </td>
     <td class="column-2">12 </td>
     <td class="column-3">Can see the floor well and drives to find the open man. Excellent at breaking defender down to the rim.</td> 
    </tr> 
    <tr class="row-29 odd"> 
     <td class="column-1">Dominic Grauso </td>
     <td class="column-2">11</td>
     <td class="column-3">Agressive slasher with a high motor who gets to the rim with ease. Very good ball handler who sees the floor well and makes good decisions. </td> 
    </tr> 
    <tr class="row-30 even"> 
     <td class="column-1">Jeff Card </td>
     <td class="column-2">10</td>
     <td class="column-3">High IQ guard who makes hustle plays. Smart on D with a high motor.</td> 
    </tr> 
    <tr class="row-31 odd"> 
     <td class="column-1">Anthony Sorino </td>
     <td class="column-2">11</td>
     <td class="column-3">Great motor on the glass. Smart player who sees angles to find open players. </td> 
    </tr> 
    <tr class="row-32 even"> 
     <td class="column-1">Jermaine Fernandes </td>
     <td class="column-2">9</td>
     <td class="column-3">Small crafty PG with a good handle. Creates well for his teammates and reads the floor well. </td> 
    </tr> 
    <tr class="row-33 odd"> 
     <td class="column-1">Brian Short </td>
     <td class="column-2">11</td>
     <td class="column-3">Well-built jump shooter with a good frame. Uses his body well inside to score. Knows how to be physical.</td> 
    </tr> 
    <tr class="row-34 even"> 
     <td class="column-1">Aidan Hirsh </td>
     <td class="column-2">12</td>
     <td class="column-3">Physical guard who always makes the right play. Aggressive defender who can pick his defenders pocket. Uses a very high IQ to create plays.</td> 
    </tr> 
    <tr class="row-35 odd"> 
     <td class="column-1">Janell Roberts</td>
     <td class="column-2">12 </td>
     <td class="column-3">Very athletic wing with a high motor. Finds a way to make a play around the rim and finish the play. Ability to stretch the floor with his 3 pt shot. </td> 
    </tr> 
    <tr class="row-36 even"> 
     <td class="column-1">Tyler Dunn</td>
     <td class="column-2">11</td>
     <td class="column-3">Long shooter that can hit shots off the catch or off the dribble. He is also an effective passer from the wing. </td> 
    </tr> 
    <tr class="row-37 odd"> 
     <td class="column-1">David Roelke</td>
     <td class="column-2">11</td>
     <td class="column-3">David can score at will from the outside, and also has an easy time getting in the paint. He also facilitates when he draws extra defenders. </td> 
    </tr> 
    <tr class="row-38 even"> 
     <td class="column-1">Brian Monaghan</td>
     <td class="column-2">10</td>
     <td class="column-3">Point guard who is very skilled. He can score at the rim and off the dribble. He plays with great energy and speed. </td> 
    </tr> 
    <tr class="row-39 odd"> 
     <td class="column-1">Avelino Damoura</td>
     <td class="column-2">10</td>
     <td class="column-3">Avelino is a good shooter who moves the ball to find teammates. He is a good leader on both ends.</td> 
    </tr> 
    <tr class="row-40 even"> 
     <td class="column-1">Christopher Hulbert</td>
     <td class="column-2">9</td>
     <td class="column-3">Very smart player who finds open areas in the defense. He can also shoot it from distance.</td> 
    </tr> 
    <tr class="row-41 odd"> 
     <td class="column-1">Devondre Edmonds</td>
     <td class="column-2">9</td>
     <td class="column-3">Devondre is an impressive, young wing who will help a team immediately at the next level. He is smart and athletic, with a nose for the ball. </td> 
    </tr> 
    <tr class="row-42 even"> 
     <td class="column-1">Andrew Galanek</td>
     <td class="column-2">11</td>
     <td class="column-3">Andrew makes all the hustle plays and is a great off the ball defender. He is an impact on both sides of the ball.</td> 
    </tr> 
    <tr class="row-43 odd"> 
     <td class="column-1">Nazaair Bryan</td>
     <td class="column-2">9</td>
     <td class="column-3">Nazaair is an aggressive point guard with a big upside. He shoots well from mid range and works hard on defense. </td> 
    </tr> 
    <tr class="row-44 even"> 
     <td class="column-1">Anthony Kennon</td>
     <td class="column-2">11</td>
     <td class="column-3">Anthony is a long guard that is determined to get to the rim. He can passes well and can finish in transition. </td> 
    </tr> 
    <tr class="row-45 odd"> 
     <td class="column-1">John O'Connor</td>
     <td class="column-2">11</td>
     <td class="column-3">John is a tall guard that can control the pace of the floor. He finds ways to set himself up, as well as teammates, for open shots. </td> 
    </tr> 
    <tr class="row-46 even"> 
     <td class="column-1">Luqman Abdur-Rauf</td>
     <td class="column-2">11</td>
     <td class="column-3">Small guard that plays hard all the time. He sprints in transition and has a great feel for spacing the floor.</td> 
    </tr> 
    <tr class="row-47 odd"> 
     <td class="column-1">Joey Polczynski</td>
     <td class="column-2">11</td>
     <td class="column-3">Shooter who knocks down 3's. Great penetrate that finds open teammates.</td> 
    </tr> 
    <tr class="row-48 even"> 
     <td class="column-1">Timothy O'Neill</td>
     <td class="column-2">11</td>
     <td class="column-3">Quick point guard that handles the ball and pushes it forward in transition. He also finishes at the rim with both hands.</td> 
    </tr> 
    <tr class="row-49 odd"> 
     <td class="column-1">Erik Braaten</td>
     <td class="column-2">11</td>
     <td class="column-3">He is a long big man with the ability to shoot from the perimeter. He also disrupts the opposition in the paint. </td> 
    </tr> 
    <tr class="row-50 even"> 
     <td class="column-1">Geovonni Kraus</td>
     <td class="column-2">11</td>
     <td class="column-3">Big athletic wing who uses his body well inside. He plays big in the post and has a high motor on the glass.</td> 
    </tr> 
    <tr class="row-51 odd"> 
     <td class="column-1">Darrion Joran-Thomas</td>
     <td class="column-2">9</td>
     <td class="column-3">Big athletic post with a huge upside. He has a great IQ that helps him on defense. </td> 
    </tr> 
    <tr class="row-52 even"> 
     <td class="column-1">Earnest Umoh</td>
     <td class="column-2">10</td>
     <td class="column-3">Big wing who plays physical. He is a good defender with good instincts.</td> 
    </tr> 
    <tr class="row-53 odd"> 
     <td class="column-1">Jason Palmer</td>
     <td class="column-2">10</td>
     <td class="column-3">Jason is a long point guard who makes smart decisions and finds open teammates. He has a good IQ and gets in the paint. </td> 
    </tr> 
    <tr class="row-54 even"> 
     <td class="column-1">Samuel Whittle</td>
     <td class="column-2">10</td>
     <td class="column-3">He is a smart, physical scorer who can shoot and get to the rim. He is a good rebounder on both ends. </td> 
    </tr> 
    <tr class="row-55 odd"> 
     <td class="column-1">Davonte Metts</td>
     <td class="column-2">10</td>
     <td class="column-3">Tough guard with a good handle. Really good shooter who can get his feet set off screens. </td> 
    </tr> 
    <tr class="row-56 even"> 
     <td class="column-1">Ryan Monaghan</td>
     <td class="column-2">10</td>
     <td class="column-3">He is an athletic guard with a good feel for the game. He shoots the ball well on the perimeter. </td> 
    </tr> 
    <tr class="row-57 odd"> 
     <td class="column-1">Wolf Kalb </td>
     <td class="column-2">10</td>
     <td class="column-3">Wing with excellent basketball IQ. Very athletic and has a nice jump shot.</td> 
    </tr> 
    <tr class="row-58 even"> 
     <td class="column-1">Imer Ortiz </td>
     <td class="column-2">11</td>
     <td class="column-3">He can shut down players on the defensive end. Great at rebounding the ball.</td> 
    </tr> 
    <tr class="row-59 odd"> 
     <td class="column-1">Bailey Patella</td>
     <td class="column-2">11</td>
     <td class="column-3">Smart player on both ends of the floor and knows when to make the right play. Uses his basketball IQ to find open players.</td> 
    </tr> 
    <tr class="row-60 even"> 
     <td class="column-1">Kyle Butler </td>
     <td class="column-2">8</td>
     <td class="column-3">Can see the floor extremely well and can blow by any defender. He is at his best when he is handling the offense.</td> 
    </tr> 
    <tr class="row-61 odd"> 
     <td class="column-1">Tyjon Gilmore </td>
     <td class="column-2">11</td>
     <td class="column-3">Big point guard with great ball handling. Knows how to break down his defender and attack the point. Has great touch around the basket. </td> 
    </tr> 
    <tr class="row-62 even"> 
     <td class="column-1">Brandon Chaisson </td>
     <td class="column-2">11</td>
     <td class="column-3">Hard-working guard who is very active on the defensive end. Uses his high IQ to make an impact on the game.</td> 
    </tr> 
    <tr class="row-63 odd"> 
     <td class="column-1">Greg Thomas </td>
     <td class="column-2">9</td>
     <td class="column-3">Athletic point guard with great vision and decision making on the floor. Excellent on ball defender who causes problems for his man. </td> 
    </tr> 
    <tr class="row-64 even"> 
     <td class="column-1">Lorenzo Fernandez </td>
     <td class="column-2">11</td>
     <td class="column-3">Long wing who causes problems on defense with his athleticism. Crashes the glass hard on both ends of the court.</td> 
    </tr> 
    <tr class="row-65 odd"> 
     <td class="column-1">Darnick Boyd </td>
     <td class="column-2">12 </td>
     <td class="column-3">Strong athlete with a good handle with the ball. Can shoot the 3 off the dribble.</td> 
    </tr> 
    <tr class="row-66 even"> 
     <td class="column-1"> Petnaude </td>
     <td class="column-2">10</td>
     <td class="column-3">Strong player and can score around the rim efficiently. Using his athleticism he can grab rebounds at a high rate.</td> 
    </tr> 
    <tr class="row-67 odd"> 
     <td class="column-1">Andrew Sciancalepore </td>
     <td class="column-2">11</td>
     <td class="column-3">Talented player that can find multiple ways to find the rim. Excels at finishing and rebounding. </td> 
    </tr> 
    <tr class="row-68 even"> 
     <td class="column-1">Cade Theil </td>
     <td class="column-2">10</td>
     <td class="column-3">Can find ways to get open and make shots. Knows how to handle the ball and plays hard at all times. </td> 
    </tr> 
    <tr class="row-69 odd"> 
     <td class="column-1">Jonathan Batista </td>
     <td class="column-2">11</td>
     <td class="column-3">Unselfish point guard that always is trying to find the open player. Knows how to break people down and get into the paint. </td> 
    </tr> 
    <tr class="row-70 even"> 
     <td class="column-1">Alex Mrusek</td>
     <td class="column-2">11</td>
     <td class="column-3">Big body forward who is physical in the paint. He is a good passer and makes smart plays. </td> 
    </tr> 
    <tr class="row-71 odd"> 
     <td class="column-1">Brian Mukasa</td>
     <td class="column-2">12</td>
     <td class="column-3">He is a very good guard with handles and a sweet jumper. If he continues to make smart plays he will be a good player at the next level.</td> 
    </tr> 
    <tr class="row-72 even"> 
     <td class="column-1">Alex Holloway</td>
     <td class="column-2">11</td>
     <td class="column-3">Alex is a crafty lefty who can get out and push in transition. He has a good change of pace and scores consistently. </td> 
    </tr> 
    <tr class="row-73 odd"> 
     <td class="column-1">Devrae Burns</td>
     <td class="column-2">9</td>
     <td class="column-3">He competes well against older competition and never backs down. He has a good jump shot and gets space to take it. </td> 
    </tr> 
    <tr class="row-74 even"> 
     <td class="column-1">Benny Mandelbrot</td>
     <td class="column-2">10</td>
     <td class="column-3">He showed his ability to impact the game on offense and defense. He catches and finishes in transition. </td> 
    </tr> 
    <tr class="row-75 odd"> 
     <td class="column-1">Jared Whitt</td>
     <td class="column-2">9</td>
     <td class="column-3">Jared has excellent size and uses it to rebound. He runs the floor well and consistently. </td> 
    </tr> 
    <tr class="row-76 even"> 
     <td class="column-1">Rayan Ibraheem</td>
     <td class="column-2">12</td>
     <td class="column-3">Rayan is a big, physical forward that plays extremely hard. Rebounds the ball well on both ends of the court.</td> 
    </tr> 
    <tr class="row-77 odd"> 
     <td class="column-1">Bahu Abdul-Wadud</td>
     <td class="column-2">10</td>
     <td class="column-3">Bahu is a guard that loves to attack the rim. Excellent ball-handler and on-ball defender.</td> 
    </tr> 
    <tr class="row-78 even"> 
     <td class="column-1">Rick'Keem Mixson</td>
     <td class="column-2">11</td>
     <td class="column-3">Rick'Keem is an athletic guard that can penetrate the lane with ease. Excels at creating shots for his teammates.</td> 
    </tr> 
    <tr class="row-79 odd"> 
     <td class="column-1">Keiayvin Hayes</td>
     <td class="column-2">11</td>
     <td class="column-3">Keiayvin is a quick and athletic guard who can get into the lane with ease. Very elusive in transition.</td> 
    </tr> 
    <tr class="row-80 even"> 
     <td class="column-1">James Lustig</td>
     <td class="column-2">9</td>
     <td class="column-3">James is a strong guard that works every single possession. Rebounds well for his size and is always apart of loose ball situations.</td> 
    </tr> 
    <tr class="row-81 odd"> 
     <td class="column-1">Jackson Mannix</td>
     <td class="column-2">10</td>
     <td class="column-3">Jackson is a forward who rebounds the ball well. Very adept at shooting the ball from deep.</td> 
    </tr> 
    <tr class="row-82 even"> 
     <td class="column-1">Kiwan Nicholson</td>
     <td class="column-2">10</td>
     <td class="column-3">Kiwan is a quick guard who can get to the rim off the bounce. Kiwan is an extremely hard worker.</td> 
    </tr> 
    <tr class="row-83 odd"> 
     <td class="column-1">Moody Bey</td>
     <td class="column-2">11</td>
     <td class="column-3">Athletic wing who is difficult to guard when attacking the basket. Good defender with quick feet.</td> 
    </tr> 
    <tr class="row-84 even"> 
     <td class="column-1">Sean Christy</td>
     <td class="column-2">9</td>
     <td class="column-3">Face-up forward that can take his defender to the basket it or give a stand still jumper. Attacks the basket aggressively when rebounding.</td> 
    </tr> 
    <tr class="row-85 odd"> 
     <td class="column-1">Sam Hanley</td>
     <td class="column-2">10</td>
     <td class="column-3">Sam is a guard that makes great decisions with the basketball. Handles the ball well, minimal turnovers, and has range shooting the ball.</td> 
    </tr> 
    <tr class="row-86 even"> 
     <td class="column-1">Malik Tavares</td>
     <td class="column-2">12</td>
     <td class="column-3">Malik is an athletic guard that plays hard on both ends of the court. Tough to guard when attacking the basket.</td> 
    </tr> 
    <tr class="row-87 odd"> 
     <td class="column-1">Victor Billups-Jackson</td>
     <td class="column-2">10</td>
     <td class="column-3">Victor is a long, athletic player with fast hands. When attacking the rim, can finish at the basket with either hand.</td> 
    </tr> 
    <tr class="row-88 even"> 
     <td class="column-1">Lawrence Sabir</td>
     <td class="column-2">11</td>
     <td class="column-3">Quick, athletic guard with terrific handle. Due to his work ethic, Lawrence is involved in almost every play.</td> 
    </tr> 
    <tr class="row-89 odd"> 
     <td class="column-1">Cameron Ray</td>
     <td class="column-2">9</td>
     <td class="column-3">Cameron has very good touch around the rim. Long and athletic, uses this to his advantage rebounding the basketball.</td> 
    </tr> 
    <tr class="row-90 even"> 
     <td class="column-1">Zachary Mancini</td>
     <td class="column-2">11</td>
     <td class="column-3">Strong, physical guard with good handle. Good shooter and tough to contest with his quick release.</td> 
    </tr> 
    <tr class="row-91 odd"> 
     <td class="column-1">Paul Durken</td>
     <td class="column-2">9</td>
     <td class="column-3">Paul can knock down the open jumper and finish at the rim with both hands. Very tough, determined player. </td> 
    </tr> 
    <tr class="row-92 even"> 
     <td class="column-1">Shandon Brown</td>
     <td class="column-2">9</td>
     <td class="column-3">Shandon is a terrific shooter from the three point line. Aggressive on-ball defender, frustrates the offense.</td> 
    </tr> 
    <tr class="row-93 odd"> 
     <td class="column-1">Isaiah Harrison</td>
     <td class="column-2">10</td>
     <td class="column-3">Isaiah is a quick, crafty guard who can score in a multitude of ways. Savvy when attacking the basket, looks for mid-range jumper when available.</td> 
    </tr> 
    <tr class="row-94 even"> 
     <td class="column-1">Nicholas Rupelli</td>
     <td class="column-2">12</td>
     <td class="column-3">Nicholas is a quick guard with a good first step. Attacks the basket at will.</td> 
    </tr> 
    <tr class="row-95 odd"> 
     <td class="column-1">Cormac Ryan</td>
     <td class="column-2">10</td>
     <td class="column-3">Cormac is a long guard that finishes well in transition. Drives to the rim and finds open teammates.</td> 
    </tr> 
    <tr class="row-96 even"> 
     <td class="column-1">Maximillian Kiehm</td>
     <td class="column-2">11</td>
     <td class="column-3">Maximillian shoots the ball well from mid-range and three point distance. Active defender, always gets his hands in passing lanes.</td> 
    </tr> 
    <tr class="row-97 odd"> 
     <td class="column-1">Elijah Lewis</td>
     <td class="column-2">12</td>
     <td class="column-3">Lefty, quick guard who is crafty around the basket. Skilled ball-handler, low turnover rate.</td> 
    </tr> 
   </tbody> 
  </table> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>