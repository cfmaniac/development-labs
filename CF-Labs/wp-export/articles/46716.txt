<article id="post-46716" class="post-46716 post type-post status-publish format-standard has-post-thumbnail hentry category-alumni-updates"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p>Dreams Do Come True</p> 
  <p>Many Hoop Group alum have succeeded at many different levels, from the NBA, Division 1 as well as small colleges and in high school. On this page we look at some of the Hoop Group alumni excelling at the highest level of basketball, the NBA.</p> 
  <p>The greatest of the NBA players are selected to showcase their skills during All Star Weekend. In addition to the NBA All Star game scheduled for Sunday, the weekend of the NBA All Star Game has become a bigger showcase of the skills of the top players with All Star Saturday night.</p> 
  <p>A look at the Hoop Group Alumni in the NBA Saturday night which includes the 3 point shooting contest, Skills Challenge, Shooting Stars Competition and Slam Dunk Contest.</p> 
  <table id="tablepress-561" class="tablepress tablepress-id-561"> 
   <thead> 
    <tr class="row-1 odd"> 
     <th class="column-1">&nbsp;</th>
     <th class="column-2">&nbsp;</th>
     <th class="column-3">&nbsp;</th>
     <th class="column-4">&nbsp;</th> 
    </tr> 
   </thead> 
   <tbody> 
    <tr class="row-2 even"> 
     <td class="column-1"><b>SEARS SHOOTING STARS</b></td>
     <td class="column-2"></td>
     <td class="column-3"></td>
     <td class="column-4"></td> 
    </tr> 
    <tr class="row-3 odd"> 
     <td class="column-1"><a href="http://hoopgroup.com/?attachment_id=46734" rel="attachment wp-att-46734"><img src="https://hoopgroup.com/wp-content/uploads/2014/02/hardawayjr-e1392232513478.jpg" alt="" width="100" height="102" class="aligncenter size-full wp-image-46734"></a></td>
     <td class="column-2">Tim Hardaway JR</td>
     <td class="column-3">Knicks</td>
     <td class="column-4">Michigan</td> 
    </tr> 
    <tr class="row-4 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2016/02/chrisbosh2100.jpg" alt="chrisbosh2100" width="100" height="120" class="aligncenter size-full wp-image-70769"></td>
     <td class="column-2">Chris Bosh</td>
     <td class="column-3">Heat</td>
     <td class="column-4">Georgia Tech</td> 
    </tr> 
    <tr class="row-5 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2011/03/kevindurant100.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-7585"></td>
     <td class="column-2">Kevin Durant</td>
     <td class="column-3">Thunder</td>
     <td class="column-4">Texas</td> 
    </tr> 
    <tr class="row-6 even"> 
     <td class="column-1"><b>TACO BELL SKILLS CHALLENGE</b></td>
     <td class="column-2"></td>
     <td class="column-3"></td>
     <td class="column-4"></td> 
    </tr> 
    <tr class="row-7 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2014/02/MichaelCarterWilliams100.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-46613"></td>
     <td class="column-2">Michael Carter-Williams</td>
     <td class="column-3">76ers</td>
     <td class="column-4">Syracuse</td> 
    </tr> 
    <tr class="row-8 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2013/04/TreyBurke100.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-29818"></td>
     <td class="column-2">Trey Burke</td>
     <td class="column-3">Jazz</td>
     <td class="column-4">Michigan</td> 
    </tr> 
    <tr class="row-9 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2016/02/DamianLillard100.jpg" alt="DamianLillard100" width="100" height="120" class="aligncenter size-full wp-image-71047"></td>
     <td class="column-2">Damian Lillard</td>
     <td class="column-3">Trail Blazers</td>
     <td class="column-4">Weber State</td> 
    </tr> 
    <tr class="row-10 even"> 
     <td class="column-1"><b>FOOT LOCKER 3 POINT CONTEST</b></td>
     <td class="column-2"></td>
     <td class="column-3"></td>
     <td class="column-4"></td> 
    </tr> 
    <tr class="row-11 odd"> 
     <td class="column-1"><a href="http://hoopgroup.com/?attachment_id=46736" rel="attachment wp-att-46736"><img src="https://hoopgroup.com/wp-content/uploads/2014/02/48_afflaloaaron-e1392232769668.jpg" alt="" width="100" height="119" class="aligncenter size-full wp-image-46736"></a></td>
     <td class="column-2">Arron Afflalo</td>
     <td class="column-3">Magic</td>
     <td class="column-4">UCLA</td> 
    </tr> 
    <tr class="row-12 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2011/02/Kyrie-Irving-100.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-2943"></td>
     <td class="column-2">Kyrie Irving</td>
     <td class="column-3">Cavaliers</td>
     <td class="column-4">Duke</td> 
    </tr> 
    <tr class="row-13 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2016/02/DamianLillard100.jpg" alt="DamianLillard100" width="100" height="120" class="aligncenter size-full wp-image-71047"></td>
     <td class="column-2">Damian Lillard</td>
     <td class="column-3">Trail Blazers</td>
     <td class="column-4">Weber State</td> 
    </tr> 
    <tr class="row-14 even"> 
     <td class="column-1"><b>SPRITE SLAM DUNK</b></td>
     <td class="column-2"></td>
     <td class="column-3"></td>
     <td class="column-4"></td> 
    </tr> 
    <tr class="row-15 odd"> 
     <td class="column-1"><a href="http://hoopgroup.com/?attachment_id=46737" rel="attachment wp-att-46737"><img src="https://hoopgroup.com/wp-content/uploads/2014/02/imgres2-e1392232825409.jpg" alt="" width="100" height="130" class="aligncenter size-full wp-image-46737"></a></td>
     <td class="column-2">Paul George</td>
     <td class="column-3">Pacers</td>
     <td class="column-4">Fresno State</td> 
    </tr> 
    <tr class="row-16 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2011/02/JohnWall100.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-2475"></td>
     <td class="column-2">John Wall</td>
     <td class="column-3">Wizards</td>
     <td class="column-4">Kentucky</td> 
    </tr> 
    <tr class="row-17 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2016/02/DamianLillard100.jpg" alt="DamianLillard100" width="100" height="120" class="aligncenter size-full wp-image-71047"></td>
     <td class="column-2">Damian Lillard</td>
     <td class="column-3">Trail Blazers</td>
     <td class="column-4">Weber State</td> 
    </tr> 
   </tbody> 
  </table> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>