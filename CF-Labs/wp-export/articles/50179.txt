<article id="post-50179" class="post-50179 post type-post status-publish format-standard hentry category-tournament-recap"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p><a href="https://hoopgroup.com/wp-content/uploads/2014/04/DSC_0316.jpg"><img class="aligncenter size-full wp-image-50190" alt="DSC_0316" src="https://hoopgroup.com/wp-content/uploads/2014/04/DSC_0316.jpg" width="441" height="200"></a></p> 
  <p>&nbsp;</p> 
  <p>Day two of the Pitt Jam Fest promised to be action-packed and it did not disappoint. The UA Association teams put together their respective shows while others took their opportunity in the spotlight and made sure it extended all the way to Championship Sunday. Heres a few top performers from the second day of the Pitt Jam Fest:</p> 
  <p><strong>Tookie Brown | Atl Xpress | 5-10 G | Class of 2015</strong></p> 
  <p>On an AAU team with plenty of high-major talent, Brown might be overlooked a bit, literally and figuratively. But the guard, who verbally committed to Mississippi State back in January, made sure he was front and center for Xpress first win on Saturday. Great passing in the lane and a scoring touch has put his team in the 17U Elite 8.</p> 
  <p><strong>David Krmpotich | Jersey Shore Warriors | 6-7 F | Class of 2015</strong></p> 
  <p>Krmpotich is a little slender, but he was a defensive force in the Warriors two Saturday games. A good rebounder and an average finisher around the rim, the forward could really see his stock rise in the final game set.</p> 
  <p><strong>Tevin Mack | Carolina Wolves | 6-6 W | Class of 2015</strong></p> 
  <p>Whether it was inside or outside, if Tevin Mack was shooting it, the ball was probably going through the hoop. It was that kind of day for both Mack and his Carolina Wolves, as he hasnt scored below 32 points in three games this weekend at the Pitt Jam Fest.</p> 
  <p><strong>Amir Hinton | PA Renegades | 6-2 G | Class of 2015</strong></p> 
  <p>Hinton did all he could against a talented WE R1 17U squad on Saturday. Whether it was grabbing a steal by blocking the passing lane or showing off a nice handle to get to the rim, Hinton was able to give his opponent a tough time for 32 minutes.</p> 
  <p><strong>Mustapha Heron | New Heights (N.Y.) | 6-5 G | Class of 2016</strong></p> 
  <p>Big game after big game for the Pittsburgh verbal commit. A 27-point performance in the Round of 16 put New Heights in contention for the 17U championship on Sunday.</p> 
  <p><strong>Grant Golden | VA Elite | 6-9 PF | Class of 2016</strong></p> 
  <p>Golden was nearly perfect from the field in his 24 point performance on Saturday. With his size, skills and IQ it dont be too much longer that his only scholarship offer remains from James Madison.</p> 
  <p><strong>Matt Moyer | VCC Ohio Warriors | 6-8 F | Class of 2016</strong></p> 
  <p>Not to be overshadowed by his heralded teammate Seth Towns, Moyer brought plenty of energy and toughness to the table. He scored 15 points from all over the court and currently holds offers from Wisconsin, Virginia Tech, Detroit and Dayton.</p> 
  <p><strong>Jon Teske | Team Work | 6-10 C | Class of 2016</strong></p> 
  <p>Teske wont wow you with dunks or shot blocking but hes a skilled post who works to get shots near the rim. Teske had an efficient scoring output with short jumphooks and 10 foot jumpers.</p> 
  <p><strong>Peyton Wejnert | ShoreShots (N.J.) | 6-5 G | Class of 2016</strong></p> 
  <p>Possessing great height for a guard, Wejnert is able to see the court and make plays for his teammates. Hes also a big time scorer and is a triple-double threat whenever he takes the court.</p> 
  <p><strong>Mamadou Ndiaye | Bmores Finest | 6-7 F | Class of 2016</strong></p> 
  <p>One of the most athletic players in attendance, Ndiaye possesses a ton of upside. When he begins to play with a non-stop motor, it will be scary what he can accomplish.</p> 
  <p><a href="https://twitter.com/AndrewKoob">@AndrewKoob</a><br> <a href="https://twitter.com/Josh_Stirn">@Josh_Stirn</a><br> contributed to this article</p> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>