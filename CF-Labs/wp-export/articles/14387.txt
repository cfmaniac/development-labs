<article id="post-14387" class="post-14387 post type-post status-publish format-standard has-post-thumbnail hentry category-tournament-recap tag-hgshowcase tag-hudson-catholic tag-rutgers-prep"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p><a href="http://hoopgroup.com/wp-content/uploads/2011/12/DSC09899.jpg"><img src="http://hoopgroup.com/wp-content/uploads/2011/12/DSC09899-1024x635.jpg" alt="" title="DSC09899" width="640" height="396" class="aligncenter size-large wp-image-14388"></a></p> 
  <p>With all the Division I interest in Hudson Catholics starting lineup, combined with a rough 4-for-23 shooting performance in the first half, Rutgers Prep looked down. </p> 
  <p>But, apparently, they werent out.</p> 
  <p>The Argonauts shot 77% from the floor in the second half, outscoring Hudson Catholic 35-17 and scoring the biggest upset of the Hoop Group Tip Off Classic, 44-43. </p> 
  <p>In the first half, we came out slow, I did too, but we never hung our heads, said Mike Klinger, who had 15 points and took home MVP honors. I think it was a little bit of first game jitters. Coach knew we had jitters, but he took us into the locker room, calmed us down, and we did what we needed to do.</p> 
  <p>Klinger scored 13 of his 15 points in the second half to lead the Rutgers Prep charge, helped along by 12 points from Gregg Grippo. </p> 
  <p>The Argonauts pulled it to within two points on a Joey Kacmarsky three-pointer with 4:51 remaining, and a layup twenty seconds later tied the game.</p> 
  <p>With twenty seconds to play, Hudson Catholics Travis Flagg was called for a traveling violation on a drive to the basket. After Rutgers Prep a missed Rutgers Prep free throw, Hudson Catholic was unable to get a final shot off and took the loss. </p> 
  <p>This game is definitely positive for us, but we cant dwell on this game, said Klinger. We have to get right back into it and get ready for our home opener. </p> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>