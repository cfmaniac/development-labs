<article id="post-91065" class="post-91065 post type-post status-publish format-standard has-post-thumbnail hentry category-uncategorized"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <div style="position: relative; height: 0; padding-bottom: 56.25%;">
   <iframe style="position: absolute; width: 100%; height: 100%; left: 0;" src="https://www.youtube.com/embed/B9HMWPgKGDs?ecver=2" width="640" height="360" frameborder="0" allowfullscreen></iframe>
  </div> 
  <p>&nbsp;</p> 
  <p><strong>This week I had a chance to take in a few scrimmages.</strong> I wanted to get a look <a href="http://hoopgroup.com/hoop-group-headquarters/new-jersey-basketball-clinics/holiday-break-hoop-fest/">at</a> the Top teams before my first ranking. Tomorrow I will get my first look at St. Rose and SJV two powerhouse teams.</p> 
  <p><strong>There is zero question in my mind that RBC</strong> can win the SCT or TOC. They really have all the pieces in place. The RBC chemistry and senior leadership was as impressive&nbsp;as their shooting.<strong><span style="color: #ff0000;"> Hayley Moore and Rose Caverly</span></strong>&nbsp;were murder from the arc. <span style="color: #ff0000;">Sophia Sabino</span> the freshman point guard, was very impressive on both sides of the ball.&nbsp;A real concern has to be when <strong><span style="color: #ff0000;">Katie Rice&nbsp; is not on the floor</span></strong>&nbsp;. <span style="color: #ff0000;">Fab Eggenshwiler</span> was most likely going to be the 5th starter and she is out with a injury.</p> 
  <p><strong>I thought RFH was in full blown rebuilding mode.</strong> But Monday they blew me away, as they dominated Middletown South at times. <span style="color: #ff0000;">Tori Hyduke</span> was on a whole different level than anyone in the gym. <span style="color: #ff0000;">Lucy Adams</span>, did just about anything you could ask a sophomore to do. <span style="color: #ff0000;">Grace Munt</span> showed shes ready for prime time. Middletown South are the walking wounded, they entire starting five with the expectation of <span style="color: #ff0000;">Stephanie Mayerhoffer.</span> So we really wont know about them for another week or two. They clearly have the talent to be a special group. <span style="color: #ff0000;">Tom Brennan</span> is on the sidelines, so you can expect major improvements.</p> 
  <p><strong>RBR was so well coaches it shocked it me.</strong> It made me remember how good <span style="color: #ff0000;">Coach John Turhan</span> is at getting kids to preform above their talent level. TRN was also very impressive, they showed many different looks on defense. They also can really score the ball, <span style="color: #ff0000;">Jenna Paul, Brille Bisgono and Amanda Johnson</span> can play with anyone. They are a Top 20 team and could be Top 10 with a few breaks.</p> 
  <h2 style="text-align: center;">Upcoming Events</h2> 
  <p style="text-align: center;">Pre-register online with links below</p> 
  <hr> 
  <p style="text-align: left;">&nbsp; <a href="https://hoopgroup.com/wp-content/uploads/2017/12/IMG_41191.jpg"><img class="aligncenter size-full wp-image-91068" src="https://hoopgroup.com/wp-content/uploads/2017/12/IMG_41191.jpg" alt="" width="600" height="250"></a><a href="http://hoopgroup.com/hoop-group-headquarters/weekly-basketball-instruction-programs/core-skills-training/">Core Skills | Saturdays 9:00 am  10:30 am and 10:30 am  12:00 pm</a></p> 
  <p><a href="http://hoopgroup.com/hoop-group-headquarters/holiday-hoop-fests/">Holiday Hoop Fests | December 26th-30th | 10:00 am  3:00 pm</a></p> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>