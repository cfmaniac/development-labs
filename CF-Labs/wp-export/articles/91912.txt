<article id="post-91912" class="post-91912 post type-post status-publish format-standard hentry category-headquarters-blog"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <h3 style="text-align: center;">2016 Team Champions</h3> 
  <table style="height: 42px;" width="575"> 
   <tbody> 
    <tr> 
     <td style="width: 279.5px; text-align: center;"><a href="http://hoopgroup.com/team-tournaments/grassroots-events/boys-showdowns/spring-showdown/attachment/champs-2-2/" rel="attachment wp-att-74834"><img class="wp-image-74834 size-medium" src="https://hoopgroup.com/wp-content/uploads/2012/03/champs-2-300x300.jpg" alt="15U Champs - NY Rens" width="300" height="300"></a>15U Champs  NY Rens</td> 
     <td style="width: 279.5px; text-align: center;"><a href="http://hoopgroup.com/team-tournaments/grassroots-events/boys-showdowns/spring-showdown/attachment/champs-3-2/" rel="attachment wp-att-74835"><img class="wp-image-74835 size-medium" src="https://hoopgroup.com/wp-content/uploads/2012/03/champs-3-300x300.jpg" alt="17U Champs - Playtime Panthers" width="300" height="300"></a>17U Champs  Playtime Panthers</td> 
    </tr> 
    <tr> 
     <td style="width: 279.5px; text-align: center;"><a href="http://hoopgroup.com/team-tournaments/grassroots-events/boys-showdowns/spring-showdown/attachment/sd-champs/" rel="attachment wp-att-74839"><img class="wp-image-74839 size-medium" src="https://hoopgroup.com/wp-content/uploads/2012/03/sd-champs-300x300.jpg" alt="16U Champs - Randallstown Runnin Rebels" width="300" height="300"></a>16U Champs <p></p> <p>Randallstown Runnin Rebels</p></td> 
     <td style="width: 279.5px; text-align: center;"></td> 
    </tr> 
   </tbody> 
  </table> 
  <h3 style="text-align: center;">2016 Most Valuable Players</h3> 
  <table style="width: 565px;"> 
   <tbody> 
    <tr> 
     <td style="text-align: center; width: 272.21875px;"><a href="http://hoopgroup.com/team-tournaments/grassroots-events/boys-showdowns/spring-showdown/attachment/mvp-1/" rel="attachment wp-att-74836"><img class="wp-image-74836 size-medium aligncenter" src="https://hoopgroup.com/wp-content/uploads/2012/03/mvp-1-300x300.jpg" alt="16U MVP - Christion Wright" width="300" height="300"></a>16U MVP  Christion Wright</td> 
     <td style="text-align: center; width: 281.78125px;"><a href="http://hoopgroup.com/team-tournaments/grassroots-events/boys-showdowns/spring-showdown/attachment/mvp-2/" rel="attachment wp-att-74837"><img class="wp-image-74837 size-medium aligncenter" src="https://hoopgroup.com/wp-content/uploads/2012/03/mvp-2-300x300.jpg" alt="15U Co - MVP - Dquan Patterson &amp; Ibrahim Wattara" width="300" height="300"></a>15U Co  MVP  Dquan Patterson &amp; Ibrahim Wattara</td> 
    </tr> 
    <tr> 
     <td style="width: 272.21875px;"> <p style="text-align: center;"><a href="http://hoopgroup.com/team-tournaments/grassroots-events/boys-showdowns/spring-showdown/attachment/mvp-3/" rel="attachment wp-att-74838"><img class="wp-image-74838 size-medium aligncenter" src="https://hoopgroup.com/wp-content/uploads/2012/03/mvp-3-300x300.jpg" alt="17U MVP - Jamir Thomas" width="300" height="300"></a>17U MVP  Jamir Thomas</p> </td> 
     <td style="width: 281.78125px;"></td> 
    </tr> 
   </tbody> 
  </table> 
  <hr> 
  <h2 style="text-align: center;">2015 Most Valuable Players/h2&gt;</h2> 
  <a href="https://hoopgroup.com/wp-content/uploads/2012/03/unnamed-3.jpg"><img data-caption="Aiden Igiehon, Taj Price, Darnell Brodie, Casmir Ochiaka" class="aligncenter size-medium wp-image-60141" src="https://hoopgroup.com/wp-content/uploads/2012/03/unnamed-3-300x300.jpg" alt="Aiden Igiehon, Taj Price, Darnell Brodie, Casmir Ochiaka" width="300" height="300"></a>
  <p class="blog2html_p ">Aiden Igiehon, Taj Price, Darnell Brodie, Casmir Ochiaka</p> 
  <h2 style="text-align: center;">2015 Team Champions</h2> 
  <table style="height: 27px; width: 570px;"> 
   <tbody> 
    <tr> 
     <td style="width: 264px; text-align: center;"><a href="https://hoopgroup.com/wp-content/uploads/2012/03/unnamed1.jpg"><img class="size-medium wp-image-60137 aligncenter" src="https://hoopgroup.com/wp-content/uploads/2012/03/unnamed1-300x253.jpg" alt="14U Champs - NY Lightning" width="300" height="253"></a>14U Champs  NY Lightning</td> 
     <td style="width: 292px; text-align: center;"><a href="https://hoopgroup.com/wp-content/uploads/2012/03/unnamed-4.jpg"><img class="size-medium wp-image-60138 aligncenter" src="https://hoopgroup.com/wp-content/uploads/2012/03/unnamed-4-300x236.jpg" alt="15U Champs - NJ Roadrunners" width="300" height="236"></a>15U Champs  NJ Roadrunners</td> 
    </tr> 
    <tr> 
     <td style="width: 264px; text-align: center;"><a href="https://hoopgroup.com/wp-content/uploads/2012/03/unnamed-2.jpg"><img class="size-medium wp-image-60139 aligncenter" src="https://hoopgroup.com/wp-content/uploads/2012/03/unnamed-2-300x300.jpg" alt="16U Champs - NJ Roadrunners" width="300" height="300"></a>16U Champs  NJ Roadrunners</td> 
     <td style="width: 292px; text-align: center;"><a href="https://hoopgroup.com/wp-content/uploads/2012/03/unnamed-1.jpg"><img class="size-medium wp-image-60140 aligncenter" src="https://hoopgroup.com/wp-content/uploads/2012/03/unnamed-1-300x241.jpg" alt="17U Champs - NJ Roadrunners" width="300" height="241"></a>17U Champs  NJ Roadrunners</td> 
    </tr> 
   </tbody> 
  </table> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>