<article id="post-34786" class="post-34786 post type-post status-publish format-standard has-post-thumbnail hentry category-elite-camp-updates"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p><a href="https://hoopgroup.com/wp-content/uploads/2013/06/ivy-league-allstars.jpg"><img class="alignnone size-full wp-image-35395" alt="SONY DSC" src="https://hoopgroup.com/wp-content/uploads/2013/06/ivy-league-allstars.jpg" width="600" height="204"></a></p> 
  <h3>Ivy League White All-Stars</h3> <table 460 not found />
  <br> 
  <h3>Ivy League Blue All-Stars</h3> 
  <table id="tablepress-461" class="tablepress tablepress-id-461"> 
   <thead> 
    <tr class="row-1 odd"> 
     <th class="column-1">Name</th>
     <th class="column-2">Height</th>
     <th class="column-3">City, State</th>
     <th class="column-4">School </th>
     <th class="column-5">Graduation Year</th> 
    </tr> 
   </thead> 
   <tbody> 
    <tr class="row-2 even"> 
     <td class="column-1">Seth Brewer</td>
     <td class="column-2">5-8</td>
     <td class="column-3">Manheim, Pa. </td>
     <td class="column-4">Manheim Township HS</td>
     <td class="column-5">2014</td> 
    </tr> 
    <tr class="row-3 odd"> 
     <td class="column-1">Brian Kelley</td>
     <td class="column-2">6-3</td>
     <td class="column-3">Potomac, Md. </td>
     <td class="column-4">Bullis School</td>
     <td class="column-5">2013</td> 
    </tr> 
    <tr class="row-4 even"> 
     <td class="column-1">Champe Barton</td>
     <td class="column-2">6-3</td>
     <td class="column-3">Davie, Fla.</td>
     <td class="column-4">University School</td>
     <td class="column-5">2014</td> 
    </tr> 
    <tr class="row-5 odd"> 
     <td class="column-1">Donte Wallette</td>
     <td class="column-2">6-3</td>
     <td class="column-3">Gate Mills, Ohio</td>
     <td class="column-4">Gilmour Academy</td>
     <td class="column-5">2014</td> 
    </tr> 
    <tr class="row-6 even"> 
     <td class="column-1">Harrison Brown</td>
     <td class="column-2">6-4</td>
     <td class="column-3">Huntington, W.V.</td>
     <td class="column-4">Huntington</td>
     <td class="column-5">2015</td> 
    </tr> 
    <tr class="row-7 odd"> 
     <td class="column-1">Byron Jones</td>
     <td class="column-2">6-0</td>
     <td class="column-3">Alexandria, Va.</td>
     <td class="column-4">Episcopal HS</td>
     <td class="column-5">2014</td> 
    </tr> 
    <tr class="row-8 even"> 
     <td class="column-1">Stephen Sangree</td>
     <td class="column-2">6-3</td>
     <td class="column-3">Morristown, N.J.</td>
     <td class="column-4">Morristown Beard School</td>
     <td class="column-5">2014</td> 
    </tr> 
    <tr class="row-9 odd"> 
     <td class="column-1">Cager Hicks</td>
     <td class="column-2">6-4</td>
     <td class="column-3">Roanoke, Va.</td>
     <td class="column-4">Cave Spring</td>
     <td class="column-5">2014</td> 
    </tr> 
    <tr class="row-10 even"> 
     <td class="column-1">Jordan McRae</td>
     <td class="column-2">6-5</td>
     <td class="column-3">Dix Hills, N.Y.</td>
     <td class="column-4">Half Hollow Hills HS East</td>
     <td class="column-5">2014</td> 
    </tr> 
    <tr class="row-11 odd"> 
     <td class="column-1">Josh Fried</td>
     <td class="column-2">6-7</td>
     <td class="column-3">Bethesda, Md.</td>
     <td class="column-4">Walt Whitman HS</td>
     <td class="column-5">2014</td> 
    </tr> 
    <tr class="row-12 even"> 
     <td class="column-1">James Lammers</td>
     <td class="column-2">6-3</td>
     <td class="column-3">San Antonio, Texas</td>
     <td class="column-4">Alamo Heights HS</td>
     <td class="column-5">2014</td> 
    </tr> 
   </tbody> 
  </table> 
  <p><em><strong>Ivy League White 62, Ivy League Blue 48</strong></em></p> 
  <p><strong>Jonah Sens</strong> earned Ivy League All-Star game MVP after finishing with an impressive 12 points and nine rebounds. &nbsp;Ivy League MVP <strong>Marko Kozul</strong> had a nice showing as well as he finished the contest with 10 points, four rebounds and one assists. <strong>Harrison Brown</strong> paced the White All-Stars with 15 points, while <strong>Jordan McRae</strong> finished with a team-high six rebounds. <strong>Champe Barton</strong> performed solidly as well chipping in with nine points two assists and two rebounds. <a href="https://twitter.com/CabWashington">@CabWashington</a></p> 
  <p><a href="https://hoopgroup.com/wp-content/uploads/2013/06/patriot-league-all-stars.jpg"><img class="alignnone size-full wp-image-35394" alt="SONY DSC" src="https://hoopgroup.com/wp-content/uploads/2013/06/patriot-league-all-stars.jpg" width="600" height="212"></a></p> 
  <h3>Patriot League White All-Stars</h3> 
  <table id="tablepress-462" class="tablepress tablepress-id-462"> 
   <thead> 
    <tr class="row-1 odd"> 
     <th class="column-1">Name</th>
     <th class="column-2">Height</th>
     <th class="column-3">City, State</th>
     <th class="column-4">School</th>
     <th class="column-5">Graduation Year</th> 
    </tr> 
   </thead> 
   <tbody> 
    <tr class="row-2 even"> 
     <td class="column-1">Brendan Barcas</td>
     <td class="column-2">6-0</td>
     <td class="column-3">Wall, N.J.</td>
     <td class="column-4">Wall HS</td>
     <td class="column-5">2015</td> 
    </tr> 
    <tr class="row-3 odd"> 
     <td class="column-1">Michael Rice</td>
     <td class="column-2">5-9</td>
     <td class="column-3">Little Silver, N.J.</td>
     <td class="column-4">Red Bank Regional</td>
     <td class="column-5">2016</td> 
    </tr> 
    <tr class="row-4 even"> 
     <td class="column-1">Ryan Jensen</td>
     <td class="column-2">6-4</td>
     <td class="column-3">Manasquan, N.J.</td>
     <td class="column-4">Manasquan HS</td>
     <td class="column-5">2016</td> 
    </tr> 
    <tr class="row-5 odd"> 
     <td class="column-1">Bennett Kautz</td>
     <td class="column-2">5-7</td>
     <td class="column-3">Bridgewater, N.J.</td>
     <td class="column-4">Bridgewater-Raritan</td>
     <td class="column-5">2016</td> 
    </tr> 
    <tr class="row-6 even"> 
     <td class="column-1">Skyler Kolli</td>
     <td class="column-2">5-9</td>
     <td class="column-3">Indianapolis, Ind.</td>
     <td class="column-4">International School of Indiana</td>
     <td class="column-5">2015</td> 
    </tr> 
    <tr class="row-7 odd"> 
     <td class="column-1">Khalil Prescod</td>
     <td class="column-2">6-2</td>
     <td class="column-3">Markham, Ontario</td>
     <td class="column-4">Bill Crothers</td>
     <td class="column-5">2016</td> 
    </tr> 
    <tr class="row-8 even"> 
     <td class="column-1">Gabe Ceribelli</td>
     <td class="column-2">6-0</td>
     <td class="column-3">Parkton, Md.</td>
     <td class="column-4">Hererford</td>
     <td class="column-5">2015</td> 
    </tr> 
    <tr class="row-9 odd"> 
     <td class="column-1">Matthew Burns</td>
     <td class="column-2">6-0</td>
     <td class="column-3">Syracuse, N.Y.</td>
     <td class="column-4">Christian Brothers Academy</td>
     <td class="column-5">2015</td> 
    </tr> 
    <tr class="row-10 even"> 
     <td class="column-1">Sebastien Pierre-Louis</td>
     <td class="column-2">6-1</td>
     <td class="column-3">Lawrenceville, N.J.</td>
     <td class="column-4">The Lawrenceville School</td>
     <td class="column-5">2015</td> 
    </tr> 
    <tr class="row-11 odd"> 
     <td class="column-1">Sam Norton</td>
     <td class="column-2">6-1</td>
     <td class="column-3">Pottstown, Pa.</td>
     <td class="column-4">Hill School</td>
     <td class="column-5">2015</td> 
    </tr> 
   </tbody> 
  </table> 
  <h3>Patriot League Blue All-Stars</h3> 
  <table id="tablepress-463" class="tablepress tablepress-id-463"> 
   <thead> 
    <tr class="row-1 odd"> 
     <th class="column-1">Name</th>
     <th class="column-2">Height</th>
     <th class="column-3">City, State</th>
     <th class="column-4">School</th>
     <th class="column-5">Graduation Year</th> 
    </tr> 
   </thead> 
   <tbody> 
    <tr class="row-2 even"> 
     <td class="column-1">Ben Lee</td>
     <td class="column-2">5-9</td>
     <td class="column-3">Great Neck, N.Y.</td>
     <td class="column-4">Great Neck South</td>
     <td class="column-5">2016</td> 
    </tr> 
    <tr class="row-3 odd"> 
     <td class="column-1">Jack Engel</td>
     <td class="column-2">6-1</td>
     <td class="column-3">South Brunswick, N.J.</td>
     <td class="column-4">South Brunswick</td>
     <td class="column-5">2015</td> 
    </tr> 
    <tr class="row-4 even"> 
     <td class="column-1">Cort Williams</td>
     <td class="column-2">6-2</td>
     <td class="column-3">East Amherst, N.Y.</td>
     <td class="column-4">Williamsville East</td>
     <td class="column-5">2015</td> 
    </tr> 
    <tr class="row-5 odd"> 
     <td class="column-1">Brody Hicks</td>
     <td class="column-2">6-3</td>
     <td class="column-3">Roanoke, Va. </td>
     <td class="column-4">Cave Spring</td>
     <td class="column-5">2016</td> 
    </tr> 
    <tr class="row-6 even"> 
     <td class="column-1">Ethan Norton</td>
     <td class="column-2">5-11</td>
     <td class="column-3">Philadelphia, Pa.</td>
     <td class="column-4">LaSalle College HS</td>
     <td class="column-5">2017</td> 
    </tr> 
    <tr class="row-7 odd"> 
     <td class="column-1">Jared Reaser</td>
     <td class="column-2">5-8</td>
     <td class="column-3">Fleetwood, Pa.</td>
     <td class="column-4">Fleetwood</td>
     <td class="column-5">2015</td> 
    </tr> 
    <tr class="row-8 even"> 
     <td class="column-1">Will Desautelle</td>
     <td class="column-2">6-1</td>
     <td class="column-3">Newtown, Pa.</td>
     <td class="column-4">Council Rock HS-North</td>
     <td class="column-5">2016</td> 
    </tr> 
    <tr class="row-9 odd"> 
     <td class="column-1">Chase Waganer</td>
     <td class="column-2">6-3</td>
     <td class="column-3">Parkton, N.J.</td>
     <td class="column-4">Mereford</td>
     <td class="column-5">2015</td> 
    </tr> 
    <tr class="row-10 even"> 
     <td class="column-1">Riley McGwin</td>
     <td class="column-2">6-3</td>
     <td class="column-3">Rochester, N.Y.</td>
     <td class="column-4">Irondequoit HS</td>
     <td class="column-5">2015</td> 
    </tr> 
    <tr class="row-11 odd"> 
     <td class="column-1">Isaiah Lewis</td>
     <td class="column-2">6-0</td>
     <td class="column-3">Chruchville, N.Y.</td>
     <td class="column-4">Churchville Chili</td>
     <td class="column-5">2015</td> 
    </tr> 
   </tbody> 
  </table> 
  <p><em><strong>Patriot League White 79, Patriot League Blue 69</strong></em></p> 
  <p>Patriot League White used a barrage of three-pointers, three from 2016 guard and MVP <strong>Michael Rice</strong>, to overcome their opponents 79-69. Rice scored 14 points for the winning side, while 2016 guard <strong>Ben Lee</strong> also hit three three-pointers en route to a 12 point performance. <a href="http://www.twitter.com/AndrewKoob">@AndrewKoob</a></p> 
  <p>&nbsp;</p> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>