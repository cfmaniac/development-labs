<article id="post-81295" class="post-81295 post type-post status-publish format-standard has-post-thumbnail hentry category-high-school-showcase-2 tag-alex-schachne tag-basketball-tournament tag-brad-bundusch tag-chris-paul tag-don-bosco-prep tag-east-orange tag-gil-st-bernards tag-high-school-basketball tag-immaculate-conception tag-jalen-carey tag-joe-jordan tag-manley-dorme tag-mike-morreale tag-new-jersey-basketball tag-newark-tech tag-nj-playaz tag-paul-mulcahy tag-playaz tag-preview tag-ron-harper tag-sports tag-vic-konopko"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p><strong>Game 1 2:30: Immaculate Conception Vs. East Orange</strong></p> 
  <p>Youre going to want to get to the NJ Tip-Off early cause the day starts off with a great matchup between Immaculate Conception and East Orange.&nbsp;East Orange, under 1st year coach and former Newark Tech coach (and state champion) Joe Jordan, looks to take down one of the best teams in New Jersey. Immaculate Conception features a player who many (Including myself) consider the best scorer in New Jersey, Jalen Carey! The 63 combo guard is capable of putting on a show against anyone! This is a matchup youre not going to want to miss!</p> 
  <p style="text-align: center;"><iframe src="//www.youtube.com/embed/UCyMAeBe2aI" width="560" height="314" allowfullscreen></iframe></p> 
  <h4 style="text-align: left;"></h4> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>