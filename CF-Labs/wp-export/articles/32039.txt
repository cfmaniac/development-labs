<article id="post-32039" class="post-32039 post type-post status-publish format-standard has-post-thumbnail hentry category-alumni-updates"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p>Countless Hoop Group alum have succeeded at many different levels, from the NBA, Division 1 as well as small colleges and in high school. On this page we look at some NBA players who are Hoop Group Alumni&nbsp;that earned honors this season .</p> 
  <p>The NBA announced some more post-season awards this week with their All Rookie Teams and All Defensive Teams and a few Hoop Group Alumni were selected.</p> 
  <table id="tablepress-451" class="tablepress tablepress-id-451"> 
   <thead> 
    <tr class="row-1 odd"> 
     <th class="column-1">&nbsp;</th>
     <th class="column-2">Name</th>
     <th class="column-3">Honor</th>
     <th class="column-4">Team</th> 
    </tr> 
   </thead> 
   <tbody> 
    <tr class="row-2 even"> 
     <td class="column-1"></td>
     <td class="column-2">Joakim Noah</td>
     <td class="column-3">All Defensive 1st Team</td>
     <td class="column-4">Chicago Bulls</td> 
    </tr> 
    <tr class="row-3 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2011/01/Chris-Paul-100.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-1254"></td>
     <td class="column-2">Chris Paul</td>
     <td class="column-3">All Defensive 2nd Team</td>
     <td class="column-4">Los Angeles Clippers</td> 
    </tr> 
    <tr class="row-4 even"> 
     <td class="column-1"></td>
     <td class="column-2">Paul George</td>
     <td class="column-3">All Defensive 2nd Team</td>
     <td class="column-4">Indiana Pacers</td> 
    </tr> 
    <tr class="row-5 odd"> 
     <td class="column-1"></td>
     <td class="column-2">Damian Lillard</td>
     <td class="column-3">All Rookie 1st Team</td>
     <td class="column-4">Portland Trail Blazers</td> 
    </tr> 
    <tr class="row-6 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2012/03/DionWaiters100.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-18827"></td>
     <td class="column-2">Dion Waiters</td>
     <td class="column-3">All Rookie 1st Team</td>
     <td class="column-4">Cleveland Cavaliers</td> 
    </tr> 
    <tr class="row-7 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2012/03/AndreDrummond100.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-18923"></td>
     <td class="column-2">Andre Drummond</td>
     <td class="column-3">All Rookie 2nd Team</td>
     <td class="column-4">Detroit Pistons</td> 
    </tr> 
    <tr class="row-8 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2011/03/MikeGilchrist100.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-8596"></td>
     <td class="column-2">Michael-Kidd Gilchrist</td>
     <td class="column-3">All Rookie 2nd Team</td>
     <td class="column-4">Charlotte Bobcats</td> 
    </tr> 
   </tbody> 
  </table> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>