<article id="post-47870" class="post-47870 post type-post status-publish format-standard has-post-thumbnail hentry category-alumni-updates"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p><img src="https://hoopgroup.com/wp-content/uploads/2014/03/Daniel-Aronowitz.jpg" alt="Daniel Aronowitz" width="460" height="306" class="aligncenter size-full wp-image-47871"><br> Daniel Aronowitz of Williams</p> 
  <p>Dreams Do Come True</p> 
  <p>Many Hoop Group alum have succeeded at many different levels, from the NBA, Division 1 as well as small colleges and in high school. On this page we look at some of the Hoop Group alumni excelling at the college level.</p> 
  <p>This weekend the Division 3 National Tournament heads to their Final Four. Close to 20 Hoop Group Alumni will be participating. Congratulations and best wishes to all.</p> 
  <p>Hoop Group Alums in NCAA Division 3 Final Four 2014<br> </p>
  <table id="tablepress-576" class="tablepress tablepress-id-576"> 
   <thead> 
    <tr class="row-1 odd"> 
     <th class="column-1">&nbsp;</th>
     <th class="column-2">Name</th>
     <th class="column-3">College</th> 
    </tr> 
   </thead> 
   <tbody> 
    <tr class="row-2 even"> 
     <td class="column-1"></td>
     <td class="column-2">Eric Dortch</td>
     <td class="column-3">Illinois Weleyan</td> 
    </tr> 
    <tr class="row-3 odd"> 
     <td class="column-1"></td>
     <td class="column-2">Mike Greenman</td>
     <td class="column-3">Williams</td> 
    </tr> 
    <tr class="row-4 even"> 
     <td class="column-1"></td>
     <td class="column-2">Taylor Epley</td>
     <td class="column-3">Williams</td> 
    </tr> 
    <tr class="row-5 odd"> 
     <td class="column-1"></td>
     <td class="column-2">Daniel Aronowitz</td>
     <td class="column-3">Williams</td> 
    </tr> 
    <tr class="row-6 even"> 
     <td class="column-1"></td>
     <td class="column-2">Ryan Kilcullen</td>
     <td class="column-3">Williams</td> 
    </tr> 
    <tr class="row-7 odd"> 
     <td class="column-1"></td>
     <td class="column-2">Greg Payton</td>
     <td class="column-3">Williams</td> 
    </tr> 
    <tr class="row-8 even"> 
     <td class="column-1"></td>
     <td class="column-2">Michael Mayer</td>
     <td class="column-3">Williams</td> 
    </tr> 
    <tr class="row-9 odd"> 
     <td class="column-1"></td>
     <td class="column-2">Christien Wright</td>
     <td class="column-3">Amherst</td> 
    </tr> 
    <tr class="row-10 even"> 
     <td class="column-1"></td>
     <td class="column-2">Aaron Toomey</td>
     <td class="column-3">Amherst</td> 
    </tr> 
    <tr class="row-11 odd"> 
     <td class="column-1"></td>
     <td class="column-2">Connor Gach</td>
     <td class="column-3">Amherst</td> 
    </tr> 
    <tr class="row-12 even"> 
     <td class="column-1"></td>
     <td class="column-2">David Kalema</td>
     <td class="column-3">Amherst</td> 
    </tr> 
    <tr class="row-13 odd"> 
     <td class="column-1"></td>
     <td class="column-2">Brady Holding</td>
     <td class="column-3">Amherst</td> 
    </tr> 
    <tr class="row-14 even"> 
     <td class="column-1"></td>
     <td class="column-2">Jeff Racy</td>
     <td class="column-3">Amherst</td> 
    </tr> 
    <tr class="row-15 odd"> 
     <td class="column-1"></td>
     <td class="column-2">Tom Killian</td>
     <td class="column-3">Amherst</td> 
    </tr> 
    <tr class="row-16 even"> 
     <td class="column-1"></td>
     <td class="column-2">Connor Green</td>
     <td class="column-3">Amherst</td> 
    </tr> 
    <tr class="row-17 odd"> 
     <td class="column-1"></td>
     <td class="column-2">Ben Pollack</td>
     <td class="column-3">Amherst</td> 
    </tr> 
    <tr class="row-18 even"> 
     <td class="column-1"></td>
     <td class="column-2">Joseph Mussachia</td>
     <td class="column-3">Amherst</td> 
    </tr> 
    <tr class="row-19 odd"> 
     <td class="column-1"></td>
     <td class="column-2">Alex Levine</td>
     <td class="column-3">Amherst</td> 
    </tr> 
    <tr class="row-20 even"> 
     <td class="column-1"></td>
     <td class="column-2">David George</td>
     <td class="column-3">Amherst</td> 
    </tr> 
   </tbody> 
  </table> 
  <p></p> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>