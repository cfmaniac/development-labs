<article id="post-34793" class="post-34793 post type-post status-publish format-standard has-post-thumbnail hentry category-elite-camp-updates tag-hoop-group-elite-camp"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <h3><a href="https://hoopgroup.com/wp-content/uploads/2013/07/East-White-Stars-e1374892491820.jpg"><img class="aligncenter size-full wp-image-42547" alt="East White Stars" src="https://hoopgroup.com/wp-content/uploads/2013/07/East-White-Stars-e1374892491820.jpg" width="600" height="450"></a>NBA East White All-Stars</h3> 
  <table id="tablepress-488" class="tablepress tablepress-id-488"> 
   <thead> 
    <tr class="row-1 odd"> 
     <th class="column-1">Name</th>
     <th class="column-2">Height</th>
     <th class="column-3">City, State</th>
     <th class="column-4">School</th>
     <th class="column-5">Graduation Year</th> 
    </tr> 
   </thead> 
   <tbody> 
    <tr class="row-2 even"> 
     <td class="column-1">Matt Zignorski</td>
     <td class="column-2">6-2</td>
     <td class="column-3">Sparta, N.J.</td>
     <td class="column-4">Pope John XXIII</td>
     <td class="column-5">2015</td> 
    </tr> 
    <tr class="row-3 odd"> 
     <td class="column-1">Tyler Major</td>
     <td class="column-2">6-1</td>
     <td class="column-3">Durham, N.C.</td>
     <td class="column-4">Mount Zion</td>
     <td class="column-5">2013</td> 
    </tr> 
    <tr class="row-4 even"> 
     <td class="column-1">Chieck Sy-savane</td>
     <td class="column-2">6-3</td>
     <td class="column-3">Centereach, N.J.</td>
     <td class="column-4">Our Savior New American</td>
     <td class="column-5">2014</td> 
    </tr> 
    <tr class="row-5 odd"> 
     <td class="column-1">Darian Dixon</td>
     <td class="column-2">6-5</td>
     <td class="column-3">Forestville, Md.</td>
     <td class="column-4">Bishop McNamara</td>
     <td class="column-5">2016</td> 
    </tr> 
    <tr class="row-6 even"> 
     <td class="column-1">Samuel Eckstrom</td>
     <td class="column-2">6-7</td>
     <td class="column-3">Olean, N.Y.</td>
     <td class="column-4">Olean</td>
     <td class="column-5">2014</td> 
    </tr> 
    <tr class="row-7 odd"> 
     <td class="column-1">Jordan Forehand</td>
     <td class="column-2">6-2</td>
     <td class="column-3">Jersey City, N.J.</td>
     <td class="column-4">St. Benedicts</td>
     <td class="column-5">2014</td> 
    </tr> 
    <tr class="row-8 even"> 
     <td class="column-1">Gemil Holbrook</td>
     <td class="column-2">6-4</td>
     <td class="column-3">Philadelphia, Pa.</td>
     <td class="column-4">Roman Catholic</td>
     <td class="column-5">2015</td> 
    </tr> 
    <tr class="row-9 odd"> 
     <td class="column-1">Brandon Alston</td>
     <td class="column-2">6-5</td>
     <td class="column-3">Newark, N.J.</td>
     <td class="column-4">St. Benedicts</td>
     <td class="column-5">2015</td> 
    </tr> 
    <tr class="row-10 even"> 
     <td class="column-1">Michael Silverthorn</td>
     <td class="column-2">6-4</td>
     <td class="column-3">Blairstown, N.J.</td>
     <td class="column-4">Blairstown</td>
     <td class="column-5">2014</td> 
    </tr> 
    <tr class="row-11 odd"> 
     <td class="column-1">Michael Tertsea</td>
     <td class="column-2">6-10</td>
     <td class="column-3">Bel Air, Md.</td>
     <td class="column-4">Archbishop John Carroll</td>
     <td class="column-5">2016</td> 
    </tr> 
   </tbody> 
  </table> 
  <h3><a href="https://hoopgroup.com/wp-content/uploads/2013/07/East-Blue-Stars-e1374892558318.jpg"><img class="aligncenter size-full wp-image-42546" alt="East Blue Stars" src="https://hoopgroup.com/wp-content/uploads/2013/07/East-Blue-Stars-e1374892558318.jpg" width="600" height="450"></a>NBA East Blue All-Stars</h3> 
  <p> </p>
  <table id="tablepress-489" class="tablepress tablepress-id-489"> 
   <thead> 
    <tr class="row-1 odd"> 
     <th class="column-1">Name</th>
     <th class="column-2">Height</th>
     <th class="column-3">City, State</th>
     <th class="column-4">School</th>
     <th class="column-5">Graduation Year</th> 
    </tr> 
   </thead> 
   <tbody> 
    <tr class="row-2 even"> 
     <td class="column-1">Marquis Powell</td>
     <td class="column-2">6-0</td>
     <td class="column-3">Upper Marlboro, Md.</td>
     <td class="column-4">Riverdale Baptist</td>
     <td class="column-5">2013</td> 
    </tr> 
    <tr class="row-3 odd"> 
     <td class="column-1">Caleb Mercer</td>
     <td class="column-2">6-3</td>
     <td class="column-3">Concord, N.C.</td>
     <td class="column-4">First Assembly Christian School</td>
     <td class="column-5">2014</td> 
    </tr> 
    <tr class="row-4 even"> 
     <td class="column-1">Brandon Robinson</td>
     <td class="column-2">6-5</td>
     <td class="column-3">Montreal, Can. </td>
     <td class="column-4">Dawson College Prep</td>
     <td class="column-5">2014</td> 
    </tr> 
    <tr class="row-5 odd"> 
     <td class="column-1">Curtis Hyman</td>
     <td class="column-2">6-5</td>
     <td class="column-3">New Britain, Conn.</td>
     <td class="column-4">New Britain</td>
     <td class="column-5">2014</td> 
    </tr> 
    <tr class="row-6 even"> 
     <td class="column-1">Kevin Marfo</td>
     <td class="column-2">6-6</td>
     <td class="column-3">Oradell, N.J.</td>
     <td class="column-4">Bergen Catholic</td>
     <td class="column-5">2015</td> 
    </tr> 
    <tr class="row-7 odd"> 
     <td class="column-1">Aaron Briggs</td>
     <td class="column-2">5-11</td>
     <td class="column-3">Potoman, Md.</td>
     <td class="column-4">Bullis</td>
     <td class="column-5">2014</td> 
    </tr> 
    <tr class="row-8 even"> 
     <td class="column-1">Romello Walker</td>
     <td class="column-2">6-6</td>
     <td class="column-3">Kent, Conn.</td>
     <td class="column-4">South Kent</td>
     <td class="column-5">2013</td> 
    </tr> 
    <tr class="row-9 odd"> 
     <td class="column-1">Marcel Thompson</td>
     <td class="column-2">6-4</td>
     <td class="column-3">Brooklandville, Md.</td>
     <td class="column-4">Poly</td>
     <td class="column-5">2015</td> 
    </tr> 
    <tr class="row-10 even"> 
     <td class="column-1">Nikola Maric</td>
     <td class="column-2">6-6</td>
     <td class="column-3">Prijepolje, Serb.</td>
     <td class="column-4"></td>
     <td class="column-5">2013</td> 
    </tr> 
    <tr class="row-11 odd"> 
     <td class="column-1">Lionel Owona</td>
     <td class="column-2">6-7</td>
     <td class="column-3">Bel Air, Md.</td>
     <td class="column-4">Archbishop John Carroll</td>
     <td class="column-5">2014</td> 
    </tr> 
   </tbody> 
  </table> 
  <br> 
  <em><strong>NBA East White 68, NBA East Blue 60</strong></em>
  <br> The NBA East All-Star White squad used a big run in the first quarter to keep the All-Star Blue squad at bay for the rest of the game, winning in a 68-60 contest.
  <p></p> 
  <p>An 18-3 run, paced by forward Samuel Eckstrom (eight points in the first quarter) and guard Matt Zignorski (six points in the quarter) gave the White All-Stars enough of a cushion to withstand a 12-3 run by the Blue All-Stars in the second quarter.</p> 
  <p>Zignorski, who contributed in a number of ways with a game-high 18 points, 10 assists and three rebounds, was named the All-Star game Most Valuable Player. Forwards Romello Walker and Nikola Maric were the leading scorers for the Blue All-Stars, as they both contributed ten points. <a href="http://www.twitter.com/AndrewKoob">@AndrewKoob</a></p> 
  <h3><a href="https://hoopgroup.com/wp-content/uploads/2013/07/IMG_0107-e1374892617332.jpg"><img class="aligncenter size-full wp-image-42533" alt="IMG_0107" src="https://hoopgroup.com/wp-content/uploads/2013/07/IMG_0107-e1374892617332.jpg" width="600" height="450"></a>NBA West White All-Stars</h3> 
  <table id="tablepress-490" class="tablepress tablepress-id-490"> 
   <thead> 
    <tr class="row-1 odd"> 
     <th class="column-1">Name</th>
     <th class="column-2">Height</th>
     <th class="column-3">City, State</th>
     <th class="column-4">School</th>
     <th class="column-5">Graduation Year</th> 
    </tr> 
   </thead> 
   <tbody> 
    <tr class="row-2 even"> 
     <td class="column-1">Dante Hatem</td>
     <td class="column-2">6-0</td>
     <td class="column-3">Syracuse, N.Y.</td>
     <td class="column-4">Christian Brothers Academy</td>
     <td class="column-5">2014</td> 
    </tr> 
    <tr class="row-3 odd"> 
     <td class="column-1">Tyralle Jordan</td>
     <td class="column-2">5-8</td>
     <td class="column-3">Bergenfield, N.J.</td>
     <td class="column-4">Bergenfield High School</td>
     <td class="column-5">2014</td> 
    </tr> 
    <tr class="row-4 even"> 
     <td class="column-1">Thai Scott</td>
     <td class="column-2">5-8</td>
     <td class="column-3">North Arlington, N.J.</td>
     <td class="column-4">North Arlington High School</td>
     <td class="column-5">2014</td> 
    </tr> 
    <tr class="row-5 odd"> 
     <td class="column-1">Evan Lester</td>
     <td class="column-2">6-5</td>
     <td class="column-3">Washington, D.C.</td>
     <td class="column-4">Archbishop Carroll</td>
     <td class="column-5">2013</td> 
    </tr> 
    <tr class="row-6 even"> 
     <td class="column-1">Jeremy Del Valle</td>
     <td class="column-2">6-2</td>
     <td class="column-3">Hackettstown, N.J.</td>
     <td class="column-4">Hackettstown High School</td>
     <td class="column-5">2015</td> 
    </tr> 
    <tr class="row-7 odd"> 
     <td class="column-1">R.J. Laugand IV</td>
     <td class="column-2">5-10</td>
     <td class="column-3">Clarion Township, Pa.</td>
     <td class="column-4">Clarion-Limestone High School</td>
     <td class="column-5">2015</td> 
    </tr> 
    <tr class="row-8 even"> 
     <td class="column-1">Shawn Ajavon</td>
     <td class="column-2">6-2</td>
     <td class="column-3">Jersey City, N.J.</td>
     <td class="column-4">Hudson Catholic</td>
     <td class="column-5">2015</td> 
    </tr> 
    <tr class="row-9 odd"> 
     <td class="column-1">Nicholas Colon</td>
     <td class="column-2">5-7</td>
     <td class="column-3">Brooklyn, N.Y.</td>
     <td class="column-4">Teachers Prep</td>
     <td class="column-5">2014</td> 
    </tr> 
    <tr class="row-10 even"> 
     <td class="column-1">Sean McKinless</td>
     <td class="column-2">6-1</td>
     <td class="column-3">Montvale, N.J.</td>
     <td class="column-4">St. Joseph's Regional</td>
     <td class="column-5">2015</td> 
    </tr> 
    <tr class="row-11 odd"> 
     <td class="column-1">Alexander Simpson</td>
     <td class="column-2">6-0</td>
     <td class="column-3">Ardmore, Pa.</td>
     <td class="column-4">Harriton</td>
     <td class="column-5">2014</td> 
    </tr> 
    <tr class="row-12 even"> 
     <td class="column-1">Cory Hicks</td>
     <td class="column-2">6-0</td>
     <td class="column-3">Romulus, Mich.</td>
     <td class="column-4">Summit Academy North</td>
     <td class="column-5">2015</td> 
    </tr> 
   </tbody> 
  </table> 
  <h3><a href="https://hoopgroup.com/wp-content/uploads/2013/07/West-Blue-Stars.jpg"><img class="aligncenter size-full wp-image-42577" alt="West Blue Stars" src="https://hoopgroup.com/wp-content/uploads/2013/07/West-Blue-Stars-e1374894250698.jpg" width="600" height="450"></a>NBA West Blue All-Stars</h3> 
  <p> </p>
  <table id="tablepress-491" class="tablepress tablepress-id-491"> 
   <thead> 
    <tr class="row-1 odd"> 
     <th class="column-1">Name</th>
     <th class="column-2">Height</th>
     <th class="column-3">City, State</th>
     <th class="column-4">School</th>
     <th class="column-5">Graduation Year</th> 
    </tr> 
   </thead> 
   <tbody> 
    <tr class="row-2 even"> 
     <td class="column-1">Ryan Keller</td>
     <td class="column-2">6-2</td>
     <td class="column-3">Bangor, Pa.</td>
     <td class="column-4">Pius X</td>
     <td class="column-5">2014</td> 
    </tr> 
    <tr class="row-3 odd"> 
     <td class="column-1">Rich Benoit</td>
     <td class="column-2">6-3</td>
     <td class="column-3">Stamford, Conn.</td>
     <td class="column-4">Stamford High School</td>
     <td class="column-5">2015</td> 
    </tr> 
    <tr class="row-4 even"> 
     <td class="column-1">Eric Young</td>
     <td class="column-2">6-0</td>
     <td class="column-3">Wilbraham, Mass.</td>
     <td class="column-4">Wilbraham &amp; Monson Academy </td>
     <td class="column-5">2015</td> 
    </tr> 
    <tr class="row-5 odd"> 
     <td class="column-1">David Ruiz</td>
     <td class="column-2">5-11</td>
     <td class="column-3">Spain</td>
     <td class="column-4"></td>
     <td class="column-5">2014</td> 
    </tr> 
    <tr class="row-6 even"> 
     <td class="column-1">Christian Mitchell</td>
     <td class="column-2">6-0</td>
     <td class="column-3">Lancaster, Pa.</td>
     <td class="column-4">Manheim Township High School</td>
     <td class="column-5">2015</td> 
    </tr> 
    <tr class="row-7 odd"> 
     <td class="column-1">Norris Brown</td>
     <td class="column-2">6-0</td>
     <td class="column-3">Colts Neck, N.J.</td>
     <td class="column-4">Colts Neck High School</td>
     <td class="column-5">2014</td> 
    </tr> 
    <tr class="row-8 even"> 
     <td class="column-1">Logan Weathers</td>
     <td class="column-2">6-4</td>
     <td class="column-3">Kernersville, N.C.</td>
     <td class="column-4">Bishop McGuiness</td>
     <td class="column-5">2014</td> 
    </tr> 
    <tr class="row-9 odd"> 
     <td class="column-1">Michael Schroback</td>
     <td class="column-2">5-11</td>
     <td class="column-3">Hasbrouck Heights, N.J.</td>
     <td class="column-4">Hasbrouck heights High School</td>
     <td class="column-5">2014</td> 
    </tr> 
    <tr class="row-10 even"> 
     <td class="column-1">Isaac Mattson</td>
     <td class="column-2">6-2</td>
     <td class="column-3">Harbor Creek, Pa. </td>
     <td class="column-4">Harbor Creek High School</td>
     <td class="column-5">2014</td> 
    </tr> 
    <tr class="row-11 odd"> 
     <td class="column-1">Wyatt Smith</td>
     <td class="column-2">6-2</td>
     <td class="column-3">Irondequoit, N.Y.</td>
     <td class="column-4">West Irondequoit High School</td>
     <td class="column-5">2014</td> 
    </tr> 
    <tr class="row-12 even"> 
     <td class="column-1">Travis Alvarez</td>
     <td class="column-2">6-5</td>
     <td class="column-3">Keyport, N.J.</td>
     <td class="column-4">Keyport High School</td>
     <td class="column-5">2016</td> 
    </tr> 
   </tbody> 
  </table> 
  <br> 
  <em><strong>NBA West White 76, NBA West Blue 64</strong></em>
  <br> In the NBA West All-Star Game, the White team grabbed the lead from the opening tip, and never trailed, as they went on to defeat the Blue team 76-64.
  <p></p> 
  <p><strong>Jeremy Del Valle (Hackettstown (N.J.) High School/6-2/2015)</strong> copped MVP honors as he led the White team with 20 points.</p> 
  <p>It feels good knowing all the work (in camp this week) paid off, said Del Valle, and while you are improving your game, its nice to pick up an honor like this.</p> 
  <p>Also starring for the White team was <strong>Evan Lester (Archbishop Carroll (D.C.)/6-5/2014)</strong>, who finished with 10 points, and <strong>Sean McKinless (St. Josephs Regional (N.J.)/6-1/2015)</strong>, who added six.</p> 
  <p>After going into the half trailing 57-42, the Blue team pulled to within four thanks to some big 3-pointers from <strong>Eric Young (Wilbraham &amp; Monson (Mass.) Academy/6-0/2015)</strong>, who tallied 12 points in the loss, but never got any closer. <a href="https://twitter.com/pfebbraro">@pfebbraro</a></p> 
  <h3><a href="https://hoopgroup.com/wp-content/uploads/2013/07/FF-White-Stars-e1374892772127.jpg"><img class="aligncenter size-full wp-image-42557" alt="FF White Stars" src="https://hoopgroup.com/wp-content/uploads/2013/07/FF-White-Stars-e1374892772127.jpg" width="600" height="450"></a>Fab Frosh/Super Soph White All-Stars</h3> 
  <table id="tablepress-492" class="tablepress tablepress-id-492"> 
   <thead> 
    <tr class="row-1 odd"> 
     <th class="column-1">Name</th>
     <th class="column-2">Height</th>
     <th class="column-3">City, State</th>
     <th class="column-4">School</th>
     <th class="column-5">Graduation Year</th> 
    </tr> 
   </thead> 
   <tbody> 
    <tr class="row-2 even"> 
     <td class="column-1">Marley Blommers</td>
     <td class="column-2">6-3</td>
     <td class="column-3">Chatham, Ontario</td>
     <td class="column-4">Chatham Kent</td>
     <td class="column-5">2016</td> 
    </tr> 
    <tr class="row-3 odd"> 
     <td class="column-1">Derek Ford</td>
     <td class="column-2">6-4</td>
     <td class="column-3">Camp Hill, Pa.</td>
     <td class="column-4">Cedar Cliff High School</td>
     <td class="column-5">2016</td> 
    </tr> 
    <tr class="row-4 even"> 
     <td class="column-1">Ryan O'Neil</td>
     <td class="column-2">5-8</td>
     <td class="column-3">Pelham, N.Y.</td>
     <td class="column-4">Pelham Memorial High School</td>
     <td class="column-5">2016</td> 
    </tr> 
    <tr class="row-5 odd"> 
     <td class="column-1">Corey Romich</td>
     <td class="column-2">6-4</td>
     <td class="column-3">Groton, Mass.</td>
     <td class="column-4">Groton-Dunstable High School</td>
     <td class="column-5">2016</td> 
    </tr> 
    <tr class="row-6 even"> 
     <td class="column-1">Chris Whitaker</td>
     <td class="column-2">5-7</td>
     <td class="column-3">Dauphin, Pa.</td>
     <td class="column-4">Central Dauphin East High School</td>
     <td class="column-5">2017</td> 
    </tr> 
    <tr class="row-7 odd"> 
     <td class="column-1">Michael Caruso</td>
     <td class="column-2">5-7</td>
     <td class="column-3">Rumson, N.J.</td>
     <td class="column-4">Rumson High School</td>
     <td class="column-5">2016</td> 
    </tr> 
    <tr class="row-8 even"> 
     <td class="column-1">Ivory Davis</td>
     <td class="column-2">5-10</td>
     <td class="column-3">Washington, D.C.</td>
     <td class="column-4">Bell Multicultural</td>
     <td class="column-5">2016</td> 
    </tr> 
    <tr class="row-9 odd"> 
     <td class="column-1">Parker Hess</td>
     <td class="column-2">5-10</td>
     <td class="column-3">Westfield, N.J.</td>
     <td class="column-4">Westfield High School</td>
     <td class="column-5">2016</td> 
    </tr> 
    <tr class="row-10 even"> 
     <td class="column-1">Alexander Karamanos</td>
     <td class="column-2">6-2</td>
     <td class="column-3">Kingston, N.Y.</td>
     <td class="column-4">Kingston High School</td>
     <td class="column-5">2016</td> 
    </tr> 
    <tr class="row-11 odd"> 
     <td class="column-1">Montrel Morgan</td>
     <td class="column-2">6-0</td>
     <td class="column-3">York, Pa.</td>
     <td class="column-4">New Hope Academy Charter</td>
     <td class="column-5">2016</td> 
    </tr> 
   </tbody> 
  </table> 
  <h3><a href="https://hoopgroup.com/wp-content/uploads/2013/07/FF-Blue-Stars.jpg"><img class="aligncenter size-full wp-image-42556" alt="FF Blue Stars" src="https://hoopgroup.com/wp-content/uploads/2013/07/FF-Blue-Stars-e1374892793368.jpg" width="600" height="450"></a>Fab Frosh/Super Soph Blue All-Stars</h3> 
  <p> </p>
  <table id="tablepress-493" class="tablepress tablepress-id-493"> 
   <thead> 
    <tr class="row-1 odd"> 
     <th class="column-1">Name</th>
     <th class="column-2">Height</th>
     <th class="column-3">City, State</th>
     <th class="column-4">School</th>
     <th class="column-5">Graduation Year</th> 
    </tr> 
   </thead> 
   <tbody> 
    <tr class="row-2 even"> 
     <td class="column-1">Jordan Guzman</td>
     <td class="column-2">5-9</td>
     <td class="column-3">Brooklyn, N.Y.</td>
     <td class="column-4">Xaverian High School</td>
     <td class="column-5">2016</td> 
    </tr> 
    <tr class="row-3 odd"> 
     <td class="column-1">Conor Peterson</td>
     <td class="column-2">6-1</td>
     <td class="column-3">New York, N.Y.</td>
     <td class="column-4">Regis</td>
     <td class="column-5">2016</td> 
    </tr> 
    <tr class="row-4 even"> 
     <td class="column-1">John Turner</td>
     <td class="column-2">6-2</td>
     <td class="column-3">Bowie, Md.</td>
     <td class="column-4">Archbishop Carroll</td>
     <td class="column-5">2016</td> 
    </tr> 
    <tr class="row-5 odd"> 
     <td class="column-1">Matthew DeWolf</td>
     <td class="column-2">6-7</td>
     <td class="column-3">Barrington, R.I.</td>
     <td class="column-4">Barrington High School</td>
     <td class="column-5">2016</td> 
    </tr> 
    <tr class="row-6 even"> 
     <td class="column-1">Steven Ferraro</td>
     <td class="column-2">5-11</td>
     <td class="column-3">West Orange, N.J.</td>
     <td class="column-4">Seton Hall Prep</td>
     <td class="column-5">2016</td> 
    </tr> 
    <tr class="row-7 odd"> 
     <td class="column-1">Everette Hammond</td>
     <td class="column-2">6-1</td>
     <td class="column-3">Silver Spring, Md.</td>
     <td class="column-4">Palotti</td>
     <td class="column-5">2017</td> 
    </tr> 
    <tr class="row-8 even"> 
     <td class="column-1">Mikey Dixon</td>
     <td class="column-2">5-10</td>
     <td class="column-3">Wilmington, De.</td>
     <td class="column-4">Sanford</td>
     <td class="column-5">2016</td> 
    </tr> 
    <tr class="row-9 odd"> 
     <td class="column-1">Damon Stern</td>
     <td class="column-2">5-10</td>
     <td class="column-3">Reading, Pa.</td>
     <td class="column-4">Reading High School</td>
     <td class="column-5">2016</td> 
    </tr> 
    <tr class="row-10 even"> 
     <td class="column-1">Sean Hoggs</td>
     <td class="column-2">6-0</td>
     <td class="column-3">Trenton, N.J.</td>
     <td class="column-4">Notre Dame</td>
     <td class="column-5">2016</td> 
    </tr> 
    <tr class="row-11 odd"> 
     <td class="column-1">Patrick Jamison</td>
     <td class="column-2">5-8</td>
     <td class="column-3">West Orange, N.J.</td>
     <td class="column-4">Seton Hall Prep</td>
     <td class="column-5">2016</td> 
    </tr> 
   </tbody> 
  </table> 
  <br> 
  <em><strong>Fab Frosh/Super Soph White 79, Fab Frosh/Super Soph Blue 75</strong></em>
  <p></p> 
  <p>The Fab Freshman/Super Sophomore All-Star game was a competitive exhibition match-up between some of Elite 3s top rising ninth and tenth graders. In the end, the White All-Stars defeated the Blue All-Stars by a final score of 79-75.</p> 
  <p><strong>Parker Hess (Westfield, N.J. 2016)</strong> had quite the performance from beyond the arc finishing with 14 points including four three-pointers for the White All-Stars. Hess hot shooting earned him the Fab Freshman/Super Sophomore All-Star game MVP.</p> 
  <p>6-foot-7 big man M<strong>atthew Dewolf (Barrington, R.I. 2016)</strong> paced the Blue All-Stars with a game-high 16 points while <strong>Mikey Dixon (Sanford, De. 2014)</strong> had a nice outing as well with 11 points. <a href="https://twitter.com/CabWashington">@CabWashington</a></p> 
  <p>&nbsp;</p> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>