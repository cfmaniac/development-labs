<article id="post-63198" class="post-63198 post type-post status-publish format-standard has-post-thumbnail hentry category-tournament-recap"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p class="p1"><img class="aligncenter size-full wp-image-63203" src="https://hoopgroup.com/wp-content/uploads/2015/07/11745489_927187657345766_153080099579233400_n.jpg" alt="11745489_927187657345766_153080099579233400_n" width="600" height="300"></p> 
  <p class="p1">Summer Jam Fest started with a bang Friday night. Right in the middle of the live recruiting period, Summer Jam Fest saw <a href="http://hoopgroup.com/summer-jam-fest-college-coaches-list/" target="_blank">hundreds of coaches come to Spooky</a> to recruit, including some Division 1 head coaches. Here are a few guys who looked great Friday.</p> 
  <h4 class="p1"><span class="s1">Blake Pagon; Gainesville Elite; 2016</span></h4> 
  <p class="p1"><span class="s1">&nbsp;</span><span class="s1">He came in with a blue collar attitude, and his hard work paid off in a big way. Despite losing a tough one to a good Diamond State Titans team, Pagon was fantastic for Gainesville scoring 28 points and 16 points in their two games. He was the hardest working kid on the court, and his mix of outside shooting ability and craftiness around the basket made him tough to guard.</span></p> 
  <h4 class="p1"><span class="s1">Trey Shifflett; York Ballers; 2016</span></h4> 
  <p class="p1"><span class="s1">&nbsp;</span><span class="s1">He was a menace defensively, turning steals into easy lay ups on the other end. Shifflett has lightning quick hands defensively, and on the offensive end he was efficient all game. He finished with 29 points, including four triples in their first win.</span></p> 
  <h4 class="p1"><span class="s1">EJ Crawford; USAD; 2016</span></h4> 
  <p class="p1"><span class="s1">&nbsp;</span><span class="s1">The big 65 wing out of St. Thomas showed a terrific arsenal on the offensive end, attacking hard off the wing and scoring from all three levels. He used his frame and well in transition, finishing both through and over his defenders. Crawford was one of the most polished scorers we saw, and finished with 25 points in their win.</span></p> 
  <h4 class="p1">Alex Nunnally; Southern Maryland All Stars; 2015</h4> 
  <p class="p1"><span class="s1">Another big wing, Nunnally played with a chip on his shoulder all game and had his way against South Jersey Select. He played hard, attacked from the high post, and shot it well in the mid-range. Hes also versatile, playing the three different positions from the 2 through the 4.</span><span class="s1">&nbsp;</span></p> 
  <h4 class="p1"><span class="s1">Kameron Hedgepath; Higher Level; 2016</span></h4> 
  <p class="p1"><span class="s1">Hes heading into a postgrad year at Hargrave Military Academy, and the Virginia wing showed his grit and scoring ability in their first day. They lost a physical and close matchup with Team Final Black, but Hedgepath matched the physicality attacking defenders hard on the drive. Hes a 62 combo guard of sorts who can create for himself by slashing off the wing.</span><span class="s1">&nbsp;</span></p> 
  <h4 class="p1"><span class="s1">Jagan Mosely; NJ Playaz Basketball Club; 2016</span></h4> 
  <p class="p1"><span class="s1">The St. Anthonys lead guard was terrific in their dogfight of a win over Positive Image. Mosely set the tone on both ends, hounding ball handlers the length of the court while controlling the pace on the offensive end. The 63 point is a terrific ball handler that keeps everyone involved but is able to get a bucket at will when needed.</span></p> 
  <h4 class="p1"><span class="s1">Nathan Davis; PA Slam; 2016</span></h4> 
  <p class="p1"><span class="s1">Davis was an assassin for the Slam, shooting it well from both deep and the midrange. He doesnt need to dominate the basketball, but is able to get his own off the bounce when he needs to. He was efficient, played within the system and moved well without the ball on his way to a 26 point outing.</span></p> 
  <h4 class="p1"><span class="s1">Nate Shafer; NOVA Cavs; 2016</span></h4> 
  <p class="p1"><span class="s1">This kid is the definition of toughness. While the Cavs had a tough time matching the size and athleticism of the Jersey Shore Warriors in their second game, the 64 Shafer patrolled the paint, swatting six shots and rebounding well against the Warriors front line of 67 and 68. Hes constantly matching up with larger players, and every division 3 coach in the DMV area is following the Cavs for Shafer and his teammates.</span></p> 
  <h4 class="p1"><span class="s1">Jay Barber; Team Loaded 434; 2017</span></h4> 
  <p class="p1"><span class="s1">Shooter alert! Barber, who usually plays with the Team Loaded VA 16s, is a knock down assassin from deep. He has in the gym range so you have to deny him as soon as he crosses half court. Still, he moves well without the ball, reads screens well, and showed some savvy off the bounce yesterday.</span></p> 
  <h4 class="p1"><span class="s1">Vinny Dalessandro; Jersey Shore Warriors; 2016</span></h4> 
  <p class="p1"><span class="s1">Coming off a terrific week at Hoop Group Elite 2, Dalessandro picked up right where he left off in their blowout win over the NOVA Cavs. Hes a huge 66 stretch four who can knock down the trail three and bang inside on the glass. He showed some surprisingly springs when coming down the middle too, and its just a matter of time before his first offer comes in.</span></p> 
  <p class="p1"><span class="s1"> </span></p> 
  <p class="p3"><strong><span class="s1"><i>Jack Herron</i></span></strong></p> 
  <p class="p3"><strong><span class="s1"><i>Vantage Hoops Analyst</i></span></strong></p> 
  <p class="p3"><strong><span class="s1"><i>Greater Richmond Area</i></span></strong></p> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>