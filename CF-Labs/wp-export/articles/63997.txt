<article id="post-63997" class="post-63997 post type-post status-publish format-standard has-post-thumbnail hentry category-alumni-updates category-player-feature tag-adam-finkelstein tag-christian-brothers-academy tag-jersey-shore-warriors tag-lehigh tag-pat-andree tag-steve-keller"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p><img class="aligncenter size-full wp-image-63998" src="https://hoopgroup.com/wp-content/uploads/2015/08/Pat-Andree.jpg" alt="Pat Andree" width="600" height="300"></p> 
  <p>Yesterday, Christian Brothers Academy forward <b>Pat Andree&nbsp;</b>committed to Lehigh University. After taking official visits to <em>Lehigh, William &amp; Mary and George Washington</em>, the 67 wing&nbsp;decided to call Lehigh home for his college career.&nbsp;The decision made Andree, who played AAU with the <a href="http://www.jerseyshorewarriors.com/" target="_blank" rel="noopener">Jersey Shore Warriors program</a>, the first commitment in Lehighs Class of 2016, and its a pretty solid foundation for a Mountain Hawk team that finished 16-14 last season.</p> 
  <p>The news adds a shooter&nbsp;to a team that shot just over 34% from three last season.&nbsp;But, while Andree is known for his shooting ability, ESPNs Adam Finkelstein points out that his game is more well rounded than most may think.</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck"> 
   <p dir="ltr" lang="en">Huge score last night for Lehigh in Pat Andree. Skilled 4 known for shot-making but handles &amp; passes just as well. Immediate impact in PL.</p> 
   <p> Adam Finkelstein (@AdamFinkelstein) <a href="https://twitter.com/AdamFinkelstein/status/638331554780024832">August 31, 2015</a></p>
  </blockquote> 
  <p><script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>As Finkelstein points out, Andree should be able to provide an immediate&nbsp;impact&nbsp;for a Lehigh team that finished third in the regular season in the Patriot League.&nbsp;And, as Steve Keller of the National Recruiting Report put it, he has the chance to be more than just an average player there.</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p>The commit of CBAs Pat Andree to Lehigh is absolute Grand Slam for the Mountain Hawks/The 67 Andree should be an All League star one day.</p> 
   <p> Steve Keller (@SteveKellerNRR) <a href="https://twitter.com/SteveKellerNRR/status/638189391605825537">August 31, 2015</a></p>
  </blockquote> 
  <p>Before he gets to Bethlehem however, Andree gears up for his senior season at <a href="https://www.cbalincroftnj.org" target="_blank" rel="noopener">Christian Brothers Academy</a>, located in Lincroft, New Jersey. As a junior, Andree helped lead the Colts to a State Sectional Championship and also reached a personal milestone, scoring his 1,000th career point.</p> 
  <p>The bar is set high for Andree and CBA this season, and we look forward to seeing them compete again this year during our <a href="http://hoopgroup.com/team-tournaments/highschoolshowcase/" target="_blank" rel="noopener">Hoop Group High School Showcase</a> series. Congratulations again to Pat!</p> 
  <p><script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script></p> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>