<article id="post-46721" class="post-46721 post type-post status-publish format-standard has-post-thumbnail hentry category-alumni-updates"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p>Dreams Do Come True</p> 
  <p>Many Hoop Group alum have succeeded at many different levels, from the NBA, Division 1 as well as small colleges and in high school. On this page we look at some of the Hoop Group alumni excelling at the highest level of basketball, the NBA.</p> 
  <p>The greatest of the NBA players are selected to showcase their skills during All Star Weekend. In addition to the NBA All Star game scheduled for Sunday, the weekend of the NBA All Star Game has become a bigger showcase of the skills of the top players with events all weekend.</p> 
  <p>A look at the Hoop Group Alumni in the NBA weekend Rising Stars game which features the best first and second year players in the league.</p> 
  <table id="tablepress-560" class="tablepress tablepress-id-560"> 
   <thead> 
    <tr class="row-1 odd"> 
     <th class="column-1">PHOTO</th>
     <th class="column-2">NAME</th>
     <th class="column-3">NBA TEAM</th>
     <th class="column-4">COLLEGE</th> 
    </tr> 
   </thead> 
   <tbody> 
    <tr class="row-2 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2014/02/MichaelCarterWilliams100.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-46613"></td>
     <td class="column-2">Michael Carter-Williams</td>
     <td class="column-3">76ers</td>
     <td class="column-4">Syracuse</td> 
    </tr> 
    <tr class="row-3 odd"> 
     <td class="column-1"><img src="http://hoopgroup.com/wp-content/uploads/2012/03/DionWaiters100.jpg" alt="" title="DionWaiters100" width="100" height="120" class="aligncenter size-full wp-image-18827"></td>
     <td class="column-2">Dion Waiters</td>
     <td class="column-3">Cavaliers</td>
     <td class="column-4">Syracuse</td> 
    </tr> 
    <tr class="row-4 even"> 
     <td class="column-1"><a href="http://hoopgroup.com/?attachment_id=46734" rel="attachment wp-att-46734"><img src="https://hoopgroup.com/wp-content/uploads/2014/02/hardawayjr-e1392232513478.jpg" alt="" width="100" height="102" class="aligncenter size-full wp-image-46734"></a></td>
     <td class="column-2">Tim Hardaway</td>
     <td class="column-3">Knicks</td>
     <td class="column-4">Michigan</td> 
    </tr> 
    <tr class="row-5 odd"> 
     <td class="column-1"><a href="http://hoopgroup.com/?attachment_id=46733" rel="attachment wp-att-46733"><img src="https://hoopgroup.com/wp-content/uploads/2014/02/8b0d68d5-bdb2-e211-a98a-002655e6c126_original-e1392230639188.jpg" alt="" width="100" height="136" class="aligncenter size-full wp-image-46733"></a></td>
     <td class="column-2">Damian Lillard</td>
     <td class="column-3">Trailblazers</td>
     <td class="column-4">Weber State</td> 
    </tr> 
    <tr class="row-6 even"> 
     <td class="column-1"><img src="http://hoopgroup.com/wp-content/uploads/2012/03/AndreDrummond100.jpg" alt="" title="AndreDrummond100" width="100" height="120" class="aligncenter size-full wp-image-18923"></td>
     <td class="column-2">Andre Drummond</td>
     <td class="column-3">Pistons</td>
     <td class="column-4">Connecticut</td> 
    </tr> 
    <tr class="row-7 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2013/04/TreyBurke100.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-29818"></td>
     <td class="column-2">Trey Burke</td>
     <td class="column-3">Jazz</td>
     <td class="column-4">Michigan</td> 
    </tr> 
    <tr class="row-8 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2012/02/jaredsullinger2100.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-15435"></td>
     <td class="column-2">Jared Sullinger</td>
     <td class="column-3">Celtics</td>
     <td class="column-4">Ohio State</td> 
    </tr> 
    <tr class="row-9 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2011/02/Mason-Plumlee-100.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-7187"></td>
     <td class="column-2">Mason Plumlee</td>
     <td class="column-3">Nets</td>
     <td class="column-4">Duke</td> 
    </tr> 
   </tbody> 
  </table> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>