<article id="post-13651" class="post-13651 post type-post status-publish format-standard has-post-thumbnail hentry category-tournament-recap tag-hgshowcase"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p><a href="http://hoopgroup.com/wp-content/uploads/2011/10/showcase.jpg"><img src="http://hoopgroup.com/wp-content/uploads/2011/10/showcase.jpg" alt="hoop group high school showcase" title="showcase" width="640" height="425" class="alignleft size-full wp-image-13665"></a></p> 
  <p>Hoop Group is hosting the high school showcase this weekend which features players from 20 of the top teams from New Jersey, New York, Pennsylvania, Delaware, and Maryland (the teams participating feature players from the high school not official teams). We will keep you updated throughout the day as results are happening. If there is something youre looking for comment below or tweet me @TheHoopGroup or @DaveKrupinski</p> 
  <p><strong>12:20 PM</strong> 1st game sets are underway with a lot of talent in the building already. South Shore from Brooklyn, NY are off to quick start putting up 30 points in the first half against Egg Harbor Township of southern New Jersey. </p> 
  <p>On the other court Lakewood (NJ) is taking on a strong Boys Latin team from Philadelphia. This game hasnt seen a lead extend more than 7 points with plenty of full court pressure.</p> 
  <p><strong>12:45</strong> Egg Harbor Township made a 2nd half surge but South Shore proved to be too strong winning the game 58-48.</p> 
  <p>Boys Latin edged Lakewood 61-55. Both teams guards went toe to toe as Lakewoods Jameer Jones and Boys Latins Maurice Watson highlighted the game.</p> 
  <p>Up next is a great match up between Gil St Benard vs Philadelphia Electrical Technology</p> 
  <p><strong>1:04</strong> Tyler Roberson is pacing Roselle Catholic (NJ) who is also getting major contributions from Isaiah Still and Steven Bush. They lead St. Edmonds (NY) 34-25 at the half.</p> 
  <p><strong>1:19</strong> Imhotep didnt waste any time in letting everyone know they are in this to win it. They hold a 34-16 lead at the half led by Brandon Austin with 8 points.</p> 
  <p><strong>2:30</strong> Gil St. Benards torched the nets all game long and never really gave Philadelphia Electrical Technology a chance to be in the game. GSB was led by Jaren Sina and Alex Mitola. Imhotep and Roselle Catholic both coasted to victory to advance in the winners bracket. </p> 
  <p><strong>4:15</strong> Gil St. Benards and Imhotep matched up in the 2nd round which was a match up that caught everyones eye on the bracket. The stands were filled to capacity and the balcony was filled with teams not playing watching on. </p> 
  <p>This game was over pretty quickly as Imhotep wasnt ready for the perimeter attack of GSB. GSG came out firing from deep and connected at near perfect percentages. The crowd erupted when Jaren Sina hit Dom Hoffman for a backdoor alleyoop in the 2nd half. (this play was caught on film check out <a href="http://youtube.com/thehoopgroup">Hoop Group YouTube</a> channel later)</p> 
  <p><strong>5:00</strong> Snow is really coming down strong in the North and Gil St. Benards pulls out of the tournament to travel safely back home. South Shore takes home the National Division championship. </p> 
  <p>Neptune took on local rival Colts Neck which attracted a lot of local fan fare. Neptune had a few players missing due to football but received some unexpected contributions to walk away with a confidence building win. </p> 
  <p>Check back tomorrow for Day 2 of the High School #HGShowcase</p> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>