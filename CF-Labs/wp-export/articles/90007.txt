<article id="post-90007" class="post-90007 post type-post status-publish format-standard has-post-thumbnail hentry category-headquarters-blog category-tournament-results"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <div id="attachment_65880" class="wp-caption aligncenter"> 
   <h3><strong>2015 Team Champions</strong></h3> 
   <a href="https://hoopgroup.com/wp-content/uploads/2015/07/unnamed-5.jpg"><img data-caption="13U Champs  Jersey Jaguars" class="aligncenter size-medium wp-image-65876" src="https://hoopgroup.com/wp-content/uploads/2015/07/unnamed-5-300x300.jpg" alt="13U Champs - Jersey Jaguars" width="300" height="300"></a>
   <p class="blog2html_p ">13U Champs  Jersey Jaguars</p> 
   <a href="https://hoopgroup.com/wp-content/uploads/2015/07/unnamed-4.jpg"><img data-caption="14U Champs  NJ Shoreshots" class="aligncenter size-medium wp-image-65875" src="https://hoopgroup.com/wp-content/uploads/2015/07/unnamed-4-300x300.jpg" alt="14U Champs - NJ Shoreshots" width="300" height="300"></a>
   <p class="blog2html_p ">14U Champs  NJ Shoreshots</p> 
   <a href="https://hoopgroup.com/wp-content/uploads/2015/07/unnamed-11.jpg"><img data-caption="15U Champs  NJ Shoreshots" class="aligncenter size-medium wp-image-65877" src="https://hoopgroup.com/wp-content/uploads/2015/07/unnamed-11-300x300.jpg" alt="15U Champs - NJ Shoreshots" width="300" height="300"></a>
   <p class="blog2html_p ">15U Champs  NJ Shoreshots</p> 
   <h3></h3> 
   <h3><strong>2015 Most Valuable Players</strong></h3> 
   <a href="https://hoopgroup.com/wp-content/uploads/2015/07/unnamed-2.jpg"><img data-caption="15U MVP  Eddie Davis" class="aligncenter size-medium wp-image-65878" src="https://hoopgroup.com/wp-content/uploads/2015/07/unnamed-2-300x300.jpg" alt="15U MVP - Eddie Davis" width="300" height="300"></a>
   <p class="blog2html_p ">15U MVP  Eddie Davis</p> 
   <a href="https://hoopgroup.com/wp-content/uploads/2015/07/unnamed-3.jpg"><img data-caption="14U MVP  Charlie Gordinier" class="aligncenter size-medium wp-image-65879" src="https://hoopgroup.com/wp-content/uploads/2015/07/unnamed-3-300x300.jpg" alt="14U MVP - Charlie Gordinier" width="300" height="300"></a>
   <p class="blog2html_p ">14U MVP  Charlie Gordinier</p> 
   <a href="https://hoopgroup.com/wp-content/uploads/2015/07/unnamed-6.jpg"><img data-caption="13U MVP  Colin Farell" class="aligncenter size-medium wp-image-65880" src="https://hoopgroup.com/wp-content/uploads/2015/07/unnamed-6-300x300.jpg" alt="13U MVP - Colin Farell" width="300" height="300"></a>
   <p class="blog2html_p ">13U MVP  Colin Farell</p> 
  </div> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>