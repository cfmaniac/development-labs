<article id="post-2633" class="post-2633 post type-post status-publish format-standard hentry category-tournament-recap tag-hgshowcase tag-boardwalk-showcase-2 tag-high-school-basketball"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p><span style="text-decoration: underline;"><strong>2011 Boardwalk Boys Recap </strong></span></p> 
  <p>This weekend was another great event for the Hoop Group. In the first game of the 11<sup>th</sup> annual Hoop Group Boardwalk Showcase, Jackson Memorial HS who defeated New Egypt HS 42 to 37. The two most valuable players was Connor Saker of Jackson Memorial HS and for New Egypt HS was Desmond Hubert.&nbsp;</p> 
  <p>In our second contest Lakewood HS competing against Middletown North HS. This game was very competitive being that Lakewood was without one of their star players Jarrod Davis. He was out do to an investigation of the New Jersey Shore Conference rules. Lakewood was still able to hold off Middletown North with the outstanding play of senior Tony Walker who led all scorers with 23 points, who was also the game MVP along side Shilique Calhoun of Middletown North. The final score of the contest was 66 to 63.</p> 
  <p>In the third game of the day, Monsignor Donovan HS took the victory over Point Pleasant Beach&nbsp; 61 to 51.The players of the game was split between Anthony Dusczack and Sean Grennan of Mon Don. For Point Pleasant Beach Jarelle Reischel was the MVP. All three players had impressive performances.</p> 
  <p>The forth game of the day CBA host red hot undefeated Lincoln of New York. CBA came out of the gate shooting the lights out of the basketball. While the Colts were hitting jump shot after jump shot, Lincolns own Shaquille Stokes had a great games as well. He finished up with 27 points while Matt McMullen of CBA finished the game with 14 points. Both were MVPS of the game.</p> 
  <p>In the last game of the day Neptune out scored Eastside HS of Newark NJ 61 to 43.The players of the game was Ike Calderon of Neptune and Kasim Chandler of Eastside. For more information about the this event visit our site, <a href="http://hoopgroup.com/hoopgroup/">www.HoopGroup.com</a></p> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>