<article id="post-80477" class="post-80477 post type-post status-publish format-standard has-post-thumbnail hentry category-alumni-updates"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p><img class="aligncenter size-full wp-image-57819" src="https://hoopgroup.com/wp-content/uploads/2015/02/Karl-Malone-award.png" alt="Karl Malone award" width="354" height="137"></p> 
  <p><img class="aligncenter size-full wp-image-80484" src="https://hoopgroup.com/wp-content/uploads/2016/11/CarltonBragg2.jpg" alt="" width="600" height="250"></p> 
  <p style="text-align: center;">Kansas star <strong>Carlton Bragg</strong>&nbsp;is among the Hoop Group alumni named to the Malone Award Watch List.</p> 
  <p>Dreams Do Come True</p> 
  <p>Many Hoop Group alum have succeeded at many different levels, from the NBA, Division 1 as well as small colleges and in high school. On this page we look at some of the Hoop Group alumni excelling at the college level.</p> 
  <p>Named after Hall of Famer and two-time NBA Most Valuable Player Karl Malone, the award recognizes the top power forwards in Division I mens college basketball. A national committee comprised of top college basketball personnel determined the watch list of 20&nbsp;candidates.</p> 
  <p>8 of the 20 players on the finalist list are Hoop Group Alums</p> 
  <p><strong>Hoop Group Alumni Finalists for Malone Award 2017</strong></p> 
  <table id="tablepress-1543" class="tablepress tablepress-id-1543"> 
   <thead> 
    <tr class="row-1 odd"> 
     <th class="column-1">&nbsp;</th>
     <th class="column-2">Name</th>
     <th class="column-3">College</th> 
    </tr> 
   </thead> 
   <tbody> 
    <tr class="row-2 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2016/11/JohnathanMotley100.jpg" alt="johnathanmotley100" width="100" height="120" class="aligncenter size-full wp-image-80422"></td>
     <td class="column-2">Johnathan Motley</td>
     <td class="column-3">Baylor</td> 
    </tr> 
    <tr class="row-3 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2011/08/amilejefferson100.jpg" alt="amilejefferson100" width="100" height="120" class="aligncenter size-full wp-image-12405"></td>
     <td class="column-2">Amile Jefferson</td>
     <td class="column-3">Duke</td> 
    </tr> 
    <tr class="row-4 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2016/11/carltonbragg100.jpg" alt="SONY DSC" width="100" height="120" class="size-full wp-image-80412"></td>
     <td class="column-2">Carlton Bragg</td>
     <td class="column-3">Kansas</td> 
    </tr> 
    <tr class="row-5 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2016/11/IsaiahHicks100.jpg" alt="isaiahhicks100" width="100" height="120" class="aligncenter size-full wp-image-80414"></td>
     <td class="column-2">Isaiah Hicks</td>
     <td class="column-3">North Carolina</td> 
    </tr> 
    <tr class="row-6 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2014/03/MikeYoung100.jpg" alt="MikeYoung100" width="100" height="120" class="aligncenter size-full wp-image-48175"></td>
     <td class="column-2">Michael Young</td>
     <td class="column-3">Pittsburgh</td> 
    </tr> 
    <tr class="row-7 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2016/11/TylerLydon100.jpg" alt="tylerlydon100" width="100" height="120" class="aligncenter size-full wp-image-80416"></td>
     <td class="column-2">Tyler Lydon</td>
     <td class="column-3">Syracuse</td> 
    </tr> 
    <tr class="row-8 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2016/11/KrisJenkins100.jpg" alt="September 24, 2011: Under Armour hosts the inaugural Grind Session at Lake Barrington Field House featuring the top 50 prep basketball players in the nation." width="100" height="120" class="size-full wp-image-80368"></td>
     <td class="column-2">Kris Jenkins</td>
     <td class="column-3">Villanova</td> 
    </tr> 
    <tr class="row-9 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2016/11/NigelHayes100.jpg" alt="nigelhayes100" width="100" height="120" class="aligncenter size-full wp-image-80418"></td>
     <td class="column-2">Nigel Hayes</td>
     <td class="column-3">Wisconsin</td> 
    </tr> 
   </tbody> 
  </table> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>