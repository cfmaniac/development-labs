<article id="post-9298" class="post-9298 post type-post status-publish format-standard has-post-thumbnail hentry category-basketball-training tag-regen tag-regen2recover"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p><span style="font-size: large;"><strong>Dr. Amys REGEN Nutrition Tip of the Week</strong></span></p> 
  <p>I have been inspired by the NCAA Final Four to create my Top Four Nutrition Tips for Athletes.</p> 
  <p><strong>#1 RECOVER:</strong> Dont forget to recover within 60 minutes after exercise by consuming a carbohydrate and protein snack or beverage! Consuming these nutrients when the muscles need them most will enhance your muscle recovery and allow you to work out just as hard the next day.</p> 
  <p><strong>#2 BREAKFAST:</strong> Start the day off right by eating a mix of carbohydrates and protein at breakfast. Research suggests that eating moderate amount of protein throughout the day may be better than eating one or two large protein meals. Add in some eggs or peanut butter at breakfast time to start the day off right!</p> 
  <p><strong>#3 FRUITS and VEGETABLES:</strong> Fruits and veggies not only provide you with lots of great vitamins and minerals but they also provide you with fiber all day long. Eating different colors of fruits and vegetables will provide you with different nutrients so dont forget to eat across the rainbow of colors!</p> 
  <p><strong>#4: HYDRATE, HYDRATE, HYDRATE:</strong> Make sure you consume at least 64 oz. of water or non-caffeinated drinks each day. Exercise increases the amount of fluid that your body needs so be sure to increase that on days that you are exercising and practicing. Carrying around a water bottle with you at all times will be a friendly reminder to you to stay hydrated so you can perform at your best!</p> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>