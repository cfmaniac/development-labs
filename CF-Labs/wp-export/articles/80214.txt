<article id="post-80214" class="post-80214 post type-post status-publish format-standard hentry category-uncategorized"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p>A message from Tiny Green to all the players from the Shore who committed to playing college basketball this week!</p> 
  <p><iframe src="//www.youtube.com/embed/BXdd6_q4GFE" width="560" height="314" allowfullscreen></iframe>A</p> 
  <p>&nbsp;</p> 
  <h2>Make sure to check out upcoming workouts and clinics!</h2> 
  <p style="text-align: center;"><a href="http://hoopgroup.com/hoop-group-headquarters/new-jersey-basketball-clinics/black-friday/">Black Friday Hoop Fest</a></p> 
  <p style="text-align: center;"><a href="http://hoopgroup.com/hoop-group-headquarters/new-jersey-basketball-clinics/preseason-high-school-boot-camp/">&nbsp;Preseason Boot Camp</a></p> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>