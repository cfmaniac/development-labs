<article id="post-95124" class="post-95124 post type-post status-publish format-standard has-post-thumbnail hentry category-hgsl tag-alvin-gentry-elite tag-ehtan-spry tag-hgsl tag-hoop-group-showcase-league tag-reed-myers"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p><img class="aligncenter wp-image-93523" src="https://hoopgroup.com/wp-content/uploads/2018/02/hgsl_alvin_gentry_twitter.jpg" alt="" width="517" height="266"></p> 
  <p>&nbsp;</p> 
  <p style="text-align: center;"><strong>Director:</strong> Mike Myers</p> 
  <p style="text-align: center;"><strong>Social Media Handles</strong>: Instagram: @alvin_gentry_elite</p> 
  <p style="text-align: center;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Twitter: <a href="https://twitter.com/AlvinGentryBB" target="_blank" rel="noopener">@AlvinGentryEliteBB</a></p> 
  <p><strong>Key players:</strong></p> 
  <p style="padding-left: 30px;">Ethan Spry</p> 
  <p style="padding-left: 30px;">Reed Myers</p> 
  <p><strong>Team analysis:</strong> Alvin Gentry Elite is an AAU program sponsored by New Orleans Pelicans Head Coach Alvin Gentry. The team is headquartered in Scottsdale, Arizona.</p> 
  <p>Alvin Gentry Elite, or AGE, is an organization that is committed to character, commitment, and integrity as well as maintaining all NCAA academic eligibility requirements. Alvin Gentry is well known and respected in the basketball community. The product this team puts forth represents his name well every time they grace the hardwood. The team had a nice mix of age groups on the 17u team from last year. As a result, this group will return a nice nucleus of battle tested players who are comfortable playing high level games on the HGSL. One of the standouts is guard <strong>Reed Myers</strong>. Myers relayed that hes hearing from Cornell, Pepperdine, Dartmouth, Rice and Colorado Christian.<em>Colorado Christian and Pepperdine came to one of my open gyms before the season and in the past summer I visited Rice University, met the coaches and did their elite camp.</em></p> 
  <p>AGE plays a high octane style of basketball on the offensive side of the floor. Their willingness to push the ball and create in transition makes them one of of the most exciting teams on the HGSL. Get your Iphones out everyone, the show is about to begin!</p> 
  <p><strong>Stat to know:</strong> The AGE was founded originally as the Arizona Eagles in 2009. The Club excelled as a youth team and won numerous Arizona state titles as well as West Coast Regional titles.</p> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>