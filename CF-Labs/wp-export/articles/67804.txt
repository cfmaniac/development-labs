<article id="post-67804" class="post-67804 post type-post status-publish format-standard has-post-thumbnail hentry category-headquarters-leagues"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <img data-caption="Former St. Anthony Friar <strong>Taurean Thompson</strong> put on a show in last years Boardwalk Showcase, who will this year?" class="aligncenter  wp-image-67680" src="https://hoopgroup.com/wp-content/uploads/2016/01/Taurean-Thompson-dunk1.jpg" alt="Former St. Anthony Friar Taurean Thompson put on a show in last year's Boardwalk Showcase, who will this year?" width="600" height="250">
  <p class="blog2html_p ">Former St. Anthony Friar &lt;strong&gt;Taurean Thompson&lt;/strong&gt; put on a show in last years Boardwalk Showcase, who will this year?</p> 
  <p><strong>January 9th</strong> The first showcase of the new year is upon us. Were at Long Branch High School in Long Branch, NJ for the Hoop Group Boardwalk Showcase. Five games on slate today, featuring some of the best teams in the Jersey Shore. Four of the Top 20 teams in the state, headlined by national powerhouse St. Anthony. Lets get going!<br> <strong><br> 8:14 pm</strong><br> And thats a wrap! Long Branch hangs on to top SJV in the finale. And the Boardwalk Showcase has come to an end. Thank you to everyone who made this a tremendous day of basketball at the Jersey Shore! </p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck"> 
   <p lang="en" dir="ltr"><a href="https://twitter.com/hashtag/hgshowcase?src=hash">#hgshowcase</a> Long Branch holds off SJV 54-47 Anthony Velazquez-Rivera 19 Zach Howarth 18 for SJV</p> 
   <p> NJ Hoops (@NJHoops) <a href="https://twitter.com/NJHoops/status/685992162346020868">January 10, 2016</a></p>
  </blockquote> 
  <p><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></p> 
  <p><strong>7:51 pm</strong><br> Tied again. <strong>Grant Goode</strong> sinks two free throws and we are tied at 37 with 6 minutes to play. This has the makings for a classic.</p> 
  <p><strong>7:36 pm</strong><br> Long Branch started hot, but SJV climbed back in it. Zach Howarth ties it up to start the second half</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck"> 
   <p lang="en" dir="ltr">Carnegie Mellon bound Zach Howarth buries a triple and we are knotted at 24 early in the third quarter <a href="https://twitter.com/hashtag/HGShowcase?src=hash">#HGShowcase</a></p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/status/685983456417943552">January 10, 2016</a></p>
  </blockquote> 
  <p><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></p> 
  <p><strong>7:15 pm</strong><br> Long Branch student section showing their support for the Green Waves<br> <img src="https://hoopgroup.com/wp-content/uploads/2016/01/IMG_8192.jpg" alt="IMG_8192" width="400" height="267" class="aligncenter size-full wp-image-67846"></p> 
  <p><strong>6:51 pm</strong><br> Were coming down the home stretch! St. John Vianney and Long Branch tipping off now.</p> 
  <p><strong>6:44 pm</strong><br> St. Anthony crusies to the win. Easy to see why so many consider them the #1 team in the state. FDU commit <strong>Kaleb Bishop</strong> wins game MVP in what was a balanced effort by the Friars.and then gets the congratulations from his teammates<br> <img src="https://hoopgroup.com/wp-content/uploads/2016/01/IMG_8190.jpg" alt="IMG_8190" width="400" height="267" class="aligncenter size-full wp-image-67843"></p> 
  <p><strong>6:15 pm</strong></p> 
  <p>So many big names for St. Anthony, but they have such a balance on offense. No one guy dominates the books. A true team</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck"> 
   <p lang="en" dir="ltr">At half St Ants over CBA 37-17 <a href="https://twitter.com/hashtag/HGShowcase?src=hash">#HGShowcase</a> Gist, Gibbs &amp; Joyner 7 each</p> 
   <p> NJ Hoops (@NJHoops) <a href="https://twitter.com/NJHoops/status/685957549494022144">January 9, 2016</a></p>
  </blockquote> 
  <p><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script><br> <strong>5:51 pm</strong></p> 
  <p>Gym is PACKED for this game.<br> <img class="size-medium wp-image-67839 alignleft" src="https://hoopgroup.com/wp-content/uploads/2016/01/IMG_8181-300x200.jpg" alt="IMG_8181" width="300" height="200"><img class="aligncenter size-medium wp-image-67838" src="https://hoopgroup.com/wp-content/uploads/2016/01/IMG_8182-300x200.jpg" alt="IMG_8182" width="300" height="200"></p> 
  <p><strong>5:38 pm</strong><br> Got to love the toughness that St. Anthony brings. 61 guard&nbsp;<strong>Shyquan Gibbs&nbsp;</strong>is guarding 68&nbsp;<strong>Pat Andree</strong>. NJIT bound senior does not give an inch <a href="http://www.twitter.com/thehoopgroup" target="_blank">@TheHoopGroup</a></p> 
  <p><strong>5:15 pm</strong><br> Next game is the prime time match up: Christian Brothers Academy vs St. Anthony. St Anthony has 6 college commits, including Georgetown bound&nbsp;<strong>Jagan Mosely.</strong> The Colts of CBA are led by Lehigh commit&nbsp;<strong>Pat Andree</strong> who has been on a tear of late, scoring 30+ in his last five games. Can CBA pull the upset?</p> 
  <a href="https://hoopgroup.com/wp-content/uploads/2011/01/FullSizeRender1.jpg"><img data-caption="2015 NCAA Signing Day: Coach Hurley is with former campers Jagan Mosely (Georgetown), Kaleb Bishop (FDU), Shyquan Gibbs (NJIT), and Jaleel Lord (Merrimack) who all signed their NLI in November." class="aligncenter  wp-image-66585" src="https://hoopgroup.com/wp-content/uploads/2011/01/FullSizeRender1.jpg" alt="2015 NCAA Signing Day: Coach Hurley is with former campers Jagan Mosely (Georgetown), Kaleb Bishop (FDU), Shyquan Gibbs (NJIT), and Jaleel Lord (Merrimack) who all signed their NLI in November. " width="474" height="543"></a>
  <p class="blog2html_p ">2015 NCAA Signing Day: Coach Hurley is with former campers Jagan Mosely (Georgetown), Kaleb Bishop (FDU), Shyquan Gibbs (NJIT), and Jaleel Lord (Merrimack) who all signed their NLI in November.</p> 
  <p><strong>5:06 pm</strong><br> The Warriors of Manasquan remain undefeated with a come from behind win over Point Beach.&nbsp;<strong>Devin Jensen&nbsp;</strong>finishes with 19 points for MVP honors. For Point Beach, Michael Rice was the MVP, stuffing the stat sheet with 8 points, 7 rebounds and 3 assists</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck"> 
   <p dir="ltr" lang="en">Manasquan comes from behind to beat Point Beach. Great game by both sides. 3 down, 2 to go <a href="https://twitter.com/hashtag/HGShowcase?src=hash">#HGShowcase</a> <a href="https://t.co/EU9IhWaWup">pic.twitter.com/EU9IhWaWup</a></p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/status/685945755887091712">January 9, 2016</a></p>
  </blockquote> 
  <p><script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script></p> 
  <p><strong>4:57 pm</strong><br> Manasquan is leaving the door open with some missed free throws. Great finish here at Long Branch</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p> with 39.4 left, Manasquan 45, Point Beach 39. Great finish here. <a href="https://twitter.com/hashtag/HGshowcase?src=hash">#HGshowcase</a></p> 
   <p> Sherlon Christie (@sherlonapp) <a href="https://twitter.com/sherlonapp/status/685943315439697920">January 9, 2016</a> </p>
  </blockquote> 
  <p><script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script></p> 
  <p><strong>5:06 pm</strong><br> The Warriors of Manasquan remain undefeated with a come from behind win over Point Pleasant Beach. <strong>Devin Jensen</strong> is MVP with 19 points. For Point Beach, <strong>Michael Rice</strong> was MVP with a full stat line: 8 points, 7 rebounds, 3 assits</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck"> 
   <p dir="ltr" lang="en">Manasquan comes from behind to beat Point Beach. Great game by both sides. 3 down, 2 to go <a href="https://twitter.com/hashtag/HGShowcase?src=hash">#HGShowcase</a> <a href="https://t.co/EU9IhWaWup">pic.twitter.com/EU9IhWaWup</a></p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/status/685945755887091712">January 9, 2016</a></p>
  </blockquote> 
  <p><script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script></p> 
  <p><strong>4:26 pm</strong><br> Brothers <strong>Devin an Ryan Jensen</strong> have combined for 16 for Manasquans 25 points so far in this game. Squan trails Point Beach 34-25 in the third quarter</p> 
  <p><strong>4:13 pm</strong></p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p> <a href="https://twitter.com/hashtag/hgshowcase?src=hash">#hgshowcase</a> at half Point Beach 25-24 over unbeaten Manasquan Trevor Covey 7</p> 
   <p> NJ Hoops (@NJHoops) <a href="https://twitter.com/NJHoops/status/685931685209346048">January 9, 2016</a> </p>
  </blockquote> 
  <p><script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script><br> <strong>4:04 pm</strong></p> 
  <p>Not many players in the Shore play harder than&nbsp;<strong>Ryan Jensen</strong> (Manasquan 16). Senior is 64, but rebounds like hes 68. Tremendous motor <a href="http://www.twitter.com/thehoopgroup" target="_blank">@TheHoopGroup</a></p> 
  <p><strong>3:51 pm</strong><br> Jimmy Panzini is one of the best seniors still available in NJ.</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck"> 
   <p dir="ltr" lang="en">Nice finish in transition for Point Beachs Jimmy Panzini. Finishes through contact for the and-1. Beach &amp; Manasquan tied early <a href="https://twitter.com/hashtag/HGShowcase?src=hash">#HGShowcase</a></p> 
   <p> James Albano (@JN_Albano) <a href="https://twitter.com/JN_Albano/status/685926926729490433">January 9, 2016</a></p>
  </blockquote> 
  <p><script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script></p> 
  <p><strong>3:31 pm</strong><br> Game 3 today is Point Pleasant Beach versus <a href="http://hoopgroup.com/tournaments/high-school-showcase-2/manasquan-looking-to-keep-rolling/" target="_blank">Manasquan</a>. The Warriors of Manasquan are off to a hot start this season, thanks in part to some senior leadership with <strong>Ryan Jensen</strong> (TCNJ) and <strong>Jack Sheehan</strong>, both of which have junior brothers on the team. Point Beach will look to their senior guards <strong>Michael Rice</strong> (Franklin &amp; Marshall) and <strong>Jimmy Panzini</strong> to stop the streaking Warriors</p> 
  <p><strong>3:24 pm</strong><br> Rumson completes the upset. Takes down Gill St. Bernards. <strong>Brendan Barry</strong> wins his second Boardwalk MVP in as many years. <strong>Adam Mitola</strong> is MVP for GSB</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p> <a href="https://twitter.com/hashtag/hgshowcase?src=hash">#hgshowcase</a> Rumson upsets Gill St Bernards from unbeaten ranks 66-49. Brendan Barry 29</p> 
   <p> NJ Hoops (@NJHoops) <a href="https://twitter.com/NJHoops/status/685919165073473536">January 9, 2016</a> </p>
  </blockquote> 
  <p><script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script></p> 
  <p><strong>3:05 pm</strong><br> Lot of young talent on Gill. They will be a very good high school team in NJ for a few years. <strong>Alex Schachne</strong> is just one piece to the puzzle</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p>Sophomore Alex Schachne with 5 straight points for Gill. Pulls to within 6. Just over 3 minutes left <a href="https://twitter.com/hashtag/HGShowcase?src=hash">#HGShowcase</a></p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/status/685915332968595456">January 9, 2016</a></p>
  </blockquote> 
  <p><script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script><br> <strong>2:54 pm</strong><br> Brendan Barry has a knack for making plays at the Boardwalk Showcase<br> <iframe src="https://www.youtube.com/embed/7O0MCsyMSXk" width="560" height="315" frameborder="0" allowfullscreen></iframe></p> 
  <p><strong>2:40 pm</strong><br> <strong>Mickey Schluter</strong> buries a three and Rumson extends their lead to 16 over Gill St. Bernards. The Bull dogs are hot from the outside.</p> 
  <p><strong>2:17 pm</strong><br> Excellent play on the court, and a few celebrations on the sidelines has Rumson on top</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p> At half <a href="https://twitter.com/hashtag/hgshowcase?src=hash">#hgshowcase</a> Rumson leading 6th ranked Gill St Bernards 27-16. Brendan Barry 13. Excellent execution offensively &amp; defensively</p> 
   <p> NJ Hoops (@NJHoops) <a href="https://twitter.com/NJHoops/status/685903217583087616">January 9, 2016</a> </p>
  </blockquote> 
  <p><script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script></p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p>Rumsons bench has a lot to celebrate. Lead Gill St. Bernard 27-16 at half <a href="https://twitter.com/hashtag/HGShowcase?src=hash">#HGShowcase</a> <a href="https://t.co/Oer6dYe4Js">pic.twitter.com/Oer6dYe4Js</a></p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/status/685903223023112192">January 9, 2016</a></p>
  </blockquote> 
  <p><script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script></p> 
  <p><strong>2:08 pm</strong><br> Great crowd on hand for game 2, and theres still three more left today!<img class="aligncenter size-full wp-image-67820" src="https://hoopgroup.com/wp-content/uploads/2016/01/IMG_8109.jpg" alt="IMG_8109" width="400" height="267"></p> 
  <p><strong>1:59 pm</strong></p> 
  <p>Offense hard to come by after one quarter. Rumson with a 1 point lead, thanks to Barry</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p> <a href="https://twitter.com/hashtag/hgshowcase?src=hash">#hgshowcase</a> after 1, Rumson leads 6th ranked Gill St. Bernards 8-7. Brendan Barry 5</p> 
   <p> NJ Hoops (@NJHoops) <a href="https://twitter.com/NJHoops/status/685898582159966208">January 9, 2016</a> </p>
  </blockquote> 
  <p><script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script></p> 
  <p><strong>1:38 pm</strong></p> 
  <p>Game 2 should be another great game. Gill St. Bernards has a lot of young talent, and are led by seniors&nbsp;<strong>Adam Mitola&nbsp;</strong>(George Washington) and&nbsp;<strong>Julius Stoma</strong>, who recently picked up and offer from St. Francis. Rumson has a great scorer in&nbsp;<strong>Brendan Barry</strong>, who holds the record for points scored in a game at the Boardwalk Showcase. He scored 34 last year. Can he do it again?</p> 
  <p><strong>1:26 pm</strong></p> 
  <p>One game down here at Long Branch. Congrats to Mater Dei on another win.</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p>Final. Kyle Elliot MVP for <a href="https://twitter.com/MaterDeiPrepNJ">@MaterDeiPrepNJ</a> with 21 pts, Barry Brown had 19 for Neptune <a href="https://twitter.com/hashtag/HGShowcase?src=hash">#HGShowcase</a> <a href="https://t.co/4DMuv6LEeI">pic.twitter.com/4DMuv6LEeI</a></p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/status/685890232928067584">January 9, 2016</a></p>
  </blockquote> 
  <p><strong>1:16 pm</strong></p> 
  <p>This game has turned into dunk city for Mater Dei. <strong>Elijah Barnes</strong> has about 5, <strong>Kyle Elliot</strong> has a couple, and&nbsp;<strong>Joshua Green</strong> has one, too. 20 point lead for the Seraphs-<a href="http://www.twitter.com/thehoopgroup" target="_blank">@TheHoopGroup</a></p> 
  <p><strong>1:07 pm</strong></p> 
  <p>Check out Elijah Barness first half dunks. Lot of athleticism from the 67 junior!<br> <iframe src="https://www.youtube.com/embed/KmGYRT6E72U" width="560" height="315" frameborder="0" allowfullscreen></iframe><br> <strong>1:04 pm</strong><br> Mater Dei is beginning to extend their lead. More dunks by Barnes, <strong>Kyle Elliot</strong> leads the way with 16</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p>10-0 run for Mater Dei extends lead to 18 after third quarter. Same defensive intensity from Seraphs, now offense is clicking. <a href="https://twitter.com/hashtag/HGShowcase?src=hash">#HGShowcase</a></p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/status/685884822515007488">January 9, 2016</a></p>
  </blockquote> 
  <p><script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script><br> <strong>12:37 pm</strong></p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p> At half 18th ranked Mater Dei 25-24 in good one. Nyke McCombs 7 Barry Brown 14 for Neptune <a href="https://twitter.com/hashtag/hgshowcase?src=hash">#hgshowcase</a></p> 
   <p> NJ Hoops (@NJHoops) <a href="https://twitter.com/NJHoops/status/685877383585476609">January 9, 2016</a> </p>
  </blockquote> 
  <p><script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script></p> 
  <p><strong>12:31 pm</strong></p> 
  <p>When your offense is struggling, you need to rely on your defense to keep you in it, thats exactly what Mater Dei is doing today.</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p>Stifling defense by Mater Dei. Offense is struggling, but able to take lead over Neptune thanks to lock down D <a href="https://twitter.com/hashtag/HGShowcase?src=hash">#HGShowcase</a></p> 
   <p> James Albano (@JN_Albano) <a href="https://twitter.com/JN_Albano/status/685876527297368066">January 9, 2016</a></p>
  </blockquote> 
  <p><script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script></p> 
  <p><strong>12:21 pm</strong></p> 
  <p>After one, its been tight</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p> After 1Q: Neptune 15, Mater Dei Prep 13. Here is the ending to quarter. <a href="https://twitter.com/hashtag/HGshowcase?src=hash">#HGshowcase</a> <a href="https://t.co/vw9nZgQXsO">pic.twitter.com/vw9nZgQXsO</a></p> 
   <p> Sherlon Christie (@sherlonapp) <a href="https://twitter.com/sherlonapp/status/685873197007400962">January 9, 2016</a> </p>
  </blockquote> 
  <p><script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script></p> 
  <p><strong>12:13 pm</strong></p> 
  <p>Great exchange between our players to watch: Barnes with a huge slam down the lane, and Brown answers with a three for Neptune. Brown has 7 of Neptunes 11 points</p> 
  <p><strong>11:54 am</strong></p> 
  <p>Good group of media on hand, including Shore Sports Zone, NJ Hoops, Shore Sports Network, Asbury Park Press, MSG Varsity, and more!</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p><a href="https://twitter.com/MDP_Seraphs">@MDP_Seraphs</a> stretches before game 1 of <a href="https://twitter.com/TheHoopGroup">@TheHoopGroup</a> Boardwalk Showcase. Highlights later on SSZ. <a href="https://t.co/PA5Jj21zek">pic.twitter.com/PA5Jj21zek</a></p> 
   <p> Shore Sports Zone (@ShoreSportsZone) <a href="https://twitter.com/ShoreSportsZone/status/685861353748299776">January 9, 2016</a></p>
  </blockquote> 
  <p><script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script></p> 
  <p><strong>11:48 am</strong></p> 
  <p>First game on tap is between the Scarlett Fliers of Neptune and the Mater Dei Seraphs. Neptune features&nbsp;<strong>Barry Brown</strong>, last years Boardwalk MVP, and a young core. Mater Dei is having great success this season, ranking second in the Shore. They will look to <a href="http://hoopgroup.com/tournaments/high-school-showcase-2/mater-dei-ready-for-showdown-in-long-branch/" target="_blank">Elijah Barnes</a> to have a big day.</p> 
 </div>
 <!-- .entry-content --> 
</article>