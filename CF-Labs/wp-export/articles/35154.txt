<article id="post-35154" class="post-35154 post type-post status-publish format-standard has-post-thumbnail hentry category-alumni-updates"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p><span style="font-size: 13px;"><img class="aligncenter size-full wp-image-10802" alt="rsz_nerlens-noel---friar-basketball" src="https://hoopgroup.com/wp-content/uploads/2011/05/rsz_nerlens-noel-friar-basketball.jpg" width="120" height="90">&nbsp;</span></p> 
  <p><span style="font-size: 13px;">#1 pick Anthony Bennett will join the long list of&nbsp;</span><a style="font-size: 13px;" href="http://hoopgroup.com/hoopgroup/about/alumni.php">Hoop Group Alumni</a><span style="font-size: 13px;">&nbsp;to go on to play in the NBA. 17 Hoop Group alum, included 3 of the first 6 picks were selected in this years NBA Draft&nbsp;</span></p> 
  <p><span style="font-size: 13px;">Congratulations to all the Hoop Group Alumni for fulfilling their dreams to make it to the NBA. We all wish them continued success in reaching their new goals.</span></p> 
  <table id="tablepress-494" class="tablepress tablepress-id-494"> 
   <thead> 
    <tr class="row-1 odd"> 
     <th class="column-1">PHOTO</th>
     <th class="column-2">NAME</th>
     <th class="column-3">NBA TEAM</th>
     <th class="column-4">COLLEGE</th>
     <th class="column-5">HIGH SCHOOL</th> 
    </tr> 
   </thead> 
   <tbody> 
    <tr class="row-2 even"> 
     <td class="column-1"></td>
     <td class="column-2">Anthony Bennett</td>
     <td class="column-3">Cavaliers</td>
     <td class="column-4">UNLV</td>
     <td class="column-5">Findlay NV</td> 
    </tr> 
    <tr class="row-3 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2016/02/codyzeller100.jpg" alt="codyzeller100" width="100" height="120" class="aligncenter size-full wp-image-71011"></td>
     <td class="column-2">Cody Zeller</td>
     <td class="column-3">Bobcats</td>
     <td class="column-4">Indiana</td>
     <td class="column-5">Washington IN</td> 
    </tr> 
    <tr class="row-4 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2013/11/nerlens-noel-100.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-44659"></td>
     <td class="column-2">Nerlens Noel</td>
     <td class="column-3">Sixers</td>
     <td class="column-4">Kentucky</td>
     <td class="column-5">Tilton NH</td> 
    </tr> 
    <tr class="row-5 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2013/04/TreyBurke100.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-29818"></td>
     <td class="column-2">Trey Burke</td>
     <td class="column-3">Jazz</td>
     <td class="column-4">Michigan</td>
     <td class="column-5">Northland OH</td> 
    </tr> 
    <tr class="row-6 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2016/02/CJMcCollum100.jpg" alt="CJMcCollum100" width="100" height="120" class="aligncenter size-full wp-image-70778"></td>
     <td class="column-2">CJ McCollum</td>
     <td class="column-3">Trail Blazers</td>
     <td class="column-4">Lehigh</td>
     <td class="column-5">Glen Oak OH</td> 
    </tr> 
    <tr class="row-7 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2013/11/MichaelCarterWilliams100.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-44624"></td>
     <td class="column-2">Michael Carter-Williams</td>
     <td class="column-3">Sixers</td>
     <td class="column-4">Syracuse</td>
     <td class="column-5">St. Andrew's RI</td> 
    </tr> 
    <tr class="row-8 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2015/02/mason-plumlee.jpg" alt="mason plumlee" width="100" height="120" class="aligncenter size-full wp-image-57350"></td>
     <td class="column-2">Mason Plumlee</td>
     <td class="column-3">Nets</td>
     <td class="column-4">Duke</td>
     <td class="column-5">Christ NC</td> 
    </tr> 
    <tr class="row-9 odd"> 
     <td class="column-1"></td>
     <td class="column-2">Tim Hardaway Jr.</td>
     <td class="column-3">Knicks</td>
     <td class="column-4">Michigan</td>
     <td class="column-5">Palmetto FL</td> 
    </tr> 
    <tr class="row-10 even"> 
     <td class="column-1"></td>
     <td class="column-2">Reggie Bullock</td>
     <td class="column-3">Clippers</td>
     <td class="column-4">North Carolina</td>
     <td class="column-5">Kinston NC</td> 
    </tr> 
    <tr class="row-11 odd"> 
     <td class="column-1"></td>
     <td class="column-2">Ray McCallum</td>
     <td class="column-3">Pistons</td>
     <td class="column-4">Detroit</td>
     <td class="column-5">Country Day MI</td> 
    </tr> 
    <tr class="row-12 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2011/03/RickyLedo100.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-8437"></td>
     <td class="column-2">Ricky Ledo</td>
     <td class="column-3">Mavericks</td>
     <td class="column-4">Providence</td>
     <td class="column-5">South Kent CT</td> 
    </tr> 
    <tr class="row-13 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2016/02/ErickGreen100.jpg" alt="ErickGreen100" width="100" height="120" class="aligncenter size-full wp-image-71018"></td>
     <td class="column-2">Erick Green</td>
     <td class="column-3">Hawks</td>
     <td class="column-4">Virginia Tech</td>
     <td class="column-5">Paul VI VA </td> 
    </tr> 
    <tr class="row-14 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2012/03/RyanKelly100.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-18907"></td>
     <td class="column-2">Ryan Kelly</td>
     <td class="column-3">Bulls</td>
     <td class="column-4">Duke</td>
     <td class="column-5">Ravencroft NC</td> 
    </tr> 
    <tr class="row-15 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2011/02/Erik-Murphy-100.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-2522"></td>
     <td class="column-2">Erik Murphy</td>
     <td class="column-3">Hawks</td>
     <td class="column-4">Florida</td>
     <td class="column-5">St. Mark's MA</td> 
    </tr> 
    <tr class="row-16 even"> 
     <td class="column-1"></td>
     <td class="column-2">Romero Osby</td>
     <td class="column-3">Timberwolves</td>
     <td class="column-4">Oklahoma</td>
     <td class="column-5">North Lauderdale MI</td> 
    </tr> 
    <tr class="row-17 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2011/03/alexoriakhi1001.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-8432"></td>
     <td class="column-2">Alex Oriakhi</td>
     <td class="column-3">Spurs</td>
     <td class="column-4">Missouri</td>
     <td class="column-5">Winchendon MA</td> 
    </tr> 
    <tr class="row-18 even"> 
     <td class="column-1"></td>
     <td class="column-2">Deshaun Thomas</td>
     <td class="column-3">Timberwolves</td>
     <td class="column-4">Ohio State</td>
     <td class="column-5">Bishop Luers IN</td> 
    </tr> 
   </tbody> 
  </table> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>