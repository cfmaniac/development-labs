<article id="post-19782" class="post-19782 post type-post status-publish format-standard has-post-thumbnail hentry category-tournament-recap tag-hgjamfest tag-pitt"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p><a href="http://www.hoopgroup.com/wp-content/uploads/2012/04/coaches1.jpg"><img src="http://www.hoopgroup.com/wp-content/uploads/2012/04/coaches1.jpg" alt="College Coaches at Pitt Jam Fest" title="College Coaches at Pitt Jam Fest" width="600" height="250" class="alignright size-full wp-image-19785"></a></p> 
  <p>The Under Armour Hoop Group Pitt Jam Fest is annually one of the premier AAU tournaments of the spring. With the NCAA changing the recruiting calendar allowing coaches to be on the road evaluating players again in April, the Pitt Jam Fest was the destination of choice for Division 1 College Coaches with over 160 D1 schools attending the event. </p> 
  <p>With teams spanning from coast to coast including international competition, Pitt Jam Fest allowed college coaches to see talent unparalleled to any other tournament in the country. </p> 
  <table id="tablepress-278" class="tablepress tablepress-id-278"> 
   <thead> 
    <tr class="row-1 odd"> 
     <th colspan="3" class="column-1">Division I Colleges Attending </th> 
    </tr> 
   </thead> 
   <tbody> 
    <tr class="row-2 even"> 
     <td class="column-1">Akron</td>
     <td class="column-2">High Point</td>
     <td class="column-3">ODU</td> 
    </tr> 
    <tr class="row-3 odd"> 
     <td class="column-1">Alabama</td>
     <td class="column-2">Hofstra</td>
     <td class="column-3">Oregon</td> 
    </tr> 
    <tr class="row-4 even"> 
     <td class="column-1">UAB</td>
     <td class="column-2">Holy Cross</td>
     <td class="column-3">Oregon St</td> 
    </tr> 
    <tr class="row-5 odd"> 
     <td class="column-1">Albany</td>
     <td class="column-2">Houston</td>
     <td class="column-3">Penn</td> 
    </tr> 
    <tr class="row-6 even"> 
     <td class="column-1">American</td>
     <td class="column-2">Howard</td>
     <td class="column-3">Penn St</td> 
    </tr> 
    <tr class="row-7 odd"> 
     <td class="column-1">App St</td>
     <td class="column-2">Indiana U- Purdue U, Fort Wayne</td>
     <td class="column-3">Pitt</td> 
    </tr> 
    <tr class="row-8 even"> 
     <td class="column-1">Auburn</td>
     <td class="column-2">Iowa</td>
     <td class="column-3">Presbyterian</td> 
    </tr> 
    <tr class="row-9 odd"> 
     <td class="column-1">Baylor</td>
     <td class="column-2">Iowa State</td>
     <td class="column-3">Princeton</td> 
    </tr> 
    <tr class="row-10 even"> 
     <td class="column-1">Bethune- Cookman</td>
     <td class="column-2">Jacksonville</td>
     <td class="column-3">Providence</td> 
    </tr> 
    <tr class="row-11 odd"> 
     <td class="column-1">Boston College</td>
     <td class="column-2">James Madison</td>
     <td class="column-3">Quinnipiac</td> 
    </tr> 
    <tr class="row-12 even"> 
     <td class="column-1">Boston U</td>
     <td class="column-2">Kansas</td>
     <td class="column-3">Radford</td> 
    </tr> 
    <tr class="row-13 odd"> 
     <td class="column-1">Bradley</td>
     <td class="column-2">Kansas State</td>
     <td class="column-3">Rhode Island</td> 
    </tr> 
    <tr class="row-14 even"> 
     <td class="column-1">Bryant University</td>
     <td class="column-2">Kennesaw State</td>
     <td class="column-3">Richmond</td> 
    </tr> 
    <tr class="row-15 odd"> 
     <td class="column-1">Bucknell</td>
     <td class="column-2">Kentucky</td>
     <td class="column-3">Rider</td> 
    </tr> 
    <tr class="row-16 even"> 
     <td class="column-1">Buffalo</td>
     <td class="column-2">La Salle</td>
     <td class="column-3">Robert Morris</td> 
    </tr> 
    <tr class="row-17 odd"> 
     <td class="column-1">Butler</td>
     <td class="column-2">Lafayette</td>
     <td class="column-3">Rutgers</td> 
    </tr> 
    <tr class="row-18 even"> 
     <td class="column-1">Campbell</td>
     <td class="column-2">Lamar</td>
     <td class="column-3">Seton Hall</td> 
    </tr> 
    <tr class="row-19 odd"> 
     <td class="column-1">Canisius</td>
     <td class="column-2">Lehigh</td>
     <td class="column-3">Siena</td> 
    </tr> 
    <tr class="row-20 even"> 
     <td class="column-1">Central Florida</td>
     <td class="column-2">Liberty</td>
     <td class="column-3">South Carolina</td> 
    </tr> 
    <tr class="row-21 odd"> 
     <td class="column-1">Charleston</td>
     <td class="column-2">Lipscomb</td>
     <td class="column-3">USF</td> 
    </tr> 
    <tr class="row-22 even"> 
     <td class="column-1">Cincinnati</td>
     <td class="column-2">Long Island U- Brooklyn</td>
     <td class="column-3">St Bonaventure</td> 
    </tr> 
    <tr class="row-23 odd"> 
     <td class="column-1">Citadel</td>
     <td class="column-2">Longwood</td>
     <td class="column-3">St Francis</td> 
    </tr> 
    <tr class="row-24 even"> 
     <td class="column-1">Clemson</td>
     <td class="column-2">Loyola U Maryland</td>
     <td class="column-3">St Joes</td> 
    </tr> 
    <tr class="row-25 odd"> 
     <td class="column-1">Cleveland St</td>
     <td class="column-2">Manhattan</td>
     <td class="column-3">St Peter's</td> 
    </tr> 
    <tr class="row-26 even"> 
     <td class="column-1">Colgate</td>
     <td class="column-2">Marist</td>
     <td class="column-3">Stanford</td> 
    </tr> 
    <tr class="row-27 odd"> 
     <td class="column-1">Columbia</td>
     <td class="column-2">Marshall</td>
     <td class="column-3">Stetson</td> 
    </tr> 
    <tr class="row-28 even"> 
     <td class="column-1">Connecticut</td>
     <td class="column-2">Maryland</td>
     <td class="column-3">Stony Brook</td> 
    </tr> 
    <tr class="row-29 odd"> 
     <td class="column-1">Cornell</td>
     <td class="column-2">U Maryland, College Park</td>
     <td class="column-3">Syracuse</td> 
    </tr> 
    <tr class="row-30 even"> 
     <td class="column-1">Dartmouth</td>
     <td class="column-2">UMASS</td>
     <td class="column-3">Temple</td> 
    </tr> 
    <tr class="row-31 odd"> 
     <td class="column-1">Davidson</td>
     <td class="column-2">Memphis</td>
     <td class="column-3">Tenn @ Chattanooga</td> 
    </tr> 
    <tr class="row-32 even"> 
     <td class="column-1">Dayton</td>
     <td class="column-2">Miami</td>
     <td class="column-3">Tenn</td> 
    </tr> 
    <tr class="row-33 odd"> 
     <td class="column-1">Delaware</td>
     <td class="column-2">Miami (Ohio)</td>
     <td class="column-3">Texas A&amp;M</td> 
    </tr> 
    <tr class="row-34 even"> 
     <td class="column-1">DePaul</td>
     <td class="column-2">Middle Tenn St</td>
     <td class="column-3">UTEP</td> 
    </tr> 
    <tr class="row-35 odd"> 
     <td class="column-1">Drexel</td>
     <td class="column-2">Minnesota</td>
     <td class="column-3">Toledo</td> 
    </tr> 
    <tr class="row-36 even"> 
     <td class="column-1">Duquesne</td>
     <td class="column-2">Mississippi</td>
     <td class="column-3">Towson</td> 
    </tr> 
    <tr class="row-37 odd"> 
     <td class="column-1">East Carolina</td>
     <td class="column-2">Monmouth</td>
     <td class="column-3">Airforce</td> 
    </tr> 
    <tr class="row-38 even"> 
     <td class="column-1">Eastern Kentucky</td>
     <td class="column-2">Morgan State</td>
     <td class="column-3">USMA</td> 
    </tr> 
    <tr class="row-39 odd"> 
     <td class="column-1">Elon</td>
     <td class="column-2">Mt St Marys</td>
     <td class="column-3">Navy</td> 
    </tr> 
    <tr class="row-40 even"> 
     <td class="column-1">Fairfield</td>
     <td class="column-2">Murray St</td>
     <td class="column-3">Vanderbilt</td> 
    </tr> 
    <tr class="row-41 odd"> 
     <td class="column-1">Florida A&amp;M</td>
     <td class="column-2">UNLV</td>
     <td class="column-3">Villanova</td> 
    </tr> 
    <tr class="row-42 even"> 
     <td class="column-1">Florida Atlantic</td>
     <td class="column-2">New Hampshire</td>
     <td class="column-3">Virginia</td> 
    </tr> 
    <tr class="row-43 odd"> 
     <td class="column-1">Florida Gulf Coast</td>
     <td class="column-2">NJIT</td>
     <td class="column-3">VCU</td> 
    </tr> 
    <tr class="row-44 even"> 
     <td class="column-1">Florida International</td>
     <td class="column-2">Niagara</td>
     <td class="column-3">VirginiaPoly</td> 
    </tr> 
    <tr class="row-45 odd"> 
     <td class="column-1">Florida State</td>
     <td class="column-2">Norfolk State</td>
     <td class="column-3">Wagner</td> 
    </tr> 
    <tr class="row-46 even"> 
     <td class="column-1">Fordham</td>
     <td class="column-2">North Carolina St</td>
     <td class="column-3">Wake Forest</td> 
    </tr> 
    <tr class="row-47 odd"> 
     <td class="column-1">Furman</td>
     <td class="column-2">UNC</td>
     <td class="column-3">West Virginia</td> 
    </tr> 
    <tr class="row-48 even"> 
     <td class="column-1">George Mason</td>
     <td class="column-2">UNC Greensboro</td>
     <td class="column-3">Western Carolina</td> 
    </tr> 
    <tr class="row-49 odd"> 
     <td class="column-1">George Washington</td>
     <td class="column-2">North Florida</td>
     <td class="column-3">Western Kentucky</td> 
    </tr> 
    <tr class="row-50 even"> 
     <td class="column-1">Georgetown</td>
     <td class="column-2">Northeastern</td>
     <td class="column-3">William and Mary</td> 
    </tr> 
    <tr class="row-51 odd"> 
     <td class="column-1">Georgia</td>
     <td class="column-2">Northwestern</td>
     <td class="column-3">Winthrop</td> 
    </tr> 
    <tr class="row-52 even"> 
     <td class="column-1">Georgia Tech</td>
     <td class="column-2">Notre Dame</td>
     <td class="column-3">Xavier</td> 
    </tr> 
    <tr class="row-53 odd"> 
     <td class="column-1">Georgia State</td>
     <td class="column-2">Oakland</td>
     <td class="column-3">Yale</td> 
    </tr> 
    <tr class="row-54 even"> 
     <td class="column-1">Hartford</td>
     <td class="column-2">Ohio</td>
     <td class="column-3">Youngstown St</td> 
    </tr> 
    <tr class="row-55 odd"> 
     <td class="column-1">Harvard</td>
     <td class="column-2">Ok St</td>
     <td class="column-3"></td> 
    </tr> 
   </tbody> 
  </table> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>