<article id="post-44851" class="post-44851 post type-post status-publish format-standard has-post-thumbnail hentry category-alumni-updates"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p>Dreams Do Come True</p> 
  <p>Many Hoop Group alum have succeeded at many different levels, from the NBA, Division 1 as well as small colleges and in high school. On this page we look at some of the Hoop Group alumni excelling at the college level.</p> 
  <p>D-3Hoops.com has released their pre-season 2014 All American Teams. &nbsp;The list of filled with Hoop Group alumni, with over 50% of the players honored having attended Hoop Group Camps, Clinics or Tournaments. &nbsp;A look at the Hoop Group Alumni selected pre-season All America.</p> 
  <p>&nbsp;</p> 
  <table id="tablepress-533" class="tablepress tablepress-id-533"> 
   <thead> 
    <tr class="row-1 odd"> 
     <th class="column-1">Name</th>
     <th class="column-2">College</th>
     <th class="column-3">Hometown</th> 
    </tr> 
   </thead> 
   <tbody> 
    <tr class="row-2 even"> 
     <td class="column-1">1ST TEAM</td>
     <td class="column-2"></td>
     <td class="column-3"></td> 
    </tr> 
    <tr class="row-3 odd"> 
     <td class="column-1">Aaron Toomey</td>
     <td class="column-2">Amherst</td>
     <td class="column-3">Greensboro NC</td> 
    </tr> 
    <tr class="row-4 even"> 
     <td class="column-1">Joey Kizel</td>
     <td class="column-2">Middlebury</td>
     <td class="column-3">Millburn NJ</td> 
    </tr> 
    <tr class="row-5 odd"> 
     <td class="column-1">Tra Benefield</td>
     <td class="column-2">Christopher Newport</td>
     <td class="column-3">Folkston GA</td> 
    </tr> 
    <tr class="row-6 even"> 
     <td class="column-1">2ND TEAM</td>
     <td class="column-2"></td>
     <td class="column-3"></td> 
    </tr> 
    <tr class="row-7 odd"> 
     <td class="column-1">Tyler Peters</td>
     <td class="column-2">Wheaton</td>
     <td class="column-3">Medina OH</td> 
    </tr> 
    <tr class="row-8 even"> 
     <td class="column-1">Taylor Epley</td>
     <td class="column-2">Williams</td>
     <td class="column-3">Louisville KY</td> 
    </tr> 
    <tr class="row-9 odd"> 
     <td class="column-1">3RD TEAM</td>
     <td class="column-2"></td>
     <td class="column-3"></td> 
    </tr> 
    <tr class="row-10 even"> 
     <td class="column-1">Aaron Walton-Moss</td>
     <td class="column-2">Cabrini</td>
     <td class="column-3">Camden</td> 
    </tr> 
    <tr class="row-11 odd"> 
     <td class="column-1">John Ivy</td>
     <td class="column-2">Brockport St</td>
     <td class="column-3">Rochester NY</td> 
    </tr> 
    <tr class="row-12 even"> 
     <td class="column-1">Michael Mayer</td>
     <td class="column-2">Williams</td>
     <td class="column-3">Durham NC</td> 
    </tr> 
    <tr class="row-13 odd"> 
     <td class="column-1">4TH TEAM</td>
     <td class="column-2"></td>
     <td class="column-3"></td> 
    </tr> 
    <tr class="row-14 even"> 
     <td class="column-1">DJ Woodmore</td>
     <td class="column-2">Virginia Wesleyan</td>
     <td class="column-3">Virginia Beach VA</td> 
    </tr> 
    <tr class="row-15 odd"> 
     <td class="column-1">Doug Thorpe</td>
     <td class="column-2">Wooster</td>
     <td class="column-3">Columbus OH</td> 
    </tr> 
    <tr class="row-16 even"> 
     <td class="column-1">Richie Bonney</td>
     <td class="column-2">Hobart</td>
     <td class="column-3">Norwich NY</td> 
    </tr> 
    <tr class="row-17 odd"> 
     <td class="column-1">HONORABLE MENTION</td>
     <td class="column-2"></td>
     <td class="column-3"></td> 
    </tr> 
    <tr class="row-18 even"> 
     <td class="column-1">Darius Watson</td>
     <td class="column-2">Albertus Magnus</td>
     <td class="column-3">New Britain CT</td> 
    </tr> 
    <tr class="row-19 odd"> 
     <td class="column-1">Gerry Wixted</td>
     <td class="column-2">Dickinson</td>
     <td class="column-3">Moorestown</td> 
    </tr> 
   </tbody> 
  </table> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>