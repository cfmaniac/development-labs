<article id="post-13182" class="post-13182 post type-post status-publish format-standard has-post-thumbnail hentry category-tournament-recap tag-hgjamfest tag-fall-jam-fest"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p>Sixty-six teams from 15-Open divisions will be competing in likely their last competition together before the start of the high school basketball season. Hoop Group will be updating LIVE from the Fall Jam Fest at Hoop Group Headquarters in Neptune, New Jersey. Check back throughout the day for scores, updates, and insight. </p> 
  <p><em><MOST RECENT ENTRIES AT BOTTOM OF PAGE></em></p> 
  <p><strong><em>September 24th, 2011, 9:22AM</em></strong></p> 
  <p><a href="http://hoopgroup.com/wp-content/uploads/2011/09/hotshots.jpg"><img data-caption="17U Jersey Hot Shots rooting for the younger guys in their program after their huge 2nd half comback against the NJ Pirates." src="http://hoopgroup.com/wp-content/uploads/2011/09/hotshots-300x199.jpg" alt="" title="Jersey Hot Shots" width="300" height="199" class="alignright size-medium wp-image-13188"></a></p>
  <p class="blog2html_p ">17U Jersey Hot Shots rooting for the younger guys in their program after their huge 2nd half comback against the NJ Pirates.</p>First set of games is underway. Just taking a quick look around, there is a lot of talent here. Jameel Warney, recently committed to Stony Brook, is playing for the Hot Shots on Court 1, St. Benedicts guard Melvin Johnson is here, along with highly-rated forward Tyler Roberson.
  <p></p> 
  <p>Game of the most intrigue right now is New Jersey Playaz and Isaiah Briscoe.</p> 
  <p>Briscoe just began his freshman year at St. Benedicts and is already one of the most promising young prospects in the Class of 2015. He is already 61 with a muscular build, impressive athleticism, and all the tools to be an impressive combo guard at the college level.</p> 
  <p><strong><em>September 24th, 2011, 10:31AM</em></strong></p> 
  <p>Isaiah Briscoe lead the NJ Playaz to victory in the first game set. When he plays in his age group, his natural talents allow him to do most anything he pleases on the court. He has the size to rebound, makes good decisions with the basketball, and can attack to basket. </p> 
  <p>Tyler Roberson (Roselle Catholic (NJ)) and the New Jersey Roadrunners playing now in the second game set. Roberson is the most athletically gifted player on the floor, but he has yet to shift it into high gear today and make a significant impact. </p> 
  <p>Still, it is easy to see why high-major schools, including Syracuse, Kansas State, Florida, and others are taking an interest in the 67 junior. He is long and athletic, runs the floor with ease, and block shots from the weak side on defense. For a player his size, he has a strong handle, and is working to polish his face-up game. </p> 
  <p>Noah Vonleh, a 67 sophomore from Mass Rivals, plays at 3pm. ESPN ranks Vonleh as the 6th best prospect in the country for the Class of 2014. </p> 
  <p><em><strong>September 24th, 2011, 11:39AM</strong></em></p> 
  <p>In a field of players that includes a great crop of young talent, Hudson Catholic freshman point guard Nassir Barrino is beginning to separate himself from the field. </p> 
  <p>After talking to Barrino following his 10:10am game, I can confidently say that the 511 guard is one of the most mature young men at that age. He shook my hand firmly and introduced himself with a Nice to meet you, sir.</p> 
  <p>On the court, Barrino is equally as mature.</p> 
  <p>His passing ability reminds me of TJ Ford at Texas. A small point guard, but has good vision and can thread the ball into tight spaces. He also has the ball handling skills to get to the basket and can be creative when finishing at the rim.</p> 
  <p>As I get older, I have to work on my jumpshot and get stronger. I also need to work on muscle memory to make my game better, he said. </p> 
  <p><em><strong>September 24th, 2011, 12:39pm</strong></em></p> 
  <p>Competition heating up as games move on into the afternoon.</p> 
  <p>Playaz took care of business once again, behind a strong backcourt performance from 2014 St. Anthony guard Jordan Forehand and the aforementioned Isaiah Briscoe. </p> 
  <p>Got a chance to talk to Forehand after the game and a feature is coming on the guards in the Playaz program and Forehands relationship with USC guard Jio Fontan.</p> 
  <p>At 3pm, two big-time games are set to tip off: New Jersey Roadrunners vs. Long Island Lightning and Mass Rivals vs. New Jersey Hot Shots.</p> 
  <p><em><strong>September 24th, 2011, 2:10pm</strong></em></p> 
  <p>Very good game going on right now between Jersey Hot Shots and Beacon Prep (GA). Its a low scoring affair, coming out of the half, but not sloppy. Beacon Prep leads 24-20. Good defense on both ends of the floor, highlighted by a monster block by guard Sajae Pryor in transition.</p> 
  <p>The Hot Shots go-to man is Stony Brook commit jam eel Warney, who has been mentioned before. The 67, 190 pound power forward will be a good fit in the America East Conference. When he shifts into high gear, he can be ferocious in the paint, both rebounding and attacking the basket.</p> 
  <p>Beacon Prep is headlined by former St. Johns and Manhattan commit, point guard Davontay Grace.</p> 
  <p>Grace is an interesting case because his skill alone could land him at a solid basketball school. The problem is, though, that he has been plagued by academic issues, as well as questions about his weight. </p> 
  <p>But, there is no denying that he has a solid foundation of skills. He has a strong crossover and can get into the lane and finish creatively at the rim. He is built similarly to LeVance Fields, formerly of the University of Pittsburgh: a large guard who can use his body well. </p> 
  <p>Noah Vonleh and Mass Rivals tip off at 3pm.</p> 
  <p><em><strong>September 24th, 2011, 3:01pm</strong></em></p> 
  <p>As Noah Vonleh and Mass Rivals tip off, another game comes down to the wire.</p> 
  <p>South Mountain Select, after being up by 10 with only a few minutes remaining, had to fend off a late comeback from Game 7 Sharks, getting the victory 69-66.</p> 
  <p><em><strong>September 24th, 2011, 3:59pm</strong></em></p> 
  <p>Noah Vonleh and Mass Rivals beat Jersey Hot Shots handily, in a game that went final moments ago.</p> 
  <p>Though Vonleh is a superb player in his own right, Mass Rivals as a whole is a team with players who complement each other, work hard on the defensive, force turnovers, and move the ball on the offensive end. </p> 
  <p>Mass Rivals three-point shooters were also on today, working off of dribble penetration from point guard Sultan Olusekun and Noah Vonleh. Caleb Joseph, in particular, knocked down a number of open shots from the perimeter, in a addition to getting into the passing lanes and forcing turnovers on the defensive end.</p> 
  <p>Of the teams I have seen today, Mass Rivals is definitely up there with teams that will be competing for the championship tomorrow.</p> 
  <p><em><strong>September 24th, 2011, 5:31pm</strong></em></p> 
  <p>The 5:20pm set of games are underway and there are two good match ups adjacent courts. On Court 1, the New England Playaz are taking on NEBC (NY) and, on Court 2, Mass Rivals plays their second game of the day, this one against the New Jersey Pirates. </p> 
  <p>Mass Rivals holds a 10-7 lead early, but it looks like the Pirates may have found a way to limit the potent Rivals offense. If the Rivals settle for jumpshots and the Pirates get to the rim and get Noah Vonleh in foul trouble, the Pirates could keep the game close.</p> 
  <p><strong><em>September 24th, 2011, 6:29pm</em></strong></p> 
  <p>The Pirates held on for a time, but, eventually, athleticism and ball movement became too much and they fall to Mass Rivals by double digits. </p> 
  <p>Noah Vonleh had a dominating physical performance, grabbing rebounds, running the floor, working off the dribble, and taking control of the game late in the second half. His performance was punctuated by two thunderous tip-slams, the second using just one hand.</p> 
  <p>Terrell Bagley shot well from long distance for the Pirates, knocking down three three-pointers and providing the spark that kept the Pirates in it for a time.</p> 
  <p>September 24th, 2011, 8:08pm</p> 
  <p>Night games in full swing at Fall Jam Fest. Just as one would think players are getting tired, the competition and energy are picking up. While watching games on one side of the gym, you can hear oooos and ahhhs from the other side of the gym.</p> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>