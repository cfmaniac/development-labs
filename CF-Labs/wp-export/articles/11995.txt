<article id="post-11995" class="post-11995 post type-post status-publish format-standard has-post-thumbnail hentry category-tournament-recap tag-hgjamfest tag-west-virginia"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p><a href="http://hoopgroup.com/wp-content/uploads/2011/07/Team-Loaded.jpg"><img src="http://hoopgroup.com/wp-content/uploads/2011/07/Team-Loaded.jpg" alt="Team Loaded" title="Team Loaded" width="640" height="425" class="alignleft size-full wp-image-11999"></a></p> 
  <p><strong>15U Team Scan (NY) v Team Loaded (VA)</strong></p> 
  <p>When players, parents, and fans took notice of the 15U championship game, they looked on in surprise. Why? </p> 
  <p>They didnt see these teams all weekend nor did they know they were only 15 years old. Everyone was shocked when they found out that this was the 15U championship . Both Team Scan and Team Loaded feature a plethora of talent that could give some 17U teams a run for their money. </p> 
  <p>Team Scan was led by point guard Shavar Newkirk and power forward Chris McCullough who both finished with 13 points a piece. This wasnt nearly enough to stop the balanced scoring attack of Team Loaded which had 9 different players score (6 players who scored more than 5 points). </p> 
  <p>Team Loaded led by 3 points at the half (20-17) but their deep roster wore down Team Scan in the 2nd half. The lead kept extending and eventually ended with a 9 point margin 53-44. </p> 
  <p><a href="http://hoopgroup.com/wp-content/uploads/2011/07/OBC.jpg"><img src="http://hoopgroup.com/wp-content/uploads/2011/07/OBC.jpg" alt="" title="OBC" width="640" height="425" class="alignleft size-full wp-image-11998"></a></p> 
  <p><strong>16U East Coast Elite (DE) vs Ohio Basketball Club (OH)</strong></p> 
  <p>East Coast Basketball expended a ton of energy on and off the floor. Every single member of the 16U team was in the stands cheering for the 17U team during bracket play. It was one of those moments that really make your cherish AAU basketball. After they rooted their elders in the program they carried their energy on the floor shocking Team Final (PA) in the semis. </p> 
  <p>As this went on Ohio Basketball Club was battling against Team Loaded (VA). OBC proved to be too much for Team Loaded moving on to play EOC. </p> 
  <p>Ohio Basketball Club took control of the game early on against East Coast Elite and never really looked back. 65 Anthony Dallier paced OBC with 13 points including three treys. Elijah Brown ran the show while Kash Blackwell did a lot of the blue collar work down low. </p> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>