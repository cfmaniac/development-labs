<article id="post-53204" class="post-53204 post type-post status-publish format-standard has-post-thumbnail hentry category-tournaments tag-hgjamfest tag-aau-basketball tag-hoop-group tag-spooky-nook tag-summer-jam-fest-2"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p>One tournament ends and another begins. Just as Summer Classic East was put to bed, Summer Jam Fest woke up roaring and ready to go. At 3 pm Friday, AAU teams ages 14, 15, 16 and 17 will be coming in and competing for a chance to become Summer Jam Fest champions. Stay will us as we provide coverage and updates for all teams involved on all 22 courts here in Spooky Nook.</p> 
  <p><em><strong>Sunday July 20: DAY 3</strong></em></p> 
  <p><strong>4:30 pm</strong><br> Were all set here in Manheim. Congratulations to all 4 of our champions! A lot of praise and recognition is deserved after coming out on top of a tough bracket this weekend. For full recaps and coverage of this weekend, be sure to check out <a href="http://www.facebook.com/thehoopgroup" title="Facebook" target="_blank">Facebook</a>, <a href="http://www.twitter.com/thehoopgroup" title="Twitter" target="_blank">Twitter</a>, <a href="http://www.instagram.com/hoopgroup" title="Instagram" target="_blank">Instagram</a> and the <a href="http://hoopgroup.com/news/" title="blog section" target="_blank">Blog section</a> of our website. Also, be on the look out for mix-tapes on our new <a href="https://www.youtube.com/user/HoopGroupBBall" title="Youtube channel" target="_blank">Youtube channel</a>. Well be back at Albright this week for Elite Camp session 2 and then in Neptune for Buzzer Beater Classic next weekend. </p> 
  <p><strong>4:22 pm</strong><br> And thats a wrap! Our last 2 winners are crowned. Congratulations to 15U DC Thunder and 17U Expressions Elite (pictured below)<br> <iframe src="//instagram.com/p/qr6A7qQ8mN/embed/" width="612" height="710" frameborder="0" scrolling="no" allowtransparency="true"></iframe></p> 
  <p><strong>4:03 pm</strong><br> Expressions creating separation. </p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck"> 
   <p>Under 4 to go. Expressions Elite holds a 6 point lead with the ball <a href="https://twitter.com/hashtag/HGJamFest?src=hash">#HGJamFest</a></p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/statuses/490951397366431744">July 20, 2014</a></p>
  </blockquote> 
  <p><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck"> 
   <p>Aaron Falzon making the difference down the stretch. Cannot be stopped right now. <a href="https://twitter.com/hashtag/HGJamFest?src=hash">#HGJamFest</a></p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/statuses/490951469294956545">July 20, 2014</a></p>
  </blockquote> 
  <p><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></p> 
  <p><strong>3:38 pm</strong><br> 2 more close games. What an extraordinary championship match ups it has been!</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck"> 
   <p>Halftime Scores: York Ballers up 25-21 on DC Thunder Expressions up 27-24 on ICC Truth <a href="https://twitter.com/hashtag/HGJamFest?src=hash">#HGJamFest</a></p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/statuses/490944532469805056">July 20, 2014</a></p>
  </blockquote> 
  <p><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></p> 
  <p><strong>3:26 pm</strong><br> Our last 2 championships of the weekend:</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck"> 
   <p>Championship games up next: 17U Expressions vs. ICC Truth 15U York Ballers vs. DC Thunder <a href="https://twitter.com/hashtag/HGJamFest?src=hash">#HGJamFest</a></p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/statuses/490936931492970497">July 20, 2014</a></p>
  </blockquote> 
  <p><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></p> 
  <p><strong>3:01 pm</strong><br> 2 champions crowned! 14U Baltimore Elite edged out Team Felton and 16U DC Thunder beat Burlington ISBA! DC Thunder (pictured below) are your Summer Classic East and Summer Jam Fest Champs!<br> <iframe src="//instagram.com/p/qr0cbHQ8p9/embed/" width="612" height="710" frameborder="0" scrolling="no" allowtransparency="true"></iframe></p> 
  <p><strong>2:31 pm</strong><br> 2 good championship games going on right now, 2 more still remain.</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p>DC Thunder holds a 33-26 lead at halftime of the 16U Championship. <a href="https://twitter.com/hashtag/HGJamFest?src=hash">#HGJamFest</a></p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/statuses/490926179096879105">July 20, 2014</a></p>
  </blockquote> 
  <p>&nbsp;</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p>Team Felton and BEWL are tied at 24 at the half. <a href="https://twitter.com/hashtag/HGJamFest?src=hash">#HGJamFest</a></p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/statuses/490926912781291520">July 20, 2014</a></p>
  </blockquote> 
  <p>&nbsp;</p> 
  <p><strong>2:08 pm</strong><br> 17U Final setExpressions Elite vs. ICC Truth<br> <iframe src="//instagram.com/p/qrrgZxw8m3/embed/" height="710" width="612" frameborder="0" scrolling="no"></iframe></p> 
  <p><strong>1:49 pm</strong><br> Few guys stepping up on the big stage that is Final Four action</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p>Branden Kellam on ICC Truth is a sleeper! Athletic and a smooth finisher around the hoop. Great court vision too. <a href="https://twitter.com/hashtag/HGJamFest?src=hash">#HGJamFest</a></p> 
   <p> Josh King (@CoachJoshKing) <a href="https://twitter.com/CoachJoshKing/statuses/490916235811373056">July 20, 2014</a></p>
  </blockquote> 
  <p>&nbsp;</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p>Jared Wilson-Frame having a good tournament. Finds a way to get the ball in the hoop. <a href="https://twitter.com/hashtag/HGJamFest?src=hash">#HGJamFest</a></p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/statuses/490914062339809280">July 20, 2014</a></p>
  </blockquote> 
  <p>&nbsp;</p> 
  <p><strong>1:14 pm</strong><br> ICC Truth has had a great run to the semifinals, and they are still going, giving the Middlesex Magic everything they got.</p> 
  <blockquote class="twitter-tweet" lang="en">
   <p>ICC Truth playing with loads of energy in their semi final game against Middlesex Magic. Great energy from guys on the court/on the bench</p> 
   <p> James Albano (@JN_Albano) <a href="https://twitter.com/JN_Albano/statuses/490906580578095106">July 20, 2014</a></p>
  </blockquote> 
  <p>&nbsp;</p> 
  <p><strong>12:56 pm</strong><br> And now our 16U Final is set as well: <strong>DC Thunder</strong> back in the championship vs. <strong>Burlington ISBA</strong>.</p> 
  <p><strong>12:54 pm</strong><br> 14U Final is set. <strong>Team Felton</strong> will face <strong>Baltimore Elite</strong> at 1:45 for the championship!</p> 
  <p><strong>12:32 pm</strong><br> Pictures from yesterday are up on Facebook! Be sure to check them out.</p> 
  <div id="fb-root"></div> 
  <p><script type="text/javascript">// <!<CDATA<
(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)<0>; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/en_US/all.js#xfbml=1"; fjs.parentNode.insertBefore(js, fjs); }(document, 'script', 'facebook-jssdk'));
// >>&gt;</script></p> 
  <div class="fb-post" data-href="https://www.facebook.com/media/set/?set=a.733857510012116.1073741957.110289042368969&amp;type=1" data-width="466"> 
   <div class="fb-xfbml-parse-ignore">
    <a href="https://www.facebook.com/media/set/?set=a.733857510012116.1073741957.110289042368969&amp;type=1">Post</a> by 
    <a href="https://www.facebook.com/thehoopgroup">The Hoop Group</a>.
   </div> 
  </div> 
  <p><strong>11:45 am</strong><br> As the 16U and 14U Final Fours get under way, your 17U and 15U Final Four is now set.</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p>Advancing to the 17U final four: ICC Truth Expressions JB Hoops Middlesex Magic <a href="https://twitter.com/hashtag/HGJamFest?src=hash">#HGJamFest</a></p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/statuses/490882489934573568">July 20, 2014</a></p>
  </blockquote> 
  <p>&nbsp;</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p>Advancing to the 15U Final Four: DC Thunder Team Nelson Sports U York Ballers <a href="https://twitter.com/hashtag/HGJamFest?src=hash">#HGJamFest</a></p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/statuses/490885574924845056">July 20, 2014</a></p>
  </blockquote> 
  <p>&nbsp;</p> 
  <p><strong>11:29 am</strong><br> DC Warriors vs. Middlesex Magic may have been the game of the weekend so far. An awesome ending and very competitive.</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p>Andrew Robinson with ice in his veins! Three-pointer with less than 20 seconds left ties it up at 50 between DC Warriors/Middlesex Magic.</p> 
   <p> Andrew Koob (@AndrewKoob) <a href="https://twitter.com/AndrewKoob/statuses/490879806163005441">July 20, 2014</a></p>
  </blockquote> 
  <p><script charset="utf-8" type="text/javascript" src="//platform.twitter.com/widgets.js" async></script><br> <iframe src="//instagram.com/p/qrZDiaw8gG/embed/" height="710" width="612" frameborder="0" scrolling="no"></iframe></p> 
  <p><strong>11:11 am</strong><br> Battle between Expressions Elite and PK Flash just heated up. Both teams fighting to advance to the Final Four.</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p> Huge run by Expressions Elite turns a 25 point deficit into a 1 point lead. Another great Elite 8 game being played now <a href="https://twitter.com/hashtag/HGJamfest?src=hash">#HGJamfest</a></p> 
   <p> James Albano (@JN_Albano) <a href="https://twitter.com/JN_Albano/statuses/490876316242432000">July 20, 2014</a> </p>
  </blockquote> 
  <p>&nbsp;</p> 
  <p><strong>11:03 am</strong><br> Defending Summer Classic East champs Middlesex Magic have their hands full with the DC Warriors. Hoop Heaven Elite and ICC Truth also in a tight game in the 2nd. Sit tight.</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p> DC Warriors and Middlesex Magic tied at the half. It is going to be a fun second half. Two teams with different styles of play. <a href="https://twitter.com/hashtag/HGJamFest?src=hash">#HGJamFest</a></p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/statuses/490870844088807424">July 20, 2014</a> </p>
  </blockquote> 
  <p>&nbsp;</p> 
  <p><strong>10:45 pm</strong><br> Speaking of repeats, Middlesex Magic 17U are playing right now versus DC Warriors. Two Magic players, Jackson Donahue and AJ Brodeur made our list of players who performed well on Saturday. See the full list here.</p> 
  <p><strong>10:19 am</strong><br> 16U Final Four is set:</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p> Advancing to the 16U final four: Burlington IBSA BC Eagles Jersey Shore Warriors DC Thunder <a href="https://twitter.com/hashtag/HGJamFest?src=hash">#HGJamFest</a></p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/statuses/490863393515917312">July 20, 2014</a> </p>
  </blockquote> 
  <p>&nbsp;</p> 
  <p><strong>10:02 am</strong><br> 16U DC Thunder having a strong showing this week. 3 days ago they won our Summer Classic East championship and now are headed to the Final Four for Summer Jam Fest! Can they make it two in a row?</p> 
  <p><strong>9:10 am</strong><br> Were up and ready to go for championship Sunday here at Spooky Nook. Playoff action starts today with 16U Elite Eight games.</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p> Warm-ups from <a href="https://twitter.com/hashtag/HGJamFest?src=hash">#HGJamFest</a> <a href="https://t.co/ShTsWKBiKM">https://t.co/ShTsWKBiKM</a></p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/statuses/490841698889506817">July 20, 2014</a> </p>
  </blockquote> 
  <p>&nbsp;</p> 
  <p><em><strong>Saturday July 19: DAY 2</strong></em></p> 
  <p><strong>10:38 pm</strong><br> Day two is in the books and we have 3 Elite Eights and 1 Final Four set for tomorrow. Lot of basketball to look forward to! Dont forget to check our <a title="twitter" href="http://www.twitter.com/thehoopgroup" target="_blank">twitter</a> feed for full recaps of the day. See you all tomorrow!</p> 
  <p><strong>10:01 pm</strong><br> Summer Classic East Champs Middlesex Magic getting contributions from a number of guys. No surprise seeing them back in the ELite Eight.</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p> AJ Brodeur 2016 stretch 4 from (Northfield Mount Hermon) is a bucket getter. Scores the ball in a variety of ways.</p> 
   <p> Josh King (@CoachJoshKing) <a href="https://twitter.com/CoachJoshKing/statuses/490671607053299712">July 20, 2014</a> </p>
  </blockquote> 
  <p>&nbsp;</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p> Collin McManus has been all over the boards tonight. Has not stopped playing hard for a second. <a href="https://twitter.com/hashtag/HGJamFest?src=hash">#HGJamFest</a></p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/statuses/490678935521210369">July 20, 2014</a> </p>
  </blockquote> 
  <p>&nbsp;</p> 
  <p><strong>9:27 pm</strong><br> Your 16U Elite Eight is set: DC Thunder vs. VA Elite, House of Sports vs. Jersey Shore Warriors (PA), Burlington ISBA vs. Hihh Rise Team Up and Expressions Elite vs. BC Eagles. Games start tomorrow at 9 am. Dont miss any of the action!</p> 
  <p><strong>9:20 pm</strong><br> More 17u games going on. Saving some of the best for last today.</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p> Tough to pick a winner in House of Sports vs. Hoop Heaven Elite, two tough TEAMS with a lot of talent. <a href="https://twitter.com/hashtag/HGJamFest?src=hash">#HGJamFest</a></p> 
   <p> James Albano (@JN_Albano) <a href="https://twitter.com/JN_Albano/statuses/490667893051633665">July 20, 2014</a> </p>
  </blockquote> 
  <p>&nbsp;</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p> Battle of the Blue on Court 1. <a href="https://twitter.com/hashtag/MiddlesexMagic?src=hash">#MiddlesexMagic</a> <a href="https://twitter.com/hashtag/TeamPA?src=hash">#TeamPA</a> <a href="https://twitter.com/hashtag/HGJamFest?src=hash">#HGJamFest</a> <a href="http://t.co/piYyeLr7oU">pic.twitter.com/piYyeLr7oU</a></p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/statuses/490667090211127296">July 20, 2014</a> </p>
  </blockquote> 
  <p>&nbsp;</p> 
  <p><strong>8:03 pm</strong><br> Two guys stepping up for their teams in crucial games right now.</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p> JB Hoops continues to tear it up. This had been a big weekend for Ian Langendorfer. <a href="https://twitter.com/hashtag/HGJamFest?src=hash">#HGJamFest</a></p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/statuses/490641068577140736">July 19, 2014</a> </p>
  </blockquote> 
  <p>&nbsp;</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p> Donte Williams of ICC Truth is a big-time athlete. Fun to watch make acrobatic finishes. <a href="https://twitter.com/hashtag/HGJamFest?src=hash">#HGJamFest</a></p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/statuses/490650866609700864">July 20, 2014</a> </p>
  </blockquote> 
  <p>&nbsp;</p> 
  <p><strong>7:21 pm</strong><br> PK Flash advance to the Elite Eight and will face Expressions Elite tomorrow morning.</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p> PK Flash with the win over NYC Jayhawks. Theyve got some guys who can put it in the basket. <a href="https://twitter.com/hashtag/HGJamFest?src=hash">#HGJamFest</a></p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/statuses/490631942719557632">July 19, 2014</a> </p>
  </blockquote> 
  <p>&nbsp;</p> 
  <p><strong>6:49 pm</strong><br> 16U House of Sports won a close one versus Team Philly; they overcame a 10 point game deficit and won 68-62 in an awesome game. They advance!</p> 
  <p><strong>6:40 pm</strong><br> Been a solid weekend of basketball for Isaiah Lamb, coaches should take notes.</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p> If Im a HM coach Im giving Isaiah Still 15 of Union Catholic a hard look. Kid can do it all at 67 : pass, finish, and shoot.</p> 
   <p> Josh King (@CoachJoshKing) <a href="https://twitter.com/CoachJoshKing/statuses/490619433787064320">July 19, 2014</a> </p>
  </blockquote> 
  <p>&nbsp;</p> 
  <p><strong>6:02 pm</strong><br> Our 17U Sweet 16 is set. Expressions Elite, NJ Got Game, NYC Jayhawks, PK Flash, NY Blazers, Syracuse Select, High Rise Team Up, Baltimore Elite, ICC Truth, Middlesex Magic, House of Sports, Hoop Heaven Elite, Team PA, Stamford Peace and DC Warriors all still alive to become champs. Sweet 16 action starting now! <a title="@JN_Albano" href="http://www.twitter.com/jn_albano" target="_blank">@JN_Albano</a></p> 
  <p><strong>5:01 pm</strong><br> Its been a great weekend so far for JB Hoops. 3 straight wins has them in the sweet 16. Can they keep the streak rolling?</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p> 17u JB Hoops will have a chance to continue their hot streak when they take on High Rise Team Up at 6:30. Winner to the Elite 8 <a href="https://twitter.com/hashtag/HGJamFest?src=hash">#HGJamFest</a></p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/statuses/490602282858205186">July 19, 2014</a> </p>
  </blockquote> 
  <p>&nbsp;</p> 
  <p><strong>4:39 pm</strong><br> This place is crowded right now! Great environment for players to play in.<br> <iframe src="//instagram.com/p/qpXWK3Q8mi/embed/" height="710" width="612" frameborder="0" scrolling="no"></iframe><br> <strong>3:58 pm</strong><br> 14U action underway now and all 4 brackets are firing on all cylinders!</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p> Sizzle defeats Advance Hoops 71-42 in the Summer Jam Fest. <a href="https://twitter.com/TheHoopGroup">@TheHoopGroup</a> Next gm at 5:20 on court 21</p> 
   <p> Team Sizzle Hoops (@SIZZLEAAU) <a href="https://twitter.com/SIZZLEAAU/statuses/490584507108454401">July 19, 2014</a> </p>
  </blockquote> 
  <p>&nbsp;</p> 
  <p><strong>3:31 pm</strong><br> Great match up coming up soon. Cant wait.</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p> And Baltimore Elite advances after a pair of free throws fall their way. Summer Classic East champs Middlesex Magic up next.</p> 
   <p> Andrew Koob (@AndrewKoob) <a href="https://twitter.com/AndrewKoob/statuses/490576448701210624">July 19, 2014</a> </p>
  </blockquote> 
  <p>&nbsp;</p> 
  <p><strong>2:54 pm</strong><br> Basketball is everywhere!</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p> A panoramic view of <a href="https://twitter.com/hashtag/HGJamFest?src=hash">#HGJamFest</a>. <a href="https://t.co/AboSbNIg2l">https://t.co/AboSbNIg2l</a></p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/statuses/490569695289151488">July 19, 2014</a> </p>
  </blockquote> 
  <p>&nbsp;</p> 
  <p><strong>2:24 pm</strong><br> 3 players from Alexandria Kings had themselves a day today. <strong>John Trey Pride</strong> had 16 points and 9 assists, <strong>Jared Clipper</strong> had 8 points and 12 rebounds and <strong>John Upkama</strong> has 14 points and 14 boards in their win earlier. All 3 players already have interest from some schools and drew more this afternoon. Great day to be a King.</p> 
  <p><strong>1:37pm</strong><br> The Shoreshots and PK Flash (PA) are in a heated back-and-forth affair with just under six minutes left to play. Who will step up to be the hero and lead their team into the 17U Elite 8?</p> 
  <p><strong>12:37pm</strong><br> Despite a 23-point performance from Nate Sestina, NJ Got Game 17U was able to whittle down their deficit and walk out with a 66-61 victory. That sets up a much anticipated matchup with Expressions Elite, who took a double-digit victory against Force 1. NJ Got Game and Expressions Elite will meet at 5:20 today on Court 1. Be there!</p> 
  <p><strong>12:14pm</strong><br> BSA Select is trying to put NJ Got Game into the consolation brackets. With six minutes left, can they extend their winning streak into the Elite 8?</p> 
  <blockquote class="twitter-tweet" lang="en">
   <p> Big slam from 6-9 Nate Sestina from BSA select has them up by five against NJ Got Game.</p> 
   <p> Andrew Koob (@AndrewKoob) <a href="https://twitter.com/AndrewKoob/statuses/490529745218461696">July 19, 2014</a> </p>
  </blockquote> 
  <p><script charset="utf-8" type="text/javascript" src="//platform.twitter.com/widgets.js" async></script><strong>11:57 am</strong> Theres a reason Expressions Elite forward Aaron Falzon has plenty of Divison I coaches watching him right now. His three at the end of the first half gives Expressions a little breathing room against Force 1 as they lead 28-23 at the break.</p> 
  <p><strong>11:07 am</strong> Team Jersey has had an uphill battle much of the way, but have found a way to take a lead late. Can they hold on?</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p>Team Jersey is now firing on all cylinders and has gained a two point lead. <a href="https://twitter.com/hashtag/HGJamfest?src=hash">#HGJamfest</a>  Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/statuses/490512941176086529">July 19, 2014</a></p>
  </blockquote> 
  <p><strong>10:54 am</strong> Tons of close games early. Lot of good match ups between teams have fans enjoying their Saturday morning. <iframe src="https://vine.co/v/MQVeIwZd2aA/embed/simple" height="600" width="600" frameborder="0"></iframe><script charset="utf-8" type="text/javascript" src="//platform.vine.co/static/scripts/embed.js" async></script><strong>10:03 am</strong> Flashback to Friday and see who some of our top performers from last night were here&gt;&gt; <a title="http://hoopgroup.com/tournaments/summer-jam-fest-day-1-top-performers/" href="http://hoopgroup.com/tournaments/summer-jam-fest-day-1-top-performers/" target="_blank">http://hoopgroup.com/tournaments/summer-jam-fest-day-1-top-performers/ </a> <strong>9:38 am</strong> House of Sports 16U having a strong showing thus far. Peter Kiss and Emmanuel Chukwu making plays for them as they lead a younger 15U Team Final squad playing up.</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p> Peter Kiss gets the alley oop feed and throws it down. Seen a lot of him this summer. Should have more offers coming his way <a href="https://twitter.com/hashtag/HGJamfest?src=hash">#HGJamfest</a>  James Albano (@JN_Albano) <a href="https://twitter.com/JN_Albano/statuses/490487949797380096">July 19, 2014</a> </p>
  </blockquote> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p> Emmanuel Chukwu playing well for the House of Sports 16U team. Blocking and altering shots at the rim. <a href="https://twitter.com/hashtag/HGJamFest?src=hash">#HGJamFest</a>  Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/statuses/490487365815640064">July 19, 2014</a> </p>
  </blockquote> 
  <p><strong>9:15 am</strong> Great match up between Team Pennsylvania and NYC Jayhawks to start the morning. Scoring coming early so far. <iframe src="https://vine.co/v/MQ9Lr3TtFVd/embed/postcard" height="600" width="600" frameborder="0"></iframe><script charset="utf-8" type="text/javascript" src="//platform.vine.co/static/scripts/embed.js" async></script><strong>9:02 am</strong> Back at it for day 2 here at Spooky. Starting our day with some 16U action, featuring teams such as DC Thunder, NYC Jayhawks, Team PA and the 15U Team Final and Expressions squads, to name a few. Get ready for an action packed day of basketball! <em><strong>Friday, July 18: DAY 1</strong></em> <strong>10:51 pm</strong> Were all wrapped up here in Spooky for Day 1. For full coverage, pictures and more check us out on <a title="Facebook" href="http://www.facebook.com/thehoopgroup" target="_blank">Facebook</a>, <a title="Twitter" href="http://www.twitter.com/thehoopgroup" target="_blank">Twitter</a> and <a title="Instagram" href="http://www.instagram.com/hoopgroup" target="_blank">Instagram</a>. <strong>10:36 pm</strong> We have uploaded some of our pictures from today onto our Facebook page. Be sure to check them out!</p> 
  <div id="fb-root"></div> 
  <p><script type="text/javascript">// <!<CDATA<
(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)<0>; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/en_US/all.js#xfbml=1"; fjs.parentNode.insertBefore(js, fjs); }(document, 'script', 'facebook-jssdk'));
// >>&gt;</script></p> 
  <div class="fb-post" data-href="https://www.facebook.com/media/set/?set=a.733857510012116.1073741957.110289042368969&amp;type=1" data-width="466"> 
   <div class="fb-xfbml-parse-ignore">
    <a href="https://www.facebook.com/media/set/?set=a.733857510012116.1073741957.110289042368969&amp;type=1">Post</a> by 
    <a href="https://www.facebook.com/thehoopgroup">The Hoop Group</a>.
   </div> 
  </div> 
  <p><strong>10:12 pm</strong><br> Two things you know Wayne PAL will always do: play a very structured game and bring great jerseys.<br> <iframe src="//instagram.com/p/qnWi3BQ8q5/embed/" height="710" width="612" frameborder="0" scrolling="no"></iframe></p> 
  <p><strong>9:47 pm</strong><br> BC Eagles seal the deal. One game set left on the day.</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p>Darnell Edge helps seal the deal with these two free throws for the BC Eagles. <a href="https://twitter.com/hashtag/HGJamFest?src=hash">#HGJamFest</a> <a href="https://t.co/h27qSgaF7u">https://t.co/h27qSgaF7u</a></p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/statuses/490310842576302081">July 19, 2014</a></p>
  </blockquote> 
  <p>&nbsp;</p> 
  <p><strong>9:13 pm</strong><br> Summer Classsic East champs, the Middlesex Magic, set to start their Summer Jam run now.</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p>Middlesex Magic riding off of their momentum from Summer Classic East. Up 18 in their 1st round game. <a href="https://twitter.com/hashtag/HGJamFest?src=hash">#HGJamFest</a> <a href="http://t.co/1Ouxlanc6O">pic.twitter.com/1Ouxlanc6O</a></p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/statuses/490308665707331584">July 19, 2014</a></p>
  </blockquote> 
  <p>&nbsp;</p> 
  <p><strong>8:39 pm</strong><br> NJ Got Game was pushed to the limit by HC Dream Team, but 2 clutch free throws late by Corey Tate helped them hang on. They will advance.</p> 
  <p><strong>8:16 pm</strong><br> PA Renegades were a final four team in Summer Classic East, can they do the same this weekend?</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p>Been impressed with Matt Penecales play as of late. Was great in Renegades loss last night, bringing great energy here today.</p> 
   <p> Andrew Koob (@AndrewKoob) <a href="https://twitter.com/AndrewKoob/statuses/490266297494564864">July 18, 2014</a></p>
  </blockquote> 
  <p>&nbsp;</p> 
  <p><strong>7:43 pm</strong><br> 2 teams who are a threat to win it all in the 17U bracket are playing right now. They are Expressions Elite and NJ Roadrunners. 2 very well-coached and established programs and they look good so far. <a title="@TheHoopGroup" href="http://www.twitter.com/thehoopgroup" target="_blank">@TheHoopGroup</a></p> 
  <p><strong>7:19 pm</strong><br> Tough to pack a house in this giant facility, but were doing a pretty good job so far!<br> <iframe src="//instagram.com/p/qm_yghQ8pG/embed/" height="710" width="612" frameborder="0" scrolling="no"></iframe></p> 
  <p><strong>6:58 pm</strong><br> Big game drawing a lot of coaches.</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p>Lot of mid-major coaches have their eyes on this NYC Jayhawks vs. Team Supernatural match-up. <a href="https://twitter.com/hashtag/HGJamFest?src=hash">#HGJamFest</a> <a href="http://t.co/4TChk1ZT0A">pic.twitter.com/4TChk1ZT0A</a></p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/statuses/490267309257527298">July 18, 2014</a></p>
  </blockquote> 
  <p>&nbsp;</p> 
  <p><strong>6:03 pm</strong><br> Couple more guys having good games</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p>Basketball 2 The Limit with the 63-59 first round win. Eric Demers was hot from deep! <a href="https://twitter.com/hashtag/HGJamFest?src=hash">#HGJamFest</a></p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/statuses/490264794663231488">July 18, 2014</a></p>
  </blockquote> 
  <p>&nbsp;</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p>Great game between Syracuse Select and F.A.B Phenoms. Teams fought hard down the stretch. Connor Evans helped SS win w/ 22 2nd half pts.</p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/statuses/490252824496726016">July 18, 2014</a></p>
  </blockquote> 
  <p>&nbsp;</p> 
  <p><strong>4:31 pm</strong><br> Wild games already here in Spooky! Overtimes and buzzer beaters in our first game set.</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p>Pat Murray (Fordham Prep 15) hits a crazy shot at the buzzer and the Westchester Knights top the Philly Ballhawks 73-71 <a href="https://twitter.com/hashtag/HGJamfest?src=hash">#HGJamfest</a></p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/statuses/490232903582744577">July 18, 2014</a></p>
  </blockquote> 
  <p>&nbsp;</p> 
  <p><strong>4:03 pm</strong><br> Dont forget to follow along on Twitter for full coverage using the hashtag <a title="#HGJamfest" href="https://twitter.com/search?f=realtime&amp;q=%23HGJamfest&amp;src=typd" target="_blank">#HGJamfest</a></p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p>Dont forget to tweet with the <a href="https://twitter.com/hashtag/HGJamFest?src=hash">#HGJamFest</a> hashtag this weekend if you are at Summer JamFest!</p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/statuses/490221946073001984">July 18, 2014</a></p>
  </blockquote> 
  <p>&nbsp;</p> 
  <p><strong>3:44 pm</strong><br> Already so big time performers in our first game set, playing against one another!</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p>10 minutes into the game and Kahlil Newkirk has 5 threes and a total of 17 points for FBCG Dynamic Disciples. Incredible start <a href="https://twitter.com/hashtag/HGJamfest?src=hash">#HGJamfest</a></p> 
   <p> Hoop Group (@TheHoopGroup) <a href="https://twitter.com/TheHoopGroup/statuses/490215122413953024">July 18, 2014</a></p>
  </blockquote> 
  <p>&nbsp;</p> 
  <blockquote class="twitter-tweet" data-partner="tweetdeck">
   <p>CC Thunder in a tight game with FBCG Disciples right now. Benjamin Casanova getting to the line time and time again. Hes 10-12. <a href="https://twitter.com/hashtag/HGJamfest?src=hash">#HGJamfest</a></p> 
   <p> James Albano (@JN_Albano) <a href="https://twitter.com/JN_Albano/statuses/490219466446225411">July 18, 2014</a></p>
  </blockquote> 
  <p>&nbsp;</p> 
  <p><strong>3:11 pm</strong><br> Big shout out to Spooky Nook Sports for their tremendous facility. 22 courts under one roof, makes running such a big tournament easier!<br> <iframe src="https://vine.co/v/MQFgz0i6FLY/embed/simple" height="600" width="600" frameborder="0"></iframe><script charset="utf-8" type="text/javascript" src="//platform.vine.co/static/scripts/embed.js" async></script></p> 
  <p><strong>3:00 pm</strong><br> And we are underway! First game sets have started and we cannot wait to see what this weekend haas in store for us. 4 age divisions, 22 courts, lets get it started. <a title="@TheHoopGroup" href="http://www.twitter.com/thehoopgroup" target="_blank">@TheHoopGroup</a></p> 
  <p><strong>2:31 pm</strong><br> Are you ready? Less than a half hour away from the tip of Summer Jam Fest!<br> http://instagram.com/p/qmfzq9w8kK/</p> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>