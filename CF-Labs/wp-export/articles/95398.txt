<article id="post-95398" class="post-95398 post type-post status-publish format-standard has-post-thumbnail hentry category-alumni-updates"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p><img class="aligncenter size-full wp-image-94913" src="https://hoopgroup.com/wp-content/uploads/2018/03/milesbridges2.jpg" alt="" width="280" height="203"></p> 
  <p style="text-align: center;">All American Miles Bridges of Michigan State</p> 
  <p><em>Dreams Do Come True</em></p> 
  <p>Many Hoop Group alum have succeeded at many different levels, from the NBA, Division 1 as well as small colleges and in high school. On this page we look at the NCAA All-Americans who are Hoop Group Alumni.</p> 
  <p>HOOP GROUP ALUMNI 2018 NCAA AP ALL-AMERICANS</p> 
  <table id="tablepress-1622" class="tablepress tablepress-id-1622"> 
   <thead> 
    <tr class="row-1 odd"> 
     <th class="column-1">&nbsp;</th>
     <th class="column-2">Name</th>
     <th class="column-3">College</th> 
    </tr> 
   </thead> 
   <tbody> 
    <tr class="row-2 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2016/11/JalenBrunson100.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-80267"></td>
     <td class="column-2">Jalen Brunson</td>
     <td class="column-3">Villanova</td> 
    </tr> 
    <tr class="row-3 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2016/11/Devontegraham100.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-80529"></td>
     <td class="column-2">Devonte Graham</td>
     <td class="column-3">Kansas</td> 
    </tr> 
    <tr class="row-4 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2015/10/Miles-Bridges100.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-65042"></td>
     <td class="column-2">Miles Bridges</td>
     <td class="column-3">Michigan State</td> 
    </tr> 
    <tr class="row-5 odd"> 
     <td class="column-1"></td>
     <td class="column-2">Mikal Bridges</td>
     <td class="column-3">Villanova</td> 
    </tr> 
    <tr class="row-6 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2013/11/JaylenAdams100.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-45041"></td>
     <td class="column-2">Jaylen Adams</td>
     <td class="column-3">St. Bonaventure</td> 
    </tr> 
    <tr class="row-7 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2013/11/PeytonAldridge100.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-44981"></td>
     <td class="column-2">Peyton Aldridge</td>
     <td class="column-3">Davidson</td> 
    </tr> 
    <tr class="row-8 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2017/01/Mohammed-Bamba-100.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-82449"></td>
     <td class="column-2">Mo Bamba</td>
     <td class="column-3">Texas</td> 
    </tr> 
    <tr class="row-9 odd"> 
     <td class="column-1"></td>
     <td class="column-2">Trae Bell-Haynes</td>
     <td class="column-3">Vermont</td> 
    </tr> 
    <tr class="row-10 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2015/11/Tony-Carr.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-66397"></td>
     <td class="column-2">Tony Carr</td>
     <td class="column-3">Penn State</td> 
    </tr> 
    <tr class="row-11 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2014/02/GaryClark100.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-47070"></td>
     <td class="column-2">Gary Clark</td>
     <td class="column-3">Cincinnati</td> 
    </tr> 
    <tr class="row-12 even"> 
     <td class="column-1"></td>
     <td class="column-2">Jermaine Crumpton</td>
     <td class="column-3">Canisius</td> 
    </tr> 
    <tr class="row-13 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2014/02/AngelDelgadosmall.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-47071"></td>
     <td class="column-2">Angel Delgado</td>
     <td class="column-3">Seton Hall</td> 
    </tr> 
    <tr class="row-14 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2018/03/kahlildukes100.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-94883"></td>
     <td class="column-2">Kahlil Dukes</td>
     <td class="column-3">Niagara</td> 
    </tr> 
    <tr class="row-15 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2014/05/Calebmartinsmall.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-51054"></td>
     <td class="column-2">Caleb Martin</td>
     <td class="column-3">Nevada</td> 
    </tr> 
    <tr class="row-16 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2016/11/KelanMartin100.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-80320"></td>
     <td class="column-2">Kelan Martin</td>
     <td class="column-3">Butler</td> 
    </tr> 
    <tr class="row-17 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2018/04/shamorieponds100.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-95440"></td>
     <td class="column-2">Shamorie Ponds</td>
     <td class="column-3">St. John's</td> 
    </tr> 
    <tr class="row-18 even"> 
     <td class="column-1"></td>
     <td class="column-2">Jerome Robinson</td>
     <td class="column-3">Boston College</td> 
    </tr> 
    <tr class="row-19 odd"> 
     <td class="column-1"></td>
     <td class="column-2">Zach Thomas</td>
     <td class="column-3">Bucknell</td> 
    </tr> 
    <tr class="row-20 even"> 
     <td class="column-1"></td>
     <td class="column-2">Seth Towns</td>
     <td class="column-3">Harvard</td> 
    </tr> 
    <tr class="row-21 odd"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2016/11/AllonzoTrier100-1.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-80532"></td>
     <td class="column-2">Allonzo Trier</td>
     <td class="column-3">Arizona</td> 
    </tr> 
    <tr class="row-22 even"> 
     <td class="column-1"><img src="https://hoopgroup.com/wp-content/uploads/2018/04/JustinWrightForeman100-1.jpg" alt="" width="100" height="120" class="aligncenter size-full wp-image-95388"></td>
     <td class="column-2">Justin Wright-Foreman</td>
     <td class="column-3">Hofstra</td> 
    </tr> 
   </tbody> 
  </table> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>