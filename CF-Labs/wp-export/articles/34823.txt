<article id="post-34823" class="post-34823 post type-post status-publish format-standard has-post-thumbnail hentry category-elite-camp-updates tag-elite-2 tag-hoop-group-elite-camp"> 
 <!-- .entry-header --> 
 <div class="entry-content"> 
  <p><a href="https://hoopgroup.com/wp-content/uploads/2013/07/DSC06748-e1373318513165.jpg"><img class="aligncenter size-full wp-image-37515" alt="DSC06748" src="https://hoopgroup.com/wp-content/uploads/2013/07/DSC06748-e1373318513165.jpg" width="600" height="399"></a></p> 
  <h3>NBA&nbsp; Awards 
   <table id="tablepress-497" class="tablepress tablepress-id-497"> 
    <thead> 
     <tr class="row-1 odd"> 
      <th class="column-1">Award</th>
      <th class="column-2">Name</th>
      <th class="column-3">Height</th>
      <th class="column-4">School</th>
      <th class="column-5">Graduation Year</th> 
     </tr> 
    </thead> 
    <tbody> 
     <tr class="row-2 even"> 
      <td class="column-1">Most Outstanding Player</td>
      <td class="column-2">Abdul Lewis</td>
      <td class="column-3">6-8</td>
      <td class="column-4">East Side (N.J.) High School</td>
      <td class="column-5">2014</td> 
     </tr> 
     <tr class="row-3 odd"> 
      <td class="column-1">Most Improved Player</td>
      <td class="column-2">Sam Light</td>
      <td class="column-3">5-11</td>
      <td class="column-4">Northern Lebanon (Pa.) High School</td>
      <td class="column-5">2014</td> 
     </tr> 
     <tr class="row-4 even"> 
      <td class="column-1">Mr. Hustle</td>
      <td class="column-2">Karo Adjekughele</td>
      <td class="column-3">6-6</td>
      <td class="column-4">Fordham (N.Y.) Prep School</td>
      <td class="column-5">2014</td> 
     </tr> 
     <tr class="row-5 odd"> 
      <td class="column-1">All-Star MVP</td>
      <td class="column-2">Akbar Hoffman</td>
      <td class="column-3">5-11</td>
      <td class="column-4">Newark (N.J) Eastside High School </td>
      <td class="column-5">2014</td> 
     </tr> 
     <tr class="row-6 even"> 
      <td class="column-1">Station Star</td>
      <td class="column-2">Josh Reed</td>
      <td class="column-3">6-6</td>
      <td class="column-4">Arlee (Mont.) High School</td>
      <td class="column-5">2014</td> 
     </tr> 
     <tr class="row-7 odd"> 
      <td class="column-1">Sweatshop Star</td>
      <td class="column-2">Johnny Perterson</td>
      <td class="column-3">6-3</td>
      <td class="column-4">St. Augustine (Ca.) High School</td>
      <td class="column-5">2014</td> 
     </tr> 
    </tbody> 
   </table> </h3> 
  <p><a href="https://hoopgroup.com/wp-content/uploads/2013/07/DSC06760-e1373318573128.jpg"><img class="aligncenter size-full wp-image-37512" alt="DSC06760" src="https://hoopgroup.com/wp-content/uploads/2013/07/DSC06760-e1373318573128.jpg" width="600" height="399"></a></p> 
  <h3>NCAA Awards</h3> 
  <table id="tablepress-498" class="tablepress tablepress-id-498"> 
   <thead> 
    <tr class="row-1 odd"> 
     <th class="column-1">Award</th>
     <th class="column-2">Name</th>
     <th class="column-3">Height</th>
     <th class="column-4">School</th>
     <th class="column-5">Graduation Year</th> 
    </tr> 
   </thead> 
   <tbody> 
    <tr class="row-2 even"> 
     <td class="column-1">Most Outstanding Player</td>
     <td class="column-2">Cory Hicks</td>
     <td class="column-3">6-0</td>
     <td class="column-4">Summit Academy North (Mich.) High School</td>
     <td class="column-5">2015</td> 
    </tr> 
    <tr class="row-3 odd"> 
     <td class="column-1">Most Improved Player</td>
     <td class="column-2">Domonique Senet</td>
     <td class="column-3">6-6</td>
     <td class="column-4">Elmont Memorial (N.Y.) High School</td>
     <td class="column-5">2015</td> 
    </tr> 
    <tr class="row-4 even"> 
     <td class="column-1">Mr. Hustle</td>
     <td class="column-2">Raul Barbosa</td>
     <td class="column-3">6-4</td>
     <td class="column-4">Reading (Pa.) High School</td>
     <td class="column-5">2015</td> 
    </tr> 
    <tr class="row-5 odd"> 
     <td class="column-1">All-Star MVP</td>
     <td class="column-2">Dwight Whitlock</td>
     <td class="column-3">6-1</td>
     <td class="column-4">Central Dauphin East (Pa.) High School</td>
     <td class="column-5">2015</td> 
    </tr> 
    <tr class="row-6 even"> 
     <td class="column-1">Station Star</td>
     <td class="column-2">Nicholas Gittings</td>
     <td class="column-3">6-6</td>
     <td class="column-4">Good Counsel (Md.) High School</td>
     <td class="column-5">2016</td> 
    </tr> 
    <tr class="row-7 odd"> 
     <td class="column-1">Sweatshop Star</td>
     <td class="column-2">Phill Polanco</td>
     <td class="column-3">5-2</td>
     <td class="column-4">Pleasantville (N.J.) High School</td>
     <td class="column-5">2015</td> 
    </tr> 
   </tbody> 
  </table> 
  <h3>Early Bird Workout Stars</h3> 
  <table id="tablepress-499" class="tablepress tablepress-id-499"> 
   <thead> 
    <tr class="row-1 odd"> 
     <th class="column-1">Name</th>
     <th class="column-2">Height</th>
     <th class="column-3">School</th>
     <th class="column-4">Graduation Year</th> 
    </tr> 
   </thead> 
   <tbody> 
    <tr class="row-2 even"> 
     <td class="column-1">Khiron Anderson</td>
     <td class="column-2">5-11</td>
     <td class="column-3">Bayside (N.Y.) High School</td>
     <td class="column-4">2015</td> 
    </tr> 
    <tr class="row-3 odd"> 
     <td class="column-1">Thomas Bacchia</td>
     <td class="column-2">5-11</td>
     <td class="column-3">Cardinal O' Hara (Pa.) High School</td>
     <td class="column-4">2015</td> 
    </tr> 
    <tr class="row-4 even"> 
     <td class="column-1">Spencer Bartron</td>
     <td class="column-2">6-2</td>
     <td class="column-3">Caesar Rodney (Del.) High School</td>
     <td class="column-4">2014</td> 
    </tr> 
    <tr class="row-5 odd"> 
     <td class="column-1">Gregg Boyd</td>
     <td class="column-2">5-9</td>
     <td class="column-3">Grace Brethren (Md.) High School</td>
     <td class="column-4">2016</td> 
    </tr> 
    <tr class="row-6 even"> 
     <td class="column-1">Derryn Bronstein</td>
     <td class="column-2">5-9</td>
     <td class="column-3">Park Ridge (N.J.) High School</td>
     <td class="column-4">2014</td> 
    </tr> 
    <tr class="row-7 odd"> 
     <td class="column-1">Zach Brunner</td>
     <td class="column-2">6-4</td>
     <td class="column-3">Mechanicsburg (Pa.) High School</td>
     <td class="column-4">2014</td> 
    </tr> 
    <tr class="row-8 even"> 
     <td class="column-1">Andre Butler</td>
     <td class="column-2">6-1</td>
     <td class="column-3">Eleanor Roosevelt (Md.) High School</td>
     <td class="column-4">2015</td> 
    </tr> 
    <tr class="row-9 odd"> 
     <td class="column-1">Michael Crowley</td>
     <td class="column-2">6-2</td>
     <td class="column-3">Saddle River (N.J.) Day School</td>
     <td class="column-4">2015</td> 
    </tr> 
    <tr class="row-10 even"> 
     <td class="column-1">Vincent Debellis</td>
     <td class="column-2">6-1</td>
     <td class="column-3">Dwight-Englewood (N.J.) School</td>
     <td class="column-4">2015</td> 
    </tr> 
    <tr class="row-11 odd"> 
     <td class="column-1">Jesse Dwyer</td>
     <td class="column-2">6-1</td>
     <td class="column-3">Medgar Evans (N.Y.) College Prep</td>
     <td class="column-4">2014</td> 
    </tr> 
    <tr class="row-12 even"> 
     <td class="column-1">Andrew Eudy</td>
     <td class="column-2">6-6</td>
     <td class="column-3">Cedar Crest (Pa.) High School</td>
     <td class="column-4">2014</td> 
    </tr> 
    <tr class="row-13 odd"> 
     <td class="column-1">Matthew Flood</td>
     <td class="column-2">6-2</td>
     <td class="column-3">Cresskill (N.J.) High School</td>
     <td class="column-4">2015</td> 
    </tr> 
    <tr class="row-14 even"> 
     <td class="column-1">Michael Howard</td>
     <td class="column-2">6-2</td>
     <td class="column-3">Grove City Christian (Oh.) High School</td>
     <td class="column-4">2014</td> 
    </tr> 
    <tr class="row-15 odd"> 
     <td class="column-1">Curtis Jenkins</td>
     <td class="column-2">6-2</td>
     <td class="column-3">Farmingdale (N.Y.) High School</td>
     <td class="column-4">2014</td> 
    </tr> 
    <tr class="row-16 even"> 
     <td class="column-1">Jesse Justice</td>
     <td class="column-2">5-11</td>
     <td class="column-3">Reading (Pa.) High School</td>
     <td class="column-4">2014</td> 
    </tr> 
    <tr class="row-17 odd"> 
     <td class="column-1">John Kassis</td>
     <td class="column-2">6-0</td>
     <td class="column-3">Nazareth (Pa.) High School</td>
     <td class="column-4">2014</td> 
    </tr> 
    <tr class="row-18 even"> 
     <td class="column-1">Justin Kellman</td>
     <td class="column-2">6-3</td>
     <td class="column-3">Reading (Pa.) High School</td>
     <td class="column-4">2014</td> 
    </tr> 
    <tr class="row-19 odd"> 
     <td class="column-1">Randy Kincel</td>
     <td class="column-2">6-2</td>
     <td class="column-3">Riverside (Pa.) High School</td>
     <td class="column-4">2015</td> 
    </tr> 
    <tr class="row-20 even"> 
     <td class="column-1">William Lenihan</td>
     <td class="column-2">5-5</td>
     <td class="column-3">Hackley (N.Y.) High School</td>
     <td class="column-4">2017</td> 
    </tr> 
    <tr class="row-21 odd"> 
     <td class="column-1">Sam Light</td>
     <td class="column-2">5-11</td>
     <td class="column-3">Northern Lebanon (Pa.) High School</td>
     <td class="column-4">2014</td> 
    </tr> 
    <tr class="row-22 even"> 
     <td class="column-1">Sean Mann</td>
     <td class="column-2">6-1</td>
     <td class="column-3">Methacton (Pa.) High School</td>
     <td class="column-4">2015</td> 
    </tr> 
    <tr class="row-23 odd"> 
     <td class="column-1">Ryan Matthews</td>
     <td class="column-2">6-0</td>
     <td class="column-3">Bergen Catholic (N.J.) High School</td>
     <td class="column-4">2015</td> 
    </tr> 
    <tr class="row-24 even"> 
     <td class="column-1">Alexander Mattina</td>
     <td class="column-2">6-3</td>
     <td class="column-3">NYC Lab School</td>
     <td class="column-4">2014</td> 
    </tr> 
    <tr class="row-25 odd"> 
     <td class="column-1">Chris McCullough</td>
     <td class="column-2">6-1</td>
     <td class="column-3">Harborsfields (N.J.) High School</td>
     <td class="column-4">2014</td> 
    </tr> 
    <tr class="row-26 even"> 
     <td class="column-1">Tyler McShea</td>
     <td class="column-2">6-5</td>
     <td class="column-3">Pemberton (N.J.) High School</td>
     <td class="column-4">2016</td> 
    </tr> 
    <tr class="row-27 odd"> 
     <td class="column-1">Liam Monaghan</td>
     <td class="column-2">6-0</td>
     <td class="column-3">Xavier (N.Y.) High School</td>
     <td class="column-4">2014</td> 
    </tr> 
    <tr class="row-28 even"> 
     <td class="column-1">Conor Powers</td>
     <td class="column-2">5-11</td>
     <td class="column-3">Victor Central (N.Y.) High School</td>
     <td class="column-4">2014</td> 
    </tr> 
    <tr class="row-29 odd"> 
     <td class="column-1">Blaise Radosevic</td>
     <td class="column-2">6-5</td>
     <td class="column-3">Marquette University (Wis.) High School</td>
     <td class="column-4">2015</td> 
    </tr> 
    <tr class="row-30 even"> 
     <td class="column-1">Joshua Raime</td>
     <td class="column-2">6-1</td>
     <td class="column-3">Holy Cross (N.Y.) High School</td>
     <td class="column-4">2014</td> 
    </tr> 
    <tr class="row-31 odd"> 
     <td class="column-1">Josh Reed</td>
     <td class="column-2">6-6</td>
     <td class="column-3">Arlee (Mont.) High School</td>
     <td class="column-4">2014</td> 
    </tr> 
    <tr class="row-32 even"> 
     <td class="column-1">Matthew Schaengold</td>
     <td class="column-2">6-4</td>
     <td class="column-3">Bethesday-Chevy Chase (Md.) High School</td>
     <td class="column-4">2016</td> 
    </tr> 
    <tr class="row-33 odd"> 
     <td class="column-1">Drew Schankweiler</td>
     <td class="column-2">6-6</td>
     <td class="column-3">Mechanicsburg (Pa.) High School</td>
     <td class="column-4">2014</td> 
    </tr> 
    <tr class="row-34 even"> 
     <td class="column-1">Logan Schwartz</td>
     <td class="column-2">6-0</td>
     <td class="column-3">Northwestern Lehigh (Pa.) High School</td>
     <td class="column-4">2015</td> 
    </tr> 
    <tr class="row-35 odd"> 
     <td class="column-1">Daniel Tari</td>
     <td class="column-2">6-0</td>
     <td class="column-3">St. Joan of Arc (Ont.) High School</td>
     <td class="column-4">2014</td> 
    </tr> 
    <tr class="row-36 even"> 
     <td class="column-1">Tyler Tobin</td>
     <td class="column-2">5-7</td>
     <td class="column-3">Cherokee (N.J.) High School</td>
     <td class="column-4">2016</td> 
    </tr> 
    <tr class="row-37 odd"> 
     <td class="column-1">Nibra White</td>
     <td class="column-2">6-0</td>
     <td class="column-3">Evanston Township (Ill.) High School</td>
     <td class="column-4">2015</td> 
    </tr> 
    <tr class="row-38 even"> 
     <td class="column-1">Dwight Whitlock</td>
     <td class="column-2">6-1</td>
     <td class="column-3">Central Dauphin East (Pa.) High School</td>
     <td class="column-4">2015</td> 
    </tr> 
   </tbody> 
  </table> 
 </div>
 <!-- .entry-content -->  
 <!-- .entry-footer --> 
</article>