
<title>WordPress to Titan Article/Post Export</title>
<cfsetting requesttimeout = "10000">
<cffunction name="replaceNonAscii" returntype="string" output="false">
<cfargument name="argString" type="string" default="" />
<cfreturn REReplace(arguments.argString,"[^\0-\x80]","","all") />
</cffunction>

<cffunction name="convertWPMarkUp" returntype="string" output="false">
    <cfargument name="argString" type="string" default="" />
    <!---Declare the REturn--->
    <cfset var CleanWP = arguments.argString />

    <cfscript>
        CleanWP = rereplace(CleanWP, "\[", "<", "All");
        CleanWP = rereplace(CleanWP, "\]", ">", "All");


        return CleanWP;
    </cfscript>
</cffunction>

<cfscript>
    function stripWPHTML(str) {
    // remove the whole tag and its content
    var list = "header";
    for (var tag in list){
        str = reReplaceNoCase(str, "<s*(#tag#)[^>]*?>(.*?)","","all");
    }

    //str = reReplaceNoCase(str, "<.*?>","","all");
    //get partial html in front
    //str = reReplaceNoCase(str, "^.*?>","");
    //get partial html at end
    //str = reReplaceNoCase(str, "<.*$","");

    return trim(str);

}
</cfscript>

<cffunction name="replaceDiacriticMarks" returntype="string" output="false">
<cfargument name="argString" type="string" default="" />
<!--- Declare retString --->
<cfset var retString = arguments.argString />

<!--- Do Replaces --->
<cfset retString = REReplace(retString,"#chr(192)#|#chr(193)#|#chr(194)#|#chr(195)#|#chr(196)#|#chr(197)#|#chr(913)#|#chr(8704)#","A","all") />
<cfset retString = REReplace(retString,"#chr(198)#","AE","all") />
<cfset retString = REReplace(retString,"#chr(223)#|#chr(914)#|#chr(946)#","B","all") />
<cfset retString = REReplace(retString,"#chr(162)#|#chr(169)#|#chr(199)#|#chr(231)#|#chr(8834)#|#chr(8835)#|#chr(8836)#|#chr(8838)#|#chr(8839)#|#chr(962)#","C","all") />
<cfset retString = REReplace(retString,"#chr(208)#|#chr(272)#","D","all") />
<cfset retString = REReplace(retString,"#chr(200)#|#chr(201)#|#chr(202)#|#chr(203)#|#chr(8364)#|#chr(8707)#|#chr(8712)#|#chr(8713)#|#chr(8715)#|#chr(8721)#|#chr(917)#|#chr(926)#|#chr(931)#|#chr(949)#|#chr(958)#","E","all") />
<cfset retString = REReplace(retString,"#chr(294)#|#chr(919)#","H","all") />
<cfset retString = REReplace(retString,"#chr(204)#|#chr(205)#|#chr(206)#|#chr(207)#|#chr(8465)#|#chr(921)#","I","all") />
<cfset retString = REReplace(retString,"#chr(306)#","IJ","all") />
<cfset retString = REReplace(retString,"#chr(312)#|#chr(922)#|#chr(954)#","K","all") />
<cfset retString = REReplace(retString,"#chr(319)#|#chr(321)#|#chr(915)#","L","all") />
<cfset retString = REReplace(retString,"#chr(924)#","M","all") />
<cfset retString = REReplace(retString,"#chr(209)#|#chr(330)#|#chr(925)#","N","all") />
<cfset retString = REReplace(retString,"#chr(210)#|#chr(211)#|#chr(212)#|#chr(213)#|#chr(214)#|#chr(216)#|#chr(920)#|#chr(927)#|#chr(934)#","O","all") />
<cfset retString = REReplace(retString,"#chr(338)#","OE","all") />
<cfset retString = REReplace(retString,"#chr(174)#|#chr(8476)#","R","all") />
<cfset retString = REReplace(retString,"#chr(167)#|#chr(352)#","S","all") />
<cfset retString = REReplace(retString,"#chr(358)#|#chr(932)#","T","all") />
<cfset retString = REReplace(retString,"#chr(217)#|#chr(218)#|#chr(219)#|#chr(220)#","U","all") />
<cfset retString = REReplace(retString,"#chr(935)#|#chr(967)#","X","all") />
<cfset retString = REReplace(retString,"#chr(165)#|#chr(221)#|#chr(376)#|#chr(933)#|#chr(936)#|#chr(947)#|#chr(978)#","Y","all") />
<cfset retString = REReplace(retString,"#chr(918)#|#chr(950)#","Z","all") />
<cfset retString = REReplace(retString,"#chr(170)#|#chr(224)#|#chr(225)#|#chr(226)#|#chr(227)#|#chr(228)#|#chr(229)#|#chr(945)#","a","all") />
<cfset retString = REReplace(retString,"#chr(230)#","ae","all") />
<cfset retString = REReplace(retString,"#chr(273)#|#chr(8706)#|#chr(948)#","d","all") />
<cfset retString = REReplace(retString,"#chr(232)#|#chr(233)#|#chr(234)#|#chr(235)#","e","all") />
<cfset retString = REReplace(retString,"#chr(402)#|#chr(8747)#","f","all") />
<cfset retString = REReplace(retString,"#chr(295)#","h","all") />
<cfset retString = REReplace(retString,"#chr(236)#|#chr(237)#|#chr(238)#|#chr(239)#|#chr(305)#|#chr(953)#","i","all") />
<cfset retString = REReplace(retString,"#chr(307)#","j","all") />
<cfset retString = REReplace(retString,"#chr(320)#|#chr(322)#","l","all") />
<cfset retString = REReplace(retString,"#chr(241)#|#chr(329)#|#chr(331)#|#chr(951)#","n","all") />
<cfset retString = REReplace(retString,"#chr(240)#|#chr(242)#|#chr(243)#|#chr(244)#|#chr(245)#|#chr(246)#|#chr(248)#|#chr(959)#","o","all") />
<cfset retString = REReplace(retString,"#chr(339)#","oe","all") />
<cfset retString = REReplace(retString,"#chr(222)#|#chr(254)#|#chr(8472)#|#chr(929)#|#chr(961)#","p","all") />
<cfset retString = REReplace(retString,"#chr(353)#|#chr(383)#","s","all") />
<cfset retString = REReplace(retString,"#chr(359)#|#chr(964)#","t","all") />
<cfset retString = REReplace(retString,"#chr(181)#|#chr(249)#|#chr(250)#|#chr(251)#|#chr(252)#|#chr(956)#|#chr(965)#","u","all") />
<cfset retString = REReplace(retString,"#chr(957)#","v","all") />
<cfset retString = REReplace(retString,"#chr(969)#","w","all") />
<cfset retString = REReplace(retString,"#chr(215)#|#chr(8855)#","x","all") />
<cfset retString = REReplace(retString,"#chr(253)#|#chr(255)#","y","all") />
<!--- ' --->
<cfset retString = REReplace(retString,"#chr(180)#|#chr(8242)#|#chr(8216)#|#chr(8217)#","#chr(39)#","all") />
<!--- " --->
<cfset retString = REReplace(retString,"#chr(168)#|#chr(8220)#|#chr(8221)#|#chr(8222)#|#chr(8243)#","#chr(34)#","all") />

<cfreturn retString />
</cffunction>
<cffunction name="convertMarkup" returntype="string" output="false">
<cfargument name="argString" type="string" default="" />
<!--- Declare retString --->
<cfset var retString = arguments.argString />
<cfscript>
retString =REPLACE("#retString#","[","<","ALL");
retString= REPLACE("#retString#", "]",">","All");
</cfscript>
<cfreturn retString />
</cffunction>

<cfscript>
    //Get Post information for Content Export:
    local.sql = "Select ID, post_title, post_name, post_content, post_type, post_date, post_modified, REPLACE(post_title, ' ', '_') as slug from wp_posts";
    WPExport = new Query(sql="#local.sql#").execute().getResult();
    scrape.sql = "Select url from wp_simply_static_pages where URL NOT LIKE '%wp-content%' or URL NOT LIKE '%tag%' OR URL NOT LIKE '%feed%' OR URL NOT LIKE '%author%' OR URL NOT LIKE '%category%'";
    WPScrape = new Query(sql=#scrape.sql#).execute().getResult();
</cfscript>

<h2>CSV File Generation:</h2>
<!---Articles CSV Generation--->
<cfif directoryExists(expandPath("articles"))>
    <cfdirectory action="delete" directory="#expandPath("articles")#" recurse="true"/>
    <cfdirectory action="create" directory="#expandPath("articles")#" />
<cfelse>

<cfdirectory action="create" directory="#expandPath("articles")#" />
</cfif>


<cffile action="write" file="#expandPath("articles.csv")#"
output="" addnewline="yes">

<cfoutput>
<cfloop query="WPExport">
<!---//
0 = wordpress article id (this is good now)
1 = wordpress article title (same)
2 = Longer description/summary if one exists; if not, title will do
3 = original publish date in format YYYY-MM-DDTHH:MM:SS-OFFSET like: "2000-04-20T00:00:00-04:00" if you don't have offset available, 04:00 is a reasonable plug as they are east coast.
4 = "hidden" - probably "false" for everything
5 = "approved" - we have "true" for all (if it's not ok to publish, why bother to import)
6 = Associated image id, if one exists
7 = last modified date in same format. if not available use same as publish date
8 = Filename = this is the WP user-friendly URL i believe
9 = primary sport id. if all of the articles are in one category in the wordpress source, we can just put a fixed placeholder value here (like "1" or "99").
10 = additonal sport id
//--->
<cffile action="append" file="#expandPath("articles.csv")#" addnewline="yes"
output='"#trim(id)#", "#trim(post_title)#", "#trim(post_title)#", "#datetimeformat(post_date, "YYYY-MM-DD HH:nn:ss z")#","false", "true", "", "#datetimeformat(post_modified, "YYYY-MM-DD HH:nn:ss z")#", "#trim(post_name)#", "1", ""'>
</cfloop>
<p><strong>Articles.csv</strong> has been successfully created</p>
</cfoutput>

<p>Creating Article Contents Export in <cfoutput>#expandPath('/articles')#</cfoutput></p>
<!---p>A Total of <strong><cfoutput>#WPExport.recordCount#</strong></cfoutput> records were added to the Export Script.</p--->
<!---Content Export--->

<!---p>Creating Contents: <cfoutput>#WPScrape.recordcount#</cfoutput></p--->
<div style="width:80%; margin-left:auto; margin-right: auto; height: 350px; overflow-y: scroll;">
<!---Let's Try Scraping Content from a Static Site Shall We?--->
<cfoutput>
    <cfloop query="#WPScrape#" startrow="1" endrow="50">
    <cfhttp url="#WPScrape.url#" username="#CGI.Http_User_Agent#"></cfhttp>


<cfscript>
jsoup = CreateObject("java", "org.jsoup.Jsoup");
HTMLDocument = jsoup.parse(CFHTTP.fileContent);
/* Identify a specific table containing the data to scrape */
TheArticle = HTMLDocument.select("article");

/*  Alternate DOM select methods if table doesn't have a unique ID
TheTable = HTMLDocument.select("table.tableClass").first();
TheTable = HTMLDocument.select("table").first();   */
/* Auto sanitize HTML. Removes most attributes  http://jsoup.org/apidocs/org/jsoup/safety/Whitelist.html
Whitelist = CreateObject("java", "org.jsoup.safety.Whitelist");
TheTable = jsoup.clean(TheTable.toString(), Whitelist.relaxed());
*/
/* Manually remove all class and other misc attributes from table (optional) */
TheArticle.removeAttr("class");



local.id = TheArticle.attr("id");
if(local.id contains "post-"){
    local.id = rereplace(local.id, "post-", "");
} else if(local.id contains "page-"){
    local.id = rereplace(local.id, "page-", "");
}


    if(isNumeric(local.id)) {
        if(!fileExists(expandPath( 'articles/#local.id#.txt' ))) {
             local.article = replaceNonAscii(TheArticle.toString());
            local.article = replaceDiacriticMarks(local.article);
            local.article = convertWPMarkUp(local.article);
            local.article = stripWPHTML(local.article);


        //We Create the Article's Text File with it's contents:
        fileWrite( expandPath( 'articles/#local.id#.txt' ), local.article);
        WriteOutput('<strong color="green">Successfully</strong> Scraped Content for '& local.id &".txt<br>");

        } else if(fileExists(expandPath( 'articles/#local.id#.txt' ))){
           //is this a Duplicate ID?
        local.article = replaceNonAscii(TheArticle.toString());
        local.article = replaceDiacriticMarks(local.article);
        local.article = convertWPMarkUp(local.article);
        local.article = stripWPHTML(local.article);

        //We Create the Article's Text File with it's contents:
        fileWrite( expandPath( 'articles/#local.id#-#wpscrape.currentrow#.txt' ), local.article);
        WriteOutput('<strong color="green">Successfully</strong> Scraped Content for '& local.id & "-"& wpscrape.currentrow&".txt<br>");

        }


    }
</cfscript>
    <cfflush>
    </cfloop>
</cfoutput>

</div>
<hr>
