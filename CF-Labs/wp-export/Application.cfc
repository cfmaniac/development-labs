component {
    this.name              = 'WP-Exporter';
    this.sessionManagement = true;
    this.sessionTimeout    = createTimeSpan(2,0,0,0);

    // Mappingsframework/
    this.applicationRoot = getDirectoryFromPath( getCurrentTemplatePath( ) );

    this.datasource = 'wp-export';
}
