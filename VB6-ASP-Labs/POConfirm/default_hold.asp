<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!--#INCLUDE FILE="include/include.asp" -->
<%
' check for authentication
Set user = New POUser
user.set_cmd(cmdTemp)

If NOT user.get_logged_in() Then
	XRedirect("login.asp")
End If 

' grab the location and save it in the session

if Len(Request("loc")) <= 3 AND Len(Request("loc")) >= 1 Then
	loc = SafeChar(Left(Request("loc"), 3))
	Session("location") = loc
End If

if Len(Request("buyer_no")) <= 3 AND Len(Request("buyer_no")) >= 1 Then
	buyer_no = SafeChar(Left(Request("buyer_no"), 3))
	Session("buyer_no") = buyer_no
End If

If request("clear") = "accessed" Then
	clear_accessed_pos
End If

%>

<!--#INCLUDE FILE="header1.asp" -->

	<title>Eckler Industries Inc. PO Confirmation</title>
	<meta name="ROBOTS" content="NOINDEX,NOFOLLOW"> 
	<link href="stylesheet/style.css" rel="stylesheet" type="text/css" />
	<link href="stylesheet/datepicker.css" rel="stylesheet" type="text/css" />

	<script src="javascript/datepicker.js" type="text/javascript"></script>
	<script src="javascript/scripts.js" type="text/javascript"></script>

<!--#INCLUDE FILE="header2.asp" -->

<!--START CONTENT-->

	<div id="po_main"></div>
<%
	' get the vendor numebr
	vendor_no = user.get_vendor_no()
	
	If Len(Session("location")) <= 3 AND Session("location") <> "ALL" AND Trim(Session("location")) <> "" Then 
		glo_list_filter_sql = " AND location = '"&SafeChar(Session("location"))&"' "
	End If
	
	If Len(Session("buyer_no")) <= 3 AND Session("buyer_no") <> "ALL" AND Trim(Session("buyer_no")) <> "" Then 
		If Trim(Session("buyer_no")) = "*" Then 
			glo_list_filter_sql = glo_list_filter_sql & " AND buyer_no = '' " ' for po's with out a buyer number
		Else
			glo_list_filter_sql = glo_list_filter_sql & " AND buyer_no = '"&SafeChar(Session("buyer_no"))&"' "
		End If
	End If
	
	If vendor_no = 0 Then ' eckler's employee login 
	%>	
		<table width="750" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="left" valign="middle">Eckler's Purchasing dept. </td>
				<td align="right" valign="middle">
					Filter By Buyer: 
					
					<% ' preselect the saved location
					buyer_no = Session("buyer_no")
					
					%>
					
					<select name="buyer_no" onchange="javascript: set_buyer_no(this.value);">
						<option value="ALL">All</option>
						<%=get_buyer_no_select(buyer_no)%>
					</select>
					
					<!--Location:--> 
					
					<% ' preselect the saved location
					'location = Session("location")
					'if Len(location) <= 3 Then 
					'	Select Case location
					'		Case "FL"
					'			selected_fl 	= " selected=""selected"""
					'		Case "GA"
					'			selected_ga 	= " selected=""selected"""
					'		Case Default
					'			selected_all 	= " selected=""selected"""
					'	End Select					
					'End If
					
					%>
					<!--<select name="destination" onchange="javascript: set_location(this.value);">
						<option value="ALL"<%=selected_all%>>All</option>
						<option value="FL"<%=selected_fl%>>FL</option>
						<option value="GA"<%=selected_ga%>>GA</option>
					</select>-->
				</td>
			</tr>		
		</table>
		
		
		
		
		<table width="750" border="0" cellspacing="0" cellpadding="0" class="po_table">
			<tr>
				<td colspan="3" class="po_table_header">PO Confirm Dashboard</td>
			</tr>
			<tr>
				<td width="33%" align="center"><b>Awaiting Approval</b></td>
				<!--<td width="25%" align="center"><b>Confirmed W/Changes</b></td>-->
				<td width="33%" align="center"><b>Confirmed</b></td>
				<td width="33%" align="center"><b>Accessed</b> (<a href="default.asp?clear=accessed">Clear Completed</a>)</td>
			</tr>
			<tr>
				<td valign="top">
					<%getShortApprovalList%>
				</td>
				<!--<td valign="top">
					<%'getShortConfirmedWChangesList%>
				</td>-->
				<td valign="top">
					<%getShortConfirmedList%>
				</td>
				<td valign="top">
					<%getShortAccessedList%>
				</td>
			</tr>
		</table>		
		
	<%
	Else ' get a list of po's for the vendor
		cmdAS400.CommandText = "SELECT DISTINCT " & _
							   "	POHED1.PRPOR#, POHED1.PRPOTC, POHED1.PRENT#, POHED1.PRORDD, POHED1.PRPRMD, " & _
							   "	PODET1.PELNST, POHED1.PRREF " & _
							   "FROM " & _
							   "    POHED1 " & _
							   "INNER JOIN PODET1 ON " & _
							   "    POHED1.PRPOR# = PODET1.PEPOR#    " & _
							   "WHERE " & _
							   "    POHED1.PRENT# = '"&vendor_no&"' AND " & _
							   "    (POHED1.PRPOTC = 'STD' OR " & _ 
							   "    POHED1.PRPOTC = 'DRP') AND " & _
							   "    POHED1.PRORDD >= " & user.get_start_date() & " AND " & _
							   "    PODET1.PELNST = 'OP' AND PODET1.PEQOR# - (PODET1.PEQRC# + PODET1.PEQSR#)  > 0  " & _
							   "ORDER BY " & _
							   "    POHED1.PRPOR# DESC "
							   
		Set pohedrpr = Server.CreateObject("ADODB.Recordset")
		pohedrpr.Open cmdAS400, , adOpenDynamic, adLockOptimistic	
		
		If NOT pohedrpr.eof Then
		%>
			<table width="500" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td class="tr_finished" height="15" width="15">&nbsp;</td>
					<td>&nbsp;PO has been confirmed</td>
					<td class="tr_waiting" height="15" width="15">&nbsp;</td>
					<td>&nbsp;PO is waiting for approval</td>
					<td class="tr_past_due" height="15" width="15">&nbsp;</td>
					<td>&nbsp;Items are past due</td>
				</tr>
			</table>
			<br />
			<table width="500" border="0" cellspacing="0" cellpadding="0" class="po_table">
				<tr>
					<td class="po_table_header">Open Purchase Orders</td>
				</tr>
				<tr>
					<td class="po_table_body">
						<table width="500" border="0" cellspacing="0" cellpadding="2" class="po_table">
							<tr>
								<td class="po_line_item_col_header"><b>PO#</b></td>
								<td class="po_line_item_col_header"><b>Type</b></td>
								<td class="po_line_item_col_header"><b>Date Created</b></td>
								<td class="po_line_item_col_header"><b>Expected Delevery</b></td>
								<td class="po_line_item_col_header" align="center"><b>Viewed</b></td>
								<td class="po_line_item_col_header" align="center"><b>Confirmed</b></td>
								<td class="po_line_item_col_header2" align="center"><b>Pending Approval</b></td>
							</tr>
		<%
		
			i = 0
		
			do while NOT pohedrpr.EOF
				
				' make sure the WEB VIEW flag is not 'N'
				if Mid(pohedrpr("PRREF"), 6, 1) <> "N" then 
					
					' see if it has been confirmed
					cmdTemp.CommandText = "SELECT vendor_accessed, confirmed, needs_approval, approved " & _
										  "FROM poconfirm_po_header " & _
										  "WHERE po_number = '"&pohedrpr("PRPOR#")&"'"
										  
					Set rsPOConfHeader = Server.CreateObject("ADODB.Recordset")
					rsPOConfHeader.Open cmdTemp, , adOpenDynamic, adLockOptimistic
					
					If NOT rsPOConfHeader.EOF Then
						confirmed	= rsPOConfHeader("confirmed")
						If confirmed = "Y" Then
							confirmed = "Yes"
						Else
							confirmed = "&nbsp;"
						End If
						
						vendor_accessed		= rsPOConfHeader("vendor_accessed")
						If vendor_accessed = "Y" Then
							vendor_accessed = "Yes"
						Else
							vendor_accessed = "&nbsp;"
						End If
						
						needs_approval		= rsPOConfHeader("needs_approval")
						If needs_approval = "Y" Then
							needs_approval = "Yes"
						Else
							needs_approval = "&nbsp;"
						End If
						
						approved		= rsPOConfHeader("approved")
						If approved = "Y" Then
							approved = "Yes"
						Else
							approved = "&nbsp;"
						End If
					Else
						confirmed		= "&nbsp;"
						vendor_accessed	= "&nbsp;"
						needs_approval 	= "&nbsp;"
						approved 		= "&nbsp;"
					End If
					rsPOConfHeader.Close
					
					' check to see if the PO is past due
					TheMMDDYY = DatePart("m",date)&"/"&DatePart("d",date)&"/"&DatePart("yyyy",date)
					past_due = false
					If cdate(jt_convertDate(pohedrpr("PRPRMD"))) < cdate(TheMMDDYY) Then
						past_due = true
					End If
					
					i = i + 1
					If i MOD 2 = 0 Then
						tr_style = "tr2"
					Else
						tr_style = "tr1"
					End If
					
					If confirmed = "Yes" Then
						tr_style = "tr_finished"
					ElseIf needs_approval = "Yes" Then
						tr_style = "tr_waiting"
					ElseIf past_due Then
						tr_style = "tr_past_due"
					Else
						tr_style = "tr1"
					End If
						
					%>
					<tr class="<%=tr_style%>">
						<td class="po_line_item_border"><a href="confirm_po.asp?o=<%=pohedrpr("PRPOR#")%>"><%=pohedrpr("PRPOR#")%></a></td>
						<td class="po_line_item_border"><%=exp_po_type(pohedrpr("PRPOTC"))%></td>
						<td class="po_line_item_border"><%=jt_convertDate(pohedrpr("PRORDD"))%></td>	
						<td class="po_line_item_border"><%=jt_convertDate(pohedrpr("PRPRMD"))%></td>
						<td class="po_line_item_border" align="center"><%=vendor_accessed%></td>
						<td class="po_line_item_border" align="center"><%=confirmed%></td>
						<td class="po_line_item_border" align="center"><%=needs_approval%></td>
					</tr>
					<%
				End If
				
				pohedrpr.MoveNext
			loop
			
			%>
						</table></td>
				</tr>		
			</table>
			<%
		Else
		%>
			No Purchase Orders where found.
		<%
		End If
	End If %>

<!--END CONTENT-->

<!--#INCLUDE FILE="footer.asp" -->