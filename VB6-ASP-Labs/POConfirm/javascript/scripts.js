function setNLA(quantity_field, date_field, quantity_field, select_list)
{
	var dropdownValue;
	
	if(select_list != null)
	{
		dropdownValue = select_list[select_list.selectedIndex].value;
	}
	else
	{
		dropdownValue = null;	
	}
	//alert(dropdownValue);
	//date_field_ele = document.getElementById(date_field)
	if( date_field != null && quantity_field != null)
	{
		if(dropdownValue == "DISCO")
		{
			date_field.style.color = "#ACACAC";
			date_field.value = "DISCO";
			date_field.readOnly  = true;
			quantity_field.value = "0";
			
			// remove the onclick event 
			date_field.onclick = null;
			quantity_field.disabled = true;
		}	
		else if(dropdownValue == "NLA")
		{
			date_field.style.color = "#ACACAC";
			date_field.value = "NLA";
			date_field.readOnly  = true;
			quantity_field.value = "0";
			
			// remove the onclick event 
			date_field.onclick = null;
			quantity_field.disabled = true;
		}	
		else
		{	
			date_field.style.color = "#000000";
			if(!isDate(date_field.value))
				date_field.value = "NONE";
			date_field.readOnly  = false;
			date_field.onclick = function(){displayDatePicker(date_field.name)};
			quantity_field.disabled = false;
		}
		date_field.focus()
	}
	
}
function callAHAHPOST(url, postdata, pageElement, callMessage)
{
	var req; // we have multiple instances at the same time
	
	document.getElementById(pageElement).innerHTML = callMessage;
	
	try {
		req = new XMLHttpRequest();	/* fire fox */
	} catch(e) {
		try { 
			req = new ActiveXObject("Msxml2.XMLHTTP") /* some versions of IE */
		} catch(e){
			try {
				req = new ActiveXObject("Microsoft.XMLHTTP") /* other versions of IE */
			} catch(E) {
				req = false;
			}
		}
	}
	// test to see it xmlHttp has been created
	if (req==null)
	{
		alert ("Your browser does not support AJAX!");
		return;
	} 
	var myRand = parseInt(Math.random()*9999999);
	req.onreadystatechange = function(){responseAHAH(pageElement, req);}
	req.open('POST', url + '&rand=' + myRand, true);
	req.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=UTF-8")
	//req.setRequestHeader("Content-length", postdata.length);
	req.send(postdata);
}
function callAHAH(url, pageElement, callMessage)
{
	var req; // we have multiple instances at the same time
	
	document.getElementById(pageElement).innerHTML = callMessage;
	
	try {
		req = new XMLHttpRequest();	/* fire fox */
	} catch(e) {
		try { 
			req = new ActiveXObject("Msxml2.XMLHTTP") /* some versions of IE */
		} catch(e){
			try {
				req = new ActiveXObject("Microsoft.XMLHTTP") /* other versions of IE */
			} catch(E) {
				req = false;
			}
		}
	}
	// test to see it xmlHttp has been created
	if (req==null)
	{
		alert ("Your browser does not support AJAX!");
		return;
	} 
	var myRand = parseInt(Math.random()*9999999);
	req.onreadystatechange = function(){responseAHAH(pageElement, req);}
	req.open("GET", url + '&rand=' + myRand, true);
	req.send(null);
}

function responseAHAH(pageElement, req)
{
	var output = '';
	
	if(req.readyState == 4) {
		if(req.status == 200)
		{
			//alert(pageElement); debug
			output = req.responseText;
			document.getElementById(pageElement).innerHTML = output;
		}
		else if(req.status == 500)
		{
			//alert(pageElement); debug
			output = req.responseText;
			document.getElementById(pageElement).innerHTML = output;
		}
		else if(req.status == 404)
		{
			//alert(pageElement); debug
			output = req.responseText;
			document.getElementById(pageElement).innerHTML = output;
		}
	}
	setTimeout('5');
}
var IE;
var FF;

if(document.getElementById)
{
	IE = true; 
}
else if (document.all) 
{
	FF = true; 
}

var tipWin; 		// the current items style 
					
function showTip(evt, elemID) 
{
	var topVal;
	var leftVal;
	
	//alert("showTip");
	if(tipWin != null)
	{
		tipWin.visibility = "hidden";
	}
	
	if(IE)
	{
		tipWin = document.getElementById(elemID).style;
	}
	else
	{
		tipWin = document.all[elemID].style;	
	}
	
	if(evt.pageY)
	{
		topVal = evt.pageY - 10;
		leftVal = evt.pageX + 10;
	}
	else if(evt.y)
	{
		topVal = evt.y - 10 + document.body.scrollTop;
		leftVal = evt.x + 10 + document.body.scrollLeft;
	}
	
	tipWin.top = topVal;
	tipWin.left = leftVal;
	tipWin.visibility = "visible";
}

function hideTip(elemID) 
{
	//alert("hideTip");
	if(IE)
	{
		document.getElementById(elemID).style.visibility = "hidden";
	}
	else if(FF)
	{
		document.all[elemID].style.visibility = "hidden";
	}
}


// ******************************************************************
// This function accepts a string variable and verifies if it is a
// proper date or not. It validates format matching either
// mm-dd-yyyy or mm/dd/yyyy. Then it checks to make sure the month
// has the proper number of days, based on which month it is.

// The function returns true if a valid date, false if not.
// ******************************************************************

function isDate(dateStr) {
	
	var datePat = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
	var matchArray = dateStr.match(datePat); // is the format ok?
	
	if (matchArray == null) {
		//alert("Please enter date as either mm/dd/yyyy or mm-dd-yyyy.");
		return false;
	}
	
	month = matchArray[1]; // p@rse date into variables
	day = matchArray[3];
	year = matchArray[5];
	
	if (month < 1 || month > 12) { // check month range
		//alert("Month must be between 1 and 12.");
		return false;
	}
	
	if (day < 1 || day > 31) {
		//alert("Day must be between 1 and 31.");
		return false;
	}
	
	if ((month==4 || month==6 || month==9 || month==11) && day==31) {
		//alert("Month "+month+" doesn`t have 31 days!")
		return false;
	}
	
	if (month == 2) { // check for february 29th
		var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
		if (day > 29 || (day==29 && !isleap)) {
			//alert("February " + year + " doesn`t have " + day + " days!");
			return false;
		}
	}
	return true; // date is valid
}
// set cookies for po_list filters
function set_location(loc)
{
	//alert(loc);
	window.location = "/default.asp?loc=" + loc;
}
function set_buyer_no(buyer_no)
{
	//alert(loc);
	window.location = "/default.asp?buyer_no=" + buyer_no;
}

function submit_for_approval(theForm)
{
	var newInput = document.createElement('input');
	newInput.setAttribute('name', 'submit_for_approval');
	newInput.setAttribute('id', 'submit_for_approval');
	newInput.setAttribute('type', 'hidden');
	newInput.setAttribute('value', ' Submit for Approval ');
	
	theForm.appendChild(newInput);
	theForm.submit();
}