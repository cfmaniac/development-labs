<%
'-- Global Variables -----------------------------------------------------------------'
'-------------------------------------------------------------------------------------'
glo_purchasing_phone = "321-267-6271"
glo_purchasing_email = ""
glo_help_email = "larry.bailey@ecklers.net, james.harvey@ecklers.net"
glo_po_confirm_doc_root = "/"

' global po related variables
'glo_po_confirmed = false ' gets set from db after po_header gets called
'glo_po_submitted_for_approval = false ' replace with po.needs_approval
'glo_po_approved = false ' replace with po.approved


glo_list_filter_sql = ""

'-- Display Functions ----------------------------------------------------------------'
'-------------------------------------------------------------------------------------'
' check to see if the PO is confirmed

' print the modify PO Form
Sub print_po_form(po, ByRef errorList)

	If Not ISObject(errorList) Then
		Set errorList = CreateObject("Scripting.Dictionary")
	End If
	
	%>
	<form id="po_confirm_form" name="po_confirm_form" action="confirm_po.asp" method="POST">
		<input type="hidden" name="o" value="<%=po_number%>">
		<%po_header_HTML po, errorList%>
		<%po_item_detail_HTML po, errorList%>
		
		<% If po.isEditable() Then %>
		<input name="save_po" type="submit" class="btn_blu" value=" Save PO " title="Saves changes without submiting"/>
		<!--<input name="submit_for_approval" type="submit" class="btn_blu" value=" Submit for Approval " title="Submits the PO to our purchasing department in the event that information needs needs to be changed on the PO."/>-->	
		<input name="confirm" type="submit" class="btn_grn" value=" Confirm PO " title="Confirms that the purchase order is correct and submits it to our purchasing department."/>	
		<% End If%>
		
		<%
		' administrator only options
		If po.user.get_vendor_no() = "0" Then
			' if the po has been submited for approval then 
			' show the administrator the approve changes button
			
			If po.needs_approval = "Y" Then
		%>
			<input name="admin_confirm_changes" type="submit" class="btn_blu" value=" Confirm Changes" title="Saves the PO to the AS400"/>
		<%
			End If
		End If
		%>
		
	</form>
	<%
	
	
End Sub

Sub po_header_HTML(po, ByRef errorList)
	
	If po.IsConfirmed() Then %>
		<div class="info_box">
			This Purchase Order has been confirmed. Please ship at your earliest convenience.
		</div>
		<br />
	<%	
	ElseIf po.needs_approval = "Y" Then
	%>
		<div class="info_box">
			This Purchase Order has been submitted for approval. <b>Do not ship this PO until we have contacted you and confirmed.</b> If you need to make changes please call our Purchasing Department at the Bill To phone number below.
		</div>
		<br />
	<%
	Else
	%>
		<%If po.po_type = "DRP" Then%>
		<div class="error_box">
			This is a Drop Ship Purchase Order Please Confirm. 
		</div>		
		<%Else%>
		<div class="error_box">
			This is not a PO Until it has been confirmed or approved by our purchasing department. <br />
			<b>Once the PO is confirmed no further price changes will be allowed. Eckler's will pay the PO amount.</b>
			<!--If you make changes to pricing then you must submit the PO for approval before it can be confirmed. <br />-->
		</div>
		<%End If%>
		<br />
	<%
	End If
	
	%>
	<table width="750" border="0" cellspacing="0" cellpadding="0" class="po_table">
		<tr>
			<td class="po_table_header">PO Details</td>
		</tr>
		<tr>
			<td class="po_table_body"><%
				If po.po_msg <> "" Then 
				%>
				<h1 style="color: #f00; text-align: center;">**<%=po.po_msg%>**</h1>
				<%
				End If
				%><table width="750" border="0" cellspacing="2" cellpadding="0">
				  <tr>
					<td width="144" align="left" valign="top"><b>PO Date :</b> <%=po.po_date%></td>
					<td width="178" align="left" valign="top"><b>PO Number:</b> <%=po.po_number%> </td>
					<td width="428"><b>Our Customer Number:</b> <%=po.our_cust_number%></td>
				  </tr>
				  <tr>
					<td><b>Ship Via :</b> <%=po.ship_via%></td>
					<td><b>Terms:</b> <%=po.terms%></td>
					<td><b>Buyer No.:</b> <%=po.buyer_number%>&nbsp;<%=po.buyer_name%>&nbsp;<%=po.buyer_phone%></td>
				  </tr>
				  <tr>
					<td><b>PO Type:</b> <%=exp_po_type(po.po_type)%></td>
					<td></td>
					<td></td>
				  </tr>
				  <tr>
					<td colspan="3">&nbsp;</td>
				  </tr>
				  <tr>
					<td colspan="2"><b>BILL TO:</b></td>
					<td colspan="2"><b>SHIP TO:</b></td>
				  </tr>
				  <tr>
					<td colspan="2" align="left" valign="top">
					<%=po.address_billing.getAddressHTML()%>
					</td>
					<td colspan="2" align="left" valign="top">
					<%=po.address_shipping.getAddressHTML()%>
					<input type="hidden" name="to_state" value="<%=po.address_shipping.State%>">
					<input type="hidden" name="po_type" value="<%=po.po_type%>">
					</td>
				  </tr>
				   <tr>
					<td colspan="3">&nbsp;</td>
				  </tr>
				  <tr>
					<td colspan="3"><b>Purchaser Comment:</b> <%=po.po_notes%></td>
				  </tr>
				  <tr>
					<td colspan="3">&nbsp;<br /></td>
				  </tr>
				  <tr>
					<td colspan="3"><p style="color: #FF0000; font-size: 12px">(Please let our purchasing department know if your contact information needs to be updated.) </p></td>
				  </tr>
				  <tr>
					<td colspan="3" valign="top">
						<br />
						<table cellspacing="0" cellpadding="0" width="97%" border="0">
							<tr>
							  <td valign="top"><p><b>Contact: </b></p></td>
							  <td valign="top"><%=po.vendor_contact%></td>
							  <td valign="top"><b>Comment:</b></td>
							</tr>
							<tr>
								<td width="70" valign="top"></td>
								<td valign="top"><%=po.address_vendor.getAddressHTML()%><input type="hidden" name="from_state" value="<%=po.address_vendor.State%>"></td>
								<td valign="top">
									<textarea cols="40" rows="4" name="comment" id="comment"><%=Server.HTMLEncode(po.comment)%></textarea>
								</td>
							</tr>
						</table>
					</td>
				  </tr>
				</table>
			</td>
		</tr>
	</table>
<%
End Sub

Function po_header_email(po)

	ret_str = "" ' this is the string that will be returned
						
	If po.isConfirmed() Then 
		ret_str = ret_str & "<div class=""info_box"" style=""width: 750px;""> " & vbCrLF & _
							"	This Purchase Order has been confirmed. Please ship at your earliest convenience. " & vbCrLF & _
							"</div><br />" & vbCrLF
	ElseIF po.needs_approval = "Y" Then
		ret_str = ret_str & "<div class=""info_box"">" & vbCrLF & _
							"	This Purchase Order has been submitted for approval. <b>Do not ship this PO until we have contacted you and confirmed.</b> If you need to make changes please call our Purchasing Department at the Bill To phone number below." & vbCrLF & _
							"</div><br />" & vbCrLF
	Else
		ret_str = ret_str & "<div class=""error_box"">" & vbCrLF & _
							"	This is not a PO Until it has been confirmed or approved by our purchasing department.<br />If you make changes to pricing then you must submit the PO for approval before it can be confirmed.<br />" & vbCrLF & _
							"</div><br />" & vbCrLF
	End If
	
	ret_str = ret_str & _
	"<table width=""750"" border=""0"" cellspacing=""0"" cellpadding=""0"" class=""po_table"">" & vbCrLF & _
	"	<tr>" & _
	"		<td class=""po_table_header"">PO Details</td>" & vbCrLF & _
	"	</tr>" & vbCrLF & _
	"	<tr>" & vbCrLF & _
	"		<td class=""po_table_body"">" 
	
	If po.po_msg <> "" Then 
		ret_str = ret_str & "<h1 style=""color: #f00; text-align: center;"">**" & po.po_msg & "**</h1>"
	End If
	
	ret_str = ret_str & _
	"			<table width=""750"" border=""0"" cellspacing=""2"" cellpadding=""0"">" & vbCrLF & _
	"			  <tr>" & vbCrLF & _
	"				<td width=""144"" align=""left"" valign=""top""><b>PO Date :</b> " & po.po_date & "</td>" & vbCrLF & _
	"				<td width=""178"" align=""left"" valign=""top""><b>PO Number:</b> " & po.po_number & " </td>" & vbCrLF & _
	"				<td width=""428"" align=""left""><b>Our Customer Number:</b> " & po.our_cust_number & "</td>" & vbCrLF & _
	"			  </tr>" & vbCrLF & _
	"			  <tr>" & vbCrLF & _
	"				<td align=""left""><b>Ship Via :</b> " & ship_via & "</td>" & vbCrLF & _
	"				<td align=""left""><b>Terms:</b> " & terms & "</td>" & vbCrLF & _
	"				<td align=""left""><b>Buyer No.:</b> " & po.buyer_number & "&nbsp;" & po.buyer_name & "&nbsp;" & po.buyer_phone & "</td>" & vbCrLF & _
	"			  </tr>" & vbCrLF & _
	"			  <tr>" & vbCrLF & _
	"			  	<td align=""left""><b>PO Type:</b> " & po.po_type & "</td>" & vbCrLF & _
	"				<td></td>" & vbCrLF & _
	"				<td></td>" & vbCrLF & _
	"			  </tr>" & vbCrLF & _
	"			  <tr>" & vbCrLF & _
	"				<td colspan=""3"">&nbsp;</td>" & vbCrLF & _
	"			  </tr>" & vbCrLF & _
	"			  <tr>" & vbCrLF & _
	"				<td colspan=""2"" align=""left""><b>BILL TO:</b></td>" & vbCrLF & _
	"				<td colspan=""2"" align=""left""><b>SHIP TO:</b></td>" & vbCrLF & _
	"			  </tr>" & vbCrLF & _
	"			  <tr>" & vbCrLF & _
	"				<td colspan=""2"" align=""left"" valign=""top"">" & vbCrLF & _
	"				" & po.address_billing.getAddressHTML() & "" & vbCrLF & _
	"				</td>" & vbCrLF & _
	"				<td colspan=""2"" align=""left"" valign=""top"">" & vbCrLF & _
	"				" & po.address_shipping.getAddressHTML() & "" & vbCrLF & _
	"				</td>" & vbCrLF & _
	"			  </tr>" & vbCrLF & _
	"			   <tr>" & vbCrLF & _
	"				<td colspan=""3"">&nbsp;</td>" & vbCrLF & _
	"			  </tr>" & vbCrLF & _
	"			  <tr>" & vbCrLF & _
	"				<td colspan=""3"" align=""left""><b>Purchaser Comment:</b> " & po.po_notes & "</td>" & vbCrLF & _
	"			  </tr>" & vbCrLF & _
	"			  <tr>" & vbCrLF & _
	"				<td colspan=""3"">&nbsp;<br /></td>" & vbCrLF & _
	"			  </tr>" & vbCrLF & _
	"			  <tr>" & vbCrLF & _
	"				<td colspan=""3""><p style=""color: #FF0000; font-size: 12px"">(Please let our purchasing department know if your contact information needs to be updated.) </p></td>" & vbCrLF & _
	"			  </tr>" & vbCrLF & _
	"			  <tr>" & vbCrLF & _
	"				<td colspan=""3"">" & vbCrLF & _
	"					<table cellspacing=""0"" cellpadding=""0"" width=""97%"" border=""0"">" & vbCrLF & _
	"						<tr>" & vbCrLF & _
	"						  <td width=""10%"" valign=""top"" align=""left""><p><b>Contact: </b></p></td>" & vbCrLF & _
	"						  <td width=""40%"" valign=""top"" align=""left"">" & po.vendor_contact & "</td>" & vbCrLF & _
	"						  <td width=""50%"" valign=""top"" align=""left""><b>Comment:</b></td>" &  vbCrLF & _
	"						</tr>" & vbCrLF & _
	"						<tr>" & vbCrLF & _
	"						  <td width=""10%"" valign=""top"" align=""left""></td>" & vbCrLF & _
	"						  <td width=""40%"" valign=""top"" align=""left"">" & po.address_vendor.getAddressHTML() & "</td>" & vbCrLF & _
	"						  <td width=""50%"" valign=""top"" align=""left"">" & vbCrLF & _
	"							"& Server.HTMLEncode(po.comment) & vbCrLF & _
	"						  </td>" & vbCrLF & _
	"						</tr>" & vbCrLF & _
	"					</table>" & vbCrLF & _
	"				</td>" & vbCrLF & _
	"			  </tr>" & vbCrLF & _
	"			</table>" & vbCrLF & _
	"		</td>" & vbCrLF & _
	"	</tr>" & vbCrLF & _
	"</table>" & vbCrLF

	po_header_email = ret_str
	
End Function

Sub po_item_detail_HTML ( po, ByRef errorList)

	If po.items.count > 0 Then
		 
		i = 1 ' counter
		
		%>	
	<table width="750" border="0" cellspacing="0" cellpadding="0" class="po_table">
		<tr>
			<td class="po_table_header">Item Details</td>
		</tr>
		<tr>
			<td valign="top" class="po_table_body">
				<table cellspacing="0" cellpadding="2" width="750">
					<tr>
					  <td class="po_line_item_col_header"><b>Line</b></td>
					  <td class="po_line_item_col_header"><b>Vend&nbsp;Part</b></td>
					  <td class="po_line_item_col_header"><b>Eck&nbsp;Part</b></td>
					  <td class="po_line_item_col_header" align="left"><b>Vendor&nbsp;Desc.</b></td>
					  <td class="po_line_item_col_header"><b><a title="Quantity we are ordering">Qty</a></b></td>
					  <td class="po_line_item_col_header"><b><a title="Quantity we have recieved">Rec</a></b></td>
					  <td class="po_line_item_col_header"><b><a title="Unit of measure">UM</a></b></td>
					  <td class="po_line_item_col_header"><b><a title="Price per unit">Cost&nbsp;Ea</a></b></td>
					  <td class="po_line_item_col_header" align="right"><b>Ext</b></td>
					  <td class="po_line_item_col_header" bgcolor="#ffffba"><b>Qty&nbsp;Avail</b></td>
					  <td class="po_line_item_col_header" bgcolor="#ffffba"><b>Ship&nbsp;Date</b></td>
					  <td class="po_line_item_col_header" bgcolor="#ffffba"><b>Status</b></td>
					  <td class="po_line_item_col_header2" align="right" bgcolor="#ffffba"><b><a title="If the price has changed please list the new price.">New&nbsp;$&nbsp;Ea</a></b></td>
					  <td class="po_line_item_col_header2" align="right" bgcolor="#ffffba">&nbsp;</td>
					</tr>
		<%
		For Each key in po.items
			
			Set item = po.items.Item(key)
			
			' item.our_description 	' not yet used but it is available
			' item.euro_cost_each	' not yet used but it is available							
			
			If item.conf_status_code = "DISCO" Then
				If po.po_type <> "DRP" Then  
					read_only_conf_date = " readonly style=""color: #ACACAC"""
					disable_conf_quantity = " disabled"
				End If
			Else 
				read_only_conf_date = " onclick=""displayDatePicker('conf_date"&i&"', this);"""
				disable_conf_quantity = ""
			End If
			
			If po.po_type = "DRP" Then ' hide the quantity text field
				conf_quantity_style = " visibility: hidden; display: none;"
			End If
			
			' check for errors 
			conf_date_error_text = ""
		
			If errorList.Exists("conf_date"&i&"error") Then
				conf_date_error_text = "class=""highlight_input_error"" " & _
									   "onmouseover=""showTip( event, 'conf_date"&i&"error');"" " & _
									   "onmouseout=""hideTip( 'conf_date"&i&"error');"""
			End If
			
			conf_quantity_error_text = ""
							
			If errorList.Exists("conf_quantity"&i&"error") Then
				conf_quantity_error_text = "class=""highlight_input_error"" " & _
									   "onmouseover=""showTip( event, 'conf_quantity"&i&"error');"" " & _
									   "onmouseout=""hideTip( 'conf_quantity"&i&"error');"""
			End If
			
			
			' if there is no vendor description then use our description
			item_description = item.vendor_description
			
			If trim(item_description) = "" Then
				item_description = item.our_description
			End If
			
			tr_class = ""
			if i = 1 Then
				tr_class = " class=""first"""
			ElseIf i = po.items.count Then
				tr_class = " class=""last"""
			End If
				
			%>
				<tr<%=tr_class%>>
					<td class="po_line_item_border gray_border_right" align="right" valign="top"><%=item.line_no%></td>
					<td class="po_line_item_border" align="left" valign="top"><%=item.vendor_part_number%></td>
					<td class="po_line_item_border" align="left" valign="top"><%=item.our_part_number%></td>
					<td class="po_line_item_border" align="left" valign="top"><%=item_description%></td>
					<td class="po_line_item_border" align="center" valign="top"><%=item.quantity%></td>
					<td class="po_line_item_border" align="center" valign="top"><%=item.quantity_recieved%></td>
					<td class="po_line_item_border" align="left" valign="top"><%=item.uom%></td>
					<td class="po_line_item_border" align="right" valign="top"><%=priceFormat(item.cost_each)%></td>
					<td class="po_line_item_border" align="right" valign="top"><%=priceFormat(item.cost_each * item.quantity)%></td>
					<% ' if this cell as changed mark it
						td_style = ""
						If cDbl(item.conf_quantity) <> cDbl(item.quantity) Then
							td_style = " line_diff"
						End If
					%>
					<td class="po_line_item_border<%=td_style%>" align="center" valign="top" bgcolor="#ffffba">
						<% If po.isEditable() Then %>
						<input name="conf_quantity<%=i%>" type="text" value="<%=item.conf_quantity%>" size="4" style="text-align:center;<%=conf_quantity_style%>" <%=conf_quantity_error_text%> <%=disable_conf_quantity%>>
							<% If po.po_type = "DRP" Then %>
								<%=item.conf_quantity%>
							<% End If%>
						<% Else %>
						<%=item.conf_quantity%>
						<% End If %>
					</td>
					<% ' if this cell has changed mark it
						td_style = ""
						If item.conf_date <> po.default_promise_date Then
							td_style = " line_diff"
						End If
					%>
					<td class="po_line_item_border<%=td_style%>" align="center" valign="top" bgcolor="#ffffba">
						<% If po.isEditable() Then %>
						<input name="conf_date<%=i%>" id="conf_date<%=i%>" type="text" value="<%=item.conf_date%>" size="10" style="width: 70px;" <%=conf_date_error_text%> <%=read_only_conf_date%>>
						<% Else %>
						&nbsp;<%=item.conf_date%>
						<% End If %>
					</td>
					<% ' if this cell as changed mark it
						td_style = ""
						If item.conf_status_code <> "INSTOCK" Then
							td_style = " line_diff"
						End If
						
						If po.po_type <> "DRP" Then
							po_status_select_onchange = "onchange=""setNLA(conf_quantity"&i&", conf_date"&i&", conf_quantity"&i&", this);"""
						End If
					%>
					<td class="po_line_item_border<%=td_style%>" align="center" valign="top" bgcolor="#ffffba">
						<% If po.isEditable() Then %>
							<select style="font-size: 10px; width: 75px" name="conf_status_code<%=i%>" id="conf_status_code<%=i%>" <%=po_status_select_onchange%>>
								<%=get_status_code_options(item.conf_status_code)%>
							</select>
						<% Else%>
							<%=item.conf_status_title%>
						<% End If %>
					</td>
					<% ' if this cell as changed mark it
						td_style = ""
						If priceFormat(item.conf_cost) <> priceFormat(item.cost_each) Then
							td_style = " line_diff"
						End If
					%>
					<td class="po_line_item_border<%=td_style%>" align="right" valign="top" bgcolor="#ffffba">
						<% If po.isEditable() Then %>
						<input name="conf_cost<%=i%>" type="text" value="<%=priceFormat(item.conf_cost)%>" size="8" style="text-align:right; width: 60px">
						
						<input name="po_quantity<%=i%>" type="hidden" value="<%=item.quantity%>">
						<input name="po_date<%=i%>" type="hidden" value="<%=po.default_promise_date%>">
						<input name="po_cost<%=i%>" type="hidden" value="<%=priceFormat(item.cost_each)%>">
						<input name="po_our_part_number<%=i%>" type="hidden" value="<%=item.our_part_number%>" />
						<% Else %>
						<%=priceFormat(item.conf_cost)%>
						<% End If %>
						<input name="line_no<%=i%>" type="hidden" value="<%=item.line_no%>">
					</td>
					<td valign="top" class="po_line_item_border" bgcolor="#ffffba">
					<%
					If po.user.get_vendor_no() = "0" AND po.approved = "N" AND po.needs_approval = "Y" Then
					%>
						<select name="item_action<%=i%>" style="font-size: 10px;">
						<%If item.conf_action = "OK" Then%>
							<option value="OK" selected="selected">OK</option>
						<%Else%>
							<option value="OK">OK</option>
						<%End If%>
						
						<%If item.conf_action = "DELETE" Then%>
							<option value="DELETE" style="background-color:#FF0000" selected="selected">DELETE</option>
						<%Else%>
							<option value="DELETE" style="background-color:#FF0000">DELETE</option>
						<%End If%>
						</select>
					<%
					Else
					%>
					&nbsp;
					<%
					End If
					%>
					</td>
				</tr>
			<%	If po.po_type = "DRP" Then	%>
					<tr<%=tr_class%>>
						<td colspan="3">&nbsp;</td>
						<td colspan="10">Ship Via: <%= trim(item.oe_ship_via_desc) %></td>
					</tr>
				<%	if trim(item.oe_comment_L1) <> "" then	%>
						<tr<%=tr_class%>>
							<td colspan="3">&nbsp;</td>
							<td colspan="10"><%= trim(item.oe_comment_L1) %></td>
						</tr>
				<%	end if	
					if trim(item.oe_comment_L2) <> "" then	%>
						<tr<%=tr_class%>>
							<td colspan="3">&nbsp;</td>
							<td colspan="10"><%= trim(item.oe_comment_L2) %></td>
						</tr>
				<%	end if	
				end if			
			
			If item.prompt <> "" Then %>
			
			<tr>
				<td class="gray_border_right">&nbsp;</td>
				<td colspan="14" class="prompt_td">
					<b>Custom Information:</b><br />
					<%=item.prompt%>
				</td>
			</tr>
			
			<%
			End If
			' move to next line
			i = i + 1
		Next
		
		%>
				</table>
			</td>
		</tr>
	</table>
		
		<%
	Else
		%><div align="left" style="width: 750px;">PO Line Items Not Found.</div><%
	End If
End Sub

Function po_item_details_email (po)

	ret_str = ""  ' this is the string that will be returned
		 
	i = 1 ' counter
		
	ret_str = ret_str & _
	"<table width=""750"" border=""0"" cellspacing=""0"" cellpadding=""0"" class=""po_table"">" & vbCrLF & _
	"	<tr> " & vbCrLF & _
	"		<td class=""po_table_header"">Item Details</td> " & vbCrLF & _
	"	</tr> " & vbCrLF & _
	"	<tr> " & vbCrLF & _
	"		<td valign=""top"" class=""po_table_body""> " & vbCrLF & _
	"			<table cellspacing=""0"" cellpadding=""2"" width=""750""> " & vbCrLF & _
	"				<tr> " & vbCrLF & _
	"				  <td class=""po_line_item_col_header""><b>Line</b></td> " & vbCrLF & _
	"				  <td class=""po_line_item_col_header""><b>Vend&nbsp;Part</b></td> " & vbCrLF & _
	"				  <td class=""po_line_item_col_header""><b>Eckler's&nbsp;Part</b></td> " & vbCrLF & _
	"				  <td class=""po_line_item_col_header"" align=""left""><b>Vendor&nbsp;Desc.</b></td> " & vbCrLF & _
	"				  <td class=""po_line_item_col_header""><b><a title=""Quantity we are ordering"">Qty</a></b></td> " & vbCrLF & _
	"				  <td class=""po_line_item_col_header""><b><a title=""Quantity we are ordering"">Qty Rec</a></b></td> " & vbCrLF & _
	"				  <td class=""po_line_item_col_header""><b><a title=""Unit of measure"">UM</a></b></td> " & vbCrLF & _
	"				  <td class=""po_line_item_col_header""><b><a title=""Status"">Status</a></b></td> " & vbCrLF & _
	"				  <td class=""po_line_item_col_header""><b><a title=""Price per unit"">Cost&nbsp;$&nbsp;Ea</a></b></td> " & vbCrLF & _
	"				  <td class=""po_line_item_col_header2""><b>Ext&nbsp;$</b></td> " & vbCrLF & _
	"				</tr>" & vbCrLF
	
	For Each key in po.items
			
		Set item = po.items.Item(key) 
		
		ret_str = ret_str & _
		"			<tr>" & _
		"				<td class=""po_line_item_border gray_border_right"" align=""right"" valign=""top"">" & item.line_no & "</td>" & vbCrLF & _
		"				<td class=""po_line_item_border"" align=""left"" valign=""top"">" & item.vendor_part_number & "</td>" & vbCrLF & _
		"				<td class=""po_line_item_border"" align=""left"" valign=""top"">" & item.our_part_number & "</td>" & vbCrLF & _
		"				<td class=""po_line_item_border"" align=""left"" valign=""top"">" & item.vendor_description & "</td>" & vbCrLF & _
		"				<td class=""po_line_item_border"" align=""center"" valign=""top"">" & item.quantity & "</td>" & vbCrLF & _
		"				<td class=""po_line_item_border"" align=""center"" valign=""top"">" & item.quantity_recieved & "</td>" & vbCrLF & _
		"				<td class=""po_line_item_border"" align=""left"" valign=""top"">" & item.uom & "</td>" & vbCrLF & _
		"				<td class=""po_line_item_border"" align=""left"" valign=""top"">" & item.conf_status_title & "</td>" & vbCrLF & _
		"				<td class=""po_line_item_border"" align=""right"" valign=""top"">" & priceFormat(item.cost_each) & "</td>" & vbCrLF & _
		"				<td class=""po_line_item_border"" align=""right"" valign=""top"">" & priceFormat(item.cost_each * item.quantity) & "</td>" & vbCrLF
		ret_str = ret_str & _
		"			</tr>" & vbCrLF
		If item.prompt <> "" Then
			ret_str = ret_str & _
			"		<tr> " & vbCrLF & _
			"			<td class=""gray_border_right"">&nbsp;</td>" & vbCrLF & _
			"			<td colspan=""9"" class=""prompt_td"">" & vbCrLF & _
			"				<b>Custom Information:</b><br />" & vbCrLF & _
			"				"&item.prompt& vbCrLF & _
			"			</td>" & vbCrLF & _
			"		</tr>" 
		End If
	Next
			
	ret_str = ret_str & _
	"			</table>" & vbCrLF & _
	"		</td>" & vbCrLF & _
	"	</tr>" & vbCrLF & _
	"</table>" & vbCrLF
	
	po_item_details_email = ret_str
	
End Function

Sub po_item_errors(ByRef errorList)
	Dim arrayKeys
	arrayKeys = errorList.Keys
	
	For i = 0 to UBound(arrayKeys)
    %>
		<div id="<%=arrayKeys(i)%>" class="float_error_div">
			<%=errorList.Item(arrayKeys(i))%>
		</div>
	<%
	Next
End Sub

'-- Data Manipulation and Access -----------------------------------------------------'
'-------------------------------------------------------------------------------------'
' retutns true if the record exists
' data is sanitized before entering this function
' Depreciated: PO.poExists(po_number)
'Function check_po(po_number)
'	cmdAS400.CommandText = "select PRPOR# from pohedrpr where PRPOR# = '" & trim(po_number) & "'"
'	Set rsCheckPo = Server.CreateObject("ADODB.Recordset")
'	rsCheckPo.Open cmdAS400, , adOpenDynamic, adLockOptimistic
'	
'	If NOT rsCheckPo.EOF Then
'		check_po = true
'	Else
'		check_po = false
'	End If
'End Function

Function priceFormat(price_str)
	'price_str = FormatCurrency(price_Str)
	price_str = formatNumber(price_str, 2, 0, 0, 0)
	priceFormat = price_str
End Function

Function confirm_po(po_number, submited_by_email)
	cmdTemp.CommandText = "UPDATE poconfirm_po_header SET date_confirmed = (getdate()), confirmed = 'Y', submited_by_email = '"&submited_by_email&"' WHERE po_number = '"&po_number&"'"
	cmdTemp.Execute
	confirm_po = true
End Function

Function mark_as_changed(po_number)
	cmdTemp.CommandText = "UPDATE poconfirm_po_header SET changed_values = 'Y' WHERE po_number = '"&po_number&"'"
	cmdTemp.Execute
	mark_as_changed = true
End Function

Function submit_for_approval(po_number, submited_by_email)
	cmdTemp.CommandText = "UPDATE poconfirm_po_header SET date_approval_requested = (getdate()), needs_approval = 'Y', submited_by_email = '"&submited_by_email&"' WHERE po_number = '"&po_number&"'"
	cmdTemp.Execute
	submit_for_approval = true
End Function

Function approve_changes(po_number)
	cmdTemp.CommandText = "UPDATE poconfirm_po_header SET date_confirmed = (getdate()), confirmed = 'Y', needs_approval = 'N', approved = 'Y' WHERE po_number = '"&po_number&"'"
	cmdTemp.Execute
	approve_changes = true
End Function

Function save_po_comment(po_number)
	comment = Left(Trim(Request.Form("comment")), 1000)
	
	cmdTemp.CommandText = 	"UPDATE poconfirm_po_header " & _ 
							"SET comment = '"&SafeChar(comment)&"' " & _
							"WHERE po_number = '"&SafeChar(po_number)&"'"
	cmdTemp.Execute
	
	save_po_comment = true
End Function

Function save_po_items(po_number)

	i = 1
	from_state 	= SafeChar(Trim(Request.Form("from_state")))
	to_state	= SafeChar(Trim(Request.Form("to_state")))
	po_type 	= SafeChar(Trim(Request.Form("po_type")))
	do while Not IsNull(Request.Form("conf_cost"&i)) AND Trim(Request.Form("conf_cost"&i)) <> ""
		' keys to find the record
		line_no	= SafeChar(Trim(Request.Form("line_no"&i)))
		
		' DATA TO UPDATE
		' sanitize data with no feedback
		conf_quantity = SafeChar(Trim(Request.Form("conf_quantity"&i)))
		If NOT isNumeric(conf_quantity) Then
			conf_quantity = 0
		ElseIf conf_quantity < 0 Then
			conf_quantity = 0
		Else 
			conf_quantity = conf_quantity
		End If
		
		conf_cost = SafeChar(Trim(Request.Form("conf_cost"&i)))
		If NOT isNumeric(conf_cost) Then
			conf_cost = 0
		ElseIf conf_cost < 0 Then
			conf_cost = 0
		Else
			conf_cost = conf_cost
		End If
		
		' take anything under 10 chars, escape quotes
		conf_date 		= Left(Trim(Replace(Request.Form("conf_date"&i), "'", "''")), 10)
		po_date			= Left(Trim(Replace(Request.Form("po_date"&i), "'", "''")), 10) ' default promise date
		
		' calculate lead time
		from_state 	= SafeChar(Left(Request.Form("from_state"), 2))
		to_state	= SafeChar(Left(Request.Form("to_state"), 2))
		
		' conf_status_code  is varchar 10
		conf_status_code = SafeChar(Left(Trim(Request.Form("conf_status_code"&i)), 10))
		
		' dont calc promise date on Drop Ship PO's
		If po_type = "DRP" Then
			conf_promise_date = conf_date
		ElseIf conf_status_code = "BACKORDER" AND conf_date = po_date AND conf_quantity > 0Then
			conf_promise_date = calc_backorder_promise_date(conf_date)
		Else
			conf_promise_date = calc_promise_date(conf_date, from_state, to_state)
		End If
		
		
		
		' update the record
		cmdTemp.CommandText = 	"UPDATE poconfirm_po_line " & _ 
								"SET	conf_quantity = '"&SafeChar(conf_quantity)&"', conf_cost = '"&SafeChar(conf_cost)&"', " & _
								"		conf_date = '"&SafeChar(conf_date)&"', conf_promise_date = '"&SafeChar(conf_promise_date)&"', " & _
								"		conf_status_code = '"&SafeChar(conf_status_code)&"' " & _
								"WHERE  line_no = '"&SafeChar(line_no)&"' AND po_number = '"&SafeChar(po_number)&"'"
		cmdTemp.Execute
		i = i + 1
	loop
	
	save_po_items = true
End Function

Function escapeQuotes(str)
	escapeQuotes = Replace(str, "'", "''")
End Function

Function calc_promise_date(byVal conf_date, from_state, to_state)
	
	if isDate(conf_date) Then
		' query the database for shipping time
		cmdTemp.CommandText = "SELECT days FROM poconfirm_transit_time_fedex_ground_us WHERE (from_state = '"&from_state&"') AND (to_state = '"&to_state&"')"
		Set rsTransitTime = Server.CreateObject("ADODB.Recordset")
		rsTransitTime.Open cmdTemp, , adOpenStatic, adLockOptimistic
		
		' weekdays to add to the shipdate to get the promise date
		If NOT rsTransitTime.EOF Then
			shipping_days = rsTransitTime("days")
		Else		
			shipping_days = 0
		End If
		
		weekdays_to_ad = shipping_days + glo_processing_days
		
		promise_date = WeekDayAdd(conf_date, weekdays_to_ad)
		
		' turn it back into a string
		calc_promise_date = Right("0" & Month(promise_date), 2)&"/"&Right("0"&Day(promise_date), 2)&"/"&Year(promise_date)
	Else
		calc_promise_date = conf_date
	End If
	
End Function

Function calc_backorder_promise_date(byVal conf_date)
	
	if isDate(conf_date) Then
		backorder_processing_days = 10 ' week days
		
		weekdays_to_ad = backorder_processing_days + glo_processing_days
		
		promise_date = WeekDayAdd(conf_date, weekdays_to_ad)
		
		' turn it back into a string
		calc_backorder_promise_date = Right("0" & Month(promise_date), 2)&"/"&Right("0"&Day(promise_date), 2)&"/"&Year(promise_date)
	Else
		calc_backorder_promise_date = conf_date
	End If
	
End Function

' copied from http://www.visualbasic.happycodings.com/Date_Time/code1.html jt
Function WeekDayAdd(byVal dtDate, byVal lNumDays)
	Dim lDaysAdded, eWeekday
    Dim lAdd
    
    If lNumDays < 0 Then
        lAdd = -1
    Else
        lAdd = 1
    End If
    
    Do While lDaysAdded <> lNumDays
        dtDate = DateAdd("d", lAdd, dtDate)
        eWeekday = Weekday(dtDate)
        If eWeekday > vbSunday And eWeekday < vbSaturday Then
            lDaysAdded = lDaysAdded + lAdd
        End If
    Loop
	
    WeekDayAdd = dtDate
End Function

' validation
Function validate_po(ByRef errorList)
	
	validate_po = true
	
	po_type = SafeChar(Request.Form("po_type"))
	
	i = 1
	do while Not IsNull(Request.Form("line_no"&i)) AND Trim(Request.Form("line_no"&i)) <> ""
		
		' original data
		po_quantity 	= SafeChar(Request.Form("po_quantity"&i))
		po_date			= SafeChar(Request.Form("po_date"&i))
		po_cost			= SafeChar(Request.Form("po_cost"&i))
		
		' modified data
		conf_date 		= SafeChar(Trim(Request.Form("conf_date"&i)))
		conf_quantity 	= SafeChar(Trim(Request.Form("conf_quantity"&i)))
		If NOT IsNumeric(conf_quantity) Then 
			conf_quantity = 0
		End If
		conf_status_code= SafeChar(Trim(Request.Form("conf_status_code"&i)))
		
		' make sure quantity is not 0 and status is not INSTOCK
		If conf_quantity <= 0 AND conf_status_code = "INSTOCK" Then
			errorList.Add "conf_quantity"&i&"error", "A quantity of 0 is not allowed when the Status is ""In Stock"". Please change the Status or enter a quantity greater than 0."
			validate_po = false
		End If
		
		' catch the most common errors in conf date
		If IsDate(conf_date) OR ((conf_date = "DISCO" OR conf_date = "NLA" OR conf_date = "NONE") AND  conf_status_code <> "INSTOCK") Then
			' the date is valid
		Else
			errorList.Add "conf_date"&i&"error", "Please select a valid ship date (mm/dd/yyyy) or select the Discontinued or NLA status."
			validate_po = false
			
		End If
		
		' check that conf quantity is less than or equal to the quantity ordered
		If cDbl(conf_quantity) > cDbl(po_quantity) Then
			errorList.Add "conf_quantity"&i&"error", "The quantity availible cannot be greater than the quantity ordered."
			validate_po = false
		End If

		
		i = i + 1
	loop
	
End Function

' this function will compare each item in the po and check for changes based on post data
Function filter_po()
	filter_po = true
	
	i = 1
	do while Not IsNull(Request.Form("line_no"&i)) AND Trim(Request.Form("line_no"&i)) <> ""
		
		' original data
		po_quantity 	= SafeChar(Trim(Request.Form("po_quantity"&i)))
		po_date			= SafeChar(Trim(Request.Form("po_date"&i)))
		po_cost			= SafeChar(Trim(Request.Form("po_cost"&i)))
		' modified data
		conf_quantity 	= SafeChar(Trim(Request.Form("conf_quantity"&i)))
		conf_date		= SafeChar(Trim(Request.Form("conf_date"&i)))
		conf_cost		= SafeChar(Trim(Request.Form("conf_cost"&i)))
		conf_status_code= SafeChar(Trim(Request.Form("conf_status_code"&i)))
		
		If po_quantity <> conf_quantity Then
			filter_po = false
			exit function
		End If

		If po_date <> conf_date Then
			filter_po = false
			exit function
		End If

		If po_cost <> conf_cost Then
			filter_po = false
			exit function
		End If
		
		If UCase(conf_status_code) <> "INSTOCK" Then
			filter_po = false
			exit function
		End If
		
		i = i + 1
	loop
	
	' if we didn't find anything to check
	If i = 1 Then
		filter_po = false
	End If
	
End Function


' this function will compare each item in the po and check for changes based on post data
Function filter_drp_po()
	filter_drp_po = true
	
	i = 1
	do while Not IsNull(Request.Form("line_no"&i)) AND Trim(Request.Form("line_no"&i)) <> ""
		
		' original data
		'po_quantity 	= SafeChar(Trim(Request.Form("po_quantity"&i)))
		'po_date			= SafeChar(Trim(Request.Form("po_date"&i)))
		'po_cost			= SafeChar(Trim(Request.Form("po_cost"&i)))
		' modified data
		'conf_quantity 	= SafeChar(Trim(Request.Form("conf_quantity"&i)))
		'conf_date		= SafeChar(Trim(Request.Form("conf_date"&i)))
		'conf_cost		= SafeChar(Trim(Request.Form("conf_cost"&i)))
		conf_status_code= SafeChar(Trim(Request.Form("conf_status_code"&i)))
		
		'If po_quantity <> conf_quantity Then
		'	filter_drp_po = false
		'	exit function
		'End If

		'If po_date <> conf_date Then
		'	filter_drp_po = false
		'	exit function
		'End If

		'If po_cost <> conf_cost Then
		'	filter_drp_po = false
		'	exit function
		'End If
		
		If UCase(conf_status_code) <> "INSTOCK" Then
			filter_drp_po = false
			exit function
		End If
		
		i = i + 1
	loop
	
	' if we didn't find anything to check
	If i = 1 Then
		filter_drp_po = false
	End If
	
End Function

' this function will compare each item in the po to see if any changes where made
Function filter_po_strict()
	filter_po_strict = true
	
	i = 1
	do while Not IsNull(Request.Form("line_no"&i)) AND Trim(Request.Form("line_no"&i)) <> ""
		
		' original data
		po_quantity 	= SafeChar(Trim(Request.Form("po_quantity"&i)))
		po_date			= SafeChar(Trim(Request.Form("po_date"&i)))
		po_cost			= SafeChar(Trim(Request.Form("po_cost"&i)))
		' modified data
		conf_quantity 	= SafeChar(Trim(Request.Form("conf_quantity"&i)))
		conf_date		= SafeChar(Trim(Request.Form("conf_date"&i)))
		conf_cost		= SafeChar(Trim(Request.Form("conf_cost"&i)))
		conf_status_code= SafeChar(Trim(Request.Form("conf_status_code"&i)))
		
		If po_quantity <> conf_quantity Then
			filter_po_strict = false
			exit function
		End If

		If po_date <> conf_date Then
			filter_po_strict = false
			exit function
		End If

		If po_cost <> conf_cost Then
			filter_po_strict = false
			exit function
		End If
		
		If UCase(conf_status_code) <> "INSTOCK" Then
			filter_po_strict = false
			exit function
		End If
		
		i = i + 1
	loop
	
	' if we didn't find anything to check
	If i = 1 Then
		filter_po_strict = false
	End If
	
End Function
	
Function log_price_changes(po_number)
	
	i = 1
	
	po_type = SafeChar(Trim(Request.Form("po_type")))
	
	do while Not IsNull(Request.Form("line_no"&i)) AND Trim(Request.Form("line_no"&i)) <> ""
		
		' original data
		line_no		= SafeChar(Trim(Request.Form("line_no"&i)))
		po_cost		= SafeChar(Trim(Request.Form("po_cost"&i)))
		our_part_number = SafeChar(Trim(Request.Form("po_our_part_number"&i)))
		' modified data
		conf_cost	= SafeChar(Trim(Request.Form("conf_cost"&i)))

		If cDbl(po_cost) <> cDbl(conf_cost) Then
			cmdTemp.CommandText = 	_
				"INSERT INTO poconfirm_confirmed_price_changes " & _
				"	(po_number, line_no, our_part_number, po_type, cost, conf_cost, conf_date) " & _ 
				"VALUES " & _ 
				"	('" & po_number & "', '" & line_no & "', '" & our_part_number & "', '" & po_type & "', '" & po_cost & "', '" & conf_cost & "', getdate()) "
			cmdTemp.Execute			
		End If
		
		i = i + 1
	loop
	
	log_price_changes = true
	
End Function

Sub getShortApprovalList

	cmdTemp.CommandText = "SELECT " & _
				 		  "	po_number, vendor_no, po_type, vendor_name, po_date, vendor_accessed, accessed_date, confirmed, date_confirmed, needs_approval, date_approval_requested, approved " & _
						  "FROM poconfirm_po_header " & _
						  "WHERE (needs_approval = 'Y') " & glo_list_filter_sql & _
						  "ORDER BY date_approval_requested"
						  
	Set rsPOApprovalList = Server.CreateObject("ADODB.Recordset")
	rsPOApprovalList.Open cmdTemp, , adOpenDynamic, adLockOptimistic
	
	If NOT rsPOApprovalList.EOF Then
		%><table>
			<tr>
				<td>PO #</td>
				<td>&nbsp;</td>
				<td>Vendor</td>
			</tr>
		<%
		Do While NOT rsPOApprovalList.EOF 
			%>
			<tr>
				<td valign="top"><a href="confirm_po.asp?o=<%=rsPOApprovalList("po_number")%>"><%=rsPOApprovalList("po_number")%></a></td>
				<td valign="top"><%=rsPOApprovalList("po_type")%></td>
				<td valign="top"><%=rsPOApprovalList("vendor_name")%></td>
			</tr>
			<%
			rsPOApprovalList.MoveNext
		Loop
		
		rsPOApprovalList.Close
		%>
		</table>
		<%
	End If
End Sub

Sub getShortConfirmedWChangesList

	cmdTemp.CommandText = "SELECT " & _
				 		  "	po_number, vendor_no, po_type, vendor_name, po_date, vendor_accessed, accessed_date, confirmed, date_confirmed, needs_approval, date_approval_requested, approved " & _
						  "FROM poconfirm_po_header " & _
						  "WHERE (confirmed = 'Y') AND (changed_values = 'Y')" & glo_list_filter_sql & _
						  "ORDER BY po_number desc"
						  
	Set rsPOConfirmedList = Server.CreateObject("ADODB.Recordset")
	rsPOConfirmedList.Open cmdTemp, , adOpenDynamic, adLockOptimistic
	
	If NOT rsPOConfirmedList.EOF Then
		%><table>
			<tr>
				<td>PO #</td>
				<td>&nbsp;</td>
				<td>Vendor</td>
			</tr>
		<%
		Do While NOT rsPOConfirmedList.EOF 
			%>
			<tr>
				<td valign="top"><a href="confirm_po.asp?o=<%=rsPOConfirmedList("po_number")%>"><%=rsPOConfirmedList("po_number")%></a></td>
				<td valign="top"><%=rsPOConfirmedList("po_type")%></td>
				<td valign="top"><%=rsPOConfirmedList("vendor_name")%></td>
			</tr>
			<%
			rsPOConfirmedList.MoveNext
		Loop
		
		rsPOConfirmedList.Close
		%>
		</table>
		<%
	End If
End Sub

Sub getShortConfirmedList

	cmdTemp.CommandText = "SELECT TOP 20" & _
				 		  "	po_number, vendor_no, po_type, vendor_name, po_date, vendor_accessed, accessed_date, confirmed, date_confirmed, needs_approval, date_approval_requested, approved " & _
						  "FROM poconfirm_po_header " & _
						  "WHERE (confirmed = 'Y') AND (changed_values = 'N')" & glo_list_filter_sql & _
						  "ORDER BY date_confirmed desc"
						  
	Set rsPOConfirmedList = Server.CreateObject("ADODB.Recordset")
	rsPOConfirmedList.Open cmdTemp, , adOpenDynamic, adLockOptimistic
	
	If NOT rsPOConfirmedList.EOF Then
		%><table>
			<tr>
				<td>PO #</td>
				<td>&nbsp;</td>
				<td>Vendor</td>
			</tr>
		<%
		Do While NOT rsPOConfirmedList.EOF 
			%>
			<tr>
				<td valign="top"><a href="confirm_po.asp?o=<%=rsPOConfirmedList("po_number")%>"><%=rsPOConfirmedList("po_number")%></a></td>
				<td valign="top"><%=rsPOConfirmedList("po_type")%></td>
				<td valign="top"><%=rsPOConfirmedList("vendor_name")%></td>
			</tr>
			<%
			rsPOConfirmedList.MoveNext
		Loop
		
		rsPOConfirmedList.Close
		%>
		</table>
		<%
	End If
End Sub

Sub getShortAccessedList

	cmdTemp.CommandText = "SELECT " & _
				 		  "	po_number, vendor_no, po_type, vendor_name, po_date, vendor_accessed, accessed_date, confirmed, date_confirmed, needs_approval, date_approval_requested, approved " & _
						  "FROM poconfirm_po_header " & _
						  "WHERE (vendor_accessed = 'Y' AND confirmed = 'N' AND needs_approval = 'N') " & glo_list_filter_sql & _
						  "ORDER BY accessed_date desc"
				  
	Set rsPOAccessedList = Server.CreateObject("ADODB.Recordset")
	rsPOAccessedList.Open cmdTemp, , adOpenDynamic, adLockOptimistic
	
	If NOT rsPOAccessedList.EOF Then
		%><table>
			<tr>
				<td>PO #</td>
				<td>&nbsp;</td>
				<td>Vendor</td>
			</tr>
		<%
		Do While NOT rsPOAccessedList.EOF 
			%>
			<tr>
				<td valign="top"><a href="confirm_po.asp?o=<%=rsPOAccessedList("po_number")%>"><%=rsPOAccessedList("po_number")%></a></td>
				<td valign="top"><%=rsPOAccessedList("po_type")%></td>
				<td valign="top"><%=rsPOAccessedList("vendor_name")%></td>
			</tr>
			<%
			rsPOAccessedList.MoveNext
		Loop
		
		rsPOAccessedList.Close
		%>
		</table>
		<%
	End If	
End Sub

Sub getShortWaitingList

	cmdAS400.CommandText = "SELECT DISTINCT " & _
							   "	POHEDRPR.PRPOR#, POHEDRPR.PRPOTC, POHEDRPR.PRENT#, POHEDRPR.PRORDD, POHEDRPR.PRPRMD, " & _
							   "	PODETLPE.PELNST " & _
							   "FROM " & _
							   "    POHEDRPR POHEDRPR " & _
							   "INNER JOIN PODETLPE PODETLPE ON " & _
							   "    POHEDRPR.PRPOR# = PODETLPE.PEPOR#    " & _
							   "WHERE " & _
							   "    (POHEDRPR.PRPOTC = 'STD' OR " & _ 
							   "    POHEDRPR.PRPOTC = 'DRP') AND " & _
							   "    POHEDRPR.PRORDD >= 20090101 AND " & _
							   "    PODETLPE.PELNST = 'OP' AND PODETLPE.PEQOR# - (PODETLPE.PEQRC# + PODETLPE.PEQSR#)  > 0  " & _
							   "ORDER BY " & _
							   "    POHEDRPR.PRPOR# DESC "
							   
		Set pohedrpr = Server.CreateObject("ADODB.Recordset")
		pohedrpr.Open cmdAS400, , adOpenDynamic, adLockOptimistic	
		
		If NOT pohedrpr.eof Then
		
			do while NOT pohedrpr.EOF
				
				' see if it has been confirmed
				cmdTemp.CommandText = "SELECT vendor_accessed, confirmed, needs_approval, approved " & _
									  "FROM poconfirm_po_header " & _
									  "WHERE po_number = '"&pohedrpr("PRPOR#")&"'"
									  
				Set rsPOConfHeader = Server.CreateObject("ADODB.Recordset")
				rsPOConfHeader.Open cmdTemp, , adOpenDynamic, adLockOptimistic
				
				If NOT rsPOConfHeader.EOF Then
				
					confirmed			= rsPOConfHeader("confirmed")
					vendor_accessed		= rsPOConfHeader("vendor_accessed")
					needs_approval		= rsPOConfHeader("needs_approval")
					approved			= rsPOConfHeader("approved")
					
					If confirmed = "Y" Then 
						' do nothing
					ElseIf vendor_accessed = "Y" Then
						' do nothing
					ElseIf needs_approval = "Y" Then
						' do nothing
					Else
						%><a href="confirm_po.asp?o=<%=pohedrpr("PRPOR#")%>"><%=pohedrpr("PRPOR#")%></a><br /><%
					End If
									
				Else
					' if we dont find it on the MS SQL side then it needs to be shown
					%><a href="confirm_po.asp?o=<%=pohedrpr("PRPOR#")%>"><%=pohedrpr("PRPOR#")%></a><br /><%
				End If
				
				rsPOConfHeader.Close
				
				
				pohedrpr.MoveNext
			Loop
		End If	
End Sub

Function exp_po_type(str_type)
	If str_type = "STD" Then
		exp_po_type = "STANDARD"
	ElseIf str_type = "DRP" Then
		exp_po_type = "DROP SHIP"
	Else
		exp_po_type = str_type
	End If
End Function

Sub send_confirmation_email(po, vendor_email) ' vendor email comes from the account that confirmed or submited the po

	' gather information needed to email the po
%>	
<!-- 
    METADATA 
    TYPE="typelib" 
    UUID="CD000000-8B95-11D1-82DB-00C04FB1625D"  
    NAME="CDO for Windows 2000 Library" 
-->  
<%	
    if glo_production then
	    Set cdoConfig = CreateObject("CDO.Configuration")  
 
		With cdoConfig.Fields  	
			.Item(cdoSendUsingMethod) = cdoSendUsingPort  
			.Item(cdoSMTPServer) = "10.50.2.77" 
			'.Item("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = false
			.Update  
		End With 

	    Set cdoMessage = CreateObject("CDO.Message")  
 
		With cdoMessage 
			Set .Configuration = cdoConfig 
			.From = "po@ecklers.net" 
			'.Cc = "palmbayebdesigns@gmail.com"
			.To	= vendor_email
	        '.To = "palmbayebdesigns@gmail.com" 
	
			If po.po_type = "DRP" Then
				.Subject = "Dropship Status Change Order # " & po.po_number & " - Eckler Industries Inc."
			Else
				.Subject = "Purchase Order # " & po.po_number & " - Eckler Industries Inc."
			End If
				
			'If po.po_type = "DRP" Then
			'	.Importance = 2
			'End If
			
			.HTMLBody  ="<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">" _ 
					   &"<html>" & vbCrLF _
					   &"<head> " & vbCrLF _
					   & get_email_style_sheet() & vbCrLF _
					   &"</head> " & vbCrLF _
					   &"<body>" & vbCrLF _
					   &"	<center> " & vbCrLF _
					   &"	<table width=""750""><tr><td>" & vbCrLF _
					   & po_header_email(po) & vbCrLF _
					   & po_item_details_email(po) & vbCrLF _
					   &"	</td></tr></table>" & vbCrLF _
					   &"	</center>" & vbCrLF _
					   &"</body>" & vbCrLF _
					   &"</html>" & vbCrLF 
	
			.Send 
		End With 
		
	    Set cdoMessage = Nothing  
	    Set cdoConfig = Nothing  
		
	else
		' display the email without sending it
		message_body  ="<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">" _ 
					  &"<html>" & vbCrLF _
					  &"<head> " & vbCrLF _
					  & get_email_style_sheet() & vbCrLF _
					  &"</head> " & vbCrLF _
					  &"<body>" & vbCrLF _
					  &"	<center> " & vbCrLF _
					  &"	<table width=""750""><tr><td>" & vbCrLF _
					  & po_header_email(po) & vbCrLF _
					  & po_item_details_email(po) & vbCrLF _
					  &"	</td></tr></table>" & vbCrLF _
					  &"	</center>" & vbCrLF _
					  &"</body>" & vbCrLF _
					  &"</html>" & vbCrLF 
		
		Response.Write("<br />--DEV MODE START EMAIL BODY--<br />")
		Response.Write(message_body)
		Response.Write("<br />--DEV MODE END EMAIL BODY--<br />")
	End If
	
End Sub

Function get_email_style_sheet()
ret_str = _
"<style type=""text/css"">" & vbCrLF & _
"<!--   " & vbCrLF & _
" body { font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; font-size: 10px; background:#FFFFFF; margin-top: 0;} " & vbCrLF & _
" a:link, a:active, a:visited { color: #0000ff;} " & vbCrLF & _
" table { font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; font-size: 10px;} " & vbCrLF & _
" .po_table{ margin-bottom: 3px;} " & vbCrLF & _
" .po_table_header{ background-color: #0066FF; font-size:14px; color: #FFFFFF; font-weight: bold; padding:3px; text-align: center;} " & vbCrLF & _
" .po_table_body{ border: solid 1px #0066FF;} " & vbCrLF & _
" .po_line_item_col_header{ border-bottom: 1px solid #0099FF; border-right: 1px solid #0099FF;} " & vbCrLF & _
" .po_line_item_col_header2{ border-bottom: 1px solid #0099FF;} " & vbCrLF & _
" .po_line_item_border{ border-bottom: 1px solid #CCCCCC;} " & vbCrLF & _
" .highlight_input_error{ background-color: #E04E38 !important;} " & vbCrLF & _
" .error_box{ border: solid 1px #FF3366; color: #000000; font-size: 14px; background-color: #F99999; width: 750px; text-align: center;} " & vbCrLF & _
" .info_box{ border: solid 1px #0099FF; color: #000000; font-size: 14px; background-color: #D9EAFB; width: 750px; text-align: center;} " & vbCrLF & _
" .float_error_div{ width: 150px; height: 15px; visibility: hidden;	position: absolute;	background-color:#FFFFFF; border: 1px solid #FF0000;} " & vbCrLF & _
" .tr1{ background-color: #ffffff;} " & vbCrLF & _
" .tr2{background-color: #D6D6D6;} " & vbCrLF & _
" .tr_finished{ background-color: #3EFF47;} " & vbCrLF & _
" .tr_waiting{ background-color: #FFFF00;} " & vbCrLF & _
" .line_diff{ background-color: #66FFFF;} " & vbCrLF & _
" .gray_border_right{ border-right: solid 1px #CCCCCC;} " & vbCrLF & _
"-->   " & vbCrLF & _
"</style>"
	
	get_email_style_sheet = ret_str
End Function

Function get_status_code_options(conf_status_code)
	ret_str = ""
	
	' get list of line item status
	cmdTemp.CommandText = "SELECT code, title " & _
						  "FROM poconfirm_po_line_item_status_code " & _
						  "ORDER BY display_sequence"
						  
	Set rsLineStat = Server.CreateObject("ADODB.Recordset")
	rsLineStat.Open cmdTemp, , adOpenDynamic, adLockOptimistic
	
	Do While NOT rsLineStat.EOF
	
		If conf_status_code = rsLineStat("code") Then
			ret_str = ret_str & "<option value="""&rsLineStat("code")&""" SELECTED>"&rsLineStat("title")&"</option>"
		Else
			ret_str = ret_str & "<option value="""&rsLineStat("code")&""">"&rsLineStat("title")&"</option>"
		End If	
		rsLineStat.MoveNext
	Loop
	
	get_status_code_options = ret_str
End Function

Function get_submited_by_email(po_number)
	' get list of line item status
	cmdTemp.CommandText = "SELECT submited_by_email " & _
						  "FROM poconfirm_po_header " & _
						  "WHERE (po_number = '"&po_number&"')"
						  
	Set rsPO = Server.CreateObject("ADODB.Recordset")
	rsPO.Open cmdTemp, , adOpenDynamic, adLockOptimistic
	
	If NOT rsPO.EOF Then
		get_submited_by_email = rsPO("submited_by_email")
	Else
		get_submited_by_email = "noonetosendto" ' use email server to send error notification
	End If
	
End Function

Sub process_actions( po_number )

	i = 1
	
	do while Not IsNull(Request.Form("item_action"&i)) AND Trim(Request.Form("item_action"&i)) <> ""
		' keys to find the record
		line_no	= Trim(Request.Form("line_no"&i))
		
		' DATA TO UPDATE
		' sanitize data with no feedback
		item_action = Trim(Request.Form("item_action"&i))
		
		' update the record
		cmdTemp.CommandText = 	"UPDATE poconfirm_po_line " & _ 
								"SET	action = '"&SafeChar(item_action)&"' " & _
								"WHERE  line_no = '"&SafeChar(line_no)&"' AND po_number = '"&SafeChar(po_number)&"'"
		cmdTemp.Execute
		i = i + 1
	loop

End Sub


Function get_buyer_no_select(buyer_no)
	
	ret_str = ""
	
	cmdTemp.CommandText = 	"SELECT " & _
							"	buyer_number, title " & _
							"FROM " & _
							"	poconfirm_buyer_number"
						  
	Set rsPOBuyerNumber = Server.CreateObject("ADODB.Recordset")
	rsPOBuyerNumber.Open cmdTemp, , adOpenDynamic, adLockOptimistic
	
	Do While NOT rsPOBuyerNumber.EOF 
		
		buyer_number	= rsPOBuyerNumber("buyer_number")
		title			= rsPOBuyerNumber("title")
		selected_text 	= ""
		
		' select the correct value
		If buyer_no = buyer_number Then 
			selected_text = " selected=""selected"""
		End If
		
		ret_str = ret_str & "<option value="""&buyer_number&""""&selected_text&">"&title&"</option>"
		
		rsPOBuyerNumber.MoveNext
	Loop
	
	get_buyer_no_select = ret_str
	
End Function

' loop through viewed POs and remove the completed ones from the viewed list
Sub clear_accessed_pos
	cmdTemp.CommandText = "SELECT " & _
				 		  "	po_number, vendor_no, po_type, vendor_name, po_date, vendor_accessed, accessed_date, confirmed, date_confirmed, needs_approval, date_approval_requested, approved " & _
						  "FROM poconfirm_po_header " & _
						  "WHERE (vendor_accessed = 'Y' AND confirmed = 'N' AND needs_approval = 'N') " & _
						  "ORDER BY accessed_date"
				  
	Set rsPOAccessedList = Server.CreateObject("ADODB.Recordset")
	rsPOAccessedList.Open cmdTemp, , adOpenDynamic, adLockOptimistic
		
	Do While NOT rsPOAccessedList.EOF 
	
		po_number = rsPOAccessedList("po_number")
		
		if po_number = "60442933" then
			Response.Write(get_po_status(po_number))
		end if

		' check the po to see if the status is complete
		If get_po_status(po_number) = "CL" Then 
			cmdTemp.CommandText = "UPDATE poconfirm_po_header SET confirmed = '-' WHERE po_number = '"&po_number&"'"
			cmdTemp.Execute
		End if
		rsPOAccessedList.MoveNext
	Loop
		
	rsPOAccessedList.Close
	
End Sub

Function get_po_status(po_number)
	cmdAS400.CommandText = "SELECT  " & _
						   "	PROSTC " & _
						   "FROM " & _
						   "    POHED1 " & _
						   "WHERE " & _
						   "	PRPOR# = '" & trim(po_number) & "'" 
	
	Set pohedrpr = Server.CreateObject("ADODB.Recordset")
	pohedrpr.Open cmdAS400, , adOpenDynamic, adLockOptimistic	
	
	if NOT pohedrpr.EOF Then 
		get_po_status = trim(pohedrpr("PROSTC"))
	Else
		get_po_status = "NOTFOUND"
	End if

End Function

%>