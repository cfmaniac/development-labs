<%	

Function Update_AS400_PO(po_number)

	if trim(po_number) <> "0" and not isnull(po_number) Then

		cmdTemp.CommandText = "SELECT * from poconfirm_po_line " & _
							  "WHERE po_number = '" & trim(po_number) & "'"
							  
		Set rsPOline = Server.CreateObject("ADODB.Recordset")
		rsPOline.Open cmdTemp, , adOpenStatic, adLockOptimistic

		headertotal = 0
		lastdate = "19710101"
				
		do while not rsPOline.EOF
			qty_changed = false
			cost_changed = false
			
			conf_qty = 0
			conf_cost = 0
			
			line_no			= rsPOline("line_no")
			eck_part_no		= rsPOline("our_part_number")
			vendor_part_no	= rsPOline("vendor_part_number")
			conf_qty		= rsPOline("conf_quantity")
			conf_cost		= rsPOline("conf_cost")
			conf_date		= rsPOline("conf_date")
			prom_date		= rsPOline("conf_promise_date")
			action			= rsPOline("action")
			
			cmdAS400.CommandText = "SELECT * from PODETLPE WHERE PEPOR# = '" & trim(po_number) & "' " _
								 & "and PEPRT# = '" & trim(eck_part_no) & "' " _
								 & "and PESEQ# = '" & trim(line_no) & "'"
								 
			Set podetlpe = Server.CreateObject("ADODB.Recordset")
				
			podetlpe.Open cmdAS400, , adOpenDynamic, adLockOptimistic	
		
			if not podetlpe.eof Then
				vendorno = trim(podetlpe("PEENT#"))
				warehouse = trim(podetlpe("PEWHS#"))
				
				if trim(action) = "DELETE" or trim(conf_date) = "NLA" or trim(conf_date) = "DISCO" then
					qty_changed = true
					save_qty = podetlpe("PEQOR#")
					podetlpe("PELNST") = "CN"
					podetlpe("PEQOR#") = 0
					podetlpe("PECAND") = sortDate(Now())
					conf_qty = 0
				else			
					if cdbl(conf_qty) <> cdbl(podetlpe("PEQOR#")) then
						qty_changed = true
						save_qty = podetlpe("PEQOR#")
						if conf_qty = 0 then
							podetlpe("PELNST") = "CN"
							podetlpe("PEQOR#") = 0
							podetlpe("PECAND") = sortDate(Now())
						else
							podetlpe("PEQOR#") = conf_qty
							podetlpe("PEQAK#") = conf_qty
						end if				
					end if
					
					'podetlpe("PEPMDD") = mid(conf_date,7,4) & mid(conf_date,1,2) & mid(conf_date,4,2)
					'podetlpe("PEEXDD") = mid(conf_date,7,4) & mid(conf_date,1,2) & mid(conf_date,4,2)
					' new calculated promise date
					podetlpe("PEPMDD") = mid(prom_date,7,4) & mid(prom_date,1,2) & mid(prom_date,4,2)
					podetlpe("PEEXDD") = mid(prom_date,7,4) & mid(prom_date,1,2) & mid(prom_date,4,2)
					podetlpe("PERQDD") = mid(prom_date,7,4) & mid(prom_date,1,2) & mid(prom_date,4,2)
					
					if conf_qty <> 0 then
						if clng(podetlpe("PEPMDD")) > clng(lastdate) then
							lastdate = podetlpe("PEPMDD")
						end if
					end if
					
				end if
						
				if cdbl(conf_cost) <> cdbl(podetlpe("PEUPR$")) then
					cost_changed = true
					podetlpe("PEUPR$") = conf_cost
				end if
				
				headertotal = headertotal + (cdbl(conf_qty) * cdbl(conf_cost))
				
				if cost_changed then
					cmdAS400.CommandText = "SELECT * from ICECTLCT WHERE CTENT# = '" & trim(vendorno) & "' " _
										 & "and CTPRT# = '" & trim(eck_part_no) & "'"
	
					Set icectlct = Server.CreateObject("ADODB.Recordset")
						
					icectlct.Open cmdAS400, , adOpenDynamic, adLockOptimistic	
					
					if not icectlct.eof then
						icectlct("CTBSP$") = conf_cost
						
						if trim(icectlct("CTDSGF")) = "1" then
							cmdAS400.CommandText = "SELECT * from ICBALMIE WHERE IEPRT# = '" & trim(eck_part_no) & "'"
							Set iccost = Server.CreateObject("ADODB.Recordset")
							iccost.Open cmdAS400, , adOpenDynamic, adLockOptimistic
							
							do while not iccost.eof
								iccost("IEBSC$") = conf_cost
								iccost("IESTC$") = conf_cost
								iccost("IEAVC$") = conf_cost
								iccost("IELSC$") = conf_cost
								
								if trim(iccost("IEWHS#")) = "MWS" then
									cmdAS400.CommandText = "SELECT * from AUDIE WHERE IEPRT# = '" & trim(eck_part_no) & "'"
									Set audie = Server.CreateObject("ADODB.Recordset")
									audie.Open cmdAS400, , adOpenDynamic, adLockOptimistic
									
									audie.addnew
									audie("IERIDC") = iccost("IERIDC")
									audie("IECOM#") = iccost("IECOM#")
									audie("IEWHS#") = iccost("IEWHS#")
									audie("IEPRT#") = iccost("IEPRT#")
									audie("IEQOH#") = iccost("IEQOH#")
									audie("IEQIT#") = iccost("IEQIT#")
									audie("IEQOO#") = iccost("IEQOO#")
									audie("IEQOR#") = iccost("IEQOR#")
									audie("IEQBO#") = iccost("IEQBO#")
									audie("IEQCM#") = iccost("IEQCM#")
									audie("IEQRS#") = iccost("IEQRS#")
									audie("IEQTM#") = iccost("IEQTM#")
									audie("IEQLP#") = iccost("IEQLP#")
									audie("IEFL50") = iccost("IEFL50")
									audie("IELPGD") = iccost("IELPGD")
									audie("IEQUS#") = iccost("IEQUS#")
									audie("IEQUR#") = iccost("IEQUR#")
									audie("IEBSC$") = iccost("IEBSC$")
									audie("IECURC") = iccost("IECURC")
									audie("IESTC$") = iccost("IESTC$")
									audie("IEAVC$") = iccost("IEAVC$")
									audie("IELSC$") = iccost("IELSC$")
									audie("IEMEM$") = iccost("IEMEM$")
									audie("IEMEM2") = iccost("IEMEM2")
									audie("IEMEM3") = iccost("IEMEM3")
									audie("IEMNTF") = iccost("IEMNTF")
									audie("IEFL01") = iccost("IEFL01")
									audie("IESCMC") = iccost("IESCMC")
									audie("IESTCC") = iccost("IESTCC")
									audie("IEFL51") = iccost("IEFL51")
									audie("IEEASD") = iccost("IEEASD")
									audie("IELASD") = iccost("IELASD")
									audie("IEFL52") = iccost("IEFL52")
									audie("IESTCD") = iccost("IESTCD")
									audie("IEUMD") = iccost("IEUMD")
									audie("IELOC1") = iccost("IELOC1")
									audie("IELOC2") = iccost("IELOC2")
									audie("IELOC3") = iccost("IELOC3")
									audie("IEFL02") = iccost("IEFL02")
									audie("IEQTR#") = iccost("IEQTR#")
									audie("IEQAL#") = iccost("IEQAL#")
									audie("IEQIP#") = iccost("IEQIP#")
									audie("IEMM1#") = iccost("IEMM1#")
									audie("IEMM2#") = iccost("IEMM2#")
									audie("IEMM3#") = iccost("IEMM3#")
									audie("IEMM4#") = iccost("IEMM4#")
									audie("IEMM5#") = iccost("IEMM5#")
									audie("IELBR#") = iccost("IELBR#")
									audie("IEOVH#") = iccost("IEOVH#")
									audie("IEPLC1") = iccost("IEPLC1")
									audie("IEPLC2") = iccost("IEPLC2")
									audie("IEPLC3") = iccost("IEPLC3")
									audie("IERLC1") = iccost("IERLC1")
									audie("IERLC2") = iccost("IERLC2")
									audie("IERLC3") = iccost("IERLC3")
									audie("IEUSRN") = "WEBAPP"
									audie("IETRND") = sortDate(Now())
									thetime = FormatDateTime(Now(),4)
									audie("IETRT") = mid(thetime,1,2) & mid(thetime,4,2) & "30"
									audie("IETRNC") = "043"
									audie("IEPRCF") = "1"
									audie.update
								end if
								
								iccost.update
								iccost.movenext
							loop
							
							iccost.close

							cmdAS400.CommandText = "SELECT * from ICPRTMIA WHERE IAPRT# = '" & trim(eck_part_no) & "'"
							Set icprtmia = Server.CreateObject("ADODB.Recordset")
							icprtmia.Open cmdAS400, , adOpenDynamic, adLockOptimistic
							
							if not icprtmia.eof then
								if icprtmia("IARC14") = "CHD" then
	 	                           cmdAS400.CommandText = "Select * from ICOPTNJY where JYITM# = '" & Trim(Icprtmia("IAPRT#")) & "'"
									Set icoptnjy = Server.CreateObject("ADODB.Recordset")
									icoptnjy.Open cmdAS400, , adOpenDynamic, adLockOptimistic
									
									if not icoptnjy.eof then
										cmdAS400.CommandText = "SELECT * from ICBALMIE WHERE IEPRT# = '" & trim(icoptnjy("JYPRT#")) & "'"
										Set icbal2 = Server.CreateObject("ADODB.Recordset")
										icbal2.Open cmdAS400, , adOpenDynamic, adLockOptimistic
										
										do while not icbal2.eof
											icbal2("IEBSC$") = conf_cost
											icbal2("IESTC$") = conf_cost
											icbal2("IEAVC$") = conf_cost
											icbal2("IELSC$") = conf_cost
											icbal2.movenext
										loop
										
										icbal2.close
									end if
									
									icoptnjy.close
								end if
							end if
							
							icprtmia.close

						end if

						cmdAS400.CommandText = "SELECT * from AUDCT WHERE CTENT# = '" & trim(vendorno) & "' and CTPRT# = '" & trim(eck_part_no) & "'"
						Set audct = Server.CreateObject("ADODB.Recordset")
						audct.Open cmdAS400, , adOpenDynamic, adLockOptimistic
						
						audct.addnew
						audct("CTRIDC") = icectlct("CTRIDC")
						audct("CTCOM#") = icectlct("CTCOM#")
						audct("CTENT#") = icectlct("CTENT#")
						audct("CTPRT#") = icectlct("CTPRT#")
						audct("CTPT2#") = icectlct("CTPT2#")
						audct("CTPTL2") = icectlct("CTPTL2")
						audct("CTLDD#") = icectlct("CTLDD#")
						audct("CTMIN#") = icectlct("CTMIN#")
						audct("CTMNUM") = icectlct("CTMNUM")
						audct("CTINC#") = icectlct("CTINC#")
						audct("CTINUM") = icectlct("CTINUM")
						audct("CTENT2") = icectlct("CTENT2")
						audct("CTSFX2") = icectlct("CTSFX2")
						audct("CTBSP$") = conf_cost
						audct("CTUMP") = icectlct("CTUMP")
						audct("CTPR$C") = icectlct("CTPR$C")
						audct("CTMLT") = icectlct("CTMLT")
						audct("CTASC$") = icectlct("CTASC$")
						audct("CTCURC") = icectlct("CTCURC")
						audct("CTCTR#") = icectlct("CTCTR#")
						audct("CTOVRF") = icectlct("CTOVRF")
						audct("CTCMQC") = icectlct("CTCMQC")
						audct("CTCMPC") = icectlct("CTCMPC")
						audct("CTDSGF") = icectlct("CTDSGF")
						audct("CTEFCD") = icectlct("CTEFCD")
						audct("CTEXPD") = icectlct("CTEXPD")
						audct("CTRCTF") = icectlct("CTRCTF")
						audct("CTCMT#") = icectlct("CTCMT#")
						audct("CTUSRN") = "WEBAPP"
						audct("CTTRND") = sortDate(Now())
						thetime = FormatDateTime(Now(),4)
						audct("CTTRT") = mid(thetime,1,2) & mid(thetime,4,2) & "30"
						audct("CTTRNC") = "071"
						audct.update

						audct.close
						
						icectlct.update
					end if
					
					icectlct.close

					cmdAS400.CommandText = "SELECT * from EKPSHCQ WHERE VQENT# = '" & trim(vendorno) & "' " _
										 & "and VQPT2# = '" & trim(vendor_part_no) & "'"
	
					Set ekpshcq = Server.CreateObject("ADODB.Recordset")
					ekpshcq.Open cmdAS400, , adOpenDynamic, adLockOptimistic	
					
					If Not ekpshcq.EOF Then
						ekpshcq("VQINFO") = conf_cost
						ekpshcq("VQQDTE") = sortDate(Now())
						ekpshcq.Update
					Else
						ekpshcq.AddNew
						ekpshcq("VQCOM#") = "001"
						ekpshcq("VQENT#") = trim(vendorno)
						ekpshcq("VQPRT#") = trim(eck_part_no)
						ekpshcq("VQPT2#") = trim(vendor_part_no)
						ekpshcq("VQINFO") = conf_cost
						ekpshcq("VQQDTE") = sortDate(Now())
						ekpshcq.Update
					End If
					
					ekpshcq.Close
					
				end if
								
				if qty_changed then
					cmdAS400.CommandText = "SELECT * from ICBALMIE WHERE IEPRT# = '" & trim(eck_part_no) & "' " _
										 & "and IEWHS# = '" & trim(warehouse) & "'"
										 
					Set icbalmie = Server.CreateObject("ADODB.Recordset")
						
					icbalmie.Open cmdAS400, , adOpenDynamic, adLockOptimistic
					
					if not icbalmie.eof then
						newqty = (cdbl(icbalmie("IEQOO#")) - cdbl(save_qty)) + cdbl(conf_qty)
						icbalmie("IEQOO#") = newqty
						icbalmie.update
					end if
					
					icbalmie.close
				end if
				
				podetlpe.update
			end if

			podetlpe.close
						
			rsPOline.movenext
		loop

		cmdAS400.CommandText = "SELECT * from POHEDRPR WHERE PRPOR# = '" & trim(po_number) & "'" 
							 
		Set pohedrpr = Server.CreateObject("ADODB.Recordset")
			
		pohedrpr.Open cmdAS400, , adOpenDynamic, adLockOptimistic
		
		if not pohedrpr.eof then
			pohedrpr("PRTAM$") = headertotal
			pohedrpr("PRAMT$") = headertotal
			pohedrpr("PRPRMD") = lastdate
			pohedrpr("PRREQD") = lastdate
			
			if headertotal = 0 then
				pohedrpr("PROSTC") = "CN"
				pohedrpr("PRCAND") = sortDate(Now())
			end if
			
			if trim(pohedrpr("PRPOTC")) = "DRP" then
				pohedrpr("PRASUF") = "1"
				pohedrpr("PRPRCF") = " "
			end if
			
			pohedrpr.update
		end if
		
		pohedrpr.close

		cmdAS400.CommandText = "SELECT * from POHISTPY WHERE PYPOR# = '" & trim(po_number) & "'" 
		Set pohistpy = Server.CreateObject("ADODB.Recordset")
		pohistpy.Open cmdAS400, , adOpenDynamic, adLockOptimistic
		
		pohistpy.addnew
		pohistpy("PYRIDC") = "PY"
		pohistpy("PYCOM#") = "001"
		pohistpy("PYPOR#") = trim(po_number)
		pohistpy("PYSTSF") = "1"
		pohistpy("PYENT#") = trim(vendorno)
		pohistpy("PYSFX#") = "000"
		pohistpy("PYTRND") = sortDate(Now())

		thetime = FormatDateTime(Now(),4)
		pohistpy("PYTRT") = mid(thetime,1,2) & mid(thetime,4,2) & "30"

		pohistpy("PYTRNC") = "025"
		pohistpy("PYRELN") = "0"
		pohistpy("PYUSER") = "WEBAPP"
		pohistpy.update
		
		if headertotal = 0 then
			pohistpy.addnew
			pohistpy("PYRIDC") = "PY"
			pohistpy("PYCOM#") = "001"
			pohistpy("PYPOR#") = trim(po_number)
			pohistpy("PYSTSF") = "1"
			pohistpy("PYENT#") = trim(vendorno)
			pohistpy("PYSFX#") = "000"
			pohistpy("PYTRND") = sortDate(Now())
	
			thetime = FormatDateTime(Now(),4)
			pohistpy("PYTRT") = mid(thetime,1,2) & mid(thetime,4,2) & "30"
	
			pohistpy("PYTRNC") = "015"
			pohistpy("PYRELN") = "0"
			pohistpy("PYUSER") = "WEBAPP"
			pohistpy.update
		end if			
		
		pohistpy.close
		
	end if	
			
end function

Function sortDate(pDate)
    aDate = CDate(pDate)
    sortDate = Year(aDate)&Right("0"&Month(aDate),2)&Right("0"&Day(aDate),2)
End Function



%>