<%
Class PO
	Public po_number
	Public our_cust_number
	Public po_date
	Public vendor_no
	Public po_type
	Public vendor_suffix
	Public ship_via
	Public term_code ' used to get the terms
	Public terms
	Public vendor_contact
	Public buyer_number
	Public bill_to_no
	Public bill_to_suffix
	Public ship_to_no
	Public ship_to_suffix
	Public po_msg_no
	Public po_msg
	Public po_display
	
	Public confirmed
	Public needs_approval
	Public approved
	Public vendor_accessed
	Public accessed_date
	Public date_approval_requested
	
	Public comment
	Public po_notes
	Public use_euro
	Public buyer_name
	Public buyer_phone
	Public orderno
	
	Public default_promise_date
	
	' PO_Address
	Public address_billing
	Public address_shipping
	Public address_vendor
	
	' Accesscontrol and Accountability
	Public user
	
	' conditional display
	Public show_submit_for_approval_button
	
	' database
	Public cmdAS400
	Public cmdTemp
	
	' Storage For Line Items
	Dim items
	
	' Behavioral flags
	Public items_view_mode ' all_items, open_items, unreceived_items
	
	Private Sub Class_Initialize() ' fill in default values
		po_number = "0"
		our_cust_number = "" 
		po_date = ""
		vendor_no = ""
		po_type = ""
		'vendor_suffix
		ship_via = ""
		'term_code
		terms    = ""
		vendor_contact = ""
		buyer_number = ""
		'bill_to_no
		'bill_to_suffix
		'ship_to_no
		'ship_to_suffix	
		
		confirmed = "N"
		comment = ""
		po_notes = ""
		use_euro = "N"
		buyer_name = ""
		buyer_phone = ""
		orderno = ""
		
		Set items = CreateObject("Scripting.Dictionary")
		
		items_view_mode = "open_items"
		
	End Sub
	
	Public Function load_data(po_num)
	
		' Begin Loading the PO Header information
		po_number = po_num
		
		xmlStr = "<?xml version=""1.0""?>" & vbcrlf 
		xmlStr = xmlStr & "<root>" & vbcrlf 
		xmlStr = xmlStr & vbtab & "<info_request>" & vbcrlf 
		xmlStr = xmlStr & vbtab & vbtab & "<po_no>" & trim(po_number) & "</po_no>" & vbcrlf

		if user.vendor_no <> 0 then
			xmlStr = xmlStr & vbtab & vbtab & "<vendor_no>" & trim(user.vendor_no) & "</vendor_no>" & vbcrlf
		else
			xmlStr = xmlStr & vbtab & vbtab & "<vendor_no> </vendor_no>" & vbcrlf
		end if
		
		xmlStr = xmlStr & vbtab & "</info_request>" & vbcrlf 			
		xmlStr = xmlStr & "</root>" 
		

		'Load the XML into an XMLDOM object 
		Set SendDoc = server.createobject("Microsoft.XMLDOM") 
		SendDoc.ValidateOnParse= True 
		SendDoc.LoadXML(xmlStr) 

		'Set the URL of the receiver 
		xURL = "http://10.50.2.8:8181/myrxs/th411" 

		'Call the XML Send function) 
		set NewDoc = xmlSend (xURL, SendDoc)'xmlString) 
		
		'We receive back another XML DOM object! 
		
		'For testing 
		'response.Write "<b>XML DOC posted off:</b><br>" 
		'response.write SendDoc.XML & "<br>" 
		'response.write "<b>Target URL:</b> " & xURL & "<br>" 
		'response.write "<b>XML DOC Received back: </b><br>" 
		'response.write (NewDoc.Xml) 

		Set nodeList = NewDoc.selectnodes("//po_info") 

		'for testing
		'if not (nodeList is nothing) then
		'	for each node in nodeList 
		'		thepo = node.selectSingleNode("po_no").text
		'		response.write thepo	
		'	next 
		'end if	
		
		if not (nodeList is nothing) then	
			for each node in nodeList 
				thepo = node.selectSingleNode("po_no").text
				thepo_type = node.selectSingleNode("po_type").text
				thepo_date = node.selectSingleNode("po_date").text
		
				po_date 		= jt_convertDate(node.selectSingleNode("po_date").text) 'CREATION DATE
				default_promise_date	= Cdate(DateAdd("d",1,jt_convertDate(node.selectSingleNode("po_date").text))) 'Ship Date
				default_promise_date	= Right("0" & Month(default_promise_date), 2)&"/"&Right("0"&Day(default_promise_date), 2)&"/"&Year(default_promise_date)
				
				glo_po.po_date		= jt_convertDate(node.selectSingleNode("po_date").text) 'CREATION DATE
				
				'po_notes	=  pohedrpr("")
			
				vendor_no 		= trim(node.selectSingleNode("vendor_no").text)
				po_type			= trim(node.selectSingleNode("po_type").text)
				vendor_suffix 	= trim(node.selectSingleNode("vendor_suffix").text)
				ship_via 		= trim(node.selectSingleNode("ship_via").text)
				' used to get the value from the 'Discount Terms Code'
				term_code 		= trim(node.selectSingleNode("terms_code").text)
				vendor_contact	= trim(node.selectSingleNode("vendor_contact").text)
				buyer_number	= trim(node.selectSingleNode("buyer_no").text)
				bill_to_no		= trim(node.selectSingleNode("billto_no").text)
				bill_to_suffix	= trim(node.selectSingleNode("billto_suffix").text)
				ship_to_no		= trim(node.selectSingleNode("shipto_no").text)
				ship_to_suffix	= trim(node.selectSingleNode("shipto_suffix").text)
				orderno         = trim(node.selectSingleNode("order_no").text)
				po_msg_no		= trim(node.selectSingleNode("po_msg").text)
				po_diplsy		= trim(node.selectSingleNode("po_diplsy").text)
			
				Set address_billing  = get_address_from_as400( glo_po.bill_to_no, glo_po.bill_to_suffix, "billto", NewDoc.Xml)
				Set address_shipping = get_address_from_as400( glo_po.ship_to_no, glo_po.ship_to_suffix, "shipto", NewDoc.Xml)
				Set address_vendor	 = get_address_from_as400( glo_po.vendor_no,  glo_po.vendor_suffix, "vendor", NewDoc.Xml)
				
				our_cust_number = trim(node.selectSingleNode("acct_no").text) 'our account# with vendor
				use_euro = trim(node.selectSingleNode("use_euro").text)
				ship_via = trim(node.selectSingleNode("ship_via_desc").text)
				terms = trim(node.selectSingleNode("terms_desc").text)
				buyer_name = trim(node.selectSingleNode("buyer_name").text)
				buyer_phone = trim(node.selectSingleNode("buyer_email").text)
				po_msg = trim(node.selectSingleNode("po_msg").text)

			' check to see if there is extra information about this order
				cmdTemp.CommandText = "SELECT po_number, vendor_no, po_type, buyer_no, po_date, vendor_accessed, accessed_date, confirmed, date_confirmed, needs_approval, date_approval_requested, approved, comment " & _
								  "FROM poconfirm_po_header " & _
								  "WHERE po_number = '"&po_number&"' AND vendor_no = '"&vendor_no&"'"
				Set rsPOConfHeader  = Server.CreateObject("ADODB.Recordset")
				rsPOConfHeader.Open cmdTemp, , adOpenStatic, adLockOptimistic	
			
				If NOT rsPOConfHeader.EOF Then
					
					vendor_accessed	= rsPOConfHeader("vendor_accessed")
					accessed_date	= rsPOConfHeader("accessed_date")
					confirmed		= rsPOConfHeader("confirmed")
					needs_approval	= rsPOConfHeader("needs_approval")
					date_approval_requested	= rsPOConfHeader("date_approval_requested")
					approved		= rsPOConfHeader("approved")
					date_confirmed	= rsPOConfHeader("date_confirmed")
					
					comment 		= rsPOConfHeader("comment")
					
					If isNull(comment) Then
						comment = ""
					End If
					
					If vendor_accessed = "N" AND  user.get_vendor_no() <> "0" Then 
						rsPOConfHeader("vendor_accessed") = "Y"
						rsPOConfHeader("accessed_date") = Now()
						rsPOConfHeader.Update
					End If
					rsPOConfHeader.Close
					' update the accessed times
					
				' if there isn't any info in the db then insert it
				Else
					rsPOConfHeader.Close
					
					vendor_name = trim(node.selectSingleNode("vendor_name").text)
					
					' save record to keep track of changes to this po
					If user.get_vendor_no() <> "0" Then ' vendor
						cmdTemp.CommandText = "INSERT INTO poconfirm_po_header " & _
											  "	(po_number, vendor_no, po_type, buyer_no, vendor_name, po_date, vendor_accessed, accessed_date, confirmed, date_confirmed, location) " & _
											  "VALUES " & _
											  "	('"&po_number&"', '"&vendor_no&"', '"&po_type&"', '"&buyer_number&"', '"&escapeQuotes(vendor_name)&"', '"&po_date&"', 'Y', GETDATE(), " & _
											  "	 'N', null, '"&address_shipping.state&"') "
					Else ' eckler's employee
						cmdTemp.CommandText = "INSERT INTO poconfirm_po_header " & _
											  "	(po_number, vendor_no,  po_type, buyer_no, vendor_name, po_date, vendor_accessed, accessed_date, confirmed, date_confirmed, location) " & _
											  "VALUES " & _
											  "	('"&po_number&"', '"&vendor_no&"', '"&po_type&"', '"&buyer_number&"', '"&escapeQuotes(vendor_name)&"', '"&po_date&"', 'N', GETDATE(), " & _
											  "	 'N', null, '"&address_shipping.state&"') "
					End If
					
					cmdTemp.Execute
				End If
			
				' NOTE: add additional Error Checking/Notification 
				load_data = True
			
				' start Loading Line Items
				load_line_items(NewDoc.Xml)			
			
			Next
		Else
			load_data = False
		End If 
		
	End Function
	
	Public Function isConfirmed()
		If confirmed = "Y" Then
			IsConfirmed = True
		Else
			IsConfirmed = False
		End If
	End Function
	
	Public Function isEditable()
		If confirmed = "Y" Then
			IsEditable = False
		ElseIf needs_approval = "Y" Then
			IsEditable = False
		Else
			IsEditable = True
		End If
	End Function
	
	Public Function isLoaded()
		isLoaded = is_loaded ' Mak
	End Function
	
	
	' ------ Private Functions ----------------
	
	'' load_line_items() ''''''''''''''''''''''''''''''''''''''''''''
	' Description: 	Queries the AS400 for line items after the      '
	'				header information has been loaded.             '
	'				                                                '
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	Private Function load_line_items(xmlStr)
		Set xmlDoc = CreateObject("Microsoft.XMLDOM") 
		xmlDoc.async = False 
		xmlDoc.LoadXml(xmlStr) 
		
		items_view_mode = "all_items"

		Set nodeListLines = xmlDoc.selectnodes("//po_info/po_item") 

		'for testing
		'if not (nodeList is nothing) then
		'	for each node in nodeList 
		'		thepo = node.selectSingleNode("po_number").text
		'		response.write thepo	
		'	next 
		'end if	
		
		if not (nodeListLines is nothing) then	
			i = 1 ' counter
			for each node in nodeListLines 
				our_part_number	= trim(node.selectSingleNode("eck_item_no").text)
				quantity 	= cint(trim(node.selectSingleNode("qty_ordered").text))
				quantity_recieved = cint(trim(node.selectSingleNode("qty_received").text))
				line_no		= trim(node.selectSingleNode("line_no").text)
				cost_each 	= trim(node.selectSingleNode("cost").text)
				uom 		= trim(node.selectSingleNode("uom").text)
				item_status = trim(node.selectSingleNode("line_status").text)
				our_description = trim(node.selectSingleNode("eck_item_desc").text)
				vendor_part_number	= trim(node.selectSingleNode("vnd_item_no").text)
				vendor_description 	= trim(node.selectSingleNode("vnd_item_desc").text)
				euro_cost_each 		= trim(node.selectSingleNode("euro_cost").text)
				
				'if trim(po_type) = "DRP" then
					oe_comment_L1 = trim(node.selectSingleNode("order_line_comment1").text)
					oe_comment_L2 = trim(node.selectSingleNode("order_line_comment2").text)
					oe_ship_via_desc = trim(node.selectSingleNode("order_line_ship_via").text)
				'end if
				
				Set nodeListPrompts = node.selectnodes("prompt_info") 

				if not (nodeListPrompts is nothing) then	
					for each prompt_info in nodeListPrompts 
						prompts = trim(prompts) & trim(prompt_info.selectSingleNode("prompt_question").text) & " : " & trim(prompt_info.selectSingleNode("prompt_answer").text) & "<br>"
					next
				end if

				' check if changes have already been made
				cmdTemp.CommandText = "SELECT " & _
									  "	pol.po_number, pol.line_no, pol.vendor_part_number, pol.our_part_number, pol.vendor_description, " & _
									  "	pol.conf_quantity, pol.conf_cost, pol.conf_date, pol.conf_status_code, pol.action, sc.title as conf_status_title " & _
									  "FROM poconfirm_po_line pol " & _
									  "LEFT OUTER JOIN poconfirm_po_line_item_status_code sc on pol.conf_status_code = sc.code " & _
									  "WHERE (po_number = '"&po_number&"') AND (line_no = '"&line_no&"') AND (our_part_number = '"&our_part_number&"')"
				
				Set rsPOConfLine = Server.CreateObject("ADODB.Recordset")
				rsPOConfLine.Open cmdTemp, , adOpenStatic, adLockOptimistic
				
				' If we already have saved changes
				If NOT rsPOConfLine.EOF Then
					conf_quantity 		= rsPOConfLine("conf_quantity")
					conf_cost 			= rsPOConfLine("conf_cost")
					conf_date 			= rsPOConfLine("conf_date")
					conf_status_code	= rsPOConfLine("conf_status_code")
					conf_status_title	= rsPOConfLine("conf_status_title")
					conf_action			= rsPOConfLine("action")
				
				Else	' otherwise use defaults and save the record			
					'rsPOConfLine.Close	
					conf_quantity 		= quantity 
					conf_cost 			= cost_each 
					conf_date 			= default_promise_date
					conf_status_code	= "INSTOCK"
					conf_status_title	= "In Stock"
					conf_action			= ""
					
					
					cmdTemp.CommandText = "INSERT INTO poconfirm_po_line " & _
										  "	(po_number, line_no, vendor_part_number, our_part_number, vendor_description, " & _
										  "	 conf_quantity, conf_cost, conf_date, conf_status_code) " & _
										  "VALUES " & _
										  "	('"&po_number&"', '"&line_no&"', '"&escapeQuotes(vendor_part_number)&"', '"&our_part_number&"', '"&escapeQuote(vendor_description)&"', " & _
										  "	 '"&conf_quantity&"', '"&conf_cost&"', '"&conf_date&"', '"&conf_status_code&"') "
					Set rsPOInsertConfLine = Server.CreateObject("ADODB.Recordset")
					
					'rsPOInsertConfLine.Open cmdTemp, , adOpenDynamic, adLockOptimistic
					rsPOInsertConfLine.Open cmdTemp, , adOpenStatic, adLockOptimistic
					
					If rsPOInsertConfLine.State <> 0 Then
						rsPOInsertConfLine.Close
					End If						
					
				End If
				
				rsPOConfLine.Close
	
				Set newLine = new PO_Line
				newLine.line_no 			= line_no
				newLine.vendor_part_number	= vendor_part_number
				newLine.our_part_number		= our_part_number
				newLine.vendor_description 	= vendor_description
				newLine.our_description 	= our_description
				newLine.quantity 			= quantity 
				newLine.quantity_recieved 	= quantity_recieved
				newLine.uom 				= uom
				newLine.cost_each 			= cost_each
				newLine.euro_cost_each 		= euro_cost_each
				newLine.conf_quantity 		= conf_quantity
				newLine.conf_date 			= conf_date
				newLine.conf_status_code 	= conf_status_code
				newLine.conf_status_title 	= conf_status_title
				newLine.conf_cost 			= conf_cost
				newLine.item_status			= item_status
				newLine.oe_comment_L1		= oe_comment_L1
				newLine.oe_comment_L2		= oe_comment_L2
				newLine.oe_ship_via_desc	= oe_ship_via_desc
						
				newLine.prompt				= prompts		
				items.Add ""&i, newLine
				 
				'Set newLine = Nothing 
				
				' move to next line
				i = i + 1
				
				prompts = ""
				
			next
			
			load_line_items = True			
		Else
			
			load_line_items = False
		End If
			
	End Function
	
	'' get_address_from_as400( no, suffix) ''''''''''''''''''''''''''
	' Description: 	Queries the AS400 for a address based on the 	'
	'				the vendor's number and address suffix.         '
	'				Returns a PO_Address Object	see PO_Address.asp  '
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	Public Function get_address_from_as400( no, suffix, thetype, xmlStr)
		' default 
		ret_str = "" ' address
		ret_str2= "" ' non address contact info
		to_name = ""
		street1 = ""
		street2 = ""
		street3 = ""
		city 	= ""
		state 	= ""
		zip 	= ""
		country = ""
		phone 	= ""
		fax 	= ""
		email 	= ""

		Set xmlDoc = CreateObject("Microsoft.XMLDOM") 
		xmlDoc.async = False 
		xmlDoc.LoadXml(xmlStr) 
		
'		Set Node = objXMLDoc.documentElement.selectSingleNode("Parent Node/NodeTextToRead")

		if trim(thetype) = "vendor" then
			Set nodeListaddr = xmlDoc.selectnodes("//po_info/vendor_info") 
		else
			if trim(thetype) = "billto" then
				Set nodeListaddr = xmlDoc.selectnodes("//po_info/billto_info") 
			else
				Set nodeListaddr = xmlDoc.selectnodes("//po_info/shipto_info") 
			end if
		end if

		'for testing
		'if not (nodeListaddr is nothing) then
		'	for each node in nodeListaddr 
		'		thepo = node.selectSingleNode("po_number").text
		'		response.write thepo	
		'	next 
		'end if	
		
		if not (nodeListaddr is nothing) then	
			for each node in nodeListaddr
				to_name = trim(node.selectSingleNode("name").text)
				street1 = trim(node.selectSingleNode("street1").text)
				street2 = trim(node.selectSingleNode("street2").text)
				street3 = trim(node.selectSingleNode("street3").text)
				city 	= trim(node.selectSingleNode("city").text)
				state 	= trim(node.selectSingleNode("state").text)
				zip 	= trim(node.selectSingleNode("zip").text)
				country = trim(node.selectSingleNode("country").text)
				phone 	= trim(node.selectSingleNode("phone").text)
				fax 	= trim(node.selectSingleNode("fax").text)
				email 	= trim(node.selectSingleNode("email").text)
			
			' create a new address obj
				Dim retAddr
				Set retAddr = new PO_Address
				retAddr.init to_name, street1, street2, street3, city, state, "", zip, country, phone, fax, email
				
				Set get_address_from_as400 = retAddr
			next
		Else
			' default is to return nothing
			Set get_address_from_as400 = Nothing		
		End If
		
	End Function

	'' poExists(po_number) ''''''''''''''''''''''''''''''''''''''''''
	' Description: 	Queries the AS400 for the po number and returns '
	'				true if po number exists false if it does not	'
	'				exist.											'
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	Function poExists(po_number)
		cmdAS400.CommandText = "select PRPOR# from pohed1 where PRPOR# = '" & SafeChar(trim(po_number)) & "'"
		Set rsCheckPo = Server.CreateObject("ADODB.Recordset")
		rsCheckPo.Open cmdAS400, , adOpenDynamic, adLockOptimistic
		
		If NOT rsCheckPo.EOF Then
			poExists = true
		Else
			poExists = false
		End If
		rsCheckPo.Close
		
	End Function
End Class

%>