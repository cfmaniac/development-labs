<%
Class PO_Line
	
	Public line_no
	Public vendor_part_number
	Public our_part_number
	Public vendor_description
	Public our_description
	Public quantity
	Public quantity_recieved
	Public uom
	Public cost_each
	Public euro_cost_each
	Public conf_quantity
	Public conf_date
	Public conf_status_code
	Public conf_status_title
	Public conf_cost 
	Public conf_action
	Public item_status
	Public prompt
	Public oe_comment_L1
	Public oe_comment_L2
	Public oe_ship_via_desc
	
	Private Sub Class_Initialize() ' fill in default values	
		line_no 			= "" 
		vendor_part_number 	= ""
		our_part_number 	= ""
		vendor_description	= ""
		our_description 	= "" 
		our_description 	= ""
		quantity 			= 0
		quantity_recieved	= 0
		uom					= "-"
		cost_each 			= 0
		euro_cost_each		= 0
		conf_quantity		= 0
		conf_date			= ""
		conf_status_code	= ""
		conf_status_title	= ""
		conf_cost 			= 0
		conf_action 		= "" ' OK
		item_status			= ""
		oe_comment_L1		= ""
		oe_comment_L2		= ""
		oe_ship_via_desc	= ""
	End Sub

End Class
%>