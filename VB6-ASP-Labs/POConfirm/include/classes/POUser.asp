<%

Class POUser
	' database
	Dim email_address
	Dim first_name
	Dim last_name
	Dim company
	Dim vendor_no
	Dim start_date
	Dim account_created
	Dim last_login
	Dim last_login_ip
	Dim session_id
		
	'class data
	Dim logged_in
	Dim cmd
		
	Public Sub Class_Initialize()
		logged_in = false
	End Sub
	
	Function set_cmd(cmdTemp)
		Set cmd = cmdTemp
	End Function
	
	Public Sub init(rsuser)
	
		email_address	= rsuser("email_address")
		first_name		= rsuser("first_name")
		last_name		= rsuser("last_name")
		company			= rsuser("company")
		vendor_no		= rsuser("vendor_no")
		start_date		= rsuser("start_date")
		account_created	= rsuser("account_created")
		last_login		= rsuser("last_login")
		last_login_ip	= rsuser("last_login_ip")
		session_id 		= rsuser("session_id")
	End Sub
	
	Sub deinit()
		email_address	= ""
		first_name		= ""
		last_name		= ""
		company			= ""
		vendor_no		= ""
		start_date		= ""
		account_created	= ""
		last_login		= ""
		last_login_ip	= ""
		session_id 		= ""
	End Sub
	
	' attempt to authenticate the user
	Function do_login(username, password)
		
		If chkEmail(username) = 1 AND len(password) >= 8 Then ' rough validation
			'sanatize input
			username = SafeChar(username)
			password = SafeChar(password)
			
			' see if the user exists
			cmd.CommandText = "SELECT email_address FROM poconfirm_users WHERE email_address = '"&username&"' AND password = '"&password&"'"
			
			Set rslogin = Server.CreateObject("ADODB.Recordset")
			rslogin.Open cmd, , adOpenStatic, adLockReadOnly
			
			If NOT rslogin.EOF Then
				' mark the user as logged in
				cmd.CommandText = "UPDATE poconfirm_users " & _
								  "SET session_id = '"&session.SessionID&"', last_login = GETDATE(), " & _
								  "	   last_login_ip = '"&Request.ServerVariables("REMOTE_ADDR")&"' " & _
								  "WHERE email_address = '"&username&"' AND password = '"&password&"'"
				cmd.Execute
				do_login = true
			Else
				do_login = false
			End If
		Else
			do_login = false
		End If
		
		' audit login activity
		result = "denied"
		If do_login Then
			result = "allowed"
		End If

		cmd.CommandText = "INSERT INTO poconfirm_login_audit " & _
						  "		(username, result, ipaddress) " & _
						  "VALUES " & _
						  "		('"&username&"', '"&result&"', '"&Request.ServerVariables("REMOTE_ADDR")&"')"
		cmd.Execute
		
		' clear out records older than 14 days
		cmd.CommandText = "DELETE FROM poconfirm_login_audit " & _
						  "WHERE (attempt_date < DATEADD(day, - 30, GETDATE()))"
		cmd.Execute
	End Function

	' Forgotten Password request
    	Function get_pass(username)


    			username = SafeChar(username)


    			' see if the user exists
    			cmd.CommandText = "SELECT password FROM poconfirm_users WHERE email_address = '"&username&"'"

    			Set rsPass = Server.CreateObject("ADODB.Recordset")
    			rsPass.Open cmd
   				cmd.Execute

   				password = rsPass("password")


    	End Function
	
	' see if the user is logged in if so return
	Function get_logged_in()
	
		cmd.CommandText = "SELECT * FROM poconfirm_users " & _ 
						  "WHERE session_id = '"&session.SessionID&"' AND last_login_ip = '"&Request.ServerVariables("REMOTE_ADDR")&"'"
		Set rslogin = Server.CreateObject("ADODB.Recordset")
		rslogin.Open cmd, , adOpenStatic, adLockReadOnly
		
		If NOT rslogin.EOF Then	
			logged_in = true
			init(rslogin)
			get_logged_in = true
		Else
			get_logged_in = false
		End If
		
	End Function

	' log the user out
	Function do_logout()
	
		cmd.CommandText = "UPDATE poconfirm_users " & _
						  "SET session_id = '' " & _ 
						  "WHERE session_id = '"&session.SessionID&"' AND last_login_ip = '"&Request.ServerVariables("REMOTE_ADDR")&"'"
		cmd.Execute
		logged_in = false
		deinit
		do_logout = true
	End Function
	
	' checks to see if the current password is = 'password' 
	' so we can ask them to change it.
	Function has_default_password()
		cmd.CommandText = "SELECT password FROM poconfirm_users " & _ 
						  "WHERE session_id = '"&session.SessionID&"' AND last_login_ip = '"&Request.ServerVariables("REMOTE_ADDR")&"'"
		Set rschkpasswd = Server.CreateObject("ADODB.Recordset")
		rschkpasswd.Open cmd, , adOpenStatic, adLockReadOnly
		
		If NOT rschkpasswd.EOF Then	
			If trim(rschkpasswd("password")) = "password" Then
				has_default_password = true
				Exit Function
			Else
				has_default_password = false
				Exit Function
			End If
		Else
			has_default_password = false
			Exit Function
		End If
		rschkpasswd.Close
		
	End Function
	
	Function pw_policy_check(password)
		
		password = trim(password) 
		
		
		If UCase(password) = "PASSWORD" Then ' password must not be 'password'
			pw_policy_check = false
			exit function
		ElseIf Len(password) < 8 Then ' password must be at least 8 charactors
			pw_policy_check = false
			exit function
		Else
			pw_policy_check = true
			exit function
		End If
		
	End Function
	
	Function change_password(old_password, new_password)
		cmd.CommandText = "UPDATE poconfirm_users " & _
						  "SET password = '"&new_password&"' " & _ 
						  "WHERE  email_address = '"&email_address&"' AND " & _
						  "		  session_id = '"&session.SessionID&"' AND " & _
						  "		  last_login_ip = '"&Request.ServerVariables("REMOTE_ADDR")&"' AND " & _ 
						  " 	  password = '"&old_password&"'"
		cmd.Execute
		
		cmd.CommandText = "SELECT * FROM poconfirm_users " & _
						  "WHERE  email_address = '"&email_address&"' AND " & _
						  "		  session_id = '"&session.SessionID&"' AND " & _
						  "		  last_login_ip = '"&Request.ServerVariables("REMOTE_ADDR")&"' AND " & _ 
						  " 	  password = '"&new_password&"'"
		'verify the password was updated
		Set rschkpasswd = Server.CreateObject("ADODB.Recordset")
		rschkpasswd.Open cmd, , adOpenStatic, adLockReadOnly
		
		' Display the first field in the recordset.
		If NOT rschkpasswd.EOF Then 
			change_password = true
		Else
			change_password = false
		End If
		rschkpasswd.Close
	End Function
	
	' ACCESS FUNCTIONS
	Function get_email_address()
		get_email_address = email_address
	End Function
	
	Function get_first_name()
		get_first_name = first_name
	End Function
	
	Function get_last_name()
		get_last_name = last_name
	End Function
	
	Function get_company()
		get_company = company
	End Function
	
	Function get_vendor_no()
		get_vendor_no = vendor_no
	End Function

	Function get_start_date()
		get_start_date = start_date
	End Function
	
	Function get_account_created()
		get_account_created = account_created
	End Function
	
	Function get_last_login()
		get_last_login = last_login
	End Function
	
	Function get_last_login_ip()
		get_last_login_ip = last_login_ip
	End Function
	
	Function get_session_id()
		get_session_id = session_id
	End Function
	
End Class


%>