<%
Class PO_Address
	Private g_name, g_street1, g_street2, g_street3, g_city, g_state, g_province, g_zip, g_country, g_phone, g_fax, g_email
	
	Public Sub Class_Initialize()
		
	End Sub
	
	Public Sub init( s_name, s_street1, s_street2, s_street3, s_city, s_state, s_province, s_zip, s_country, s_phone, s_fax, s_email)
		g_name 		= s_name
		g_company 	= s_company
		g_street1 	= s_street1
		g_street2 	= s_street2
		g_city 		= s_city
		g_state 	= s_state
		g_province 	= s_province
		g_zip 		= s_zip
		g_country 	= s_country
		g_phone		= s_phone 
		g_fax		= s_fax
		g_email		= s_email
		
	End Sub
	
	Public Function getAddressHTML()
		
		retstr = ""
		
		' build the formatted address string	
		ret_str = ret_str & g_name & "<br>" & vbCrLF
		If street1 <> "" Then
			ret_str = ret_str & g_street1 & "<br>" & vbCrLF
		End If
		If street2 <> "" Then
			ret_str = ret_str & g_street2 & "<br>" & vbCrLF
		End If
		If street3 <> "" Then
			ret_str = ret_str & g_street3 & "<br>" & vbCrLF
		End If
		ret_str = ret_str & g_city & ", " & g_state & " " & g_zip & "<br>" & vbCrLF
		ret_str = ret_str & g_country & "<br>" & vbCrLF
		
		ret_str2 = ret_str2 & "<b>Phone:</b> " & g_phone & "<br>" & vbCrLF
		ret_str2 = ret_str2 & "<b>Fax:</b>   " & g_fax & "<br>" & vbCrLF
		ret_str2 = ret_str2 & "<b>Email:</b> " & g_email & "<br>" & vbCrLF
	
		getAddressHTML = "<table><tr><td valign=""top"">" & ret_str & "</td><td>&nbsp;</td><td valign=""top"">" & ret_str2 & "</td></tr></table>"
		
	End Function 
	
	' GET/LET Functions
	Public Property Get Name
		Name = g_name
	End Property
	Public Property Let Name(s)
		g_name = s
	End Property
	
	Public Property Get Street1
		Street1 = g_street1
	End Property
	Public Property Let Street1(s)
		g_street1 = s
	End Property
	
	Public Property Get Street2
		Street2 = g_street2
	End Property
	Public Property Let Street2(s)
		g_street2 = s
	End Property
	
	Public Property Get Street3
		Street3 = g_street3
	End Property
	Public Property Let Street3(s)
		g_street3 = s
	End Property
	
	Public Property Get City
		City = g_city
	End Property
	Public Property Let City(s)
		g_city = s
	End Property
	
	Public Property Get State
		State = g_state
	End Property
	Public Property Let State(s)
		g_state = s
	End Property
	
	Public Property Get Province
		Province = g_province
	End Property
	Public Property Let Province(s)
		g_province = s
	End Property
	
	Public Property Get Zip
		Zip = g_zip
	End Property
	Public Property Let Zip(s)
		g_zip = s
	End Property
	
	Public Property Get Country
		Country = g_country
	End Property
	Public Property Let Country(s)
		g_country = s
	End Property
	
	Public Property Get Phone
		Phone = g_phone
	End Property
	Public Property Let Phone(s)
		g_phone = s
	End Property
	
	Public Property Get Fax
		Fax = g_fax
	End Property
	Public Property Let Fax(s)
		g_fax = s
	End Property
	
	Public Property Get Email
		Email = g_email
	End Property
	Public Property Let Email(s)
		g_email = s
	End Property
End Class

%>