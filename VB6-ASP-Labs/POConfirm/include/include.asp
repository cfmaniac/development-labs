<%

' prevents certain operations from happening in a non production
' 1. does not redirect to https
'
glo_production = true

' processing days to add to promise date calculation
glo_processing_days = 2

' text that gets printed just before the </body> tag
glo_body_first_text = ""
glo_body_last_text = ""

%>


<!--#INCLUDE FILE="classes/PO.asp" -->
<!--#INCLUDE FILE="classes/PO_Address.asp" -->
<!--#INCLUDE FILE="classes/PO_Line.asp" -->
<!--#INCLUDE FILE="classes/POUser.asp" -->
<!--#INCLUDE FILE="classes/MD5.asp" -->
<!--#INCLUDE FILE="conn.asp" -->
<!--#INCLUDE FILE="po_functions.asp" -->
<!--#INCLUDE FILE="po_update.asp" -->

<%

' Login and Authentication Functions

'Replaced IllegalChar Function 8/19/08 removing the loop 
Function IllegalChars(sInput) 

	'Declare variables 
	
	'Set IllegalChars to False 
	IllegalChars=False
	
	'Use Function Instr to check presence of illegal character in our variable
	If Instr(sInput,"select") Then
		IllegalChars=True
	End If
	
	If Instr(sInput,"drop") Then
		IllegalChars=True
	End If
	
	If Instr(sInput,"--") Then
		IllegalChars=True
	End If
	
	If Instr(sInput,"insert") Then
		IllegalChars=True
	End If
	
	If Instr(sInput,"delete") Then
		IllegalChars=True
	End If
	
	If Instr(sInput,"xp_") Then
		IllegalChars=True
	End If
	
	If Instr(sInput,"update") Then
		IllegalChars=True
	End If
	
	If Instr(sInput,"+") Then
		IllegalChars=True
	End If
	
	If Instr(sInput,"declare") Then
		IllegalChars=True
	End If
	
	If Instr(sInput,"varchar") Then
		IllegalChars=True
	End If
	
	If Instr(sInput,"iframe") Then
		IllegalChars=True
	End If
	
	If Instr(sInput,"varchar") Then
		IllegalChars=True
	End If

	If Instr(sInput,"%") Then
		IllegalChars=True
	End If
	
	If Instr(sInput,"(") Then
		IllegalChars=True
	End If
	
	If Instr(sInput,")") Then
		IllegalChars=True
	End If
	
	If Instr(sInput,"/") Then
		IllegalChars=True
	End If
	
	If Instr(sInput,"\") Then
		IllegalChars=True
	End If
	If Instr(sInput,":") Then
		IllegalChars=True
	End If
	
	If Instr(sInput,";") Then
		IllegalChars=True
	End If
	If Instr(sInput,"<") Then
		IllegalChars=True
	End If
	
	If Instr(sInput,">") Then
		IllegalChars=True
	End If
	
	If Instr(sInput,"=") Then
		IllegalChars=True
	End If
	
	If Instr(sInput,"[") Then
		IllegalChars=True
	End If
	
	If Instr(sInput,"]") Then
		IllegalChars=True
	End If
	
	If Instr(sInput,"?") Then
		IllegalChars=True
	End If
	
	If Instr(sInput,"|") Then
		IllegalChars=True
	End If
	
	If Instr(sInput,"'") Then
		IllegalChars=True
	End If
	
	If Instr(sInput,"""") Then
		IllegalChars=True
	End If
	
	If Instr(sInput,"*") Then
		IllegalChars=True
	End If
End function

Function IllegalChars_XX(sInput) 
	
	'Declare variables 
	Dim sBadChars, iCounter 
	
	'Set IllegalChars to False 
	IllegalChars=False
	
	if not isnull(sInput) then
	'Create an array of illegal characters and words 
		sBadChars=array("select", "drop", "--", "insert", "delete", "xp_", "update", "+", "declare", "varchar", "iframe", _
		"#", "%", "(", ")", "/", "\", ":", ";", "<", ">", "=", "[", "]", "?", "`", "|", "*", """") 
	
		'Loop through array sBadChars using our counter & UBound function
		For iCounter = 0 to uBound(sBadChars) 
			'Use Function Instr to check presence of illegal character in our variable
			If Instr(sInput,sBadChars(iCounter))>0 Then
			IllegalChars=True
			End If
		Next 
	end if

End function

Function Scrub_Input(input_line)
	
	if not isnull(input_line) then
	'	Scrub_Input = Replace(input_line,"'","''")
		Scrub_Input = Replace(Replace(Replace(Replace(Replace(input_line,">","%gt;"),")","&#x28;"),"(","&#x29;"),"'","&apos;"),"''","&#x22;")
	else
		Scrub_Input = input_line
	end if
	
end function

Function SafeChar(sInput)
	
	if not isnull(sInput) then
		SafeChar = Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(sInput,"'","''"),"%","[%]"),"[","[[]"),";",""),"--",""),"xp_",""),"/*",""),"*/",""),"<",""),">",""),"""",""),"(",""),"(","")
	else
		SafeChar = sInput
	end if
	
end function


function chkEmail(theAddress)
   ' checks for a vaild email
   ' returns 0 for invalid addresses
   ' returns 1 for valid addresses
   dim atCnt
   chkEmail = 1

   ' chk length
   if len(theAddress) < 5  then 
      ' a@b.c should be the shortest an
      ' address could be
      chkEmail = 0
 
   ' chk format
   ' has at least one "@"
   elseif instr(theAddress,"@") = 0 then
      chkEmail = 0
 
   ' has at least one "."
   elseif instr(theAddress,".") = 0 then
      chkEmail = 0
 
   ' has no more than 3 chars after last "."
   elseif len(theAddress) - instrrev(theAddress,".") > 3 then
      chkEmail = 0
 
   ' has no "_" after the "@"
   elseif instr(theAddress,"_") <> 0 and _
       instrrev(theAddress,"_") > instrrev(theAddress,"@")  then
      chkEmail = 0

   else
      ' has only one "@"
      atCnt = 0
	  
      for ii = 1 to len(theAddress)
         if mid(theAddress,ii,1) = "@" then
            atCnt = atCnt + 1
         end if
      next
 
      if atCnt > 1 then
         chkEmail = 0
      end if

      ' chk each char for validity
      for i = 1 to len(theAddress)
        if  not isnumeric(mid(theAddress,i,1)) and _
		(lcase(mid(theAddress,i,1)) < "a" or _
		lcase(mid(theAddress,i,1)) > "z") and _
		mid(theAddress,i,1) <> "_" and _
		mid(theAddress,i,1) <> "." and _
		mid(theAddress,i,1) <> "@" and _
		mid(theAddress,i,1) <> "-" then
            chkEmail = 0
        end if
      next
  end if
end function

function XRedirect(sURL)
	Response.Buffer = True
	Response.Clear
	Response.Status = "301 Moved"
	Response.AddHeader "Location", sUrl
	Response.End
end function

Function escapeQuote(str)
	escapeQuote = replace(str, "'", "''")
End Function

Function jt_convertDate(str_date)
	syear 	= Mid(str_date, 1,4)
	smonth	= Mid(str_date, 5, 2)
	sday	= Mid(str_date, 7, 2)
	
	jt_convertDate = smonth & "/" & sday & "/" & syear
End Function

function xmlsend(url, docSubmit) 
	Set poster = Server.CreateObject("MSXML2.ServerXMLHTTP") 
	poster.open "POST", url, false 
	poster.setRequestHeader "CONTENT_TYPE", "text/xml" 
	poster.send docSubmit 
	
	Set NewDoc = server.createobject("Microsoft.XMLDOM") 
	newDoc.ValidateOnParse= True 
	newDoc.LoadXML(poster.responseTEXT) 

	Set XMLSend = NewDoc 
	Set poster = Nothing 
end function 

%>