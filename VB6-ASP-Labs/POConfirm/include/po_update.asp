<%	

Function Update_AS400_PO(po_number)

	if trim(po_number) <> "0" and not isnull(po_number) Then

		cmdTemp.CommandText = "SELECT * from poconfirm_po_line " & _
							  "WHERE po_number = '" & trim(po_number) & "'"
							  
		Set rsPOline = Server.CreateObject("ADODB.Recordset")
		rsPOline.Open cmdTemp, , adOpenStatic, adLockOptimistic

		xmlStr = "<?xml version=""1.0""?>" & vbcrlf 
		xmlStr = xmlStr & "<root>" & vbcrlf 
		
		do while not rsPOline.EOF
			prom_date = rsPOline("conf_promise_date")
			prom_date = mid(prom_date,7,4) & mid(prom_date,1,2) & mid(prom_date,4,2)
			
			xmlStr = xmlStr & vbtab & "<update_info>" & vbcrlf 
			xmlStr = xmlStr & vbtab & vbtab & "<po_no>" & trim(po_number) & "</po_no>" & vbcrlf
			xmlStr = xmlStr & vbtab & vbtab & "<eck_part_no>" & trim(rsPOline("our_part_number")) & "</eck_part_no>" & vbcrlf
			xmlStr = xmlStr & vbtab & vbtab & "<line_no>" & trim(rsPOline("line_no")) & "</line_no>" & vbcrlf
			'xmlStr = xmlStr & vbtab & vbtab & "<vnd_part_no>" & trim(rsPOline("vendor_part_number")) & "</vnd_part_no>" & vbcrlf
			xmlStr = xmlStr & vbtab & vbtab & "<conf_qty>" & trim(rsPOline("conf_quantity")) & "</conf_qty>" & vbcrlf
			xmlStr = xmlStr & vbtab & vbtab & "<conf_cost>" & trim(rsPOline("conf_cost")) & "</conf_cost>" & vbcrlf
			xmlStr = xmlStr & vbtab & vbtab & "<conf_date>" & trim(rsPOline("conf_date")) & "</conf_date>" & vbcrlf
			xmlStr = xmlStr & vbtab & vbtab & "<prom_date>" & trim(prom_date) & "</prom_date>" & vbcrlf
			
			if isnull(rsPOline("action")) then
				xmlStr = xmlStr & vbtab & vbtab & "<action> </action>" & vbcrlf
			else
				xmlStr = xmlStr & vbtab & vbtab & "<action>" & trim(rsPOline("action")) & "</action>" & vbcrlf
			end if
			
			xmlStr = xmlStr & vbtab & "</update_info>" & vbcrlf 			
		
			rsPOline.movenext
		loop
		
		xmlStr = xmlStr & "</root>" 

		'Load the XML into an XMLDOM object 
		Set SendDoc = server.createobject("Microsoft.XMLDOM") 
		SendDoc.ValidateOnParse= True 
		SendDoc.LoadXML(xmlStr) 

		'Set the URL of the receiver 
		xURL = "http://10.50.2.8:8181/myrxs/th412" 

		'Call the XML Send function) 
		set NewDoc = xmlSend (xURL, SendDoc)'xmlString) 

	end if	
			
end function

Function sortDate(pDate)
    aDate = CDate(pDate)
    sortDate = Year(aDate)&Right("0"&Month(aDate),2)&Right("0"&Day(aDate),2)
End Function



%>