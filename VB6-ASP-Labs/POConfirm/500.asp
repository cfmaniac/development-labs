<%@ LANGUAGE=vbscript enablesessionstate=true LCID=1033 %>

<% Response.Status = "500 Internal Server Error" %>

<html>
<head>
	<title>500 Internal Server Error</title>
</head>
<body bgcolor="#FFFFFF">
	<center>
	<div align="left" style="width: 315px; border: 1px solid #000066; padding: 15px;">
	<%
	' If a user went directly to this page (without errors)
	If Request.QueryString = " " Then
		' Tell the user that this page is ment for handling missing pages
		%>
		<br><img src="/Assets/Images/erroricon.gif" border="0">
		<P>
			This page is not ment to be viewed on its own.  It is
			here to capture and report errors on web pages on this
			server.
		</P>
		<%
	Else
		' Let the user know that the page is missing
		%>
		
		<br><img src="/Assets/Images/erroricon.gif" border="0" style="float:left; margin-right: 10px;">
		<P>
			The server has encountered an error.
		</P>
		<br>
		<br>
			<%
			' If we were able to send a report to the webmaster
			If SendReport() Then
				' Tell the user we sent a report
				%>
				A message has been sent to the webmaster about the problem.
				<br>
				<br>
				<%
			End If
			%>
			We apologize for any inconvenience that this may have caused.
		
		<%
	End If
	%>
	</div>

	</center>
</body>
</html>

<%
Function SendReport()

	' Store the URL that refered the page
	referer = Request.ServerVariables("HTTP_REFERER")
	if referer = "" then
		referer = "Direct Request"
	end if
	
	user_agent = Request.ServerVariables("HTTP_USER_AGENT")
	remote_addr = Request.ServerVariables("REMOTE_ADDR")
	remote_host = Request.ServerVariables("REMOTE_HOST")
	
	script_name = Request.ServerVariables("SCRIPT_NAME")
	
	' Acquire the URL that was not found
	url	= Request.ServerVariables("URL")
	query_string = Request.ServerVariables("QUERY_STRING")

	' Remove the "404;" prefix
	'url = Replace(url, "404;", "")
	
	
	set objMail = Server.CreateObject("CDONTS.NewMail")
	
	objMail.To = "james.harvey@ecklers.net"
	objMail.From = "ecklers@ecklers.net"
	objMail.Subject = "500 PO Confirm Site"
	objMail.Importance = 2
	
	objMail.Body = "URL:            " & url  & vbCrLF _
				  &"Query String:   " & query_string & vbCrLF _
				  &"Referer:        " & referer & vbCrLF _
				  &"User Agent:     " & user_agent & vbCrLF _
				  &"Script Name:    " & script_name & vbCrLF _
				  &"Address/Host:   " & remote_addr & "/" & remote_host & vbCrLF _
				  &"Server:          T" & vbCrLF 
	
	' gather more information about session and post data
	
	objMail.Body = objMail.Body & _
		"POST DATA:" & vbCrLF 
	For Each PostData in request.Form()
		objMail.Body = objMail.Body & _
		PostData & " = " & request.Form(PostData) & vbCrLF 
	Next
	
	objMail.Send
	
		
	SendReport = true
	'SendReport = false
End Function

%>