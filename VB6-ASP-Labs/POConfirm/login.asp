<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>

<!--#INCLUDE FILE="include/include.asp" -->

<%
'if Request.ServerVariables("HTTPS") = "off"  AND glo_production = true then ' dont use https for testing
'	srvname = Request.ServerVariables("SERVER_NAME")
'	scrname = Request.ServerVariables("SCRIPT_NAME")
'	response.redirect("https://" & srvname & scrname)
'end if


' check for authentication
Set user = New POUser
user.set_cmd(cmdTemp)

' store the message for the user
sucess = ""

' Process a logout
If Request("logout") = "yes" Then
	If user.do_logout() Then
		sucess = "You have been logged out"
	Else
		sucess = "There was a problem logging out"
	End If
End If

' if the user is logged in send them to the default page
' and if the have the default password ask them to change it.
If user.get_logged_in() Then
	XRedirect("default.asp")
End If

' Process a login
username = Request("username")
password = Request("password")

If Request("submit") <> "" Then
	If user.do_login(username, password) Then
		If user.has_default_password() Then
			XRedirect("change_password.asp?defaultpw=yes")
		End if
		sucess = "Logged In"
		XRedirect("default.asp")
	Else
		sucess = "Invalid Login"
	End If
End If
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Purchase Order Confirmation Site Login</title>

<style type="text/css" media="screen">
body{
	text-align: center;
	font-family: Verdana;
	background-color: #CCCCCC;
}
.login_form
{
	display: block;
	position: relative;
	width: 400px;
	border: solid 1px #0099ff;
	background-color: #FFFFFF;
	text-align: center;
}
input.btn {   
	color:#ffffff;   
	font: bold 14px 'trebuchet ms',helvetica,sans-serif;   
	background-color:#004dc0;   
	border: 1px solid;   
	border-color: #0099ff;   
	filter:progid:DXImageTransform.Microsoft.Gradient   
	(GradientType=0,StartColorStr='#ff47b5ff',EndColorStr='#ff004dc0');   
}   
.login_error
{
	border: solid 1px #FF3366;
	color: #000000;
	font-size: 14px;
	background-color: #F99999;
	width: 90%;
	text-align: center;
}

</style>
</head>

<body>
<center>
<form action="login.asp" method="post" name="login_form" class="login_form" id="login_form">
  <div align="center">
    <p style="padding-top: 10px;">
		<strong>Purchase Order Confirmation Site</strong><br />
		<img src="assets/logos/Eck_Auto1.jpg" alt="Eckler Industries Inc." width="175" height="47" />
    </p>
  </div>
  <p style="font-size: 9px; color:#FF0000">Unauthorized access is prohibited. Use of this site is granted to our vendors and employees for the purpose of managing purchase orders only.&nbsp;</p>
  <%If sucess <> "" Then%>
  <center>
  <p class="login_error"><%=sucess%></p>
  </center>
  <%End If%>
  <table border="0" cellspacing="2" cellpadding="2" align="center">
    <tr>
      <td align="right">
        Username:
      </td>
      <td align="left"><input name="username" type="text" id="username" value="<%=Server.HTMLEncode(username)%>"/></td>
    </tr>
    <tr>
      <td align="right">
        Password:
      </td>
      <td align="left"><input name="password" type="password" id="password" /></td>
    </tr>

    <tr>
      <td colspan="2">
          <input name="submit" type="submit" class="btn" value="       Sign In       " />
      </td>
    </tr>

  </table>
  <!--Added Forgot Password Link-->
  <p style="font-size: 12px; padding-bottom: 10px;"><a href="forgot.asp">Forgot your Password?</a></p>

  <p style="font-size: 12px; padding-bottom: 10px;">Send sign in issues to: <a href="mailto:james.harvey@ecklers.net?subject=PO Confirmation Site">james.harvey@ecklers.net</a></p>
</form>
</center>


</body>
</html>
