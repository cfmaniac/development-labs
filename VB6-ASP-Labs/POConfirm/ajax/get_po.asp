<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>

<!--#INCLUDE FILE="../include/include.asp" -->

<%
' check for authentication
Set user = New POUser
user.set_cmd(cmdTemp)

If NOT user.get_logged_in() Then
	XRedirect("../login.asp")
End If 

%>

<%
response.expires=-1  ' do not cach data 

response.contenttype="text/html" ' we are sending html


' get the po number
po_number = 0

If Request.QueryString("o") <> "" AND IsNumeric(Request.QueryString("o")) Then
	po_number = SafeChar(Request.QueryString("o"))
ElseIf Request.Form("o") <> "" AND IsNumeric(Request.Form("o")) Then
	po_number = SafeChar(Request.Form("o"))
End If

' object that stores all the information for the purchase order
Dim glo_po 

'create a new PO Object
Set glo_po = new PO
' set the database connections 
set glo_po.cmdTemp  = cmdTemp
set glo_po.cmdAS400 = cmdAS400
set glo_po.user  = user

' help us display the correct message
action = "view"

' if we have valid information
po_saved 	= false
po_confirmed= false
po_submitted_for_approval = false
valid_po 	= true
passes_filter = true
made_changes = true
send_email_confirmation = false

' Dictionary to hold errors
Dim errorList
Set errorList = CreateObject("Scripting.Dictionary")

If glo_po.poExists(po_number) Then ' Check that we have a valid PO number
	
	' if the confirm po button is clicked
	If NOT IsNull(Request.Form("confirm")) AND Trim(Request.Form("confirm")) <> "" Then
		
		action = "confirm"
		
		' save the po
		po_saved = save_po_comment(po_number)
		If po_saved Then
			po_saved = save_po_items(po_number)
		End If
		
		' validate the po 
		valid_po = validate_po(errorList)
		
		' if the po is valid then check for changes
		If valid_po Then
			If trim(Request.Form("po_type")) = "DRP" Then
				passes_filter = true
				send_drp_po_email = NOT filter_drp_po() ' run checks specific to drp po's
			Else
				passes_filter = filter_po()
			End If
			
			made_changes = NOT filter_po_strict()
		End If
		
		If valid_po AND passes_filter Then
			po_confirmed = confirm_po(po_number, user.get_email_address())
			' mark as having changes
			If made_changes Then
				mark_as_changed(po_number)
				log_price_changes(po_number)
			End If
			' return the errors 
			If po_confirmed Then
				' update dropship po's
				If glo_production Then
					' update changes in the AS400
					'--------------------------------
					If trim(Request.Form("po_type")) = "DRP" Then
						Update_AS400_PO(po_number)
					End If
				End If
				 
				If trim(Request.Form("po_type")) = "DRP" AND send_drp_po_email Then
					send_email_confirmation = true
				ElseIf trim(Request.Form("po_type")) = "DRP" Then 
					' do nothing
				Else ' all others
					send_email_confirmation = true
				End If 
			End If
		Else
			po_confirmed = false
		End If
	ElseIf NOT IsNull(Request.Form("save_po")) AND Trim(Request.Form("save_po")) <> "" Then
		
		action = "save"
		' save the po
		po_saved = save_po_comment(po_number)
		If po_saved Then
			po_saved = save_po_items(po_number)
		End If
		
	ElseIf NOT IsNull(Request.Form("submit_for_approval")) AND Trim(Request.Form("submit_for_approval")) <> "" Then
		action = "submit_for_approval"
		
		' save the po
		po_saved = save_po_comment(po_number)
		If po_saved Then
			po_saved = save_po_items(po_number)
		End If
		
		' validate the po and confirm
		valid_po = validate_po(errorList)
		If valid_po Then
			'po_confirmed = confirm_po(po_number, user.get_email_address()) 
			po_submitted_for_approval = submit_for_approval(po_number, user.get_email_address())
		Else ' submit for approval 
			po_confirmed = false
		End If
	ElseIf NOT IsNull(Request.Form("admin_confirm_changes")) AND Trim(Request.Form("admin_confirm_changes")) <> "" Then
		action = "admin_confirm_changes"
		
		'save modifications to comment
		cs = save_po_comment(po_number)
	
		' mark items as deleted or accepted
		process_actions po_number
		
		' mark the po as approved and confirmed
		po_confirmed = approve_changes(po_number)
		
		If glo_production Then
			' update changes in the AS400
			'--------------------------------
			Update_AS400_PO(po_number)
		End If 
		
		' notify the vendor that the po can be shipped 
		If po_confirmed Then
			send_email_confirmation = true
		End If
			
	End If
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Load the po's data and Start Output
	If glo_po.load_data(po_number) Then
	
		If send_email_confirmation Then
			send_confirmation_email glo_po, get_submited_by_email(po_number)
		End If
		
		If Not valid_po Then	%>
			<div class="error_box">
				Please correct the items that are highlighted in red.
			</div>
			<br />
		<% 
		ElseIf NOT passes_filter Then %>
			<%
				glo_po.show_submit_for_approval_button = true
			%>
			<div class="error_box">
				The PO has been modified and can not be confirmed please submit it for approval using the button below.<br />
				<input name="submit_for_approval" type="submit" class="btn_blu" value=" Submit for Approval " title="Submits the PO to our purchasing department in the event that information needs needs to be changed on the PO." onclick="submit_for_approval(document.po_confirm_form);"/>
			</div>
			<br />
		<% 
		ElseIf po_saved AND NOT po_confirmed AND NOT po_submitted_for_approval Then	%>
			<div class="error_box">
				The PO has been saved. Please note that the PO must still be confirmed.<!-- or submitted for approval at a later date.-->
			</div>
			<br />
		<% 
		End If %>
		
		<%print_po_form glo_po, errorList%>
		
		<%po_item_errors errorList%>
		<!--Number of Errors = <%=errorList.Count%>-->
		<%
	Else
		%>
		<div class="error_box">
			Could Not Load The PO.<br />
		</div>
		<br />
		<%
	End If 
Else
%>
	<div class="error_box">
		Could Not Find The PO.<br />
	</div>
	<br />
<%
End If

%>