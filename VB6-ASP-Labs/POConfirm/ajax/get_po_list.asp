<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>

<!--#INCLUDE FILE="../include/include.asp" -->

<%
' check for authentication
Set user = New POUser
user.set_cmd(cmdTemp)

If NOT user.get_logged_in() Then
	XRedirect("../login.asp")
End If 

%>

<%
response.expires=-1  ' do not cach data 

response.contenttype="text/html" ' we are sending html

Set user = New POUser
user.set_cmd(cmdTemp)

' if the user is not logged in send them access denied 
If NOT user.get_logged_in() Then
	%>You are not logged in. Access Denied.<%
Else
	' get the vendor numebr
	vendor_no = user.get_vendor_no()
	
	If Len(Session("location")) <= 3 AND Session("location") <> "ALL" AND Trim(Session("location")) <> "" Then 
		glo_list_filter_sql = " AND location = '"&SafeChar(Session("location"))&"' "
	End If
	
	If Len(Session("buyer_no")) <= 3 AND Session("buyer_no") <> "ALL" AND Trim(Session("buyer_no")) <> "" Then 
		If Trim(Session("buyer_no")) = "*" Then 
			glo_list_filter_sql = glo_list_filter_sql & " AND buyer_no = '' " ' for po's with out a buyer number
		Else
			glo_list_filter_sql = glo_list_filter_sql & " AND buyer_no = '"&SafeChar(Session("buyer_no"))&"' "
		End If
	End If
	
	If vendor_no = 0 Then ' eckler's employee login 
	%>	
		<table width="750" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="left" valign="middle">Eckler's Purchasing dept. </td>
				<td align="right" valign="middle">
					Filter By Buyer: 
					
					<% ' preselect the saved location
					buyer_no = Session("buyer_no")
					
					%>
					
					<select name="buyer_no" onchange="javascript: set_buyer_no(this.value);">
						<option value="ALL">All</option>
						<%=get_buyer_no_select(buyer_no)%>
					</select>
					
					<!--Location:--> 
					
					<% ' preselect the saved location
					'location = Session("location")
					'if Len(location) <= 3 Then 
					'	Select Case location
					'		Case "FL"
					'			selected_fl 	= " selected=""selected"""
					'		Case "GA"
					'			selected_ga 	= " selected=""selected"""
					'		Case Default
					'			selected_all 	= " selected=""selected"""
					'	End Select					
					'End If
					
					%>
					<!--<select name="destination" onchange="javascript: set_location(this.value);">
						<option value="ALL"<%=selected_all%>>All</option>
						<option value="FL"<%=selected_fl%>>FL</option>
						<option value="GA"<%=selected_ga%>>GA</option>
					</select>-->
				</td>
			</tr>		
		</table>
		
		
		
		
		<table width="750" border="0" cellspacing="0" cellpadding="0" class="po_table">
			<tr>
				<td colspan="3" class="po_table_header">PO Confirm Dashboard</td>
			</tr>
			<tr>
				<td width="33%" align="center"><b>Awaiting Approval</b></td>
				<!--<td width="25%" align="center"><b>Confirmed W/Changes</b></td>-->
				<td width="33%" align="center"><b>Confirmed</b></td>
				<td width="33%" align="center"><b>Accessed</b> (<a href="default.asp?clear=accessed">Clear Completed</a>)</td>
			</tr>
			<tr>
				<td valign="top">
					<%getShortApprovalList%>
				</td>
				<!--<td valign="top">
					<%'getShortConfirmedWChangesList%>
				</td>-->
				<td valign="top">
					<%getShortConfirmedList%>
				</td>
				<td valign="top">
					<%getShortAccessedList%>
				</td>
			</tr>
		</table>		
		
	<%
	Else ' get a list of po's for the vendor
		xmlStr = "<?xml version=""1.0""?>" & vbcrlf 
		xmlStr = xmlStr & "<root>" & vbcrlf 
		xmlStr = xmlStr & vbtab & "<info_request>" & vbcrlf 
		xmlStr = xmlStr & vbtab & vbtab & "<vendor_no>" & trim(vendor_no) & "</vendor_no>" & vbcrlf
		xmlStr = xmlStr & vbtab & vbtab & "<start_date>" & user.get_start_date() & "</start_date>" & vbcrlf
		xmlStr = xmlStr & vbtab & "</info_request>" & vbcrlf 
		xmlStr = xmlStr & "</root>" 

		'Load the XML into an XMLDOM object 
		Set SendDoc = server.createobject("Microsoft.XMLDOM") 
		SendDoc.ValidateOnParse= True 
		SendDoc.LoadXML(xmlStr) 

		'Set the URL of the receiver 
		xURL = "http://10.50.2.8:8181/myrxs/th410" 

		'Call the XML Send function) 
		set NewDoc = xmlSend (xURL, SendDoc)'xmlString) 
		
		'We receive back another XML DOM object! 
		
		'For testing 
		'response.Write "<b>XML DOC posted off:</b><br>" 
		'response.write SendDoc.XML & "<br>" 
		'response.write "<b>Target URL:</b> " & xURL & "<br>" 
		'response.write "<b>XML DOC Received back: </b><br>" 
		'response.write (NewDoc.Xml) 

		Set nodeList = NewDoc.selectnodes("//po_list/po_info") 

		'for testing
		'if not (nodeList is nothing) then
		'	for each node in nodeList 
		'		thepo = node.selectSingleNode("po_number").text
		'		response.write thepo	
		'	next 
		'end if	

		if not (nodeList is nothing) then	
		%>
			<table width="500" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td class="tr_finished" height="15" width="15">&nbsp;</td>
					<td>&nbsp;PO has been confirmed</td>
					<td class="tr_waiting" height="15" width="15">&nbsp;</td>
					<td>&nbsp;PO is waiting for approval</td>
					<td class="tr_past_due" height="15" width="15">&nbsp;</td>
					<td>&nbsp;Items are past due</td>
				</tr>
			</table>
			<br />
			<table width="500" border="0" cellspacing="0" cellpadding="0" class="po_table">
				<tr>
					<td class="po_table_header">Open Purchase Orders</td>
				</tr>
				<tr>
					<td class="po_table_body">
						<table width="500" border="0" cellspacing="0" cellpadding="2" class="po_table">
							<tr>
								<td class="po_line_item_col_header"><b>PO#</b></td>
								<td class="po_line_item_col_header"><b>Type</b></td>
								<td class="po_line_item_col_header"><b>Date Created</b></td>
								<td class="po_line_item_col_header"><b>Expected Delevery</b></td>
								<td class="po_line_item_col_header" align="center"><b>Viewed</b></td>
								<td class="po_line_item_col_header" align="center"><b>Confirmed</b></td>
								<td class="po_line_item_col_header2" align="center"><b>Pending Approval</b></td>
							</tr>
		<%
		
			i = 0
		
			for each node in nodeList 
				thepo = node.selectSingleNode("po_number").text
				thepo_type = node.selectSingleNode("po_type").text
				thepo_date = node.selectSingleNode("po_date").text
				thepo_prom_date = node.selectSingleNode("promise_date").text
				thepo_ref = node.selectSingleNode("ref_text").text
				
				' make sure the WEB VIEW flag is not 'N'
				if Mid(thepo_ref, 6, 1) <> "N" then 
					
					' see if it has been confirmed
					cmdTemp.CommandText = "SELECT vendor_accessed, confirmed, needs_approval, approved " & _
										  "FROM poconfirm_po_header " & _
										  "WHERE po_number = '" & thepo & "'"
										  
					Set rsPOConfHeader = Server.CreateObject("ADODB.Recordset")
					rsPOConfHeader.Open cmdTemp, , adOpenDynamic, adLockOptimistic
					
					If NOT rsPOConfHeader.EOF Then
						confirmed	= rsPOConfHeader("confirmed")
						If confirmed = "Y" Then
							confirmed = "Yes"
						Else
							confirmed = "&nbsp;"
						End If
						
						vendor_accessed		= rsPOConfHeader("vendor_accessed")
						If vendor_accessed = "Y" Then
							vendor_accessed = "Yes"
						Else
							vendor_accessed = "&nbsp;"
						End If
						
						needs_approval		= rsPOConfHeader("needs_approval")
						If needs_approval = "Y" Then
							needs_approval = "Yes"
						Else
							needs_approval = "&nbsp;"
						End If
						
						approved		= rsPOConfHeader("approved")
						If approved = "Y" Then
							approved = "Yes"
						Else
							approved = "&nbsp;"
						End If
					Else
						confirmed		= "&nbsp;"
						vendor_accessed	= "&nbsp;"
						needs_approval 	= "&nbsp;"
						approved 		= "&nbsp;"
					End If
					rsPOConfHeader.Close
					
					' check to see if the PO is past due
					TheMMDDYY = DatePart("m",date) & "/" & DatePart("d",date) & "/" & DatePart("yyyy",date)
					past_due = false
					If cdate(jt_convertDate(thepo_prom_date)) < cdate(TheMMDDYY) Then
						past_due = true
					End If
					
					i = i + 1
					If i MOD 2 = 0 Then
						tr_style = "tr2"
					Else
						tr_style = "tr1"
					End If
					
					If confirmed = "Yes" Then
						tr_style = "tr_finished"
					ElseIf needs_approval = "Yes" Then
						tr_style = "tr_waiting"
					ElseIf past_due Then
						tr_style = "tr_past_due"
					Else
						tr_style = "tr1"
					End If
						
					%>
					<tr class="<%=tr_style%>">
						<td class="po_line_item_border"><a href="confirm_po.asp?o=<%= thepo %>"><%= thepo %></a></td>
						<td class="po_line_item_border"><%= exp_po_type(thepo_type) %></td>
						<td class="po_line_item_border"><%= jt_convertDate(thepo_date) %></td>	
						<td class="po_line_item_border"><%= jt_convertDate(thepo_prom_date) %></td>
						<td class="po_line_item_border" align="center"><%=vendor_accessed%></td>
						<td class="po_line_item_border" align="center"><%=confirmed%></td>
						<td class="po_line_item_border" align="center"><%=needs_approval%></td>
					</tr>
					<%
				End If
				
			next
						
			%>
						</table></td>
				</tr>		
			</table>
			<%
		Else
		%>
			No Purchase Orders where found.
		<%
		End If
	End If
End If
%>

