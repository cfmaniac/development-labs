<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<%
'if Request.ServerVariables("HTTPS") = "off" then 
'	srvname = Request.ServerVariables("SERVER_NAME") 
'	scrname = Request.ServerVariables("SCRIPT_NAME") 
'	response.redirect("https://" & srvname & scrname) 
'end if 
%>


<!--#INCLUDE FILE="include/include.asp" -->
<%
' check for authentication
Set user = New POUser
user.set_cmd(cmdTemp)

If NOT user.get_logged_in() Then
	XRedirect("login.asp")
End If

action 		= ""
error_str 	= ""
message_str = ""
password_updated = false

'If Trim(Request.QueryString("defaultpw")) = "yes" Then ' we are about to change our default password
'	error_str = "Please change your password to somthing other than 'password'" 
'Else

If Request.Form("changepw") <> "" AND NOT IsNull(Request.Form("changepw")) Then ' we are submiting the new password.

	old_password 	= Left(SafeChar(Request.Form("old_password")), 20)
	password1 		= Left(SafeChar(Request.Form("password1")), 20)
	password2 		= Left(SafeChar(Request.Form("password2")), 20)

	' make sure that the password meets our pw policy
	If user.pw_policy_check(password1) Then
		If password1 = password2 Then
			If user.change_password(old_password, password1) Then
				message_str = "Your password has been updated."
				password_updated = true
			Else
				error_str = "your old password does not match."
			End If
		Else
			error_str = "Confirm password does not match."
		End If
	Else ' show them an error
		error_str = "Password does not meet our policy."
	End If 
	'response.Write("asdfsdfA")
End If 





%>

<!--#INCLUDE FILE="header1.asp" -->

	<title>Eckler Industries Inc. PO Confirmation</title>
	<meta name="ROBOTS" content="NOINDEX,NOFOLLOW"> 
	<link href="stylesheet/style.css" rel="stylesheet" type="text/css" />
	<link href="stylesheet/datepicker.css" rel="stylesheet" type="text/css" />
	<script src="javascript/scripts.js" type="text/javascript"></script>


<!--#INCLUDE FILE="header2.asp" -->
<!--START CONTENT-->
	
	<%
	If Trim(Request.QueryString("defaultpw")) = "yes" Then
	%>
		<h2>You have successfully logged in.</h2>
		<div class="info_box">Please take a moment to change your password to something other than the default password. We appreciate your time making our site more secure.</div><br />
	<%
	End If
		
	If error_str <> "" Then
	%>
		<div class="error_box"><%=error_str%></div><br />
	<%
	End If 
	
	If message_str <> "" Then
	%>
		<div class="error_box"><%=message_str%></div><br />
	<%
	End If 
	
	If password_updated Then
	%>
		<div class="info_box">you will automaticly be redirected to the dashboard.</div>
		<meta http-equiv="refresh" content="2;URL=/default.asp" />
	<%
	Else
		
		%>
		<div class="info_box">Please Note: All fields are required. Your password can not contain the word 'password', can not contain spaces and must be at least 8 characters.</div>
	
		<form action="change_password.asp" method="post" name="pw_change_form">
			<table width="300" cellspacing="0" cellpadding="3">
				<tr>
					<td align="right" style="border-bottom: #0033FF solid 2px;"><b>Old Password:&nbsp;</b></td>
					<td style="border-bottom: #0033FF solid 2px;"><input name="old_password" type="password" size="20" /></td>
				</tr>
				<tr>
					<td align="right"><b>New Password:&nbsp;</b></td>
					<td><input name="password1" type="password" size="20" /></td>
				</tr>
				<tr>
					<td align="right"><b>Confirm New Password:&nbsp;</b></td>
					<td><input name="password2" type="password" size="20" /></td>
				</tr>
				<tr>
					<td align="right">&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2" align="center"><input name="changepw" type="submit" class="btn_blu" value=" Change Password " title="Submit your password request change."/></td>
				</tr>
			</table>
		</form>
	<%
	End If
	%>
<!--END CONTENT-->
<!--#INCLUDE FILE="footer.asp" -->