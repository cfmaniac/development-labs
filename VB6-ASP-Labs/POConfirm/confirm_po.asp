<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>

<!--#INCLUDE FILE="include/include.asp" -->
<%
' check for authentication
Set user = New POUser
user.set_cmd(cmdTemp)

If NOT user.get_logged_in() Then
	XRedirect("login.asp")
End If

' flag problems 
valid_data = true

' get the po number
po_number = trim(Request("o"))

If Not IsNumeric(po_number) Then ' if it is not a valid number make it harmless
	po_number = 0
	valid_data = false
End If


' default to false
po_confirmed = false

' Dictionary to hold errors
Dim errorList
Set errorList = CreateObject("Scripting.Dictionary")

%>

<!--#INCLUDE FILE="header1.asp" -->

	<title>Eckler Industries Inc. PO Confirmation</title>
	<meta name="ROBOTS" content="NOINDEX,NOFOLLOW"> 
	<link href="stylesheet/style.css" rel="stylesheet" type="text/css" />
	<link href="stylesheet/datepicker.css" rel="stylesheet" type="text/css" />
	<link href="stylesheet/print.css" rel="stylesheet" type="text/css" media="print"/>
	
	<script src="javascript/datepicker.js" type="text/javascript"></script>
	<script src="javascript/scripts.js" type="text/javascript"></script>


<!--#INCLUDE FILE="header2.asp" -->
<!--START CONTENT-->

<%
' if the po is confirmed
If po_confirmed Then
%>
	The Purchase Order has been confirmed. 
	<meta http-equiv="refresh" content="3;confirm_po.asp?o=<%=po_number%>" />
<%
Else ' show the po
	data = Request.ServerVariables("QUERY_STRING")
	If data = "" Then
		data = Request.Form()
	End If
%>
	<div id="po_main"></div>
	<script type="text/javascript">
	<!-- // set the model and the saved year after the form has been created
		callAHAHPOST('ajax/get_po.asp?', '<%=data%>', 'po_main', 'We are retrieving the Purchase Order. Please allow up to 60 seconds for the page to load.<br><img src="assets/icon/ajax-loader-circle-thickbox.gif" ><br>Loading...');
	 // -->
	</script>
	
<%
End If
%>

<!--END CONTENT-->
<!--#INCLUDE FILE="footer.asp" -->