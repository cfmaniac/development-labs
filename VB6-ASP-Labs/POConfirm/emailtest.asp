<%@ LANGUAGE=vbscript enablesessionstate=true LCID=1033 %>
<!-- 
    METADATA 
    TYPE="typelib" 
    UUID="CD000000-8B95-11D1-82DB-00C04FB1625D"  
    NAME="CDO for Windows 2000 Library" 
-->  
<%  
    'Set cdoConfig = CreateObject("CDO.Configuration")  
 
'    With cdoConfig.Fields  	
'        .Item(cdoSendUsingMethod) = cdoSendUsingPort  
'        .Item(cdoSMTPServer) = "10.50.2.2"  
'        .Update  
'    End With 
 
'    Set cdoMessage = CreateObject("CDO.Message")  
 
'    With cdoMessage 
'        Set .Configuration = cdoConfig 
'        .From = "po@ecklers.net"
'        .To = "james.harvey@ecklers.net"
'        .Subject = "Sample CDO Message" 
'        .TextBody = "This is a test for CDO.message" 
'        .Send 
'    End With 
 
'    Set cdoMessage = Nothing  
'    Set cdoConfig = Nothing  
%>

<!-- 
    METADATA 
    TYPE="typelib" 
    UUID="CD000000-8B95-11D1-82DB-00C04FB1625D"  
    NAME="CDO for Windows 2000 Library" 
-->  
<%	
	    Set cdoConfig = CreateObject("CDO.Configuration")  
 
		With cdoConfig.Fields  	
			.Item(cdoSendUsingMethod) = cdoSendUsingPort  
			.Item(cdoSMTPServer) = "10.50.2.2"  
			.Update  
		End With 

	    Set cdoMessage = CreateObject("CDO.Message")  
 
		With cdoMessage 
			Set .Configuration = cdoConfig 
			'.From = "po@ecklers.net"
            '.To = "james.harvey@ecklers.net"
			.Subject = "Purchase Order # 12345678 - Eckler Industries Inc."
			'.Importance = 2
			
			.HTMLBody  ="<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">" _ 
					   &"<html>" & vbCrLF _
					   &"<head> " & vbCrLF _
					   & get_email_style_sheet() & vbCrLF _
					   &"</head> " & vbCrLF _
					   &"<body>" & vbCrLF _
					   &"	<center> " & vbCrLF _
					   &"	<table width=""750""><tr><td>" & vbCrLF _
					   & "po_header_email(po)" & vbCrLF _
					   & "po_item_details_email(po)" & vbCrLF _
					   &"	</td></tr></table>" & vbCrLF _
					   &"	</center>" & vbCrLF _
					   &"</body>" & vbCrLF _
					   &"</html>" & vbCrLF 
	
			.Send 
		End With 
		
	    Set cdoMessage = Nothing  
	    Set cdoConfig = Nothing  
		
	
Function get_email_style_sheet()
ret_str = _
"<style type=""text/css"">" & vbCrLF & _
"<!--   " & vbCrLF & _
" body { font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; font-size: 10px; background:#FFFFFF; margin-top: 0;} " & vbCrLF & _
" a:link, a:active, a:visited { color: #0000ff;} " & vbCrLF & _
" table { font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; font-size: 10px;} " & vbCrLF & _
" .po_table{ margin-bottom: 3px;} " & vbCrLF & _
" .po_table_header{ background-color: #0066FF; font-size:14px; color: #FFFFFF; font-weight: bold; padding:3px; text-align: center;} " & vbCrLF & _
" .po_table_body{ border: solid 1px #0066FF;} " & vbCrLF & _
" .po_line_item_col_header{ border-bottom: 1px solid #0099FF; border-right: 1px solid #0099FF;} " & vbCrLF & _
" .po_line_item_col_header2{ border-bottom: 1px solid #0099FF;} " & vbCrLF & _
" .po_line_item_border{ border-bottom: 1px solid #CCCCCC;} " & vbCrLF & _
" .highlight_input_error{ background-color: #E04E38 !important;} " & vbCrLF & _
" .error_box{ border: solid 1px #FF3366; color: #000000; font-size: 14px; background-color: #F99999; width: 750px; text-align: center;} " & vbCrLF & _
" .info_box{ border: solid 1px #0099FF; color: #000000; font-size: 14px; background-color: #D9EAFB; width: 750px; text-align: center;} " & vbCrLF & _
" .float_error_div{ width: 150px; height: 15px; visibility: hidden;	position: absolute;	background-color:#FFFFFF; border: 1px solid #FF0000;} " & vbCrLF & _
" .tr1{ background-color: #ffffff;} " & vbCrLF & _
" .tr2{background-color: #D6D6D6;} " & vbCrLF & _
" .tr_finished{ background-color: #3EFF47;} " & vbCrLF & _
" .tr_waiting{ background-color: #FFFF00;} " & vbCrLF & _
" .line_diff{ background-color: #66FFFF;} " & vbCrLF & _
" .gray_border_right{ border-right: solid 1px #CCCCCC;} " & vbCrLF & _
"-->   " & vbCrLF & _
"</style>"
	
	get_email_style_sheet = ret_str
End Function

%>