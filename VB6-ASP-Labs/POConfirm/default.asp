<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!--#INCLUDE FILE="include/include.asp" -->
<%
' check for authentication
Set user = New POUser
user.set_cmd(cmdTemp)

If NOT user.get_logged_in() Then
	XRedirect("login.asp")
End If 

' grab the location and save it in the session

if Len(Request("loc")) <= 3 AND Len(Request("loc")) >= 1 Then
	loc = SafeChar(Left(Request("loc"), 3))
	Session("location") = loc
End If

if Len(Request("buyer_no")) <= 3 AND Len(Request("buyer_no")) >= 1 Then
	buyer_no = SafeChar(Left(Request("buyer_no"), 3))
	Session("buyer_no") = buyer_no
End If

If request("clear") = "accessed" Then
	clear_accessed_pos
End If

%>

<!--#INCLUDE FILE="header1.asp" -->

	<title>Eckler Industries Inc. PO Confirmation</title>
	<meta name="ROBOTS" content="NOINDEX,NOFOLLOW"> 
	<link href="stylesheet/style.css" rel="stylesheet" type="text/css" />
	<link href="stylesheet/datepicker.css" rel="stylesheet" type="text/css" />

	<script src="javascript/datepicker.js" type="text/javascript"></script>
	<script src="javascript/scripts.js" type="text/javascript"></script>

<!--#INCLUDE FILE="header2.asp" -->

<!--START CONTENT-->

	<div id="po_main"></div>
	<script type="text/javascript">
	<!-- // set the model and the saved year after the form has been created
		callAHAH('./ajax/get_po_list.asp?', 'po_main', 'We are retrieving open Purchase Orders. Please allow up to 60 seconds for the page to load.<br><img src="assets/icon/ajax-loader-circle-thickbox.gif" ><br>Loading...');
	 // -->
	</script>

<!--END CONTENT-->

<!--#INCLUDE FILE="footer.asp" -->