/*JavaScript function I have been asked
to write for various Online Coding Tests*/

function isPrime(value) {
    for(var i = 2; i < value; i++) {
        if(value % i === 0) {
            return false;
        }
    }
    return value > 1;
}