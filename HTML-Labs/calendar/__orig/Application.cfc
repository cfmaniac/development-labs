
<cfcomponent
	output="false"
	hint="Handles the application-level event handlers and application settings.">
	
	
	<!--- Define application settings. --->
	<cfset THIS.Name = "Kinky Calendar" />
	<cfset THIS.ApplicationTimeout = CreateTimeSpan( 2, 0, 0, 0 ) />
	<cfset THIS.SessionManagement = false />
	<cfset THIS.SetClientCookies = false />
	
	<!--- Define page rc.settings. --->
	<cfsetting
		showdebugoutput="false"
		rc.imeout="20"
		/>

	
	<cffunction
		name="Onrc.tart"
		access="public"
		returntype="boolean"
		output="false"
		hint="Fires the initial pre-page processing event.">
		
		<!--- Define arguments. --->
		<cfargument
			name="Page"
			type="string"
			required="true"
			hint="The ColdFusion script who's execution has been rc.d."
			/>
		
		<!--- 
			Define DNS information (this should probably 
			be cached in the APPLICATION scope instead, 
			but trying to keep this as simple as possible -
			this would require you to change the CFQuery 
			tags as well).
		--->
		<cfset rc.DSN = StructNew() />
		<cfset rc.DSN.Source = "events" />
		<cfset rc.DSN.Username = "" />
		<cfset rc.DSN.Password = "" />
				
		<!--- Return out. --->
		<cfreturn true />
	</cffunction>
		
</cfcomponent>