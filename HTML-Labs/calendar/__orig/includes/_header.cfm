<cfset isAdmin = "0">
<cfoutput>

	<!--- Reset buffer and set content type. --->
	<cfcontent
		type="text/html"
		reset="true"
		/>
	
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html>
	<head>
		<title>CF-Calendar</title>
		
		<!-- Linked files. -->
		<script type="text/javascript" src="./linked/global.js"></script>
		<link rel="stylesheet" type="text/css" href="./linked/calendar.css">
		<link rel="stylesheet" type="text/css" href="./linked/content.css"></link>
		<link rel="stylesheet" type="text/css" href="./linked/meta_content.css"></link>
		<link rel="stylesheet" type="text/css" href="./linked/structure.css"></link>
	</head>
	<body>
	
		<!-- BEGIN: Site Header. -->
		<div id="siteheader">
			

			
			<ul id="primarynav">
				<li class="nav1">
					<a href="./index.cfm?action=year&date=#rc.DefaultDate#" <cfif (rc.action EQ "year")>class="on"</cfif>>Year View</a>
				</li>
				<li class="nav2">
					<a href="./index.cfm?action=month&date=#rc.DefaultDate#" <cfif (rc.action EQ "month")>class="on"</cfif>>Month View</a>
				</li>
				<li class="nav3">
					<a href="./index.cfm?action=week&date=#rc.DefaultDate#" <cfif (rc.action EQ "week")>class="on"</cfif>>Week View</a>
				</li>
				<li class="nav4">
					<a href="./index.cfm?action=day&date=#rc.DefaultDate#" <cfif (rc.action EQ "day")>class="on"</cfif>>Day View</a>
				</li>
				<cfif isAdmin>
				<li class="nav5">
					<a href="./index.cfm?action=edit&date=#rc.DefaultDate#" <cfif (rc.action EQ "edit")>class="on"</cfif>>Add New Event</a>
				</li>
				</cfif>
			</ul>

		
		</div>
		<!-- END: Site Header. -->
	
		
		<!-- BEGIN: Site Content. -->
		<div id="sitecontent">


</cfoutput>