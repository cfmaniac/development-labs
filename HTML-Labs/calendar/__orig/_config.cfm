
<!--- Kill extra output. --->
<cfsilent>

	<!--- Set up repeat types. --->
	<cfset rc.RepeatTypes = QueryNew( "id, name" );
	
	<!--- Set repeat types. --->
	<cfset QueryAddRow( rc.RepeatTypes );
	<cfset rc.RepeatTypes[ "id" ][ rc.RepeatTypes.RecordCount ] = 1 />
	<cfset rc.RepeatTypes[ "name" ][ rc.RepeatTypes.RecordCount ] = "Daily" />
	
	<cfset QueryAddRow( rc.RepeatTypes );
	<cfset rc.RepeatTypes[ "id" ][ rc.RepeatTypes.RecordCount ] = 2 />
	<cfset rc.RepeatTypes[ "name" ][ rc.RepeatTypes.RecordCount ] = "Weekly" />
	
	<cfset QueryAddRow( rc.RepeatTypes );
	<cfset rc.RepeatTypes[ "id" ][ rc.RepeatTypes.RecordCount ] = 3 />
	<cfset rc.RepeatTypes[ "name" ][ rc.RepeatTypes.RecordCount ] = "Bi-Weekly" />
		
	<cfset QueryAddRow( rc.RepeatTypes );
	<cfset rc.RepeatTypes[ "id" ][ rc.RepeatTypes.RecordCount ] = 4 />
	<cfset rc.RepeatTypes[ "name" ][ rc.RepeatTypes.RecordCount ] = "Monthly" />
	
	<cfset QueryAddRow( rc.RepeatTypes );
	<cfset rc.RepeatTypes[ "id" ][ rc.RepeatTypes.RecordCount ] = 5 />
	<cfset rc.RepeatTypes[ "name" ][ rc.RepeatTypes.RecordCount ] = "Yearly" />
	
	<cfset QueryAddRow( rc.RepeatTypes );
	<cfset rc.RepeatTypes[ "id" ][ rc.RepeatTypes.RecordCount ] = 6 />
	<cfset rc.RepeatTypes[ "name" ][ rc.RepeatTypes.RecordCount ] = "Mon - Fri" />
	
	<cfset QueryAddRow( rc.RepeatTypes );
	<cfset rc.RepeatTypes[ "id" ][ rc.RepeatTypes.RecordCount ] = 7 />
	<cfset rc.RepeatTypes[ "name" ][ rc.RepeatTypes.RecordCount ] = "Sat - Sun" />
	
	
	<!--- Set up event colors. --->
	<cfset arrColors = ArrayNew( 1 );
	ArrayAppend( arrColors, "000033" );
	ArrayAppend( arrColors, "000066" );
	ArrayAppend( arrColors, "000099" );
	ArrayAppend( arrColors, "0000CC" );
	ArrayAppend( arrColors, "0000FF" );
	ArrayAppend( arrColors, "003300" );
	ArrayAppend( arrColors, "003333" );
	ArrayAppend( arrColors, "003366" );
	ArrayAppend( arrColors, "003399" );
	ArrayAppend( arrColors, "0033CC" );
	ArrayAppend( arrColors, "0033FF" );
	ArrayAppend( arrColors, "006600" );
	ArrayAppend( arrColors, "006633" );
	ArrayAppend( arrColors, "006666" );
	ArrayAppend( arrColors, "006699" );
	ArrayAppend( arrColors, "0066CC" );
	ArrayAppend( arrColors, "0066FF" );
	ArrayAppend( arrColors, "009900" );
	ArrayAppend( arrColors, "009933" );
	ArrayAppend( arrColors, "009966" );
	ArrayAppend( arrColors, "009999" );
	ArrayAppend( arrColors, "0099CC" );
	ArrayAppend( arrColors, "0099FF" );
	ArrayAppend( arrColors, "00CC00" );
	ArrayAppend( arrColors, "00CC33" );
	ArrayAppend( arrColors, "00CC66" );
	ArrayAppend( arrColors, "00CC99" );
	ArrayAppend( arrColors, "00CCCC" );
	ArrayAppend( arrColors, "00CCFF" );
	ArrayAppend( arrColors, "00FF00" );
	ArrayAppend( arrColors, "00FF33" );
	ArrayAppend( arrColors, "00FF66" );
	ArrayAppend( arrColors, "00FF99" );
	ArrayAppend( arrColors, "00FFCC" );
	ArrayAppend( arrColors, "00FFFF" );
	ArrayAppend( arrColors, "330000" );
	ArrayAppend( arrColors, "330033" );
	ArrayAppend( arrColors, "330066" );
	ArrayAppend( arrColors, "330099" );
	ArrayAppend( arrColors, "3300CC" );
	ArrayAppend( arrColors, "3300FF" );
	ArrayAppend( arrColors, "333300" );
	ArrayAppend( arrColors, "333333" );
	ArrayAppend( arrColors, "333366" );
	ArrayAppend( arrColors, "333399" );
	ArrayAppend( arrColors, "3333CC" );
	ArrayAppend( arrColors, "3333FF" );
	ArrayAppend( arrColors, "336600" );
	ArrayAppend( arrColors, "336633" );
	ArrayAppend( arrColors, "336666" );
	ArrayAppend( arrColors, "336699" );
	ArrayAppend( arrColors, "3366CC" );
	ArrayAppend( arrColors, "3366FF" );
	ArrayAppend( arrColors, "339900" );
	ArrayAppend( arrColors, "339933" );
	ArrayAppend( arrColors, "339966" );
	ArrayAppend( arrColors, "339999" );
	ArrayAppend( arrColors, "3399CC" );
	ArrayAppend( arrColors, "3399FF" );
	ArrayAppend( arrColors, "33CC00" );
	ArrayAppend( arrColors, "33CC33" );
	ArrayAppend( arrColors, "33CC66" );
	ArrayAppend( arrColors, "33CC99" );
	ArrayAppend( arrColors, "33CCCC" );
	ArrayAppend( arrColors, "33CCFF" );
	ArrayAppend( arrColors, "33FF00" );
	ArrayAppend( arrColors, "33FF33" );
	ArrayAppend( arrColors, "33FF66" );
	ArrayAppend( arrColors, "33FF99" );
	ArrayAppend( arrColors, "33FFCC" );
	ArrayAppend( arrColors, "33FFFF" );
	ArrayAppend( arrColors, "660000" );
	ArrayAppend( arrColors, "660033" );
	ArrayAppend( arrColors, "660066" );
	ArrayAppend( arrColors, "660099" );
	ArrayAppend( arrColors, "6600CC" );
	ArrayAppend( arrColors, "6600FF" );
	ArrayAppend( arrColors, "663300" );
	ArrayAppend( arrColors, "663333" );
	ArrayAppend( arrColors, "663366" );
	ArrayAppend( arrColors, "663399" );
	ArrayAppend( arrColors, "6633CC" );
	ArrayAppend( arrColors, "6633FF" );
	ArrayAppend( arrColors, "666600" );
	ArrayAppend( arrColors, "666633" );
	ArrayAppend( arrColors, "666666" );
	ArrayAppend( arrColors, "666699" );
	ArrayAppend( arrColors, "6666CC" );
	ArrayAppend( arrColors, "6666FF" );
	ArrayAppend( arrColors, "669900" );
	ArrayAppend( arrColors, "669933" );
	ArrayAppend( arrColors, "669966" );
	ArrayAppend( arrColors, "669999" );
	ArrayAppend( arrColors, "6699CC" );
	ArrayAppend( arrColors, "6699FF" );
	ArrayAppend( arrColors, "66CC00" );
	ArrayAppend( arrColors, "66CC33" );
	ArrayAppend( arrColors, "66CC66" );
	ArrayAppend( arrColors, "66CC99" );
	ArrayAppend( arrColors, "66CCCC" );
	ArrayAppend( arrColors, "66CCFF" );
	ArrayAppend( arrColors, "66FF00" );
	ArrayAppend( arrColors, "66FF33" );
	ArrayAppend( arrColors, "66FF66" );
	ArrayAppend( arrColors, "66FF99" );
	ArrayAppend( arrColors, "66FFCC" );
	ArrayAppend( arrColors, "66FFFF" );
	ArrayAppend( arrColors, "990000" );
	ArrayAppend( arrColors, "990033" );
	ArrayAppend( arrColors, "990066" );
	ArrayAppend( arrColors, "990099" );
	ArrayAppend( arrColors, "9900CC" );
	ArrayAppend( arrColors, "9900FF" );
	ArrayAppend( arrColors, "993300" );
	ArrayAppend( arrColors, "993333" );
	ArrayAppend( arrColors, "993366" );
	ArrayAppend( arrColors, "993399" );
	ArrayAppend( arrColors, "9933CC" );
	ArrayAppend( arrColors, "9933FF" );
	ArrayAppend( arrColors, "996600" );
	ArrayAppend( arrColors, "996633" );
	ArrayAppend( arrColors, "996666" );
	ArrayAppend( arrColors, "996699" );
	ArrayAppend( arrColors, "9966CC" );
	ArrayAppend( arrColors, "9966FF" );
	ArrayAppend( arrColors, "999900" );
	ArrayAppend( arrColors, "999933" );
	ArrayAppend( arrColors, "999966" );
	ArrayAppend( arrColors, "999999" );
	ArrayAppend( arrColors, "9999CC" );
	ArrayAppend( arrColors, "9999FF" );
	ArrayAppend( arrColors, "99CC00" );
	ArrayAppend( arrColors, "99CC33" );
	ArrayAppend( arrColors, "99CC66" );
	ArrayAppend( arrColors, "99CC99" );
	ArrayAppend( arrColors, "99CCCC" );
	ArrayAppend( arrColors, "99CCFF" );
	ArrayAppend( arrColors, "99FF00" );
	ArrayAppend( arrColors, "99FF33" );
	ArrayAppend( arrColors, "99FF66" );
	ArrayAppend( arrColors, "99FF99" );
	ArrayAppend( arrColors, "99FFCC" );
	ArrayAppend( arrColors, "99FFFF" );
	ArrayAppend( arrColors, "CC0000" );
	ArrayAppend( arrColors, "CC0033" );
	ArrayAppend( arrColors, "CC0066" );
	ArrayAppend( arrColors, "CC0099" );
	ArrayAppend( arrColors, "CC00CC" );
	ArrayAppend( arrColors, "CC00FF" );
	ArrayAppend( arrColors, "CC3300" );
	ArrayAppend( arrColors, "CC3333" );
	ArrayAppend( arrColors, "CC3366" );
	ArrayAppend( arrColors, "CC3399" );
	ArrayAppend( arrColors, "CC33CC" );
	ArrayAppend( arrColors, "CC33FF" );
	ArrayAppend( arrColors, "CC6600" );
	ArrayAppend( arrColors, "CC6633" );
	ArrayAppend( arrColors, "CC6666" );
	ArrayAppend( arrColors, "CC6699" );
	ArrayAppend( arrColors, "CC66CC" );
	ArrayAppend( arrColors, "CC66FF" );
	ArrayAppend( arrColors, "CC9900" );
	ArrayAppend( arrColors, "CC9933" );
	ArrayAppend( arrColors, "CC9966" );
	ArrayAppend( arrColors, "CC9999" );
	ArrayAppend( arrColors, "CC99CC" );
	ArrayAppend( arrColors, "CC99FF" );
	ArrayAppend( arrColors, "CCCC00" );
	ArrayAppend( arrColors, "CCCC33" );
	ArrayAppend( arrColors, "CCCC66" );
	ArrayAppend( arrColors, "CCCC99" );
	ArrayAppend( arrColors, "CCCCCC" );
	ArrayAppend( arrColors, "CCCCFF" );
	ArrayAppend( arrColors, "CCFF00" );
	ArrayAppend( arrColors, "CCFF33" );
	ArrayAppend( arrColors, "CCFF66" );
	ArrayAppend( arrColors, "CCFF99" );
	ArrayAppend( arrColors, "CCFFCC" );
	ArrayAppend( arrColors, "CCFFFF" );
	ArrayAppend( arrColors, "FF0000" );
	ArrayAppend( arrColors, "FF0033" );
	ArrayAppend( arrColors, "FF0066" );
	ArrayAppend( arrColors, "FF0099" );
	ArrayAppend( arrColors, "FF00CC" );
	ArrayAppend( arrColors, "FF00FF" );
	ArrayAppend( arrColors, "FF3300" );
	ArrayAppend( arrColors, "FF3333" );
	ArrayAppend( arrColors, "FF3366" );
	ArrayAppend( arrColors, "FF3399" );
	ArrayAppend( arrColors, "FF33CC" );
	ArrayAppend( arrColors, "FF33FF" );
	ArrayAppend( arrColors, "FF6600" );
	ArrayAppend( arrColors, "FF6633" );
	ArrayAppend( arrColors, "FF6666" );
	ArrayAppend( arrColors, "FF6699" );
	ArrayAppend( arrColors, "FF66CC" );
	ArrayAppend( arrColors, "FF66FF" );
	ArrayAppend( arrColors, "FF9900" );
	ArrayAppend( arrColors, "FF9933" );
	ArrayAppend( arrColors, "FF9966" );
	ArrayAppend( arrColors, "FF9999" );
	ArrayAppend( arrColors, "FF99CC" );
	ArrayAppend( arrColors, "FF99FF" );
	ArrayAppend( arrColors, "FFCC00" );
	ArrayAppend( arrColors, "FFCC33" );
	ArrayAppend( arrColors, "FFCC66" );
	ArrayAppend( arrColors, "FFCC99" );
	ArrayAppend( arrColors, "FFCCCC" );
	ArrayAppend( arrColors, "FFCCFF" );
	ArrayAppend( arrColors, "FFFF00" );
	ArrayAppend( arrColors, "FFFF33" );
	ArrayAppend( arrColors, "FFFF66" );
	ArrayAppend( arrColors, "FFFF99" );
	ArrayAppend( arrColors, "FFFFCC" );

</cfsilent>