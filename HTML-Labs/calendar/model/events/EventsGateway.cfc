<cfcomponent displayname="Event Gateway" hint="I am the Events gateway component" accessors="true" extends="model.abstract.BaseGateway" output="false">
	<cfscript>
		// ------------------------ PUBLIC METHODS ------------------------ //

		/**
		 * I delete an Event
		 */
		void function deleteEvent(required Event theEvent) {
			delete(arguments.theEvent);
		}
        
        
        Event function getCategories(required numeric EventId) {
			return get(entityName = "Category");
		}
        
		/**
		 * I return an Event matching an id
		 */
		Event function getEvent(required numeric EventId) {
			return get(entityName = "Event", id = arguments.EventId);
		}

		/**
		 * I return an Event matching a unique id
		 */
		Event function getEventsBySlug(required string slug) {
			local.Event = ORMExecuteQuery("from Event where slug = :slug and published <= :published", {slug = arguments.slug, published = Now()}, true);
			if (IsNull(local.Event)) {
				local.Event = new("Event");
			}
			return local.Event;
		}

		/**
		 * I return the count of Events
		 */
		numeric function getEventsCount() {
			return ORMExecuteQuery("select count(*) from Event", [], true);
		}
	</cfscript>

	<cffunction name="getEventss" output="false" returntype="Array" hint="I return an array of Events">
		<cfargument name="published" type="boolean" required="false" default="false" hint="True if only published Events should be returned">
		<cfargument name="maxResults" type="numeric" required="false" default="0" hint="The maximum number of results to return">
		<cfargument name="year" type="string" required="false" default="">
		<cfargument name="month" type="string" required="false" default="">
		<cfargument name="offset" type="numeric" required="false" default="0" hint="The offset">
        <cfargument name="siteID" type="numeric" required="true">
		<cfset local.ormOptions = {}>
		<cfif len(arguments.year) && len(arguments.month)>
			    <cfif arguments.month EQ "01" or arguments.month EQ "03" or arguments.month EQ "05" or arguments.month EQ "07"
			    or arguments.month EQ "08" or arguments.month EQ "10" or arguments.month EQ "12">
				    <cfset endDay = "31">
				<cfelseif arguments.month EQ "04" or arguments.month EQ "06" or arguments.month EQ "09" or arguments.month EQ "11">
				    <cfset endDay="30">    
			    <cfelseif arguments.month EQ "02">
			        <cfset endDay = "28">
			    </cfif>
		</cfif>	    
				
		<cfquery name="local.Events" dbtype="hql" ormoptions="#local.ormOptions#">
			FROM Event
			WHERE 1 = 1
			and site_id = #arguments.siteID#
			<cfif structKeyExists(arguments, 'user')>
				and IDUser = #arguments.user#
			</cfif>
			<cfif arguments.published>
				AND published < <cfqueryparam cfsqltype="cf_sql_integer" value="1">
			</cfif>
			<cfif len(arguments.year) && len(arguments.month)>
			    <cfset myEndDate = #createDate(arguments.year, arguments.month, endDay)#>
				AND DateTime BETWEEN <cfqueryparam cfsqltype="cf_sql_date" value="#createDate(arguments.year, arguments.month, 1)#"> 
				AND <cfqueryparam cfsqltype="cf_sql_date" value="#myEndDate#">
			</cfif>
			ORDER BY DateTime DESC
		</cfquery>

		<cfreturn local.Events>
	</cffunction>

	<cfscript>
		/**
		 * I save an Event
		 */
		Event function saveEvent(required Event theEvent) {
		    
			return save(arguments.theEvent);
		}
		
		numeric function getPublishedCount() {
		return ORMExecuteQuery("select count(*) from Event where published = true", [], true);
	}

	/**
	 * I return a count of unpublished Blogs
	 */
	numeric function getUnpublishedCount() {
		return ORMExecuteQuery("select count(*) from Event where published = false", [], true);
	}
	
		
	</cfscript>
</cfcomponent>
