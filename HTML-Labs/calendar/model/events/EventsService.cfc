/**
 * I am the news service component.
 */
component accessors = true extends = "model.abstract.BaseService" {

	// ------------------------ DEPENDENCY INJECTION ------------------------ //

	property name = "MetaData" getter = false;
	property name = "EventsGateway" getter = false;
	property name = "SecurityService" getter = false;
	property name = "Validator" getter = false;

	// ------------------------ PUBLIC METHODS ------------------------ //

	/**
	 * I delete an event
	 */
	struct function deleteEvent(required numeric eventId) {
		transaction{
			local.Event = variables.EventsGateway.getEvents(eventId = Val(arguments.eventId));
			local.result = variables.Validator.newResult();
			if (local.Event.isPersisted()) {
				variables.EventsGateway.deleteEvent(theEvent = local.Event);
				local.result.setSuccessMessage("The event &quot;#local.Event.getTitle()#&quot; has been deleted.");
			} else {
				local.result.setErrorMessage("The event could not be deleted.");
			}
		}
		return local.result;
	}

	/**
	 * I return an event matching an id
	 */
	Event function getEvents(required numeric eventId) {
		return variables.EventsGateway.getEvents(eventId = Val(arguments.eventId));
	}

	/**
	 * I return an event matching a unique id
	 */
	Event function getEventsBySlug(required string slug) {
		return variables.EventsGateway.getEventsBySlug(slug = arguments.slug);
	}

	/**
	 * I return the count of events
	 */
	numeric function getEventsCount() {
		return variables.EventsGateway.getEventsCount();
	}

    array function getCategories() {
	    
	    return variables.EventsGateway.getCategories();
    }
	/**
	 * I return an array of events
	 */
	array function getEventss(string searchTerm = "", numeric siteID, string month, string=year, string sortOrder = "published desc", boolean published = false, numeric maxresults = 0, numeric offset = 0) {
		local.args = Duplicate(arguments);
		local.args.maxResults = Val(local.args.maxResults);
		local.args.offset = Val(local.args.offset);
		
		return variables.EventsGateway.getEventss(argumentCollection = local.args);
	}
	/**
	 * I return an array of events
	 */
	array function getUserEvents(numeric User, numeric siteID, string month, string=year, string sortOrder = "published desc", boolean published = false, numeric maxresults = 0, numeric offset = 0) {
		local.args = Duplicate(arguments);
		local.args.maxResults = Val(local.args.maxResults);
		local.args.offset = Val(local.args.offset);

		return variables.EventsGateway.getEventss(argumentCollection = local.args);
	}

	/**
	 * I validate and save an event
	 */
	struct function saveEvent(required struct properties, required string websiteTitle) {
		transaction{
			local.args = Duplicate(arguments);
			param name = "local.args.properties.eventId" default = 0;
			param name = "local.args.properties.metaGenerated" default = false;
			local.Event = variables.EventsGateway.getEvents(eventId = Val(local.args.properties.eventId));
			
			local.User = variables.SecurityService.getCurrentUser();
			if (!IsNull(local.User)) {
				local.args.properties.updatedBy = local.User.getName();
			}
			
			populate(Entity = local.Event, memento = local.args.properties);
			
			
			
			
			
			local.result = variables.Validator.validate(theObject = local.Event);
			if (!local.result.hasErrors()) {
				variables.EventsGateway.saveEvent(theEvent = local.Event);
				
				//Saves Categories to Event
				//Remove All Cateogories from Event:
			    if(isDefined('local.args.properties.eventID') && local.args.properties.eventID GT "0") {
				ClearCats = new Query( sql = "Delete from blog_event_category where IDEvent=#local.args.properties.eventId#" ).execute( ).getREsult( );
				
			    }
				//Now Populate the New Categories to the Event:	
				for(i=1;i<=ListLen(#local.args.properties.Category#);i++) {
				value = ListGetAt( #local.args.properties.Category#, i );
				sql = "Insert into blog_event_category (IDPOST, IDCATEGORY) VALUES (#local.Event.getEventsID()#, #value#)";
				NewCat = new Query( sql = #sql# ).execute( ).getResult( );
				//writeOutPut('#value#<br />');
			    }
				
				
				local.result.setSuccessMessage("The event &quot;#local.Event.getTitle()#&quot; has been saved.");
			} else {
				local.result.setErrorMessage("Your event could not be saved. Please amend the highlighted fields.");
			}
		}
		return local.result;
	}
	
	query function getArchives(numeric siteID) {
		//return ORMExecuteQuery("SELECT DISTINCT DateTime FROM Event where site_id=#arguments.SiteID#", [], true);
	}
	
	
	numeric function getPublishedCount(numeric siteID) {
		return ORMExecuteQuery("select count(*) from Event where published = true and site_ID=#arguments.siteID#", [], true);
	}
	
	numeric function getUnpublishedCount(numeric siteID) {
		return ORMExecuteQuery("select count(*) from Event where published = false and site_ID=#arguments.siteID#", [], true);
	}
	
	numeric function getCommentsUnreadCount(numeric siteID) {
		return ORMExecuteQuery("select count(*) from Comment where comment_approved = false and site_ID=#arguments.siteID#", [], true);
	}
	
	

}
