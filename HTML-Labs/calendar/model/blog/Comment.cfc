/**
* I am a Comment Entity
*/
component persistent="true" table="blog_comment"
{
	/* ----------------------------------------- PROPERTIES -----------------------------------------  */
	
	/**
	* I am the unique identifier, which is numeric and generated for you
	*/
	property name="IDComment" fieldtype="id" generator="native" type="numeric" setter="false" ormtype="integer" notnull="true";
	property name="Name" type="string" default="" ormtype="string" column="comment_Name" length="200" notnull="true";
	property name="Value" type="string" default="" ormtype="string" column="comment_Value" length="500" notnull="true";
	property name="Comment_DateTime" type="date" default="{ts '1900-01-01 00:00:00'}" ormtype="date" column="comment_DateTime" notnull="true";
	property name="approved" type="boolean" default="0" ormtype="boolean" column="comment_approved" notnull="true";
	property name="Post" cfc="Post" fieldtype="many-to-one" fkcolumn="IDPost";
		
	/* ----------------------------------------- PUBLIC -----------------------------------------  */
	
	public string function getFormattedDateTime()
	{
		return DateFormat( getDateTime(), "dddd, dd mmm yy" );
	}

}