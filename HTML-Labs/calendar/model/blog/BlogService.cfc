/**
 * I am the news service component.
 */
component accessors = true extends = "model.abstract.BaseService" {

	// ------------------------ DEPENDENCY INJECTION ------------------------ //

	property name = "MetaData" getter = false;
	property name = "BlogGateway" getter = false;
	property name = "SecurityService" getter = false;
	property name = "Validator" getter = false;

	// ------------------------ PUBLIC METHODS ------------------------ //

	/**
	 * I delete an post
	 */
	struct function deletePost(required numeric postId) {
		transaction{
			local.Post = variables.BlogGateway.getPost(postId = Val(arguments.postId));
			local.result = variables.Validator.newResult();
			if (local.Post.isPersisted()) {
				variables.BlogGateway.deletePost(thePost = local.Post);
				local.result.setSuccessMessage("The post &quot;#local.Post.getTitle()#&quot; has been deleted.");
			} else {
				local.result.setErrorMessage("The post could not be deleted.");
			}
		}
		return local.result;
	}

	/**
	 * I return an post matching an id
	 */
	Post function getPost(required numeric postId) {
		return variables.BlogGateway.getPost(postId = Val(arguments.postId));
	}

	/**
	 * I return an post matching a unique id
	 */
	Post function getPostBySlug(required string slug) {
		return variables.BlogGateway.getPostBySlug(slug = arguments.slug);
	}

	/**
	 * I return the count of posts
	 */
	numeric function getPostCount() {
		return variables.BlogGateway.getPostCount();
	}

    array function getCategories() {
	    
	    return variables.BlogGateway.getCategories();
    }
	/**
	 * I return an array of posts
	 */
	array function getPosts(string searchTerm = "", numeric siteID, string month, string=year, string sortOrder = "published desc", boolean published = false, numeric maxresults = 0, numeric offset = 0) {
		local.args = Duplicate(arguments);
		local.args.maxResults = Val(local.args.maxResults);
		local.args.offset = Val(local.args.offset);
		
		return variables.BlogGateway.getPosts(argumentCollection = local.args);
	}
	/**
	 * I return an array of posts
	 */
	array function getUserPosts(numeric User, numeric siteID, string month, string=year, string sortOrder = "published desc", boolean published = false, numeric maxresults = 0, numeric offset = 0) {
		local.args = Duplicate(arguments);
		local.args.maxResults = Val(local.args.maxResults);
		local.args.offset = Val(local.args.offset);

		return variables.BlogGateway.getPosts(argumentCollection = local.args);
	}

	/**
	 * I validate and save an post
	 */
	struct function savePost(required struct properties, required string websiteTitle) {
		transaction{
			local.args = Duplicate(arguments);
			param name = "local.args.properties.postId" default = 0;
			param name = "local.args.properties.metaGenerated" default = false;
			local.Post = variables.BlogGateway.getPost(postId = Val(local.args.properties.postId));
			
			local.User = variables.SecurityService.getCurrentUser();
			if (!IsNull(local.User)) {
				local.args.properties.updatedBy = local.User.getName();
			}
			
			populate(Entity = local.Post, memento = local.args.properties);
			
			
			
			
			
			local.result = variables.Validator.validate(theObject = local.Post);
			if (!local.result.hasErrors()) {
				variables.BlogGateway.savePost(thePost = local.Post);
				
				//Saves Categories to Post
				//Remove All Cateogories from Post:
			    if(isDefined('local.args.properties.postID') && local.args.properties.postID GT "0") {
				ClearCats = new Query( sql = "Delete from blog_post_category where IDPost=#local.args.properties.postId#" ).execute( ).getREsult( );
				
			    }
				//Now Populate the New Categories to the Post:	
				for(i=1;i<=ListLen(#local.args.properties.Category#);i++) {
				value = ListGetAt( #local.args.properties.Category#, i );
				sql = "Insert into blog_post_category (IDPOST, IDCATEGORY) VALUES (#local.Post.getPostID()#, #value#)";
				NewCat = new Query( sql = #sql# ).execute( ).getResult( );
				//writeOutPut('#value#<br />');
			    }
				
				
				local.result.setSuccessMessage("The post &quot;#local.Post.getTitle()#&quot; has been saved.");
			} else {
				local.result.setErrorMessage("Your post could not be saved. Please amend the highlighted fields.");
			}
		}
		return local.result;
	}
	
	query function getArchives(numeric siteID) {
		//return ORMExecuteQuery("SELECT DISTINCT DateTime FROM Post where site_id=#arguments.SiteID#", [], true);
	}
	
	
	numeric function getPublishedCount(numeric siteID) {
		return ORMExecuteQuery("select count(*) from Post where published = true and site_ID=#arguments.siteID#", [], true);
	}
	
	numeric function getUnpublishedCount(numeric siteID) {
		return ORMExecuteQuery("select count(*) from Post where published = false and site_ID=#arguments.siteID#", [], true);
	}
	
	numeric function getCommentsUnreadCount(numeric siteID) {
		return ORMExecuteQuery("select count(*) from Comment where comment_approved = false and site_ID=#arguments.siteID#", [], true);
	}
	
	

}
