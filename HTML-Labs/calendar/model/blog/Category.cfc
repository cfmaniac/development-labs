/**
* I am a Category Entity
*/
component persistent="true" table="blog_category" 
{
	property name="IDCategory" fieldtype="id" generator="native" type="numeric" setter="false" ormtype="integer" notnull="true";
	property name="Name" type="string" default="" column="category_Name" ormtype="string" length="200" notnull="true";
	property name="Slug" type="string" default="" ormtype="string" column="category_slug" length="500" notnull="true";
	property name="parentID" type="numeric" default="0" column="category_parent" ormtype="integer" notnull="true";
	property name="OrderIndex" type="numeric" default="0" column="category_OrderIndex" ormtype="integer" notnull="true";
	property name="Posts" cfc="model.blog.Post" fieldtype="many-to-many" type="array" linktable="blog_post_category" inversejoincolumn="IDCategory" fkcolumn="IDPost";
	property name="site_id" column="site_id" getter="true"  setter="true" type="numeric" sqltype="cf_sql_integer";
}