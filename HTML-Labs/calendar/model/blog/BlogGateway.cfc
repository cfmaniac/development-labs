<cfcomponent displayname="Blog Gateway" hint="I am the blog gateway component" accessors="true" extends="model.abstract.BaseGateway" output="false">
	<cfscript>
		// ------------------------ PUBLIC METHODS ------------------------ //

		/**
		 * I delete an Post
		 */
		void function deletePost(required Post thePost) {
			delete(arguments.thePost);
		}
        
        
        Post function getCategories(required numeric PostId) {
			return get(entityName = "Category");
		}
        
		/**
		 * I return an Post matching an id
		 */
		Post function getPost(required numeric PostId) {
			return get(entityName = "Post", id = arguments.PostId);
		}

		/**
		 * I return an Post matching a unique id
		 */
		Post function getPostBySlug(required string slug) {
			local.Post = ORMExecuteQuery("from Post where slug = :slug and published <= :published", {slug = arguments.slug, published = Now()}, true);
			if (IsNull(local.Post)) {
				local.Post = new("Post");
			}
			return local.Post;
		}

		/**
		 * I return the count of Posts
		 */
		numeric function getPostCount() {
			return ORMExecuteQuery("select count(*) from Post", [], true);
		}
	</cfscript>

	<cffunction name="getPosts" output="false" returntype="Array" hint="I return an array of Posts">
		<cfargument name="published" type="boolean" required="false" default="false" hint="True if only published Posts should be returned">
		<cfargument name="maxResults" type="numeric" required="false" default="0" hint="The maximum number of results to return">
		<cfargument name="year" type="string" required="false" default="">
		<cfargument name="month" type="string" required="false" default="">
		<cfargument name="offset" type="numeric" required="false" default="0" hint="The offset">
        <cfargument name="siteID" type="numeric" required="true">
		<cfset local.ormOptions = {}>
		<cfif len(arguments.year) && len(arguments.month)>
			    <cfif arguments.month EQ "01" or arguments.month EQ "03" or arguments.month EQ "05" or arguments.month EQ "07"
			    or arguments.month EQ "08" or arguments.month EQ "10" or arguments.month EQ "12">
				    <cfset endDay = "31">
				<cfelseif arguments.month EQ "04" or arguments.month EQ "06" or arguments.month EQ "09" or arguments.month EQ "11">
				    <cfset endDay="30">    
			    <cfelseif arguments.month EQ "02">
			        <cfset endDay = "28">
			    </cfif>
		</cfif>	    
				
		<cfquery name="local.Posts" dbtype="hql" ormoptions="#local.ormOptions#">
			FROM Post
			WHERE 1 = 1
			and site_id = #arguments.siteID#
			<cfif structKeyExists(arguments, 'user')>
				and IDUser = #arguments.user#
			</cfif>
			<cfif arguments.published>
				AND published < <cfqueryparam cfsqltype="cf_sql_integer" value="1">
			</cfif>
			<cfif len(arguments.year) && len(arguments.month)>
			    <cfset myEndDate = #createDate(arguments.year, arguments.month, endDay)#>
				AND DateTime BETWEEN <cfqueryparam cfsqltype="cf_sql_date" value="#createDate(arguments.year, arguments.month, 1)#"> 
				AND <cfqueryparam cfsqltype="cf_sql_date" value="#myEndDate#">
			</cfif>
			ORDER BY DateTime DESC
		</cfquery>

		<cfreturn local.Posts>
	</cffunction>

	<cfscript>
		/**
		 * I save an Post
		 */
		Post function savePost(required Post thePost) {
		    
			return save(arguments.thePost);
		}
		
		numeric function getPublishedCount() {
		return ORMExecuteQuery("select count(*) from Post where published = true", [], true);
	}

	/**
	 * I return a count of unpublished Blogs
	 */
	numeric function getUnpublishedCount() {
		return ORMExecuteQuery("select count(*) from Post where published = false", [], true);
	}
	
		
	</cfscript>
</cfcomponent>
