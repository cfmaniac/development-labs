component persistent="true" table="blog_Post" 
{
	property name="PostID" column="PostID" fieldtype = "id" generator="native" type="numeric" setter="false" ormtype="integer" notnull="true";
	property name="Title" type="string" default="" ormtype="string" column="post_Title" length="500" notnull="true";
	property name="Slug" type="string" default="" ormtype="string" column="post_slug" length="500" notnull="true";	
	property name="Body" type="string" default="" ormtype="text" column="post_Body" notnull="true";
	property name="Header" type="string" default="" ormtype="string" column="post_header";
	property name="published" type="boolean" default="0" ormtype="boolean" column="post_published" notnull="true";
	property name="DateTime" type="date" default="{ts '1900-01-01 00:00:00'}" ormtype="date" column="post_DateTime" notnull="true";
	property name="Updated" type="date" default="{ts '1900-01-01 00:00:00'}" ormtype="date" column="post_Updated" notnull="true";
	property name="site_id" column="site_id" getter="true"  setter="true" type="numeric" sqltype="cf_sql_integer";
	
	property name="User" cfc="model.user.User" fieldtype="many-to-one" fkcolumn="IDUser";
	property name="Comments" cfc="model.blog.Comment" fieldtype="one-to-many" type="array" fkcolumn="IDPost"; 
	property name="Categories" cfc="model.blog.Category" fieldtype="many-to-many" type="array" linktable="blog_post_category" inversejoincolumn="IDPost" fkcolumn="IDCategory";
	
	/* ----------------------------------------- PUBLIC -----------------------------------------  */
	
	Post function init() {
	
		variables.published = true;
		return this;
	}
	
	public string function getFormattedDateTime()
	{
		return DateFormat( getDateTime(), "dddd, dd mmm yy" );
	}
	
	
	public string function getAuthorName()
	{
		return getUser().getName();
	}
	
	public string function getHeader()
	{
		return variables.header();
	}
	
	public void function removeAllCategories()
	{
		if ( hasCategories() )
		{
			variables.Categories = [];
		}
	}
	
	public numeric function getNumberOfComments()
	{
		return ArrayLen( getComments() );
	}
	
	
	public void function preInsert()
	{
		setDateTime( Now() );
	}
	
	boolean function isPersisted() {
		return !IsNull(variables.postId);
	}

}