
<cfcomponent
	output="false"
	hint="Handles the application-level event handlers and application settings.">
	
	
	<!--- Define application settings. --->
	<cfset THIS.Name = "Kinky Calendar" />
	<cfset THIS.ApplicationTimeout = CreateTimeSpan( 2, 0, 0, 0 ) />
	<cfset THIS.SessionManagement = false />
	<cfset THIS.SetClientCookies = false />
	
	<!--- Define page request settings. --->
	<cfsetting
		showdebugoutput="false"
		requesttimeout="20"
		/>

	
	<cffunction
		name="OnRequestStart"
		access="public"
		returntype="boolean"
		output="false"
		hint="Fires the initial pre-page processing event.">
		
		<!--- Define arguments. --->
		<cfargument
			name="Page"
			type="string"
			required="true"
			hint="The ColdFusion script who's execution has been requested."
			/>
		
		<!--- 
			Define DNS information (this should probably 
			be cached in the APPLICATION scope instead, 
			but trying to keep this as simple as possible -
			this would require you to change the CFQuery 
			tags as well).
		--->
		<cfset REQUEST.DSN = StructNew() />
		<cfset REQUEST.DSN.Source = "kinky_solutions" />
		<cfset REQUEST.DSN.Username = "" />
		<cfset REQUEST.DSN.Password = "" />
				
		<!--- Return out. --->
		<cfreturn true />
	</cffunction>
		
</cfcomponent>