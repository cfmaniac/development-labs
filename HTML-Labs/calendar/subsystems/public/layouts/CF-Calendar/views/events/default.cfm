<cfsavecontent variable="rc.htmlHead">
    <cfoutput>
	<link rel='stylesheet' type='text/css' href='layouts/#rc.template.name#/assets/js/fullCalendar/fullcalendar.css' />
	</cfoutput>
</cfsavecontent>
<cfsavecontent variable="rc.htmlFoot">
	<cfoutput>
		<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js'></script>
		<script src='layouts/#rc.template.name#/assets/js/fullCalendar/fullcalendar.min.js'></script>
		
		
		<script type='text/javascript'>
		
		$('##eventsCalendar').fullCalendar( {
		    header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
		    timezone: 'America/New_York',
		    selectable: true,
			selectHelper: true,
			eventLimit: true, // allow "more" link when too many events
			events: [
	        <cfloop array="#rc.Events#" index="event">
				{
				title : "#event.getTitle()#",
				start : "#dateformat(event.getstartAt(), 'YYYY-MM-DD')#"
				},
	        </cfloop>
			]
    
		});
		</script>
	</cfoutput>
</cfsavecontent>


<div id="main-content">
	<h3>Our Events</h3>
	<div class="scrollContent mCustomScrollbar" data-mcs-theme="rounded-dark" data-mcs-positon="outside">
	<cfoutput>
	
	<div id="eventsCalendar"></div>
	
	
	</cfoutput>
	
	</div>
</div>