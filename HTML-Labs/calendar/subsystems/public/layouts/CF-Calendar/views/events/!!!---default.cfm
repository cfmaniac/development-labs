<cfscript>
 local.since_date = '01/01/2016';
 local.until_date = '12/02/2016';
 local.facebookTKNURL ='https://graph.facebook.com/oauth/access_token?client_id=#rc.config.events.fbappID#&client_secret=#rc.config.events.fbappSCT#&grant_type=client_credentials';
 local.fields="id,name,description,place,timezone,start_time,cover";
 //This is the Events:
 //https://www.facebook.com/groups/237808572997544/events/
 local.facebookURL = "https://graph.facebook.com/"
 v2.5/237808572997544/events?access_token=#rc.config.events.fbappTKN#";
 //This is the Feed for the Group:
 //local.facebookURL = 'https://graph.facebook.com/#rc.config.name#/posts?access_token=#rc.config.events.fbappTKN#';

</cfscript>
<div id="main-content">
	<h3>Our Events</h3>
	<div class="scrollContent mCustomScrollbar" data-mcs-theme="rounded-dark" data-mcs-positon="outside">
    <cfscript>
    rc.EventsList = new http(); 
    /* set attributes using implicit setters */ 
    rc.EventsList.setMethod("get"); 
    rc.EventsList.setCharset("utf-8"); 
    rc.EventsList.setUrl(local.facebookURL); 
    
    rc.EventsResult = rc.EventsList.send().getPrefix();
    
    writeDump(var=rc.EventsResult); 
    </cfscript>
	</div>
</div>