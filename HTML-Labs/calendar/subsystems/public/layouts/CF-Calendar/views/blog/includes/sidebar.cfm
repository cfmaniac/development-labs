<cfoutput>
<h3>Blog Categories</h3>
<cfscript>
		rc.BlogCats = EntityLoad("Category", {parentid=0,site_ID=#rc.config.siteid#}, "OrderIndex ASC");
	</cfscript>   
<div id="accordion" role="tablist" aria-multiselectable="true">
<cfif arrayLen(rc.BlogCats)>
<cfloop array="#rc.BlogCats#" index="cats">
		 <cfscript>
            rc.SubCats = entityload('Category', {"parentid":#cats.getIDCategory()#}, "OrderIndex ASC");
         </cfscript>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="##accordion" href="###cats.GetSlug()#" aria-expanded="true" aria-controls="#cats.GetSlug()#">
            #cats.GetName()#
        </a>
      </h4>
    </div>
    <cfif arrayLen(rc.Subcats)>
    <div id="#cats.GetSlug()#" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
            <cfset First  = arraylen(rc.SubCats) /2>
	        <cfset second = first+1>
			
			<ul class="list-unstyled">
			    <cfloop array="#rc.SubCats#" index="subcat">
				<li><a href="#buildURL(action='blog.category', querystring="slug/#subcat.getSlug()#")#">#subcat.GetName()#</a></li>
				</cfloop>
			</ul>
			
     
     
    
    </div>
    </cfif>
  </div>
</cfloop>  
</cfif>  
<h3>Archives</h3>
<ul>
<cfloop query="#rc.Archives#">
	<cfif len(Month) is "1">
		<cfset month = '0#rc.archives.month#'>
	<cfelse>
		<cfset month = '#rc.archives.month#'>	
	</cfif>
	<li><a href="#BuildURL(action='blog.default', queryString='year=#Year#&month=#month#')#">#monthAsString(Month)# #rc.archives.Year#</a></li>	
</cfloop>

</ul>
</div>    
</cfoutput>    