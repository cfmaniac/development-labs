<!---
<div id="main-content">
	
	<div class="row col-sm-9">
	<cfoutput>
	<h3>Our Blog</h3>
	<cfif arraylen(rc.posts)>
	<cfloop array="#rc.Posts#" index="Post">
		    <div class="row">
			<div class="col-sm-2">
			    <time datetime="#post.getDateTime()#" class="icon" >
				  <em>#dateformat(post.getDateTime(),"dddd")#</em>
	              <strong>#dateformat(post.getDateTime(), "mmmm")#</strong>
	              <span>#dateformat(post.getDateTime(), "DD")#</span>
				</time>
			</div>
			<div class="col-sm-10">
				<a  href="#buildURL(action='blog.post', querystring='slug=#post.getSlug()#')#"><h3 class="windsong ws-lg siteColor">#Post.getTitle()#</h3></a>
							
				<span style="text-align: left">
				#snippet(post.getBody(), 355)#
				</span>
				
					<a href="#buildURL(action='blog.post', querystring='slug=#post.getSlug()#')#" class="btn btn-info pull-right">Read More</a>
				
				
			</div>
		    </div>
	</cfloop>
	<cfelse>
	<p>We don't have any Blog Posts at the moment.</p>
	</cfif>
	</div>
	<div class="col-sm-3">
		 <cfinclude template="/subsystems/public/layouts/#rc.templates.name#/views/blog/includes/sidebar.cfm"> 
	</div>
	
	</cfoutput>
	
	
</div>
--->
<cfoutput>
	<div class="container">
    <section class="margin-bottom">
        <div class="row">
            <div class="col-md-12">
                <h2 class="right-line">
	           #uCase('#rc.pageTitle#')#
                </h2>
            </div>
            <div class="col-md-10">
	             <cfif arraylen(rc.posts)>
	<cfloop array="#rc.Posts#" index="Post">
		    <div class="row">
			<div class="col-sm-2">
			    <time datetime="#post.getDateTime()#" class="icon" >
				  <em>#dateformat(post.getDateTime(),"dddd")#</em>
	              <strong>#dateformat(post.getDateTime(), "mmmm")#</strong>
	              <span>#dateformat(post.getDateTime(), "DD")#</span>
				</time>
			</div>
			<div class="col-sm-10">
				<a  href="#buildURL(action='blog.post', querystring='slug=#post.getSlug()#')#"><h3 class="windsong ws-lg siteColor">#Post.getTitle()#</h3></a>
							
				<span style="text-align: left">
				#snippet(post.getBody(), 355)#
				</span>
				
					<a href="#buildURL(action='blog.post', querystring='slug=#post.getSlug()#')#" class="btn btn-info pull-right">Read More</a>
				
				
			</div>
		    </div>
	</cfloop>
	<cfelse>
	<p>We don't have any Blog Posts at the moment.</p>
	</cfif>
            </div>
            <div class="col-md-2">
	             <cfinclude template="/subsystems/public/layouts/#rc.templates.name#/views/blog/includes/sidebar.cfm"> 
            </div>
        </div>
    </section>
	</div>
</cfoutput>