<cfoutput>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-magic"></i>Blog <span>> Archives > #rc.Year#</span> </h1>
        </div>
    </div>

    <div class="well well-sm">
    <cfif #rc.Posts.recordcount# GT "0">
        <cfloop query="#rc.Posts#">
                <div class="well well-sm">
                <p class="calendar">#DateFormat(rc.Posts.post_Datetime, "DD")# <em>#DateFormat(rc.Posts.post_Datetime, "Mmm YYYY")#</em></p>
                <h4><a href="#BuildURL(action='blog.post', querystring='id=#rc.posts.IDPost#')#">#rc.Posts.Post_title#</a> <a href="#BuildURL(action='blog.post', querystring='id=#IDpost#')#" class="btn btn-info pull-right">Read More...</a>
                <!---small class="pull-right">#DateFormat( rc.Posts.post_Datetime, "full" )#</small--->
                </h4>
                <p>Categories: #rc.Posts.Category_name# </p>
                <p>#snippet( rc.posts.post_body, 500 )#</p>
                </div>

        </cfloop>
        <cfelse>
            There are No Posts at this time.
            
    </cfif>




    </div>
</cfoutput>