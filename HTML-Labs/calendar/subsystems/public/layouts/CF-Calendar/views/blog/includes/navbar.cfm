<cfoutput>

<cfscript>
		rc.BlogCats = EntityLoad("Category", {parentid=0,site_ID=#rc.config.siteid#}, "OrderIndex ASC");
	</cfscript>
   		<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
   		<cfif arrayLen(rc.BlogCats)>
       
        <cfloop array="#rc.BlogCats#" index="cats">
         <cfscript>
            rc.SubCats = entityload('Category', {"parentid":#cats.getIDCategory()#}, "OrderIndex ASC");
         </cfscript>
             <cfif arrayLen(rc.Subcats)>
             <li class="dropdown-submenu">
                <a tabindex="-1" href="##">#cats.GetName()#</a>
                <ul class="dropdown-menu">
                <cfloop array="#rc.SubCats#" index="subcat">
                  <li><a tabindex="-1" href="#buildURL(action='blog.category', querystring="slug/#subcat.getSlug()#")#">#subcat.GetName()#</a></li>
                </cfloop>  
                 
                </ul>
              </li>
             <cfelse>
                <li><a class="dropdown-item" href="#buildURL(action='blog.category', querystring="slug/#cats.getSlug()#")#">#cats.getName()#</a></li>
             </cfif>
         </cfloop>
              
         </cfif>     
            </ul>
        

</cfoutput>


