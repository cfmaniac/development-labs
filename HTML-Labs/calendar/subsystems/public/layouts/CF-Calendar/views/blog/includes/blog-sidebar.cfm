<cfoutput>
	
	<ul>
		<cfloop array="#rc.BlogCategories#" index="cats">
		<cfscript>
            rc.SubCats = entityload('Category', {"parentid":#cats.getIDCategory()#}, "OrderIndex ASC");
         </cfscript>
		<li><a <cfif arraylen(rc.SubCats)>data-toggle="collapse" href="###cats.GetSlug()#" aria-expanded="false"<cfelse>href="#buildURL(action='blog.category', querystring="slug/#cats.getSlug()#")#"</cfif>>#cats.GetName()#</a>
			<cfif arraylen(rc.SubCats)>
			<ul class="collapse" id="#cats.GetSlug()#">
				<cfloop array="#rc.subCats#" index="subcats">
				<li><a href="#buildURL(action='blog.category', querystring="slug/#subcats.getSlug()#")#">#subcats.GetName()#</a></li>
				</cfloop>
			</ul>
			</cfif>
		</li>
		</cfloop>
	</ul>
</cfoutput>