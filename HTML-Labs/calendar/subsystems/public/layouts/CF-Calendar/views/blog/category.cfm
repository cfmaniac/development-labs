<cfscript>
  thirtyDaysAgo = '#dateformat(dateAdd("d", -60, Now()), 'MM/DD/YYYY')#';
  today = #dateformat(dateAdd("d", +1, Now()), 'MM/DD/YYYY')#;
  rc.RecentBlogs = new Query(sql="Select Top 15 * from blog_Post where post_DateTime between '#thirtyDaysAgo#' AND '#today#' Order By post_DateTime DESC" ).execute().getResult();
  
  
  //writeDump(var="#rc#", abort='true');
</cfscript>

<cfoutput>
<div class="container">
    <section class="margin-bottom">
        <div class="row">
            <div class="col-md-12">
                <h2 class="right-line">#uCase('#rc.config.sitename# - Blog : #rc.posts.category_name#')#</h2>
            </div>
            <div class="col-md-8">
	            <cfif #rc.posts.recordcount# GT "0">
            <cfloop query="#rc.posts#">
             <cfscript>
             rc.PostCategories = new Query(sql="Select * from blog_post_category PC INNER JOIN blog_category CT on PC.IDCategory = CT.IDCategory where IDPost =#IDpost#").execute().getResult();
             </cfscript>
            <article class="post animated fadeInLeft animation-delay-8">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3 class="post-title"><a href="#BuildURL(action='blog.post', querystring='slug=#rc.posts.Post_slug#')#">#rc.posts.Post_Title#</a></h3>
                        <div class="row">
                            
                            <div class="col-lg-12 post-content">
                              #snippet( rc.posts.post_body, 500 )# 
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer post-info-b">
                        <div class="row">
                            <div class="col-lg-10 col-md-9 col-sm-8">
                                <i class="fa fa-clock-o"></i> #DateFormat(rc.posts.post_Datetime, "Full")# <i class="fa fa-user"> </i>.
                            </div>
                            <div class="col-lg-2 col-md-3 col-sm-4">
                                <a href="#BuildURL(action='blog.post', querystring='slug=#rc.posts.Post_slug#')#" class="pull-right">Read more...</a>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            
            
            </cfloop>
         <cfelse>
         <p>We haven't posted anything on our Blog Yet.</p>
         
         </cfif>
            </div>
            <div class="col-md-4">
	            <cfinclude template="/subsystems/public/layouts/#rc.templates.name#/views/blog/includes/sidebar.cfm">
            </div>
        </div>
    </section>
	</div>

</cfoutput>

