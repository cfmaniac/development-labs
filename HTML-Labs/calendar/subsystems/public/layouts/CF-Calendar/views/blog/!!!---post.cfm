<cfscript>
	if( cgi.rc.method == "Post" ) {
	
              q_service = new query();

              q_service.setName("NewComment");
              q_service.addParam(name = "postID", value = "#rc.PostID#", cfsqltype = "cf_sql_integer");
              q_service.addParam(name = "comment", value = "#rc.comment#", cfsqltype = "cf_sql_varchar");
              q_service.addParam(name = "name", value = "#rc.name#", cfsqltype = "cf_sql_varchar");
              q_service.addParam(name = "commentdate", value = "#now()#", cfsqltype = "cf_sql_date");
              q_service.addParam(name = "commentApproved", value="1", cfsqltype="cf_sql_integer");

              sql = "INSERT INTO blog_comment (Comment_Name, Comment_Value, Comment_DateTime, IDPost, Comment_Approved) VALUES (
            					:name, :comment, :commentdate, :postID, :commentApproved)"; 
              q_service.setSQL(sql).execute().getResult();
              
              
    }
    
    
    </cfscript>
    
    <cfloop array="#rc.Post#" index="local.Post">
    <cfscript>
	rc.MetaData.setMetaTitle("Reading: #local.post.getTitle()#");
	</cfscript>
	</cfloop>

<cfoutput>
<div id="main-content">
   
   <cfloop array="#rc.Post#" index="local.Post">
   <cfscript>
   //Get the PostID:
   rc.PostID = #local.post.getPostID()#;
   //Get Post Comments:
   rc.CommentsInfo = new Query(sql="select * from Blog_comment where IDPost=#rc.PostID# and comment_approved=1").execute().getResult();
   
   </cfscript>
   <div class="row">
	   <div class="col-sm-9">
		 <h1 class="windsong ws-lg siteColor">#local.post.getTitle()#</h1>
   
	<div class="scrollContent mCustomScrollbar" data-mcs-theme="rounded-dark" data-mcs-positon="outside">
	<div class="row" style="margin-bottom: 10px">
	   <div class="col-sm-6">
	   
		    <a href="#buildURL(action='blog.post', querystring='/slug=#post.getSlug()###Comments')#" class="btn btn-danger">Comments</a>    
	   </div>
	   <div class="col-sm-6"><a href="#buildURL('blog')#" class="btn btn-info pull-right">Back to Blog</a></div>
   </div>
   
	#local.post.getBody()#
	
	
	
   </cfloop>
	   </div>
	   
	   <div class="col-sm-3">
		   #view('/blog/includes/sidebar')#
	   </div>
   </div>
   
	
	
	<hr>
	<a name="Comments"></a>
	<h4>Comments (#numberFormat(rc.CommentsInfo.RecordCount)#)</h4>
	<cfif #rc.CommentsInfo.RecordCount# GT "0">
     <cfloop query="#rc.CommentsInfo#">
     <div class="row col-sm-12">
               
        <p class="post-comment" style="padding: 10px;">
        <em>#rc.CommentsInfo.comment_Name# <span style="font-size: 12px;">commented on  #DateFormat( rc.CommentsInfo.comment_DateTime, "Mmm YYYY" )#</em></span>:<br>    
        #htmlEditFormat(rc.commentsInfo.comment_Value)#
        </p>
  
    </div>
    </cfloop>
    	
    <cfelse>
    No Comments Yet
    </cfif>
    
    <!---Hey, is this a Short Blog Post? Don't show the Second Comments|Back to Glob Bar--->
    <cfif #len(local.Post.getBody())# GTE "750">
    <div class="row" style="margin-bottom: 10px">
	   <div class="col-sm-6"><cfif StructKeyExists(rc, "CurrentUser")>
	    
		    <a href="#buildURL(action='blog.post', querystring='/slug=#post.getSlug()###Comments')#" class="btn btn-danger">Comments</a>    
	   
	  
	   
	   </cfif></div>
	   <div class="col-sm-6"><a href="#buildURL('blog')#" class="btn btn-info pull-right">Back to Blog</a></div>
   </div>
   </cfif>
   <!---End Second Bar--->
    
	<cfif structKeyExists(rc, "loggedin") && structkeyExists(rc, "CurrentUser") >
	<div class="col-sm-12">
   
    <h4>Add a Comment...</h4>
		    <cfif cgi.rc.method is "Post">
			    <p class="alert alert-info">
				    Thank you, #rc.CurrentUser.getName()# for posting your comment.
			    </p>
		    <cfelse>
            <form action="" method="post" class="smart-form">
            <input type="hidden" name="postID" value="#rc.PostID#">
            <div class="form-group">
                    <label>Your Name</label>
                    <input type="text" type="text" name="name" value="#rc.CurrentUser.getName()#" class="form-control pbp_input" >
                    
            </div>
            <div class="form-group">
                    <label>Your Comments</label>
                    <textarea rows="3" name="comment" class="form-control pbp_input" placeholder="Your Comments..."></textarea>
                    
            </div>
            <button type="submit" class="btn btn-success pull-right">
                        Submit Comment
             </button>
            <!---<fieldset>
    								<div class="row">
    									<section class="col col-12">
    										<label class="input"> <i class="icon-prepend fa fa-user"></i>
    											<input type="text" name="name" value="#rc.CurrentUser.getName()#">
    										</label>
    									</section>
    									
    								</div>
    
                                        <section>
        									<label class="textarea"> 										
        										<textarea rows="3" name="comment" placeholder="Your Comments..."></textarea> 
        									</label>
        								</section>
        								
        								
        								
                                     <button type="submit" class="btn btn-primary">
            									Submit Comment
            						 </button>
    							</fieldset>--->
                                
            		
            
            	</form>
            	</cfif>
    </div>
	
	</cfif>
	</div>
	
</div>
</cfoutput>