<cfset rc.layout = false>

<cfsetting showdebugoutput="no">

<cfset local.myStruct = {}>
<cfset local.myStruct.title = rc.modules.blog.rssTitle>
<cfset local.myStruct.link = rc.basehref>
<cfset local.myStruct.description = rc.modules.blog.rssDescription>
<cfset local.myStruct.pubDate = Now()>
<cfset local.myStruct.version = "rss_2.0">

<cfset local.myStruct.item = []>

<cfset local.currentRow = 1>

<cfloop array="#rc.posts#" index="local.Post">
	<cfset local.myStruct.item[local.currentRow] = {}>
	<cfset local.myStruct.item[local.currentRow].title = local.Post.getTitle()>
	<cfset local.myStruct.item[local.currentRow].link = buildURL(action = 'news.article', path = rc.baseHref & "index.cfm", queryString = 'slug=#local.Post.getSlug()#')>
	<cfset local.myStruct.item[local.currentRow].description = {}>
	<cfset local.myStruct.item[local.currentRow].description.value = #snippet(local.post.getBody(), 355)#>
	<cfset local.myStruct.item[local.currentRow].pubDate = DateFormat(local.Post.getDateTime())>

	<cfset local.currentRow++>
</cfloop>

<cffeed action="create" name="#local.myStruct#" escapechars="false" xmlVar="local.xml">

<cfcontent type="text/xml" reset="yes"><cfoutput>#local.xml#</cfoutput>
