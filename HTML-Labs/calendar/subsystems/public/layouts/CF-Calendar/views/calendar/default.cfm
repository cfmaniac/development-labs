<cfoutput>
    <div id="calendar-wrap">
		<div id="calendar">
    			<ul class="weekdays">
    				<li>Sunday</li>
    				<li>Monday</li>
    				<li>Tuesday</li>
    				<li>Wednesday</li>
    				<li>Thursday</li>
    				<li>Friday</li>
    				<li>Saturday</li>
    			</ul>
    			<!---//Dynamic Days Generation--->
    			<!--- Loop over all the days. --->
	<cfloop index="dtDay" from="#rc.dtFirstDay#" to="#rc.dtLastDay#" step="1">

		<!---
			If we are on the first day of the week, then
			start the current table fow.
		--->
		<ul class="days">
			<li class="day <cfif ((Month( dtDay ) NEQ Month( rc.dtThisMonth )) OR (Year( dtDay ) NEQ Year( rc.dtThisMonth )))>other-month
			<cfelseif (dtDay EQ Fix( Now() ))> today </cfif>">
				<div class="date">#Day( dtDay )#</div>
				<cfif StructKeyExists( rc.objEvents.Index, dtDay )>

                </cfif>

                </li>


		<cfif NOT (DayOfWeek( dtDay ) MOD 7)>
		</ul>
		</cfif>

	</cfloop>

	</div>

</cfoutput>