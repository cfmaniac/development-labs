<cfoutput>
	<header id="header-full-top" class="hidden-xs header-full">
    <div class="container">
        <div class="header-full-title ">
            
    <h1 class="animated fadeInRight">
    
    <a href="#buildURL('')#"><span>#listFirst(rc.config.SiteName, 'X')#</span> - #listLast(rc.config.sitename, 'Replicator')#</a></h1>
            <p class="animated fadeInRight">Website Management System</p>
           
        </div>
        <nav class="top-nav">
            <cfif len(rc.config.facebookurl) || len(rc.config.linkedinurl) || len(rc.config.twitterurl) || len(rc.config.youtubeurl) || len(rc.config.googleplusurl)>
                <cfinclude template="header/socialbar.cfm">
            </cfif>
           
            <cfif rc.modules.users.enableFrontEnd>
                <cfinclude template="header/login.cfm">
            </cfif>
            
            <cfinclude template="header/searchbar.cfm">
        </nav>
    </div> <!-- container -->
</header> <!-- header-full -->
</cfoutput>