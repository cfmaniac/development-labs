<cfoutput>
	<div class="dropdown animated fadeInDown animation-delay-11">
	<cfif rc.modules.users.enableFrontEnd && structKeyExists(rc, "loggedin") && structkeyExists(rc, "CurrentUser") >
	            <a href="#buildURL('users.default')#"><i class="fa fa-user"></i> My Account</a>
	<cfelse>
                <a href="##" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Login</a>
                <div class="dropdown-menu dropdown-menu-right dropdown-login-box animated flipCenter">
                    <form role="form" action="#buildURL('security/login')#" method="post">
                        <h4>Login Form</h4>

                        <div class="form-group">
                            <div class="input-group login-input">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" class="form-control" name="email" id="email"class="form-control" placeholder="Username" required autofocus>
                            </div>
                            <br>
                            <div class="input-group login-input">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input type="password" class="form-control" name="password" id="password" class="form-control" placeholder="Password" required>
                            </div>
                            <!---div class="checkbox pull-left">
                                  <input type="checkbox" id="checkbox_remember1">
                                  <label for="checkbox_remember1">
                                     Remember me
                                  </label>
                            </div--->
                            <button type="submit" class="btn btn-ar btn-primary pull-right">Login</button>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>
            </div> <!-- dropdown -->
    </cfif>        
</cfoutput>