<cfoutput>
	
<nav class="navbar navbar-default navbar-header-full navbar-dark yamm navbar-static-top" role="navigation" id="header">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="##bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <i class="fa fa-bars"></i>
            </button>
            <a id="ar-brand" class="navbar-brand hidden-lg hidden-md hidden-sm" href="#buildURL('')#">#rc.config.sitename#</a>
        </div> <!-- navbar-header -->
       
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="pull-right">
            <a href="javascript:void(0);" class="sb-icon-navbar sb-toggle-right"><i class="fa fa-bars"></i></a>
        </div>
         
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <div class="nav navbar-nav">
            <ul class="nav navbar-nav">
                <cfloop query="rc.navigation">
                    <cfif rc.navigation.slug is "home">
                        <cfset homepageID = #rc.navigation.pageid#>
                        <li class="nav-item">
                        <a href="#buildURL(rc.modules.pages.defaultSlug)#" title="#buildURL(action=rc.modules.pages.defaultSlug)#">#UCase('Home')#</a>
                        </li>
                    <cfelseif rc.navigation.slug NEQ "home">
                        <cfscript>
                        rc.subSQL = "Select page_title as title, page_slug as slug from Pages where page_ancestorid = :pageID and site_id=:siteID";
                        rc.subs = new Query();
                        rc.subs.addParam(name="pageID", value="#rc.navigation.pageid#");
                        rc.subs.addParam(name="show", value="1");
                        rc.subs.addParam(name="siteID", value="#rc.config.siteID#");
                        rc.subnavigation = rc.subs.execute(SQL=rc.SubSQL).getResult();
                        </cfscript>
                        <cfif rc.subnavigation.recordcount GT "0">
                        <li class="nav-item dropdown">
                        <a href="#buildURL(rc.navigation.slug)#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        #UCase(rc.navigation.title)#</a>
                        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                        <cfloop query="rc.subnavigation">
                            <li>
                            <a class="nav-link link" href="#buildURL(rc.subnavigation.slug)#">#UCase(rc.subnavigation.title)# </a>
                            </li>
                        </cfloop>                        
                        </ul>                        
                        </li>    
                        <cfelse>
                                <cfif rc.navigation.slug is "blog">
                                    <cfif rc.modules.blog.enabled>
                                    <li class="nav-item dropdown">
                                        <a href="#buildURL(rc.navigation.slug)#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        #UCase(rc.navigation.title)#</a>
                                        <cfinclude template="/subsystems/public/layouts/#rc.templates.name#/views/blog/includes/navbar.cfm">
                                        
                                      </li>
                                    </cfif>
                                <cfelseif rc.navigation.slug is "events">
                                    <cfif rc.modules.calendars.enabled>
                                    <li class="nav-item">
                                    <a class="nav-link link" href="#buildURL('events')#">#UCase('Events')#</a>
                                    </li>	
                                    </cfif>
                                <cfelseif rc.navigation.slug is "store">
                                    <cfif rc.modules.stores.enabled>
                                    <li class="nav-item dropdown">
                                        <a href="#buildURL(rc.navigation.slug)#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        #UCase(rc.navigation.title)#</a>
                                        <cfinclude template="/subsystems/public/layouts/#rc.templates.name#/views/store/includes/navbar.cfm">
                                        
                                      </li>
                                    
                                                            
                                    </cfif>
                                <cfelseif rc.navigation.slug is "contact">
                                    <cfif rc.modules.inbox.enabled>
                                    <li class="nav-item">
                                    <a class="nav-link link" href="#buildURL(rc.navigation.slug)#">#UCase(rc.navigation.title)#</a>
                                    </li>	
                                    </cfif>	            
                                <cfelse>
                                <!---Standard Pages--->
                                <li class="nav-item">
                                <a class="nav-link link" href="#buildURL(rc.navigation.slug)#">
                                #UCase(rc.navigation.title)#</a>
                                </li> 
                                </cfif>            
                        
                           
                        
                        </cfif>
                        
                    </cfif>
                </cfloop>
            </ul>
        </div>    
        <!---
                                
             --->               
              
    </div><!-- container -->
</nav>
</cfoutput>