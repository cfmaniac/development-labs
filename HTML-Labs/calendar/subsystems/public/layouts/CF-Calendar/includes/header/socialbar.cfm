<cfoutput>
	 <ul class="top-nav-social hidden-sm">
                <li><a href="##" class="animated fadeIn animation-delay-6 rss"><i class="fa fa-rss"></i></a></li>
                <cfif len(rc.config.twitterurl)>
                    <li><a href="#BuildURL(rc.config.twitterurl)#" class="animated fadeIn animation-delay-7 twitter"><i class="fa fa-twitter"></i></a></li>
                </cfif>
                <cfif len(rc.config.twitterurl)>
                    <li><a href="#BuildURL(rc.config.facebookurl)#" class="animated fadeIn animation-delay-8 facebook"><i class="fa fa-facebook"></i></a></li>
                </cfif>
                <cfif len(rc.config.linkedinurl)>
                     <li><a href="#BuildURL(rc.config.linkedinurl)#" class="animated fadeIn animation-delay-7 linkedin"><i class="fa fa-linkedin"></i></a></li>                   
                </cfif>
                <cfif len(rc.config.googleplusurl)>
                    <li><a href="#BuildURL(rc.config.googleplusurl)#" class="animated fadeIn animation-delay-9 google-plus"><i class="fa fa-google-plus"></i></a></li>
                </cfif>
                <!---
                <li><a href="##" class="animated fadeIn animation-delay-9 google-plus"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="##" class="animated fadeIn animation-delay-9 instagram"><i class="fa fa-instagram"></i></a></li>
                <li><a href="##" class="animated fadeIn animation-delay-8 vine"><i class="fa fa-vine"></i></a></li>
                
                <li><a href="##" class="animated fadeIn animation-delay-6 flickr"><i class="fa fa-flickr"></i></a></li>
                --->
     </ul>
</cfoutput>