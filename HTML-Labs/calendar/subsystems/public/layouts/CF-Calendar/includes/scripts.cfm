<cfoutput>
<script src="public/layouts/#rc.templates.name#/assets/js/vendors.js"></script>
<script src="public/layouts/#rc.templates.name#/assets/js/styleswitcher.js"></script>

<!-- Syntaxhighlighter -->
<script src="public/layouts/#rc.templates.name#/assets/js/syntaxhighlighter/shCore.js"></script>
<script src="public/layouts/#rc.templates.name#/assets/js/syntaxhighlighter/shBrushXml.js"></script>
<script src="public/layouts/#rc.templates.name#/assets/js/syntaxhighlighter/shBrushJScript.js"></script>

<script src="public/layouts/#rc.templates.name#/assets/js/DropdownHover.js"></script>
<script src="public/layouts/#rc.templates.name#/assets/js/app.js"></script>
<script src="public/layouts/#rc.templates.name#/assets/js/holder.js"></script>
<script src="public/layouts/#rc.templates.name#/assets/js/home_info.js"></script>
</cfoutput>