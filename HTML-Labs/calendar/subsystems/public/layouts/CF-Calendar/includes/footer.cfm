<cfoutput>
	
<aside id="footer-widgets">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h3 class="footer-widget-title">Sitemap</h3>
                <ul class="list-unstyled three_cols">
                    <cfloop query="#rc.navigation#">
                    <li><a href="#buildURL('#slug#')#">#title#</a></li>
                    </cfloop>
                    <cfif rc.modules.users.enableFrontEnd && structKeyExists(rc, "loggedin") && structkeyExists(rc, "CurrentUser") >
		                      
			            <li>
			            <a class="nav-link dropdown-toggle" href="#buildURL('users.default')#" data-toggle="dropdown-submenu" >My Account</a>
                        </li>
			            <li>
			            <a class="dropdown-item" href="#buildURL('security.logout')#">Logout</a>
			            </li>
			            <cfelse>
			            <li>
			            <a href="#buildURL('security')#">Login / Register</a>
			            </li>
		            </cfif>	  
                </ul>
                <cfif rc.modules.emaillist.enabled>
                <h3 class="footer-widget-title">Be in the Know...</h3>
                <p>Subscribe to our newsletter and get news, helpful aticles and Square 1 updates!</p>
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Email Adress">
                    <span class="input-group-btn">
                        <button class="btn btn-ar btn-primary" type="button">Subscribe</button>
                    </span>
                </div><!-- /input-group -->
                </cfif>
            </div>
            <div class="col-md-4">
                <div class="footer-widget">
                    <h3 class="footer-widget-title">Recent Posts</h3>
                    <cfscript>
                        rc.Posts = entityload('Post', {site_ID='#rc.config.siteID#'}, 'DateTime DESC'); 
                    </cfscript>
                    <cfif arraylen(rc.posts)>
                    <cfset counter = 1>
                    <cfloop array="#rc.Posts#" index="Post">
                            <div class="row">
                            <div class="media">
                            
                                <div class="media-body">
                                    <h4 class="media-heading"><a href="#buildURL(action='blog.post', querystring='slug=#post.getSlug()#')#">#Post.getTitle()#</a></h4>
                                    <small>#dateformat(post.getDateTime(), "mmmm")# #dateformat(post.getDateTime(), "DD")#, #dateformat(post.getDateTime(), "YYYY")#</small>
                                </div>
                            </div>
                            </div>
                             <cfset counter = counter + 1>
                              <cfif counter gte 5>
                                      <cfbreak>
                              </cfif>
                    </cfloop>
                    <cfelseif !arraylen(rc.posts)>
                    <div class="row">
                            <div class="media">
                            
                                <div class="media-body">
                    <h4 class="media-heading"><a href="##">No New Blog Posts</a></h4>
                                   
                                </div>
                            </div>
                            </div>
                    </cfif>
                    
                </div>
            </div>
            <div class="col-md-4">
                <div class="footer-widget">
                    <h3 class="footer-widget-title">About Us</h3>
                     #pageSnippet('about-us', '#rc.config.siteID#')#
                </div>
            </div>
        </div> <!-- row -->
    </div> <!-- container -->
</aside> <!-- footer-widgets -->
<footer id="footer">
    <p>&copy; #dateformat(now(), "YYYY")# <a style="color:##ccc;" href="#buildURL('')#">#rc.config.sitename#</a>, Inc. All rights reserved.</p>
</footer>

</div> <!-- boxed -->
</div> <!-- sb-site -->

<div class="sb-slidebar sb-right sb-style-overlay">
    <div class="input-group">
        <input type="text" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
            <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
        </span>
    </div><!-- /input-group -->

    <h2 class="slidebar-header no-margin-bottom">Navigation</h2>
    <ul class="slidebar-menu">
        <li><a href="index.html">Home</a></li>
        <li><a href="portfolio_topbar.html">Portfolio</a></li>
        <li><a href="page_about3.html">About us</a></li>
        <li><a href="blog.html">Blog</a></li>
        <li><a href="page_contact.html">Contact</a></li>
    </ul>

    <h2 class="slidebar-header">Social Media</h2>
    <div class="slidebar-social-icons">
        <a href="##" class="social-icon-ar rss"><i class="fa fa-rss"></i></a>
        <a href="##" class="social-icon-ar facebook"><i class="fa fa-facebook"></i></a>
        <a href="##" class="social-icon-ar twitter"><i class="fa fa-twitter"></i></a>
        <a href="##" class="social-icon-ar pinterest"><i class="fa fa-pinterest"></i></a>
        <a href="##" class="social-icon-ar instagram"><i class="fa fa-instagram"></i></a>
        <a href="##" class="social-icon-ar wordpress"><i class="fa fa-wordpress"></i></a>
        <a href="##" class="social-icon-ar linkedin"><i class="fa fa-linkedin"></i></a>
        <a href="##" class="social-icon-ar flickr"><i class="fa fa-flickr"></i></a>
        <a href="##" class="social-icon-ar vine"><i class="fa fa-vine"></i></a>
        <a href="##" class="social-icon-ar dribbble"><i class="fa fa-dribbble"></i></a>
    </div>
</div> <!-- sb-slidebar sb-right -->
<div id="back-top">
    <a href="##header"><i class="fa fa-chevron-up"></i></a>
</div>
</cfoutput>