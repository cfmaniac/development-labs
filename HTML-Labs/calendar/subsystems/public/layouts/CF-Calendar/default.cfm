<cfset rc.layout = false>
<cfoutput>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <title>#rc.config.SiteName# #rc.MetaData.getMetaTitle()#</title>
    <base href="#rc.baseHref#">
    <link rel="shortcut icon" href="public/layouts/#rc.templates.name#/assets/img/favicon.png" />

    <meta name="description" content="">

    <!-- CSS -->
    <link href="public/layouts/#rc.templates.name#/assets/css/calendar.css" rel="stylesheet" title="default">
    </head>

<body>
<div class="sb-site-container">
<div class="boxed">

	<div class="container">
		<!---We Use Template Specific Module Layouts--->
        <cfscript>
        if(structKeyExists(rc, 'action')){
		   sectionaction = rereplace(rc.action, "public:", "","ALL");
		   sectionaction = listToArray(sectionaction, ".");
		   
		   section = sectionaction[1];
		   action = sectionaction[2];
		   
		   local.template = 'views/#section#/#action#.cfm';
	       local.templatePath = '/subsystems/public/layouts/#rc.templates.name#/#local.template#';
	   }
       </cfscript>
       <cfif fileExists(#expandPath(local.templatePath)#)>
            <cfinclude template="#local.template#">  
       <cfelse>
   
       <!---Uses the Default Template--->
       <cfscript>
	       local.template = 'views/#section#/#action#.cfm';
	       local.templatePath = '/subsystems/public/layouts/Default/#local.template#';
       </cfscript>
       </cfif> 
        

	</div>

</div>
</div>



</body>

</html>
</cfoutput>