component accessors = true {
    property name = "BlogService" setter = true getter = false;
    property name = "StoreService" setter = true getter = false;
    property name = "UserService" setter = true getter = false;
    property name = "UserGateway" setter = true getter = false;
	property name = "SecurityService" setter = true getter = false;
	// ------------------------ PUBLIC METHODS ------------------------ //
    // Public Function: getPlugin()
	
	
	
	void function init(required any fw) {
		variables.fw = arguments.fw;
	}
	
	//Function: TemplateView:
   string function TemplateView(required string path) {
	   
	   
	   
	   
	   
        //include '#arguments.path#';
    
  
	
  }
   //Get Layout View from Template:
  
	void function before(required struct rc) {
	    
	   rc.regions = variables.fw.getPlugin('regionHelper');
	    
	    rc.siteName = ReReplace(LCase(rc.config.sitename), "&trade;", "", "all");
	    rc.siteName = ReReplace(LCase(rc.siteName), "[^a-z0-9]", "-", "all");	  	  
	   	    
	    if(structKeyExists(session, 'userid')){
		    rc.User = variables.UserService.getUser(#session.userid#);
			rc.CurrentUser = variables.SecurityService.getCurrentUser( );
			rc.loggedin = variables.SecurityService.isAllowed(action = variables.fw.getFullyQualifiedAction());
	    }
		
	}
		
			
	

}
