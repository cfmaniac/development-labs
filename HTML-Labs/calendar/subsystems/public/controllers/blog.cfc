component accessors = true extends = "abstract"{
    
    property name = "BlogService" setter = true getter = false;
    property name = "UserService" setter="true" getter="false";
    property name="SecurityService" setter = true getter = false;
	property name = "config" setter = true getter = false;
    
    function init( fw )
    {
        variables.fw = arguments.fw;
    }

    function getCurrentUser(){
        if( hasCurrentUser() ){
            var map = getCurrentStorage();
            var userkey = map[ variables.userkey ];
            return variables.UserGateway.getUser( userkey );
        }
    }
    
    
    function before(required struct rc){

		if(!structKeyExists(session, 'userid')){
		   variables.fw.redirect("security");
	   } else {

			//rc.Archives = variables.BlogService.getArchives(siteID='#rc.config.siteID#');
			rc.ArchivesSQL = "Select Distinct MONTH(post_dateTime) as Month, YEAR(post_dateTime) as Year from blog_post where site_id=:siteID GROUP BY MONTH(post_dateTime), YEAR(post_dateTime)";
			rc.arch = new Query( );
			rc.arch.addParam( name = 'siteID', value = '#rc.config.siteID#' );
			rc.Archives = rc.arch.execute( sql = rc.ArchivesSQL ).getResult( );

			if( structKeyExists( session, 'userid' ) ) {
				rc.User = variables.UserService.getUser( #session.userid# );
				rc.CurrentUser = variables.SecurityService.getCurrentUser( );
				rc.loggedin = variables.SecurityService.isAllowed( action = variables.fw.getFullyQualifiedAction( ) );
			}

		}
    }
    
    void function default(required struct rc) {
	  param name="rc.year" default="";
	  param name="rc.month" default="";
	   
	   if (len(rc.year) && len(rc.month)){
		   rc.Posts = variables.BlogService.getPosts(siteID='#rc.config.siteID#', month='#rc.month#', year='#rc.year#');
		   rc.MetaData.setMetaTitle("Our Blog Posts for #monthAsString(rc.month)# #rc.year#");
		   rc.PageTitle = 'Our Blog Posts Archive for #monthAsString(rc.month)# #rc.year#';
	   } else {
		  rc.Posts = variables.BlogService.getPosts(siteID='#rc.config.siteID#'); 
		  rc.MetaData.setMetaTitle("Our Blog");
		  rc.PageTitle = 'Our Blog';
	   }
	   
	   rc.MetaData.setMetaDescription("");
       rc.MetaData.setMetaKeywords("");
       
       
    }
    
    public function category(required struct rc){
                
	    param name="rc.maxresults" default='50';
        param name="rc.offset" default="0";
        
         rc.posts = new Query(sql="select * from Blog_Post P
                                  INNER JOIN Blog_Post_Category PC on P.PostID = PC.IDPost
                                  INNER JOIN Blog_Category C on C.IDCategory = PC.IDCategory
                                  WHERE C.category_slug = '#rc.slug#' and p.site_id = '#rc.config.siteID#' ORDER By P.Post_DateTime DESC").execute().getResult();
         
		 rc.MetaData.setMetaTitle( 'Blog / Category / #rc.posts.category_name#' );
		 
		 
		 
		 
    }
    
    void function archives( required struct rc ){
        rc.posts = new Query(sql="SELECT * FROM Blog_post P 
                                  INNER JOIN Blog_Post_Category PC on P.PostID = PC.IDPost
                                  INNER JOIN Blog_Category C on C.IDCategory = PC.IDCategory
                                  WHERE datepart(yy,P.post_Datetime)= #rc.Year# ORDER By P.Post_DateTime DESC").execute().getResult();

        rc.MetaData.setMetaTitle( 'Blog / Archives / #rc.Year#' );
    }
    
    public function post(required struct rc){
	  
		//rc.Post = variables.BlogService.getPostbySlug(slug=rc.slug);
		rc.Post = entityLoad("Post", {'slug': rc.slug});
	    /*rc.MetaData.setMetaTitle("Reading: #rc.Post.getTitle()#");
        rc.MetaData.setMetaDescription("");
        rc.MetaData.setMetaKeywords("");*/
	
	return rc.Post;
	    
    }
    
    void function rss(required struct rc) {
		rc.Posts = variables.BlogService.getPosts(published = true, siteid=#rc.config.siteID#);
	}
}
