/**
 * Created by jharv on 1/23/2016.
 */
component accessors = true extends = "abstract"{
    property name = "EventsService" setter = true getter = false;

    public function before(required struct rc){
       rc.DefaultDate = Fix( Now() );
       rc.date = rc.DefaultDate;
       //Get the First Day of the Month:
       rc.dtThisMonth = CreateDate(Year( Now() ), Month( Now() ),1);
       rc.dtPrevMonth = DateAdd( "m", -1, dtThisMonth );
	   rc.dtNextMonth = DateAdd( "m", 1, dtThisMonth );
	   //Get the Last Day of the Month:
	   rc.dtLastDayOfMonth = (dtNextMonth - 1);
	   //First Day of Week:
	   rc.dtFirstDay = (rc.dtThisMonth - DayOfWeek( dtThisMonth ) + 1);
       //Last Day of Week:
       rc.dtLastDay = (rc.dtLastDayOfMonth + 7 - DayOfWeek( rc.dtLastDayOfMonth ));

       //Get the Events:
       rc.objEvents = GetEvents(rc.dtFirstDay, rc.dtLastDay );s
    }
    
    
    public function default(required struct rc) {
	    param name="rc.year" default="";
	    param name="rc.month" default="";
	    param name="rc.day" default="";



       rc.Events = variables.EventsService.getEvents(siteID='#rc.config.siteID#');

	   rc.MetaData.setMetaTitle("CF Events Calendar");
       rc.MetaData.setMetaDescription('CF Events Calender');
       rc.MetaData.setMetaKeywords("CF Events Calendar");
	         
    }


}
