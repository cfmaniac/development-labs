
<cfoutput>
	 <div class="row">
                
                <div class="col-md-12">
                    <div class="text-center">
                    
                    <h1>Your Search Results for <em>#rc.SearchTerm#</em> in <em>#rc.SearchCity.City# (#rc.SearchZip#)</em> 
                   
				    </h1>
                    
                    </div>
                </div>
                
          </div>
          <div class="gap"></div>
          
	 <cfif #rc.ListingResults.recordcount# EQ "0">
	    <p>We didn't find any results for your search <em>#rc.SearchTerm#</em> in <em>#rc.SearchCity.City# (#rc.SearchZip#)</em>.
	   
	    </p>
	<cfelse>
	    <div class="row">
	    <cfloop query="#rc.ListingResults#">
		 
				<a class="col-sm-3" href="#buildURL(action='coupons.product', querystring="prodID=#rc.ListingResults.listingid#")#">
				<div class="product-thumb">
                    <header class="product-header">
                    <cfif fileExists('#rc.ListingsFolder#/#rc.ListingResults.ListingImage#')>
                       <img class="img-responsive" src="/globals/#rc.sitename#/coupons/listings/#rc.ListingResults.ListingImage#" alt="Yogurtland">                                
                    </cfif>
                    </header>
                    <div class="product-inner">
                        <h5 class="product-title">
                            #rc.ListingResults.ListingName#
                        </h5>
                        <p class="product-desciption">
                        <!---#rc.ListingResults.ListingDescription#--->
                        </p>
                        
                        <div class="product-meta">
                        <span class="product-time"></span>
                            <ul class="product-price-list">
                                <li><span class="product-price">View Coupon</span>
                                </li>
                            </ul>
                        </div>
                        <p class="product-location"><i class="fa fa-map-marker"></i> #City#, #State#</p>
                        
                    </div>
                </div>
		        </a>
		 
	    </cfloop>
	    </div> 
	     
    </cfif>
	<div class="gap"></div>
	
	
</cfoutput>