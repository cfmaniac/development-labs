<cfoutput>
	 <div class="row">
                
                <div class="col-md-12">
                    <div class="text-center">
                    
                    <h1>All Coupons available in 
                    <cfif structKeyExists(cookie, 'UserCity') && structKeyExists(cookie, 'UserState') && structKeyExists(cookie, 'UserZip')>
					    <strong>#cookie.UserCity#, #cookie.UserState#</strong>
				    <cfelse>
				        <strong>#rc.config.City#, #rc.Config.State#</strong>
				    </cfif>
				    </h1>
                    
                    </div>
                </div>
                
          </div>
          <div class="gap"></div>
          
	 <cfif #rc.Listings.recordcount# EQ "0">
	    <p>We didn't find any deals in your Area of 
	    <cfif structKeyExists(cookie, 'UserCity') && structKeyExists(cookie, 'UserState') && structKeyExists(cookie, 'UserZip')>
		    <strong>#cookie.UserCity#, #cookie.UserState#</strong>
	    <cfelse>
	        <strong>#rc.config.City#, #rc.Config.State#</strong>
	    </cfif>
	    </p>
	<cfelse>
	    <div class="row">
	    <cfloop query="#rc.Listings#" startrow="1" endrow="12">
		 
				<a class="col-sm-3" href="#buildURL(action='coupons.product', querystring="prodID=#rc.listings.listingid#")#">
				<div class="product-thumb">
                    <header class="product-header">
                    <cfif fileExists('#rc.listingsFolder#/#rc.Listings.ListingImage#')>
                       <img class="img-responsive" src="/globals/#rc.sitename#/coupons/listings/#rc.Listings.ListingImage#" alt="Yogurtland">                                
                    </cfif>
                    </header>
                    <div class="product-inner">
                        <h5 class="product-title">
                            #rc.listings.ListingName#
                        </h5>
                        <p class="product-desciption">
                        <!---#rc.Listings.ListingDescription#--->
                        </p>
                        
                        <div class="product-meta">
                        <span class="product-time"></span>
                            <ul class="product-price-list">
                                <li><span class="product-price">View Coupon</span>
                                </li>
                            </ul>
                        </div>
                        <p class="product-location"><i class="fa fa-map-marker"></i> #City#, #State#</p>
                        
                    </div>
                </div>
		        </a>
		 
	    </cfloop>
	    </div> 
	     
    </cfif>
	<div class="gap"></div>
	
	
</cfoutput>