
<cfimport taglib="/lib/replicator/barcodes/" prefix="ui"/>
<cfoutput>
<!---cfdump var="#rc.ProductInfo#"--->
<div class="gap"></div>
<div class="row">
	
	
	<h2>#rc.ProductInfo.ListingName#</h2>
	<div class="row">
	    	
		<cfif fileExists(expandPath('/globals/#rc.siteName#/coupons/logos/#rc.ProductInfo.companyid#/#rc.ProductInfo.companyLogo#'))>
			<!---Store Logo--->
		<div class="col-sm-3">
			<div class="thumbnail-img">
			<div class="overflow-hidden">
				<img style="width: 350px;" class="img-responsive" src="/globals/#rc.siteName#/coupons/logos/#rc.ProductInfo.companyid#/#rc.ProductInfo.companyLogo#" alt="#rc.ProductInfo.Name#" title="#rc.ProductInfo.Name#">
			</div>
			</div>
		</div>
		<!---End Store Logo--->	
		</cfif>
			
		
		<div class="col-sm-6">
        <p>Offered by: <a href="#buildURL(action='coupons.profile', querystring='slug/#rc.ProductInfo.slug#')#">#rc.ProductInfo.Name#</a></p>
		<cfif structKeyExists(rc, 'CurrentUser')>
		<div class="alert-success">Code: <strong>#rc.ProductInfo.listingCode#</strong></div>	
		<cfelse>
		<div class="alert-danger"><a class="popup-text" href="##login-dialog" data-effect="mfp-move-from-top" style="color:##a94442; font-weight: bold;">Login</a> or 
		<a class="popup-text" href="##register-dialog" data-effect="mfp-move-from-top" style="color:##a94442; font-weight: bold;">SignUp</a> to View Coupon Code</div>		
		</cfif>	
		<p>Valid Through: #dateformat(rc.ProductInfo.ListingStarted, "MM/DD/YYYY")# - #dateformat(rc.ProductInfo.ListingEnds, "MM/DD/YYYY")#</p>
		</div>
		
		
	</div>
	<div class="row">
		<div class="col-sm-12">
			#rc.ProductInfo.ListingDescription#
		</div>	
	</div>
	<div class="gap"></div>
	
	<!---Email List Subscription--->
	<cfif structKeyExists(rc, 'CurrentUser')>
	    <div class="alert alert-info">
	    <strong>Like this Offer?</strong> <a href="#buildURL(action='coupons.subscribe', querystring="company=#rc.ProductInfo.companyid#")#">Subscribe</a> to <em>#rc.ProductInfo.Name#'s</em> Email list and get similiar offers!
	    </div>
	<div class="gap"></div>    
	</cfif>
	<!--- End Email List Subscription --->
	
	
	<div class="row">
		<h2 class="noPrint">Print This Coupon</h2>
		<div class="well">
			<div class="print_coup yesPrint">
				<div class="text-center">
				<cfif fileExists('#rc.listingsFolder#/#rc.ProductInfo.ListingImage#')>
				<div class="thumbnail-img">
				<div class="overflow-hidden">
				<img class="img-responsive" src="/globals/#rc.sitename#/coupons/listings/#rc.ProductInfo.ListingImage#" alt="#rc.ProductInfo.ListingName#" title="#rc.ProductInfo.ListingName#">
				</div>
				</div>
				</cfif>
				
		        <h1>#rc.ProductInfo.ListingName#</h1>
		        <cfif structKeyExists(rc, 'CurrentUser')>
					<div class="row">
					<ui:barcode root="/lib/replicator/barcodes" type="code128" class="img-responsive" height="100">
					   #rc.ProductInfo.listingCode#
					</ui:barcode>
					
					</div>
				</cfif>
							
				<div class="row">#rc.ProductInfo.ListingDescription#</div>
				
				<div class="row">
				<span>Expires on:</span> <strong class="expired">#dateformat(rc.ProductInfo.ListingEnds, "Full")#</strong>
				</div>
							
				</div>
				
			</div>
			<div class="text-center">	<input type="button" value="Print Coupon" class="btn btn-primary noPrint" onclick="window.print();"></div></div>
		</div>
	</div>
	
	
</div>
</cfoutput>