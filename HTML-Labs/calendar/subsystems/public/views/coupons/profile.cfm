<cfoutput>
<!---cfdump var="#rc.ProductInfo#"--->
<div class="gap"></div>
<div class="row">
	
	<h2>#rc.CompanyProfile.Name#</h2>
	<div class="row">
	    
	    <!--Show Map and Address-->
	    <div class="col-sm-9">
	       <cfif fileExists(expandPath('/globals/#rc.sitename#/coupons/logos/#rc.CompanyProfile.companyid#/#rc.CompanyProfile.companyLogo#'))>
			<!---Store Logo--->
		
			<div class="thumbnail-img">
			<div class="overflow-hidden">
				<img style="width: 350px;" class="img-responsive" src="/globals/#rc.sitename#/coupons/logos/#rc.CompanyProfile.companyid#/#rc.CompanyProfile.companyLogo#" alt="#rc.CompanyProfile.Name#" title="#rc.CompanyProfile.Name#">
			</div>
			</div>
		</cfif>
		<!---End Store Logo--->
		<cfif len(rc.CompanyProfile.CompanyPhone)>
		<p><strong>Phone:</strong> #phoneFormat(rc.companyProfile.CompanyPhone, "(xxx) xxx-xxxx")#</p>	
		</cfif>
		<cfif len(rc.CompanyProfile.CompanyFax)>
		<p><strong>Fax:</strong> #phoneFormat(rc.companyProfile.CompanyFax, "(xxx) xxx-xxxx")#</p>	
		</cfif>
		<cfif len(rc.CompanyProfile.YearEstablished)>
			<p><strong>Established:</strong> #rc.CompanyProfile.YearEstablished#</p>
		</cfif>
		<cfif len(rc.CompanyProfile.URL)>
			<p><strong>Website:</strong> <a href="#rc.companyProfile.URL#" target="_blank">#rc.companyProfile.URL#</a></p>
		</cfif>
	        
	       <cfif #len(rc.CompanyProfile.Description)#>
			#rc.CompanyProfile.Description#
		   </cfif>  
		   
		   <hr>
		   <div class="gap"></div>
	<div class="row">
		<h2>#rc.CompanyProfile.Name#'s Offerings</h2>
		<cfif rc.CompanyListings.recordcount GT "0">
			<cfloop query="#rc.CompanyListings#">
				
				<a class="col-sm-3" href="#buildURL(action='coupons.product', querystring="prodID=#rc.CompanyListings.listingid#")#">
				<div class="product-thumb">
                    <header class="product-header">
                    <cfif fileExists('#rc.listingsFolder#/#rc.CompanyListings.ListingImage#')>
                       <img class="img-responsive" src="/globals/#rc.sitename#/coupons/listings/#rc.CompanyListings.ListingImage#" alt="Yogurtland">                                
                    </cfif>
                    </header>
                    <div class="product-inner">
                        <h5 class="product-title">
                            #rc.CompanyListings.ListingName#
                        </h5>
                        <p class="product-desciption">
                        #rc.CompanyListings.ListingDescription#
                        </p>
                        
                        <div class="product-meta">
                        <span class="product-time"></span>
                            <ul class="product-price-list">
                                <li><span class="product-price">View Coupon</span>
                                </li>
                            </ul>
                        </div>
                        <!---p class="product-location"><i class="fa fa-map-marker"></i> Boston</p--->
                        
                    </div>
                </div>
		        </a>
			</cfloop>
		<cfelse>
			#rc.CompanyProfile.Name# has no Offering at the moment
		</cfif>
	</div>  
	    </div>
	    <div class="col-sm-3">
	        <cfif len(rc.CompanyProfile.addressStreet)>
	        <h2>Our Address</h2>
	        #rc.CompanyProfile.addressStreet#<br>
	        <cfif len(rc.CompanyProfile.addressStreet2)>#rc.CompanyProfile.addressStreet2#<br></cfif>
	        #rc.CompanyProfile.addresscity#, #rc.CompanyProfile.addressstate#
	        <hr>
	        </cfif>
	        
	        <cfif len(rc.CompanyProfile.showMap)>
	        <h2>Our Map</h2>
            <div id="map_canvas" style="height: 350px;">
	            
            </div>
            <script type="text/javascript" src="http://maps.google.com/maps/api/js"></script>
<script type="text/javascript">
var geocoder;
var map;
var address = "<cfoutput>#rc.CompanyProfile.addressStreet# #rc.CompanyProfile.addresscity#, #rc.CompanyProfile.addressstate#</cfoutput>";

function initialize() {
  geocoder = new google.maps.Geocoder();
  var latlng = new google.maps.LatLng(-34.397, 150.644);
  var myOptions = {
    zoom: 14,
    center: latlng,
    mapTypeControl: true,
    mapTypeControlOptions: {
      style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
    },
    navigationControl: true,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
  if (geocoder) {
    geocoder.geocode({
      'address': address
    }, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
          map.setCenter(results[0].geometry.location);

          var infowindow = new google.maps.InfoWindow({
            content: '<b>' + address + '</b>',
            size: new google.maps.Size(150, 50)
          });

          var marker = new google.maps.Marker({
            position: results[0].geometry.location,
            map: map,
            title: address
          });
          google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(map, marker);
          });

        } else {
          alert("No results found");
        }
      } else {
        alert("Geocode was not successful for the following reason: " + status);
      }
    });
  }
}
google.maps.event.addDomListener(window, 'load', initialize);

	</script>
            </cfif>
	    </div>
	    
</div>
<div class="gap"></div>
</div>
</cfoutput>

