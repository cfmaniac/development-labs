<cfset rc.layout = "false">
<cfparam name="rc.debug" default="">
<cfparam name="rc.dump" default="">
<cfoutput>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Replicator Error">
<meta name="author" content="">
<title>#cgi.http_host# - An Error Occurred</title>
<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<style>
/* Error Page Inline Styles */
body {
  padding-top: 20px;
}
/* Layout */
.jumbotron {
  font-size: 21px;
  font-weight: 200;
  line-height: 2.1428571435;
  color: inherit;
  padding: 10px 0px;
}
/* Everything but the jumbotron gets side spacing for mobile-first views */
.masthead, .body-content, {
  padding-left: 15px;
  padding-right: 15px;
}
/* Main marketing message and sign up button */
.jumbotron {
  text-align: center;
  background-color: transparent;
}
.jumbotron .btn {
  font-size: 21px;
  padding: 14px 24px;
}
/* Colors */
.green {color:##5cb85c;}
.orange {color:##f0ad4e;}
.red {color:##d9534f;}
</style>
<script type="text/javascript">
  function loadDomain() {
    var display = document.getElementById("display-domain");
    display.innerHTML = document.domain;
  }
</script>
</head>
<body onload="javascript:loadDomain();">
<!--- [ Variables ] --->
<cfset executionTime = structKeyExists(rc.context, 'startTime')? getTickCount()-rc.context.startTime : ''>
<cfset formStructMaskList = 'cardNumber, cardName, cardSecurity, cardCVV2, password, password2'>
<cfset fwStruct = {
	subsystem 		= rc.subSystem,
	action 			= rc.failedAction,
	section	        = listFirst(listLast(rc.failedAction, ':'), '.'),
	item 			= listLast(rc.failedAction, '.'),
	layout			= structKeyExists(rc. 'layouts') && isArray(rc.layouts)? rc.layouts[1] : ''
}>
<cfset errorStruct = duplicate(rc.exception)>
<cfset sessionStruct = {
	cfid 			= isDefined("session") and structkeyExists(session, "cfid")? session.cfid : (isDefined('client.cfid')? client.cfid : ''),
	cfToken 		= isDefined("session") and structkeyExists(session, "cftoken")? session.cftoken :(isDefined('client.cftoken')? client.cftoken : ''),
	sessionID		= isDefined("session")? session.sessionID : ''
}>

<cfset cgiStruct    = duplicate(cgi) />
<cfset serverStruct = {
	hostname = CreateObject("java", "java.net.InetAddress").getLocalHost().getHostName(),
	ip       = CreateObject("java", "java.net.InetAddress").getLocalHost().getHostAddress()
} />
<cfset formStruct   = StructMaskKeys(duplicate(form), formStructMaskList) />
<cfset urlStruct    = StructMaskKeys(duplicate(url), formStructMaskList) />
<cfset cookieStruct = StructMaskKeys(duplicate(cookie), formStructMaskList) />

<!--- [ Struct Function: StructMaskKeys() ] --->
<cffunction name="StructMaskKeys" returntype="struct" output="no">
    <cfargument name="struct" type="struct" requried="true" />
    <cfargument name="keyList" type="string" requried="true" />
    <cfargument name="delimiters" type="string" required="false" default="," />
    
    <cfloop list="#arguments.keyList#" delimiters="#arguments.delimiters#" index="key">
        <cfif structKeyExists(arguments.struct, trim(key))>
			<cfset arguments.struct[trim(key)] = repeatString('*', len(arguments.struct[trim(key)] )) />
		</cfif>
    </cfloop>
    
    <cfreturn arguments.struct />
</cffunction>
<div class="container">
  <!-- Jumbotron -->
  <div class="jumbotron">
    <h1><i class="fa fa-exclamation-triangle orange"></i> An Error Ocurred</h1>
    <p class="lead">#errorStruct.cause.message# for <em><span id="display-domain"></span></em>.</p>
    <div class="row">
    <div class="col-sm-4">
	    <a href="##" onclick="window.history.go(-1); return false;" class="btn btn-info btn-lg text-center"><span class="green">Go Back</span></a>
  
    </div>
    <div class="col-sm-4">OR</div>
    <div class="col-sm-4">
	    <a href="javascript:document.location.reload(true);" class="btn btn-default btn-lg text-center"><span class="green">Try This Page Again</span></a>
  
    </div>
    </div>
    </div>
</div>
<div class="container">
  <div class="body-content">
    <div class="row">
      <div class="col-md-6">
        <h2>What happened?</h2>
        <cfif listFindNoCase('pro', getEnvironment())>
        <p class="lead">The page you rc.d cannot be found or resulted in an error. This page might have been removed, had its name changed, or is temporarily unavailable.</p>
	<p>Please try the following:
		<ul>
			<li>If you typed the page address in the Address bar, make sure that it is spelled correctly.</li>
			<li>Open the <a href="/">home page</a> and look for links to the information you want. </li>
			<li>Use the navigation bar to find the link you are looking for. </li>
			<li>Click the Back button to try another link or submit your rc.again. </li>
		</ul>
	</p>
	<cfabort>
	<cfelse>
	<p class="lead">
		<strong>#errorStruct.cause.message#</strong>
	</p>
	<!--- ERROR TYPE --->
	<cfif len(errorStruct.type)>
	<strong>Error Type: </strong> #errorStruct.type# : <cfif structKeyExists(errorStruct, 'cause') AND structKeyExists(errorStruct.cause, 'errorCode') AND len(errorStruct.cause.errorCode)>#errorStruct.cause.errorCode#<cfelse>[N/A]</cfif><br />
	</cfif>
	
	<!--- ERROR EXCEPTIONS --->
	<cfif isStruct(errorStruct) >
		<strong>Error Messages:</strong>
		#errorStruct.cause.message#<br />
		<cfif len(errorStruct.detail)>
			#errorStruct.detail#<br />
	 	</cfif>
	 	<cfif len(rc.exception.detail) neq 0>
		 	#errorStruct.cause.detail#
		 </cfif>
	</cfif>
	
	</cfif>
	
      </div>
      <div class="col-md-6">
        <h2>Tag Context: <div style="float:right">[#arrayLen(createObject("java","java.lang.Exception").init().tagContext)#]</h2>
        <cfloop array="#errorStruct.tagContext#" index="item">
		<div class="row">
			<div class="col-sm-12">
				
						<cfloop collection="#item#" item="key">
							<cfif key neq "fieldnames">
							<div class="row">
								<div class="col-sm-3"><cfif key is "line"><strong>#key#:</strong><cfelse>#key#:</cfif>  </div>
								<div class="col-sm-9">#htmlEditFormat(item[key])#</div>
							</div>
							</cfif>
						</cfloop>
			</div>
		</div>
	</cfloop>
	
	</div>
    </div>
    <cfdump var="#rc.config#">
  </div>
</div>

<!--Scripts-->
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</body>
</html>
</cfoutput>