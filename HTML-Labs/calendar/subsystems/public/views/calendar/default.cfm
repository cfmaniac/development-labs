<cfsavecontent variable="rc.htmlHead">
    <cfoutput>
	
	</cfoutput>
</cfsavecontent>
<!---cfset htmlHeader(rc.htmlHead)--->

<cfsavecontent variable="rc.htmlFoot">
	<cfoutput>
		<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js'></script>
		<script src='assets/js/fullCalendar/fullcalendar.min.js'></script>
		<!---Event Details Modal--->
		<div class="modal fade" id="eventDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
		      </div>
		      <div class="modal-body">
		        
		      </div>
		      <div class="modal-footer">
		        <!---button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		        <button type="button" class="btn btn-primary">Save changes</button--->
		      </div>
		    </div>
		  </div>
		</div>
		<!---End Event Details Modal--->
					
		<script type='text/javascript'>
		
		$('##eventsCalendar').fullCalendar( {
		    header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
		    timezone: 'America/New_York',
		    selectable: true,
		    weekMode: 'liquid',
			selectHelper: true,
			eventLimit: true, // allow "more" link when too many events
			events: [
			<cfif arrayLen(rc.Events)>
	        <cfloop array="#rc.Events#" index="event">
	        <cfset eventBody = #rereplace(event.getBody(), "#chr(13)#|#chr(9)#|\n|\r", "", "ALL")#>
	        <cfset eventBody = #rereplace(eventBody, "'", '"', "ALL")#>
				{
				title : "#event.getTitle()#",
				description : '#eventBody#',

				<cfif event.getAllDayEvent()>
		        allDay : true
		        <cfelse>
		        start : '#dateformat(event.getstartAt(), 'YYYY-MM-DD')#T#timeFormat(event.getStartTime(), "HH:MM")#',
				end: '#dateformat(event.getEndAt(), 'YYYY-MM-DD')#T#timeFormat(event.getEndTime(), "HH:MM")#',
		        allDay : false
				</cfif>
				},
	        </cfloop>
	        </cfif>
			],
			<cfif arrayLen(rc.Events)>
			eventClick: function(calEvent, jsEvent, view) {
		        $('##eventDetail' ).modal('show');
				$('##eventDetail').on('shown.bs.modal', function (e) {
				  $('.modal-title' ).html("Event Details: "+calEvent.title);
				  $('.modal-body' ).html(calEvent.description);
				  $('.modal-footer' ).html('Starts: #timeFormat(event.getStartTime(), "HH:MM TT")# Ends #timeFormat(event.getEndTime(), "HH:MM TT")# (Times are EST)');
				});
		        //alert('Event: ' + calEvent.title);
		        //alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
		        //alert('View: ' + view.name);
		        // change the border color just for fun
		        $(this).css('border-color', 'red');
		
		    }
			</cfif>
    
		});
		</script>
	</cfoutput>
</cfsavecontent>
<!---cfset htmlFooter(rc.htmlfoot)--->


<!---<div id="main-content">
	<h3>Our Events</h3>
	<div class="scrollContent mCustomScrollbar" data-mcs-theme="rounded-dark" data-mcs-positon="outside">
	<cfoutput>
	
	<div id="eventsCalendar"></div>
	
	
	</cfoutput>
	
	</div>
</div>--->
	<cfoutput>
	
	<div id="eventsCalendar"></div>
	
	
	</cfoutput>
