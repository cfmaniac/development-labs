Files and folders here contain empty files that have corresponding views in your
template /views/ folder, that control the actual look and feel of your website.

All the views correspond to a ReplicatorX module.