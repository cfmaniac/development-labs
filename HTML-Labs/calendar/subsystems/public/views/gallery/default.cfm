<style>
.profile-header-container{
    margin: 0 auto;
    text-align: center;
}

.profile-header-img {
    padding: 54px;
}

.profile-header-img > img.img-circle {
    width: 150px;
    height: 150px;
    border: 2px solid #CF9D3E;
}

.profile-header {
    margin-top: 43px;
}

/**
 * Ranking component
 */
.rank-label-container {
    margin-top: -19px;
    /* z-index: 1000; */
    text-align: center;
    
}

.label.label-default.rank-label {
    background-color: #CF9D3E;
    padding: 5px 10px 5px 10px;
    border-radius: 27px;
    font-size: 1.0em;
}
</style>
<cfsavecontent variable="rc.htmlFoot">
	<script>
		//Gallery Items:
		$('.gallery_item').on('click', function(){
		  $(this).closest('.gallery_item').toggleClass('col-sm-6');
		  $(this).closest('.gallery_item').find(".gallery-img" ).toggleClass('img-circle');
		  $(this).closest('.gallery_item').find(".caption" ).toggleClass('hidden');
		  //alert('I was clicked');
		});
	</script>
</cfsavecontent>
<div id="main-content">
	<h3 class="windsong ws-lg siteColor">Our Gallery</h3>
	<div class="scrollContent mCustomScrollbar" data-mcs-theme="rounded-dark" data-mcs-positon="outside">
	<cfoutput>
	<cfif arraylen(rc.Photos)>
	<p>Click on a Picture to enlarge it and view it's caption</p>
	<div class="row">
	<cfloop array="#rc.Photos#" index="photo">
	<div class="col-sm-6 gallery_item">
	
	<div class="profile-header-container" >   
    		<div class="profile-header-img">
                <img class="gallery-img img-circle" src="/globals/#rc.siteName#/gallery/#photo.getfile()#" />
                <!-- badge -->
                <div class="rank-label-container">
                    <span class="label label-default rank-label">#photo.getName()#</span>
                    <div class="caption hidden">#photo.getCaption()#</div>
                </div>
            </div>
        </div> 
	</div>
		
	</cfloop>
	</div>
	<cfelse>
	<p>We don't have any Photos in our Gallery at the moment.</p>	
	</cfif>
	</cfoutput>
	
	</div>
</div>