<cfquery name="CurrentBilling" dbtype="query">
   select * from rc.CompanyBilling where BillingStatus='Due'
</cfquery>
<cfquery name="PastBilling" dbtype="query">
   select * from rc.CompanyBilling where BillingStatus='Paid' Order by BillingDate DESC
</cfquery>
<cfoutput>
 <h2>Your Account Billing</h2>
 <p>View Invoices and Billing History, Make Payments online.</p>
 
 <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="##current" role="tab" data-toggle="tab">Current Billing</a></li>
    <li role="presentation"><a href="##history" role="tab" data-toggle="tab">Billing History</a></li>
 </ul>
 
 <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="current">
      <h2>Current Billing <a href="" class="btn btn-primary pull-right">Download Invoice</a></h2>
      <cfif CurrentBilling.RecordCount GT "0">
         <p class="alert alert-warning">
             Your Account is coming up for Renewal. Your Due date is <strong>#dateformat(CurrentBilling.BillingDateDue, "MM/DD/YYYY")#</strong><br>
             You can ofcourse <a href="">Renew online</a> at any time. 
         </p>
      </cfif>
      <table class="table table-first-column-number data-table display full">
			<thead>
				<tr>
					<th>Term</th>
					<th>Price</th>
					<th>Status</th>
					<th class="center">Billing Date</th>
					<th class="center">Date Due By</th>
					<th class="center">Notes</th>
					<th class="center"></th>
				</tr>
			</thead>
			
			<tbody>
      <cfloop query="CurrentBilling">
         <tr>
         <td>#BillingTerm# Months </td>
         <td>#dollarFormat(billingPrice)# </td>
         <td>#billingStatus#</td>
         <td>#dateformat(BillingDate, "MM/DD/YYYY")#</td>
         <td><strong>#dateformat(BillingDateDue, "MM/DD/YYYY")#</strong></td>
         <td>#BillingNotes#</td>
         <td><cfif #Billingstatus# is "Due"><a href="" class="btn btn-info">Pay Now</a> </cfif></td>
         </tr>  
      </cfloop>
      </tbody>
      </table>
      </div>
      <div role="tabpanel" class="tab-pane" id="history">
      <h2>Billing History</h2>
       <table class="table table-first-column-number data-table display full">
			<thead>
				<tr>
					<th>Term</th>
					<th>Price</th>
					<th>Status</th>
					<th class="center">Billing Date</th>
					<th class="center">Paid Date</th>
					<th class="center">Notes</th>
				</tr>
			</thead>
			
			<tbody>
      <cfloop query="PastBilling">
         <tr>
         <td>#BillingTerm# Months </td>
         <td>#dollarFormat(billingPrice)# </td>
         <td>#billingStatus#</td>
         <td>#dateformat(BillingDate, "MM/DD/YYYY")#</td>
         <td>#dateformat(BillingPaidDate, "MM/DD/YYYY")#</td>
         <td>#BillingNotes#</td>
         </tr> 
      </cfloop>
      </tbody>
      </table>
      </div>
 </div>
</cfoutput>
<script type="text/javascript">
    $(function(){
     
   
       $('table.data-table.full').dataTable( {
            "dom": 'flrtip',
            "order": [ [2,'desc'], [1,'asc'] ],
            "paging":   true,
            "info": true,
            "scrollY": "375px",
            "pageLength": 75,
            "scrollCollapse": true,
            "lengthMenu": [[10, 25, 50, 75, 100 -1], [10, 25, 50,75,100, "All"]],
            "fnDrawCallback": function(o) {
               $('.dataTables_scrollBody').scrollTop(0);
            },
            "language": {
            "lengthMenu": "Display _MENU_ records per page",
            "zeroRecords": "Nothing found - sorry",
            "info": "Showing page _PAGE_ of _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtered from _MAX_ total records)"
        }   
        });
        
     $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
       e.target // activated tab 
       e.relatedTarget // previous tab 
       var table = $.fn.dataTable.fnTables(true);
       if (table.length > 0) {
           $(table).dataTable().fnAdjustColumnSizing();
       }
     });
   
   
   
    });
    
       
    });
</script>