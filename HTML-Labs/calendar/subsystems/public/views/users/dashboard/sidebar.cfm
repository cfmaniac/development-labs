<cfoutput>
   <section>
                <div class="panel panel-primary">
                    <div class="panel-heading"><i class="fa fa-dashboard"></i> Dashboard</div>
                    <div class="panel-body">
                        
                        <h4 class="section-title no-margin-top">Account Actions</h4>
                        <ul class="list-unstyled">
                            <li><a href="#BuildURL(action='users.dashboard')#">My Dashboard</a></li>
                            <li><br></li>
                           <li><a href="#BuildURL(action='users.dashboard', queryString='view=Traffic')#">Profile Traffic</a></li>
                           <li><a href="#BuildURL(action='users.dashboard', queryString='view=Info')#">My Information</a></li>
                           <li><a href="#BuildURL(action='users.dashboard', queryString='view=Listings')#">My Listings</a></li>
                           <li><br></li>
                           <li><a href="#BuildURL(action='users.dashboard', queryString='view=Settings')#">My Settings</a></li>
                           <li><a href="#BuildURL(action='users.dashboard', queryString='view=Users')#">My Users</a></li>
                           <li><a href="#BuildURL(action='users.dashboard', queryString='view=Billing')#">Billing & Invoices</a></li>
                           
                        <cfif #rc.User.getAdmin()#>
                           <li><a href="#BuildURL('admin~main')#">RN-1 Administration</a></li>
                        </cfif>    
                            <li><a href="#BuildURL('security.logout')#">Logout</a></li>
                        </ul>
                        
                        
                    </div>
                </div>
            </section>
</cfoutput>