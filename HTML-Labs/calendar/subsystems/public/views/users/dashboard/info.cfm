<cfimport taglib="/lib/replicator/" prefix="ui">
<cfoutput>
 <h2>Your Company Information</h2>
 <p>Update your Company Details, Contact Information and more.</p>
   <ul class="nav nav-tabs">
            <li class="nav-item">
              <a class="nav-link active" href="##one" data-toggle="tab">Information</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="##two" data-toggle="tab">Addresses</a>
            </li>
   </ul> 
   
   <form name="companyaddress" method="post">
     <div class="tab-content p-a-1 m-b-1">
            <div class="tab-pane active" id="one"> <h3>Merchant Information</h3>
                 <ui:form-row label="Company Name" name="name" value="#rc.CompanyInfo.name#">
                 <ui:form-row label="URL" infotext="your company Website" name="url" value="#rc.CompanyInfo.url#">
                 <ui:form-row label="Email Address" name="email" value="#rc.CompanyInfo.Email#"> 
                 
            </div>
     
            <div class="tab-pane" id="two"> <h3>Locations</h3>
                <ui:form-row label="Site Address" name="address" value="#rc.CompanyAddress.AddressStreet#">
		        <ui:form-row label="Site Address (additional)" name="address2" value="#rc.CompanyAddress.AddressStreet2#">
		        <ui:form-row label="Site City" name="city" value="#rc.CompanyAddress.AddressCity#">
		        <ui:form-row type="state" label="Site State" name="state" value="#rc.CompanyAddress.AddressState#">
		        <ui:form-row label="Site ZipCode" name="zipcode" value="#rc.CompanyAddress.AddressZip#">
            
            <hr>
            <h4>Location Contact Information</h4>
                 <ui:form-row label="Company Phone" name="phone" value="#rc.CompanyAddress.CompanyPhone#">
                 <ui:form-row label="Company Mobile" name="mobile" value="#rc.CompanyAddress.CompanyMobile#">
                 <ui:form-row label="Company Fax" name="fax" value="#rc.CompanyAddress.CompanyFax#">
                 <ui:form-row label="Location Email" name="email" value="#rc.CompanyAddress.CompanyEmail#">
            </div>
     
     </div>       
   
   <div class="gap"></div>
   <div class="row">
        <button type="button" class="btn btn-primary pull-right">Save Changes</button>
   </div>
   <div class="gap"></div>
   </form>
</cfoutput>