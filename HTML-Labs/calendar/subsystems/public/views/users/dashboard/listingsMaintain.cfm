<cfimport taglib="/lib/replicator/" prefix="ui">
<cfif cgi.rc.method is "post">
	<cfif structKeyExists(form, "listingImage") and form.listingImage NEQ "">
		<cffile action="upload" fileField="listingImage" destination="#expandPath('./globals/#rc.siteName#/coupons/listings/')#" nameConflict="unique">
	</cfif>
	
    <cfscript>
	    param name ="rc.siteId" default=1;
        param name ="rc.companyID" default="1";
        param name ="rc.listingName" default="";
        param name ="rc.listingdescription" default="";
        param name ="rc.listingimage" default="";
        param name ="rc.listingcode" default="";
        param name ="rc.listingStarted" default="";
        param name ="rc.listingEnds" default="";
        param name ="rc.listingActive" default="1";
        
        SaveListing = new Query();
        if(rc.context is "update"){
			
			//writeDump(var='#form#', abort="true");              
			SaveListing.addParam(name="listingID", value="#rc.id#", cfsqltype="CF_SQL_INTEGER");
			
			if(structKeyExists(form,"listingImage") && form.listingImage !=""){
				ListingSQL = "Update Companies_Listings SET listingName=:listingName, listingDescription=:listingDescription, listingCode=:ListingCode, ListingStarted=:ListingStarted,
			              ListingEnds=:ListingEnds, listingImage=:listingImage, listingActive=:ListingActive, companyID=:companyID where listingID =:listingID";
			
			    SaveListing.addParam(name="listingImage", value="#CFFile.ClientFile#", cfsqltype="CF_SQL_VarChar" );
			
			} else {
				ListingSQL = "Update Companies_Listings SET listingName=:listingName, listingDescription=:listingDescription, listingCode=:ListingCode, ListingStarted=:ListingStarted,
			              ListingEnds=:ListingEnds, companyID=:companyID where listingID =:listingID";
			}              
        } else {
	        //writeDump(var='#form#', abort="true");	        
	          
	        if(structKeyExists(form,"listingImage") && form.listingImage !=""){
			ListingSQL = "INSERT into Companies_Listings (listingName, ListingDescription, ListingCode, ListingImage, ListingStarted, ListingEnds, ListingActive, CompanyID, Site_ID) VALUES 
	                      (:listingName, :ListingDescription, :ListingCode, :ListingImage, :ListingStarted, :ListingEnds, :ListingActive, :CompanyID, :siteID)";
			SaveListing.addParam(name="listingImage", value="#CFFile.ClientFile#", cfsqltype="CF_SQL_VarChar" );
			
			} else {
				ListingSQL = "INSERT into Companies_Listings (listingName, ListingDescription, ListingCode, ListingStarted, ListingEnds, ListingActive, CompanyID, Site_ID) VALUES 
	                      (:listingName, :ListingDescription, :ListingCode,  :ListingStarted, :ListingEnds, :CompanyID, :ListingActive,:siteID)";
			}                   
        }
        
       
        SaveListing.addParam(name="siteID", value="#rc.config.siteID#", cfsqltype="CF_SQL_INTEGER");
        SaveListing.addParam(name="companyID", value="#rc.companyID#", cfsqltype="CF_SQL_INTEGER");
        SaveListing.addParam(name="listingName", value="#rc.listingName#", cfsqltype="CF_SQL_VARCHAR");
        SaveListing.addParam(name="listingDescription", value="#rc.listingdescription#", cfsqltype="CF_SQL_VARCHAR");
        SaveListing.addParam(name="listingcode", value="#rc.listingcode#", cfsqltype="CF_SQL_VARCHAR");
        SaveListing.addParam(name="listingStarted", value="#rc.listingStarted#", cfsqltype="CF_SQL_DATETIME");
        SaveListing.addParam(name="listingEnds", value="#rc.listingends#", cfsqltype="CF_SQL_DATETIME");
        SaveListing.addParam(name="listingActive", value="#rc.listingActive#", cfsqltype="CF_SQL_BOOLEAN");
        
        //writeDump(var=#form#, abort=true);
        
        rc.ListingSaves = SaveListing.execute(sql=ListingSQL).getResult();
        //variables.fw.redirect("stores.listings/message/501");
        location(url='#BuildURL(action='users', queryString='view=Listings/message/501')#', addtoken='false'); 
</cfscript>

</cfif>

<cfscript>
     rc.listingsFolder = expandPath( "/contents/listings/" );
	if(structkeyexists(rc, 'id')){
		rc.context = "update";
		rc.CompanyListings = new Query( sql = "select * from Companies_listings where listingid=#rc.id#" ).execute( ).getResult( );
	} else{
		rc.context = "create";
		rc.CompanyListings = {
			listingid = '',
			companyid = '#rc.User.getCompany( )#',
			listingname = 'New Listing',
			listingImage = '',
			listingdescription = '',
			listingcode = '',
			listingStarted = '#dateFormat(now(), "MM/DD/YYYY")#',
			listingEnds = '#dateFormat(now()+30, "MM/DD/YYYY")#',
		    listingactive = '1'
		};
		rc.listingName='New Listing';
	}
</cfscript>
<cfoutput>
<div class="row" style="margin-top: 40px; margin-bottom: 80px;">
		 <div class="col-sm-8"><h2>#rc.CompanyListings.listingName#</h2></div>
         <div class="col-sm-4">
         <a href="#BuildURL(action='users', queryString='view=Listings')#" class="btn btn-success btn-success-outline pull-right">Back to Coupons</a>
         <cfif rc.context is "update">
         
            <a href="" class="btn btn-danger btn-danger-outline pull-right">Delete</a>
         </cfif>
         </div>
</div>
<div class="row">
<form action="" method="post" class="form-horizontal" id="ListingsForm" enctype="multipart/form-data">
	<input type="hidden" name="context" value="#rc.context#">
	<input type="hidden" name="companyID" value="#rc.User.getCompany()#">
	<cfif rc.context is "update">
		<input type="hidden" name="id" value="#rc.id#">
		
	</cfif>
	<div class="row">
		<p>The Listing will be placed within the Zipcode located in your Store Front-Front Preferences.</p>
		<div class="col-sm-6">
			
			
			<ui:form-row id="title" label="Coupon Title" name="listingname" value="#rc.CompanyListings.listingname#">
			<ui:form-row id="description" type="htmlfull" label="Listing Description" name="listingdescription" value="#rc.CompanyListings.listingDescription#">
			
			<div class="form-group">
				<label for="page-content">Coupon Image </label>
				<input type="file" class="form-control" name="listingImage">
				<cfif rc.context is "update" and fileExists('#expandpath('./globals/store/listings/#rc.CompanyListings.ListingImage#')#')>
				<img src="/globals/store/listings/#rc.CompanyListings.ListingImage#" width="275" height="275">
				</cfif>
			</div>
			
			<ui:form-row id="code" label="Coupon Code" name="listingcode" value="#rc.CompanyListings.listingcode#">
			<ui:form-row type="date" label="Coupon Starts" name="listingStarted" value="#rc.CompanyListings.ListingStarted#"> 
			<ui:form-row type="date" label="Coupon Expires" name="listingEnds" value="#rc.CompanyListings.ListingEnds#"> 
			
			<ui:input label="Listing Active" type="boolean" name="ListingActive" value="#rc.CompanyListings.ListingActive#">
		</div>
		<div class="col-sm-5 text-center pull-right">
		
			<h3>Coupon Preview</h3>
			
			<div class="coupon" style="border: 1px dashed ##000; border-radius: 5px; background: ##fff; margin-left: auto; margin-right: auto; text-align: center;">
			<cfif fileExists('#rc.listingsFolder#/#rc.CompanyListings.ListingImage#')>
				<div class="thumbnail-img">
				<div class="overflow-hidden">
				<img class="img-responsive" src="/globals/store/listings/#rc.CompanyListings.ListingImage#" alt="#rc.CompanyListings.ListingName#" title="#rc.CompanyListings.ListingName#">
				</div>
				</div>
			</cfif>
				<h2 id="previewTit">#rc.CompanyListings.listingname#</h2>
				<p id="previewDes">#rc.CompanyListings.listingDescription#</p>
				<p id="preivewDates">Expires on: <span id="StartDate">#dateformat(rc.CompanyListings.ListingStarted, "MM/DD/YYYY")#</span> - <span id="EndDate">#dateformat(rc.CompanyListings.ListingEnds,"MM/DD/YYYY")#</span></p>
			</div>
			
			
			
			
				
			
		</div>
	</div>
	<div class="row">
		<input type="submit" class="btn btn-primary pull-right" value="Save Listing">
	</div>
	</form>
	</div>
	<div class="gap"></div> 
	<cfsavecontent variable="rc.htmlFoot">
	<cfoutput>
	<script src="/admin/assets/js/ckeditor/ckeditor.js"></script>
	<script src="assets/js/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script> <!-- Bootstrap datepicker -->
    <script src="assets/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script> <!-- Timepicker -->
	<script>
	 /**
   * jQuery datepicker plugin wrapper for compatibility
   */
  $.fn.plusDatePicker = function () {
      if (! this.length) return;

      if (typeof $.fn.datepicker != 'undefined') {
        this.datepicker();
      }
  };
  
  $('.datepicker').plusDatePicker();
  
  /**
  * jQuery timepicker plugin wrapper for compatibility
 */
  $.fn.plusTimePicker = function () {
      if (! this.length) return;

      if (typeof $.fn.datepicker != 'undefined') {
          this.timepicker({
              minuteStep: 5,
              showInputs: false,
              disableFocus: true,
              icons: {
                  up: 'fa fa-chevron-up',
                  down: 'fa fa-chevron-down'
              }
          });
      }
  };
  
  $('.timepicker').plusTimePicker({
		minuteStep: 5,
		showInputs: false,
		disableFocus: true,
	});
	
	$('input[name="listingname"]' ).on('keyup', function(){
      titleVal = $(this ).val();
      $('##previewTit' ).html(titleVal);
      }); 
      $('input[name="listingStarted"]' ).on('change', function(){
      startVal = $(this ).val();
      $('##StartDate' ).html(startVal);
      });
      
      $('input[name="listingEnds"]' ).on('change', function(){
      endVal = $(this ).val();
      $('##EndDate' ).html(endVal);
      });
	</script>
	</cfoutput>
	</cfsavecontent>
</cfoutput>