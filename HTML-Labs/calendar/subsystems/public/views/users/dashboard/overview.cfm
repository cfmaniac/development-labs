<cfoutput>
 <div class="row" style="margin-top: 40px; margin-bottom: 80px;">
		 <div class="col-sm-8"><h2>Your Account Dashboard, #rc.User.getName()#</h2></div>
         <div class="col-sm-4">
         <cfif rc.currentUser.getAdmin()>
         <a href="#buildURL('admin~main')#" class="btn btn-info" target="_blank">Control Panel</a>
         </cfif>
         <cfif rc.modules.coupons.enabled && rc.CurrentUser.getgroup() EQ "6" || rc.modules.coupons.enabled && rc.currentUser.getAdmin()>
          <a href="#buildURL(action='users', queryString='view=ListingsMaintain')#" class="btn btn-success">Coupon Creator&trade;</a>
         </cfif>
         <a href="#buildURL('security.logout')#" class="btn btn-danger pull-right">Logout</a>
         </div>
     
     
 </div>
 

 <div class="row-fluid">
   <h2>Profile Quick Links</h2>
   <p>From Here you are able to manage your account and virtually all aspects of your Profile.</p>   
   <div class="col-sm-4">
        <h5>My Profile</h5>
       <ul>
         <li><a href="#BuildURL(action='users', queryString='view=Traffic')#">Profile Traffic</a></li>
         <li><a href="#BuildURL(action='users', queryString='view=Info')#">My Information</a></li>
         <cfif  rc.CurrentUser.getgroup() EQ "6" && rc.modules.coupons.enabled || rc.currentUser.getAdmin()> 
         <li><a href="#BuildURL(action='users', queryString='view=Favs')#">My Favorites</a></li>
         </cfif>
         <li><a href="#BuildURL(action='users', queryString='view=Settings')#">My Settings</a></li>
         <cfif rc.modules.coupons.enabled>
         <li><a href="#buildURL(action='users', querystring='view=messages')#">My Email Subscriptions</a></li>
         </cfif>    
       </ul>
   </div>
   
   <cfif rc.modules.coupons.enabled && rc.CurrentUser.getgroup() EQ "6" || rc.modules.coupons.enabled && rc.currentUser.getAdmin()>
	   <cfinclude template="/public/views/coupons/includes/dashboard.cfm">
   </cfif>
   
 </div>

 
   <script>
       $(function () {
    $('##Traffic').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Profile Views for #dateformat(now(), "YYYY")#'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [
                'Jan',
                'Feb',
                'Mar',
                'Apr',
                'May',
                'Jun',
                'Jul',
                'Aug',
                'Sep',
                'Oct',
                'Nov',
                'Dec'
            ]
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Visitors'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Visitors',
            data: [49, 71, 106, 129, 144, 176, 135, 148, 216, 194, 95, 0]

        }]
    });
});
       
   </script>
</cfoutput>