
<!---cfquery name="rc.TrafficDateResults" dbtype="query">
   SELECT DISTINCT Month(TrafficDate) AS month, YEAR(TrafficDate) AS year from rc.CompanyTraffic ORDER BY year DESC, month DESC
</cfquery--->
<cfoutput>
<script src="/public/layouts/#rc.config.template.site#/assets/js/charts/highcharts.js"></script>
 <h2>Your Profile Traffic</h2>
 <p>By Default we show the last 30-Days worth of traffic, however you can select a date range as well to see trends in traffic.</p>
 <!---div class="row-fluid">
     <div class="col-sm-6">
          Select From Date: <select name="startDate" class="form-control">
                            <cfloop query="rc.TrafficDateResults">
                              <cfset FixedMonth= #numberFormat(month,"0_")#>
                              <option value="#year#-#Fixedmonth#">#FixedMonth#-#year#</option>
                       
                            </cfloop>
                            </select>
     </div>
      <div class="col-sm-6">
          Select to Date: <select name="endDate" class="form-control">
                            <cfloop query="rc.TrafficDateResults">
                             <cfset FixedMonth= #numberFormat(month,"0_")#>
                              <option value="#year#-#Fixedmonth#">#FixedMonth#-#year#</option>
                              </cfloop>
                            </select>
     </div>
 </div--->
   <div class="row-Fluid">
       <div id="Traffic" class="col-sm-12">
           
       </div>
   </div>
<script>
    $(function () {
    $('##Traffic').highcharts({
        title: {
            text: 'Monthly Profile Views',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        yAxis: {
            title: {
                text: 'Visitors'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '##808080'
            }]
        },
        tooltip: {
            valueSuffix: ' Visitors'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: '2010',
            data: [7, 6, 9, 14, 18, 21, 25, 26, 23, 18, 13, 9]
        }, {
            name: '2011',
            data: [2, 0, 5, 11, 17, 22, 24, 24, 20, 14, 8, 2]
        }, {
            name: '2012',
            data: [9, 6, 3, 8, 13, 17, 18, 17, 14, 9, 3, 1]
        }, {
            name: '2013',
            data: [4, 4, 5, 8, 11, 15, 17, 16, 14, 10, 6, 4]
        }, {
            name: '2014',
            data: [5, 4, 5, 8, 11, 15, 7.0, 16, 1, 10, 3, 0]
        }]
    });
});
</script>
</cfoutput>
