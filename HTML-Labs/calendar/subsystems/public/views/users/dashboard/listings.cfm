<cfscript>
	rc.CompanyListings = new Query( sql = "select * from Companies_listings where CompanyID=#rc.User.getCompany( )#" ).execute( ).getResult( );
	if (structKeyExists(rc, 'deleteListing')){
		//WriteOutput('This would have deleted Listing #rc.listingID#');
		DeleteListing = new Query(sql="Delete from companies_listings where listingID=#rc.listingID#").execute().getResult();
		location(url='#BuildURL(action='users', queryString='view=Listings/message/502')#', addtoken='false');
	}
</cfscript>

<cfoutput>
<div class="row" style="margin-top: 40px; margin-bottom: 80px;">
		 <div class="col-sm-8"><h2>Your Store's Listings</h2></div>
         <div class="col-sm-4">
         <a href="#BuildURL(action='users', queryString='view=ListingsMaintain')#" class="btn btn-success btn-success-outline pull-right">Coupon Creator&trade;</a>
         </div>
     
     
 </div>
 
 #view('partials/messages')#
 
 <div class="row">
     <table id="datatable-example" class="table table-bordered table-striped table-hover table-sm">
        <thead>
          <tr>
			    <th class="">&nbsp;</th>
				<th class="">Listing Title</th>
				<th class="center">Start Date</th>
				<th class="center">End Date</th>
				<th class="center">Active</th>
				<th class="">&nbsp;</th>
				
			</tr>
        </thead>
        <tbody>
        <cfloop query="#rc.CompanyListings#">
            <tr>
	            <td class=""><img src="/globals/#rc.sitename#/coupons/listings/#rc.CompanyListings.ListingImage#" style="width:50px;height:50px;"></td>
	            <td class=""><a href="#buildURL(action='users', queryString='view=ListingsMaintain&id=#rc.CompanyListings.listingid#')#"><strong>#ListingName#</strong></a></td>
	            <td class="center">#dateFormat(listingStarted, "MM/DD/YYYY")#</td>
				<td class="center">#dateFormat(listingEnds, "MM/DD/YYYY")#</td>
				<td class="center">#yesNoFormat(ListingActive)#</td>
				<td class=""><a href="#buildURL(action='users', queryString='view=listings&deleteListing=true&listingid=#rc.CompanyListings.listingid#')#" class="btn btn-danger btn-rounded-deep pull-right"><i class="fa fa-trash-o"></i></td>
            </tr>
        </cfloop>
        </tbody>
    </table>    
 </div>
</cfoutput>