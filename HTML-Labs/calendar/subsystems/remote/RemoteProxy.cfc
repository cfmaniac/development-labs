component {
    /*
    This is a remote proxy CFC for the use of AJAX / and/or API rc.
    All functions within this should be remote functions;
    */
	remote any function logClientSideError(required string message, required string url, required numeric lineNumber) {
		/*if (StructKeyExists(application, "exceptionTracker")) {
			// Hoth expects a struct with the following keys
			local.exception = {
				detail = "[#arguments.url# (#arguments.lineNumber#)] #arguments.message#",
				type = "clientside",
				tagContext = "[#arguments.url# (#arguments.lineNumber#)]",
				// stack is used to identify unique errors so include lots of info!
				stackTrace = SerializeJSON(arguments),
				message = arguments.message
			};
			application.exceptionTracker.track(local.exception);
			return true;
		}*/
		return false;
	}
    
    remote any function keepalive(){
	    //This is the function to keep the Session Alive:
    }
}
