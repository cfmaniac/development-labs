<cfoutput>
	
  <div class="content-wrapper">
   <h1 class="page-heading">#rc.currentsite.sitename# Blog : Categories</h1>
    
    #view("partials/messages")#
    
    <div class="row">
   <div class="col-sm-12">
   <div class="card">
      <div class="card-block">
        <div class="media">
          <div class="media-body media-middle">
            <h4 class="m-b-0">Blog Categories</h4>
            <p class="text-muted m-b-0"></p>
          </div>
        </div>    
      </div>
      
      <div class="card-block p-t-0">
        <table id="datatable-example" class="table table-bordered table-striped table-hover table-sm">
        <thead>
          <tr>
            <th></th>
            <th>Name</th>
            <th></th>
            <th>Order</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
        <cfloop array="#rc.BlogCategories#" index="local.category">
        <tr>
		    <td>#local.category.GetIDCategory()#</td>
			<td><a href="#buildURL(action='blog.maintainCats', querystring='catId=#local.category.GetIDCategory()#')#">#local.category.getName()#</a></td>
			<td></td>
			<td>#local.category.getOrderIndex()#</td>
			<td><a href="#buildURL(action='blog.deleteCat', querystring='catId=#local.category.GetIDCategory()#')#" title="Delete" class="btn btn-danger btn-rounded-deep"><i class="fa fa-trash-o"></i></a></td>
			
		</tr>
		<cfscript>
		      rc.BlogSubCategories = EntityLoad('Category',{"parentID": '#local.category.getIDCategory()#'}, "OrderIndex Asc");
		    </cfscript>
		    <cfif arrayLen(#rc.BlogSubCategories#)>
		    <cfloop array="#rc.BlogSubCategories#" index="local.subcategory">
		    <tr>
		    <td>#local.category.GetIDCategory()#</td>
			<td><i class="fa fa-chevron-right"></i> <a href="#buildURL(action='blog.maintainCats', querystring='catId=#local.subcategory.GetIDCategory()#')#">#local.subcategory.getName()#</a></td>
			<td></td>
			<td>#local.subcategory.getOrderIndex()#</td>
			<td><a href="#buildURL(action='blog.deleteCat', querystring='catId=#local.subcategory.GetIDCategory()#')#" title="Delete" class="btn btn-danger btn-rounded-deep"><i class="fa fa-trash-o"></i></a></td>
		    </tr>
		    </cfloop>
		    </cfif>
		
		  	
		</cfloop>
        </tbody>
      </table>
      </div>
    </div>
    </div>
    
    </div>
  </div>
</cfoutput>    