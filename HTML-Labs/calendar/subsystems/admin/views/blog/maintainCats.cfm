<cfscript>
	rc.BlogCategories =EntityLoad('Category', {site_id='#rc.currentsite.siteID#', parentID='0'} , {});
	
	if( !StructIsEmpty( form ) ){
		if(form.context == "update"){
	    //Update:
	    Category = EntityLoadByPK( "Category", rc.id );
		Category.setName( form.title );
		Category.setslug( #ReReplace(LCase(form.slug), "[^a-z0-9]{1,}", "-", "all")# );
		Category.setOrderIndex( form.orderIndex );
		Category.setParentID( form.parentID );
		
		// transactions are fixed in 9.01
		transaction 
		{
			// update entity
			EntitySave( Category );
		}
		
		location( url="#buildURL(action='admin:blog.categories')#", addtoken="false" );
		
		} else {
	    //Add:
		Category = EntityNew( "Category" );
		
		// set properties
		Category.setName( form.title );
		Category.setslug( #ReReplace(LCase(form.title), "[^a-z0-9]{1,}", "-", "all")# );
		Category.setOrderIndex( form.orderIndex );
		Category.setParentID( form.parentID );
		Category.setSite_ID( rc.CurrentSite.siteID );

		// transactions are fixed in 9.01
		transaction 
		{
			// save the new category
			EntitySave( Category );
		}
		
		location( url="#buildURL(action='admin:blog.categories', queryString="message=Blog Category Successfully Added")#", addtoken="false" );
		}
		
	}
	
</cfscript>
<cfoutput>
	<div class="content-wrapper">
    <ol class="breadcrumb m-b-0">
      <li><a href="#BuildURL('')#">Home</a></li>
      <li>Blog</li>
      <li class="active">#rc.pageTitle#</li>
    </ol>
    
    #view("partials/messages")#
    <form action="" method="post" class="form-horizontal" id="page-form">
    <div class="row">
	   <div class="col-sm-12">
		   <div class="card">
		      <div class="card-block">
		        <div class="media">
		          <div class="media-body media-middle">
		            <h4 class="m-b-0">#rc.pageTitle#</h4>
		            <p class="text-muted m-b-0"></p>
		          </div>
		        </div>    
		      </div>
		      
		      <div class="card-block p-t-0">
		      <fieldset>
			<legend>Category</legend>

			<div class="form-group">
				<label for="title">Title </label>
				<input class="form-control" type="text" name="title" id="title" value="#HtmlEditFormat(rc.Category.getName())#" maxlength="100" placeholder="Title">
				
			</div>
			
			<cfif rc.context EQ "update">
			<div class="form-group ">
				<label for="title">Slug </label>
				<input class="form-control" type="text" name="slug" id="slug" value="#HtmlEditFormat(rc.Category.getSlug())#" maxlength="100" placeholder="Title">
				
			</div>	
			</cfif>
			
            <div class="form-group ">
				<label for="page-content">Parent Category </label>
				<select name="parentid" class="form-control">
				    <option value="0">None (Root Category)</option>
				    <cfloop array="#rc.BlogCategories#" index="local.category">
				    <option value="#local.category.getIDCategory()#" <cfif rc.context EQ "Update" && local.category.getIDCategory() EQ rc.Category.getParentID()>selected="selected"</cfif>>#local.category.getName()#</option>
				    </cfloop>
				
				</select>

			</div>
			
			<div class="form-group ">
				<label for="page-content">Order  </label>
				<select name="orderIndex" class="form-control">
				   <cfloop from="0" to="50" index="order">
				   <option value="#order#" <cfif rc.context EQ "Update" && local.category.getOrderIndex() EQ #order#>selected="selected"</cfif>>#order#</option>
				   </cfloop>
				</select>
				
			</div>
		</fieldset>
		      </div>
		      
		   </div>
	   </div>
	</div>
	 <div class="row pull-right">
		<input type="hidden" name="id" id="pageid" value="#HtmlEditFormat(rc.Category.getIDCategory())#">
		<input type="hidden" name="updated" id="updated" value="#dateformat(now(), "YYYY-MM-DD")#">
		<input type="hidden" name="context" id="context" value="#HtmlEditFormat(rc.context)#">
		<input type="submit" name="submit" value="Save &amp; exit" class="btn btn-primary">
	 </div>
	</form>
</cfoutput>
