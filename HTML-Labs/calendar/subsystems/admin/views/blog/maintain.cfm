<cfimport taglib="/lib/tags/" prefix="ui">
<cfscript>
	rc.Categories = EntityLoad( "Category", {"parentID": '0', "site_ID": rc.CurrentSite.siteID}, "OrderIndex" );
</cfscript>

<cfoutput>
	
  <div class="content-wrapper">
  <h1 class="page-heading">#rc.currentsite.sitename# Blog : #rc.pageTitle#</h1>
    
    
    #view("partials/messages")#
    
    <form action="#buildURL('blog.save')#" method="post" class="form-horizontal" id="page-form">
        <input type="hidden" name="postid" id="pageid" value="#HtmlEditFormat(rc.Post.getPostId())#">
		<input type="hidden" name="updated" id="updated" value="#dateformat(now(), "YYYY-MM-DD")#">
		<input type="hidden" name="context" id="context" value="#HtmlEditFormat(rc.context)#">
   <div class="row">
   <div class="col-sm-8">
   <div class="card">
      <div class="card-block">
        <div class="media">
          <div class="media-body media-middle">
            <h4 class="m-b-0">#rc.pageTitle#</h4>
            <p class="text-muted m-b-0"></p>
          </div>
        </div>    
      </div>
      
      <div class="card-block p-t-0">
        <div class="form-group <cfif rc.result.hasErrors('title')>error</cfif>">
				<label for="title">Title <cfif rc.Validator.propertyIsRequired("title", rc.context)>*</cfif></label>
				<input class="form-control" type="text" name="title" id="title" value="#HtmlEditFormat(rc.Post.getTitle())#" maxlength="100" placeholder="Title">
				#view("partials/failures", {property = "title"})#
			</div>
			
			<cfif rc.context is "Update">
			<div class="form-group <cfif rc.result.hasErrors('title')>error</cfif>">
				<label for="title">Slug <cfif rc.Validator.propertyIsRequired("title", rc.context)>*</cfif></label>
				<input class="form-control" type="text" name="slug" id="slug" value="#HtmlEditFormat(rc.Post.getSlug())#" maxlength="100" placeholder="Title">
				#view("partials/failures", {property = "title"})#
			</div>	
			</cfif>

			<ui:form-row type="htmlfull" id="content" label="Content" name="body" value="#rc.Post.getBody()#">
			
			<div class="form-group ">
				<label for="page-content">Published </label>
				<select name="published" class="form-control">
					<option value="0" <cfif !rc.post.getPublished()>selected="selected"</cfif>>No</option>
					<option value="1" <cfif rc.post.getPublished()>selected="selected"</cfif>>Yes</option>
				</select>
				
			</div>
            
            <div class="form-group ">
				<label for="page-content">Author </label>
				<select name="IDuser" class="form-control">
					<cfloop array="#rc.AdminUsers#" index="author">
					<option value="#author.getUserID()#" <cfif #rc.CurrentUser.getUserID()# EQ #author.getUserID()#>selected</cfif>>#author.getName()#</option>
					</cfloop>
				</select>
				
			</div>
			
			
        
      </div>
    </div>
    </div>
    
    <div class="col-sm-4">
      <div class="card">
      <div class="card-block">
        <div class="media">
          <div class="media-body media-middle">
            <h4 class="m-b-0">Post Categories</h4>
            <p class="text-muted m-b-0"></p>
          </div>
        </div>    
      </div>
      
      <div class="card-block p-t-0">
            <ul style="list-style: none;">
	    <cfloop array="#rc.Categories#" index="Category">
           <cfscript>
               rc.SubCategories = new Query(sql="select * from blog_Category where category_Parent=#Category.getIDCategory()#").execute().getResult();
               if(rc.context == 'update'){
	           rc.postcategories = new Query(sql = 'Select * from blog_post_category where IDPost = #rc.Post.getPostId()#').execute().getResult();

	           rc.SelectedCats = ValueList(rc.postcategories.IDCategory, ",");
               }
           </cfscript>
                <li>	
                <cfif rc.context is "update" && #rc.postcategories.IDCategory# EQ Category.getIDCategory()>
                <ui:input type="check" value="#Category.getIDCategory()#" selected="true" name="category" valuelabel="#Category.getName()#"></ui:input>
				<cfelse>
				<ui:input type="check" value="#Category.getIDCategory()#" name="category" valuelabel="#Category.getName()#"></ui:input>
                </cfif> 				    
				   <cfif rc.SubCategories.recordcount GT "0">
				   
				    <ul style="list-style: none;">
				      <cfloop query="rc.SubCategories">
				      <li>
				      <cfif rc.context is 'update'>
				      <cfif ListFind('#rc.SelectedCats#', '#rc.Subcategories.IDCategory#', ",")>
				      <ui:input type="check" value="#rc.Subcategories.IDCategory#" selected="true" name="category" valuelabel="#rc.Subcategories.category_Name#"></ui:input>
				      <cfelse>
				      <ui:input type="check" value="#rc.Subcategories.IDCategory#" name="category" valuelabel="#rc.Subcategories.category_Name#"></ui:input>
				      
				      </cfif>
				      <cfelse>
				      <ui:input type="check" value="#rc.Subcategories.IDCategory#"  name="category" valuelabel="#rc.Subcategories.category_Name#"></ui:input>
				      </cfif>
				      </li>
                      
                     <li></li>
                 
				      </cfloop>
				    </ul>
				   </cfif>
				</li>
           </cfloop>
           </ul>
      </div>
        
        
		
		
    </div>
    </div>  
    
    
        
   
   
  </div>
  <div class="row pull-right">
	    <input type="submit" name="submit" value="Save &amp; continue" class="btn btn-primary">
		<input type="submit" name="submit" value="Save &amp; exit" class="btn btn-primary">
  </div>
  </form>
</cfoutput>