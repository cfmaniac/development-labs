<cfimport taglib="/lib/tags/" prefix="ui">
<cfoutput>
	
<ui:container title="Blog Post Comments" main="false" style="margin-top:0;" size="12" infotext="">
        <table id="datatable-example" class="table table-bordered table-striped table-hover table-sm">
        <thead>
          <tr>
            <th></th>
            <th>Name</th>
            <th>Post</th>
            <th>Comment Date</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
        <cfloop array="#rc.Comments#" index="local.comment">
        
        <tr>
		    <td>#local.comment.GetIDComment()#</td>
			<td><a href="#buildURL(action='blog.maintainComments', querystring='Id=#local.comment.GetIDComment()#')#">#local.comment.getName()#</a></td>
			<td>#local.comment.getPost().getTitle()#</td>
			<td>#dateformat(local.comment.getComment_DateTime(), "full")#</td>
			<td><a href="#buildURL(action='blog.deleteComment', querystring='Id=#local.comment.GetIDComment()#')#" title="Delete" class="btn btn-danger btn-rounded-deep"><i class="fa fa-trash-o"></i></a></td>
			
		</tr>
		
		    
		
		  	
		</cfloop>
        </tbody>
      </table>
</ui:container>     
</cfoutput>    