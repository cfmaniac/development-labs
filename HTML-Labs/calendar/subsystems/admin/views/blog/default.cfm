<cfimport taglib="/lib/tags/" prefix="ui">
<cfoutput>
	
  <div class="content-wrapper">
    <h1 class="page-heading">#rc.currentsite.sitename# Blog : Posts</h1>
    
    
    #view("partials/messages")#
    
    <div class="row">
   <div class="col-sm-12">
   <div class="card">
      <div class="card-block">
        <div class="media">
          <div class="media-body media-middle">
            <h4 class="m-b-0">Blog Posts</h4>
            <p class="text-muted m-b-0"></p>
          </div>
        </div>    
      </div>
      
      <div class="card-block p-t-0">
      
      <ui:table tableID="blogTable" datatable="true" showheader="true" pagelength="50" columnsHidden="0" sortcol="0"
            sortOrder="DESC" columns="Name|Created|Updated" showDelete="true">
        <cfloop array="#rc.posts#" index="post">
            <ui:table-row id="#post.getPostID()#" 
            editColumn="1"
            editLink="#buildURL(action='blog.maintain', querystring='postId=#post.GetPostID()#')#"
            
            deleteLink="#buildURL(action='blog.delete', querystring='postId=#post.GetPostID()#')#" 
            showdelete="true"
            values="#post.GetTitle()#|#DateFormat(post.getDateTime(), "full")# at #TimeFormat(post.getDateTime())#|#DateFormat(post.getUpdated(), "full")# at #TimeFormat(post.getUpdated())#">
            </ui:table-row> 
        </cfloop>
      </ui:table>
      </div>
    </div>
    </div>
    
    </div>
  </div>
</cfoutput>    