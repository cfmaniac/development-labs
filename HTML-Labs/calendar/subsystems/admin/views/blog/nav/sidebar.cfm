<cfoutput>
 <li class="media active">
        <div class="media-left">
          <div class="sidebar-activity-icon">
            <i class="material-icons">mail</i>
          </div>
        </div>
        <div class="media-body">
          <a href="#buildURL('blog.maintain')#">Add Post</a>
        </div>
 </li>
 <li class="media active">
        <div class="media-left">
          <div class="sidebar-activity-icon">
            <i class="material-icons">mail</i>
          </div>
        </div>
        <div class="media-body">
          <a href="#buildURL('blog.maintainCats')#">Add Category</a>
        </div>
 </li>
 <cfif rc.modules.blog.enableComment>
     <li class="media active">
        <div class="media-left">
          <div class="sidebar-activity-icon">
            <i class="material-icons">mail</i>
          </div>
        </div>
        <div class="media-body">
          <a href="#buildURL('blog.comments')#">Moderate Comments</a>
        </div>
 </li>
 </cfif>
</cfoutput>      