<cfimport taglib="/lib/tags/" prefix="ui">


<cfoutput>
	
  <div class="content-wrapper">
    <ol class="breadcrumb m-b-0">
      <li><a href="#BuildURL('')#">Home</a></li>
      <li>Blog</li>
      <li class="active">#rc.pageTitle#</li>
    </ol>
    
    #view("partials/messages")#
    
    <form action="#buildURL('blog.saveComment')#" method="post" class="form-horizontal" id="page-form">
        <input type="hidden" name="commentid" id="pageid" value="#HtmlEditFormat(rc.comment.GetIDComment())#">
		
   <div class="row">
   <div class="col-sm-8">
   <div class="card">
      <div class="card-block">
        <div class="media">
          <div class="media-body media-middle">
            <h4 class="m-b-0">#rc.pageTitle#</h4>
            <p class="text-muted m-b-0"></p>
          </div>
        </div>    
      </div>
      
      <div class="card-block p-t-0">
       
			<div class="form-group">
				<label for="page-content">Comment Content </label>
				<textarea class="form-control ckeditor" name="body" id="page-content">#HtmlEditFormat(rc.comment.getValue())#</textarea>
				
			</div>
			
			
            
            <div class="form-group ">
				<label for="page-content">Author </label>
				#rc.comment.getName()#
				
			</div>
			
			
        
      </div>
    </div>
    </div>
    
    
    </div>  
    
    
        
   
   
  </div>
  <div class="row pull-right">
	    
		<input type="submit" name="submit" value="Save Comment" class="btn btn-primary">
  </div>
  </form>
</cfoutput>