<cfoutput>
	<cfimport taglib="/lib/tags/" prefix="ui">

<!--- [ Output ] --->
<cfoutput>

<ui:container title="Events" style="margin-top:0;" main="true" size="12" infotext="Manage your Events and Functions">
	#view("partials/messages")#

    <table id="datatable-example" class="table table-bordered table-striped table-hover table-sm">
        <thead>
          <tr>
            <th class="hidden"></th>
            <th>Title</th>
            <th>Starts On</th>
            <th>Times</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
	<cfif arraylen(#rc.events#)>
        <cfloop array="#rc.events#" index="local.event">
        <tr>
	        <td>#local.event.getID()#</td>
	        <td><a href="#buildURL(action='calendars.maintain', querystring='id=#local.event.GetID()#')#">#local.event.getTitle()#</a></td>
	        <td>#dateFormat(local.event.getStartAt(), "Full")#</td>
	        <td>#timeFormat(local.event.getStartTime(), "HH:MM TT")# - #timeFormat(local.event.getEndTime(), "HH:MM TT")#</td>
	        <td><a href="#buildURL(action='calendars.delete', querystring='Id=#local.event.GetID()#')#" title="Delete" class="btn btn-danger btn-rounded-deep"><i class="fa fa-trash-o"></i></a></td>
        </tr>

        </cfloop>

    <cfelse>
        <tr>
            <td colspan="5">
                No Events have been added.
            </td>
        </tr>
	</cfif>


        </tbody>
        </table>
</ui:container>


</cfoutput>


</cfoutput>