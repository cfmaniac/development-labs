<cfimport taglib="/lib/tags/" prefix="ui">
<cfoutput>

<ui:container title="#rc.pageTitle#" style="margin-top:0;" size="12" formcontrols="true" form="eventsForm" formcontrolsContinue="false">
 
 
 <ui:form method="post" action="#buildURL('calendars.save')#" name="eventsForm">
        <ui:form-row label="Event Title" name="title" value="#rc.event.getTitle()#">
        
		<ui:form-row type="htmlfull" label="Event Description" name="body" value="#rc.event.getBody()#">
		
		
		<ui:form-row type="boolean" label="All Day Event" name="alldayevent" value="#rc.event.getAllDayEvent()#">
		<ui:form-row type="date" label="Start Date" name="startAt" value="#rc.event.getStartAt()#">  
		<ui:form-row type="time" label="Start Time" name="startTime" value="#rc.event.getStartTime()#">  
		<ui:form-row type="date" label="End Date" name="endsAt" value="#rc.event.getEndAt()#"> 
		<ui:form-row type="time" label="End Time" name="endTime" value="#rc.event.getEndTime()#">  
		
        
        <!---NOT HANDLED BY CUSTOM TAGS--->
        <input type="hidden" name="id" value="#rc.event.getID()#">
        <input type="hidden" name="context" value="#rc.context#">
  </ui:form> 
 
</ui:container>

  	
   
</cfoutput>
