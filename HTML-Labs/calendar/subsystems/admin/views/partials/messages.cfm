<cfoutput>
	<cfif StructKeyExists(rc, "result") and rc.result.hasMessage()>
		<div class="alert alert-#rc.result.getMessageType()#">#rc.result.getMessage()#</div>
	</cfif>
	
	<cfif structKeyExists(rc, "message") and len(rc.message)>
		<div class="alert alert-success">#rc.Message#</div>
	</cfif>
</cfoutput>
