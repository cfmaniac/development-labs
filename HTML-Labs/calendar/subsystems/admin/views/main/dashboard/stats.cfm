<cfoutput>
<cfif #len(rc.config.gaProfileID)#>
<div class="row">
	<div class="col-xs-1"> <!-- required for floating -->
          <!-- Nav tabs -->
          <ul class="nav nav-tabs tabs-left sideways">
            <li class="active"><a href="##ia" data-toggle="tab">Internal Stats</a></li>
             <cfif #len(rc.config.gaProfileID)#><li><a href="##ga" data-toggle="tab">Google Analytics</a></li></cfif>
            
          </ul>
        </div>

        <div class="col-xs-11">
          <!-- Tab panes -->
          <div class="tab-content">
            <div class="tab-pane active" id="ia">
	            <ui:container title="Site Analytics & Stats" main="false" style="margin-top:0;" size="12" infotext="These are your Site's Internally Tracked Analytics">
  <div class="row">
        <div class="col-sm-4">
             
	         <table class="table table-md m-a-0">
              <tbody>
              <tr><td>
	              Total Views <strong>This Month</strong>: <span class="pull-right btn btn-success btn-rounded-deep"> #numberFormat(ArraySum(rc.PageViews))#</span>
              </td></tr>   
              
              <tr><td>
	              Total Visitors <strong>This Month</strong>: <span class="pull-right btn btn-success btn-rounded-deep">#numberFormat(ArraySum(rc.Visitors))#</span>
              </td></tr>
              <tr><td>
	              <a href="#buildURL('stats')#" class="btn btn-success btn-rounded-deep pull-right">More Stats</a>
              </td></tr>                        
              </tbody>
            </table>
        </div>
        <div id="usage" class="col-sm-8" style="height: 275px; margin: 0 auto"></div>
        
        </div> 
</ui:container>
            </div>
            <cfif #len(rc.config.gaProfileID)#>
            <div class="tab-pane" id="ga">
	            <ui:container title="Google Analytics & Stats" main="false" style="margin-top:0;" size="12" infotext="These are your Site's Analytics as Tracked by Google Analytics">
  <div class="row">
        <div class="col-sm-4">
        </div>
        <div class="col-sm-8">
        </div>
  </div>
  </ui:container>      
            </div>
            </cfif>
          </div>
        </div>
</div>
<cfelse>

<ui:container title="Site Analytics & Stats" main="false" style="margin-top:0;" size="12" infotext="These are your Site's Internally Tracked Analytics">
  <div class="row">
        <div class="col-sm-4">
             
	         <table class="table table-md m-a-0">
              <tbody>
              <tr><td>
	              Total Views <strong>This Month</strong>: <span class="pull-right btn btn-success btn-rounded-deep"> #numberFormat(ArraySum(rc.PageViews))#</span>
              </td></tr>   
              
              <tr><td>
	              Total Visitors <strong>This Month</strong>: <span class="pull-right btn btn-success btn-rounded-deep">#numberFormat(ArraySum(rc.Visitors))#</span>
              </td></tr>
              <tr><td>
	              <a href="#buildURL('stats')#" class="btn btn-success btn-rounded-deep pull-right">More Stats</a>
              </td></tr>                        
              </tbody>
            </table>
        </div>
        <div id="usage" class="col-sm-8" style="height: 275px; margin: 0 auto"></div>
        
        </div> 
</ui:container>
</cfif>
</cfoutput>