<cfoutput>
<h1 class="page-heading">About Replicator</h1>

<div class="row">
        <div class="col-xl-12">
          <div class="card">
            <div class="card-block">
              <div class="media">
                <div class="media-left media-middle">
                  
                </div>
                <div class="media-body media-middle">
                  <h4 class="card-title">Replicator X (v.#rc.config.version#)</h4>
                  <p class="card-subtitle">Core Version: #rc.config.coreversion#</p>
                </div>
                
              </div>
            </div>
            

            <div class="card-block">
              <h4 class="card-title">About Replicator</h4>
              #rc.AboutReplicator#
            </div>
          </div>
          
      </div>

</cfoutput>