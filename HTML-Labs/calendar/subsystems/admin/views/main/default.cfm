<cfoutput>
<h1 class="page-heading">#rc.currentsite.sitename# Dashboard</h1>

#view("partials/messages")#

<div class="row">
        <div class="col-xl-12">
          <div class="card">
             

            <div class="card-block">
              <h4 class="card-title">#rc.currentsite.sitename# Quick Stats</h4>
              #view('main/dashboard/stats')#
            </div>
          </div>
          
      </div>
      
      <!---cfdump var="#rc#"--->

</cfoutput>
