<cfimport taglib="/lib/tags/" prefix="ui">
<cfoutput>
<h1 class="page-heading">Add a New Site</h1>

#view("partials/messages")#

<div class="card">
        <ul class="nav nav-tabs">
          <li class="nav-item">
            <a class="nav-link active" href="##site" data-toggle="tab" aria-expanded="true">Site Settings</a>
          </li>
          
          <!---li class="nav-item">
            <a class="nav-link" href="##modules" data-toggle="tab" aria-expanded="false">Module Settings</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="##plugins" data-toggle="tab" aria-expanded="false">Plugins Settings</a>
          </li--->
          
        </ul>
        <div class="card-block tab-content">
          <div class="tab-pane active" id="site" aria-expanded="true"> 
          <p class="alert alert-info">
	           Module & Plugin Options will become available <em>after</em> the new site has been added
          </p>
           <div class="stepwizard">
    <div class="stepwizard-row setup-panel">
        <div class="stepwizard-step">
            <a href="##step-1" type="button" class="btn btn-primary btn-circle"><i class="fa fa-info fa-3x"></i></a>
            <p>Site Info</p>
        </div>
        <div class="stepwizard-step">
            <a href="##step-2" type="button" class="btn btn-default btn-circle" ><i class="fa fa-bar-chart fa-3x"></i></a>
            <p>Analytics & Social</p>
        </div>
        <div class="stepwizard-step">
            <a href="##step-3" type="button" class="btn btn-default btn-circle" ><i class="fa fa-map-marker fa-3x"></i></a>
            <p>Location & Contact</p>
        </div>
        <div class="stepwizard-step">
            <a href="##step-4" type="button" class="btn btn-default btn-circle" ><i class="fa fa-photo fa-3x"></i></a>
            <p>Templates & Layout</p>
        </div>
    </div>
</div>
<form method="post" action="#buildURL('settings.saveNewSite')#" id="siteSettings" name="siteSettings">
				
    <div class="row setup-content" id="step-1">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3>Site Info</h3>
               
	           <ui:form-row id="Title" label="Site Title" placeholder="Site Title" name="title" value="">
	           <ui:form-row id="domain" label="Site Domain" placeholder="Site Domain" name="domain" value="">
	           <ui:form-row id="devdomain" label="Development Domain " placeholder="Development Domain" name="domaindev" value=""
	           infotext="Development or Alternate Domain that can load the site">
	           <ui:form-row type="boolean" id="isSubsite" label="Is this a subsite?" name="isSubsite" value="0" infotext="Will this be a domain controlled by another?">
	           <div class="form-group row">
	   				   <label class="col-sm-3 form-control-label">
				    Parent Site
			    </label>
	              <div class="col-sm-9">
                  <select name="parentsiteID" class="form-control">
	              <option value="0">No Parent Site</option>
	              <cfloop array="#rc.Sites#" index="site">
	              <option value="#site.getID()#" <cfif #rc.CurrentSite.siteID# EQ #site.getID()#>selected="selected"</cfif>>#site.getTitle()# <cfif !site.getIsActive()>-(InActive)</cfif></option>
	              </cfloop>  
		          </select>
                    
                 </div>
		
	              
		          </div>
	           <ui:form-row type="boolean" id="forcessql" label="Force SSL Connection" name="forceSSL" value="0">
	           <ui:form-row type="boolean" id="enablesiteSearch" label="Enable Site Search" name="enablesitesearch" infotext="Enables a Search of all active Content Modules i.e., Pages, Blog, News, etc." value="0">
	           <ui:form-row type="boolean" id="active" label="Site Active" name="active" value="1"> 
	           <ui:form-row type="htmlmin" id="copyright" label="Site Copyright" name="copyright" value="">
	
                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-2">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3> Analytics & Social</h3>
                <h4>Analytics</h4>
            <ui:form-row type="boolean" id="internalAnalytics" label="Use Internal Analytics" name="internal" value="0">
            <ui:form-row id="googleAnalytics" label="Google Analytics ID" name="googleAnalytics" value="">
            <ui:form-row id="googleProfileID" label="Google Analytics Profile ID" name="gaProfileID" value="">
            
            <hr>
          <h4>Social Networking</h4>
          <div class="row">
            <div class="col-md-6">
            <ui:form-row id="facebookUrl" label="FaceBook URL" name="facebookUrl" value="">
            <button class="btn btn-info pull-right" type="button" data-toggle="collapse" data-target="##FacebookInteg" aria-expanded="false" aria-controls="collapseExample">
            Integration Settings
            </button>
            <div class="collapse alert-info" id="FacebookInteg">
            <div class="row">
            <ui:form-row id="facebookAppID" label="FaceBook App ID" name="facebookAppID" value="">
            <ui:form-row id="facebookAppSecret" label="FaceBook App Secret Key" name="facebookAppSecret" value="">
            <ui:form-row type="boolean" id="facebookLogin" label="Use Facebook Login" name="facebooklogin" value="0">
            </div>
            </div>
            </div>
            <div class="col-md-6">
            <ui:form-row id="twitterUrl" label="Twitter URL" name="twitterUrl" value="">
            <button class="btn btn-info pull-right" type="button" data-toggle="collapse" data-target="##TwitterInteg" aria-expanded="false" aria-controls="collapseExample">
            Integration Settings
            </button>
            <div class="collapse alert-info" id="TwitterInteg">
            <div class="row">
            <ui:form-row id="twitterconsumerkey" label="Twitter Consumer Key" name="twitterconsumerkey" value="">
            <ui:form-row id="twitterconsumersecret" label="Twitter Consumer Secret Key" name="twitterconsumersecret" value="">
            <ui:form-row id="twitteraccesstoken" label="Twitter Access Token" name="twitteraccesstoken" value="">
            <ui:form-row id="twitteraccesstokensecret" label="Twitter Access Secret Key" name="twitteraccesstokensecret" value="">
            <ui:form-row id="twitterscreenname" label="Twitter ScreenName" name="twitterscreenname" value="">
            <ui:form-row id="twitteruserid" label="Twitter Username" name="twitteruserid" value="">
            </div>
            </div>
            </div>
          </div>  
          <div class="row">
            <div class="col-md-6">
            <ui:form-row id="linkedInUrl" label="LinkedIn URL" name="linkedInUrl" value="">
            <button class="btn btn-info pull-right" type="button" data-toggle="collapse" data-target="##LinkedInteg" aria-expanded="false" aria-controls="collapseExample">
            Integration Settings
            </button>
            <div class="collapse alert-info" id="LinkedInteg">
            <div class="row">
            <ui:form-row id="linkedinApiKey" label="LinkedIn API Key" name="linkedinApiKey" value="">
            <ui:form-row id="linkedinSecretKey" label="LinkedIn Secret Key" name="linkedinSecretKey" value="">
            </div>
            </div>
            </div>
            <div class="col-md-6">
            <ui:form-row id="youTubeUrl" label="YouTube URL" name="youTubeUrl" value="">
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
            <ui:form-row id="googlePlusUrl" label="Google+ URL" name="googlePlusUrl" value="">
            <button class="btn btn-info pull-right" type="button" data-toggle="collapse" data-target="##GoogInteg" aria-expanded="false" aria-controls="collapseExample">
            Integration Settings
            </button>
            <div class="collapse alert-info" id="GoogInteg">
            <div class="row">
            <ui:form-row id="googleApiKey" label="Google API Key" name="googleApiKey" value="">
            <ui:form-row id="googleSecretkey" label="Google Secret Key" name="googleSecretkey" value="">
            </div>
            </div>
            </div>
           
          </div> 
                <button class="btn btn-info prevBtn btn-lg pull-left" type="button" >Prev</button>
                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-3">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3>Location & Contact</h3>
                <ui:form-row id="email" label="Site email" placeholder="Site email" name="email" value="">
           <ui:form-row id="address" label="Site Address" placeholder="Site Address" name="address" value="">
           <ui:form-row id="addressw" label="Site Address(2)" placeholder="Site Address(2)" name="address2" value="">
           <ui:form-row id="city" label="Site City" placeholder="Site City" name="city" value="">
           <!---ui:form-row id="state" label="Site State" placeholder="Site State" name="state" value="#setting.getstate()#"--->
           #rc.regions.stateSelect(name='state', icon='', label="Site State", default='', allownone=true, value='abv2')#
           <!---ui:form-row id="state" type="state" Label="Site State" name="state" value="#setting.getState#"--->
           <ui:form-row id="zipcode" label="Site Zipcode" placeholder="Site Zipcode" name="zipcode" value="">
           
          <h4>Geolocation Settings</h4>
           <ui:form-row type="boolean" id="useGeoLocation" label="Use GeoLocation" name="useGeoLocation" value="0">
           <ui:form-row id="geolocationRadius" label="Geolocation Radius (in Miles)" placeholder="Geolocation Radius" name="geolocationRadius" value="0"> 
          
                <button class="btn btn-info prevBtn btn-lg pull-left" type="button" >Prev</button>
                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-4">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3>Templates & Layout</h3>
                 <cfset rc.OptionsCore = #ValueList(rc.SiteTemplates.Name,",")#>
          <cfset listPos = ListFind(rc.OptionsCore,"default.cfm",",")>
          <cfset rc.Options = ListDeleteAt(rc.OptionsCore, listPos , ",")>
          <ui:input label="Site Layout" type="select" options="#rc.Options#" name="defaultLayout" value="">
	      <ui:input label="Control Panel Timeout" type="select" optionlabels="30 Munites, 1-Hour, 2-Hours, 3-Hours, 5-Hours" options="30,60,120,240,300" name="timeout" >
	      
		  <ui:form-row type="boolean" id="showSitemapLink" label="Show Sitemap Link in Footer" name="showSitemapLink" value="1">          
          
                <button class="btn btn-info prevBtn btn-lg pull-left" type="button" >Prev</button>
                <button class="btn btn-success btn-lg pull-right" type="submit">Save New Site</button>
            </div>
        </div>
    </div>
</form>
			  
          </div>
         
          <div class="tab-pane " id="plugins" aria-expanded="false"> Plugins Settings
          
          </div>
          <div class="tab-pane" id="modules" aria-expanded="false"> 
                
            <div class="row m-b-2">
                <!---cfloop query="#rc.ModulesSettings#">        
	            <div class="col-md-6">
		          <div class="card">
		            <div class="card-button-wrapper">
		              <div class="dropdown">
		                <a href="##" class="card-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="material-icons">tune</i></a>
		                <div class="dropdown-menu dropdown-menu-right">
		                  <!---<a class="dropdown-item" href="##">Action</a>
		                  <a class="dropdown-item" href="##">Another action</a>
		                  <a class="dropdown-item" href="##">Something else here</a>--->
		                  <cfif moduleActive>
                          <a class="dropdown-item" href="#buildURL(action='settings.disableEnableModule', querystring='moduleID=#rc.ModulesSettings.id#&com=0')#">Disable</a>
		                  <cfelse>
		                  <a class="dropdown-item" href="#buildURL(action='settings.disableEnableModule', querystring='moduleID=#rc.ModulesSettings.id#&com=1')#">Enable</a>
		                  </cfif>
		                </div>
		              </div>
		            </div>
		            <div class="card-block">
		              <h5 class="card-title <cfif !moduleActive>alert-danger<cfelse>alert-success</cfif>">#name#  <cfif !moduleActive></cfif></h5>
		              <p class="card-text">#description#</p>
		              
		              <cfif moduleActive && hasConfig>
		              <div class="row">
		              <a href="###lcase(name)#" data-toggle="tab" aria-expanded="false" class="nav-link btn btn-primary pull-right">#Name# Options</a>
		              </div>
		              </cfif>
		            </div>
		          </div>
		        </div>
                </cfloop--->    
            </div>
            <!---cfdump var="#rc.modules#"--->
          </div>
          <!---cfloop query="#rc.ModulesSettings#">
          <cfscript>
	            ModulesConfSQL = 'Select * from modules_settings modset
					            WHERE modset.site_id = :siteID and modset.module_id=:modID';
                ModulesConfQuery = new Query();
                ModulesConfQuery.addParam(name='modID', value='#rc.ModulesSettings.id#');
                ModulesConfQuery.addParam(name='siteID', value='#rc.config.SiteID#');
                rc.ModulesConf = ModulesConfQuery.execute(sql=ModulesConfSQL).getResult();
          </cfscript>
          <div class="tab-pane" id="#lcase(name)#" aria-expanded="false">
            <div class="row">
	            <div class="col-sm-6">
	            <h4>#Name# Settings</h4>
	            </div>
	            
	            <div class="col-sm-6">
	                <a href="##modules" data-toggle="tab" aria-expanded="false" class="nav-link btn btn-info pull-right">Back to Modules</a>
	            </div>
            </div>
	        <cfloop query="#rc.Modulesconf#">
		        <cfif rc.modulesconf.module_setting_type is "Text" or rc.modulesconf.module_setting_type is "INT">
		            <!---Text & Int--->
			        <ui:form-row id="#rc.modulesconf.module_setting_key#" label="#rc.modulesconf.module_setting_key#" 
		           name="#rc.modulesconf.module_setting_key#" value="#rc.modulesconf.module_setting_value#"> 
			    <cfelseif rc.modulesconf.module_setting_type is "bool">
			        <!---Boolean--->
			        <ui:form-row type="boolean" id="#rc.modulesconf.module_setting_key#" label="#rc.modulesconf.module_setting_key#" 
			        name="#rc.modulesconf.module_setting_key#" value="#rc.modulesconf.module_setting_value#">    
		        </cfif>
	        </cfloop>
	      </div>
	                
          </cfloop--->
         
         
      </div>
     
</cfoutput>