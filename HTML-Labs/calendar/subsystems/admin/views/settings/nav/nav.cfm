<cfoutput>
	<ul class="sidebar-submenu">
          <li class="sidebar-menu-item">
            <a class="sidebar-menu-button" href="#buildURL('settings')#">Site Settings</a>
          </li>
          <li class="sidebar-menu-item">
            <a class="sidebar-menu-button" href="#buildURL('stats')#">Analytics</a>
          </li>
          <li class="sidebar-menu-item">
            <a class="sidebar-menu-button" href="reports.html">Reports</a>
          </li>
          <li class="sidebar-menu-item">
            <a class="sidebar-menu-button" href="#buildURL("main.diagnostics")#">Diagnostics</a>
          </li>
          <li class="sidebar-menu-item">
            <a class="sidebar-menu-button" href="#buildURL("main.about")#">About Replicator</a>
          </li>
          <li class="sidebar-menu-item">
          #getEngineAdmin()#
          </li>
        </ul>
</cfoutput>