<cfimport taglib="/lib/tags/" prefix="ui">
<cfoutput>
<h1 class="page-heading">#rc.currentsite.sitename# Site Settings</h1>

#view("partials/messages")#

<div class="card">
        <ul class="nav nav-tabs">
          <li class="nav-item">
            <a class="nav-link active" href="##site" data-toggle="tab" aria-expanded="true">Site Settings</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="##modules" data-toggle="tab" aria-expanded="false">Module Settings</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="##plugins" data-toggle="tab" aria-expanded="false">Plugins Settings</a>
          </li>
          
        </ul>
        <div class="card-block tab-content">
          <div class="tab-pane active" id="site" aria-expanded="true"> 
           <div class="stepwizard">
    <div class="stepwizard-row setup-panel">
        <div class="stepwizard-step">
            <a href="##step-1" type="button" class="btn btn-primary btn-circle"><i class="fa fa-info fa-3x"></i></a>
            <p>Site Info</p>
        </div>
        <div class="stepwizard-step">
            <a href="##step-2" type="button" class="btn btn-default btn-circle" ><i class="fa fa-bar-chart fa-3x"></i></a>
            <p>Analytics & Social</p>
        </div>
        <div class="stepwizard-step">
            <a href="##step-3" type="button" class="btn btn-default btn-circle" ><i class="fa fa-map-marker fa-3x"></i></a>
            <p>Location & Contact</p>
        </div>
        <div class="stepwizard-step">
            <a href="##step-4" type="button" class="btn btn-default btn-circle" ><i class="fa fa-photo fa-3x"></i></a>
            <p>Templates & Layout</p>
        </div>
    </div>
</div>
<form method="post" action="#buildURL('settings.save')#" id="siteSettings" name="siteSettings">
				<input type="hidden" name="siteID" value="#rc.currentSite.siteID#">
    <div class="row setup-content" id="step-1">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3>Site Info</h3>
                <cfloop array="#rc.SiteSettings#" index="setting">
	           <ui:form-row id="Title" label="Site Title" placeholder="Site Title" name="title" value="#setting.getTitle()#">
	           <ui:form-row id="domain" label="Site Domain" placeholder="Site Domain" name="domain" value="#setting.getDomain()#">
	           <ui:form-row id="devdomain" label="Development Domain " placeholder="Development Domain" name="domaindev" value="#setting.getDevDomain()#"
	           infotext="Development or Alternate Domain that can load the site">
	           <ui:form-row type="boolean" id="isSubsite" label="Is this a subsite?" name="isSubsite" value="#setting.getisSubsite()#" infotext="Will this be a domain controlled by another?">
	           <div class="form-group row">
	   				   <label class="col-sm-3 form-control-label">
				    Parent Site
			    </label>
	              <div class="col-sm-9">
                  <select name="parentsiteID" class="form-control">
	              <option value="0" <cfif #setting.getparentsiteID()# EQ "0">selected="selected"</cfif>>No Parent Site</option>
	              <cfloop array="#rc.Sites#" index="site">
	              <option value="#site.getID()#" <cfif #setting.getparentsiteID()# NEQ "0" &&  #setting.getparentsiteID()# EQ #site.getID()#>selected="selected"</cfif>>#site.getTitle()# <cfif !site.getIsActive()>-(InActive)</cfif></option>
	              </cfloop>  
		          </select>
                    
                 </div>
		
	              
		          </div>
	           <ui:form-row type="boolean" id="forcessql" label="Force SSL Connection" name="forceSSL" value="#setting.getForceSSL()#">
	           <ui:form-row type="boolean" id="enablesiteSearch" label="Enable Site Search" name="enablesitesearch" infotext="Enables a Search of all active Content Modules i.e., Pages, Blog, News, etc." value="#setting.getEnableSiteSearch()#">
	           <ui:form-row type="boolean" id="active" label="Site Active" name="active" value="#setting.getisActive()#"> 
	           <ui:form-row type="htmlmin" id="copyright" label="Site Copyright" name="copyright" value="#setting.getCopyright()#">
	         
          </cfloop>
                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-2">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3> Analytics & Social</h3>
                <h4>Analytics</h4>
            <ui:form-row type="boolean" id="internalAnalytics" label="Use Internal Analytics" name="internal" value="#setting.getuseInternalAnalytics()#">
            <ui:form-row id="googleAnalytics" label="Google Analytics ID" name="googleAnalytics" value="#setting.getgaTrackerID()#">
            <ui:form-row id="googleProfileID" label="Google Analytics Profile ID" name="gaProfileID" value="#setting.getgaProfileID()#">
            
            <hr>
          <h4>Social Networking</h4>
          <div class="row">
            <div class="col-md-6">
            <ui:form-row id="facebookUrl" label="FaceBook URL" name="facebookUrl" value="#setting.getfacebookUrl()#">
            <button class="btn btn-info pull-right" type="button" data-toggle="collapse" data-target="##FacebookInteg" aria-expanded="false" aria-controls="collapseExample">
            Integration Settings
            </button>
            <div class="collapse alert-info" id="FacebookInteg">
            <div class="row">
            <ui:form-row id="facebookAppID" label="FaceBook App ID" name="facebookAppID" value="#setting.getfacebookAppID()#">
            <ui:form-row id="facebookAppSecret" label="FaceBook App Secret Key" name="facebookAppSecret" value="#setting.getfacebookAppSecret()#">
            <ui:form-row type="boolean" id="facebookLogin" label="Use Facebook Login" name="facebooklogin" value="#setting.getusefacebookLogin()#">
            </div>
            </div>
            </div>
            <div class="col-md-6">
            <ui:form-row id="twitterUrl" label="Twitter URL" name="twitterUrl" value="#setting.gettwitterUrl()#">
            <button class="btn btn-info pull-right" type="button" data-toggle="collapse" data-target="##TwitterInteg" aria-expanded="false" aria-controls="collapseExample">
            Integration Settings
            </button>
            <div class="collapse alert-info" id="TwitterInteg">
            <div class="row">
            <ui:form-row id="twitterconsumerkey" label="Twitter Consumer Key" name="twitterconsumerkey" value="#setting.gettwitterConsumerKey()#">
            <ui:form-row id="twitterconsumersecret" label="Twitter Consumer Secret Key" name="twitterconsumersecret" value="#setting.gettwitterConsumerSecret()#">
            <ui:form-row id="twitteraccesstoken" label="Twitter Access Token" name="twitteraccesstoken" value="#setting.gettwitterAccessToken()#">
            <ui:form-row id="twitteraccesstokensecret" label="Twitter Access Secret Key" name="twitteraccesstokensecret" value="#setting.gettwitterAccessTokenSecret()#">
            <ui:form-row id="twitterscreenname" label="Twitter ScreenName" name="twitterscreenname" value="#setting.gettwitterScreenName()#">
            <ui:form-row id="twitteruserid" label="Twitter Username" name="twitteruserid" value="#setting.gettwitterUserId()#">
            </div>
            </div>
            </div>
          </div>  
          <div class="row">
            <div class="col-md-6">
            <ui:form-row id="linkedInUrl" label="LinkedIn URL" name="linkedInUrl" value="#setting.getlinkedInUrl()#">
            <button class="btn btn-info pull-right" type="button" data-toggle="collapse" data-target="##LinkedInteg" aria-expanded="false" aria-controls="collapseExample">
            Integration Settings
            </button>
            <div class="collapse alert-info" id="LinkedInteg">
            <div class="row">
            <ui:form-row id="linkedinApiKey" label="LinkedIn API Key" name="linkedinApiKey" value="#setting.getlinkedinApiKey()#">
            <ui:form-row id="linkedinSecretKey" label="LinkedIn Secret Key" name="linkedinSecretKey" value="#setting.getlinkedinSecretKey()#">
            </div>
            </div>
            </div>
            <div class="col-md-6">
            <ui:form-row id="youTubeUrl" label="YouTube URL" name="youTubeUrl" value="#setting.getyouTubeUrl()#">
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
            <ui:form-row id="googlePlusUrl" label="Google+ URL" name="googlePlusUrl" value="#setting.getgooglePlusUrl()#">
            <button class="btn btn-info pull-right" type="button" data-toggle="collapse" data-target="##GoogInteg" aria-expanded="false" aria-controls="collapseExample">
            Integration Settings
            </button>
            <div class="collapse alert-info" id="GoogInteg">
            <div class="row">
            <ui:form-row id="googleApiKey" label="Google API Key" name="googleApiKey" value="#setting.getgoogleApiKey()#">
            <ui:form-row id="googleSecretkey" label="Google Secret Key" name="googleSecretkey" value="#setting.getgoogleSecretkey()#">
            </div>
            </div>
            </div>
           
          </div> 
                <button class="btn btn-info prevBtn btn-lg pull-left" type="button" >Prev</button>
                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-3">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3>Location & Contact</h3>
                <ui:form-row id="email" label="Site email" placeholder="Site email" name="email" value="#setting.getemail()#">
           <ui:form-row id="address" label="Site Address" placeholder="Site Address" name="address" value="#setting.getaddress()#">
           <ui:form-row id="address2" label="Site Address(2)" placeholder="Site Address(2)" name="address2" value="#setting.getaddress2()#">
           <ui:form-row id="city" label="Site City" placeholder="Site City" name="city" value="#setting.getcity()#">
           <!---ui:form-row id="state" label="Site State" placeholder="Site State" name="state" value="#setting.getstate()#"--->
           #rc.regions.stateSelect(name='state', icon='', label="Site State", default=setting.getstate(), allownone=true, value='abv2')#
           <!---ui:form-row id="state" type="state" Label="Site State" name="state" value="#setting.getState#"--->
           <ui:form-row id="zipcode" label="Site Zipcode" placeholder="Site Zipcode" name="zipcode" value="#setting.getzipcode()#">
           
          <h4>Geolocation Settings</h4>
           <ui:form-row type="boolean" id="useGeoLocation" label="Use GeoLocation" name="useGeoLocation" value="#setting.getuseGeoLocation()#">
           <ui:form-row id="geolocationRadius" label="Geolocation Radius (in Miles)" placeholder="Geolocation Radius" name="geolocationRadius" value="#setting.getgeolocationRadius()#"> 
          
                <button class="btn btn-info prevBtn btn-lg pull-left" type="button" >Prev</button>
                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-4">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3>Templates & Layout</h3>
                 <cfset rc.OptionsCore = #ValueList(rc.SiteTemplates.Name,",")#>
          <cfset listPos = ListFind(rc.OptionsCore,"default.cfm",",")>
          <cfset rc.Options = ListDeleteAt(rc.OptionsCore, listPos , ",")>
          <ui:input label="Site Layout" type="select" options="#rc.Options#" name="defaultLayout" value="#setting.getdefaultLayout()#">
	      <ui:input label="Control Panel Timeout" type="select" optionlabels="30 Minutes, 1-Hour, 2-Hours, 3-Hours, 5-Hours" options="1800000,3600000,7200000 ,10800000,18000000" name="timeout" value="#setting.getSessionTimeout()#">
	      
		  <cfset rc.HomePages = ValueList(rc.RootPages.Slug,",")>
		  <cfset rc.HomeLabels = ValueList(rc.RootPages.Title,",")>
	      <ui:input label="Site Home Page" type="select" optionlabels="#rc.HomeLabels#" options="#rc.HomePages#" name="homepage" value="#setting.getHomePage()#">
	      <ui:form-row type="boolean" id="showSitemapLink" label="Show Sitemap Link in Footer" name="showSitemapLink" value="#setting.getshowSitemapLink()#">      
	      <ui:form-row label="Mobile Theme Color" type="colorpicker" name="themecolor" value="#setting.getthemecolor()#">     
          
                <button class="btn btn-info prevBtn btn-lg pull-left" type="button" >Prev</button>
                <button class="btn btn-success btn-lg pull-right" type="submit">Save #rc.CurrentSite.siteName# Settings</button>
            </div>
        </div>
    </div>
</form>
			  
          </div>
         
          <div class="tab-pane " id="plugins" aria-expanded="false"> Plugins Settings
          
          </div>
          <div class="tab-pane" id="modules" aria-expanded="false"> 
            <h2>Active Modules</h2>    
            <div class="row m-b-2">
                <cfloop query="#rc.ModulesSettings#">        
	            <div class="col-md-6">
		          <div class="card">
		            <div class="card-button-wrapper">
		              <div class="dropdown">
		                <a href="##" class="card-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="material-icons">tune</i></a>
		                <div class="dropdown-menu dropdown-menu-right">
		                  <!---<a class="dropdown-item" href="##">Action</a>
		                  <a class="dropdown-item" href="##">Another action</a>
		                  <a class="dropdown-item" href="##">Something else here</a>--->
		                  <cfif moduleActive>
                          <a class="dropdown-item" href="#buildURL(action='settings.disableEnableModule', querystring='moduleID=#rc.ModulesSettings.id#&com=0')#">Disable</a>
		                  <cfelse>
		                  <a class="dropdown-item" href="#buildURL(action='settings.disableEnableModule', querystring='moduleID=#rc.ModulesSettings.id#&com=1')#">Enable</a>
		                  </cfif>
		                </div>
		              </div>
		            </div>
		            <div class="card-block">
		              <h5 class="card-title <cfif !moduleActive>alert-danger<cfelse>alert-success</cfif>">#name#  <cfif !moduleActive></cfif></h5>
		              <p class="card-text">#description#</p>
		              
		              <cfif moduleActive && hasConfig>
		              <div class="row">
		              <a href="###lcase(name)#" data-toggle="tab" aria-expanded="false" class="nav-link btn btn-primary pull-right">#Name# Options</a>
		              </div>
		              </cfif>
		            </div>
		          </div>
		        </div>
                </cfloop>
                
                
            </div>
            
            <hr>
         <h2>InActive Modules</h2>
         <div class="row m-b-2">
         <cfloop query="#rc.InActiveModulesSettings#">        
	            <div class="col-md-6">
		          <div class="card">
		            <div class="card-button-wrapper">
		              <div class="dropdown">
		                <a href="##" class="card-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="material-icons">tune</i></a>
		                <div class="dropdown-menu dropdown-menu-right">
		                  <!---<a class="dropdown-item" href="##">Action</a>
		                  <a class="dropdown-item" href="##">Another action</a>
		                  <a class="dropdown-item" href="##">Something else here</a>--->
		                  <a class="dropdown-item" href="#buildURL(action='settings.disableEnableModule', querystring='moduleID=#rc.ModulesSettings.id#&com=1')#">Enable</a>
		                  </div>
		              </div>
		            </div>
		            <div class="card-block">
		              <h5 class="card-title alert-danger">#name#</h5>
		              <p class="card-text">#description#</p>
		              
		             
		            </div>
		          </div>
		        </div>
                </cfloop>
	     </div>             
            
            <!---cfdump var="#rc.modules#"--->
          </div>
          <cfloop query="#rc.ModulesSettings#">
          <cfscript>
	            ModulesConfSQL = 'Select * from modules_settings modset
					            WHERE modset.site_id = :siteID and modset.module_id=:modID';
                ModulesConfQuery = new Query();
                ModulesConfQuery.addParam(name='modID', value='#rc.ModulesSettings.id#');
                ModulesConfQuery.addParam(name='siteID', value='#rc.config.SiteID#');
                rc.ModulesConf = ModulesConfQuery.execute(sql=ModulesConfSQL).getResult();
          </cfscript>
          <div class="tab-pane" id="#lcase(name)#" aria-expanded="false">
            <div class="row">
	            <div class="col-sm-6">
	            <h4>#Name# Settings</h4>
	            </div>
	            
	            <div class="col-sm-6">
	                <a href="##modules" data-toggle="tab" aria-expanded="false" class="nav-link btn btn-info pull-right">Back to Modules</a>
	            </div>
            </div>
	        <cfloop query="#rc.Modulesconf#">
		        <cfif rc.modulesconf.module_setting_type is "Text" or rc.modulesconf.module_setting_type is "INT">
		            <!---Text & Int--->
			        <ui:form-row id="#rc.modulesconf.module_setting_key#" label="#rc.modulesconf.module_setting_key#" 
		           name="#rc.modulesconf.module_setting_key#" value="#rc.modulesconf.module_setting_value#"> 
			    <cfelseif rc.modulesconf.module_setting_type is "bool">
			        <!---Boolean--->
			        <ui:form-row type="boolean" id="#rc.modulesconf.module_setting_key#" label="#rc.modulesconf.module_setting_key#" 
			        name="#rc.modulesconf.module_setting_key#" value="#rc.modulesconf.module_setting_value#">    
		        </cfif>
	        </cfloop>
	        
	
	      </div>
	                
          </cfloop>
          
      </div>
     
</cfoutput>