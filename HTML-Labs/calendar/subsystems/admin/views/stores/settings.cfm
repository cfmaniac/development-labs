<cfimport taglib="/lib/tags/" prefix="ui">
<cfoutput>
  #view("partials/messages")#
  
 <ui:container title="Store : Settings" main="false" style="margin-top:0;" size="12" infotext="The Settings for your Store">
   <div class="card">
        <ul class="nav nav-tabs">
          <li class="nav-item">
            <a class="nav-link active" href="##core" data-toggle="tab">Core Settings</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="##first" data-toggle="tab">Product Settings</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="##second" data-toggle="tab">Payment Settings</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="##third" data-toggle="tab">Tax Settings</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="##fourth" data-toggle="tab">Shipping Settings</a>
          </li>
        </ul>
        <div class="card-block tab-content">
          <div class="tab-pane active" id="core">
              <h5>Core Settings</h5>
              <ui:form method="post" action="#buildURL('')#" name="modSettingsForm">
              <cfloop query="#rc.CoreSettings#">
              <cfif rc.CoreSettings.Module_setting_type is "bool">
              <ui:input label="#rc.CoreSettings.Module_setting_key#" type="boolean" name="#rc.CoreSettings.Module_setting_key#" value="#rc.CoreSettings.Module_setting_value#">
              <cfelseif rc.CoreSettings.Module_setting_type is "opt">
		      <ui:input label="#rc.CoreSettings.Module_setting_key#" type="select" options="#rc.CoreSettings.module_setting_options#" name="#rc.CoreSettings.Module_setting_key#" value="#rc.CoreSettings.Module_setting_value#">
	          <cfelseif rc.CoreSettings.Module_setting_type is "text">
		      <ui:form-row label="#rc.CoreSettings.Module_setting_key#" name="#rc.CoreSettings.Module_setting_key#" value="#rc.CoreSettings.Module_setting_value#"> 
	          </cfif>
			  </cfloop>
              </ui:form>
              <!---cfdump var='#rc.CoreSettings#' expand="false"--->
          </div>
          <div class="tab-pane " id="first">
              <h5>Product Settings</h5>
              <ui:form method="post" action="#buildURL('')#" name="modSettingsForm">
              <cfloop query="#rc.ProductSettings#">
              <cfif rc.ProductSettings.Module_setting_type is "bool">
              <ui:input label="#rc.ProductSettings.Module_setting_key#" type="boolean" name="#rc.ProductSettings.Module_setting_key#" value="#rc.ProductSettings.Module_setting_value#">
              <cfelseif rc.ProductSettings.Module_setting_type is "opt">
		      <ui:input label="#rc.ProductSettings.Module_setting_key#" type="select" options="#rc.ProductSettings.module_setting_options#" name="#rc.ProductSettings.Module_setting_key#" value="#rc.ProductSettings.Module_setting_value#">
	          <cfelseif rc.ProductSettings.Module_setting_type is "text">
		      <ui:form-row label="#rc.ProductSettings.Module_setting_key#" name="#rc.ProductSettings.Module_setting_key#" value="#rc.ProductSettings.Module_setting_value#"> 
	          </cfif>
			  </cfloop>
			  </ui:form>
          </div>
          <div class="tab-pane" id="second">
              <h5>Payment Settings</h5>
              <ui:form method="post" action="#buildURL('')#" name="modSettingsForm">
              <cfloop query="#rc.PaymentSettings#">
              <cfif rc.PaymentSettings.Module_setting_type is "bool">
              <ui:input label="#rc.PaymentSettings.Module_setting_key#" type="boolean" name="#rc.PaymentSettings.Module_setting_key#" value="#rc.PaymentSettings.Module_setting_value#">
              <cfelseif rc.PaymentSettings.Module_setting_type is "opt">
		      <ui:input label="#rc.PaymentSettings.Module_setting_key#" type="select" options="#rc.PaymentSettings.module_setting_options#" name="#rc.PaymentSettings.Module_setting_key#" value="#rc.PaymentSettings.Module_setting_value#">
	          <cfelseif rc.PaymentSettings.Module_setting_type is "text">
		      <ui:form-row label="#rc.PaymentSettings.Module_setting_key#" name="#rc.PaymentSettings.Module_setting_key#" value="#rc.PaymentSettings.Module_setting_value#"> 
	          </cfif>
			  </cfloop>
			  </ui:form>
          </div>
          <div class="tab-pane" id="third"> 
          <h5>Sales Tax Settings</h5>
           
           <ui:table tableID="cfdatatable" datatable="true" showheader="true" pagelength="10" columnsHidden="0" columns="State|Rate" showDelete="true">
              <cfloop array="#rc.TaxSettings#" index="tax">
                <ui:table-row id="#tax.getID()#" editColumn="1" 
                editLink="#tax.getID()#" 
                sortcol="0" 
                values="#tax.getState()#|#tax.getRate()#" 
                showdelete="true"></ui:table-row>
              </cfloop>    
           </ui:table>
          </div>
          <div class="tab-pane" id="fourth">
              <h5>Shipping Settings</h5>
              <ui:form method="post" action="#buildURL('')#" name="modSettingsForm">
              <cfloop query="#rc.ShippingSettings#">
              <cfif rc.ShippingSettings.Module_setting_type is "bool">
              <ui:input label="#rc.ShippingSettings.Module_setting_key#" type="boolean" name="#rc.ShippingSettings.Module_setting_key#" value="#rc.ShippingSettings.Module_setting_value#">
              <cfelseif rc.ShippingSettings.Module_setting_type is "opt">
		      <ui:input label="#rc.ShippingSettings.Module_setting_key#" type="select" options="#rc.ShippingSettings.module_setting_options#" name="#rc.ShippingSettings.Module_setting_key#" value="#rc.ShippingSettings.Module_setting_value#">
	          <cfelseif rc.ShippingSettings.Module_setting_type is "text">
		      <ui:form-row label="#rc.ShippingSettings.Module_setting_key#" name="#rc.ShippingSettings.Module_setting_key#" value="#rc.ShippingSettings.Module_setting_value#"> 
	          </cfif>
			  </cfloop>
			  </ui:form>
          </div>
        </div>
      </div>
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
     
    </ui:container>
   
</cfoutput>    