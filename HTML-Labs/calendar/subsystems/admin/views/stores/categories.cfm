<cfimport taglib="/lib/tags/" prefix="ui">
<cfoutput>
  #view("partials/messages")#
  
 <ui:container title="Store : Product Categories" main="false" style="margin-top:0;" size="12" infotext="The Store within your site">
    
   <!---ui:table tableID="cfdatatable" showChildRow="true" datatable="true" subrow="true" showheader="true" pagelength="10" columnsHidden="0" columns="Name|Active" showDelete="true">
      <cfloop array="#rc.Categories#" index="cat">
      <cfscript>
          rc.SubCat = entityload('Products_Categories', {parentID=#cat.GetID()#});
          /*catName = '#cat.GetName()#';
            if(arraylen(rc.SubCat)){
               for(subCat in rc.SubCat){
               catName = catName & '<br><i class="fa fa-chevron-right"></i> #subCat.getName()#<br>';    
               } 
            }*/
      </cfscript>
      <ui:table-row id="#cat.getID()#"
      editColumn="1" 
      editLink="maintin.cfm?#cat.getID()#" 
      sortcol="0" 
      values="#Cat.GetName()#|#yesNoFormat(cat.getIsActive())#" 
      showdelete="true" subrow="true" showChildRow="true"></ui:table-row>
        
      </cfloop>    
      </ui:table--->
      <table class="table table-striped">
		<thead>
			<tr>
				<th>Title</th>
				<th>Last Updated</th>
				<cfif rc.modules.pages.enableAddDelete>
					<th class="center"><abbr title="Add a page below this page in the site hierarchy">Add</abbr></th>
				</cfif>
				<th class="center"><abbr title="Sort the pages below this page in the site hierarchy">Sort</abbr></th>
				<th class="center">Edit</th>
				<cfif rc.modules.pages.enableAddDelete>
					<th class="center">Delete</th>
				</cfif>
			</tr>
		</thead>

		<tbody>
			<cfloop array="#rc.Categories#" index="cat">
			<cfscript>
              rc.SubCat = entityload('Products_Categories', {parentID=#cat.GetID()#});
            </cfscript>	
				<tr>
				    
                        
                    <td>
					<a href="#buildURL(action = 'stores.maintaincat', queryString = 'catid/#Cat.GetID()#')#" title="Edit"> #Cat.GetName()#</a> 
					</td>
				
				        
					
					<td title="last updated on "></td>
					
						<td class="center">
							
						</td>
					
					<td class="center">
						
					</td>
					<td class="center">
						
					</td>
					
						<td class="center">
							
						</td>
					
				</tr>
				<cfif arraylen(rc.Subcat)>
				        <cfloop array="#rc.Subcat#" index="subcat">
                        <tr>
                        <td>
                        <i class="fa fa-chevron-right"></i>    
                        <a href="#buildURL(action = 'stores.maintaincat', queryString = 'catid/#subCat.GetID()#')#" title="Edit"> #subCat.GetName()#</a> 
                        </td>
                        <td title="last updated on "></td>
					
						<td class="center">
							
						</td>
					
					<td class="center">
						
					</td>
					<td class="center">
						
					</td>
					
						<td class="center">
							
						</td>
                        </tr>  
				        </cfloop>
				</cfif>        
			</cfloop>
		</tbody>
		</table>
    </ui:container>
   
</cfoutput>    