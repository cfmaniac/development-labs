<cfimport taglib="/lib/tags/" prefix="ui">
<cfoutput>
  #view("partials/messages")#
  
 <ui:container title="Store Overview" main="false" style="margin-top:0;" size="12" infotext="Quick Stats on your Store">
   
    <div class="card-block p-t-0">
        <div class="row row-eq-height placeholders">
                <div class="col-xs-3 placeholder text-center">
                    <img src="//placehold.it/100/d6d6d6/fff?text=#arrayLen(rc.NewOrders)#" class="center-block img-responsive img-circle" alt="Generic placeholder thumbnail">
                    <h5><a href="#buildURL('stores.orders')#"><i class="fa fa-truck"></i> New Orders </a></h5>
                    <span class="text-muted">New Orders</span>
                </div>
                
                <div class="col-xs-3 placeholder text-center">
                    <img src="//placehold.it/100/dddddd/fff?text=#arrayLen(rc.AllOrders)#" class="center-block img-responsive img-circle" alt="Generic placeholder thumbnail">
                    <h5><a href="#buildURL('stores.orders')#"><i class="fa fa-shopping-cart"></i> Total Orders</a></h5>
                    <span class="text-muted">Total Orders</span>
                </div>
                <div class="col-xs-3 placeholder text-center">
                    <img src="//placehold.it/100/e4e4e4/fff?text=#arrayLEn(rc.AllProducts)#" class="center-block img-responsive img-circle" alt="Generic placeholder thumbnail">
                    <h5><a href="#buildURL('stores.products')#"><i class="fa fa-shopping-basket"></i> Products</a></h5>
                    <span class="text-muted">Total Products </span>
                    
                </div>
                <div class="col-xs-3 placeholder text-center">
                    <img src="//placehold.it/100/e4e4e4/fff?text=#arrayLEn(rc.AllPromos)#" class="center-block img-responsive img-circle" alt="Generic placeholder thumbnail">
                    <h5><a href="#buildURL('stores.promos')#"><i class="fa fa-bullhorn"></i> Promotions</a></h5>
                    <span class="text-muted">Total Promotions </span>
                    
                </div>
               
                
                
        </div>
        
        
    </div>  
    </ui:container>
    
    <ui:container title="New Orders" main="false" style="margin-top:0;" size="3" infotext="Newest Orders recieved">
        <div class="card-block p-t-0">
        <div class="row row-eq-height placeholders">
        <cfif arrayLen(rc.NewOrders)>
                    <cfloop array="#rc.NewOrders#" index="order">
                        <cfscript>
                            rc.Customer = entityLoadByPK('Customers', #order.getCustomerID()#);
                        </cfscript>
                        <a href="#buildURL('stores.orders')#" class="dropdown-item">
                    
                        <div class="media">
                            <div class="media-left media-middle hidden-sm-down" href="#buildURL('contact')#">
                                <i class="fa fa-shopping-basket"></i>
                            </div>
                            <div class="media-body media-middle">
                                <small class="text-muted pull-xs-right"></small>
                                <p class="m-a-0"><cfif structKeyExists(rc, 'Customer')>
                                #rc.Customer.getFirstName()# #rc.Customer.getLastName()#
                                <cfelse>
                                Guest Checkout
                                </cfif></p>
                                <p class="small m-a-0 text-muted">Order Total: #dollarFormat(order.getTotal())#</p>
                            </div>
                        </div>
                    </a> 
                    </cfloop>
                    <cfelse>
                     <a href="##" class="dropdown-Item">
		                   <div class="media">
		                    <p class="small m-a-0 text-muted">No New Orders</p>
		                   </div>
		            </a> 
                    </cfif>
        </div>
        </div>
    </ui:container>
</cfoutput>    