<cfimport taglib="/lib/tags/" prefix="ui">
<cfoutput>
  #view("partials/messages")#
  
 <ui:container title="Store : Order Statuses" main="false" style="margin-top:0;" size="12" infotext="The Store within your site">
   
   <ui:table tableID="cfdatatable" datatable="true" showheader="true" pagelength="10" columnsHidden="0" 
   columns="Name|CreatedAt" showDelete="true">
      <cfloop array="#rc.Statuses#" index="status">
        <ui:table-row id="#status.GetID()#" editColumn="1" 
        editLink="maintin.cfm?#status.GetID()#" 
        sortcol="0" values="#status.GetName()#|#dateFormat(status.getCreatedAt(), "full")#" showdelete="true"></ui:table-row>
      </cfloop>    
      </ui:table>
     
    </ui:container>
   
</cfoutput>    