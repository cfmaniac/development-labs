<cfimport taglib="/lib/tags/" prefix="ui">
<cfoutput>
  #view("partials/messages")#
  
 <ui:container title="Store : Promotions" main="false" style="margin-top:0;" size="12" infotext="">
   
   <ui:table tableID="cfdatatable" datatable="true" showheader="true" pagelength="10" columnsHidden="0" columns="Name|Promo Code|Value|Percent Off" showDelete="true">
      <cfloop array="#rc.Promos#" index="promo">
        <ui:table-row id="#promo.getID()#" 
        editColumn="1" 
        editLink="#BuildURL(action='stores.maintainpromo', querystring='id/#promo.getID()#')#" 
        sortcol="0" 
        values="#promo.getName()#|<em>#promo.getCode()#</em>|#promo.getValue()#|#yesNoFormat(promo.getisPercentOff())#" 
        showdelete="true"></ui:table-row>
      </cfloop>    
      </ui:table>
     
    </ui:container>
   
</cfoutput>    