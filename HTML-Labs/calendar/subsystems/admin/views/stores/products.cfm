<cfimport taglib="/lib/tags/" prefix="ui">
<cfoutput>
  #view("partials/messages")#
  
 <ui:container title="Store : Products" main="false" style="margin-top:0;" size="12" infotext="">
   
   <ui:table tableID="cfdatatable" datatable="true" sortcol="2" 
   showheader="true" pagelength="10" 
   columnsHidden="0" columns="Image|Name|Product Code|Featured|Price" 
   showDelete="true" colsnosort="1,6">
      <cfloop array="#rc.Products#" index="product">
      <cfif len(product.getProductCode())>
          <cfset ProdCode = #product.GetProductCode()#>
      <cfelse>
          <cfset Prodcode = '&nbsp;'>
      </cfif>
      <cfloop array="#product.getImages()#" index="image">
        <cfset ProdImage = #image.getFilePath()#>
      </cfloop>
        <ui:table-row id="#product.getID()#" 
        editColumn="2" 
        editLink="#buildURL(action='store.maintainprod', querystring='?id=#product.GetID()#')#" 
         
        values="<img src='#prodImage#' width='75' height='75' class='img-responsive'>|#product.getName()#|#prodCode#|#yesNoFormat(product.getisFeatured())#|#dollarFormat(product.GetBasePRice())#" 
        showdelete="true"></ui:table-row>
      </cfloop>    
   </ui:table>
     
    </ui:container>
    
</cfoutput>    