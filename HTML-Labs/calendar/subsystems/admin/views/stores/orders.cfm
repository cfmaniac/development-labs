<cfimport taglib="/lib/tags/" prefix="ui">
<cfoutput>
  #view("partials/messages")#
  
 <ui:container title="Store : New Orders" main="false" style="margin-top:0;" size="12" infotext="">
    
   <ui:table tableID="cfdatatable" datatable="true" showheader="true" pagelength="50" columnsHidden="0" 
   columns="Name|Total|Order Date" showDelete="true">
      <cfloop array="#rc.NewOrders#" index="order">
      <cfscript>
        rc.Customer = entityLoadByPK('Customers', #order.getCustomerID()#);
      </cfscript>
      <cfif structKeyExists(rc, 'Customer')>
          <cfset Customer = "#rc.Customer.getFirstName()# #rc.Customer.getLastName()#">
            <cfelse>
            <cfset Customer ="Guest Checkout">
            </cfif>
        <ui:table-row id="#order.getID()#"
        editColumn="1" class="table-success"
        editLink="#buildURL(action='store.orderdetails', querystring='id/#order.getId()#')#" 
        sortcol="0" 
        values="#Customer#|#dollarFormat(order.getTotal())#|#dateformat(order.getCreatedAt(), "MM/DD/YYYY")#" showdelete="true"></ui:table-row>
      </cfloop>    
      </ui:table>
     
    </ui:container>
   
   
    <ui:container title="Store : All Orders" main="false" style="margin-top:0;" size="12" infotext="">
    
   <ui:table tableID="cfdatatable2" datatable="true" showheader="true" pagelength="50" columnsHidden="0" 
   columns="Name|Total|Status|Order Date" showDelete="true">
      <cfloop array="#rc.AllOrders#" index="order">
      <cfscript>
        rc.Status = entityLoadByPK('Orders_Statuses', #order.getStatusID()#);
        rc.Customer = entityLoadByPK('Customers', #order.getCustomerID()#);
      </cfscript>
      <cfif structKeyExists(rc, 'Customer')>
          <cfset Customer = "#rc.Customer.getFirstName()# #rc.Customer.getLastName()#">
            <cfelse>
            <cfset Customer ="Guest Checkout">
            </cfif>
        <ui:table-row id="#order.getID()#"
        editColumn="1" class="table-success"
        editLink="#buildURL(action='store.orderdetails', querystring='id/#order.getId()#')#" 
        sortcol="0" 
        values="#Customer#|#dollarFormat(order.getTotal())#|#rc.Status.getName()#|#dateformat(order.getCreatedAt(), "MM/DD/YYYY")#" showdelete="true"></ui:table-row>
      </cfloop>    
      </ui:table>
     
    </ui:container>
</cfoutput>    