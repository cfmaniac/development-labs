<cfoutput>
	<ul class="sidebar-submenu">
          <li class="sidebar-menu-item">
            <a class="sidebar-menu-button" href="#buildURL('stores.products')#">Products</a>
          </li>
          <li class="sidebar-menu-item">
            <a class="sidebar-menu-button" href="#buildURL('stores.categories')#">Categories</a>
          </li>
          <li class="sidebar-menu-item">
            <a class="sidebar-menu-button" href="#buildURL('stores.promos')#">Promotions</a>
          </li>
          <li class="sidebar-menu-item">
            <a class="sidebar-menu-button" href="#buildURL('stores.orders')#">Orders</a>
          </li>
          <li class="sidebar-menu-item">
            <a class="sidebar-menu-button" href="#buildURL('stores.settings')#">Settings</a>
          </li>
        </ul>
</cfoutput>