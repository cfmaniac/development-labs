<cfscript>
    
		rc.pagination.setItemsPerPage(5);
		rc.pagination.setUrlPageIndicator("view");
		rc.pagination.setShowNumericLinks(true);
      rc.pagination.setQuerytoPaginate(rc.ActiveCompanies);
      rc.pagination.setMissingNumbersHTML('...');
      rc.pagination.setBaseLink("#buildURL('listings')#");
      rc.pagination.setClassName('pull-right');
   
</cfscript>
<cfoutput>
	
  <div class="content-wrapper">
    <h1 class="page-heading">#rc.currentsite.sitename# Exclusive Listings : #NumberFormat(rc.ActiveBuinessesCount)#</h1>
    
    
    #view("partials/messages")#
    
    <div class="row">
   <div class="col-sm-12">
   <div class="card">
      <div class="card-block">
        <div class="media">
          <div class="media-body media-middle">
            <h4 class="m-b-0">Exclusive Listings</h4>
            <p class="text-muted m-b-0"></p>
          </div>
        </div>    
      </div>
      
      <div class="card-block p-t-0">
      <table class="table table-first-column-number data-table display full">
		<thead>
			<tr>
			   
				<th class="col-sm-3">Company Name</th>
				<th class="col-sm-2">City</th>
				<th class="center col-sm-1">State</th>
				<th class="center col-sm-1">ZipCode</th>
				<th class="center col-sm-2">Vertical Domain(s)</th>
				<th class="center col-sm-1">Listings</th>
				<th class="center col-sm-1">Active</th>
				<th class="center col-sm-1">Actions</th>
			</tr>
		</thead>
		
		<tbody>
		<!---#rc.pagination.getStartRow()# | #rc.pagination.getEndRow()#--->
<cfloop query="#rc.ActiveCompanies#" startrow="#rc.pagination.getStartRow()#" endrow="#rc.pagination.getEndRow()#">
          <cfscript>
             rc.CompanyVerticals = new query(sql='Select Title from sites 
             Inner Join companies_verticals on sites.ID=companies_verticals.verticalID
             where companies_verticals.companyID = #rc.ActiveCompanies.CompanyID#').execute().getResult();
             rc.CompanyExclusives = new query(sql='Select Count(*) as ExclusiveCount from companies_listings
             INNER JOIN companies on companies.companyID = companies_listings.companyID
             where companies_listings.listingExclusive=1 and companies_listings.companyID=#rc.ActiveCompanies.CompanyID# and companies_listings.listingActive=1').execute().getResult();
             rc.CompanyExclusivesOff = new query(sql='Select Count(*) as ExclusiveCount from companies_listings
             INNER JOIN companies on companies.companyID = companies_listings.companyID
             where companies_listings.listingExclusive =1 and companies_listings.companyID=#rc.ActiveCompanies.CompanyID# and companies_listings.listingActive=0').execute().getResult();
          </cfscript>
    
          <tr>
			    <td class="col-sm-3"><a href="#buildURL(action='listings.maintain', queryString='id=#rc.ActiveCompanies.CompanyID#')#"><strong>#Name#</strong></a></td>
				<td class="col-sm-2">#AddressCity#</td>
				<td class="center col-sm-1">#AddressState#</td>
				<td class="center col-sm-1">#AddressZip#</td>
				<td class="center col-sm-2">
				<cfloop query="rc.CompanyVerticals">
				#Title#<br>
				</cfloop>
				
				</td>
				<td class="center col-sm-1"><span class="badge badge-success">#numberformat(rc.CompanyExclusives.ExclusiveCount)#</span> Active<br>
				<span class="badge badge-important">#numberformat(rc.CompanyExclusivesOff.ExclusiveCount)#</span> InActive
				</td>
				<td class="center col-sm-1">#yesnoFormat(active)#</td>
				<td class="center col-sm-1"><a href="#buildURL(action='listings.maintain', queryString='id=#rc.ActiveCompanies.CompanyID#')#" class="btn btn-info">Edit</a></td>
			</tr>
</cfloop>
</tbody>
</table>
<div class="row" style="text-align: right;">
	#rc.pagination.getRenderedHTML()#
</div>

      
      </div>
    </div>
    </div>
    
    </div>
  </div>
</cfoutput>
	