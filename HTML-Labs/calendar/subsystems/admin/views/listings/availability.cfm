<cfimport taglib="/lib/tags/" prefix="ui">
<cfscript>
    param name='rc.region' default='';
    param name='rc.verticalID' default='0';
    if (structKeyExists(rc, 'region')) {
	    rc.region = '#rc.region#';
	    rc.verticalid = '#rc.verticalID#';
    }
    
    
    
</cfscript>
<cfoutput>
  <div class="content-wrapper">
    <h1 class="page-heading">#rc.currentsite.sitename# Exclusive Listings : Availability Search</h1>
    
    
    #view("partials/messages")#
    
    <div class="row">
   <div class="col-sm-12">
   <div class="card">
      <div class="card-block">
        <div class="media">
          <div class="media-body media-middle">
            <h4 class="m-b-0">Exclusive Listings : Availability  <cfif len(rc.region)>in #rc.region#</cfif></h4>
            <p class="text-muted m-b-0"></p>
          </div>
        </div>    
      </div>
      
      <div class="card-block p-t-0">
      <form action="#buildURL('listings.availability')#" method="post" class="form-horizontal" id="page-form">
          <p></p>
          <div class="row">
	          <div class="col-sm-6">
		          #rc.regions.stateSelect(name='region', id='state', icon='', width='12', label="State Availability", default='#rc.region#', allownone=true, value='abv2')#
	          </div>
	          <div class="col-sm-6">
	           <div class="form-group row">
	   
				   <label class="col-sm-12 form-control-label">
				    Vertical Availability
			    </label>
	    
	    
		
	              <select name="verticalID" class="form-control">
	              <option value="0">All Verticals</option>
	              <cfloop query="#rc.Verticals#">
	              <option value="#ID#" <cfif rc.VerticalID EQ ID>selected="selected"</cfif>>#Title# (#domain#)</option>
	              </cfloop>  
		          </select>
		          </div>
	          </div>
          </div>
	      <div class="row">
		      <button class="btn btn-success btn-lg pull-right" type="submit">Check Availability in <span id="selection"></span></button>
	      </div> 
	      
      </form>
      
      <script>
	      $('##state').on('change', function(){
	        var region = $("##state option:selected").text();
	        $('##selection').html(region);
	      });
	      <cfif len(rc.region)>
		      $('##state').change();
	      </cfif>
	      
      </script>
      <cfif len(rc.region)>
      <p></p>
	      <cfscript>
		      CountySQL = "Select Distinct(County) from zips where state=:state Order by County";
		      rc.County = new Query();
		      rc.County.addParam(name='state', value='#rc.region#');
		      rc.Counties = rc.County.execute(sql=CountySQL).getResult();
		  </cfscript>
		  
		  <ui:table tableID="AvailTable" datatable="true" showheader="true" pagelength="50" columnsHidden="0" sortcol="1"
            sortOrder="asc" columns="County Name|Total Zips|Status" showDelete="false" scroller="true">
	        <cfloop query="#rc.Counties#">
	        <cfscript>
		      rc.available = 'true';
		      rc.status = 'Available';
		      rc.avail = 'table-success';
		      
			  ZipsSQL = "select count(zip) as TotalZips from zips where county=:county";
			  rc.zips = new Query();
		      rc.zips.addParam(name='county', value='#rc.Counties.County#');
		      rc.ZipsCount = rc.zips.execute(sql=zipsSQL).getResult();
		      
		      if(rc.verticalID == '0'){
			    rc.ExclusiveZips = new Query(sql = "select companyID, ListingZipCode, ListingExclusive, ListingActive, VerticalID from Companies_Listings
                                        INNER JOIN Zips on companies_listings.Listingzipcode = zips.zip
                                        WHERE zips.zip = companies_listings.Listingzipcode and county='#rc.counties.county#'").execute().getResult();
                 
		      }else{
			     rc.ExclusiveZips = new Query(sql = "select companyID, ListingZipCode, ListingExclusive, ListingActive, VerticalID from Companies_Listings
                                        INNER JOIN Zips on companies_listings.Listingzipcode = zips.zip
                                        WHERE zips.zip = companies_listings.Listingzipcode and county='#rc.counties.county#' and companies_listings.verticalID=#rc.verticalID#").execute().getResult();
                
		      }
		                               
              if(rc.ExclusiveZips.recordCount GTE 1){
		        rc.avail = 'table-danger';
		        rc.status = 'unAvailable';
              }                        
		      </cfscript>  
	            <ui:table-row id="#rc.counties.currentrow#" 
	            editColumn="0" class="#rc.avail#"
	            showdelete="false"
	            values="#County#|#rc.zipsCount.TotalZips#|#rc.status#">
	            </ui:table-row> 
	        </cfloop>
          </ui:table>
		
      </cfif>
      
      
      
      
      </div>
   </div>
   </div>
   </div> 
   
   </div> 
   
</cfoutput>    