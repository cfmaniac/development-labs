<cffunction name="SelectState">
 
    <cfargument    name="selected"    default="Select State:">
    <cfargument    name="name"      default="state">
    <cfargument    name="id"          default="#name#">
    <cfargument    name="omit"      default="">
    <cfargument    name="class"      default="form-control">
    <cfargument    name="ability"  default="enabled">
    <cfargument    name="onchange" default="">
    <cfargument    name="value"      default="abbreviations">
    <cfargument    name="label"      default="fullnames">
    <cfset abbreviations=
            "00,AL,AK,AZ,AR,CA,CO,CT,DE,DC,FL,GA,HI,ID,IL,IN,IA,KS,KY,LA,ME,MD,MA,MI,MN,MS,MO,MT,NE,NV,NH,NJ,NM,NY,NC,ND,OH,OK,OR,PA,PR,RI,SC,SD,TN,TX,UT,VT,VA,WA,WV,WI,WY,--">
    <cfset fullnames=
            "Select State:,Alabama,Alaska,Arizona,Arkansas,California,Colorado,Connecticut,Delaware,D.C.,Florida,Georgia,Hawaii,Idaho,Illinois,Indiana,Iowa,Kansas,Kentucky,Louisiana,Maine,Maryland,Massachusetts,Michigan,Minnesota,Mississippi,Missouri,Montana,Nebraska,Nevada,New Hampshire,New Jersey,New Mexico,New York,North Carolina,North Dakota,Ohio,Oklahoma,Oregon,Pennsylvania,Puerto Rico,Rhode Island,South Carolina,South Dakota,Tennessee,Texas,Utah,Vermont,Virginia,Washington,West Virginia,Wisconsin,Wyoming,NOT IN USA">
 
    <cfoutput>
    <select
        name="#name#" id="#id#" #ability# class="#class#"
        <cfif isdefined("style")>style="#style#"</cfif>
         <cfif isdefined("onchange")>onchange="#onchange#"</cfif>
    >
    <cfloop from="1" to="#listlen(fullnames)#" index="state">
        <cfif not listcontains(omit,#listgetat(evaluate(value), state)#) and not listcontains(omit,#listgetat(evaluate(label), state)#)>
            <option value="#listgetat(evaluate(value), state)#"
                <cfif #listgetat(evaluate(value), state)# is "#selected#">selected</cfif>
                 <cfif isdefined("style")>style="#style#"</cfif>>
                #listgetat(evaluate(label), state)#
            </option>
        </cfif>
    </cfloop>
    </select>
    </cfoutput>
 
</cffunction>
<cfscript>
	if(cgi.rc.method is "post"){
    // A Form has been submitted: but which?
    if (StructKeyExists(Form,'submitCompanyInfo')){
        //Company Info Updated
        
        UpdateInfoSQL = 'update companies set name=:name, url=:url, email=:email where companyid=:companyID';
        CompanyInfo = new query();
        CompanyInfo.addParam(name="name", value="#form.name#",cfsqltype="cf_sql_varchar");
        CompanyInfo.addParam(name="url", value="#form.url#",cfsqltype="cf_sql_varchar");
        CompanyInfo.addParam(name="email", value="#form.email#",cfsqltype="cf_sql_varchar");
        CompanyInfo.addParam(name="companyID", value="#rc.id#",cfsqltype="cf_sql_integer");
        CompanyInfoUpdate = companyinfo.execute(sql=UpdateInfoSQL).getresult();
       
        location( url="#buildURL(action='companies.maintain', querystring='id=#rc.id#&message=Company Info Successfully Updated')#", addtoken="false" );
    } else if (StructKeyExists(Form,'AddCompanyInfo')) {
        //Adding a New Company
        AddCompanySQL = 'insert into companies (name, slug, url, email, startdate, demo,active) VALUES ( :name, :slug, :url, :email, :startdate,:demo, :active)';
        CompanyInfo = new query( );

        CompanyInfo.addParam( name = "name", value = "#form.name#", cfsqltype = "cf_sql_varchar" );
        CompanyInfo.addParam( name = "slug", value = "#ReReplace( LCase( form.name ), "[^a-z0-9]{1,}", "-", "all" )#", cfsqltype = "cf_sql_varchar" );
        CompanyInfo.addParam( name = "url", value = "#form.url#", cfsqltype = "cf_sql_varchar" );
        CompanyInfo.addParam( name = "email", value = "#form.email#", cfsqltype = "cf_sql_varchar" );
        CompanyInfo.addParam( name = "startdate", value = "#dateformat( now( ), "YYYY-MM-DD" )#", cfsqltype = "cf_sql_date" );
        CompanyInfo.addParam( name = "demo", value = "0", cfsqltype = "cf_sql_boolean" );
        CompanyInfo.addParam( name = "active", value = "1", cfsqltype = "cf_sql_boolean" );
        CompanyInfoUpdate = companyinfo.execute( sql = AddCompanySQL ).getresult( );

        CompanyID = new Query( sql = "SELECT @@IDENTITY AS CompanyID FROM Companies" ).execute( ).getREsult( );
        rc.ID = CompanyID.CompanyID;

        //Inserts Company Settings:
        CompanySettingsSQL = 'insert into companies_settings(companyid) VALUES (#rc.id#)';
        InsertCompanySettings.execute( sql = CompanySettingsSQL ).getresult( );
        
        //Inserts Company Profile:
        CompanyProfileSQL = 'insert into companies_profile(companyid) VALUES (#rc.id#)';
        InsertCompanyProfile.execute( sql = CompanyProfileSQL ).getresult( );

        //Inserts Address:
        UpdateAddySQL = 'insert into companies_address (AddressStreet,AddressStreet2,AddressCity,AddressState,AddressZip,companyID) VALUES (:street, :street2, :city, :state, :zip, :companyID)';
        CompanyAddy = new query( );
        CompanyAddy.addParam( name = "street", value = "#form.address#", cfsqltype = "cf_sql_varchar" );
        CompanyAddy.addParam( name = "street2", value = "#form.address2#", cfsqltype = "cf_sql_varchar" );
        CompanyAddy.addParam( name = "city", value = "#form.city#", cfsqltype = "cf_sql_varchar" );
        CompanyAddy.addParam( name = "state", value = "#form.state#", cfsqltype = "cf_sql_varchar" );
        CompanyAddy.addParam( name = "zip", value = "#form.zipcode#", cfsqltype = "cf_sql_varchar" );
        CompanyAddy.addParam( name = "companyId", value = "#rc.id#", cfsqltype = "cf_sql_integer" );
        CompanyAddyUpdate = companyaddy.execute( sql = UpdateAddySQL ).getresult( );

        //Inserts Verticals:
        for( i = 1; i <= ListLen( #form.verticalID# ); i++) {
            value = ListGetAt( #form.verticalID#, i );
            sql = "Insert into companies_verticals (companyID, verticalID, isFreebie) VALUES (#rc.id#, #value#, 0)";
            NewVert = new Query( sql = #sql# ).execute( ).getResult( );

        }

        //Update business LAT LON
        ListingGeoInfo = new subsystems.admin.controllers.Maps( ).getCoords(
            Address1 = Trim( form.address )
            , City = Trim( form.City )
            , State = Trim( form.State )
            , Zipcode = ReReplace( form.Zipcode, '[^0-9]', '', 'ALL' )
            );
        if( ListingGeoInfo.Error is "" ) {
            CompanyGEOSQL = "UPDATE	companies_address SET CompanyLAT = :lat, CompanyLON = :lon WHERE companyID = :CompanyID";
            CompanyGEO = new query( );
            CompanyGEO.addParam( name = "lat", value = "#ListingGeoInfo.Latitude#", cfsqltype = "cf_sql_varchar" );
            CompanyGEO.addParam( name = "lon", value = "#ListingGeoInfo.Longitude#", cfsqltype = "cf_sql_varchar" );
            CompanyGEO.addParam( name = "companyId", value = "#rc.id#", cfsqltype = "cf_sql_integer" );
            CompanyGEOUpdate = companygeo.execute( sql = CompanyGEOSQL ).getresult( );

        }

        location( url = "#buildURL( action = 'companies.maintain', querystring = 'id=#rc.id#&message=Company Successfully Added' )#", addtoken = "false" );
    } else if (StructKeyExists(Form,'newBilling')){
        //New Company Billing Entry:
        BillingSQL = "INSERT into companies_billing (companyID, BillingTerm, BillingStatus, BillingPrice, BillingNotes, BillingDate) VALUES (:companyID, :BillingTerm, :BillingStatus, :BillingPrice, :BillingNotes, :billingDate)";
		AddBilling = new query( );
		AddBilling.addparam(name="companyID", value="#rc.id#",cfsqltype="cf_sql_integer");
		AddBilling.addParam(name="BillingTerm", value="#form.billingTerm#",cfsqltype="cf_sql_integer");
		AddBilling.addParam(name="BillingStatus", value="#form.billingStatus#",cfsqltype="cf_sql_varchar");
		AddBilling.addParam(name="BillingPrice", value="#form.billingPrice#",cfsqltype="cf_sql_varchar");
		AddBilling.addParam(name="BillingNotes", value="#form.billingNotes#",cfsqltype="cf_sql_varchar");
		AddBilling.addParam(name="BillingDate", value="#dateformat(now(), 'YYYY-MM-DD')#",cfsqltype="cf_sql_date");
		AddBillingInsert = AddBilling.execute(sql=BillingSQL).getResult();    
        
        location( url = "#buildURL( action = 'companies.maintain', querystring = 'id=#rc.id#&message=Company Billing Updated' )#", addtoken = "false" );
        
    } else if (StructKeyExists(Form,'submitAddy')){
        //Company Address
	    UpdateAddySQL = 'update companies_address set AddressStreet=:street, AddressStreet2=:street2, AddressCity=:city, AddressState=:state, AddressZip=:zip, CompanyPHone=:phone, companyFax=:fax, CompanyMobile=:mobile, CompanyEmail=:email where companyID=:companyID';
	    CompanyAddy = new query();
        CompanyAddy.addParam(name="street", value="#form.address#",cfsqltype="cf_sql_varchar");
        CompanyAddy.addParam(name="street2", value="#form.address2#",cfsqltype="cf_sql_varchar");
        CompanyAddy.addParam(name="city", value="#form.city#",cfsqltype="cf_sql_varchar");
        CompanyAddy.addParam(name="state", value="#form.state#",cfsqltype="cf_sql_varchar");
        CompanyAddy.addParam(name="zip", value="#form.zipcode#",cfsqltype="cf_sql_varchar");
        CompanyAddy.addParam(name="phone", value="#form.phone#",cfsqltype="cf_sql_varchar");
        CompanyAddy.addParam(name="fax", value="#form.fax#",cfsqltype="cf_sql_varchar");
        CompanyAddy.addParam(name="mobile", value="#form.mobile#",cfsqltype="cf_sql_varchar");
        CompanyAddy.addParam(name="email", value="#form.email#",cfsqltype="cf_sql_varchar");
        CompanyAddy.addParam(name="companyId", value="#rc.id#",cfsqltype="cf_sql_integer");
        CompanyAddyUpdate = companyAddy.execute(sql=UpdateAddySQL).getresult();
        
        //Update business LAT LON
		ListingGeoInfo = new lib.plugins.Maps().getCoords(
			 Address1	= Trim(form.address)
			,City		= Trim(form.City)
			,State		= Trim(form.State)
			,Zipcode	= ReReplace(form.Zipcode, '[^0-9]', '', 'ALL')
		    );
	if (ListingGeoInfo.Error is ""){
		CompanyGEOSQL = "UPDATE	companies_address SET CompanyLAT = :lat, CompanyLON = :lon WHERE companyID = :CompanyID";
		CompanyGEO = new query();
        CompanyGEO.addParam(name="lat", value="#ListingGeoInfo.Latitude#",cfsqltype="cf_sql_varchar");
        CompanyGEO.addParam(name="lon", value="#ListingGeoInfo.Longitude#",cfsqltype="cf_sql_varchar");
        CompanyGEO.addParam(name="companyId", value="#rc.id#",cfsqltype="cf_sql_integer");
        CompanyGEOUpdate = companygeo.execute(sql=CompanyGEOSQL).getresult();
		
	}
        
        location( url="#buildURL(action='companies.maintain', querystring='id=#rc.id#&message=Company Address Info Successfully Updated')#", addtoken="false" );
	    
    } else if (StructKeyExists(Form,'submitSettings')){
        //Company Settings
	    
	    writeDump(var=#form#, abort=true);
    } else if (StructKeyExists(Form,'addLocation')){
        //Company Settings
	    UpdateAddySQL = 'insert into companies_address (AddressStreet,AddressStreet2,AddressCity,AddressState,AddressZip,companyID) VALUES (:street, :street2, :city, :state, :zip, :companyID)';
	    CompanyAddy = new query();
        CompanyAddy.addParam(name="street", value="#form.address#",cfsqltype="cf_sql_varchar");
        CompanyAddy.addParam(name="street2", value="#form.address2#",cfsqltype="cf_sql_varchar");
        CompanyAddy.addParam(name="city", value="#form.city#",cfsqltype="cf_sql_varchar");
        CompanyAddy.addParam(name="state", value="#form.state#",cfsqltype="cf_sql_varchar");
        CompanyAddy.addParam(name="zip", value="#form.zipcode#",cfsqltype="cf_sql_varchar");
        CompanyAddy.addParam(name="companyId", value="#rc.id#",cfsqltype="cf_sql_integer");
        CompanyAddyUpdate = companyaddy.execute(sql=UpdateAddySQL).getresult();
	   
	   location( url="#buildURL(action='companies.maintain', querystring='id=#rc.id#&message=New Location Successfully added')#", addtoken="false" );
    }
    //Other Forms are submitted via AJAX;
        
    //		
		
		
		
	}
</cfscript>

<cfoutput>
 <div class="col-sm-12 main-content">
   <h2><cfif #parameterExists(rc.id)#>Editting Company : #rc.CompanyInfo.Name#<cfelse>Adding Company:</cfif></h2>
   
   <cfif #parameterExists(rc.id)#>
    #view('listings/includes/maintaincompany')#
   <cfelse>
   #view('listings/includes/addcompany')#
   </cfif> 
   
   
   
   
   
</cfoutput>
