<p class="alert alert-info">After you add a company, additional options will become available.</p>
<ul class="nav nav-tabs" role="tablist">
     <li role="presentation" class="active"><a href="##info" role="tab" data-toggle="tab">Information</a></li>
     <li role="presentation"><a href="##addy" role="tab" data-toggle="tab">Address</a></li>
     <li role="presentation"><a href="##verts" role="tab" data-toggle="tab">Verticals</a></li>
    
    
     
   </ul>
   <form name="companyInfo" method="post">
   <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="info">
      <h2>Company Information</h2>
      
      <div class="form-group">
					<label class="control-label" for="metakeywords">Company Name </label>
					<div class="controls">
						<input type="text" name="name" id="name"  class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">URL </label>
					<div class="controls">
						<input type="text" name="url" id="url"  class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Company Email</label>
					<div class="controls">
						<input type="text" name="email" id="email"  class="form-control">
						
					</div>
		</div>
		
		
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation"><a class="btn btn-info pull-right" href="##addy" role="tab" data-toggle="tab">Next &rAarr;</a></li>
        </ul>

      
     
      </div>
      <div role="tabpanel" class="tab-pane " id="addy">
      <h2>Company Address</h2>
      
      <div class="form-group">
					<label class="control-label" for="metakeywords">Location Address</label>
					<div class="controls">
						<input type="text" name="Address" id="Address" value="" class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Location Address 2</label>
					<div class="controls">
						<input type="text" name="Address2" id="Address2" value="" class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Location City</label>
					<div class="controls">
						<input type="text" name="city" id="city" value="" class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Location State</label>
					<div class="controls">
						<!---input type="text" name="state" id="state" value="" class="form-control"--->
						<cfoutput>#SelectState()#</cfoutput>
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Location ZipCode</label>
					<div class="controls">
						<input type="text" name="zipcode" id="zipcode" value="" class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Location Phone</label>
					<div class="controls">
						<input type="text" name="phone" id="phone" value="" class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Location Fax</label>
					<div class="controls">
						<input type="text" name="fax" id="fax" value="" class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Location Mobile</label>
					<div class="controls">
						<input type="text" name="mobile" id="mobile" value="" class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Location Email</label>
					<div class="controls">
						<input type="text" name="email" id="email" value="" class="form-control">
						
					</div>
		</div>
		
		
         <ul class="nav nav-tabs" role="tablist">
          <li role="presentation"><a class="btn btn-info pull-right" href="##verts" role="tab" data-toggle="tab">Next &rAarr;</a></li>
        </ul>
      
     
      </div>
      <div role="tabpanel" class="tab-pane " id="verts">
      <h2>Company Verticals</h2>
      
      <table class="table table-first-column-number data-table display full">
			<thead>
				<tr>
					<th class="col-sm-1">&nbsp;</th>
					<th class="col-sm-10">Vertical Domain</th>
					<th class="col-sm-1">Status</th>
					
				</tr>
			</thead>
			
			<tbody>
	  <cfoutput>
	          <cfloop query="rc.Verticals">
	         
		     <tr>
			     <td class="col-sm-1"><div class="form-group">
					<div class="controls">
					<input type="checkbox" name="verticalID" value="#rc.Verticals.siteVerticalid#" class="change-color-switch Updatevertical" data-on-color="success" data-off-color="danger">
						
					</div>
		</div>  </td>
			     <td class="col-sm-10">#siteDomain#</td>
			     <td class="col-sm-1"></td>
		     </tr>
	       
	       </cfloop>
	       </cfoutput>
		   </tbody>
		   </table>
		
		
         
      
     <div class="form-group">
                    
					<input type="submit" name="AddCompanyInfo" class="btn btn-success pull-right" value="Add Company">
					
		</div>
      </div>
      
      
       
   </form>
   </div>
   <script>
    $('.change-color-switch').bootstrapSwitch('onColor', 'success');
    $('.change-color-switch').bootstrapSwitch('offColor', 'danger');
    
    $('table.data-table.full').dataTable( {
            "dom": 'flrtip',
            "order": [ [0,'asc'],[1,'asc']],
            "paging": false,
            "info": false,
            "scrollY": "375px",
            "pageLength": 75,
            "scrollCollapse": true,
            //"lengthMenu": [[10, 25, 50, 75, 100 -1], [10, 25, 50,75,100, "All"]],
            "fnDrawCallback": function(o) {
               $('.dataTables_scrollBody').scrollTop(0);
            },
            "language": {
            "lengthMenu": "Display _MENU_ records per page",
            "zeroRecords": "Nothing found - sorry",
            "info": "Showing page _PAGE_ of _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtered from _MAX_ total records)"
        }   
        });
        
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    e.target // activated tab 
    e.relatedTarget // previous tab 
    var table = $.fn.dataTable.fnTables(true);
    if (table.length > 0) {
        $(table).dataTable().fnAdjustColumnSizing();
    }
   });
   </script>