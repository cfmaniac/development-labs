<cfoutput>
<cfscript>
   param name="rc.radius" default="150";
      if( VerticalListings.RecordCount == "0"){
          rc.ZipResults = createObject("component", "admin.controllers.zipfinder").haversineSearch(zip='#rc.CompanyAddress.AddressZip#',radius= '#rc.radius#');
      
      } else {
        rc.ZipResults = createObject("component", "admin.controllers.zipfinder").haversineSearch(zip='#rc.CompanyAddress.AddressZip#',radius= '#rc.radius#',verticalID='#rc.ListingVertical.siteVerticalID#');
        
          
      }
      
      /*WriteDump(var='#rc.zipResults#', label="Zip results for Vertical");*/
</cfscript>
     <input type="hidden" class="VertID#rc.ListingVertical.siteVerticalID#" value="#rc.ListingVertical.siteVerticalID#">
     <table id="listings#rc.ListingVertical.siteVerticalID#" class="table display full">
    <thead>
    <tr>
        <td><input type="checkbox" id="selectAll"></td>
        <td>Zip</td>
        <td>City</td>
        <td>County</td>
        <td>State</td>
        <td>Distance</td>
        
    </tr>
    </thead>
    <tbody>
	<cfloop query="#rc.ZipResults#">
	<cfscript>
     
          rc.ExclusiveZips = new Query(sql = 'select companyID, ListingZipCode, ListingExclusive, ListingActive, VerticalID from Companies_Listings
                                        INNER JOIN Zips on companies_listings.Listingzipcode = zips.zip
                                        WHERE zips.zip = #zip# and companies_listings.verticalID=#ListGetAt(rc.CompanyMarkets, rc.CompanyVerticals.currentRow,",")#').execute().getResult();
     
      if(rc.ExclusiveZips.recordcount is "0"){
         rc.ZipAvail = '1';
      } else {
         rc.ZipAvail = '0';
      }
	</cfscript>
	<tr <cfif #rc.ZipAvail#>class="alert-success"<cfelse>class="alert-danger"</cfif>>
	  <td><cfif #rc.ZipAvail#><input type="checkbox" class="ziplist" name="ListingZip" value="#zip#"></cfif> </td> 
      <td style="font-weight: bold;"><a href="javascript:WDSMaps.ViewOnMap(#currentrow#);">#zip#</a></td>
      <td>#city#</td>
      <td>#county#</td>
      <td>#state#</td>
      <td>#numberformat(dist, '0.0')#</td>
      
    </tr>
    </cfloop>
    </tbody>
	</table>
    
       
    
    <script>
    $( '##NewListing#rc.ListingVertical.siteVerticalID#' ).on( 'shown.bs.modal', function() {
     if ( $.fn.dataTable.isDataTable( '##listings#rc.ListingVertical.siteVerticalID#' ) ) {
        $('##listings#rc.ListingVertical.siteVerticalID#').dataTable().fnAdjustColumnSizing();
		
        }
    });  
    
	$('##listings#rc.ListingVertical.siteVerticalID#').dataTable( {
                "dom": 'flrtip',
                "order": [ [5,'asc']],
                "paging": true,
                "info": true,
                "scrollY": "375px",
                "pageLength": 75,
                "scrollCollapse": true,
                "lengthMenu": [[10, 25, 50, 75, 100 -1], [10, 25, 50,75,100, "All"]],
                "fnDrawCallback": function(o) {
                   $('.dataTables_scrollBody').scrollTop(0);
                },
                "language": {
                "lengthMenu": "Display _MENU_ records per page",
                "zeroRecords": "Nothing found - sorry",
                "info": "Showing page _PAGE_ of _PAGES_",
                "infoEmpty": "No records available",
                "infoFiltered": "(filtered from _MAX_ total records)",
                "search": "Find ZipCode, City or County"
            }   
            });	
		
    
    
	
	
	 
            
            
            $('##selectAll').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.ziplist').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.ziplist').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
	
    
   
    
    

   </script> 
     
     </cfoutput>