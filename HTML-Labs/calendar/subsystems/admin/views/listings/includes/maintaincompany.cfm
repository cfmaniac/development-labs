<cfimport taglib="/lib/tags/" prefix="ui">
<cfoutput>
	<div class="card">
        <ul class="nav nav-tabs">
          <li class="nav-item col-sm-2">
            <a class="nav-link active" href="##info" data-toggle="tab" aria-expanded="true">Information</a>
          </li>
          <li class="nav-item col-sm-2">
            <a class="nav-link" href="##addy" data-toggle="tab" aria-expanded="false">Addresses</a>
          </li>
          <li class="nav-item col-sm-2">
            <a class="nav-link" href="##listings" data-toggle="tab" aria-expanded="false">Exclusive Listings 
            <span class="pull-right btn btn-sm btn-success btn-rounded-deep">#numberformat(rc.CompanyListings.recordcount)#</span>
            </a>
          </li>
          <li class="nav-item col-sm-2">
            <a class="nav-link" href="##settings" data-toggle="tab" aria-expanded="false">Settings</a>
          </li>
          <li class="nav-item col-sm-2">
            <a class="nav-link" href="##verticals" data-toggle="tab" aria-expanded="false">Verticals 
            <span class="pull-right btn btn-sm btn-success btn-rounded-deep">#numberformat(rc.CompanyVerticals.recordcount)#</span></a>
          </li>
          
        </ul>
        <div class="card-block tab-content">
            <div class="tab-pane active" id="info" aria-expanded="true">
            <h2>Company Info</h2>
            <form name="companyInfo" method="post">
      <div class="form-group">
					<label class="control-label" for="metakeywords">Company Name </label>
					<div class="controls">
						<input type="text" name="name" id="name" value="#HtmlEditFormat( rc.CompanyInfo.name )#" class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">URL </label>
					<div class="controls">
						<input type="text" name="url" id="url" value="#HtmlEditFormat( rc.CompanyInfo.url )#" class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Company Email</label>
					<div class="controls">
						<input type="text" name="email" id="email" value="#HtmlEditFormat( rc.CompanyInfo.Email )#" class="form-control">
						
					</div>
		</div>
		
		
         
      <div class="form-group">
                    
					<input type="submit" name="submitCompanyInfo" class="btn btn-success pull-right" value="Update Company Information">
					
		</div>
      </form>
            </div> 
            <div class="tab-pane" id="addy" aria-expanded="false"> 
            <h2>Company Addresses</h2>
            <ul class="nav nav-tabs" role="tablist">
           <cfloop query="rc.CompanyAddress">
           <li role="presentation" <cfif rc.CompanyAddress.currentrow is "1">class="active"</cfif>><a href="##Loc#rc.CompanyAddress.currentRow#"role="tab" data-toggle="tab">Location #rc.CompanyAddress.currentRow#</a></li>
           </cfloop>
           
           <li role="presentation" class="pull-right" data-toggle="modal" data-target="##NewLocation"><a role="tab">Add a New Location</a></li>
          
         </ul>
          <div class="tab-content">
      
      <cfloop query="rc.CompanyAddress">
      <div role="tabpanel" class="tab-pane <cfif rc.CompanyAddress.currentrow is "1">active</cfif>" id="Loc#rc.CompanyAddress.currentRow#">
       <form name="companyaddress#rc.CompanyAddress.currentRow#" method="post">
        <input type="hidden" name="addyID" value="">
      <div class="form-group">
					<label class="control-label" for="metakeywords">Location Address</label>
					<div class="controls">
						<input type="text" name="address" id="address" value="#HtmlEditFormat( rc.CompanyAddress.AddressStreet)#" class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Location Address 2</label>
					<div class="controls">
						<input type="text" name="address2" id="address2" value="#HtmlEditFormat( rc.CompanyAddress.AddressStreet2)#" class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Location City</label>
					<div class="controls">
						<input type="text" name="city" id="city" value="#HtmlEditFormat( rc.CompanyAddress.AddressCity)#" class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Location State</label>
					<div class="controls">
						<!---input type="text" name="state" id="state" value="#HtmlEditFormat( rc.CompanyAddress.AddressState)#" class="form-control"--->
						
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Location ZipCode</label>
					<div class="controls">
						<input type="text" name="zipcode" id="zipcode" value="#HtmlEditFormat( rc.CompanyAddress.AddressZip)#" class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Location Phone</label>
					<div class="controls">
						<input type="text" name="phone" id="phone" value="#HtmlEditFormat( rc.CompanyAddress.CompanyPhone)#" class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Location Fax</label>
					<div class="controls">
						<input type="text" name="fax" id="fax" value="#HtmlEditFormat( rc.CompanyAddress.CompanyFax)#" class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Location Mobile</label>
					<div class="controls">
						<input type="text" name="mobile" id="mobile" value="#HtmlEditFormat( rc.CompanyAddress.CompanyMobile)#" class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Location Email</label>
					<div class="controls">
						<input type="text" name="email" id="email" value="#HtmlEditFormat( rc.CompanyAddress.CompanyEmail)#" class="form-control">
						
					</div>
		</div>
		<div class="form-group">
				<cfif rc.CompanyAddress.currentrow GT 1>
					<button id="removeAddy" class="btn btn-danger" data-addressID="#rc.CompanyAddress.AddressID#" datacompanyID="#rc.id#">Remove Address</button>
				</cfif>	
					<input type="submit" name="submitAddy" value="Update Address Information" class="btn btn-success pull-right">
		</div>
		</form>  
      </div>
      </cfloop>
      </div>
            </div>
            <div class="tab-pane" id="listings" aria-expanded="false"> 
            <h2>Company Listings</h2>
            <ul class="nav nav-tabs" role="tablist">
      <cfloop query="rc.CompanyVerticals">
      
	      <cfscript>
		      rc.CompanyVertListings = new Query(sql="Select Domain, ID from sites where ID=#rc.CompanyVerticals.verticalID#").execute().getResult();
	      </cfscript>
	      <li role="presentation" <cfif rc.CompanyVerticals.currentRow is "1">class="active"</cfif>><a href="###rc.CompanyVerticals.currentRow#" role="tab" data-toggle="tab">#rc.CompanyVertListings.Domain#</a></li>
      </cfloop>
      </ul>
      <div class="tab-content">
	  <cfloop query="rc.CompanyVerticals">
	  
	    <div role="tabpanel" class="tab-pane <cfif rc.CompanyVerticals.currentRow is "1">active</cfif>" id="#rc.CompanyVerticals.currentRow#">
         <cfquery name="VerticalListings" dbtype="query">
	        Select * from rc.CompanyListings where verticalID =#ListGetAt(rc.CompanyMarkets, rc.CompanyVerticals.currentRow,",")#
         </cfquery>
         <a href="##" class="btn btn-info pull-right" class="addListings" data-vertical="#rc.CompanyVerticals.verticalID#" data-toggle="modal" data-target="##NewListing#rc.CompanyVerticals.verticalID#">Add New Listing</a>
        
         
         
         
        <p>Use the Checkbox next to a Listing to remove it from this account</p>
        <table class="table table-first-column-number data-table display full" id="listings-#ListGetAt(rc.CompanyMarkets, rc.CompanyVerticals.currentRow,",")#">
          <thead>
          <tr>
          <th></th>
          <th>Vertical</th>
          <th>ZipCode</th>
          <th>County</th>
          <th>Listing Started</th>
          <th>Listing Expires</th>
          <th>Active</th>
          </tr>
          </thead>
          <tbody>
          <cfloop query="VerticalListings">
            <cfscript>
             rc.ListingVertical = new query(sql="select title, ID from Sites where ID=#ListGetAt(rc.CompanyMarkets, rc.CompanyVerticals.currentRow,",")#").execute().getResult();
             rc.ListingCounty = new query(sql="select county from zips where zip='#ListingZipCode#'").execute().getResult();
            </cfscript>
            <tr>
            <td><input onclick="" type="checkbox" class="DeleteListing" name="deleteListing" data-Listing="#ListingZipCode#" data-vertical="#ListGetAt(rc.CompanyMarkets, rc.CompanyVerticals.currentRow,",")#"></td>
            <td>#rc.ListingVertical.title#</td>        
            <td>#ListingZipCode#</td>
            <td>#rc.ListingCounty.County#</td>
            <td>#dateformat(listingStarted, "MM/DD/YYYY")#</td>  
            <td>#dateformat(listingEnds, "MM/DD/YYYY")#</td>  
            <td>
            
            <input type="checkbox" name="listingactive" value="1" class="RemoveListing change-color-switch" <cfif listingActive>checked="checked"</cfif>  data-on-color="success" data-off-color="danger"
            data-Listing="#ListingZipCode#" data-vertical="#ListGetAt(rc.CompanyMarkets, rc.CompanyVerticals.currentRow,",")#">
				</td>    
            </tr>
          </cfloop>
          </tbody>
          </table>
          
          
            <cfscript>
             rc.ListingVertical = new query(sql="select title, ID from Sites where ID=#ListGetAt(rc.CompanyMarkets, rc.CompanyVerticals.currentRow,",")#").execute().getResult();
            </cfscript>
          <!---Listing Modal per Vertical--->
           <cfloop query="#rc.ListingVertical#">
	       <!---New Listing Modal--->
		  <div class="modal fade" id="NewListing#rc.ListingVertical.ID#" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		        <h4 class="modal-title" id="myModalLabel">New Exclusive Listing for #rc.ListingVertical.title# </h4>
		      </div>
		      <div class="modal-body">
		        
		        #view('companies/includes/addlistings')#
		      </div>
		      <div class="modal-footer">
		        
		        <button type="button" id="ExclusiveSubmit#rc.ListingVertical.ID#"class="btn btn-primary ">Save New Listing</button>
		      </div>
		    </div>
		  </div>
		</div>
		  <!---End New Listing Modal--->
        </cfloop>
          
       </div>
       <script>
	        //Adds Exclusive Listings to Company:
		    $('##ExclusiveSubmit#rc.ListingVertical.ID#').on('click', function (e, data) {
		    var CompanyID = '#rc.id#';
		    var VerticalID = $('.VertID#rc.ListingVertical.ID#').val();
		    var data = new Array();
				$("input[name='ListingZip']:checked").each(function(i) {
					data.push($(this).val());
				});
		      //alert('Company ID: '+CompanyID+'Vertical ID: '+VerticalID+' Listings: '+data);
		      $.ajax({
		      type: "POST",
		      url: "/remote/RemoteProxy.cfc?method=SaveExclusives",
		      data: { companyid: CompanyID, verticalID: VerticalID, exclusives: data }
		      }).done(function( msg ) {
		      //alert( "Data Saved: " + msg );
		      location.reload();
		      });
  });
        </script>
      </cfloop>
      </div>
            </div>
            <div class="tab-pane" id="settings" aria-expanded="false"> 
            <h2>Company Settings</h2>
            <form name="companySettings" method="post">
            <div class="row">
      <div class="col-sm-6">
          <div class="form-group">
        <label class="control-label" for="metakeywords">Auto-Renew Account</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.autorenew>checked="checked"</cfif> name="autonew" value="1" class="change-color-switch" data-on-color="success" data-off-color="danger">
					</div>
		</div> 
		
		  <div class="form-group">
        <label class="control-label" for="metakeywords">Use Company Email as Primary Email</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.UseCompanyMailasPrimary>checked="checked"</cfif> name="UseCompanyMailasPrimary" value="1" class="change-color-switch CompanyEmail" data-on-color="success" data-off-color="danger">
					</div>
		</div> 
		
		<div class="form-group">
        <label class="control-label" for="metakeywords">Use Contact Email as Primary Email</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.UseContactMailasPrimary>checked="checked"</cfif> name="UseContactMailasPrimary" value="1" class="change-color-switch ContactEmail" data-on-color="success" data-off-color="danger">
					</div>
		</div>   
		
		<div class="form-group">
        <label class="control-label" for="metakeywords">Show URL</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.ShowURL>checked="checked"</cfif> name="ShowURL" value="1" class="change-color-switch" data-on-color="success" data-off-color="danger">
					</div>
		</div> 
		
		<div class="form-group">
        <label class="control-label" for="metakeywords">Show Click to Call</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.ShowClicktoCall>checked="checked"</cfif> name="ShowClicktoCall" value="1" class="change-color-switch" data-on-color="success" data-off-color="danger">
					</div>
		</div> 
		
		<div class="form-group">
        <label class="control-label" for="metakeywords">Show Live Call</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.ShowLiveCall>checked="checked"</cfif> name="ShowLiveCall" value="1" class="change-color-switch" data-on-color="success" data-off-color="danger">
					</div>
		</div> 
		
		<div class="form-group">
        <label class="control-label" for="metakeywords">Show Map</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.ShowMap>checked="checked"</cfif> name="ShowMap" value="1" class="change-color-switch" data-on-color="success" data-off-color="danger">
					</div>
		</div> 
		
		<div class="form-group">
        <label class="control-label" for="metakeywords">Show Directions</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.ShowDirections>checked="checked"</cfif> name="ShowDirections" value="1" class="change-color-switch" data-on-color="success" data-off-color="danger">
					</div>
		</div> 
		
		<div class="form-group">
        <label class="control-label" for="metakeywords">Show Text Message</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.ShowTextMessage>checked="checked"</cfif> name="ShowTextMessage" value="1" class="change-color-switch" data-on-color="success" data-off-color="danger">
					</div>
		</div> 
      </div>
      
      <div class="col-sm-6">
              <div class="form-group">
        <label class="control-label" for="metakeywords">Show Appointments</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.ShowAppointments>checked="checked"</cfif> name="ShowAppointments" value="1" class="change-color-switch" data-on-color="success" data-off-color="danger">
					</div>
		</div> 
		
		  <div class="form-group">
        <label class="control-label" for="metakeywords">Show Print</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.ShowPrint>checked="checked"</cfif> name="ShowPrint" value="1" class="change-color-switch" data-on-color="success" data-off-color="danger">
					</div>
		</div> 
		
		<div class="form-group">
        <label class="control-label" for="metakeywords">Show Contact</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.ShowContact>checked="checked"</cfif> name="ShowContact" value="1" class="change-color-switch" data-on-color="success" data-off-color="danger">
					</div>
		</div>   
		
		<div class="form-group">
        <label class="control-label" for="metakeywords">Show Nearby</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.ShowNEarby>checked="checked"</cfif> name="ShowNearby" value="1" class="change-color-switch" data-on-color="success" data-off-color="danger">
					</div>
		</div> 
		
		<div class="form-group">
        <label class="control-label" for="metakeywords">Show Gallery</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.ShowGallery>checked="checked"</cfif> name="ShowGallery" value="1" class="change-color-switch" data-on-color="success" data-off-color="danger">
					</div>
		</div> 
		
		<div class="form-group">
        <label class="control-label" for="metakeywords">Show Videos</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.ShowVideos>checked="checked"</cfif> name="Showvideos" value="1" class="change-color-switch" data-on-color="success" data-off-color="danger">
					</div>
		</div> 
		
		<div class="form-group">
        <label class="control-label" for="metakeywords">Show Reviews</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.Showreviews>checked="checked"</cfif> name="Showreviews" value="1" class="change-color-switch" data-on-color="success" data-off-color="danger">
					</div>
		</div> 
		
		<div class="form-group">
        <label class="control-label" for="metakeywords">Show Staff</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.ShowStaff>checked="checked"</cfif> name="ShowStaff" value="1" class="change-color-switch" data-on-color="success" data-off-color="danger">
					</div>
		</div> 
		
		<div class="form-group">
        <label class="control-label" for="metakeywords">SubScribe to Newsletter</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.subscribetoNewsletter>checked="checked"</cfif> name="subscribetoNewsletter" value="1" class="change-color-switch" data-on-color="success" data-off-color="danger">
					</div>
		</div> 
      </div>
	      <div class="form-group">
	       <input type="submit" name="submitSettings" class="btn btn-success pull-right" value="Save Settings">
	      </div>
	      </div>
      </form>
            </div>
            <div class="tab-pane" id="verticals" aria-expanded="false"> 
            <h2>Company Verticals</h2>
            <p>Add or Remove <strong>#rc.CompanyInfo.name#</strong> from the Following Markets.</p>      
              <table class="table table-first-column-number data-table display full">
			<thead>
				<tr>
					<th class="col-sm-1">&nbsp;</th>
					<th class="col-sm-10">Vertical Domain</th>
					<th class="col-sm-1">Status</th>
					
				</tr>
			</thead>
			
			<tbody>
	      
	       <cfloop array="#rc.SubSites#" index="site">
	              <tr>
			     <td class="col-sm-1"><div class="form-group">
					<div class="controls">
					<input type="checkbox" name="verticalID" data-verticalID="#site.getID()#" value="#site.getID()#" class="change-color-switch Updatevertical" <cfif ListFind(rc.CompanyMarkets, #site.getID()#, ",")>checked="checked"</cfif>  data-on-color="success" data-off-color="danger">
						
					</div>
		</div>  </td>
			     <td class="col-sm-10">#site.getTitle()#</td>
			     <td class="col-sm-1"></td>
		     </tr>
	              
	                
	              </cfloop>  
		   </tbody>
		   </table>
            </div>
            
            
        </div>    
        
        
        
         
    </div>    
</cfoutput>