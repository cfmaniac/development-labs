<cfoutput>
<ul class="nav nav-tabs" role="tablist">
     <li role="presentation" class="active"><a href="##info" role="tab" data-toggle="tab">Information</a></li>
    
     <li role="presentation"><a href="##addy" role="tab" data-toggle="tab">Addresses</a></li>
     <li role="presentation"><a href="##list" role="tab" data-toggle="tab">Exclusive Listings <span class="badge badge-success">#numberformat(rc.CompanyListings.recordcount)#</span></a></li>
     <li role="presentation"><a href="##sett" role="tab" data-toggle="tab">Settings</a></li>
     <li role="presentation"><a href="##verts" role="tab" data-toggle="tab">Verticals <span class="badge badge-success">#numberformat(rc.CompanyVerticals.recordcount)#</span></a></li>
     <!---li role="presentation"><a href="##user" role="tab" data-toggle="tab">Users <span class="badge badge-success">#numberformat(rc.CompanyUsers.recordcount)#</span></a></li--->
     
   </ul>
   
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="info">
      <h2>Company Information</h2>
      <form name="companyInfo" method="post">
      <div class="form-group">
					<label class="control-label" for="metakeywords">Company Name </label>
					<div class="controls">
						<input type="text" name="name" id="name" value="#HtmlEditFormat( rc.CompanyInfo.name )#" class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">URL </label>
					<div class="controls">
						<input type="text" name="url" id="url" value="#HtmlEditFormat( rc.CompanyInfo.url )#" class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Company Email</label>
					<div class="controls">
						<input type="text" name="email" id="email" value="#HtmlEditFormat( rc.CompanyInfo.Email )#" class="form-control">
						
					</div>
		</div>
		
		
         
      <div class="form-group">
                    
					<input type="submit" name="submitCompanyInfo" class="btn btn-success pull-right" value="Update Company Information">
					
		</div>
      </form>
      </div>
      
      <div role="tabpanel" class="tab-pane" id="addy">
      <h2>Company Addresses (#numberFormat(rc.CompanyAddress.RecordCount)#)</h2>
       <ul class="nav nav-tabs" role="tablist">
           <cfloop query="rc.CompanyAddress">
           <li role="presentation" <cfif rc.CompanyAddress.currentrow is "1">class="active"</cfif>><a href="##Loc#rc.CompanyAddress.currentRow#"role="tab" data-toggle="tab">Location #rc.CompanyAddress.currentRow#</a></li>
           </cfloop>
           
           <li role="presentation" class="pull-right" data-toggle="modal" data-target="##NewLocation"><a role="tab">Add a New Location</a></li>
          
         </ul>
          <div class="tab-content">
      
      <cfloop query="rc.CompanyAddress">
      <div role="tabpanel" class="tab-pane <cfif rc.CompanyAddress.currentrow is "1">active</cfif>" id="Loc#rc.CompanyAddress.currentRow#">
       <form name="companyaddress#rc.CompanyAddress.currentRow#" method="post">
        <input type="hidden" name="addyID" value="">
      <div class="form-group">
					<label class="control-label" for="metakeywords">Location Address</label>
					<div class="controls">
						<input type="text" name="address" id="address" value="#HtmlEditFormat( rc.CompanyAddress.AddressStreet)#" class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Location Address 2</label>
					<div class="controls">
						<input type="text" name="address2" id="address2" value="#HtmlEditFormat( rc.CompanyAddress.AddressStreet2)#" class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Location City</label>
					<div class="controls">
						<input type="text" name="city" id="city" value="#HtmlEditFormat( rc.CompanyAddress.AddressCity)#" class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Location State</label>
					<div class="controls">
						<!---input type="text" name="state" id="state" value="#HtmlEditFormat( rc.CompanyAddress.AddressState)#" class="form-control"--->
						
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Location ZipCode</label>
					<div class="controls">
						<input type="text" name="zipcode" id="zipcode" value="#HtmlEditFormat( rc.CompanyAddress.AddressZip)#" class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Location Phone</label>
					<div class="controls">
						<input type="text" name="phone" id="phone" value="#HtmlEditFormat( rc.CompanyAddress.CompanyPhone)#" class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Location Fax</label>
					<div class="controls">
						<input type="text" name="fax" id="fax" value="#HtmlEditFormat( rc.CompanyAddress.CompanyFax)#" class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Location Mobile</label>
					<div class="controls">
						<input type="text" name="mobile" id="mobile" value="#HtmlEditFormat( rc.CompanyAddress.CompanyMobile)#" class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Location Email</label>
					<div class="controls">
						<input type="text" name="email" id="email" value="#HtmlEditFormat( rc.CompanyAddress.CompanyEmail)#" class="form-control">
						
					</div>
		</div>
		<div class="form-group">
				<cfif rc.CompanyAddress.currentrow GT 1>
					<button id="removeAddy" class="btn btn-danger" data-addressID="#rc.CompanyAddress.AddressID#" datacompanyID="#rc.id#">Remove Address</button>
				</cfif>	
					<input type="submit" name="submitAddy" value="Update Address Information" class="btn btn-success pull-right">
		</div>
		</form>  
      </div>
      </cfloop>
      </div>
      
		
      </div>
      
      <div role="tabpanel" class="tab-pane" id="list">
      <h2>Exclusive Listings </h2>
      <ul class="nav nav-tabs" role="tablist">
      <cfloop query="rc.CompanyVerticals">
      
	      <cfscript>
		      rc.CompanyVertListings = new Query(sql="Select Domain, ID from sites where ID=#rc.CompanyVerticals.verticalID#").execute().getResult();
	      </cfscript>
	      <li role="presentation" <cfif rc.CompanyVerticals.currentRow is "1">class="active"</cfif>><a href="###rc.CompanyVerticals.currentRow#" role="tab" data-toggle="tab">#rc.CompanyVertListings.Domain#</a></li>
      </cfloop>
      </ul>
      <div class="tab-content">
	  <cfloop query="rc.CompanyVerticals">
	  
	    <div role="tabpanel" class="tab-pane <cfif rc.CompanyVerticals.currentRow is "1">active</cfif>" id="#rc.CompanyVerticals.currentRow#">
         <cfquery name="VerticalListings" dbtype="query">
	        Select * from rc.CompanyListings where verticalID =#ListGetAt(rc.CompanyMarkets, rc.CompanyVerticals.currentRow,",")#
         </cfquery>
         <a href="##" class="btn btn-info pull-right" class="addListings" data-vertical="#rc.CompanyVerticals.verticalID#" data-toggle="modal" data-target="##NewListing#rc.CompanyVerticals.verticalID#">Add New Listing</a>
        
         
         
         
        <p>Use the Checkbox next to a Listing to remove it from this account</p>
        <table class="table table-first-column-number data-table display full" id="listings-#ListGetAt(rc.CompanyMarkets, rc.CompanyVerticals.currentRow,",")#">
          <thead>
          <tr>
          <th></th>
          <th>Vertical</th>
          <th>ZipCode</th>
          <th>County</th>
          <th>Listing Started</th>
          <th>Listing Expires</th>
          <th>Active</th>
          </tr>
          </thead>
          <tbody>
          <cfloop query="VerticalListings">
            <cfscript>
             rc.ListingVertical = new query(sql="select title, ID from Sites where ID=#ListGetAt(rc.CompanyMarkets, rc.CompanyVerticals.currentRow,",")#").execute().getResult();
             rc.ListingCounty = new query(sql="select county from zips where zip='#ListingZipCode#'").execute().getResult();
            </cfscript>
            <tr>
            <td><input onclick="" type="checkbox" class="DeleteListing" name="deleteListing" data-Listing="#ListingZipCode#" data-vertical="#ListGetAt(rc.CompanyMarkets, rc.CompanyVerticals.currentRow,",")#"></td>
            <td>#rc.ListingVertical.title#</td>        
            <td>#ListingZipCode#</td>
            <td>#rc.ListingCounty.County#</td>
            <td>#dateformat(listingStarted, "MM/DD/YYYY")#</td>  
            <td>#dateformat(listingEnds, "MM/DD/YYYY")#</td>  
            <td>
            
            <input type="checkbox" name="listingactive" value="1" class="RemoveListing change-color-switch" <cfif listingActive>checked="checked"</cfif>  data-on-color="success" data-off-color="danger"
            data-Listing="#ListingZipCode#" data-vertical="#ListGetAt(rc.CompanyMarkets, rc.CompanyVerticals.currentRow,",")#">
				</td>    
            </tr>
          </cfloop>
          </tbody>
          </table>
          
          
            <cfscript>
             rc.ListingVertical = new query(sql="select title, ID from Sites where ID=#ListGetAt(rc.CompanyMarkets, rc.CompanyVerticals.currentRow,",")#").execute().getResult();
            </cfscript>
          <!---Listing Modal per Vertical--->
           <cfloop query="#rc.ListingVertical#">
	       <!---New Listing Modal--->
		  <div class="modal fade" id="NewListing#rc.ListingVertical.ID#" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		        <h4 class="modal-title" id="myModalLabel">New Exclusive Listing for #rc.ListingVertical.siteName# </h4>
		      </div>
		      <div class="modal-body">
		        
		        #view('companies/includes/addlistings')#
		      </div>
		      <div class="modal-footer">
		        
		        <button type="button" id="ExclusiveSubmit#rc.ListingVertical.ID#"class="btn btn-primary ">Save New Listing</button>
		      </div>
		    </div>
		  </div>
		</div>
		  <!---End New Listing Modal--->
        </cfloop>
          
       </div>
       <script>
	        //Adds Exclusive Listings to Company:
		    $('##ExclusiveSubmit#rc.ListingVertical.ID#').on('click', function (e, data) {
		    var CompanyID = '#rc.id#';
		    var VerticalID = $('.VertID#rc.ListingVertical.ID#').val();
		    var data = new Array();
				$("input[name='ListingZip']:checked").each(function(i) {
					data.push($(this).val());
				});
		      //alert('Company ID: '+CompanyID+'Vertical ID: '+VerticalID+' Listings: '+data);
		      $.ajax({
		      type: "POST",
		      url: "/remote/RemoteProxy.cfc?method=SaveExclusives",
		      data: { companyid: CompanyID, verticalID: VerticalID, exclusives: data }
		      }).done(function( msg ) {
		      //alert( "Data Saved: " + msg );
		      location.reload();
		      });
  });
        </script>
      </cfloop>
      </div>
      
      
      
       
      
      
      
      </div>
      
      <div role="tabpanel" class="tab-pane" id="sett">
      <h2>Company Settings</h2>
      
      <form name="companySettings" method="post">
      <div class="col-sm-6">
          <div class="form-group">
        <label class="control-label" for="metakeywords">Auto-Renew Account</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.autorenew>checked="checked"</cfif> name="autonew" value="1" class="change-color-switch" data-on-color="success" data-off-color="danger">
					</div>
		</div> 
		
		  <div class="form-group">
        <label class="control-label" for="metakeywords">Use Company Email as Primary Email</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.UseCompanyMailasPrimary>checked="checked"</cfif> name="UseCompanyMailasPrimary" value="1" class="change-color-switch CompanyEmail" data-on-color="success" data-off-color="danger">
					</div>
		</div> 
		
		<div class="form-group">
        <label class="control-label" for="metakeywords">Use Contact Email as Primary Email</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.UseContactMailasPrimary>checked="checked"</cfif> name="UseContactMailasPrimary" value="1" class="change-color-switch ContactEmail" data-on-color="success" data-off-color="danger">
					</div>
		</div>   
		
		<div class="form-group">
        <label class="control-label" for="metakeywords">Show URL</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.ShowURL>checked="checked"</cfif> name="ShowURL" value="1" class="change-color-switch" data-on-color="success" data-off-color="danger">
					</div>
		</div> 
		
		<div class="form-group">
        <label class="control-label" for="metakeywords">Show Click to Call</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.ShowClicktoCall>checked="checked"</cfif> name="ShowClicktoCall" value="1" class="change-color-switch" data-on-color="success" data-off-color="danger">
					</div>
		</div> 
		
		<div class="form-group">
        <label class="control-label" for="metakeywords">Show Live Call</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.ShowLiveCall>checked="checked"</cfif> name="ShowLiveCall" value="1" class="change-color-switch" data-on-color="success" data-off-color="danger">
					</div>
		</div> 
		
		<div class="form-group">
        <label class="control-label" for="metakeywords">Show Map</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.ShowMap>checked="checked"</cfif> name="ShowMap" value="1" class="change-color-switch" data-on-color="success" data-off-color="danger">
					</div>
		</div> 
		
		<div class="form-group">
        <label class="control-label" for="metakeywords">Show Directions</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.ShowDirections>checked="checked"</cfif> name="ShowDirections" value="1" class="change-color-switch" data-on-color="success" data-off-color="danger">
					</div>
		</div> 
		
		<div class="form-group">
        <label class="control-label" for="metakeywords">Show Text Message</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.ShowTextMessage>checked="checked"</cfif> name="ShowTextMessage" value="1" class="change-color-switch" data-on-color="success" data-off-color="danger">
					</div>
		</div> 
      </div>
      
      <div class="col-sm-6">
              <div class="form-group">
        <label class="control-label" for="metakeywords">Show Appointments</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.ShowAppointments>checked="checked"</cfif> name="ShowAppointments" value="1" class="change-color-switch" data-on-color="success" data-off-color="danger">
					</div>
		</div> 
		
		  <div class="form-group">
        <label class="control-label" for="metakeywords">Show Print</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.ShowPrint>checked="checked"</cfif> name="ShowPrint" value="1" class="change-color-switch" data-on-color="success" data-off-color="danger">
					</div>
		</div> 
		
		<div class="form-group">
        <label class="control-label" for="metakeywords">Show Contact</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.ShowContact>checked="checked"</cfif> name="ShowContact" value="1" class="change-color-switch" data-on-color="success" data-off-color="danger">
					</div>
		</div>   
		
		<div class="form-group">
        <label class="control-label" for="metakeywords">Show Nearby</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.ShowNEarby>checked="checked"</cfif> name="ShowNearby" value="1" class="change-color-switch" data-on-color="success" data-off-color="danger">
					</div>
		</div> 
		
		<div class="form-group">
        <label class="control-label" for="metakeywords">Show Gallery</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.ShowGallery>checked="checked"</cfif> name="ShowGallery" value="1" class="change-color-switch" data-on-color="success" data-off-color="danger">
					</div>
		</div> 
		
		<div class="form-group">
        <label class="control-label" for="metakeywords">Show Videos</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.ShowVideos>checked="checked"</cfif> name="Showvideos" value="1" class="change-color-switch" data-on-color="success" data-off-color="danger">
					</div>
		</div> 
		
		<div class="form-group">
        <label class="control-label" for="metakeywords">Show Reviews</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.Showreviews>checked="checked"</cfif> name="Showreviews" value="1" class="change-color-switch" data-on-color="success" data-off-color="danger">
					</div>
		</div> 
		
		<div class="form-group">
        <label class="control-label" for="metakeywords">Show Staff</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.ShowStaff>checked="checked"</cfif> name="ShowStaff" value="1" class="change-color-switch" data-on-color="success" data-off-color="danger">
					</div>
		</div> 
		
		<div class="form-group">
        <label class="control-label" for="metakeywords">SubScribe to Newsletter</label>
					<div class="controls" >
					<input type="checkbox" <cfif rc.CompanySettings.subscribetoNewsletter>checked="checked"</cfif> name="subscribetoNewsletter" value="1" class="change-color-switch" data-on-color="success" data-off-color="danger">
					</div>
		</div> 
      </div>
	      <div class="form-group">
	       <input type="submit" name="submitSettings" class="btn btn-success pull-right" value="Save Settings">
	      </div>
      </form>
      </div>
      
      <div role="tabpanel" class="tab-pane" id="verts">
       <h2>Company Verticals</h2>
       <p>Add or Remove <strong>#rc.CompanyInfo.name#</strong> from the Following Markets.</p>      
              <table class="table table-first-column-number data-table display full">
			<thead>
				<tr>
					<th class="col-sm-1">&nbsp;</th>
					<th class="col-sm-10">Vertical Domain</th>
					<th class="col-sm-1">Status</th>
					
				</tr>
			</thead>
			
			<tbody>
	      
	       <cfloop array="#rc.Sites#" index="site">
	              <tr>
			     <td class="col-sm-1"><div class="form-group">
					<div class="controls">
					<input type="checkbox" name="verticalID" data-verticalID="#site.getID()#" value="#site.getID()#" class="change-color-switch Updatevertical" <cfif ListFind(rc.CompanyMarkets, #site.getID()#, ",")>checked="checked"</cfif>  data-on-color="success" data-off-color="danger">
						
					</div>
		</div>  </td>
			     <td class="col-sm-10">#site.getTitle()#</td>
			     <td class="col-sm-1"></td>
		     </tr>
	              
	                
	              </cfloop>  
		   </tbody>
		   </table>
       
      </div>
      
       <div role="tabpanel" class="tab-pane" id="user">
      <h2>Company Users</h2>
      <!---cfloop query="#rc.CompanyUsers#">
      <div class="form-group">
        <label class="control-label" for="metakeywords">#rc.CompanyUsers.user_name#</label>
					<div class="controls" rel="#rc.CompanyUsers.user_id#">
					<input type="checkbox" <cfif rc.CompanyUsers.user_active>checked="checked"</cfif> name="user_active" value="1" class="change-color-switch Updateuser" data-user="#rc.CompanyUsers.user_id#" data-on-color="success" data-off-color="danger">
					</div>
		</div>  
      </cfloop--->
      </div>
         
    </div>  

   
   
   <hr>
   <cfquery name="CurrentBilling" dbtype="query">
      select * from rc.CompanyBilling where BillingStatus='Due'
   </cfquery>
   <cfquery name="PastBilling" dbtype="query">
      select * from rc.CompanyBilling where BillingStatus='Paid' Order by BillingDate DESC
   </cfquery>
   <h2>Company Billing <button id="addBilling" class="btn btn-info pull-right" datacompanyID="#rc.id#" data-toggle="modal" data-target="##NewBilling">Add Billing</button></h2>
      <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="##current" role="tab" data-toggle="tab">Current Billing</a></li>
          <li role="presentation"><a href="##history" role="tab" data-toggle="tab">Billing History</a></li>
      </ul>
      
      <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="current">
      <h2>Current Billing</h2>
      
      <table class="table table-first-column-number data-table display full">
			<thead>
				<tr>
					<th class="col-sm-2">Term</th>
					<th class="col-sm-2">Price</th>
					<th class="col-sm-1">Status</th>
					<th class="center col-sm-1">Billing Date</th>
					<th class="center col-sm-1">Date Due By</th>
					<th class="center col-sm-3">Notes</th>
					<th class="center col-sm-2"></th>
				</tr>
			</thead>
			
			<tbody>
      <cfloop query="CurrentBilling">
         <tr>
         <td class="col-sm-2">#BillingTerm# Months </td>
         <td class="col-sm-2">#dollarFormat(billingPrice)# </td>
         <td class="col-sm-1">#billingStatus#</td>
         <td class="col-sm-1">#dateformat(BillingDate, "MM/DD/YYYY")#</td>
         <td class="class="col-sm-2"><strong>#dateformat(BillingDateDue, "MM/DD/YYYY")#</strong></td>
         <td class="col-sm-3">#BillingNotes#</td>
         <td class="col-sm-2"></td>
         </tr>  
      </cfloop>
      </tbody>
      </table>
      </div>
      <div role="tabpanel" class="tab-pane" id="history">
      <h2>Billing History </h2>
       <table class="table table-first-column-number data-table display full">
			<thead>
				<tr>
					<th class="col-sm-2">Term</th>
					<th class="col-sm-2">Price</th>
					<th class="col-sm-1">Status</th>
					<th class="center col-sm-1">Billing Date</th>
					<th class="center col-sm-1">Paid Date</th>
					<th class="center col-sm-3">Notes</th>
					<th class="center col-sm-1"></th>
				</tr>
			</thead>
			
			<tbody>
      <cfloop query="PastBilling">
         <tr>
         <td class="col-sm-2">#BillingTerm# Months </td>
         <td class="col-sm-2">#dollarFormat(billingPrice)# </td>
         <td class="col-sm-1">#billingStatus#</td>
         <td class="col-sm-1">#dateformat(BillingDate, "MM/DD/YYYY")#</td>
         <td class="col-sm-1">#dateformat(BillingPaidDate, "MM/DD/YYYY")#</td>
         <td class="col-sm-3">#BillingNotes#</td>
         <td class="center col-sm-1"></td>
         </tr> 
      </cfloop>
      </tbody>
      </table>
      </div>
 </div>
     
 </div>
 
 
  <div class="col-sm-4 main-content">
  <div class="form-group">
					<label class="control-label" for="metakeywords">Company Active</label>
					<div class="controls">
					<input type="checkbox" name="active" id="CompanyActive" data-companyid="#rc.id#" value="1" class="change-color-switch" <cfif parameterExists(rc.id)><cfif rc.CompanyInfo.active>checked="checked"</cfif></cfif>  data-on-color="success" data-off-color="danger">
						
					</div>
		</div>  
  
  <div class="form-group">
					<label class="control-label" for="metakeywords">Company Signed up</label>
					<div class="controls">
					<cfif parameterExists(rc.id)>
					#dateformat(rc.companyInfo.StartDate, "MM/DD/YYYY")#
					<cfelse>
					
					</cfif>
						
					</div>
		</div>   
   
  </div>
  
  <!---New Location Modal--->
  <div class="modal fade" id="NewLocation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">New Company Location</h4>
      </div>
      <div class="modal-body">
        <form name="addLocation" method="post">
        <div class="form-group">
					<label class="control-label" for="metakeywords">Location Address</label>
					<div class="controls">
						<input type="text" name="Address" id="Address" value="" class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Location Address 2</label>
					<div class="controls">
						<input type="text" name="Address2" id="Address2" value="" class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Location City</label>
					<div class="controls">
						<input type="text" name="city" id="city" value="" class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Location State</label>
					<div class="controls">
						<input type="text" name="state" id="state" value="" class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Location ZipCode</label>
					<div class="controls">
						<input type="text" name="zipcode" id="zipcode" value="" class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Location Phone</label>
					<div class="controls">
						<input type="text" name="phone" id="phone" value="" class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Location Fax</label>
					<div class="controls">
						<input type="text" name="fax" id="fax" value="" class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Location Mobile</label>
					<div class="controls">
						<input type="text" name="mobile" id="mobile" value="" class="form-control">
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Location Email</label>
					<div class="controls">
						<input type="text" name="email" id="email" value="" class="form-control">
						
					</div>
		</div>
		  
      </div>
      <div class="modal-footer">
        
        <input type="submit" name="addLocation" id="NewLoc" class="btn btn-primary" value="Save New Location">
      </div>
      </form>
    </div>
  </div>
</div>
  <!---End New Location Modal--->
  
  
  
    <!---New User Modal--->
  <div class="modal fade" id="NewUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">New Company User</h4>
      </div>
      <div class="modal-body">
        
		
		
		
      </div>
      <div class="modal-footer">
        
        <button type="button" class="btn btn-primary">Save New User</button>
      </div>
    </div>
  </div>
</div>
  <!---End New User Modal--->
  
  <!---Vertical Manager--->
  <div class="modal fade" id="VertMgr" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Vertical Management</h4>
      </div>
      <div class="modal-body">
        <p>Please Be aware if you remove <em>#rc.CompanyInfo.name#</em> from this Vertical, thier Exclusive Listings will also be removed.</p>
		<p>Performing this action opens thier Exclusive Listings ZipCodes for availablility.</p>
		<p>Do you want to remove <em>#rc.CompanyInfo.name#</em> from this Vertical?</p>
		
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal" id="restoreMarket">Cancel</button>
        <button type="button" class="btn btn-primary" id="removeMarket">Remove Vertical from #rc.CompanyInfo.Name#</button>
      </div>
    </div>
  </div>
</div>
  <!---End Vertical Manager Modal--->
  
  <!---New Billing Modal--->
  <div class="modal fade" id="NewBilling" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">New Billing Item</h4>
      </div>
      <form name="CompanyBilling" method="post">
      <div class="modal-body">
      
       <div class="form-group">
					<label class="control-label" for="metakeywords">Billing Term</label>
					<div class="controls">
						<select name="BillingTerm" class="form-control">
							<option value="3">3 Month (90-day)</option>
							<option value="6">6 Month (180-day)</option>
							<option value="12">1 Year</option>
							<option value="18">1.5 Year</option>
							<option value="24">2 Year</option>
							<option value="36">3 Year</option>
							<option vlaue="0">Custom Term</option>
						</select>
						
					</div>
					
					<div class="form-group">
					<label class="control-label" for="metakeywords">Billing Price</label>
					<div class="controls">
						<input type="text" name="billingPrice" class="form-control">
						
					</div>
					</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Billing Status</label>
					<div class="controls">
						<select name="BillingStatus" class="form-control">
							<option value="Paid">Paid</option>
							<option value="Unpaid">Unpaid</option>
							<option value="Past Due">Past due</option>
							<option value="Suspended">Suspended</option>
							
						</select>
						
					</div>
		</div>
		
		<div class="form-group">
					<label class="control-label" for="metakeywords">Billing Notes</label>
					<div class="controls">
						<textarea name="billingNotes" class="form-control">
							
						</textarea>
						<small>These are internally Seen</small>
					</div>
		</div>
      </div>
      <div class="modal-footer">
        <input type="submit" name="newBilling" class="btn btn-primary" value="Save New Billing">
        <!---button type="button" class="btn btn-primary">Save New Billing</button--->
      </div>
      </form>
    </div>
  </div>
</div>
  <!---End New Billing Modal--->
  
 
  
  <script>
    $('.change-color-switch').bootstrapSwitch('onColor', 'success');
    $('.change-color-switch').bootstrapSwitch('offColor', 'danger');
    
    //Toggle User Update
    $('.Updateuser').on('switchChange', function (e, data) {
    var UserID = $(this).attr('data-user');
    var Active = data.value;
      //alert('User ID '+UserID+' Updated to: '+Active);
      $.ajax({
      type: "POST",
      url: "/remote/RemoteProxy.cfc?method=SaveUserStatus",
      data: { user_id: UserID, user_active: Active }
      }).done(function( msg ) {
      //alert( "Data Saved: " + msg );
      });
  });
  
  
  
  $('.Updatevertical').on('switchChange', function (e, data) {
	  var Active = data.value;
	  var RestoreThis = $(this).bootstrapSwitch();
	  var VerticalID = $(this).attr('data-verticalid'); 
	  var CompanyID = '#rc.id#';
	  
	  if(Active == false){
		  $('##VertMgr').modal('show');
		   $('##restoreMarket').on('click', function(){
            $(RestoreThis).bootstrapSwitch('toggleState', false);
           });
           
           $('##removeMarket').on('click', function(){
		     //Removes Listings and Company from Vertical
		     $.ajax({
		      type: "POST",
		      url: "/remote/RemoteProxy.cfc?method=RemoveFromVertical",
		      data: { companyID: CompanyID, VerticalID: VerticalID }
		      }).done(function( msg ) {
		      //alert( "Data Saved: " + msg );
		      });
		     	     
		     $('##VertMgr').modal('hide');
		     //location.reload();
		     
           });
	  } else if (Active == true){
	        //Adds Vertical to Company:
	        $.ajax({
		      type: "POST",
		      url: "/remote/RemoteProxy.cfc?method=AddToVertical",
		      data: { companyID: CompanyID, VerticalID: VerticalID }
		      }).done(function( msg ) {
		      //alert( "Data Saved: " + msg );
		      });
	  }
  });
  
   $('.RemoveListing').on('switchChange', function (e, data) {
    var VerticalID = $(this).attr('data-vertical');
    var ListingZip = $(this).attr('data-listing');
    var CompanyID = '#rc.id#';
    var Active = data.value;
   
      //alert('CompanyID '+CompanyID+' ZipCode: '+ListingZip+' VerticalID:'+VerticalID+' Status:'+Active);
      $.ajax({
      type: "POST",
      url: "/remote/RemoteProxy.cfc?method=ExclusiveStatus",
      data: { companyID: CompanyID, ListingZip: ListingZip, VerticalID: VerticalID, active: Active }
      }).done(function( msg ) {
      //alert( "Data Saved: " + msg );
      });
  });
  
   $('##CompanyActive').on('switchChange', function (e, data) {
    var CompanyID = '#rc.id#';
    var Active = data.value;
   
      //alert('CompanyID '+CompanyID+' ZipCode: '+ListingZip+' VerticalID:'+VerticalID+' Status:'+Active);
      $.ajax({
      type: "POST",
      url: "/remote/RemoteProxy.cfc?method=ToggleCompanyActive",
      data: { companyID: CompanyID, active: Active }
      }).done(function( msg ) {
      //alert( "Data Saved: " + msg );
      });
  });
  
  $('.DeleteListing').on('click', function (e, data) {
    var VerticalID = $(this).attr('data-vertical');
    var ListingZip = $(this).attr('data-listing');
    var CompanyID = '#rc.id#';
    
      
      //alert('CompanyID '+CompanyID+' ZipCode: '+ListingZip+' VerticalID:'+VerticalID+' Status:'+Active);
      $.ajax({
      type: "POST",
      url: "/remote/RemoteProxy.cfc?method=RemoveExclusives",
      data: { companyID: CompanyID, ListingZip: ListingZip, VerticalID: VerticalID}
      }).done(function( msg ) {
      //alert( "Data Saved: " + msg );
     
      });
      $(this).parents('tr').remove();
  });
  
  $('##removeAddy').on('click', function (e, data) {
    var AddressID = $(this).attr('data-addressID');
         
      //alert('CompanyID '+CompanyID+' ZipCode: '+ListingZip+' VerticalID:'+VerticalID+' Status:'+Active);
      $.ajax({
      type: "POST",
      url: "/remote/RemoteProxy.cfc?method=removeAddy",
      data: { AddressID: AddressID}
      }).done(function( msg ) {
      //alert( "Data Saved: " + msg );
      location.reload();
      });
     
  });
  
  
  //Toggle Company Email from Settings
  /*$('.CompanyEmail').on('switchChange', function (e, data) {
      $('.ContactEmail').bootstrapSwitch('toggleState', false);
   
  });
  
  $('.ContactEmail').on('switchChange', function (e, data) {
      $('.CompanyEmail').bootstrapSwitch('toggleState', false);
   
  });*/
  
    
  
 
    
    $('table.data-table.full').dataTable( {
            "dom": 'flrtip',
            "order": [ [0,'asc'],[1,'asc']],
            "paging": false,
            "info": false,
            "scrollY": "375px",
            "pageLength": 75,
            "scrollCollapse": true,
            //"lengthMenu": [[10, 25, 50, 75, 100 -1], [10, 25, 50,75,100, "All"]],
            "fnDrawCallback": function(o) {
               $('.dataTables_scrollBody').scrollTop(0);
            },
            "language": {
            "lengthMenu": "Display _MENU_ records per page",
            "zeroRecords": "Nothing found - sorry",
            "info": "Showing page _PAGE_ of _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtered from _MAX_ total records)"
        }   
        });
        
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    e.target // activated tab 
    e.relatedTarget // previous tab 
    var table = $.fn.dataTable.fnTables(true);
    if (table.length > 0) {
        $(table).dataTable().fnAdjustColumnSizing();
    }
   });
   
   
    
	
	

</script>
</cfoutput>