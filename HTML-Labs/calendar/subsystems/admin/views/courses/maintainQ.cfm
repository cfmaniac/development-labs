<!---<cfdump var="#rc.QuestionInfo#">
<cfdump var="#rc.Answers#">--->
<cfimport taglib="/lib/tags/" prefix="ui">
<cfoutput>
  #view("partials/messages")#
  
 <ui:container title="Maintain Question" main="false" style="margin-top:0;" size="12">
    <form name="questionsForm" method="Post" action="#buildURL('courses.saveQ')#">
	<input type="hidden" name="question_id" value="#rc.qid#">

    <h3>Question:</h3>
    <ui:form-row type="htmlmin" id="question" label="Question" name="question" value="#rc.QuestionInfo.question#">
 
    
    <div class="row">
     <h3>Answers   <a class="addsection btn btn-info pull-right">Add Answer</a></h3>
     <hr>
    </div>  
    
   <div id="sections">
   <cfif #rc.Answers.recordCount# GT "0">
    <cfloop query="#rc.Answers#">
    <div class="section row <cfif rc.Answers.answer_correct>alert alert-success</cfif>">
        <input type="hidden" name="answer_id" value="#rc.Answers.answer_id#">
        <ui:form-row type="text" id="answer_text" name="answer_text" Label="Answer" value="#rc.Answers.answer_text#">
        <!---ui:form-row type="boolean" id="answer_correct" label="Correct Answer" name="answer_correct[]" value="0"--->
        <ui:input label="Correct Answer" type="select" optionlabels="Yes, No" options="1,0" name="answer_correct" value="#rc.Answers.answer_correct#">
		<div class="row">
        <a href="##" class='remove btn btn-danger btn-rounded-deep pull-right'><i class="fa fa-trash-o"></i></a>
        </div>
        <hr>
	</div>
    </cfloop>
    <cfelse>
    <div class="section row">
        <ui:form-row type="text" id="answer_text" name="answer_text" Label="Answer" value="">
        <!---ui:form-row type="boolean" id="answer_correct" label="Correct Answer" name="answer_correct[]" value="0"--->
        <ui:input label="Correct Answer" type="select" optionlabels="Yes, No" options="1,0" name="answer_correct" value="0">
		<div class="row">
        <a href="##" class='remove btn btn-danger btn-rounded-deep pull-right'><i class="fa fa-trash-o"></i></a>
        </div>
        <hr>
	</div>
    </cfif>
    </div>
    
    <div class="row">
	    <input type="submit" value="Save Question" class="btn btn-success pull-right"> 
    </div>
    </form>
    
 </ui:container>
 
 <!---
 <div id="answerRow" class="row" style="visibility: hidden;">
 <div class="pull-right">
	 <a title="Delete" class="removeAns btn btn-danger btn-rounded-deep"><i class="fa fa-trash-o"></i></a>
 </div>
        <ui:form-row type="text" id="answer_text" Label="Answer_text" value="">
        <ui:form-row type="boolean" id="answer_correct" label="Correct Answer" name="answer_correct" value="0">
 </div>
 --->
 
 
<script>
//define template
var template = $('##sections .section:first').clone();

//define counter
var sectionsCount = 1;

//add new section
$('body').on('click', '.addsection', function() {

    //increment
    sectionsCount++;

    //loop through each input
    var section = template.clone().find(':input').each(function(){

        //set id to store the updated section number
        var newId = this.id + sectionsCount;

        //update for label
        $(this).prev().attr('for', newId);

        //update id
        this.id = newId;

    }).end()

    //inject new section
    .appendTo('##sections');
    return false;
});

//remove section
$('##sections').on('click', '.remove', function() {
    //fade out section
    $(this).parent().fadeOut(300, function(){
        //remove parent element (main section)
        //$(this).parent().parent().empty();
        $(this).parent('.section').remove();
        return false;
    });
    return false;
});
</script>
 </cfoutput>