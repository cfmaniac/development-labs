<cfimport taglib="/lib/tags/" prefix="ui">
<cfoutput>
	
  <div class="content-wrapper">
    <h1 class="page-heading">#rc.currentsite.sitename# Courses</h1>
    
    
    #view("partials/messages")#
    
    <div class="row">
   <div class="col-sm-12">
   <div class="card">
      <div class="card-block">
        <div class="media">
          <div class="media-body media-middle">
            <h4 class="m-b-0">Online Courses</h4>
            <p class="text-muted m-b-0"></p>
          </div>
        </div>    
      </div>
      
      <div class="card-block p-t-0">
      <ui:table tableID="coursesTable" datatable="true" showheader="true" pagelength="50" columnsHidden="0" sortcol="1"
            sortOrder="ASC" columns="Name|Questions List" showDelete="true" >
        <cfloop array="#rc.Courses#" index="course">
            <cfquery name="QuestionsCount">
                Select count(*) as QuestionTotal from courses_questions where course_id='#course.getcourseID()#'
            </cfquery>
            <ui:table-row id="#course.getcourseID()#" 
            editColumn="1"
            editLink="#buildURL(action='blog.maintain', querystring='postId=#course.getcourseID()#')#"
            deleteLink="#buildURL(action='blog.delete', querystring='postId=#course.getcourseID()#')#" 
            showdelete="true"
            values="#course.GetCourse_Title()# |<a href='#buildURL(action='courses.questions', queryString='course=#course.getcourseID()#')#'><i class='fa fa-list fa-lg'></i></a><span class='pull-right btn btn-success btn-rounded-deep'>#QuestionsCount.QuestionTotal#</span>">
            </ui:table-row> 
        </cfloop>
      </ui:table>
            
       </div>
    </div>
    </div>
    
    </div>
  </div>
</cfoutput>   