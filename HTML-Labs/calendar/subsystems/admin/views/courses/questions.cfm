<cfimport taglib="/lib/tags/" prefix="ui">
<cfoutput>
	
  <div class="content-wrapper">
    <h1 class="page-heading">#rc.currentsite.sitename# Course Questions </h1>
    
    
    #view("partials/messages")#
    
    <div class="row">
   <div class="col-sm-12">
   <div class="card">
      <div class="card-block">
        <div class="media">
          <div class="media-body media-middle">
            <h4 class="m-b-0">Courses Questions for Course: 
            <cfloop array="#rc.CourseCore#" index="course">
				#course.GetCourse_Title()#
            </cfloop></h4>
            <p class="text-muted m-b-0"></p>
          </div>
        </div>    
      </div>
      
      <div class="card-block p-t-0">
      <!---ui:table tableID="questionsTable" datatable="true" showheader="true" pagelength="50" columnsHidden="0" sortcol="0"
            sortOrder="ASC" columns="Question|Answers" showDelete="true" colnosort="2">
            

        <cfloop array="#rc.questions#" index="question">
            <cfquery name="Answers">
                Select answer_text, answer_correct from courses_answers where question_id='#question.getquestion_id()#'
            </cfquery>
            <ui:table-row id="#question.getquestion_id()#" 
            editColumn="1"
            editLink="#buildURL(action='courses.questionsmaintain', querystring='id=#question.getquestion_id()#')#"
            deleteLink="#buildURL(action='courses.deletequestion', querystring='id=#question.getquestion_id()#')#" 
            showdelete="true"
            values="#question.Getquestion()#|#Answers.Answer_text#">
            </ui:table-row> 
        </cfloop>
      </ui:table--->
      <!---table id="questionsTable" class="table table-bordered table-striped table-hover table-sm">
            <thead>
            <tr>
                <td class="invisible">&nbsp;</td>
                <td class="col-sm-6">Question</td>
                <td class="col-sm-5">Answers</td>
                
            </tr>
            </thead>
            <tbody>
               <cfloop array="#rc.questions#" index="question">
                <cfquery name="Answers">
                    Select answer_text, answer_correct from courses_answers where question_id='#question.getquestion_id()#'
                </cfquery> 
                <tr>
                    <td class="invisible">#question.getquestion_id()#</td>
                    <td class="col-sm-6"><a href="#buildURL(action='courses.maintainQ', querystring='qid=#question.getquestion_id()#')#">#question.Getquestion()#</a></td>
                    <td class="col-sm-5">
                    <!---div style="height: 30px; overflow: auto" class="col-sm-6">
                    <cfloop query="Answers">
                        #answers.answer_Text#<br>
                    </cfloop>
                    </div--->
                    <a href="#buildURL(action='courses.deletequestion', querystring='id=#question.getquestion_id()#')#" title="Delete" class="btn btn-danger btn-rounded-deep"><i class="fa fa-trash-o"></i></a>
                    </td>
                   
                </tr>
                </cfloop>
            </tbody>
      </table--->
      <table id="questionsTable" class="table table-bordered table-striped table-hover">
          <thead>
            <tr>
                <th>&nbsp;</th>
                <th class="col-sm-6">Questions</th>
                <th class="col-sm-6">Answers</th>
                
            </tr>
          </thead>  
          <tbody>
            <cfloop array="#rc.questions#" index="question">
                <cfquery name="Answers">
                    Select answer_text, answer_correct from courses_answers where question_id='#question.getquestion_id()#'
                </cfquery> 
                <tr id="#question.getquestion_id()#">
                <th>#question.getquestion_id()#</th>
                <td class="col-sm-6">
                <a href="#buildURL(action='courses.maintainQ', querystring='qid=#question.getquestion_id()#')#">#question.Getquestion()#</a>
                </td>
                <td class="col-sm-6"><div style="height: 30px; overflow: auto" class="col-sm-6">
                    <cfloop query="Answers">
                    <div class="alert- alert-<cfif answers.answer_correct>success<cfelse>danger</cfif>">
                        #answers.answer_Text#
                    </div>    
                    </cfloop>
                    </div>
                    <a href="" title="Delete" class="btn btn-danger btn-rounded-deep pull-right"><i class="fa fa-trash-o"></i></a></td>
                
            </tr>
            </cfloop>
            
        </tbody>
      </table>
      
      
      <script src="layouts/assets/vendor/jquery.dataTables.min.js"></script>
    <script src="layouts/assets/vendor/datatables-bootstrap3.min.js"></script>
	<script>
	var DataTable = $.fn.dataTable;
	  $.extend( true, DataTable.defaults, {
	    dom:
	      "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>>" +
	      "<'table-responsive'tr>" +
	      "<'row'<'col-sm-5'i><'col-sm-7'p>>",
	    renderer: 'bootstrap'
	    
	  } );
	  
	  $('##questionsTable').DataTable({
	      "stateSave": false,
          "pageLength": 50,
          "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
          "language": {
            "zeroRecords": "Nothing found - sorry",
            "infoEmpty": "No records available",
            "lengthMenu": "Showing _MENU_",
          },
          "order": [0, 'DESC'],
         "columnDefs": [
            
                { "orderable": false, "targets": [2] },   
                      
             
             { "targets": [0],"visible": false }
           ],
	  });
	  
	  
	  $('##questionsTable').find('thead th, tbody td').css({
      "width": "45%",
      "padding-right": "0px",
      "margin-right": "0px"
    });
		  
	  
	
  </script>    
      
            <!---cfdump var="#rc.questions#"--->
       </div>
    </div>
    </div>
    
    </div>
  </div>
</cfoutput>   