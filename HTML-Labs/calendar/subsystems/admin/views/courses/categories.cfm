<cfimport taglib="/lib/tags/" prefix="ui">
<cfoutput>
	
  <div class="content-wrapper">
    <h1 class="page-heading">#rc.currentsite.sitename# Course Categories</h1>
    
    
    #view("partials/messages")#
    
    <div class="row">
   <div class="col-sm-12">
   <div class="card">
      <div class="card-block">
        <div class="media">
          <div class="media-body media-middle">
            <h4 class="m-b-0">Online Courses Categories</h4>
            <p class="text-muted m-b-0"></p>
          </div>
        </div>    
      </div>
      
      <div class="card-block p-t-0">
      <ui:table tableID="categoriesTable" datatable="true" showheader="true" pagelength="50" columnsHidden="0" sortcol="1"
            sortOrder="ASC" columns="Name" showDelete="true" colnosort="2">
        <cfloop array="#rc.categories#" index="cats">
            <ui:table-row id="#cats.getcategory_ID()#" 
            editColumn="1"
            editLink="#buildURL(action='blog.maintain', querystring='postId=#cats.getcategory_ID#')#"
            deleteLink="#buildURL(action='blog.delete', querystring='postId=#cats.getcategory_ID()#')#" 
            showdelete="true"
            values="#cats.GetCategory_Name()#">
            </ui:table-row> 
        </cfloop>
      </ui:table>
            
       </div>
    </div>
    </div>
    
    </div>
  </div>
</cfoutput>   