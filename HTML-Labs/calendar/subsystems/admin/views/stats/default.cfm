<cfimport taglib="/lib/tags/" prefix="ui">

<cfoutput>
  #view("partials/messages")#

<div class="card">
        <ul class="nav nav-tabs">
          <li class="nav-item">
            <a class="nav-link active" href="##stats" data-toggle="tab" aria-expanded="true">Site Statistics</a>
          </li>
          <cfif #len(rc.config.gaProfileID)#>
          <li class="nav-item">
            <a class="nav-link" href="##gstats" data-toggle="tab" aria-expanded="false">Google Analytics</a>
          </li>
          </cfif>
                   
        </ul>
        <div class="card-block tab-content">  
        
        <div class="tab-pane active" id="stats" aria-expanded="true"> 
         <div class="row">   
<ui:container title="Site Analytics & Stats" main="false" style="margin-top:0;" size="12" infotext="These are your Site's Internally Tracked Analytics">
  <div class="row">
        <div class="col-sm-4">
             
	         <table class="table table-md m-a-0">
              <tbody>
              <tr><td>
	              Total Views <strong>This Month</strong>: <span class="pull-right btn btn-success btn-rounded-deep"> #numberFormat(ArraySum(rc.PageViews))#</span>
              </td></tr>   
              
              <tr><td>
	              Total Visitors <strong>This Month</strong>: <span class="pull-right btn btn-success btn-rounded-deep">#numberFormat(ArraySum(rc.Visitors))#</span>
              </td></tr>                        
              </tbody>
            </table>
        </div>
        <div id="usage" class="col-sm-8" style="height: 275px; margin: 0 auto"></div>
        
        </div> 
</ui:container>


<ui:container title="Top Pages" main="false" style="margin-top:0; height: 325px;" size="12" infotext="">
    <div id="toppages" class="col-sm-12" style="height: 275px; margin: 0 auto"></div>
    <table style="height:325px;"></table>
</ui:container>


<ui:container title="Web Browsers" main="false" style="margin-top:0; height: 325px;" size="6" infotext="">
   <div id="browsers" class="col-sm-12" style="height: 275px; margin: 0 auto"></div>
   <table style="height:325px;"></table>
</ui:container>


<ui:container title="Web Crawlers" main="false" style="margin-top:0; height: 325px;" size="6" infotext="">
    <div id="crawlers" class="col-sm-12" style="height: 275px; margin: 0 auto"></div>
    <table style="height:325px;"></table>
</ui:container>
</div> 
        </div>
        <cfif #len(rc.config.GATRACKINGID)#>
        <div class="tab-pane" id="gstats" aria-expanded="true">
        <div class="row"> 
        <ui:container title="Google&trade; Analytics & Stats" main="false" style="margin-top:0;" size="12" infotext="These are your Site's Analytics as reported from Google Analytics">
           <!---iframe src="/lib/plugins/GoogleAnalytics/index.cfm" frameborder="0" style="width=550px;height=550px;"></iframe--->
        </ui:container>      
        <!---cfdump var="#rtnQuery#"--->
        <!---<cfdump var="#rc.rtnStruct#">--->
        </div>
        
        </div>
        </cfif>
        
        </div>
</div>        
 



</cfoutput>