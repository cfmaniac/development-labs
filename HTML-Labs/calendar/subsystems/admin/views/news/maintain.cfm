<cfimport taglib="/lib/tags/" prefix="ui">
<cfoutput>
<h1 class="page-heading">#rc.currentsite.sitename# News : #rc.PageTitle#</h1>

#view("partials/messages")#

<div class="card">

<div class="card-block">

<div class="stepwizard">
    <div class="stepwizard-row setup-panel">
        <div class="stepwizard-step">
            <a href="##step-1" type="button" class="btn btn-primary btn-circle"><i class="fa fa-file fa-3x"></i></a>
            <p>Article Content</p>
        </div>
        <div class="stepwizard-step">
            <a href="##step-2" type="button" class="btn btn-default btn-circle"><i class="fa fa-bars fa-3x"></i></a>
            <p>Publishing & Navigation</p>
        </div>
        <div class="stepwizard-step">
            <a href="##step-3" type="button" class="btn btn-default btn-circle"><i class="fa fa-bullseye fa-3x"></i></a>
            <p>SEO & Meta Data</p>
        </div>
    </div>
</div>
<form role="form" action="#buildURL('news.save')#" method="post">
	<input type="hidden" name="context" id="context" value="#HtmlEditFormat(rc.context)#">
	<input type="hidden" name="Articleid" id="Articleid" value="#HtmlEditFormat(rc.Article.getArticleId())#">
    <div class="row setup-content" id="step-1">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3>Article Contents</h3>
                <ui:form-row id="Title" label="Article Title" placeholder="Article Title" name="title" value="#HtmlEditFormat(rc.Article.getTitle())#">
               
	            <ui:form-row type="htmlfull" id="content" label="Article Content" name="content" value="#rc.Article.getContent()#">
	         
                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-2">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3>Publishing & Navigation</h3>
                <cfif rc.context is "update">
                <ui:form-row id="slug" label="URL Context (Slug)" placeholder="URL Context" name="slug" value="#rc.Article.getSlug()#">
	            </cfif>
	            
	            <div class="form-group ">
					<label for="page-content">Author </label>
					<select name="IDuser" class="form-control">
						<cfloop array="#rc.AdminUsers#" index="author">
						<option value="#author.getUserID()#" <cfif #rc.CurrentUser.getUserID()# EQ #author.getUserID()#>selected</cfif>>#author.getName()#</option>
						</cfloop>
					</select>
				
				</div>              
                
                <div class="form-group <cfif rc.result.hasErrors('published')>error</cfif>">
					<label for="published">Date <cfif rc.Validator.propertyIsRequired("published")>*</cfif></label>
					<input class="form-control datepicker" type="text" name="published" id="published" value="<cfif IsDate(rc.Article.getPublished())>#HtmlEditFormat(DateFormat(rc.Article.getPublished(), 'mm/dd/yyyy'))#</cfif>" title="The date the article is to be published" placeholder="Date">
					#view("partials/failures", {property = "published"})#
					<noscript><p class="help-block">Enter in 'mm/dd/yyyy' format.</p></noscript>
				</div>
                
                <button class="btn btn-default prevBtn btn-lg pull-left" type="button" >Prev</button>
                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-3">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3>SEO & Meta Data</h3>
                <!---div class="form-group <cfif rc.result.hasErrors('metagenerated')>error</cfif>">
				<label>&nbsp;</label>
				<label class="checkbox">
					<input type="checkbox" name="metagenerated" id="metagenerated" value="true" <cfif rc.Article.getMetaGenerated()>checked="checked"</cfif>>
					Generate automatically <cfif rc.Validator.propertyIsRequired("metagenerated")>*</cfif>
					#view("partials/failures", {property = "metagenerated"})#
				</label>
			</div--->
				<div class="form-group">
	    
				<label class="c-input c-checkbox">
					<input type="checkbox" name="metagenerated" id="metagenerated" value="true" <cfif rc.Article.getMetaGenerated()>checked="checked"</cfif>>
		                <span class="c-indicator"></span>
		                <strong>Generate Meta Tags Automatically</strong>
			    </label>
			    #view("partials/failures", {property = "metagenerated"})#
				</div>

			<div class="metatags">
				<div class="form-group <cfif rc.result.hasErrors('metaTitle')>error</cfif>">
					<label for="metatitle">Title <cfif rc.Validator.propertyIsRequired("metaTitle", rc.context)>*</cfif></label>
					<input class="form-control" type="text" name="metatitle" id="metatitle" value="#HtmlEditFormat(rc.Article.getMetaTitle())#" maxlength="100" placeholder="Meta title">
					#view("partials/failures", {property = "metaTitle"})#
				</div>

				<div class="form-group <cfif rc.result.hasErrors('metaDescription')>error</cfif>">
					<label for="metadescription">Description <cfif rc.Validator.propertyIsRequired("metaDescription", rc.context)>*</cfif></label>
					<input class="form-control" type="text" name="metadescription" id="metadescription" value="#HtmlEditFormat(rc.Article.getMetaDescription())#" maxlength="200" placeholder="Meta description">
					#view("partials/failures", {property = "metaDescription"})#
				</div>

				<div class="form-group <cfif rc.result.hasErrors('metaKeywords')>error</cfif>">
					<label for="metakeywords">Keywords <cfif rc.Validator.propertyIsRequired("metaKeywords", rc.context)>*</cfif></label>
					<input class="form-control" type="text" name="metakeywords" id="metakeywords" value="#HtmlEditFormat(rc.Article.getMetaKeywords())#" maxlength="200" placeholder="Meta keywords">
					#view("partials/failures", {property = "metaKeywords"})#
				</div>
          </div>
                
                
                <button class="btn btn-default prevBtn btn-lg pull-left" type="button" >Prev</button>
                <button class="btn btn-success btn-lg pull-right" type="submit">Save #rc.Article.GetTitle()#</button>
            </div>
        </div>
    </div>
</form>
</div>

</div>

</cfoutput>