<cfimport taglib="/lib/tags/" prefix="ui">
<cfoutput>
<div class="content-wrapper">
    <h1 class="page-heading">#rc.currentsite.sitename# News : News Articles</h1>
    
    
    #view("partials/messages")#
    
    <div class="row">
   <div class="col-sm-12">
   <div class="card">
      <div class="card-block">
        <div class="media">
          <div class="media-body media-middle">
            <h4 class="m-b-0">News Articles</h4>
            <p class="text-muted m-b-0"></p>
          </div>
        </div>    
      </div>
      
      <div class="card-block p-t-0">
        <ui:table tableID="newTable" datatable="true" showheader="true" pagelength="50" columnsHidden="0" sortcol="0"
            sortOrder="DESC" columns="Name|Published|Updated" showDelete="true">
            <cfloop array="#rc.articles#" index="local.Article">
            <ui:table-row id="#local.Article.getArticleId()#" 
            editColumn="1"
            editLink="#buildURL(action = 'news.maintain', queryString = 'articleid/#local.Article.getArticleId()#')#"
            
            deleteLink="#buildURL('news.delete')#/articleid/#local.Article.getArticleId()#" 
            showdelete="true"
            values="#local.Article.getTitle()#|#DateFormat(local.Article.getPublished(), "full")#|#DateFormat(local.Article.getUpdated(), "full")# #TimeFormat(local.Article.getUpdated())# by #local.Article.getUpdatedBy()#">
            </ui:table-row> 
            </cfloop>
        </ui:table>
      </div>
    </div>
    </div>
    
    </div>
  </div>
<!---
	<div class="page-header clear">
		<h1>News</h1>
	</div>

	<p><a href="#buildURL('news.maintain')#" class="btn btn-primary">Add article <i class="glyphicon glyphicon-chevron-right glyphicon-white"></i></a></p>

	#view("partials/messages")#

	<cfif ArrayLen(rc.articles)>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Title</th>
					<th>Published</th>
					<th>Last Updated</th>
					<th class="center">View</th>
					<th class="center">Edit</th>
					<th class="center">Delete</th>
				</tr>
			</thead>

			<tbody>
				<cfloop array="#rc.articles#" index="local.Article">
					<tr>
						<td>#local.Article.getTitle()#</td>
						<td>#DateFormat(local.Article.getPublished(), "full")#</td>
						<td>#DateFormat(local.Article.getUpdated(), "full")# #TimeFormat(local.Article.getUpdated())# by #local.Article.getUpdatedBy()#</td>
						<td class="center">
							<cfif local.Article.isPublished()>
								<a href="#buildURL(action = 'public:news.article', queryString = 'slug=#local.Article.getSlug()#')#" title="View" target="_blank"><i class="glyphicon glyphicon-eye-open"></i></a>
							</cfif>
						</td>
						<td class="center"><a href="#buildURL(action = 'news.maintain', queryString = 'articleid/#local.Article.getArticleId()#')#" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a></td>
						<td class="center"><a href="#buildURL('news.delete')#/articleid/#local.Article.getArticleId()#" title="Delete"><i class="glyphicon glyphicon-trash"></i></a></td>
					</tr>
				</cfloop>
			</tbody>
		</table>
	<cfelse>
		<p>There are no articles at this time.</p>
	</cfif>
--->
</cfoutput>
