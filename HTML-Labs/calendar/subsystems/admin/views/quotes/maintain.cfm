<cfimport taglib="/lib/tags/" prefix="ui">
<cfoutput>
	
  <div class="content-wrapper">
    <ol class="breadcrumb m-b-0">
      <li><a href="#BuildURL('')#">Home</a></li>
      <li>Quotes</li>
      <li class="active">#rc.pageTitle#</li>
    </ol>
    
    #view("partials/messages")#
    
    <form action="#buildURL('quotes.save')#" method="post" class="form-horizontal" id="page-form">
        <input type="hidden" name="id" id="pageid" value="#HtmlEditFormat(rc.Quote.getQuoteId())#">
		<input type="hidden" name="context" id="context" value="#HtmlEditFormat(rc.context)#">
   <div class="row">
   <div class="col-sm-12">
   <div class="card">
      <div class="card-block">
        <div class="media">
          <div class="media-body media-middle">
            <h4 class="m-b-0">#rc.pageTitle#</h4>
            <p class="text-muted m-b-0"></p>
          </div>
        </div>    
      </div>
      
      <div class="card-block p-t-0">
        	

			<div class="form-group">
				<label for="page-content">Quote</label>
				<textarea class="form-control ckeditor" name="body" id="page-content">#HtmlEditFormat(rc.Quote.getQuote())#</textarea>
				
			</div>
			
			
			
			
        
      </div>
    </div>
    </div>
    
   
    </div>  
    
    
        
   
   
  </div>
  <div class="row pull-right">
	   <input type="submit" name="submit" value="Save &amp; exit" class="btn btn-primary">
  </div>
  </form>
</cfoutput>