<cfoutput>
	
  <div class="content-wrapper">
    <ol class="breadcrumb m-b-0">
      <li><a href="#BuildURL('')#">Home</a></li>
      <li class="active">Quotes</li>
    </ol>
    
    #view("partials/messages")#
    
    <div class="row">
   <div class="col-sm-12">
   <div class="card">
      <div class="card-block">
        <div class="media">
          <div class="media-body media-middle">
            <h4 class="m-b-0">Quotes</h4>
            <p class="text-muted m-b-0"></p>
          </div>
        </div>    
      </div>
      
      <div class="card-block p-t-0">
        <table id="datatable-example" class="table table-bordered table-striped table-hover table-sm">
        <thead>
          <tr>
            <th></th>
            <th>Name</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
        <cfif ArrayLen(rc.quotes)>
	       <cfloop array="#rc.quotes#" index="local.quote">
			<tr>
			    <td>#local.quote.GetQuoteID()#</td>
				<td><a href="#buildURL(action='quotes.maintain', querystring='Id=#local.quote.GetQuoteID()#')#">#local.quote.GetQuote()#</a></td>
				<td>&nbsp;</td>
                <td>&nbsp;</td>
				<td><a href="#buildURL(action='quotes.delete', querystring='Id=#local.quote.GetQuoteID()#')#" title="Delete" class="btn btn-danger btn-rounded-deep"><i class="fa fa-trash-o"></i></a></td>
			</cfloop>
        <cfelse>
        <tr>
	        <td>No Quotes at the Moment</td>
        </tr>
        </cfif>
        </tbody>
      </table>
      </div>
    </div>
    </div>
    
    </div>
  </div>
</cfoutput>    