<cfimport taglib="/lib/tags/" prefix="ui">
<cfoutput>
<ui:container title="Gallery" main="true" style="margin-top:0;" size="12" infotext="#arrayLen(rc.photos)# Photos in Gallery">
	#view("partials/messages")#
	<ui:table tableID="cfdatatable" pagelength="10" columnsHidden="0" columns="Title" showDelete="true">
      <cfloop array="#rc.photos#" index="local.photo">
        <ui:table-row id="#local.photo.GetPhotoID()#" editColumn="1" 
        editLink="#buildURL(action='gallery.maintain', querystring='photoId=#local.photo.GetPhotoID()#')#" sortcol="0" 
        values="#local.photo.GetName()#" 
        showdelete="true"
        deleteLink="#buildURL(action='gallery.delete', querystring='photoId=#local.photo.GetPhotoID()#')#"></ui:table-row>
      </cfloop>    
      </ui:table>  
	
	<!---table id="datatable-example" class="table table-bordered table-striped table-hover table-sm">
        <thead>
          <tr>
            <th class="hidden"></th>
            <th>Title</th>
            <th></th>
            <th></th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
        <cfloop array="#rc.photos#" index="local.photo">
		<tr>
			<td>#local.photo.Getdisplay()#</td>
			<td><a href="#buildURL(action='gallery.maintain', querystring='photoId=#local.photo.GetPhotoID()#')#">#local.photo.GetName()#</a></td>
			
			<td></td>
			<td></td>
			<td><a href="#buildURL(action='gallery.delete', querystring='photoId=#local.photo.GetPhotoID()#')#" class="btn btn-danger btn-rounded-deep" title="Delete"><i class="fa fa-trash-o"></i></a></td>
		</cfloop>
        </tbody>
    </table--->    
</ui:container>
<!---
	<div class="row page-header clear">
		<div class="col-sm-4"><h1>Gallery Manager</h1></div>
		<div class="col-sm-8"> <a href="#buildURL('gallery.maintain')#" class="btn btn-success pull-right">Add New Image</a></div>
	</div>

	#view("partials/messages")#
    <div class="row full-calendar">
    <table class="table table-striped">
		<thead>
			<tr>
				<th>Title</th>
				<th></th>
				<th></th>
				<th class="center">Edit</th>
				<th class="center">Delete</th>
				</tr>
		</thead>

		<tbody>
		<!---cfdump var="#rc.posts#"--->
		<cfloop array="#rc.photos#" index="local.photo">
		<tr>
			<td>#local.photo.GetName()#</td>
			<td></td>
			<td></td>
			<td><a href="#buildURL(action='gallery.maintain', querystring='photoId=#local.photo.GetPhotoID()#')#"><i class="fa fa-pencil"></i></a></td>
			<td><a href="#buildURL(action='gallery.delete', querystring='photoId=#local.photo.GetPhotoID()#')#" title="Delete"><i class="fa fa-trash-o"></i></a></td>
		</cfloop>
    
    </table>
    </div>
--->    
   
	</cfoutput>


