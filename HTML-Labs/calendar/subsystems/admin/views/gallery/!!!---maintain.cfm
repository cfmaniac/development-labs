<cfscript>
    /*if(structKeyExists('rc', submit)){
	   cgi.rc.METHOD;
    }*/
	if (isdefined("form.submit")) {
		writeDump(var="#form#", abort="true");
	}
	
</cfscript>

<cfoutput>
#cgi.rc.METHOD#
<form action="" method="POST" class="form-horizontal" id="page-form" enctype="multipart/form-data">
	<div class="content-wrapper">
    <ol class="breadcrumb m-b-0">
      <li><a href="#BuildURL('')#">Home</a></li>
      <li>Pages</li>
      <li class="active">#rc.pageTitle#</li>
    </ol>
    
    #view("partials/messages")#
    
    
   <div class="row">
   <div class="col-sm-12">
   <div class="card">
      <div class="card-block">
        <div class="media">
          <div class="media-body media-middle">
            <h4 class="m-b-0">#rc.pageTitle#</h4>
            <p class="text-muted m-b-0"></p>
          </div>
        </div>    
      </div>
      
      <div class="card-block p-t-0">
        <fieldset>
			<legend>Photo Information</legend>

			<div class="form-group ">
				<label for="title">Title </label>
				<input class="form-control" type="text" name="name" id="name" value="#HtmlEditFormat(rc.Photo.getName())#" maxlength="100" placeholder="Title">
				
			</div>
			
			<div class="row">
			<div class="form-group col-sm-9">
				<label for="page-content">Caption </label>
				<textarea class="form-control ckeditor" name="caption" id="page-content">#HtmlEditFormat(rc.Photo.getCaption())#</textarea>
				
			</div>
			<div class="form-group col-sm-3">
				<label for="page-content">File </label>
				<input type="file" class="form-control" name="photoFile">
				<cfif rc.context is "update" and fileExists('#expandpath('./globals/#rc.sitename#/gallery/#rc.Photo.getFile()#')#')>
				<img src="http://#cgi.http_host#/globals/#rc.sitename#/gallery/#rc.Photo.getFile()#" width="275" height="275">
				</cfif>
			</div>
			
			</div>
			
			<div class="form-group ">
				<label for="page-content">Display Photo</label>
				<select name="display" class="form-control">
					<option value="0" <cfif !rc.photo.getActive()>selected="selected"</cfif>>No</option>
					<option value="1" <cfif rc.photo.getActive()>selected="selected"</cfif>>Yes</option>
				</select>
				
			</div>
			
			<div class="form-group ">
				<label for="page-content">Display Order</label>
				<small>Smaller Numbers get displayed first</small>
				<select name="order" class="form-control">
					<option value="0" <cfif rc.photo.getDisplay() EQ "0">selected="selected"</cfif>>0</option>
					<cfloop from="1" to="100" step="1" index="order">
						<option value="#order#" <cfif rc.photo.getDisplay() EQ #order#>selected="selected"</cfif>>#order#</option>
					</cfloop>
				</select>
				
			</div>
		</fieldset>
      </div>
      
      <div class="row pull-right">
	    <input type="hidden" name="photoid" id="pageid" value="#HtmlEditFormat(rc.Photo.getPhotoId())#">
	    <input type="hidden" name="context" id="context" value="#HtmlEditFormat(rc.context)#">
	    <input type="hidden" name="siteid" id="context" value="#HtmlEditFormat(rc.currentsite)#">
        
        
		<input type="submit" name="submit" value="Save Photo" class="btn btn-primary">
    </div>
    
      
   </div>
   </form>   
</cfoutput>
<!---
<cfoutput>
	<div class="page-header clear">
		<h1>#rc.pageTitle#</h1>
	</div>

	<div class="btn-group pull-right append-bottom" data-toggle="buttons-checkbox">
		<a href="#buildURL('gallery')#" class="btn"> Back to Gallery</a>
		<cfif rc.context is "update">
			<a href="#buildURL('gallery.delete')#/photoid/#rc.Photo.getPhotoId()#" title="Delete" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete</a>
		</cfif>
	</div>

	<div class="clear"></div>

	#view("partials/messages")#

	<form action="" method="post" class="form-horizontal" id="page-form" enctype="multipart/form-data">
		<fieldset>
			<legend>Photo Information</legend>

			<div class="form-group ">
				<label for="title">Title </label>
				<input class="form-control" type="text" name="name" id="name" value="#HtmlEditFormat(rc.Photo.getName())#" maxlength="100" placeholder="Title">
				
			</div>
			
			<div class="row">
			<div class="form-group col-sm-9">
				<label for="page-content">Caption </label>
				<textarea class="form-control ckeditor" name="caption" id="page-content">#HtmlEditFormat(rc.Photo.getCaption())#</textarea>
				
			</div>
			<div class="form-group col-sm-3">
				<label for="page-content">File </label>
				<input type="file" class="form-control" name="photoFile">
				<cfif rc.context is "update" and fileExists('#expandpath('./globals/#rc.sitename#/gallery/#rc.Photo.getFile()#')#')>
				<img src="http://#cgi.http_host#/globals/#rc.sitename#/gallery/#rc.Photo.getFile()#" width="275" height="275">
				</cfif>
			</div>
			
			</div>
			
			<div class="form-group ">
				<label for="page-content">Display Photo</label>
				<select name="display" class="form-control">
					<option value="0" <cfif !rc.photo.getActive()>selected="selected"</cfif>>No</option>
					<option value="1" <cfif rc.photo.getActive()>selected="selected"</cfif>>Yes</option>
				</select>
				
			</div>
			
			<div class="form-group ">
				<label for="page-content">Display Order</label>
				<small>Smaller Numbers get displayed first</small>
				<select name="order" class="form-control">
					<option value="0" <cfif rc.photo.getDisplay() EQ "0">selected="selected"</cfif>>0</option>
					<cfloop from="1" to="100" step="1" index="order">
						<option value="#order#" <cfif rc.photo.getDisplay() EQ #order#>selected="selected"</cfif>>#order#</option>
					</cfloop>
				</select>
				
			</div>
		</fieldset>

		

		
		<input type="submit" name="submit" value="Save" class="btn btn-primary">
		<a href="#buildURL('gallery')#" class="btn cancel">Cancel</a>

		<input type="hidden" name="photoid" id="pageid" value="#HtmlEditFormat(rc.Photo.getPhotoId())#">
	    <input type="hidden" name="context" id="context" value="#HtmlEditFormat(rc.context)#">
	    <input type="hidden" name="siteid" id="context" value="#HtmlEditFormat(rc.currentsite)#">
	</form>

	<p>* this field is required</p>

	
</cfoutput>--->
