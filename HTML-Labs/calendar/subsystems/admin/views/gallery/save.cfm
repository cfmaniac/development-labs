<cfif structKeyExists(form, "photoFile") and form.photoFile NEQ "">
<cfset filePath = #expandPath('./globals/#rc.siteName#/gallery/')#>
<cfdump var="#filePath##photoFile#" abort="true">

<cffile action="upload" fileField="photoFile" destination="#expandPath('./globals/#rc.siteName#/gallery/')#" nameConflict="unique">
</cfif>


<cfscript>
    if(form.context is "update") {
	    rc.Photo = entityLoadByPK( 'gallery', form.photoid );
	    rc.Photo.setName( '#form.name#' );
	    rc.Photo.setCaption( '#form.caption#' );
	    rc.Photo.setActive( '#form.display#' );
	    
	    if(structKeyExists(form,"photoFile") && form.photoFile !=""){
			rc.Photo.setFile( '#CFFile.ClientFile#' );
		}
	    rc.Photo.setDisplay( '#form.order#' );

	    transaction {
		    entitySave( #rc.Photo# );
		    ORMFLUSH( );

	    }
    } else {
	    //Save New Image
	    rc.Photo = entityNew('gallery');
	    rc.Photo.setName( '#form.name#' );
	    rc.Photo.setCaption( '#form.caption#' );
	    rc.Photo.setActive( '#form.display#' );
	    rc.Photo.setFile( '#CFFile.ClientFile#' );
	    rc.Photo.setDisplay( '#form.order#' );
        rc.Photo.setSiteID('');
	    transaction {
		    entitySave( #rc.Photo# );
		    ORMFLUSH( );

	    }
    }
	
	location(url='admin~gallery', addtoken='false');
</cfscript>

