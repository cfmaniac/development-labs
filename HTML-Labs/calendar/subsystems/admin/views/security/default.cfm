<cfset rc.layout = "false">
<cfoutput>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>#rc.config.SiteName# - Login</title>
  <base href="#rc.basehref#admin/">
  
  <meta name="robots" content="noindex">
  
  <!-- Material Design Icons  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  <!-- Roboto Web Font -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en" rel="stylesheet">

  <!-- App CSS -->
  <link type="text/css" href="layouts/assets/css/style.min.css" rel="stylesheet">
  <link rel="icon" type="image/png" href="layouts/assets/images/replicator-icon.png" />
</head>

<body class="login">

  <div class="row">
    <div class="col-sm-10 col-sm-push-1 col-md-6 col-md-push-3 col-lg-6 col-lg-push-3">
      <h2 class="text-primary center m-a-2">
        <span class="icon-text">#rc.config.SiteName# - Login</span>
      </h2>
      <div class="card-group">
        <div class="card bg-transparent">
          <div class="card-block">
            <div class="center">
              <h4 class="m-b-0"><span class="icon-text">Login</span></h4>
              <p class="text-muted">Administrate your Website</p>
              #view("partials/messages")#
            </div>
            <form action="#buildURL('security/login')#" method="post" id="login-form">
              <div class="form-group">
                <input type="text" name="email" id="email" class="form-control" placeholder="Email Address or UserName">
              </div>
              <div class="form-group">
                <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                <a href="##" class="pull-xs-right">
                  <small>Forgot?</small>
                </a>
                <div class="clearfix"></div>
              </div>
              <div class="center">
                <button type="submit" class="btn btn-primary-outline btn-circle-large">
                  <i class="material-icons">lock</i>
                </button>
              </div>
            </form>
          </div>
        </div>
        
      </div>
     
        <p class="center">#rc.config.SiteName# powered by Replicator&trade; v.#rc.config.version# <br>
        Developed & &copy; 2008-#dateformat(now(), "YYYY")# by ManiacDevelopment, LLC</p>
     
       
      
      
      
    </div>
  </div>
<script src="layouts/assets/vendor/jquery.min.js"></script>
   <script src="layouts/assets/vendor/tether.min.js"></script>
  <script src="layouts/assets/vendor/bootstrap.min.js"></script>

  <!-- AdminPlus -->
  <script src="layouts/assets/vendor/replicator.js"></script>

  <!-- App JS -->
  <script src="layouts/assets/js/main.min.js"></script>
  
</body>
    <!---
    #rc.Validator.getInitializationScript()#

	#rc.Validator.getValidationScript(formName = "login-form", context = "login")#
    --->

</html>
</cfoutput>