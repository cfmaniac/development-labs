<cfimport taglib="/lib/tags/" prefix="ui">
<cfoutput>
	
  <div class="content-wrapper">
    <h1 class="page-heading">#rc.currentsite.sitename# Email Lists : Campaigns</h1>
    
    
    #view("partials/messages")#
    
    <div class="row">
   <div class="col-sm-12">
   <div class="card">
      <div class="card-block">
        <div class="media">
          <div class="media-body media-middle">
            <h4 class="m-b-0">Current Campaigns</h4>
            <p class="text-muted m-b-0"></p>
          </div>
        </div>    
      </div>
      
      <div class="card-block p-t-0">
      <ui:table tableID="EmailTable" datatable="true" showheader="true" pagelength="50" columnsHidden="0" sortcol="0"
            sortOrder="desc" columns="List Name|List Active|Compose Email" showDelete="true">
        <cfloop array="#rc.lists#" index="emaillist">
            
            <ui:table-row id="#emaillist.getEmailListID()#" 
            editColumn="1"
            editLink="#buildURL(action='emaillist.maintain', querystring='listId=#emaillist.getEmailListID()#')#"
            
            deleteLink="#buildURL(action='emaillist.delete', querystring='listId=#emaillist.getEmailListID()#')#" 
            showdelete="true"
            values="#emaillist.GetEmailListDesc()#|#yesNoFormat(emaillist.getEmailListActive())#|<a href='' class='btn btn-info btn-rounded-deep' alt='Compose Message'><i class='fa fa-pencil'></a>">
            </ui:table-row> 
        </cfloop>
      </ui:table>
      </div>
      
   </div>
   </div>
   </div>
   </div>
</cfoutput>      