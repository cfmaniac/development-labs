<cfset local.routes = variables.framework.routes>


<cfoutput>
	<h1 class="page-heading">#rc.currentsite.sitename# Pages</h1>

	#view("partials/messages")#
	
	<div class="row">
        <div class="col-xl-12">
          <div class="card">
             
            <div class="card-block">
            <h4 class="card-title">Pages</h4>
              <p class="alert alert-info">Pages Denoted with  <i class="fa fa-puzzle-piece"></i>  cannot be amended. These pages redirect to website module.</p>
			
			<table class="table table-striped">
		<thead>
			<tr>
				<th>Title</th>
				<th>Last Updated</th>
				<cfif rc.modules.pages.enableAddDelete>
					<th class="center"><abbr title="Add a page below this page in the site hierarchy">Add</abbr></th>
				</cfif>
				<th class="center"><abbr title="Sort the pages below this page in the site hierarchy">Sort</abbr></th>
				<th class="center">Edit</th>
				<cfif rc.modules.pages.enableAddDelete>
					<th class="center">Delete</th>
				</cfif>
			</tr>
		</thead>

		<tbody>
			<cfloop query="rc.navigation">
				<cfset local.offset = ((rc.navigation.depth - 1) * 15)>
				<cfscript>
						rc.subSQL = "Select page_id as pageid, page_title as title, page_slug as slug, page_updated as updated, page_updatedby as updatedBy from Pages where page_ancestorid = :pageID and site_id=:siteID";
                        rc.subs = new Query();
                        rc.subs.addParam(name="pageID", value="#rc.navigation.pageid#");
                        rc.subs.addParam(name="show", value="1");
                        rc.subs.addParam(name="siteID", value="#rc.config.siteID#");
                        rc.subnavigation = rc.subs.execute(SQL=rc.SubSQL).getResult();
				</cfscript>
				<tr>
					<td <cfif rc.navigation.depth gt 1>class="chevron-right" style="padding-left:#local.offset+20#px; background-position:#local.offset#px 50%"</cfif>>
						<cfif !isRoute(rc.navigation.slug)>
						<a href="#buildURL(action = 'pages.maintain', queryString = 'pageid/#rc.navigation.pageId#')#" title="Edit"> #rc.navigation.title#</a>
						<cfelse>
						<strong>#rc.navigation.title#</strong> <i class="fa fa-puzzle-piece"></i>
						</cfif>

					</td>
					<td title="last updated on #DateFormat(rc.navigation.updated, "medium")# at #TimeFormat(rc.navigation.updated, "HH:MM")#">#getTimeInterval(rc.navigation.updated)# by # rc.navigation.updatedBy#</td>
					<cfif rc.modules.pages.enableadddelete>
						<td class="center">
							<cfif rc.navigation.depth lt rc.modules.pages.maxLevels and !ListFind(rc.modules.pages.suppressAddPage, rc.navigation.pageId)>
								<a href="#buildURL(action = 'pages.maintain', queryString = 'ancestorid/#rc.navigation.pageId#')#" title="Add Page"><i class="fa fa-plus-circle"></i></a>
							</cfif>
						</td>
					</cfif>
					<td class="center">
						<cfif rc.navigation.descendants gt 1>
							<a href="#buildURL(action = 'pages.sort', queryString = 'pageid/#rc.navigation.pageId#')#" title="Sort"><i class="fa fa-sort"></i></a>
						</cfif>
					</td>
					<td class="center">
						<cfif !isRoute(rc.navigation.slug)>
							<a href="#buildURL(action = 'pages.maintain', queryString = 'pageid/#rc.navigation.pageId#')#" title="Edit"><i class="fa fa-pencil-square"></i></a>
						</cfif>
					</td>
					<cfif rc.modules.pages.enableadddelete>
						<td class="center">
								<cfif !isRoute(rc.navigation.slug)>
								<a href="#buildURL('pages.delete')#/pageid/#rc.navigation.pageId#" title="Delete"><i class="fa fa-trash"></i></a>
								</cfif>
						</td>
					</cfif>
				</tr>
				<cfif rc.subnavigation.recordcount GT "0" && rc.navigation.slug NEQ "home">
					<cfloop query="rc.subnavigation">
					<tr>
						<td><cfif !isRoute(rc.subnavigation.slug)>
						<i class="fa fa-chevron-right"></i> <a href="#buildURL(action = 'pages.maintain', queryString = 'pageid/#rc.subnavigation.pageId#')#" title="Edit"> #rc.subnavigation.title#</a>
						<cfelse>
						<i class="fa fa-chevron-right"></i> <strong>#rc.subnavigation.title#</strong> <i class="fa fa-puzzle-piece"></i>
						</cfif></td>
						<td title="last updated on #DateFormat(rc.subnavigation.updated, "medium")# at #TimeFormat(rc.subnavigation.updated, "HH:MM")#">#getTimeInterval(rc.subnavigation.updated)# by # rc.subnavigation.updatedBy#</td>

						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td class="center"><cfif !isRoute(rc.subnavigation.slug)>
							<a href="#buildURL(action = 'pages.maintain', queryString = 'pageid/#rc.subnavigation.pageId#')#" title="Edit"><i class="fa fa-pencil-square"></i></a>
						</cfif></td>
						<td class="center">
								<a href="#buildURL('pages.delete')#/pageid/#rc.subnavigation.pageId#" title="Delete"><i class="fa fa-trash"></i></a>
							</td>

					</tr>
					</cfloop>
				</cfif>



			</cfloop>
		</tbody>
		</table>
		<!---
			<cfloop query="rc.navigation">
				<cfscript>
						rc.subSQL = "Select page_id as pageid, page_title as title, page_slug as slug, page_updated as updated, page_updatedby as updatedBy from Pages where page_ancestorid = :pageID and site_id=:siteID";
                        rc.subs = new Query();
                        rc.subs.addParam(name="pageID", value="#rc.navigation.pageid#");
                        rc.subs.addParam(name="show", value="1");
                        rc.subs.addParam(name="siteID", value="#rc.config.siteID#");
                        rc.subnavigation = rc.subs.execute(SQL=rc.SubSQL).getResult();
				</cfscript>

				<cfset local.offset = ((rc.navigation.depth - 1) * 15)>
				<tr>
					<td <cfif rc.navigation.depth gt 1>class="chevron-right" style="padding-left:#local.offset+20#px; background-position:#local.offset#px 50%"</cfif>>
						<cfif !isRoute(rc.navigation.slug)>
						<a href="#buildURL(action = 'pages.maintain', queryString = 'pageid/#rc.navigation.pageId#')#" title="Edit"> #rc.navigation.title#</a>
						<cfelse>
						<strong>#rc.navigation.title#</strong> <i class="fa fa-puzzle-piece"></i>
						</cfif>

					</td>
					<td title="last updated on #DateFormat(rc.navigation.updated, "medium")# at #TimeFormat(rc.navigation.updated, "HH:MM")#">#getTimeInterval(rc.navigation.updated)# by # rc.navigation.updatedBy#</td>
					<cfif rc.modules.pages.enableadddelete>
						<td class="center">
							<cfif rc.navigation.depth lt rc.modules.pages.maxLevels and !ListFind(rc.modules.pages.suppressAddPage, rc.navigation.pageId)>
								<a href="#buildURL(action = 'pages.maintain', queryString = 'ancestorid/#rc.navigation.pageId#')#" title="Add Page"><i class="fa fa-plus-circle"></i></a>
							</cfif>
						</td>
					</cfif>
					<td class="center">
						<cfif rc.navigation.descendants gt 1>
							<a href="#buildURL(action = 'pages.sort', queryString = 'pageid/#rc.navigation.pageId#')#" title="Sort"><i class="fa fa-sort"></i></a>
						</cfif>
					</td>
					<td class="center">
						<cfif !isRoute(rc.navigation.slug)>
							<a href="#buildURL(action = 'pages.maintain', queryString = 'pageid/#rc.navigation.pageId#')#" title="Edit"><i class="fa fa-pencil-square"></i></a>
						</cfif>
					</td>
					<cfif rc.modules.pages.enableadddelete>
						<td class="center">
							<cfif rc.navigation.descendants eq 0 and !ListFind(rc.modules.pages.suppressDeletePage, rc.navigation.pageId)>
								<a href="#buildURL('pages.delete')#/pageid/#rc.navigation.pageId#" title="Delete"><i class="fa fa-trash"></i></a>
							</cfif>
						</td>
					</cfif>
				</tr>
				<cfif rc.subnavigation.recordcount GT "0">
				<cfloop query="rc.subnavigation">
				<tr>
					<td>
						<cfif !isRoute(rc.subnavigation.slug)>
						|--- <a href="#buildURL(action = 'pages.maintain', queryString = 'pageid/#rc.subnavigation.pageId#')#" title="Edit"> #rc.subnavigation.title#</a>
						<cfelse>
						|--- <strong>#rc.subnavigation.title#</strong> <i class="fa fa-puzzle-piece"></i>
						</cfif>
					</td>
					<td title="last updated on #DateFormat(rc.subnavigation.updated, "medium")# at #TimeFormat(rc.subnavigation.updated, "HH:MM")#">#getTimeInterval(rc.subnavigation.updated)# by # rc.navigation.updatedBy#</td>
					<td class="center">

					</td>
					<td class="center">
						<cfif !isRoute(rc.subnavigation.slug)>
							<a href="#buildURL(action = 'pages.maintain', queryString = 'pageid/#rc.subnavigation.pageId#')#" title="Edit"><i class="fa fa-pencil-square"></i></a>
						</cfif>
					</td>
					<cfif rc.modules.pages.enableadddelete>
						<td class="center">

								<a href="#buildURL('pages.delete')#/pageid/#rc.subnavigation.pageId#" title="Delete"><i class="fa fa-trash"></i></a>

						</td>
					</cfif>
				</tr>
				</cfloop>
				</cfif>
			</cfloop>
		</tbody>
		</table>--->

            </div>


          </div>

      </div>

</cfoutput>
	<!---
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Title</th>
				<th>Last Updated</th>
				<th class="center">View</th>
				<cfif rc.modules.pages.enableAddDelete>
					<th class="center"><abbr title="Add a page below this page in the site hierarchy">Add</abbr></th>
				</cfif>
				<th class="center"><abbr title="Sort the pages below this page in the site hierarchy">Sort</abbr></th>
				<th class="center">Edit</th>
				<cfif rc.modules.pages.enableAddDelete>
					<th class="center">Delete</th>
				</cfif>
			</tr>
		</thead>

		<tbody>
			<cfloop query="rc.navigation">
				<cfset local.offset = ((rc.navigation.depth - 1) * 15)>
				<tr>
					<td <cfif rc.navigation.depth gt 1>class="chevron-right" style="padding-left:#local.offset+20#px; background-position:#local.offset#px 50%"</cfif>>
						<a href="#buildURL(action = 'pages.maintain', queryString = 'pageid/#rc.navigation.pageId#')#" title="Edit"> #rc.navigation.title#</a> <cfif isRoute(rc.navigation.slug)><i class="glyphicon glyphicon-exclamation-sign"></i></cfif>
					</td>
					<td title="last updated on #DateFormat(rc.navigation.updated, "medium")# at #TimeFormat(rc.navigation.updated, "HH:MM")#">#getTimeInterval(rc.navigation.updated)# by # rc.navigation.updatedBy#</td>
					<td class="center"><a href="#buildURL(action = 'public:' & rc.navigation.slug)#" title="View" target="_blank"><i class="glyphicon glyphicon-eye-open"></i></a></td>
					<cfif rc.modules.pages.enableadddelete>
						<td class="center">
							<cfif rc.navigation.depth lt rc.modules.pages.maxLevels and !ListFind(rc.modules.pages.suppressAddPage, rc.navigation.pageId)>
								<a href="#buildURL(action = 'pages.maintain', queryString = 'ancestorid/#rc.navigation.pageId#')#" title="Add Page"><i class="glyphicon glyphicon-plus-sign"></i></a>
							</cfif>
						</td>
					</cfif>
					<td class="center">
						<cfif rc.navigation.descendants gt 1>
							<a href="#buildURL(action = 'pages.sort', queryString = 'pageid/#rc.navigation.pageId#')#" title="Sort"><i class="glyphicon glyphicon-retweet"></i></a>
						</cfif>
					</td>
					<td class="center">
						<cfif !isRoute(rc.navigation.slug)>
							<a href="#buildURL(action = 'pages.maintain', queryString = 'pageid/#rc.navigation.pageId#')#" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a>
						</cfif>
					</td>
					<cfif rc.modules.pages.enableadddelete>
						<td class="center">
							<cfif rc.navigation.descendants eq 0 and !ListFind(rc.modules.pages.suppressDeletePage, rc.navigation.pageId)>
								<a href="#buildURL('pages.delete')#/pageid/#rc.navigation.pageId#" title="Delete"><i class="glyphicon glyphicon-trash"></i></a>
							</cfif>
						</td>
					</cfif>
				</tr>
			</cfloop>
		</tbody>
	</table>

	<p id="routes-alert" style="display:none"><i class="glyphicon glyphicon-exclamation-sign"></i> You cannot amend this page because it redirects to another website feature.</p>

	<p><span class="label label-info">Heads up!</span> Please wait a moment for the navigation to refresh when adding or removing pages.</p>
</cfoutput>

<script>
	jQuery(function($){
		if($("tr>td i.glyphicon glyphicon-exclamation-sign").length){
			$("#routes-alert").show();
		}
	})
</script>
--->
