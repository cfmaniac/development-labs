<cfimport taglib="/lib/tags/" prefix="ui">
<cfoutput>
<h1 class="page-heading">#rc.currentsite.sitename# #rc.PageTitle#</h1>

#view("partials/messages")#

<div class="card">

<div class="card-block">

<div class="stepwizard">
    <div class="stepwizard-row setup-panel">
        <div class="stepwizard-step">
            <a href="##step-1" type="button" class="btn btn-primary btn-circle"><i class="fa fa-file fa-3x"></i></a>
            <p>Page Content</p>
        </div>
        <div class="stepwizard-step">
            <a href="##step-2" type="button" class="btn btn-default btn-circle"><i class="fa fa-bars fa-3x"></i></a>
            <p>Publishing & Navigation</p>
        </div>
        <div class="stepwizard-step">
            <a href="##step-3" type="button" class="btn btn-default btn-circle"><i class="fa fa-bullseye fa-3x"></i></a>
            <p>SEO & Meta Data</p>
        </div>
    </div>
</div>
<form role="form" action="#buildURL('pages.save')#" method="post">
	<input type="hidden" name="context" id="context" value="#HtmlEditFormat(rc.context)#">
	<input type="hidden" name="pageid" id="pageid" value="#HtmlEditFormat(rc.Page.getPageId())#">
    <div class="row setup-content" id="step-1">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3>Page Contents</h3>
                <ui:form-row id="Title" label="Page Title" placeholder="Page Title" name="title" value="#rc.Page.getTitle()#">
                <ui:form-row id="SubTitle" label="Page Sub Title" placeholder="Page Sub Title" name="subtitle" value="#rc.Page.getSubTitle()#">
	            <ui:form-row type="htmlfull" id="content" label="Page Content" name="content" value="#rc.Page.getContent()#">
	         
                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-2">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3>Publishing & Navigation</h3>
                <cfif rc.modules.pages.enableEditSlugs && rc.context is "update">
                <ui:form-row id="slug" label="URL Context (Slug)" placeholder="URL Context" name="slug" value="#rc.Page.getSlug()#">
	            </cfif>
	            
	            <cfset rc.HomePages = ValueList(rc.RootPages.pageid,",")>
		        <cfset rc.HomeLabels = ValueList(rc.RootPages.Title,",")>
	            <ui:input label="Parent Page" type="select" optionlabels="#rc.HomeLabels#" options="#rc.HomePages#" name="ancestorID" value="#rc.Page.getAncestorID()#">
	      
               
               
                <ui:form-row type="boolean" id="showinNav" label="Show Page in Main Nav Bar" name="showinNav" value="#rc.Page.getShowInNav()#">
                
                <button class="btn btn-default prevBtn btn-lg pull-left" type="button" >Prev</button>
                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-3">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3>SEO & Meta Data</h3>
               
			<div class="form-group">
	    
				<label class="c-input c-checkbox">
					<input type="checkbox" name="metagenerated" id="metagenerated" value="true"  <cfif rc.Page.getMetaGenerated()>checked="checked"</cfif>>
		                <span class="c-indicator"></span>
		                <strong>Generate Meta Tags Automatically</strong>
			    </label>
			    #view("partials/failures", {property = "metagenerated"})#
				</div>

			<div class="metatags">
				<div class="form-group <cfif rc.result.hasErrors('metaTitle')>error</cfif>">
					<label for="metatitle">Title <cfif rc.Validator.propertyIsRequired("metaTitle", rc.context)>*</cfif></label>
					<input class="form-control" type="text" name="metatitle" id="metatitle" value="#HtmlEditFormat(rc.Page.getMetaTitle())#" maxlength="100" placeholder="Meta title">
					#view("partials/failures", {property = "metaTitle"})#
				</div>

				<div class="form-group <cfif rc.result.hasErrors('metaDescription')>error</cfif>">
					<label for="metadescription">Description <cfif rc.Validator.propertyIsRequired("metaDescription", rc.context)>*</cfif></label>
					<input class="form-control" type="text" name="metadescription" id="metadescription" value="#HtmlEditFormat(rc.Page.getMetaDescription())#" maxlength="200" placeholder="Meta description">
					#view("partials/failures", {property = "metaDescription"})#
				</div>

				<div class="form-group <cfif rc.result.hasErrors('metaKeywords')>error</cfif>">
					<label for="metakeywords">Keywords <cfif rc.Validator.propertyIsRequired("metaKeywords", rc.context)>*</cfif></label>
					<input class="form-control" type="text" name="metakeywords" id="metakeywords" value="#HtmlEditFormat(rc.Page.getMetaKeywords())#" maxlength="200" placeholder="Meta keywords">
					#view("partials/failures", {property = "metaKeywords"})#
				</div>
          </div>
                
                
                <button class="btn btn-default prevBtn btn-lg pull-left" type="button" >Prev</button>
                <button class="btn btn-success btn-lg pull-right" type="submit">Save #rc.Page.GetTitle()#</button>
            </div>
        </div>
    </div>
</form>
</div>

</div>

</cfoutput>