<cfoutput>
	<ul class="sidebar-submenu">
          <li class="sidebar-menu-item">
            <a class="sidebar-menu-button" href="#buildURL('users')#">Users</a>
          </li>
          <li class="sidebar-menu-item">
            <a class="sidebar-menu-button" href="#buildURL('users.admins')#">Administrators</a>
          </li>
          <li class="sidebar-menu-item">
            <a class="sidebar-menu-button" href="#buildURL('users.admins')#">User Groups</a>
          </li>
          <cfif #rc.modules.securityvalidation.enabled#>
             <li class="sidebar-menu-item">
            <a class="sidebar-menu-button" href="#buildURL('securityvalidation')#">Validation Questions</a>
          </li> 
          </cfif>
        </ul>
</cfoutput>