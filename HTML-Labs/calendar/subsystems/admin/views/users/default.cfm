<cfoutput>
	<h1 class="page-heading">#rc.currentsite.sitename# Users</h1>

	#view("partials/messages")#
	
	<div class="row">
        <div class="col-xl-12">
          <div class="card">
             
            <div class="card-block">
            <h4 class="card-title">Users</h4>
            


	#view("partials/messages")#

	<cfif ArrayLen(rc.users)>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Name</th>
					<th>User Name</th>
					<th>Alliance(s)</th>
					<th>Last Updated</th>
					<th class="center">Edit</th>
					<th class="center">Delete</th>
				</tr>
			</thead>

			<tbody>
				<cfloop array="#rc.users#" index="local.User">
				    <cfquery name="UserAllianceGroups">
                	select user_alliance as Alliance from user_alliances where user_id='#local.User.getUserID()#'
            		</cfquery>
					<tr>
						<td>#local.User.getName()#</td>
						<td>#local.User.getUserName()#</td>
						<td><!---a href="mailto:#local.User.getEmail()#">#local.User.getEmail()#</a--->
						<cfloop list="#UserAllianceGroups.Alliance#" index="alliance" delimiters=",">
							<cfif Alliance contains "n2d">
								<span class="autobot">#alliance#</span><br>
							<cfelseif Alliance contains "ppx">
							    <span class="decepticon">#alliance#</span><br>
							</cfif>

				        </cfloop>
						</td>
						<td>#DateFormat(local.User.getUpdated(), "full")# #TimeFormat(local.User.getUpdated())#</td>
						<td class="center"><a href="#buildURL(action = 'users.maintain', queryString = 'userid/#local.User.getUserId()#')#" title="Edit"><i class="fa fa-pencil-square"></i></a></td>
						<td class="center">
							<cfif local.User.getUserId() neq rc.CurrentUser.getUserId()>
								<a href="#buildURL('users.delete')#/userid/#local.User.getUserId()#" title="Delete"><i class="fa fa-trash"></i></a>
							</cfif>
						</td>
					</tr>
				</cfloop>
			</tbody>
		</table>
	<cfelse>
		<p>There are no user accounts at this time.</p>
	</cfif>
	
	 </div>
            
            
          </div>
          
      </div>
</cfoutput>
