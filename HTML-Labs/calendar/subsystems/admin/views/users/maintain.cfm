<cfimport taglib="/lib/tags/" prefix="ui">
<cfoutput>
	<h1 class="page-heading">#rc.currentsite.sitename# #rc.pagetitle#</h1>
	#view("partials/messages")#
	<form action="#buildURL('users.save')#" enctype="multipart/form-data" method="post" class="form-horizontal" id="user-form">
	<cfif rc.context is "update">
		<input type="hidden" name="userID" value="#rc.userid#">
	</cfif>
	<div class="row">
        <div class="col-xl-9">
          <div class="card">
             
            <div class="card-block">
                <ui:form-row id="Name" label="Name" placeholder="Name" name="name" value="#HtmlEditFormat(rc.User.getName())#">
                <ui:form-row id="UserName" label="UserName" placeholder="User Name" name="username" value="#HtmlEditFormat(rc.User.getUserName())#">
                <ui:form-row id="Email" label="Email Address" placeholder="Email Address" name="email" value="#HtmlEditFormat(rc.User.getEmail())#">
                <cfset rc.CoreOptions = arrayNew(1)>
                <cfloop array="#rc.UserGroups#" index="group">
	                <cfset arrayAppend('#rc.CoreOptions#', '#group.getID#')>
	            </cfloop>
                <cfset rc.options = arrayToList('#rc.CoreOptions#', ',')>
                
                <div class="form-group row">
		        <label class="col-sm-3 form-control-label">User Group </label>	
		        <div class="col-sm-9">
	    			<select class="form-control" label="UserGroup" name="usergroup" id="usergroup">
					<option value="0" <cfif #rc.user.GetUserGroup()# eq "0">selected="selected"</cfif>>Website User (Default)</option>
					<cfloop array="#rc.UserGroups#" index="group">
					<option value="#group.getID()#" <cfif #rc.user.GetUserGroup()# eq #group.getID()#>selected="selected"</cfif>>#group.getName()#</option>
					</cfloop>
								
			        </select>
			
				</div>
				</div>
				
				<div class="form-group row <cfif #rc.User.getUserGroup()# NEQ "1">invisible</cfif>" id="UserSites">
		        <label class="col-sm-3 form-control-label">Administrate Site:</label>	
		        <div class="col-sm-9"> 
	                <select name="usersite" class="form-control" >
	                    <option value="0" <cfif rc.user.GetUserSite() eq "0">selected="selected"</cfif>>All Sites</option>
	                    <cfloop array="#rc.Sites#" index="site"><!------>
	                        <option value="#site.getID()#" <cfif rc.user.GetUserSite() eq #site.getID()#>selected="selected"</cfif>>#site.getTitle()#</option>
	              
	              
	              </cfloop>  
		          <!---option value="">Replicator X (v.5.0.0)</option--->
	            </select>
                </div>
                
		</div>
		            
            <cfif rc.context is "update">
					<p>Enter a password here to change your current password.</p>
				</cfif>
				<ui:form-row id="password" label="Password" placeholder="Password" name="password" value="">
        
				
				
					
				
		
                <ui:form-row type="boolean" id="Active" label="User Active" name="isActive" value="#rc.User.getisActive()#">
            
            </div>
          </div>
        </div>
        <div class="col-xl-3">
            
	        <div class="card">
	            <h4>Other User Options</h4>
		        <div class="card-block">
		        
		        <!---cfif fileExists('#rc.UsersDirectory#/avatars/#rc.User.getUserAvatar()#')>
		        <ui:form-row type="input-image" name="avatar" width="300" value="/globals/users/#rc.User.getUserID()#/avatars/#rc.User.getUserAvatar()#">
		        <cfelse>
		        <ui:form-row type="input-image" name="avatar" width="300" >
		        </cfif--->
			      
			       
		           
                
               
		        </div>
	        </div>
        </div>
        
        <div class="row">
	            <button type="submit" form="user-form" class="btn btn-success btn-rounded-deep pull-right">Save User</button>
          </div>
    </div> 
    </form>  
         
</cfoutput>
<script>
	$('#usergroup').on('change', function(){
		ugroup = $(this).val();
		if(ugroup == '1'){
			$('#UserSites').removeClass('invisible').fadeIn();
		} else {
		    $('#UserSites').fadeOut().addClass('invisible');
		}
	});
</script>