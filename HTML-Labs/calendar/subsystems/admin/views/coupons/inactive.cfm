<cfscript>
    rc.Pagination = createObject("component", "admin.controllers.Pagination").init();
		rc.pagination.setItemsPerPage(75);
		rc.pagination.setUrlPageIndicator("view");
		rc.pagination.setShowNumericLinks(true);
      rc.pagination.setQuerytoPaginate(rc.InActiveCompanies);
      rc.pagination.setMissingNumbersHTML('...');
      rc.pagination.setBaseLink("#buildURL('business')#");
      rc.pagination.setClassName('pull-right');
   
</cfscript>
<cfoutput>
   <div class="col-sm-8 main-content">
   <h2>Inactive Companies Listings: #NumberFormat(rc.InActiveBuinessesCount)#</h2>
   <!---cfdump var="#rc.businessResults#"--->
   <table class="table table-first-column-number data-table display full">
		<thead>
			<tr>
			   
				<th class="col-sm-6">Companies Name</th>
				<th class="col-sm-1">City</th>
				<th class="center col-sm-1">State</th>
				<th class="center col-sm-1">ZipCode</th>
				<th class="center col-sm-1">Vertical Domain</th>
				<th class="center col-sm-1">Active</th>
				<th class="center col-sm-1">Actions</th>
			</tr>
		</thead>
		
		<tbody>
		<!---#rc.pagination.getStartRow()# | #rc.pagination.getEndRow()#--->
<cfloop query="#rc.InActiveCompanies#" startrow="#rc.pagination.getStartRow()#" endrow="#rc.pagination.getEndRow()#">
   <cfscript>
       rc.CompanyVerticals = new query(sql='Select SiteName from sites 
             Inner Join companies_verticals on sites.siteverticalID=companies_verticals.verticalID
             where companies_verticals.companyID = #rc.InActiveCompanies.CompanyID#').execute().getResult();
   </cfscript>
    
          <tr>
			   <td class="col-sm-6"><a href="#buildURL(action='companies.maintain', queryString='id=#rc.InActiveCompanies.CompanyID#')#"><strong>#Name#</strong></a></td>
				<td class="col-sm-1">#addressCity#</td>
				<td class="center col-sm-1">#AddressState#</td>
				<td class="center col-sm-1">#AddressZip#</td>
				<td class="center col-sm-1"><cfloop query="rc.CompanyVerticals">
				#SiteName#<br>
				</cfloop></td>
				<td class="center col-sm-1">#yesnoFormat(active)#</td>
				<td class="center col-sm-1"><a href="#buildURL(action='companies.maintain', queryString='id=#rc.InActiveCompanies.CompanyID#')#" class="btn btn-info">Edit</a></td>
			</tr>
</cfloop>
</tbody>
</table>
#rc.pagination.getRenderedHTML()#
 
  <script>
jQuery(function($){
	
	$('table.data-table.full').dataTable( {
            "dom": 'flrtip',
            "order": [ [0,'asc']],
            "paging": false,
            "info": false,
            "scrollY": "375px",
            "pageLength": 75,
            "scrollCollapse": true,
            //"lengthMenu": [[10, 25, 50, 75, 100 -1], [10, 25, 50,75,100, "All"]],
            "fnDrawCallback": function(o) {
               $('.dataTables_scrollBody').scrollTop(0);
            },
            "language": {
            "lengthMenu": "Display _MENU_ records per page",
            "zeroRecords": "Nothing found - sorry",
            "info": "Showing page _PAGE_ of _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtered from _MAX_ total records)"
        }   
        });
})
</script>
   </div>
   <div class="col-sm-4 main-content">
   <a href="#buildURL('companies.maintain')#" class="btn btn-success">Add Company</a>
   </div>
</cfoutput>