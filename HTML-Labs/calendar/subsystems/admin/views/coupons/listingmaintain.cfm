<cfimport taglib="/lib/replicator/" prefix="ui">

<cfoutput>
#view("partials/messages")#

<ui:container title="Maintain Listings : #rc.listingName#" main="false" formcontrols="true" form="ListingsForm" style="margin-top:0;" size="12" infotext="Listings Zipcode Information is set by the Assigned Merchant">	
	<form action="#buildURL('coupons.savelisting')#" method="post" class="form-horizontal" id="ListingsForm" enctype="multipart/form-data">
	<input type="hidden" name="context" value="#rc.context#">
	<cfif rc.context is "update">
		<input type="hidden" name="listingid" value="#rc.id#">
	</cfif>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label for="title">Coupon Merchant</label>
				<select name="companyid" class="form-control">
				<cfloop query="#rc.Merchants#">
					<option value="#companyid#" <cfif #companyid# EQ #rc.CompanyListings.companyID#>selected="selected"</cfif>>#name#</option>
				</cfloop>
				</select>
			</div>
			
			<ui:form-row id="title" label="Coupon Title" name="listingname" value="#rc.CompanyListings.listingname#">
			<ui:form-row id="description" type="htmlfull" label="Listing Description" name="listingdescription" value="#rc.CompanyListings.listingDescription#">
			
			<div class="form-group">
				<label for="page-content">Coupon Image </label>
				<input type="file" class="form-control" name="listingImage">
				<cfif rc.context is "update" and fileExists('#expandpath('./globals/coupons/listings/#rc.CompanyListings.ListingImage#')#')>
				<img src="/globals/coupons/listings/#rc.CompanyListings.ListingImage#" width="275" height="275">
				</cfif>
			</div>
			
			<ui:form-row id="code" label="Coupon Code" name="listingcode" value="#rc.CompanyListings.listingcode#">
			<ui:form-row type="date" label="Coupon Starts" name="listingStarted" value="#rc.CompanyListings.ListingStarted#"> 
			<ui:form-row type="date" label="Coupon Expires" name="listingEnds" value="#rc.CompanyListings.ListingEnds#"> 
			
			<ui:input label="Listing Active" type="boolean" name="ListingActive" value="#rc.CompanyListings.ListingActive#">
		</div>
		<div class="col-sm-6 text-center">
		
			<h3>Coupon Preview</h3>
			
			<div class="coupon" style="border: 1px dashed ##000; border-radius: 5px; background: ##fff; margin-left: auto; margin-right: auto; text-align: center;">
			<cfif fileExists('#rc.listingsFolder#/#rc.CompanyListings.ListingImage#')>
				<div class="thumbnail-img">
				<div class="overflow-hidden">
				<img class="img-responsive" src="/globals/coupons/listings/#rc.CompanyListings.ListingImage#" alt="#rc.CompanyListings.ListingName#" title="#rc.CompanyListings.ListingName#">
				</div>
				</div>
			</cfif>
				<h2 id="previewTit">#rc.CompanyListings.listingname#</h2>
				<p id="previewDes">#rc.CompanyListings.listingDescription#</p>
				<p id="preivewDates">Expires on: <span id="StartDate">#dateformat(rc.CompanyListings.ListingStarted, "MM/DD/YYYY")#</span> - <span id="EndDate">#dateformat(rc.CompanyListings.ListingEnds,"MM/DD/YYYY")#</span></p>
			</div>
			
			
			
			
				
			
		</div>
	</div>
	</form>
</ui:container>

<script>
	
</script>

<!---cfdump var="#rc#"--->
</cfoutput>