<cfscript>
	if(cgi.rc.method is "post"){
    // A Form has been submitted: but which?
    if(rc.context is "update"){
	    //Company Info Updated
        
        UpdateInfoSQL = 'update companies set name=:name, url=:url, email=:email where companyid=:companyID';
        CompanyInfo = new query();
        CompanyInfo.addParam(name="name", value="#form.name#",cfsqltype="cf_sql_varchar");
        CompanyInfo.addParam(name="url", value="#form.url#",cfsqltype="cf_sql_varchar");
        CompanyInfo.addParam(name="email", value="#form.email#",cfsqltype="cf_sql_varchar");
        CompanyInfo.addParam(name="companyID", value="#rc.id#",cfsqltype="cf_sql_integer");
        CompanyInfoUpdate = companyinfo.execute(sql=UpdateInfoSQL).getresult();
        
        //Company Address
	    UpdateAddySQL = 'update companies_address set AddressStreet=:street, AddressStreet2=:street2, AddressCity=:city, AddressState=:state, AddressZip=:zip, CompanyPHone=:phone, companyFax=:fax, CompanyMobile=:mobile, CompanyEmail=:email where companyID=:companyID';
	    CompanyAddy = new query();
        CompanyAddy.addParam(name="street", value="#form.address#",cfsqltype="cf_sql_varchar");
        CompanyAddy.addParam(name="street2", value="#form.address2#",cfsqltype="cf_sql_varchar");
        CompanyAddy.addParam(name="city", value="#form.city#",cfsqltype="cf_sql_varchar");
        CompanyAddy.addParam(name="state", value="#form.state#",cfsqltype="cf_sql_varchar");
        CompanyAddy.addParam(name="zip", value="#form.zipcode#",cfsqltype="cf_sql_varchar");
        CompanyAddy.addParam(name="phone", value="#form.phone#",cfsqltype="cf_sql_varchar");
        CompanyAddy.addParam(name="fax", value="#form.fax#",cfsqltype="cf_sql_varchar");
        CompanyAddy.addParam(name="mobile", value="#form.mobile#",cfsqltype="cf_sql_varchar");
        CompanyAddy.addParam(name="email", value="#form.email#",cfsqltype="cf_sql_varchar");
        CompanyAddy.addParam(name="companyId", value="#rc.id#",cfsqltype="cf_sql_integer");
        CompanyAddyUpdate = companyAddy.execute(sql=UpdateAddySQL).getresult();
        
        //Update business LAT LON
		ListingGeoInfo = new admin.controllers.Maps().getCoords(
			 Address1	= Trim(form.address)
			,City		= Trim(form.City)
			,State		= Trim(form.State)
			,Zipcode	= ReReplace(form.Zipcode, '[^0-9]', '', 'ALL')
		    );
		if (ListingGeoInfo.Error is ""){
			CompanyGEOSQL = "UPDATE	companies_address SET CompanyLAT = :lat, CompanyLON = :lon WHERE companyID = :CompanyID";
			CompanyGEO = new query();
	        CompanyGEO.addParam(name="lat", value="#ListingGeoInfo.Latitude#",cfsqltype="cf_sql_varchar");
	        CompanyGEO.addParam(name="lon", value="#ListingGeoInfo.Longitude#",cfsqltype="cf_sql_varchar");
	        CompanyGEO.addParam(name="companyId", value="#rc.id#",cfsqltype="cf_sql_integer");
	        CompanyGEOUpdate = companygeo.execute(sql=CompanyGEOSQL).getresult();
			
		}
        
        //Company Profile
	    /*UpdateProfileSQL = 'update companies_profiles set Description=:description, yearEstablished=:yearEstablished, hoursofOperations=:email where companyID=:companyID';
	    CompanyProfile = new query();
        CompanyProfile.addParam(name="street", value="#form.address#",cfsqltype="cf_sql_varchar");
        CompanyProfile.addParam(name="street2", value="#form.address2#",cfsqltype="cf_sql_varchar");
        CompanyProfile.addParam(name="city", value="#form.city#",cfsqltype="cf_sql_varchar");
        CompanyProfile.addParam(name="companyId", value="#rc.id#",cfsqltype="cf_sql_integer");
        CompanyProfileUpdate = companyProfile.execute(sql=UpdateProfileSQL).getresult();*/
        
        location( url = "#buildURL( action = 'coupons.merchants', querystring ='message=400' )#", addtoken = "false" );
        
        
    } else if(rc.context is "create"){
        //writeDump(var="#form#", abort="true");
	    AddCompanySQL = 'insert into companies (name, slug, url, email, startdate, demo,active, site_id) VALUES ( :name, :slug, :url, :email, :startdate,:demo, :active, :siteID)';
        CompanyInfo = new query( );

        CompanyInfo.addParam( name = "name", value = "#form.name#", cfsqltype = "cf_sql_varchar" );
        CompanyInfo.addParam( name = "slug", value = "#ReReplace( LCase( form.name ), "[^a-z0-9]{1,}", "-", "all" )#", cfsqltype = "cf_sql_varchar" );
        CompanyInfo.addParam( name = "url", value = "#form.url#", cfsqltype = "cf_sql_varchar" );
        CompanyInfo.addParam( name = "email", value = "#form.email#", cfsqltype = "cf_sql_varchar" );
        CompanyInfo.addParam( name = "startdate", value = "#dateformat( now( ), "YYYY-MM-DD" )#", cfsqltype = "cf_sql_date" );
        CompanyInfo.addParam( name = "demo", value = "0", cfsqltype = "cf_sql_boolean" );
        CompanyInfo.addParam( name = "active", value = "1", cfsqltype = "cf_sql_boolean" );
        CompanyInfo.addParam( name = "siteID", value = "#rc.CurrentSite#", cfsqltype = "cf_sql_integer" );
        CompanyInfoUpdate = companyinfo.execute( sql = AddCompanySQL ).getresult( );

        CompanyID = new Query( sql = "SELECT @@IDENTITY AS CompanyID FROM Companies" ).execute( ).getREsult( );
        rc.ID = CompanyID.CompanyID;

        //Inserts Company Settings:
        InsertCompanySettings = new Query();
        CompanySettingsSQL = 'insert into companies_settings(companyid) VALUES (#rc.id#)';
        InsertCompanySettings.execute( sql = CompanySettingsSQL ).getresult();
        
        //Inserts Company Profile:
        InsertCompanyProfile = new Query();
        CompanyProfileSQL = 'insert into companies_profiles(companyid) VALUES (#rc.id#)';
        InsertCompanyProfile.execute( sql = CompanyProfileSQL ).getresult();
        
        //Inserts Company Billing:
        InsertCompanyBilling = new Query();
        CompanyBillingSQL = 'insert into companies_billing(companyid) VALUES (#rc.id#)';
        InsertCompanyBilling.execute( sql = CompanyProfileSQL ).getresult();

        //Inserts Address:
        UpdateAddySQL = 'insert into companies_address (AddressStreet,AddressStreet2,AddressCity,AddressState,AddressZip,companyID) VALUES (:street, :street2, :city, :state, :zip, :companyID)';
        CompanyAddy = new query( );
        CompanyAddy.addParam( name = "street", value = "#form.address#", cfsqltype = "cf_sql_varchar" );
        CompanyAddy.addParam( name = "street2", value = "#form.address2#", cfsqltype = "cf_sql_varchar" );
        CompanyAddy.addParam( name = "city", value = "#form.city#", cfsqltype = "cf_sql_varchar" );
        CompanyAddy.addParam( name = "state", value = "#form.state#", cfsqltype = "cf_sql_varchar" );
        CompanyAddy.addParam( name = "zip", value = "#form.zipcode#", cfsqltype = "cf_sql_varchar" );
        CompanyAddy.addParam( name = "companyId", value = "#rc.id#", cfsqltype = "cf_sql_integer" );
        CompanyAddyUpdate = companyaddy.execute( sql = UpdateAddySQL ).getresult( );
        
        ListingGeoInfo = new admin.controllers.Maps( ).getCoords(
            Address1 = Trim( form.address )
            , City = Trim( form.City )
            , State = Trim( form.State )
            , Zipcode = ReReplace( form.Zipcode, '[^0-9]', '', 'ALL' )
            );
        if( ListingGeoInfo.Error is "" ) {
            CompanyGEOSQL = "UPDATE	companies_address SET CompanyLAT = :lat, CompanyLON = :lon WHERE companyID = :CompanyID";
            CompanyGEO = new query( );
            CompanyGEO.addParam( name = "lat", value = "#ListingGeoInfo.Latitude#", cfsqltype = "cf_sql_varchar" );
            CompanyGEO.addParam( name = "lon", value = "#ListingGeoInfo.Longitude#", cfsqltype = "cf_sql_varchar" );
            CompanyGEO.addParam( name = "companyId", value = "#rc.id#", cfsqltype = "cf_sql_integer" );
            CompanyGEOUpdate = companygeo.execute( sql = CompanyGEOSQL ).getresult( );

        }
	    
	    location( url = "#buildURL( action = 'coupons.merchants', querystring ='message=401' )#", addtoken = "false" );
    
    }
    
		
		
	}
</cfscript>

<cfoutput>
<cfimport taglib="/lib/replicator/" prefix="ui">
<cfif #parameterExists(rc.id)#>
    <cfset Title = "Editting Company : #rc.CompanyInfo.Name#">
    <cfelse>
    <cfset Title = "Adding Company:">
    </cfif>
<ui:container title="#Title#" main="true" style="margin-top:0;" size="12" infotext="">
    <cfif #parameterExists(rc.id)#>
    #view('coupons/includes/maintaincompany')#
   <cfelse>
    #view('coupons/includes/addcompany')#
   </cfif>
</ui:container>
   
</cfoutput>
