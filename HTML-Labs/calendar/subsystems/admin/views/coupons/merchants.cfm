<cfimport taglib="/lib/replicator/" prefix="ui">
<cfoutput>
  #view("partials/messages")#
<ui:container title="Merchants" main="true" style="margin-top:0;" size="12" infotext="Currently Active Merchants: #numberformat(len(rc.ActiveBuinessesCount))#">
	<table id="datatable-example" class="table table-bordered table-striped table-hover table-sm">
        <thead>
          <tr>
			    <th class="">&nbsp;</th>
				<th class="">Merchant Name</th>
				<th class="center">City</th>
				<th class="center">State</th>
				<th class="center">ZipCode</th>
				<th class="">&nbsp;</th>
				
			</tr>
        </thead>
        <tbody>
        <cfloop query="#rc.ActiveCompanies#" >
            <tr>
	            <td class="">#rc.ActiveCompanies.CompanyID#</td>
	            <td class=""><a href="#buildURL(action='coupons.maintain', queryString='id=#rc.ActiveCompanies.CompanyID#')#"><strong>#Name#</strong></a></td>
	            <td class="center"><cfif len(addressCity)>#AddressCity#</cfif></td>
				<td class="center"><cfif len(addressState)>#AddressState#</cfif></td>
				<td class="center"><cfif len(addressZip)>#AddressZip#</cfif></td>
				<td class=""><a href="#buildURL(action='coupons.delete', queryString='id=#rc.ActiveCompanies.CompanyID#')#" class="btn btn-danger btn-rounded-deep pull-right"><i class="fa fa-trash-o"></i></td>
            </tr>
        </cfloop>
        </tbody>
    </table>    
</ui:container>
<!---
   <div class="col-sm-8 main-content">
   <h2>Merchant Listings: #NumberFormat(rc.ActiveBuinessesCount)# </h2>
   
   <!---cfdump var="#rc.businessResults#"--->
   <table class="table table-first-column-number data-table display full">
		<thead>
			<tr>
			   
				<th class="col-sm-3">Company Name</th>
				<th class="col-sm-2">City</th>
				<th class="center col-sm-1">State</th>
				<th class="center col-sm-1">ZipCode</th>
				<th class="center col-sm-2">Vertical Domain(s)</th>
				<th class="center col-sm-1">Exclusive Listings</th>
				<th class="center col-sm-1">Active</th>
				<th class="center col-sm-1">Actions</th>
			</tr>
		</thead>
		
		<tbody>
		<!---#rc.pagination.getStartRow()# | #rc.pagination.getEndRow()#--->
<cfloop query="#rc.ActiveCompanies#" >
          <cfscript>
             rc.CompanyVerticals = new query(sql='Select SiteName from sites 
             Inner Join companies_verticals on sites.siteverticalID=companies_verticals.verticalID
             where companies_verticals.companyID = #rc.ActiveCompanies.CompanyID#').execute().getResult();
             rc.CompanyExclusives = new query(sql='Select Count(*) as ExclusiveCount from companies_listings
             INNER JOIN companies on companies.companyID = companies_listings.companyID
             where companies_listings.listingExclusive=1 and companies_listings.companyID=#rc.ActiveCompanies.CompanyID# and companies_listings.listingActive=1').execute().getResult();
             rc.CompanyExclusivesOff = new query(sql='Select Count(*) as ExclusiveCount from companies_listings
             INNER JOIN companies on companies.companyID = companies_listings.companyID
             where companies_listings.listingExclusive =1 and companies_listings.companyID=#rc.ActiveCompanies.CompanyID# and companies_listings.listingActive=0').execute().getResult();
          </cfscript>
    
          <tr>
			    <td class="col-sm-3"><a href="#buildURL(action='companies.maintain', queryString='id=#rc.ActiveCompanies.CompanyID#')#"><strong>#Name#</strong></a></td>
				<td class="col-sm-2">#AddressCity#</td>
				<td class="center col-sm-1">#AddressState#</td>
				<td class="center col-sm-1">#AddressZip#</td>
				<td class="center col-sm-2">
				<cfloop query="rc.CompanyVerticals">
				#SiteName#<br>
				</cfloop>
				
				</td>
				<td class="center col-sm-1"><span class="badge badge-success">#numberformat(rc.CompanyExclusives.ExclusiveCount)#</span> Active<br>
				<span class="badge badge-important">#numberformat(rc.CompanyExclusivesOff.ExclusiveCount)#</span> InActive
				</td>
				<td class="center col-sm-1">#yesnoFormat(active)#</td>
				<td class="center col-sm-1"><a href="#buildURL(action='companies.maintain', queryString='id=#rc.ActiveCompanies.CompanyID#')#" class="btn btn-info">Edit</a></td>
			</tr>
</cfloop>
</tbody>
</table>

 
  <script>
jQuery(function($){
	
	$('table.data-table.full').dataTable( {
            "dom": 'flrtip',
            "order": [ [0,'asc']],
            "paging": false,
            "info": false,
            "scrollY": "375px",
            "pageLength": 75,
            "scrollCollapse": true,
            //"lengthMenu": [[10, 25, 50, 75, 100 -1], [10, 25, 50,75,100, "All"]],
            "fnDrawCallback": function(o) {
               $('.dataTables_scrollBody').scrollTop(0);
            },
            "language": {
            "lengthMenu": "Display _MENU_ records per page",
            "zeroRecords": "Nothing found - sorry",
            "info": "Showing page _PAGE_ of _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtered from _MAX_ total records)"
        }   
        });
})
</script>
   </div>
   <div class="col-sm-4 main-content">
   <a href="#buildURL('companies.maintain')#" class="btn btn-success">Add Company</a>
   </div>
   --->
</cfoutput>