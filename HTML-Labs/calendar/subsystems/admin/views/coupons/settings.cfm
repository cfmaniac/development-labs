<cfimport taglib="/lib/replicator/" prefix="ui">

<cfoutput>
#view("partials/messages")#

<ui:container title="Coupons Settings" main="false" formcontrols="true" form="StoreSettingsForm" style="margin-top:0;" size="12" infotext="These are the Settings for your Coupons Module">
<ul class="nav nav-tabs" role="tablist">
  <cfloop query="#rc.StoreSettings#">
	  <li class="nav-item">
    <a class="nav-link <cfif #rc.StoreSettings.currentrow# is "1">active</cfif>" data-toggle="tab" href="###rc.StoreSettings.settings_category#" role="tab"  href="##">#rc.StoreSettings.settings_category# Settings</a>
  </li>
  </cfloop>
</ul>
<div class="tab-content">
  <cfloop query="#rc.StoreSettings#">  
  <div class="tab-pane <cfif #rc.StoreSettings.currentrow# is "1">active</cfif>" id="#rc.StoreSettings.settings_category#" role="tabpanel">
	<h3>#rc.StoreSettings.settings_category# Settings</h3>
	<cfquery name="rc.StorecatSettings">
		select * from settings_stores where site_id=#rc.CurrentSite# and settings_category='#rc.StoreSettings.settings_category#'
	</cfquery>
	
	<cfloop query="#rc.StorecatSettings#">
	<cfif rc.StorecatSettings.settings_type is "bool">
		<ui:input label="#rc.StorecatSettings.settings_key#" type="boolean" name="#rc.StorecatSettings.settings_key#" value="#rc.StorecatSettings.settings_value#">
	
	<cfelseif rc.StorecatSettings.settings_type is "opt">
		<ui:input label="#rc.StorecatSettings.settings_key#" type="select" options="#rc.StorecatSettings.settings_options#" name="#rc.StorecatSettings.settings_key#" value="#rc.StorecatSettings.settings_value#">
	
	<cfelseif rc.StorecatSettings.settings_type is "text">
		<ui:form-row label="#rc.StorecatSettings.settings_key#" name="#rc.StorecatSettings.settings_key#" value="#rc.StorecatSettings.settings_value#"> 
	</cfif>
	</cfloop>
		  	  
  </div>
  </cfloop>
</div>
</ui:container>
</cfoutput>