<cfimport taglib="/lib/replicator/" prefix="ui">

<cfoutput>
#view("partials/messages")#

<ui:container title="Listings" main="false" style="margin-top:0;" size="12"  infotext="These are the Listings within your Store">
	<table id="datatable-example" class="table table-bordered table-striped table-hover table-sm">
        <thead>
          <tr>
			    <th class="">&nbsp;</th>
				<th class="">Listing Title</th>
				<th class="center">Company</th>
				<th class="center"></th>
				<th class="center">Active</th>
				<th class="">&nbsp;</th>
				
			</tr>
        </thead>
        <tbody>
        <cfloop query="#rc.Lsitings#">
            <tr>
	            <td class=""></td>
	            <td class=""><a href="#buildURL(action='stores.listingmaintain', queryString='id=#rc.Lsitings.listingid#')#"><strong>#ListingName#</strong></a></td>
	            <td class="center"><a href="#buildURL(action='stores.maintain', queryString='id=#rc.Lsitings.CompanyID#')#">#Name#</a></td>
				<td class="center"></td>
				<td class="center">#yesNoFormat(ListingActive)#</td>
				<td class=""><a href="#buildURL(action='stores.deleteProduct', queryString='id=#rc.Lsitings.listingid#')#" class="btn btn-danger btn-rounded-deep pull-right"><i class="fa fa-trash-o"></i></td>
            </tr>
        </cfloop>
        </tbody>
    </table>    
	
	
	<!---cfdump var="#rc.Lsitings#" expand="false"--->
</ui:container>

</cfoutput>