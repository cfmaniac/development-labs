<cfif structKeyExists(form, "listingImage") and form.listingImage NEQ "">
<cffile action="upload" fileField="listingImage" destination="#expandPath('./globals/#rc.sitename#/coupons/listings/')#" nameConflict="unique">
</cfif>

<cfscript>
	    //param name ="rc.siteId" default=1;
        param name ="rc.companyID" default="";
        param name ="rc.listingName" default="";
        param name ="rc.listingdescription" default="";
        param name ="rc.listingimage" default="";
        param name ="rc.listingcode" default="";
        param name ="rc.listingStarted" default="";
        param name ="rc.listingEnds" default="";
        param name ="rc.listingActive" default="1";
        param name ="rc.siteID" default="#rc.CurrentSite#";
        
        
        
        SaveListing = new Query();
        if(rc.context is "update"){
						
			if(structKeyExists(form,"listingImage") && form.listingImage !=""){
			ListingSQL = "Update Companies_Listings SET listingName=:listingName, listingDescription=:listingDescription, listingCode=:ListingCode, ListingStarted=:ListingStarted,
			              ListingEnds=:ListingEnds, listingImage=:listingImage, companyID=:companyID where listingID =:listingID";
			              
			SaveListing.addParam(name="listingID", value="#rc.listingid#", cfsqltype="CF_SQL_INTEGER");
			SaveListing.addParam(name="listingImage", value="#CFFile.ClientFile#", cfsqltype="CF_SQL_VarChar" );
			} else {
			ListingSQL = "Update Companies_Listings SET listingName=:listingName, listingDescription=:listingDescription, listingCode=:ListingCode, ListingStarted=:ListingStarted,
			              ListingEnds=:ListingEnds, companyID=:companyID where listingID =:listingID";
			              
			SaveListing.addParam(name="listingID", value="#rc.listingid#", cfsqltype="CF_SQL_INTEGER");              
			}              
        } else {
	          
	        if(structKeyExists(form,"listingImage") && form.listingImage !=""){
			ListingSQL = "INSERT into Companies_Listings (listingName, ListingDescription, ListingCode, ListingImage, ListingStarted, ListingEnds, CompanyID, Site_ID) VALUES 
	                      (:listingName, :ListingDescription, :ListingCode, :ListingImage, :ListingStarted, :ListingEnds, :CompanyID, :siteID)";
	        
	        
			SaveListing.addParam(name="listingImage", value="#CFFile.ClientFile#", cfsqltype="CF_SQL_VarChar" );
			
			} else {
			    ListingSQL = "INSERT into Companies_Listings (listingName, ListingDescription, ListingCode, ListingStarted, ListingEnds, CompanyID, Site_ID) VALUES 
	                      (:listingName, :ListingDescription, :ListingCode, :ListingStarted, :ListingEnds, :CompanyID, :siteID)";
				SaveListing.addParam(name="siteID", value="#rc.CurrentSite#", cfsqltype="CF_SQL_INTEGER");
			}                   
        }
        
       
        
        SaveListing.addParam(name="companyID", value="#rc.companyID#", cfsqltype="CF_SQL_INTEGER");
        SaveListing.addParam(name="listingName", value="#rc.listingName#", cfsqltype="CF_SQL_VARCHAR");
        SaveListing.addParam(name="listingDescription", value="#rc.listingdescription#", cfsqltype="CF_SQL_VARCHAR");
        SaveListing.addParam(name="listingcode", value="#rc.listingcode#", cfsqltype="CF_SQL_VARCHAR");
        SaveListing.addParam(name="listingStarted", value="#rc.listingStarted#", cfsqltype="CF_SQL_DATETIME");
        SaveListing.addParam(name="listingEnds", value="#rc.listingends#", cfsqltype="CF_SQL_DATETIME");
        SaveListing.addParam(name="listingActive", value="#rc.listingActive#", cfsqltype="CF_SQL_BOOLEAN");
        SaveListing.addParam(name="siteID", value="#rc.CurrentSite#", cfsqltype="CF_SQL_INTEGER");
        
        //writeDump(var=#form#, abort=true);
        
        rc.ListingSaves = SaveListing.execute(sql=ListingSQL).getResult();
        //variables.fw.redirect("stores.listings/message/501");
        location(url='admin~coupons.listings/message/501', addtoken='false'); 
</cfscript>