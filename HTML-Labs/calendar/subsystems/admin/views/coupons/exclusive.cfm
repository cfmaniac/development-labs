<cfoutput>
   <div class="col-sm-12 main-content">
   <h2>Exclusive Companies Listings: #NumberFormat(rc.ActiveExclusiveCount)#</h2>
   <!---cfdump var="#rc.businessResults#"--->
   <table class="table table-first-column-number data-table display full">
		<thead>
			<tr>
			   
				<th class="col-sm-6">Companies Name</th>
				<th class="col-sm-1">City</th>
				<th class="center col-sm-1">State</th>
				<th class="center col-sm-1">ZipCode</th>
				<th class="center col-sm-1">Vertical Domain</th>
				<th class="center col-sm-1">Active</th>
				<th class="center col-sm-1">Actions</th>
			</tr>
		</thead>
		
		<tbody>
<cfloop array="#rc.ActiveExclusiveListings#" index="local.Exclusive">
   
    
          <tr>
			   <td class="col-sm-6"><strong>#local.Exclusive.getName()#</strong></td>
				<td class="col-sm-1">#local.Exclusive.getaddressCity()#</td>
				<td class="center col-sm-1">#local.Exclusive.getAddressState()#</td>
				<td class="center col-sm-1">#local.Exclusive.getAddressZip()#</td>
				<td class="center col-sm-1">#local.Exclusive.getSiteDomain()#</td>
				<td class="center col-sm-1"><!---#yesnoFormat(local.Exclusive.getactive())#--->
				                             <ul>
                                             <li>
                                                <div class="btn-group btn-toggle"> 
                                                <button class="btn btn-xs btn-default">ON</button>
                                                <button class="btn btn-xs btn-danger active">OFF</button>
                                                </div>
                                              </li>
				                             </ul>
				
				</td>
				<td class="center col-sm-1"><ul></ul></td>
			</tr>
</cfloop>
</tbody>
</table>
 <script>
jQuery(function($){
	
	$('table.data-table.full').dataTable( {
            "dom": 'flrtip',
            "order": [ [0,'asc']],
            "paging":   true,
            "info": true,
            "scrollY": "375px",
            "pageLength": 75,
            "scrollCollapse": true,
            "lengthMenu": [[10, 25, 50, 75, 100 -1], [10, 25, 50,75,100, "All"]],
            "fnDrawCallback": function(o) {
               $('.dataTables_scrollBody').scrollTop(0);
            },
            "language": {
            "lengthMenu": "Display _MENU_ records per page",
            "zeroRecords": "Nothing found - sorry",
            "info": "Showing page _PAGE_ of _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtered from _MAX_ total records)"
        }   
        });
})
</script>
   </div>
</cfoutput>