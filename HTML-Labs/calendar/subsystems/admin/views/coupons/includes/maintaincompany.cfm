<cfimport taglib="/lib/replicator/" prefix="ui">
<cfoutput>
	<ul class="nav nav-tabs">
            <li class="nav-item">
              <a class="nav-link active" href="##one" data-toggle="tab">Information</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="##two" data-toggle="tab">Locations & Addresses</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="##three" data-toggle="tab"><cfif rc.CurrentSite is "3">Coupons <cfelse>Listings</cfif> </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="##four" data-toggle="tab">Settings</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="##five" data-toggle="tab">Users <span class="label label-success">#numberformat(rc.CompanyUsers.recordcount)#</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="##six" data-toggle="tab">Billing </a>
            </li>
            
            
            
  
    </ul>
    <form name="companyaddress" method="post">
    <div class="tab-content p-a-1 m-b-1">
            <div class="tab-pane active" id="one"> <h3>Merchant Information</h3>
                 <ui:form-row label="Company Name" name="name" value="#rc.CompanyInfo.name#">
                 <ui:form-row label="URL" infotext="your company Website" name="url" value="#rc.CompanyInfo.url#">
                 <ui:form-row label="Email Address" name="email" value="#rc.CompanyInfo.Email#"> 
                 
            </div>
            <div class="tab-pane" id="two"> <h3>Locations</h3>
            <cfif rc.CompanyAddress.recordCount GTE "1">
            <cfloop query="rc.CompanyAddress">
            
                <ui:form-row label="Site Address" name="address" value="#rc.CompanyAddress.AddressStreet#">
		        <ui:form-row label="Site Address (additional)" name="address2" value="#rc.CompanyAddress.AddressStreet2#">
		        <ui:form-row label="Site City" name="city" value="#rc.CompanyAddress.AddressCity#">
		        <ui:form-row type="state" label="Site State" name="state" value="#rc.CompanyAddress.AddressState#">
		        <ui:form-row label="Site ZipCode" name="zipcode" value="#rc.CompanyAddress.AddressZip#">
            
            <hr>
            <h4>Location Contact Information</h4>
                 <ui:form-row label="Company Phone" name="phone" value="#rc.CompanyAddress.CompanyPhone#">
                 <ui:form-row label="Company Mobile" name="mobile" value="#rc.CompanyAddress.CompanyMobile#">
                 <ui:form-row label="Company Fax" name="fax" value="#rc.CompanyAddress.CompanyFax#">
                 <ui:form-row label="Location Email" name="email" value="#rc.CompanyAddress.CompanyEmail#">
            
            
            
            
            <cfif rc.CompanyAddress.currentrow GT 1>
					<button id="removeAddy" class="btn btn-danger" data-addressID="#rc.CompanyAddress.AddressID#" datacompanyID="#rc.id#">Remove Address</button>
            </cfif>	
            </cfloop>
            <cfelse>
            <div class="row">
	            <a href="javaScript:void(0);" class="btn btn-info" data-toggle="modal" data-target="##AddressModal">Add Address</a>
            </div>
            </cfif>
           
            </div>
            <div class="tab-pane" id="three"> <h3>Listings</h3>
            <p>Use the Checkbox next to a Listing to remove it from this account</p>
        <table class="table table-first-column-number data-table display full">
          <thead>
          <tr>
          <th></th>
          <th>Name</th>
          <th>ZipCode</th>
          
          <th>Listing Started</th>
          <th>Listing Expires</th>
          <th>Active</th>
          </tr>
          </thead>
          <tbody>
          <cfloop query="#rc.CompanyListings#">
                    
            <tr>
            <td><input onclick="" type="checkbox" class="DeleteListing" name="deleteListing" data-Listing="#ListingZipCode#"></td>
            <td><a href="#buildURL(action='stores.listingmaintain', queryString='id=#rc.CompanyListings.listingid#')#">#ListingName#</a></td>        
            <td>#ListingZipCode#</td>
            
            <td>#dateformat(listingStarted, "MM/DD/YYYY")#</td>  
            <td>#dateformat(listingEnds, "MM/DD/YYYY")#</td>  
            <td>
            #yesNoFormat(ListingActive)#
            
				</td>    
            </tr>
          </cfloop>
          </tbody>
          </table>
            </div>
            <div class="tab-pane" id="four"> <h3>Settings</h3>
             <ui:input label="Auto Renew Company" infotext="Auto Renew Billing" type="boolean" name="autonew" value="#rc.CompanySettings.autorenew#">
             <ui:input label="Use Company Email as Primary Email" infotext="Use the Company Email as Primary?" type="boolean" name="UseCompanyMailasPrimary" value="#rc.CompanySettings.UseCompanyMailasPrimary#">
             <ui:input label="Use Contact Email as Primary Email" infotext="Use the Primary Contacts Email as Primary?" type="boolean" name="UseContactMailasPrimary" value="#rc.CompanySettings.UseContactMailasPrimary#">
             <ui:input label="Show Company URL" infotext="Show the URL on your Profile" type="boolean" name="ShowURL" value="#rc.CompanySettings.showurl#">
             <ui:input label="Show Click to Call" infotext="Allow People to Call you"type="boolean" name="ShowClicktoCall" value="#rc.CompanySettings.showClicktoCall#">
             <ui:input label="Show Map" infotext="Show Map of your Locations/Addresses" type="boolean" name="ShowMap" value="#rc.CompanySettings.showmap#">
             <ui:input label="Show Directions" infotext="Show Directions to your Locations/Addresses" type="boolean" name="ShowDirections" value="#rc.CompanySettings.showDirections#">
             <ui:input label="Show Print" infotext="Enable Printing of Listings" type="boolean" name="ShowPrint" value="#rc.CompanySettings.showPrint#">
             <ui:input label="Show Contact" infotext="Allow People to Contact you" type="boolean" name="ShowContact" value="#rc.CompanySettings.showContact#">
             <ui:input label="Show Nearby" infotext="Show nearby Companies" type="boolean" name="ShowNearby" value="#rc.CompanySettings.showNearby#">
             <ui:input label="Show Gallery" infotext="Show the Photo Gallery on your Profile" type="boolean" name="ShowGallery" value="#rc.CompanySettings.ShowGallery#">
             <ui:input label="Show Staff" infotext="Show your Staff on the Gallery" type="boolean" name="ShowContact" value="#rc.CompanySettings.ShowStaff#">
             <ui:input label="Show Reviews" infotext="Show your reviews" type="boolean" name="ShowContact" value="#rc.CompanySettings.ShowReviews#">
            
             <!---input type="submit" name="submitSettings" class="btn btn-success pull-right" value="Save Settings"--->
            </div>
            <div class="tab-pane" id="five">
            
             <div class="row">
	             <h3>Users / Staff</h3> <a href="javaScript:void(0);" class="btn btn-info pull-right" data-toggle="modal" data-target="##UsersModal">Add User</a>
            </div>
            <cfloop query="#rc.CompanyUsers#">
            <div class="form-group">
                
		        <!---label class="control-label" for="metakeywords">#rc.CompanyUsers.user_name#</label>
							<div class="controls" rel="#rc.CompanyUsers.user_id#">
							<input type="checkbox" <cfif rc.CompanyUsers.user_active>checked="checked"</cfif> name="user_active" value="1" class="change-color-switch Updateuser" data-user="#rc.CompanyUsers.user_id#" data-on-color="success" data-off-color="danger">
				</div--->
			 
				   <ui:input label="#rc.CompanyUsers.user_name#" infotext="Enable/Disable Access for this user" type="boolean" name="user_active" value="#rc.CompanyUsers.user_active#">
             
			</div>  
      </cfloop>
            </div>
            <div class="tab-pane" id="six"> 
            <cfquery name="CurrentBilling" dbtype="query">
		      select * from rc.CompanyBilling where BillingStatus='Due'
		    </cfquery>
		    <cfquery name="PastBilling" dbtype="query">
		      select * from rc.CompanyBilling where BillingStatus='Paid' Order by BillingDate DESC
		    </cfquery>
		    <ul class="nav nav-tabs">
            <li class="nav-item">
              <a class="nav-link active" href="##current" data-toggle="tab">Current Billing</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="##history" data-toggle="tab">Billing History</a>
            </li>
            </ul>
		    <div class="tab-content p-a-1 m-b-1">
            <div class="tab-pane active" id="current"> <h3>Current Billing</h3>
                <table class="table table-first-column-number data-table display full">
						<thead>
							<tr>
								<th class="col-sm-2">Term</th>
								<th class="col-sm-2">Price</th>
								<th class="col-sm-1">Status</th>
								<th class="center col-sm-1">Billing Date</th>
								<th class="center col-sm-1">Date Due By</th>
								<th class="center col-sm-3">Notes</th>
								<th class="center col-sm-2"></th>
							</tr>
						</thead>
						
						<tbody>
			      <cfloop query="CurrentBilling">
			         <tr>
			         <td class="col-sm-2">#BillingTerm# Months </td>
			         <td class="col-sm-2">#dollarFormat(billingPrice)# </td>
			         <td class="col-sm-1">#billingStatus#</td>
			         <td class="col-sm-1">#dateformat(BillingDate, "MM/DD/YYYY")#</td>
			         <td class="class="col-sm-2"><strong>#dateformat(BillingDateDue, "MM/DD/YYYY")#</strong></td>
			         <td class="col-sm-3">#BillingNotes#</td>
			         <td class="col-sm-2"></td>
			         </tr>  
			      </cfloop>
			      </tbody>
			      </table>
            </div>
            <div class="tab-pane" id="history"> <h3>Billing History</h3>
                <table class="table table-first-column-number data-table display full">
			<thead>
				<tr>
					<th class="col-sm-2">Term</th>
					<th class="col-sm-2">Price</th>
					<th class="col-sm-1">Status</th>
					<th class="center col-sm-1">Billing Date</th>
					<th class="center col-sm-1">Paid Date</th>
					<th class="center col-sm-3">Notes</th>
					<th class="center col-sm-1"></th>
				</tr>
			</thead>
			
			<tbody>
      <cfloop query="PastBilling">
         <tr>
         <td class="col-sm-2">#BillingTerm# Months </td>
         <td class="col-sm-2">#dollarFormat(billingPrice)# </td>
         <td class="col-sm-1">#billingStatus#</td>
         <td class="col-sm-1">#dateformat(BillingDate, "MM/DD/YYYY")#</td>
         <td class="col-sm-1">#dateformat(BillingPaidDate, "MM/DD/YYYY")#</td>
         <td class="col-sm-3">#BillingNotes#</td>
         <td class="center col-sm-1"></td>
         </tr> 
      </cfloop>
      </tbody>
      </table>
            </div>
            
            </div>
		    
            </div>
    </div>
    <input type="submit" name="submitAddy" value="Update Merchant Information" class="btn btn-success pull-right">
    </form>
    
     <!---Address Modal Window--->
            <!-- Modal -->
			<div class="modal fade" id="AddressModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			        <h4 class="modal-title" id="myModalLabel">Add New Address for #rc.CompanyInfo.Name#</h4>
			      </div>
			      <div class="modal-body">
			        <form name="Newcompanyaddress" method="post">
                <ui:form-row label="Site Address" name="address" value="">
		        <ui:form-row label="Site Address (additional)" name="address2" value="">
		        <ui:form-row label="Site City" name="city" value="">
		        <ui:form-row type="state" label="Site State" name="state" value="">
		        <ui:form-row label="Site ZipCode" name="zipcode" value="">
            
            <hr>
            <h4>Location Contact Information</h4>
                 <ui:form-row label="Company Phone" name="phone" value="">
                 <ui:form-row label="Company Fax" name="fax" value="">
                 
                 <ui:form-row label="Location Email" name="phone" value="">
            
            
            </form>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			        <input type="submit" name="submitAddy" value="Update Address Information" class="btn btn-success pull-right">
			      </div>
			    </div>
			  </div>
			</div>
            <!---End--->
            
    <!---Address Modal Window--->
            <!-- Modal -->
			<div class="modal fade" id="UsersModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			        <h4 class="modal-title" id="myModalLabel">New User for #rc.CompanyInfo.Name#</h4>
			      </div>
			      <div class="modal-body">
			        ...
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			        <button type="button" class="btn btn-primary">Save changes</button>
			      </div>
			    </div>
			  </div>
			</div>
            <!---End--->        
</cfoutput>    