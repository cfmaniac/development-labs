<cfimport taglib="/lib/replicator/" prefix="ui">
<cfoutput> <form name="companyaddress" method="post">
	<ul class="nav nav-tabs">
            <li class="nav-item">
              <a class="nav-link active" href="##one" data-toggle="tab">Information</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="##two" data-toggle="tab">Locations & Addresses</a>
            </li>
            
            <li class="nav-item">
              <a class="nav-link" href="##four" data-toggle="tab">Settings</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="##five" data-toggle="tab">Users </a>
            </li>
            
            
            
            
  
    </ul>
    <form name="newmerchant" method="post">
    <div class="tab-content p-a-1 m-b-1">
            <div class="tab-pane active" id="one"> <h3>Merchant Information</h3>
                 <ui:form-row label="Company Name" name="name" value="">
                 <ui:form-row label="URL" infotext="your company Website" name="url" value="">
                 <ui:form-row label="Email Address" name="email" value=""> 
                 
            </div>
            <div class="tab-pane" id="two"> <h3>Locations</h3>
            
                <ui:form-row label="Site Address" name="address" value="">
		        <ui:form-row label="Site Address (additional)" name="address2" value="">
		        <ui:form-row label="Site City" name="city" value="">
		        <ui:form-row type="state" label="Site State" name="state" value="">
		        <ui:form-row label="Site ZipCode" name="zipcode" value="">
            
            <hr>
            <h4>Location Contact Information</h4>
                 <ui:form-row label="Company Phone" name="phone" value="">
                 <ui:form-row label="Company Mobile" name="mobile" value="">
                 <ui:form-row label="Company Fax" name="fax" value="">
                 <ui:form-row label="Location Email" name="email" value="">
            
            
            
            
           
            
            
           
            </div>
            
            <div class="tab-pane" id="four"> <h3>Settings</h3>
             <ui:input label="Auto Renew Company" infotext="Auto Renew Billing" type="boolean" name="autonew" value="1">
             <ui:input label="Use Company Email as Primary Email" infotext="Use the Company Email as Primary?" type="boolean" name="UseCompanyMailasPrimary" value="1">
             <ui:input label="Use Contact Email as Primary Email" infotext="Use the Primary Contacts Email as Primary?" type="boolean" name="UseContactMailasPrimary" value="1">
             <ui:input label="Show Company URL" infotext="Show the URL on your Profile" type="boolean" name="ShowURL" value="1">
             <ui:input label="Show Click to Call" infotext="Allow People to Call you"type="boolean" name="ShowClicktoCall" value="1">
             <ui:input label="Show Map" infotext="Show Map of your Locations/Addresses" type="boolean" name="ShowMap" value="1">
             <ui:input label="Show Directions" infotext="Show Directions to your Locations/Addresses" type="boolean" name="ShowDirections" value="0">
             <ui:input label="Show Print" infotext="Enable Printing of Listings" type="boolean" name="ShowPrint" value="0">
             <ui:input label="Show Contact" infotext="Allow People to Contact you" type="boolean" name="ShowContact" value="1">
             <ui:input label="Show Nearby" infotext="Show nearby Companies" type="boolean" name="ShowNearby" value="0">
             <ui:input label="Show Gallery" infotext="Show the Photo Gallery on your Profile" type="boolean" name="ShowGallery" value="0">
             <ui:input label="Show Staff" infotext="Show your Staff on the Gallery" type="boolean" name="ShowContact" value="0">
             <ui:input label="Show Reviews" infotext="Show your reviews" type="boolean" name="ShowContact" value="0">
            
             <input type="submit" name="submitSettings" class="btn btn-success pull-right" value="Save Settings">
            </div>
            <div class="tab-pane" id="five">
            
             <div class="row">
	             <h3>Users / Staff</h3> <a href="javaScript:void(0);" class="btn btn-info pull-right" data-toggle="modal" data-target="##UsersModal">Add User</a>
            </div>
            
            </div>
          
            
            
    </div>
    <input type="submit" name="submitAddy" value="Save Merchant Information" class="btn btn-success pull-right">
    </form>
    
     
</cfoutput>    