<cfimport taglib="/lib/replicator/" prefix="ui">
<cfoutput>
  #view("partials/messages")#
<ui:container title="Store Overview" main="true" style="margin-top:0;" size="12" infotext="">
	<div class="card-block p-t-0">
        <div class="row placeholders">
                <div class="col-xs-6 col-sm-3 placeholder text-center">
                    <img src="//placehold.it/200/dddddd/fff?text=<cfif rc.ActiveCompanies.recordCount>#NumberFormat(rc.ActiveCompanies.recordCount)#<cfelse>0</cfif>" class="center-block img-responsive img-circle" alt="Generic placeholder thumbnail">
                    <h5><a href="#buildURL('coupons.merchants')#"><i class="fa fa-institution"></i> Active Merchants</a></h5>
                    <span class="text-muted"></span>
                </div>
                <div class="col-xs-6 col-sm-3 placeholder text-center">
                    <img src="//placehold.it/200/e4e4e4/fff?text=<cfif rc.ActiveListings.recordcount>#NumberFormat(rc.ActiveListings.recordcount)#<cfelse>0</cfif>" class="center-block img-responsive img-circle" alt="Generic placeholder thumbnail">
                    <h5><a href="#buildURL('coupons.listings')#"><i class="fa fa-file-text-o"></i> Active Listings</a></h5>
                    
                </div>
                <div class="col-xs-6 col-sm-3 placeholder text-center">
                    <img src="//placehold.it/200/d6d6d6/fff?text=<cfif rc.InActiveListings.recordcount>#NumberFormat(rc.InActiveListings.recordcount)#<cfelse>0</cfif>" class="center-block img-responsive img-circle" alt="Generic placeholder thumbnail">
                    <h5><a href="#buildURL('blog')#"><i class="fa fa-file-text"></i> InActive Listings </a></h5>
                    <span class="text-muted">Expired or Inactive Listings</span>
                </div>
                <!---div class="col-xs-6 col-sm-3 placeholder text-center">
                    <img src="//placehold.it/200/e0e0e0/fff?text=#numberFormat(rc.unreadBlogComments)#" class="center-block img-responsive img-circle" alt="Generic placeholder thumbnail">
                    <h5><i class="fa fa-comments"></i> Blog Comments</h5>
                    
                </div--->
        </div>
        </div>
</ui:container>
<!---
   <div class="col-sm-8 main-content">
   <h2>Merchant Listings: #NumberFormat(rc.ActiveBuinessesCount)# </h2>
   
   <!---cfdump var="#rc.businessResults#"--->
   <table class="table table-first-column-number data-table display full">
		<thead>
			<tr>
			   
				<th class="col-sm-3">Company Name</th>
				<th class="col-sm-2">City</th>
				<th class="center col-sm-1">State</th>
				<th class="center col-sm-1">ZipCode</th>
				<th class="center col-sm-2">Vertical Domain(s)</th>
				<th class="center col-sm-1">Exclusive Listings</th>
				<th class="center col-sm-1">Active</th>
				<th class="center col-sm-1">Actions</th>
			</tr>
		</thead>
		
		<tbody>
		<!---#rc.pagination.getStartRow()# | #rc.pagination.getEndRow()#--->
<cfloop query="#rc.ActiveCompanies#" >
          <cfscript>
             rc.CompanyVerticals = new query(sql='Select SiteName from sites 
             Inner Join companies_verticals on sites.siteverticalID=companies_verticals.verticalID
             where companies_verticals.companyID = #rc.ActiveCompanies.CompanyID#').execute().getResult();
             rc.CompanyExclusives = new query(sql='Select Count(*) as ExclusiveCount from companies_listings
             INNER JOIN companies on companies.companyID = companies_listings.companyID
             where companies_listings.listingExclusive=1 and companies_listings.companyID=#rc.ActiveCompanies.CompanyID# and companies_listings.listingActive=1').execute().getResult();
             rc.CompanyExclusivesOff = new query(sql='Select Count(*) as ExclusiveCount from companies_listings
             INNER JOIN companies on companies.companyID = companies_listings.companyID
             where companies_listings.listingExclusive =1 and companies_listings.companyID=#rc.ActiveCompanies.CompanyID# and companies_listings.listingActive=0').execute().getResult();
          </cfscript>
    
          <tr>
			    <td class="col-sm-3"><a href="#buildURL(action='companies.maintain', queryString='id=#rc.ActiveCompanies.CompanyID#')#"><strong>#Name#</strong></a></td>
				<td class="col-sm-2">#AddressCity#</td>
				<td class="center col-sm-1">#AddressState#</td>
				<td class="center col-sm-1">#AddressZip#</td>
				<td class="center col-sm-2">
				<cfloop query="rc.CompanyVerticals">
				#SiteName#<br>
				</cfloop>
				
				</td>
				<td class="center col-sm-1"><span class="badge badge-success">#numberformat(rc.CompanyExclusives.ExclusiveCount)#</span> Active<br>
				<span class="badge badge-important">#numberformat(rc.CompanyExclusivesOff.ExclusiveCount)#</span> InActive
				</td>
				<td class="center col-sm-1">#yesnoFormat(active)#</td>
				<td class="center col-sm-1"><a href="#buildURL(action='companies.maintain', queryString='id=#rc.ActiveCompanies.CompanyID#')#" class="btn btn-info">Edit</a></td>
			</tr>
</cfloop>
</tbody>
</table>

 
  <script>
jQuery(function($){
	
	$('table.data-table.full').dataTable( {
            "dom": 'flrtip',
            "order": [ [0,'asc']],
            "paging": false,
            "info": false,
            "scrollY": "375px",
            "pageLength": 75,
            "scrollCollapse": true,
            //"lengthMenu": [[10, 25, 50, 75, 100 -1], [10, 25, 50,75,100, "All"]],
            "fnDrawCallback": function(o) {
               $('.dataTables_scrollBody').scrollTop(0);
            },
            "language": {
            "lengthMenu": "Display _MENU_ records per page",
            "zeroRecords": "Nothing found - sorry",
            "info": "Showing page _PAGE_ of _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtered from _MAX_ total records)"
        }   
        });
})
</script>
   </div>
   <div class="col-sm-4 main-content">
   <a href="#buildURL('companies.maintain')#" class="btn btn-success">Add Company</a>
   </div>
   --->
</cfoutput>