<!---<cfdump var="#rc.QuestionInfo#">
<cfdump var="#rc.Answers#">--->
<cfimport taglib="/lib/tags/" prefix="ui">
<cfoutput>
  #view("partials/messages")#
  
 <ui:container title="Maintain Question" main="false" style="margin-top:0;" size="12">
    <form name="questionsForm" method="Post" action="#buildURL('securityvalidation.save')#">
	<input type="hidden" name="qid" value="#rc.qid#">
	<input type="hidden" name="context" value="#rc.context#">

    <h3>Question:</h3>
    <ui:form-row type="htmlmin" id="question" label="Question" name="question" value="#rc.QuestionInfo.validation_question#">
 
    
    <div class="row">
     <h3>Answers   <a class="addsection btn btn-info pull-right">Add Answer</a></h3>
     <hr>
    </div>  
    
   <div id="sections">
   <cfif len(#rc.QuestionInfo.validation_answers#)>
       <cfloop list="#rc.QuestionInfo.validation_answers#" index="answers">
       
       <div class="section row">
       
        <ui:form-row type="text" id="validation_answers" name="validation_answers" Label="Answer" value="#answers#">
        <div class="row">
            <a href="##" class='remove btn btn-danger btn-rounded-deep pull-right'><i class="fa fa-trash-o"></i></a>
        </div>
        <hr>
	</div>
   </cfloop>
   <cfelse>
   <div class="section row">
       
        <ui:form-row type="text" id="validation_answers" name="validation_answers" Label="Answer" value="">
        <div class="row">
            <a href="##" class='remove btn btn-danger btn-rounded-deep pull-right'><i class="fa fa-trash-o"></i></a>
        </div>
        <hr>
	</div>
   </cfif>
   
       
       
   
   
    </div>
    
    <ui:form-row type="boolean" label="Question Active" id="active" name="active" value="#rc.QuestionInfo.validation_active#">
    
    <div class="row">
	    <input type="submit" value="Save Question" class="btn btn-success pull-right"> 
    </div>
    </form>
    
 </ui:container>
 

 
 
<script>
//define template
var template = $('##sections .section:first').clone();

//define counter
var sectionsCount = 1;

//add new section
$('body').on('click', '.addsection', function() {

    //increment
    sectionsCount++;

    //loop through each input
    var section = template.clone().find(':input').each(function(){

        //set id to store the updated section number
        var newId = this.id + sectionsCount;

        //update for label
        $(this).prev().attr('for', newId);

        //update id
        this.id = newId;

    }).end()

    //inject new section
    .appendTo('##sections');
    return false;
});

//remove section
$('##sections').on('click', '.remove', function() {
    //fade out section
    $(this).parent().fadeOut(300, function(){
        //remove parent element (main section)
        //$(this).parent().parent().empty();
        $(this).parent('.section').remove();
        return false;
    });
    return false;
});
</script>
 </cfoutput>