<cfimport taglib="/lib/tags/" prefix="ui">
<cfoutput>
	
  <div class="content-wrapper">
    <h1 class="page-heading">#rc.currentsite.sitename# User Validiation : Questions</h1>
    
    
    #view("partials/messages")#
    
    <div class="row">
   <div class="col-sm-12">
   <div class="card">
      <div class="card-block">
        <div class="media">
          <div class="media-body media-middle">
            <h4 class="m-b-0">User Validiation Questions</h4>
            <p class="text-muted m-b-0"></p>
          </div>
        </div>    
      </div>
      
      <div class="card-block p-t-0">
      
      <ui:table tableID="uvalidTable" datatable="true" showheader="true" pagelength="50" columnsHidden="0" sortcol="0"
            sortOrder="ASC" columns="Question|Answers|Active" showDelete="true">
        <cfloop array="#rc.SecurityQuestions#" index="uv">
            <cfset AnswersList = ListQualify(#uv.getValidation_Answers()#,"","<br>","CHAR")>
            <ui:table-row id="#uv.getvalidation_ID()#" 
            editColumn="1"
            editLink="#buildURL(action='securityvalidation.maintain', querystring='qId=#uv.Getvalidation_ID()#')#"
            
            deleteLink="#buildURL(action='securityvalidation.delete', querystring='qId=#uv.Getvalidation_ID()#')#" 
            showdelete="true"
            values="#uv.Getvalidation_question()#|#AnswersList#|#yesNoFormat(uv.getValidation_active())#">
            </ui:table-row> 
        </cfloop>
      </ui:table>
      </div>
    </div>
    </div>
    
    </div>
  </div>
</cfoutput>    
