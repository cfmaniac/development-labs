<cfoutput>
<h1 class="page-heading">Reading Message : #rc.Enquiry.getSubject()#</h1>

#view("partials/messages")#

<div class="row">
        <div class="col-xl-12">
          <div class="card">
             

            <div class="card-block">
              <h4 class="card-title col-sm-9">#rc.Enquiry.getSubject()#</h4>
              <h4 class="col-sm-3"> <a href="#buildURL('inbox')#" class="btn btn-primary"> Back to Inbox</a>
				  <a href="mailto:#rc.Enquiry.getEmail()#" class="btn btn-success"> Reply</a>
		          <a href="#buildURL('inbox.delete')#/messageid/#rc.Enquiry.getEnquiryId()#" title="Delete" class="btn btn-danger"> Delete</a>
              </h4>
              <div class="form-group row">	
                  <label class="col-sm-3" for="inputSubject">From</label>
                  <div class="col-sm-9">#rc.Enquiry.getName()# &lt;#rc.Enquiry.getEmail()#&gt;</div>
              </div>
              <div class="form-group row">	
                  <label class="col-sm-3" for="inputSubject">Recieved</label>
                  <div class="col-sm-9">#DateFormat(rc.Enquiry.getCreated(), "full")# at #TimeFormat(rc.Enquiry.getCreated())#</div>
              </div>
              <div class="form-group row">	
                  <label class="col-sm-3" for="inputSubject">Subject</label>
                  <div class="col-sm-9"><strong>#rc.Enquiry.getSubject()#</strong></div>
              </div>
             
              <div class="form-group row">	
                  <label class="col-sm-3" for="inputSubject">Message</label>
                  <div class="col-sm-9"><blockquote>#rc.Enquiry.getDisplayMessage()#</blockquote></div>
              </div>
             
            </div>
          </div>
          
      </div>
	<!---
	<div class="page-header clear">
		<h1>#rc.Enquiry.getName()# <small class="pull-right">#DateFormat(rc.Enquiry.getCreated(), "full")# at #TimeFormat(rc.Enquiry.getCreated())#</small></h1>
	</div>

	<div class="btn-group pull-right append-bottom" data-toggle="buttons-checkbox">
		
	</div>

	<hr class="clear">

	<blockquote>#rc.Enquiry.getDisplayMessage()#</blockquote>--->
	
</cfoutput>
