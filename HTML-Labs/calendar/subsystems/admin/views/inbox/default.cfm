<cfimport taglib="/lib/tags/" prefix="ui">
<cfoutput>
	<h1 class="page-heading">Inbox </h1>

	#view("partials/messages")#
	
	<div class="row">
        <div class="col-xl-12">
          <div class="card">
          <p>These are messages received through your website, not your personal email inbox</p>
          <ui:table tableID="inboxTable" datatable="true" showheader="true" pagelength="50" columnsHidden="0" sortcol="3"
            sortOrder="DESC" columns="From|Subject|Date Received" showDelete="true">
        <cfloop array="#rc.enquiries#" index="local.Enquiry">
            <ui:table-row id="#local.Enquiry.getEnquiryId()#" 
            editColumn="1"
            editLink="#buildURL(action = 'inbox.enquiry', queryString = 'messageID=#local.Enquiry.getEnquiryId()#')#"
            
            deleteLink="#buildURL('inbox.delete')#/messageID/#local.Enquiry.getEnquiryId()#'" 
            showdelete="true"
            values="#local.Enquiry.getName()#|#local.enquiry.getSubject()#|#DateFormat(local.Enquiry.getCreated(), "full")# at #TimeFormat(local.Enquiry.getCreated())#">
            </ui:table-row> 
        </cfloop>
      </ui:table>
          <!---<table id="datatable-example" class="table table-striped table-hover table-sm">
          <thead>
            <tr>
              <th>From</th>
              <th>Subject</th>
              <th>Date Recieved</th>
              <th></th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
          <cfif ArrayLen(rc.enquiries)>
          <cfloop array="#rc.enquiries#" index="local.Enquiry">
					<tr <cfif !local.Enquiry.isRead()>style="font-weight:bold;"</cfif>>
						<td><a href="#buildURL(action = 'inbox.enquiry', queryString = 'messageID=#local.Enquiry.getEnquiryId()#')#" title="View">#local.Enquiry.getName()#</a></td>
						<td class="center">#local.enquiry.getSubject()#</td>
						<td>#DateFormat(local.Enquiry.getCreated(), "full")# at #TimeFormat(local.Enquiry.getCreated())#</td>
						<td class="center">&nbsp;</td>
						<td class="center"><a href="#buildURL('enquiries.delete')#/messageID/#local.Enquiry.getEnquiryId()#" title="Delete"><i class="glyphicon glyphicon-trash"></i></a></td>
					</tr>
				</cfloop>
          </cfif>
          </tbody>
        </table>--->
          </div>
	<!---<div class="page-header clear">
		<h1>Enquiries</h1>
	</div>

	<cfif rc.unreadenquirycount>
		<p><a href="#buildURL('enquiries.markread')#" class="btn btn-primary">Mark All Read</a></p>
	</cfif>

	#view("partials/messages")#

	<cfif ArrayLen(rc.enquiries)>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Name</th>
					<th>Received</th>
					<th class="center">View</th>
					<th class="center">Delete</th>
				</tr>
			</thead>

			<tbody>
				<cfloop array="#rc.enquiries#" index="local.Enquiry">
					<tr <cfif !local.Enquiry.isRead()>style="font-weight:bold;"</cfif>>
						<td><a href="#buildURL(action = 'inbox.enquiry', queryString = 'messageID=#local.Enquiry.getEnquiryId()#')#" title="View">#local.Enquiry.getName()#</a></td>
						<td>#DateFormat(local.Enquiry.getCreated(), "full")# at #TimeFormat(local.Enquiry.getCreated())#</td>
						<td class="center"><a href="#buildURL(action = 'inbox.enquiry', queryString = 'messageID=#local.Enquiry.getEnquiryId()#')#" title="View"><i class="glyphicon glyphicon-eye-open"></i></a></td>
						<td class="center"><a href="#buildURL('enquiries.delete')#/messageID/#local.Enquiry.getEnquiryId()#" title="Delete"><i class="glyphicon glyphicon-trash"></i></a></td>
					</tr>
				</cfloop>
			</tbody>
		</table>

		<p><span class="label label-info">Note</span> Unread enquiries are highlighted in bold.</p>
	<cfelse>
		<p>There are no enquiries at this time.</p>
	</cfif>--->
</cfoutput>
