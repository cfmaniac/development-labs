component accessors = true extends = "abstract" {
	
	void function about(required struct rc) {
		rc.AboutReplicator = '<p> Replicator X (v.#rc.config.version#) is the Fifth Generation of the Replicator Multi-Website Framework.</p>
		<p>This Generation includes major system enhancements compared to the fourth generation.</p>
		<p>Having been built over the period of Twelve(12)-Years Replicator has undergone major enhancements and upgrades as the System Evolves and Matures.
		<h4>What''s New</h4>
		<div class="row">
		<div class="col-md-6">
		<h5 class="label label-info"><strong>New Plugins Systems</strong></h5>
		<p>Replicator X has a new Plugins System, new to the Framework, that extends Core Functionality so that the Core Fucntionality doesn''t get
		bloated and filled with functions that don''t neccessarily need to get used.
		<p>Using the plugins system is really Easy. Just include:<br>
		<code>getPlugin(''pluginName'');
		</code><br>
		In your Controller.
		</div>
		
		<div class="col-md-6">
		<h5 class="label label-info"><strong>New Database Schema</strong></h5>
		<p>The Database Schema has been updated to follow a nicer, more organized structure.</p>
		<p>The Modules Database has been retructured, as has the Module Settings Database, for use and isolation for individual websites
		handled through Replicator X.</p>
		<p>The Replicator X System now handles both ColdFusion ORM and DataManager Databasing Systems.</p>
		</div>
		
		<div class="col-md-6">
		<h5 class="label label-info"><strong>Material & Font-Awesome Icons</strong></h5>
		<p>Replicator X is now using both Matrial & Font-Awesome Icons</p>
		</div>
		
		<div class="col-md-6">
		<h5 class="label label-info"><strong>Session Timeout Warning</strong></h5>
		<p>Replicator X now has a Control Panel Session timeout warning, giving you a window of time to rejvenate your control panel session.</p>
		</div>
		
		<div class="col-md-6">
		<h5 class="label label-info"><strong>Consolidated System & Module Settings</strong></h5>
		<p>Replicator X has consolidated the Module and Site Settings Screens into a single dashboard. Only Active Modules that have configuration
		options are shown in the Settings Dashboard.</p>
		<p><a href="#variables.fw.buildURL('settings')#" class="btn btn-info pull-right">View Settings Dashboard</a>
		</div>
		
		<div class="col-md-6">
		<h5 class="label label-info"><strong>New Login Options</strong></h5>
		<p>A Major update is the login options for the Front and Back-Ends of Replicator. You can now login via a Username or Email Address.</p>
		
		</div>
		
		<div class="col-md-6">
		<h5 class="label label-info"><strong>Updates to the UI Tags</strong></h5>
		<p>Updates to the Custom Replicator UI Tags have been implemented.</p>
		<p>The <strong>Select Custom Tag</strong> now supports both options and Custom Labels.</p>
		<p>To use the options and custom option labels, it does require a little bit of work:</p>
		<code>
		//Select List with Options from Query:<br>
		&lt;cfset rc.Options = ##ValueList(rc.SiteTemplates.Name,",")##&gt;<br>
		&lt;ui:input label="Site Layout" type="select" options="##rc.Options##" name="defaultLayout" value="##setting.getdefaultLayout()##">
		</code>
		<hr>
		<code>
		//Select List with Options (from Query) & Custom Labels (from Query):<br>
		&lt;cfset rc.HomePages = ValueList(rc.RootPages.Slug,",")&gt;<br>
		&lt;cfset rc.HomeLabels = ValueList(rc.RootPages.Title,",")&gt;<br>
	    &lt;ui:input label="Site Home Page" type="select" optionlabels="##rc.HomeLabels##" options="##rc.HomePages##" name="homepage" value="##setting.getHomePage()##"&gt;
	    </code>     
		</div>
		
		<div class="col-md-6">
		<h5 class="label label-info"><strong>New Form Builder Module</strong></h5>
		<p>We''ve added a new Module: <strong>Form Builder</strong>.</p>
		<p>The Form Builder allows you to build forms, manage submissions and if the form is to be emailed or not.</p>
		<p><a href="#variables.fw.buildURL('forms')#" class="btn btn-info pull-right">View Forms Module</a>
		
		</div>
		
		<div class="col-md-6">
		<h5 class="label label-info"><stronng>Control Panel Updates</strong></h5>
		
		
		</div>
		
		</div>';
		
	}
}
