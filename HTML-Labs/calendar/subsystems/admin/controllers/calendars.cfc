component accessors = true extends = "abstract" {
	void function before(required struct rc){
		//Set Up Event Colors:
		rc.arrColors = ArrayNew( 1 );
		ArrayAppend( rc.arrColors, "000033" );
	ArrayAppend( rc.arrColors, "000066" );
	ArrayAppend( rc.arrColors, "000099" );
	ArrayAppend( rc.arrColors, "0000CC" );
	ArrayAppend( rc.arrColors, "0000FF" );
	ArrayAppend( rc.arrColors, "003300" );
	ArrayAppend( rc.arrColors, "003333" );
	ArrayAppend( rc.arrColors, "003366" );
	ArrayAppend( rc.arrColors, "003399" );
	ArrayAppend( rc.arrColors, "0033CC" );
	ArrayAppend( rc.arrColors, "0033FF" );
	ArrayAppend( rc.arrColors, "006600" );
	ArrayAppend( rc.arrColors, "006633" );
	ArrayAppend( rc.arrColors, "006666" );
	ArrayAppend( rc.arrColors, "006699" );
	ArrayAppend( rc.arrColors, "0066CC" );
	ArrayAppend( rc.arrColors, "0066FF" );
	ArrayAppend( rc.arrColors, "009900" );
	ArrayAppend( rc.arrColors, "009933" );
	ArrayAppend( rc.arrColors, "009966" );
	ArrayAppend( rc.arrColors, "009999" );
	ArrayAppend( rc.arrColors, "0099CC" );
	ArrayAppend( rc.arrColors, "0099FF" );
	ArrayAppend( rc.arrColors, "00CC00" );
	ArrayAppend( rc.arrColors, "00CC33" );
	ArrayAppend( rc.arrColors, "00CC66" );
	ArrayAppend( rc.arrColors, "00CC99" );
	ArrayAppend( rc.arrColors, "00CCCC" );
	ArrayAppend( rc.arrColors, "00CCFF" );
	ArrayAppend( rc.arrColors, "00FF00" );
	ArrayAppend( rc.arrColors, "00FF33" );
	ArrayAppend( rc.arrColors, "00FF66" );
	ArrayAppend( rc.arrColors, "00FF99" );
	ArrayAppend( rc.arrColors, "00FFCC" );
	ArrayAppend( rc.arrColors, "00FFFF" );
	ArrayAppend( rc.arrColors, "330000" );
	ArrayAppend( rc.arrColors, "330033" );
	ArrayAppend( rc.arrColors, "330066" );
	ArrayAppend( rc.arrColors, "330099" );
	ArrayAppend( rc.arrColors, "3300CC" );
	ArrayAppend( rc.arrColors, "3300FF" );
	ArrayAppend( rc.arrColors, "333300" );
	ArrayAppend( rc.arrColors, "333333" );
	ArrayAppend( rc.arrColors, "333366" );
	ArrayAppend( rc.arrColors, "333399" );
	ArrayAppend( rc.arrColors, "3333CC" );
	ArrayAppend( rc.arrColors, "3333FF" );
	ArrayAppend( rc.arrColors, "336600" );
	ArrayAppend( rc.arrColors, "336633" );
	ArrayAppend( rc.arrColors, "336666" );
	ArrayAppend( rc.arrColors, "336699" );
	ArrayAppend( rc.arrColors, "3366CC" );
	ArrayAppend( rc.arrColors, "3366FF" );
	ArrayAppend( rc.arrColors, "339900" );
	ArrayAppend( rc.arrColors, "339933" );
	ArrayAppend( rc.arrColors, "339966" );
	ArrayAppend( rc.arrColors, "339999" );
	ArrayAppend( rc.arrColors, "3399CC" );
	ArrayAppend( rc.arrColors, "3399FF" );
	ArrayAppend( rc.arrColors, "33CC00" );
	ArrayAppend( rc.arrColors, "33CC33" );
	ArrayAppend( rc.arrColors, "33CC66" );
	ArrayAppend( rc.arrColors, "33CC99" );
	ArrayAppend( rc.arrColors, "33CCCC" );
	ArrayAppend( rc.arrColors, "33CCFF" );
	ArrayAppend( rc.arrColors, "33FF00" );
	ArrayAppend( rc.arrColors, "33FF33" );
	ArrayAppend( rc.arrColors, "33FF66" );
	ArrayAppend( rc.arrColors, "33FF99" );
	ArrayAppend( rc.arrColors, "33FFCC" );
	ArrayAppend( rc.arrColors, "33FFFF" );
	ArrayAppend( rc.arrColors, "660000" );
	ArrayAppend( rc.arrColors, "660033" );
	ArrayAppend( rc.arrColors, "660066" );
	ArrayAppend( rc.arrColors, "660099" );
	ArrayAppend( rc.arrColors, "6600CC" );
	ArrayAppend( rc.arrColors, "6600FF" );
	ArrayAppend( rc.arrColors, "663300" );
	ArrayAppend( rc.arrColors, "663333" );
	ArrayAppend( rc.arrColors, "663366" );
	ArrayAppend( rc.arrColors, "663399" );
	ArrayAppend( rc.arrColors, "6633CC" );
	ArrayAppend( rc.arrColors, "6633FF" );
	ArrayAppend( rc.arrColors, "666600" );
	ArrayAppend( rc.arrColors, "666633" );
	ArrayAppend( rc.arrColors, "666666" );
	ArrayAppend( rc.arrColors, "666699" );
	ArrayAppend( rc.arrColors, "6666CC" );
	ArrayAppend( rc.arrColors, "6666FF" );
	ArrayAppend( rc.arrColors, "669900" );
	ArrayAppend( rc.arrColors, "669933" );
	ArrayAppend( rc.arrColors, "669966" );
	ArrayAppend( rc.arrColors, "669999" );
	ArrayAppend( rc.arrColors, "6699CC" );
	ArrayAppend( rc.arrColors, "6699FF" );
	ArrayAppend( rc.arrColors, "66CC00" );
	ArrayAppend( rc.arrColors, "66CC33" );
	ArrayAppend( rc.arrColors, "66CC66" );
	ArrayAppend( rc.arrColors, "66CC99" );
	ArrayAppend( rc.arrColors, "66CCCC" );
	ArrayAppend( rc.arrColors, "66CCFF" );
	ArrayAppend( rc.arrColors, "66FF00" );
	ArrayAppend( rc.arrColors, "66FF33" );
	ArrayAppend( rc.arrColors, "66FF66" );
	ArrayAppend( rc.arrColors, "66FF99" );
	ArrayAppend( rc.arrColors, "66FFCC" );
	ArrayAppend( rc.arrColors, "66FFFF" );
	ArrayAppend( rc.arrColors, "990000" );
	ArrayAppend( rc.arrColors, "990033" );
	ArrayAppend( rc.arrColors, "990066" );
	ArrayAppend( rc.arrColors, "990099" );
	ArrayAppend( rc.arrColors, "9900CC" );
	ArrayAppend( rc.arrColors, "9900FF" );
	ArrayAppend( rc.arrColors, "993300" );
	ArrayAppend( rc.arrColors, "993333" );
	ArrayAppend( rc.arrColors, "993366" );
	ArrayAppend( rc.arrColors, "993399" );
	ArrayAppend( rc.arrColors, "9933CC" );
	ArrayAppend( rc.arrColors, "9933FF" );
	ArrayAppend( rc.arrColors, "996600" );
	ArrayAppend( rc.arrColors, "996633" );
	ArrayAppend( rc.arrColors, "996666" );
	ArrayAppend( rc.arrColors, "996699" );
	ArrayAppend( rc.arrColors, "9966CC" );
	ArrayAppend( rc.arrColors, "9966FF" );
	ArrayAppend( rc.arrColors, "999900" );
	ArrayAppend( rc.arrColors, "999933" );
	ArrayAppend( rc.arrColors, "999966" );
	ArrayAppend( rc.arrColors, "999999" );
	ArrayAppend( rc.arrColors, "9999CC" );
	ArrayAppend( rc.arrColors, "9999FF" );
	ArrayAppend( rc.arrColors, "99CC00" );
	ArrayAppend( rc.arrColors, "99CC33" );
	ArrayAppend( rc.arrColors, "99CC66" );
	ArrayAppend( rc.arrColors, "99CC99" );
	ArrayAppend( rc.arrColors, "99CCCC" );
	ArrayAppend( rc.arrColors, "99CCFF" );
	ArrayAppend( rc.arrColors, "99FF00" );
	ArrayAppend( rc.arrColors, "99FF33" );
	ArrayAppend( rc.arrColors, "99FF66" );
	ArrayAppend( rc.arrColors, "99FF99" );
	ArrayAppend( rc.arrColors, "99FFCC" );
	ArrayAppend( rc.arrColors, "99FFFF" );
	ArrayAppend( rc.arrColors, "CC0000" );
	ArrayAppend( rc.arrColors, "CC0033" );
	ArrayAppend( rc.arrColors, "CC0066" );
	ArrayAppend( rc.arrColors, "CC0099" );
	ArrayAppend( rc.arrColors, "CC00CC" );
	ArrayAppend( rc.arrColors, "CC00FF" );
	ArrayAppend( rc.arrColors, "CC3300" );
	ArrayAppend( rc.arrColors, "CC3333" );
	ArrayAppend( rc.arrColors, "CC3366" );
	ArrayAppend( rc.arrColors, "CC3399" );
	ArrayAppend( rc.arrColors, "CC33CC" );
	ArrayAppend( rc.arrColors, "CC33FF" );
	ArrayAppend( rc.arrColors, "CC6600" );
	ArrayAppend( rc.arrColors, "CC6633" );
	ArrayAppend( rc.arrColors, "CC6666" );
	ArrayAppend( rc.arrColors, "CC6699" );
	ArrayAppend( rc.arrColors, "CC66CC" );
	ArrayAppend( rc.arrColors, "CC66FF" );
	ArrayAppend( rc.arrColors, "CC9900" );
	ArrayAppend( rc.arrColors, "CC9933" );
	ArrayAppend( rc.arrColors, "CC9966" );
	ArrayAppend( rc.arrColors, "CC9999" );
	ArrayAppend( rc.arrColors, "CC99CC" );
	ArrayAppend( rc.arrColors, "CC99FF" );
	ArrayAppend( rc.arrColors, "CCCC00" );
	ArrayAppend( rc.arrColors, "CCCC33" );
	ArrayAppend( rc.arrColors, "CCCC66" );
	ArrayAppend( rc.arrColors, "CCCC99" );
	ArrayAppend( rc.arrColors, "CCCCCC" );
	ArrayAppend( rc.arrColors, "CCCCFF" );
	ArrayAppend( rc.arrColors, "CCFF00" );
	ArrayAppend( rc.arrColors, "CCFF33" );
	ArrayAppend( rc.arrColors, "CCFF66" );
	ArrayAppend( rc.arrColors, "CCFF99" );
	ArrayAppend( rc.arrColors, "CCFFCC" );
	ArrayAppend( rc.arrColors, "CCFFFF" );
	ArrayAppend( rc.arrColors, "FF0000" );
	ArrayAppend( rc.arrColors, "FF0033" );
	ArrayAppend( rc.arrColors, "FF0066" );
	ArrayAppend( rc.arrColors, "FF0099" );
	ArrayAppend( rc.arrColors, "FF00CC" );
	ArrayAppend( rc.arrColors, "FF00FF" );
	ArrayAppend( rc.arrColors, "FF3300" );
	ArrayAppend( rc.arrColors, "FF3333" );
	ArrayAppend( rc.arrColors, "FF3366" );
	ArrayAppend( rc.arrColors, "FF3399" );
	ArrayAppend( rc.arrColors, "FF33CC" );
	ArrayAppend( rc.arrColors, "FF33FF" );
	ArrayAppend( rc.arrColors, "FF6600" );
	ArrayAppend( rc.arrColors, "FF6633" );
	ArrayAppend( rc.arrColors, "FF6666" );
	ArrayAppend( rc.arrColors, "FF6699" );
	ArrayAppend( rc.arrColors, "FF66CC" );
	ArrayAppend( rc.arrColors, "FF66FF" );
	ArrayAppend( rc.arrColors, "FF9900" );
	ArrayAppend( rc.arrColors, "FF9933" );
	ArrayAppend( rc.arrColors, "FF9966" );
	ArrayAppend( rc.arrColors, "FF9999" );
	ArrayAppend( rc.arrColors, "FF99CC" );
	ArrayAppend( rc.arrColors, "FF99FF" );
	ArrayAppend( rc.arrColors, "FFCC00" );
	ArrayAppend( rc.arrColors, "FFCC33" );
	ArrayAppend( rc.arrColors, "FFCC66" );
	ArrayAppend( rc.arrColors, "FFCC99" );
	ArrayAppend( rc.arrColors, "FFCCCC" );
	ArrayAppend( rc.arrColors, "FFCCFF" );
	ArrayAppend( rc.arrColors, "FFFF00" );
	ArrayAppend( rc.arrColors, "FFFF33" );
	ArrayAppend( rc.arrColors, "FFFF66" );
	ArrayAppend( rc.arrColors, "FFFF99" );
	ArrayAppend( rc.arrColors, "FFFFCC" );

		//Setup Event Repeat Types:
		rc.RepeatTypes = QueryNew( "id, name" );
		queryAddRow(rc.RepeatTypes,
		[
		{id=1,name="Daily"},
		{id=2,name="Weekly"},
		{id=3,name="Bi-Weekly"},
		{id=4,name="Monthly"},
		{id=5,name="Yearly"},
		{id=6,name="Mon - Fri"},
		{id=7,name="Sat - Sun"}
		]);
	}
	
	void function default(required struct rc) {
		rc.Events = variables.EventsService.getEvents(siteid=rc.currentSite.siteID);

	}
	
	void function maintain(required struct rc) {
		param name = "rc.Id" default = 0;
		param name = "rc.context" default = "create";
		param name = "rc.pageTitle" default = "Add Events";
		
		if(structKeyExists(rc, "id") && rc.id GT "0"){
			rc.context = "update";
			rc.event = entityloadbypk('Events', rc.id);
			rc.pageTitle = "Edit Event";
		} else {
			rc.context = "create";
			rc.event = entityNew('Events');
			rc.pageTitle = "Add Event";
			
		}
		
		
	}
	
	void function save(required struct rc) {
		//writeDump(#rc.allDayEvent#);
		//abort;
		if( rc.context is "create" ) {
			rc.Event = entityNew( 'Events' );
			rc.Event.setTitle( '#rc.title#' );
			rc.Event.setBody( '#rc.body#' );
			rc.Event.setAllDayEvent('#rc.allDayEvent#');
			rc.Event.setStartAt( '#rc.startAt#' );
			rc.Event.setStartTime('#rc.startTime#');
			rc.Event.setEndAt( '#rc.endsAt#' );
			rc.Event.setEndTime('#rc.EndTime#');
			rc.Event.setSite_ID('#rc.currentSite.siteID#');
		}
		else {
			rc.Event = entityLoadByPK( 'Events', rc.id );
			rc.Event.setTitle( '#rc.title#' );
			rc.Event.setBody( '#rc.body#' );
			rc.Event.setAllDayEvent('#rc.allDayEvent#');
			rc.Event.setStartAt( '#rc.startAt#' );
			rc.Event.setStartTime('#rc.startTime#');
			rc.Event.setEndAt( '#rc.endsAt#' );
			rc.Event.setEndTime('#rc.EndTime#');
			rc.Event.setSite_ID('#rc.currentSite.siteID#');
		}

		transaction{
			entitySave( rc.Event );
			ORMFLUSH( );
		}

		variables.fw.redirect(action='calendars.default', queryString='message=evSV');

	}
		
	
	void function delete(required struct rc){
		 rc.Event = entityLoadByPK( 'Events', rc.id );
		 
		 transaction{
			 entityDelete(rc.Event);
			 ORMFLUSH();
		 }
		 
		 variables.fw.redirect("calendars");
	}
}

