<cfComponent output	= "no">

	<cfFunction	name= "getDirections" access= "public" returntype= "string" output="no">
		<cfArgument name="form" type="struct" required="yes" />
		<cfScript>
			// default the URL
			this.theGoogleMapsDirectionsURL	=	'';
			// trim, strip and replace destination address
			this.userAddress = '';
			this.userAddress = this.userAddress & trim(form.street) & ',+' & trim(form.city) & ',+' & trim(form.zipcode) ;
			this.userAddress = replaceNoCase(this.userAddress, " ", "+", "ALL");
			this.theGoogleMapsDirectionsURL	= form.googleURLstart & this.userAddress & '&geocode=&dirflg=&saddr=' & form.bizAddressForGURL & form.googleURLEnd;
			// 1220+NW+84th+Dr,+Pompano+Beach,+FL+33071&geocode=&dirflg=&saddr=101+NE+3rd+Ave,+Fort+Lauderdale,+FL+33301&
			return this.theGoogleMapsDirectionsURL;
		</cfScript>
	</cfFunction>

	<cfFunction name = "getCoords" access= "public" output= "no" returntype	= "any">
		<cfArgument name="Address1"		type="string"	required="yes" />
		<cfArgument name="City"			type="string"	required="yes" />
		<cfArgument name="State"		type="string"	required="yes" />
		<cfArgument name="Zipcode"		type="string"	required="yes" />
		
		<cfScript>
			var LOCAL = StructNew();
			LOCAL.Error = "";
			LOCAL.Longitude = 0;
			LOCAL.Latitude = 0;
			LOCAL.Altitude = 0;

			LOCAL.Address = REReplace(arguments.Address1, '[^0-9a-zA-Z_ ]', '', 'ALL');
			qmap = LOCAL.Address & ' #arguments.City#, #arguments.State# #arguments.Zipcode#, US';
		</cfScript>
		
		<!--- use cfhttp to get the info from google maps --->
		<cfhttp	url="http://maps.googleapis.com/maps/api/geocode/xml?address=#qmap#&sensor=false" resolveurl="no"></cfhttp>
		
		<cftry>
			<cfset LOCAL.xmlDoc = XMLParse(CFHTTP.FileContent)>
			<cfcatch>
				<cfset LOCAL.Error = CFHTTP.FileContent>
				<cfset LOCAL.ResponseCode = 400>
				<cfreturn LOCAL>
			</cfcatch>
		</cftry>
		<cfScript>
			LOCAL.ResponseCode = LOCAL.xmlDoc.GeocodeResponse.status.XMLText;

			if( LOCAL.ResponseCode is "OK" )
			{
				LOCAL.Coords = LOCAL.xmlDoc.GeocodeResponse.result.geometry.location.lng.XMLText & ',' & LOCAL.xmlDoc.GeocodeResponse.result.geometry.location.lat.XMLText & ',0';
				LOCAL.Longitude = LOCAL.xmlDoc.GeocodeResponse.result.geometry.location.lng.XMLText;
				LOCAL.Latitude = LOCAL.xmlDoc.GeocodeResponse.result.geometry.location.lat.XMLText;
			}else
			{
				LOCAL.Error = "A geolocator rc.could not be successfully parsed.";
			}
			return LOCAL;
		</cfScript>
	</cfFunction>

</cfComponent>