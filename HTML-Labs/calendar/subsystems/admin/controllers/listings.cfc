component accessors = true extends = "abstract" {
  property name = "CompanyService" setter = true getter = false;
  void function default( required struct rc ){
   
   
      rc.ActiveCompanies = new Query(sql="Select Distinct(Companies.companyID), *  from Companies 
      INNER JOIN Companies_Address on companies.companyID = companies_address.companyID
	  where companies.active=1 
	  order by Companies.Name", name="ActiveCompanies", cachedwithin='#createTimespan(0,0,0,-1)#').execute().getResult();
     
   /*} else{
     rc.ActiveCompanies = new Query(sql="Select * from Companies 
                                           INNER JOIN Companies_Address on companies.companyID = companies_address.companyID
                                           where companies.active=1 order by Name", name="ActiveCompanies", cachedwithin='#createTimespan(0,0,0,-1)#').execute().getResult();
    
   }*/
   rc.ActiveBuinessesCount = variables.CompanyService.getActiveCount();
   
  
  }
  
  void function maintain( required struct rc ){
   if(isdefined('rc.id')){
    rc.CompanyInfo = new Query(sql ="select * from Companies where CompanyID=#rc.id#").execute().getResult();
    rc.CompanyAddress = new Query(sql ="select * from Companies_address where CompanyID=#rc.id#").execute().getResult();
    rc.CompanyListings = new Query(sql ="select * from Companies_listings where CompanyID=#rc.id#").execute().getResult();
    
    rc.CompanyBilling = new Query(sql ="select * from Companies_Billing where CompanyID=#rc.id#").execute().getResult();
    
    rc.CompanySettings = new Query(sql ="select * from Companies_settings where CompanyID=#rc.id#").execute().getResult();
    rc.CompanyVerticals = new Query(sql ="select * from Companies_verticals where CompanyID=#rc.id#").execute().getResult();
    //rc.CompanyUsers = new Query(sql ="select * from users where CompanyID=#rc.id#").execute().getResult();
    
    rc.CompanyMarkets = ValueList(rc.CompanyVerticals.verticalID, ",");
    rc.subSites = entityload('Sites', {parentsiteID='#rc.CurrentSite.siteID#'});
    }
  }
  
  void function availability( required struct rc ){
        rc.regions = variables.fw.getPlugin('regionHelper');
        verticalSQL = 'Select * from sites where isSubsite=1 and parentSiteID = #session.siteID#';
        rc.VerticalSites = new Query();
        rc.Verticals = rc.VerticalSites.execute(sql=verticalSQL).getResult();
    
  }
  
  remote function SaveUser(){
      WriteDump(var=#form#);
   
      return this;
  }
  
  void function inactive( required struct rc ){
   
   //rc.InActiveCompanies = variables.CompanyService.getInActiveCompanies();
   if(session.vSiteID NEQ "0"){
      rc.InActiveCompanies = new Query(sql="Select * from Companies 
      INNER JOIN Companies_Address on companies.companyID = companies_address.companyID where companies.active=0 order by Name", name="InActiveCompanies", cachedwithin='#createTimespan(0,0,0,-1)#').execute().getResult();
   
   } else {
      rc.InActiveCompanies = new Query(sql="Select * from Companies
      INNER JOIN Companies_Address on companies.companyID = companies_address.companyID
      INNER JOIN Companies_verticals on companies.companyID = companies_verticals.companyID  where companies.active=0 order by Name
      ", name="InActiveCompanies", cachedwithin='#createTimespan(0,0,0,-1)#').execute().getResult();
   
   }
   rc.InActiveBuinessesCount = variables.CompanyService.getInActiveCount();
  }
  
  void function exclusive( required struct rc ){
 
  }

}
