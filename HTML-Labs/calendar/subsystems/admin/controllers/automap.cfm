
	   
	    
		<cfscript>
		rc.SitesSettings = variables.siteservice.getSettingsbySite(siteID=#rc.currentSite.siteID#);
	    
		rc.sesomitindex = variables.fw.getConfig().sesomitindex;
		rc.navigation = variables.ContentService.getNavigation(siteid=rc.CurrentSite.siteID);
		rc.Posts = variables.BlogService.getPosts(siteID=rc.CurrentSite.siteID);
		rc.articles = variables.NewsService.getArticles(siteID=rc.CurrentSite.siteID);
		</cfscript>   
		
		<cfsavecontent variable="local.xml"><cfoutput><?xml version="1.0" encoding="UTF-8"?>
		<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
			<!--- add pages to sitemap --->
			<cfloop query="rc.navigation">
				<cfif( rc.sesomitindex )>
					<url><loc>http://#rc.siteSettings.getdomain()#/<cfif rc.modules.page.defaultslug neq slug>#slug#</cfif></loc><lastmod>#dateformat(now(), 'YYYY-MM-DD')#</lastmod><changefreq>daily</changefreq><priority>1</priority></url>
				<cfelse>
					<url><loc>http://#rc.siteSettings.getdomain()#/<cfif rc.modules.page.defaultslug neq slug>index.cfm/#slug#</cfif></loc><lastmod>#dateformat(now(), 'YYYY-MM-DD')#</lastmod><changefreq>daily</changefreq><priority><cfif rc.modules.page.defaultslug neq slug>2<cfelse>1</cfif></priority></url>
				</cfif>
			</cfloop> 
			<!--- add blog Posts to sitemap --->
			
				<cfloop array="#rc.posts#" index="local.post">
					<cfif( rc.sesomitindex )>
						<url><loc>http://#rc.siteSettings.getdomain()#/blog/post/slug/#local.post.getSlug()#</loc><lastmod>#dateformat(now(), 'YYYY-MM-DD')#</lastmod><changefreq>daily</changefreq><priority>3</priority></url>
					<cfelse>
						<url><loc>http://#rc.siteSettings.getdomain()#/index.cfm/blog/post/slug/#local.post.getSlug()#</loc><lastmod>#dateformat(now(), 'YYYY-MM-DD')#</lastmod><changefreq>daily</changefreq><priority>3</priority></url>
					</cfif>
				</cfloop>
				
			
			<!--- add articles to sitemap --->
			
				<cfloop array="#rc.articles#" index="local.Article">
					<cfif( rc.sesomitindex )>
						<url><loc>http://#rc.siteSettings.getdomain()#/news/article/slug/#local.Article.getSlug()#</loc><lastmod>#dateformat(now(), 'YYYY-MM-DD')#</lastmod><changefreq>daily</changefreq><priority>4</priority></url>
					<cfelse>
						<url><loc>http://#rc.siteSettings.getdomain()#/index.cfm/news/article/slug/#local.Article.getSlug()#</loc><lastmod>#dateformat(now(), 'YYYY-MM-DD')#</lastmod><changefreq>daily</changefreq><priority>4</priority></url>
					</cfif>
				</cfloop>
			<url>
                <loc>http://#rc.siteSettings.getdomain()#/</loc>
            </url>
		</urlset>
	</cfoutput>
</cfsavecontent>
	<cfset siteName = "#rereplace(rc.siteSettings.getTitle(), "&trade;", "","ALL")#">
	<cfset siteName = "#rereplace(siteName, " ", "-","ALL")#">
	
<cffile action="write" file="#ExpandPath( "./" )#sitemap_#sitename#.xml" output="#local.xml#">
