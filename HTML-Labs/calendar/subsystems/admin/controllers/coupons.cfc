component accessors="true" extends="abstract"{
  
  void function default( required struct rc ){
   
   
     rc.ActiveCompanies = new Query(sql="Select * from Companies 
                                           LEFT OUTER JOIN Companies_Address on companies.companyID = companies_address.companyID
                                           where companies.active=1 and site_ID=#rc.currentsite# order by Name", name="ActiveCompanies", cachedwithin='#createTimespan(0,0,0,-1)#').execute().getResult();
     
     rc.ActiveListings = new Query(sql ="select * from Companies_listings where listingActive=1 and site_id=#rc.currentSite#").execute().getResult();
     
     rc.InActiveListings = new Query(sql ="select * from Companies_listings where listingActive=0 and site_id=#rc.currentSite#").execute().getResult();
   
     rc.ActiveBuinessesCount = variables.CompanyService.getActiveCount();
   
  
  }
  
  void function merchants( required struct rc ){
   
     
     rc.ActiveCompanies = new Query(sql="Select * from Companies 
                                           LEFT OUTER JOIN Companies_Address on companies.companyID = companies_address.companyID
                                           where companies.active=1 and site_ID=#rc.currentsite# order by Name", name="ActiveCompanies", cachedwithin='#createTimespan(0,0,0,-1)#').execute().getResult();
     
     
   rc.ActiveBuinessesCount = variables.CompanyService.getActiveCount();
   
  
  }
  
  void function listings( required struct rc ){
        rc.lsitings = new Query(sql="Select * from companies_listings CL 
                                     INNER JOIN companies cp on cp.companyID = cl.companyID
                                     where cp.site_id=#rc.currentsite# ORDER by cp.Name").execute().getResult();   
  }
  
  void function listingmaintain( required struct rc ){
        rc.listingsFolder = expandPath( "/contents/listings/" );
        rc.Merchants = new Query(sql="Select * from Companies where active=1 and site_ID=#rc.currentsite# order by Name").execute().getResult();
        if(isdefined('rc.id')){
		rc.context = 'update';
		rc.CompanyListings = new Query(sql ="select * from Companies_listings where listingID=#rc.id#").execute().getResult();
        rc.listingName = '#rc.CompanyListings.ListingName#';
        } else {
		rc.context = 'new';
		rc.CompanyListings = {
			listingid = '',
			companyid = '',
			listingname = '',
			listingImage = '',
			listingdescription = '',
			listingcode = '',
			listingStarted = '#dateFormat(now(), "MM/DD/YYYY")#',
			listingEnds = '#dateFormat(now()+30, "MM/DD/YYYY")#',
		    listingactive = '1'
		};
		rc.listingName='New Listing';
	
        }
  }
  
 
  
  void function DeleteProduct( required struct rc ){
        rc.delProd = new Query(sql="Delete from Companies_listings where listingID=#rc.id#").execute().getResult();
        
        variables.fw.redirect("stores.listings/message/500");   
  }
  
  
  void function settings( required struct rc ){
        rc.StoreSettings = new Query(sql="Select DISTINCT(Settings_Category) from settings_stores 
                                           where site_ID=#rc.currentsite# ORDER BY Settings_Category", name="ActiveCompanies", cachedwithin='#createTimespan(0,0,0,-1)#').execute().getResult();
     
   
   
  }
  
  void function maintain( required struct rc ){
   if(isdefined('rc.id')){
    rc.context = "update";
    rc.CompanyInfo = new Query(sql ="select * from Companies where CompanyID=#rc.id#").execute().getResult();
    rc.CompanyAddress = new Query(sql ="select * from Companies_address where CompanyID=#rc.id#").execute().getResult();
    rc.CompanyListings = new Query(sql ="select * from Companies_listings where CompanyID=#rc.id#").execute().getResult();
    
    rc.CompanyBilling = new Query(sql ="select * from Companies_Billing where CompanyID=#rc.id#").execute().getResult();
    
    rc.CompanySettings = new Query(sql ="select * from Companies_settings where CompanyID=#rc.id#").execute().getResult();
    rc.CompanyVerticals = new Query(sql ="select * from Companies_verticals where CompanyID=#rc.id#").execute().getResult();
    rc.CompanyUsers = new Query(sql ="select * from users where CompanyID=#rc.id#").execute().getResult();
    
    rc.CompanyMarkets = ValueList(rc.CompanyVerticals.verticalID, ",");
    } else {
    rc.context = "create";
    
    
    }
  }
  
  remote function SaveUser(){
      WriteDump(var=#form#);
   
      return this;
  }
  
  void function inactive( required struct rc ){
   
   //rc.InActiveCompanies = variables.CompanyService.getInActiveCompanies();
   if(session.vSiteID NEQ "0"){
      rc.InActiveCompanies = new Query(sql="Select * from Companies 
      INNER JOIN Companies_Address on companies.companyID = companies_address.companyID where companies.active=0 and site_ID=#rc.currentsite# order by Name", name="InActiveCompanies", cachedwithin='#createTimespan(0,0,0,-1)#').execute().getResult();
   
   } else {
      rc.InActiveCompanies = new Query(sql="Select * from Companies
      INNER JOIN Companies_Address on companies.companyID = companies_address.companyID
      INNER JOIN Companies_verticals on companies.companyID = companies_verticals.companyID  where companies.active=0 and site_ID=#rc.currentsite# order by Name
      ", name="InActiveCompanies", cachedwithin='#createTimespan(0,0,0,-1)#').execute().getResult();
   
   }
   rc.InActiveBuinessesCount = variables.CompanyService.getInActiveCount();
  }
  
  void function exclusive( required struct rc ){
 
  }

}
