component accessors = true extends = "abstract" {

	// ------------------------ PUBLIC METHODS ------------------------ //

	void function default( required struct rc ) {
		rc.Courses = variables.CoursesService.getCourses(siteid=rc.currentSite.siteID);
	}
	
	void function categories( required struct rc ) {
		rc.Categories = EntityLoad( "Courses_Category", {}, "" );
	}
	
	void function questions( required struct rc ) {
	    rc.CourseCore = variables.CoursesService.getCourse(course=rc.course);
		rc.Questions = variables.CoursesService.getCourseQuestions(course=rc.course);
	}
	
	void function maintainQ(required struct rc) {
		param name='rc.qID' default = 0;
		param name='rc.context' default='create';
		
		if(rc.qID NEQ '0'){
			rc.context = 'Update';
			rc.QuestionSQL = 'Select * from courses_questions where question_id=:qid';
			rc.Question = new Query();
			rc.question.addParam(name="qid", value="#rc.qid#");
			rc.QuestionInfo = rc.Question.execute(sql=#rc.QuestionSQL#).getResult();
			
			rc.AnswersSQL = 'Select * from courses_answers where question_id=:qid';
			rc.Answer= new Query();
			rc.Answer.addParam(name="qid", value="#rc.qid#");
			rc.Answers = rc.Answer.execute(sql=#rc.AnswersSQL#).getResult();
			
		} else {
	
		}
	}
	
	void function saveQ(required struct rc){
		//We Update the Question:
		questionSQL = "update courses_questions set question=:question where question_id=:qid";
		UpdateQuestion = new Query();
		updateQuestion.addParam(name='question', value='#rc.Question#');
		updateQuestion.addParam(name='qid', value='#rc.question_id#');
		QuestionUpdated = updateQuestion.execute(sql=questionSQL).getResult();
		//We Update the Answers:
		ClearAnswers = new Query( sql = "Delete from courses_answers where question_id=#rc.question_id#" ).execute( ).getREsult( );
				
		//writeDump(var="#rc#", abort="true");
		for(i=1;i<=ListLen(#rc.answer_text#);i++) {
			answer = ListGetAt( #rc.answer_text#, i );
			correct = ListGetAt(#rc.answer_Correct#, i);
			sql = "Insert into courses_answers (answer_text, answer_correct, question_id) VALUES ('#answer#', #correct#, #rc.question_id#);
			SELECT SCOPE_IDENTITY() as NewAnswerID;";
			NewAnswer = new Query( sql = #sql# ).execute( ).getResult( );
			local.AnswerID = NewAnswer.NewAnswerID;
			
			
			
		}
				
		variables.fw.redirect("courses");
	
	}
	
	void function maintain(required struct rc) {
		param name = "rc.postId" default = 0;
		param name = "rc.context" default = "create";
		
		if (!StructKeyExists(rc, "Post")) {
			rc.Post = variables.CoursesService.getPost(postId = Val(rc.postId));
		}
		
		if (rc.Post.isPersisted() ) {
			rc.context = "update";
		}
		rc.Validator = variables.CoursesService.getValidator(Entity = rc.Post);
		
		if (!StructKeyExists(rc, "result")) {
			rc.result = rc.Validator.newResult();
		}
		
		rc.pageTitle = rc.Post.isPersisted() ? "Edit Post" : "Add Post";
	}
	
	void function maintainCats(required struct rc) {
		param name = "rc.catId" default = 0;
		param name = "rc.context" default = "create";
		
		
		if(rc.catID){
			rc.Category = entityLoadByPK('Category', #rc.catID#);
			rc.context = "update";
			rc.pageTitle = 'Edit Category';
		} else {
			rc.Category = entityNew('Category');
			rc.context = "create";
			rc.PageTitle = 'Add Category';
		}
		
		
		
		
		
		
	}
	
	void function deleteCat(required struct rc) {
				
		if(rc.catID) {
		rc.Category = entityLoadByPK( 'Category', #rc.catID# );
		transaction 
		{
			// Delete the Category
			
			EntityDelete(rc.Category); 
		}
			
		}
		
		variables.fw.redirect("blog.categories");
		
		
		
	}
	
	void function delete(required struct rc) {
		rc.post = entityLoadByPK( 'Post', #rc.postID# );
		transaction 
		{
			// Delete the Category
			
			EntityDelete(rc.Post); 
		}
			
				
		variables.fw.redirect("blog");
		
	}
	
	void function Comments(required struct rc) {
		rc.Comments = entityload('Comment');
		
	}
	
	void function maintainComments(required struct rc) {
	    rc.PageTitle = 'Edit Comment';
		rc.Comment = entityloadbyPK('Comment', #rc.id#);
		
	}
	
	void function saveComment(required struct rc) {
		param name = "rc.commentId" default = #rc.commentid#;
		param name = "rc.body" default="";
		
		rc.Comment = entityloadbyPK('Comment', #rc.commentId#);
		
		rc.Comment.setvalue('#rc.body#');
		
		transaction 
		{
			// Delete the Category
			
			EntitySave(rc.comment); 
		}
		variables.fw.redirect("blog.comments");
	}
	
	void function deleteComment(required struct rc) {
		rc.comment = entityLoadByPK( 'Comment', #rc.id# );
		transaction 
		{
			// Delete the Category
			
			EntityDelete(rc.comment); 
		}
			
				
		variables.fw.redirect("blog.comments");
	}
	
	void function save(required struct rc) {
		param name = "rc.postId" default = 0;
		param name = "rc.title" default = "";
		param name = "rc.slug" default = ReReplace(LCase(rc.title), "[^a-z0-9]{1,}", "-", "all");
		param name = "rc.published" default = "";
		param name = "rc.author" default = "";
		param name = "rc.body" default = "";
		param name = "rc.site_ID" default="#rc.Currentsite.siteID#";
		param name = "rc.idUser" default = "";
		param name = "rc.submit" default = "Save & exit";
		
		rc.result = variables.CoursesService.savePost(properties = rc, websiteTitle = rc.config.sitename);
		rc.Post = rc.result.getTheObject();
		
		
		
		if (rc.result.getIsSuccess()) {
			   //AutoSiteMap(rc=rc);
				
			if (rc.submit == "Save & Continue") {
				//genSiteMap();
				variables.fw.redirect("blog.maintain", "Post,result", "postId");
			} else {
			    //genSiteMap();
				variables.fw.redirect("blog", "result");
			}
		} else {
			variables.fw.redirect("blog.maintain", "Post},result", "postId");
		}
	}

}	