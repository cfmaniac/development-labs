component accessors = true extends = "abstract" {
    property name = "UserService" setter = true getter = false;
	// ------------------------ PUBLIC METHODS ------------------------ //

	void function default( required struct rc ) {
		rc.Posts = variables.BlogService.getPosts(siteid=rc.currentSite.siteID);
	}
	
	void function categories( required struct rc ) {
		rc.BlogCategories =EntityLoad('Category', {site_id='#rc.currentsite.siteID#', parentID='0'} , {});
	}
	
	void function maintain(required struct rc) {
		param name = "rc.postId" default = 0;
		param name = "rc.context" default = "create";
		rc.AdminUsers = variables.UserService.getAdmins(siteID=#rc.currentsite.siteID#);
		if (!StructKeyExists(rc, "Post")) {
			rc.Post = variables.BlogService.getPost(postId = Val(rc.postId));
		}
		
		if (rc.Post.isPersisted() ) {
			rc.context = "update";
		}
		rc.Validator = variables.BlogService.getValidator(Entity = rc.Post);
		
		if (!StructKeyExists(rc, "result")) {
			rc.result = rc.Validator.newResult();
		}
		
		rc.pageTitle = rc.Post.isPersisted() ? "Edit Post" : "Add Post";
	}
	
	void function maintainCats(required struct rc) {
		param name = "rc.catId" default = 0;
		param name = "rc.context" default = "create";


		
		if(rc.catID){
			rc.Category = entityLoadByPK('Category', #rc.catID#);

			rc.context = "update";
			rc.pageTitle = 'Edit Category';
		} else {
			rc.Category = entityNew('Category');
			rc.context = "create";
			rc.PageTitle = 'Add Category';
		}
		
		
		
		
		
		
	}
	
	void function deleteCat(required struct rc) {
				
		if(rc.catID) {
		rc.Category = entityLoadByPK( 'Category', #rc.catID# );
		transaction 
		{
			// Delete the Category
			
			EntityDelete(rc.Category); 
		}
			
		}
		
		variables.fw.redirect("blog.categories");
		
		
		
	}
	
	void function delete(required struct rc) {
		rc.post = entityLoadByPK( 'Post', #rc.postID# );
		transaction 
		{
			// Delete the Category
			
			EntityDelete(rc.Post); 
		}
			
				
		variables.fw.redirect("blog");
		
	}
	
	void function Comments(required struct rc) {
		rc.Comments = entityload('Comment');
		
	}
	
	void function maintainComments(required struct rc) {
	    rc.PageTitle = 'Edit Comment';
		rc.Comment = entityloadbyPK('Comment', #rc.id#);
		
	}
	
	void function saveComment(required struct rc) {
		param name = "rc.commentId" default = #rc.commentid#;
		param name = "rc.body" default="";
		
		rc.Comment = entityloadbyPK('Comment', #rc.commentId#);
		
		rc.Comment.setvalue('#rc.body#');
		
		transaction 
		{
			// Delete the Category
			
			EntitySave(rc.comment); 
		}
		variables.fw.redirect("blog.comments");
	}
	
	void function deleteComment(required struct rc) {
		rc.comment = entityLoadByPK( 'Comment', #rc.id# );
		transaction 
		{
			// Delete the Category
			
			EntityDelete(rc.comment); 
		}
			
				
		variables.fw.redirect("blog.comments");
	}
	
	void function save(required struct rc) {
		param name = "rc.postId" default = 0;
		param name = "rc.title" default = "";
		param name = "rc.slug" default = ReReplace(LCase(rc.title), "[^a-z0-9]{1,}", "-", "all");
		param name = "rc.published" default = "";
		param name = "rc.author" default = "";
		param name = "rc.body" default = "";
		param name = "rc.site_ID" default="#rc.Currentsite.siteID#";
		param name = "rc.idUser" default = "";
		param name = "rc.submit" default = "Save & exit";
		
		rc.result = variables.BlogService.savePost(properties = rc, websiteTitle = rc.config.sitename);
		rc.Post = rc.result.getTheObject();
		
		
		
		if (rc.result.getIsSuccess()) {
			   //AutoSiteMap(rc=rc);
				
			if (rc.submit == "Save & Continue") {
				//genSiteMap();
				variables.fw.redirect("blog.maintain", "Post,result", "postId");
			} else {
			    //genSiteMap();
				variables.fw.redirect("blog", "result");
			}
		} else {
			variables.fw.redirect("blog.maintain", "Post},result", "postId");
		}
	}

}	