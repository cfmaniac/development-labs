component accessors = true {

	// ------------------------ DEPENDENCY INJECTION ------------------------ //
    
    property name = "BlogService" setter = true getter = false;
	property name = "ContentService" setter = true getter = false;
	property name = "CoursesService" setter = true getter = false;
	property name = "EnquiryService" setter = true getter = false;
	property name = "NewsService" setter = true getter = false;
	property name = "StoreService" setter = true getter = false;
	property name = "SecurityService" setter = true getter = false;
	property name = "SiteService" setter = true getter = false;
    property name = "Config" setter=true getter=false;
	// ------------------------ PUBLIC METHODS ------------------------ //

	void function init(required any fw) {
		variables.fw = arguments.fw;
	}
	
	// Public Function: getPlugin()
	public function getPlugin(required string plugin, any options={}) {
		local.execInit = true;
		local.com = createObject('plugins.' & arguments.plugin);
		if ( isBoolean(arguments[arrayLen(arguments)]) )
			local.execInit = arguments[arrayLen(arguments)];
	
		return (structKeyExists(local.com, 'init') && local.execInit)? local.com.init(argumentCollection=arguments.options) : local.com;
	}

	void function before(required struct rc) {
		rc.Pagination = createObject("component", "lib.tags.Pagination").init();    
		rc.Regions = getPlugin(plugin='RegionHelper');
		
		rc.isAllowed = variables.SecurityService.isAllowed(action = variables.fw.getFullyQualifiedAction());
		
		if (!rc.isAllowed) {
			variables.fw.redirect("security");
		} else {
			
			//Switch for Sites:
			if(structKeyExists(rc, 'SiteID')){
				session.siteID = #rc.siteID#;
			} else if(!structKeyExists(session, 'siteID')){
				session.siteID = #rc.config.siteID#;
			}
			rc.CurrentSite = variables.SecurityService.setCurrentSite(siteID=#session.siteID#);
			
			
		    
			
			
			
		       
        rc.Sites = variables.SiteService.getSites();
        
        
        rc.SitesSettings = variables.siteservice.getSettingsbySite(siteID=#rc.currentSite.siteID#);
		
		ModulesSQL = 'Select * from sites_modules smc
					  INNER JOIN modules mod on smc.moduleid = mod.id
					  WHERE smc.siteid = :siteID order by name';
        ModulesQuery = new Query();
        ModulesQuery.addParam(name='siteID', value='#rc.CurrentSite.siteID#');
        rc.SiteModules = ModulesQuery.execute(sql=ModulesSQL).getResult();
		
		
			rc.CurrentUser = variables.SecurityService.getCurrentUser();
			rc.unreadEnquiryCount = variables.EnquiryService.getUnreadCount();
			rc.enquiries = variables.EnquiryService.getEnquiries();
			
			//Quick Stats Display:
				StatsSQL = "DECLARE @endAt   [DATETIME] = DATEADD(s, -1, DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())));
			DECLARE @startAt [DATETIME] = DATEADD(dd, -30, DATEDIFF(dd, 0, @endAt));
			DECLARE @counter [INT] = 0;
			DECLARE @tmpTable TABLE([date][date], visitors[int], pageViews[int]);
			DECLARE @tmpStartAt [DATETIME];
			
			WHILE @counter < DATEDIFF(dd, @startAt, @endAt)
			  BEGIN
				SET @counter = @counter+1
				SET @tmpStartAt = DATEADD(dd, @counter, @startAt)
				
				INSERT INTO @tmpTable ([date], [visitors], [pageViews])
				SELECT @tmpStartAt, 
					   (SELECT COUNT(DISTINCT ip) FROM analyticViews WHERE host='#rc.CurrentSite.siteDomain#' AND DATEADD(dd, 0, DATEDIFF(dd, 0, startAt)) = @tmpStartAt),
					   (SELECT COUNT(id) FROM analyticViews WHERE host='#rc.CurrentSite.siteDomain#' AND DATEADD(dd, 0, DATEDIFF(dd, 0, startAt)) = @tmpStartAt)
			  END
			  
			SELECT * FROM @tmpTable";
				rc.rtnQuery = New Query(sql=StatsSQL).execute().getResult();
				
				
				rc.pageViews = listToArray(valueList(rc.rtnQuery.pageViews));
				rc.Visitors = listToArray(valueList(rc.rtnQuery.visitors)); 
				rc.StatsDate = listToArray(valueList(rc.rtnQuery.date));
			
			
		}
	}
	
	void function AutoSiteMap(required struct rc) {
		
			include "automap.cfm";

	}

}
