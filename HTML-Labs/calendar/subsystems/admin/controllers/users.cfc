component accessors = true extends = "abstract" {

	// ------------------------ DEPENDENCY INJECTION ------------------------ //

	property name = "UserService" setter = true getter = false;

	// ------------------------ PUBLIC METHODS ------------------------ //

	void function default(required struct rc) {
		rc.users = variables.UserService.getUsers(siteID=rc.CurrentSite);
		rc.UsersDirectory = expandPath('./globals/users/profiles/');
	}
	
	void function admins(required struct rc) {
		rc.adminusers = variables.UserService.getAdminUsers(siteID=rc.CurrentSite);
		rc.UsersDirectory = expandPath('./globals/users/profiles/');
	}
	
	void function groups(required struct rc) {
		rc.UserGroups = entityload('UserGroups', {"siteid": #rc.currentsite#});
		
	}

	void function delete(required struct rc) {
		param name = "rc.userId" default = 0;
		rc.result = variables.UserService.deleteUser(userId = Val(rc.userId));
		
		//We Delete the User's Folders:
		rc.UsersDirectory = expandPath('./globals/users/profiles/');
		
		if(directoryExists('#rc.UsersDirectory##rc.userId#')) {
			DirectoryDelete('#rc.UsersDirectory##rc.userId#',true);
		}
		
		variables.fw.redirect("users", "result");
	}

	void function maintain(required struct rc) {
	    rc.UserGroups = variables.UserService.getUserGroups();
		param name = "rc.userId" default = 0;
		param name = "rc.admin" default=0;
		param name = "rc.context" default = "create";
		if (!StructKeyExists(rc, "User")) {
			rc.User = variables.UserService.getUser(userId = Val(rc.userId));
		}
		if (rc.User.isPersisted()) {
			rc.context = "update";
		}
		rc.Validator = variables.UserService.getValidator(Entity = rc.User);
		if (!StructKeyExists(rc, "result")) {
			rc.result = rc.Validator.newResult();
		}
		rc.pageTitle = rc.User.isPersisted() ? "Edit User" : "Add User";
	}

	void function save(required struct rc) {
		param name = "rc.userId" default = 0;
		param name = "rc.name" default = "";
		param name = "rc.email" default = "";
		param name = "rc.password" default = "";
		param name = "rc.admin" default=0;
		param name = "rc.group" default=0;
		param name = "rc.context" default = "create";
		param name = "rc.site_id" default = "#rc.CurrentSite.siteid#";
		param name = "rc.submit" default = "Save & exit";
		
		//writeDump(var="#rc#", abort='true');
		
		rc.result = variables.UserService.saveUser(properties = rc, context = rc.context);
		rc.User = rc.result.getTheObject();
		
		//We Create the User's Folders based on The User's ID:
		rc.UsersDirectory = expandPath('./globals/users/profiles/');
		
		if(!directoryExists('#rc.UsersDirectory##rc.User.getUserID()#')) {
			
			directoryCreate('#rc.UsersDirectory##rc.User.getUserID()#');
			directoryCreate('#rc.UsersDirectory##rc.User.getUserID()#/avatars/');
			DirectoryCreate('#rc.UsersDirectory##rc.User.getUserID()#/company/');
			directoryCreate('#rc.UsersDirectory##rc.User.getUserID()#/docs/');
			
		}
		
		
		if (rc.result.getIsSuccess()) {
			if (rc.submit == "Save & Continue") {
				
				/*Added for Admin User Site Provision*/
				if(rc.admin NEQ "0"){
					//Site Administrator:
					if(rc.context == "update") {
						ClearSites = new Query( sql = "Delete from users_sites_admin where userID=#rc.userid#" ).execute( ).getREsult( );
						if( rc.adminsiteid NEQ "0" ) {
							for(i=1;i<=ListLen(#rc.adminsiteid#);i++) {
							value = ListGetAt( #rc.adminsiteid#, i );
							sql = "Insert into users_sites_admin (UserID, siteID) VALUES (#rc.userId#, #value#)";
							NewSiteAdmin = new Query( sql = #sql# ).execute( ).getResult( );
							}
						}else {
							//Can Admin All Sites:
							sql = "Insert into users_sites_admin (UserID, siteID) VALUES (#rc.userId#, 0)";
							NewSiteAdmin = new Query( sql = #sql# ).execute( ).getResult( );
						}

					}
				}
				//END
				
				variables.fw.redirect("users.maintain", "result,User", "userId");
				
			} else {
				variables.fw.redirect("users", "result");
			}
		} else {
			variables.fw.redirect("users.maintain", "result,User", "userId");
		}
	}

}
