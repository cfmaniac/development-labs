component accessors = true extends = "abstract" {
	property name = "EnquiryService" setter = true getter = false;
	// ------------------------ PUBLIC METHODS ------------------------ //

	void function default(required struct rc) {
		rc.enquiries = variables.EnquiryService.getEnquiries();
		rc.unreadEnquiryCount = variables.EnquiryService.getUnreadCount();
	}

	void function delete(required struct rc) {
		param name = "rc.messageID" default = 0;
		rc.result = variables.EnquiryService.deleteEnquiry(enquiryId = Val(rc.messageID));
		variables.fw.redirect("inbox", "result");
	}

	void function enquiry(required struct rc) {
		param name = "rc.messageID" default = 0;
		rc.Enquiry = variables.EnquiryService.getEnquiry(enquiryId = Val(rc.messageID));
		if (!IsNull(rc.Enquiry)) {
			variables.EnquiryService.markRead(enquiryId = rc.Enquiry.getEnquiryId());
		} else {
			variables.fw.redirect("main.notfound");
		}
	}

	void function markRead(required struct rc) {
		rc.result = variables.EnquiryService.markRead();
		variables.fw.redirect("inbox", "result");
	}

}
