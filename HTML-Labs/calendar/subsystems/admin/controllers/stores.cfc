component accessors="true" extends="abstract"{
  
  void function default( required struct rc ){
   
   
     
  
  }
  
  void function products( required struct rc ){
   rc.Products = variables.StoreService.getProducts(siteID=rc.CurrentSite.siteID);
  }
 
  void function categories( required struct rc ){
   rc.Categories = variables.StoreService.getProductCategories(siteID=rc.CurrentSite.siteID);	
  }
  
  
   void function orders( required struct rc ){
   rc.NewOrders = variables.StoreService.getNewOrders(siteID=rc.CurrentSite.siteID);
   
   rc.AllOrders = variables.StoreService.getOrders(siteID=rc.CurrentSite.siteID);	
  }
  
     void function orderstatus( required struct rc ){
   rc.Statuses = variables.StoreService.getOrderStatuses(siteID=rc.CurrentSite.siteID);
   
  
  }
  
  void function promos( required struct rc ){
   rc.Promos = variables.StoreService.getPromotions(siteID=rc.CurrentSite.siteID);	
  }
  
  void function settings( required struct rc ){
   rc.CoreSettings = new Query(sql="Select * from settings_modules where module_id='13' and site_id='#rc.CurrentSite.siteID#' and setting_category='core'").execute().getResult();
   rc.PaymentSettings = new Query(sql="Select * from settings_modules where module_id='13' and site_id='#rc.CurrentSite.siteID#' and setting_category='payments'").execute().getResult();
   rc.ShippingSettings = new Query(sql="Select * from settings_modules where module_id='13' and site_id='#rc.CurrentSite.siteID#' and setting_category='shipping'").execute().getResult();
   rc.ProductSettings = new Query(sql="Select * from settings_modules where module_id='13' and site_id='#rc.CurrentSite.siteID#' and setting_category='products'").execute().getResult();
   
   rc.TaxSettings = variables.StoreService.getSalesTax(siteID=rc.CurrentSite.siteID);
    
  }
  
  

}
