component accessors = true extends = "abstract"{
  
  property name = "SiteService" setter = true getter = false;
  systemdatetime=now();
  session.StartDate = #DateFormat(DateAdd('d', -30, SystemDateTime),'MM/DD/YYYY')#;
  session.EndDate = #dateFormat(SystemDateTime, 'MM/DD/YYYY')#;
  session.GA_AccessToken = '';
  
  
  void function default(required struct rc) {
        if(len(rc.config.gaProfileID)) {
            rc.daysInRange = DateDiff("d",session.startdate,session.enddate) + 1;
            
            rc.oauthSettings = {scope = "https://www.googleapis.com/auth/analytics.readonly",
     								client_id = "564424970055-nf6a9maclj4cfab6rdnibsprbm2jt6gb.apps.googleusercontent.com",
     						 		client_secret = "5tLWAZ0i5LklQidA_Fjhdhr0",
     						 		redirect_uri = "http://replicatorx.localhost.com/admin:stats",
     						 		state = "optional"};
     						 		
     		rc.loginURL = "https://accounts.google.com/o/oauth2/auth?scope=" & rc.oauthSettings["scope"] 
                   & "&redirect_uri=" & rc.oauthSettings["redirect_uri"]
                   & "&response_type=code&client_id=" & rc.oauthSettings["client_id"]
                   & "&access_type=online";				 		
     						 		
     		if(isDefined("rc.code") AND rc.code NEQ "access_denied") {
		        //ga = createObject( 'component', 'subsystems.admin.controllers.ga' ).googleOauth2login( code = rc.code );
				//WriteDump(var='#rc.code#', abort=true);
				rc.ga = fw.getPlugin('ga').googleOauth2login( code = rc.code );
				//writeDump(var='#rc.ga#', abort='true');
				
				if(!isDefined("session.ga_accessToken")){
		        location(url=rc.loginURL, addtoken=false);
		        
	        } else if( isDefined("session.ga_accessToken") AND session.ga_accessToken DOES NOT CONTAIN "Authorization Failed"){
	          
	          rc.GaProfiles = rc.ga.parseProfiles();
			      
	        }
	        }
	        
	        
	        
	       
	        
        }
		
			for (site=1;site LTE ArrayLen(rc.SitesSettings);site=site+1) {
			//WriteOutput(array[i]);
			rc.SiteDomain = rc.sitesSettings[site].GetDomain();
			}
			
				StatsSQL = "DECLARE @endAt   [DATETIME] = DATEADD(s, -1, DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())));
			DECLARE @startAt [DATETIME] = DATEADD(dd, -30, DATEDIFF(dd, 0, @endAt));
			DECLARE @counter [INT] = 0;
			DECLARE @tmpTable TABLE([date][date], visitors[int], pageViews[int]);
			DECLARE @tmpStartAt [DATETIME];
			
			WHILE @counter < DATEDIFF(dd, @startAt, @endAt)
			  BEGIN
				SET @counter = @counter+1
				SET @tmpStartAt = DATEADD(dd, @counter, @startAt)
				
				INSERT INTO @tmpTable ([date], [visitors], [pageViews])
				SELECT @tmpStartAt, 
					   (SELECT COUNT(DISTINCT ip) FROM analyticViews WHERE host='#rc.siteDomain#' AND DATEADD(dd, 0, DATEDIFF(dd, 0, startAt)) = @tmpStartAt),
					   (SELECT COUNT(id) FROM analyticViews WHERE host='#rc.siteDomain#' AND DATEADD(dd, 0, DATEDIFF(dd, 0, startAt)) = @tmpStartAt)
			  END
			  
			SELECT * FROM @tmpTable";
				rc.rtnQuery = New Query(sql=StatsSQL).execute().getResult();
				
				/*rc.rtnStruct = {
				pageViews	= listToArray(valueList(rc.rtnQuery.pageViews)),
				visitors 	= listToArray(valueList(rc.rtnQuery.visitors))
				};*/
				rc.pageViews = listToArray(valueList(rc.rtnQuery.pageViews));
				rc.Visitors = listToArray(valueList(rc.rtnQuery.visitors)); 
				rc.StatsDate = listToArray(valueList(rc.rtnQuery.date));
			
					
            GetBrowsersSQL = "SELECT TOP 10 AgentName, AgentType, host, Count(id) [count]
                              FROM AnalyticViews
                              Where EXISTS 
                              ( Select Distinct[AgentName] FROM analyticViews ) 
                              and host='#rc.siteDomain#' and agentType='browser'
                              Group By AgentName, AgentType, Host";
			 
            GetCrawlersSQL = "SELECT TOP 10 AgentName, AgentType, host, Count(id) [count]
                              FROM AnalyticViews
                              Where EXISTS 
                              ( Select Distinct[AgentName] FROM analyticViews ) 
                              and host='#rc.siteDomain#' and agentType='Crawler'
                              Group By AgentName, AgentType, Host";
            
            GetTrafficsSQL = "DECLARE @endAt   [DATETIME] = DATEADD(s, -1, DATEADD(dd, 1, DATEDIFF(dd, 0, GETDATE())));
			DECLARE @startAt [DATETIME] = DATEADD(dd, -30, DATEDIFF(dd, 0, @endAt));
			
			SELECT TOP 5 host, source, path, COUNT(id) [count], AVG(CAST(DATEDIFF(ms, startAt, endAt) AS BIGINT)) avgTimeMS,
				   ROW_NUMBER() OVER(ORDER BY COUNT(id) DESC) id
			  FROM analyticViews
			 WHERE startAt BETWEEN @startAt AND @endAt AND host='#rc.siteDomain#' and AgentType='browser'
			 GROUP BY path, source, host
			 ORDER BY [count] DESC";
			 
			 /*Top Browsers*/
			 rc.BrowserStats = new Query(sql=GetBrowsersSQL).execute().getResult();
			 //rc.Browsers = listToArray(valueList(rc.BrowserStats.AgentName));
			 //rc.BrowserViews = listToArray(valueList(rc.BrowserStats.count)); 
			
			/*Top Crawlers*/
			 rc.CrawlerStats = new Query(sql=GetCrawlersSQL).execute().getResult();
			 //rc.Crawlers = listToArray(valueList(rc.CrawlerStats.AgentName));
			 //rc.CrawlerViews = listToArray(valueList(rc.CrawlerStats.count)); 
			 
			 
			 /*Top Page Views*/
			 rc.TrafficStats = new Query(sql=GetTrafficsSQL).execute().getResult();
			 rc.Paths = listToArray(valueList(rc.TrafficStats.path));
			 rc.PathViews = listToArray(valueList(rc.TrafficStats.Count));
			
		
			 
			 
  }
}
