component accessors = true extends = "abstract" {
	
	void function default( required struct rc ) {
		rc.Photos = EntityLoad("gallery", {"active": 1, "siteID": #rc.CurrentSite#}, "Display ASC");
	}
	
	void function maintain(required struct rc) {
	    param name = "rc.photoId" default = 0;
		param name = "rc.context" default = "create";
		
		if(structKeyExists(rc, "photoID") and rc.photoID GT "0") {
			rc.Photo = entityLoadByPK( "gallery", rc.photoid );
			rc.context = "update";
			rc.PageTitle = "Edit Photo";
			
			/*for (site in rc.SiteSettings) {
			rc.siteName = "#rereplace(rc.site.getname(), "&trade;", "","ALL")#";
	        rc.siteName = "#rereplace(rc.siteName, " ", "-","ALL")#;
			}*/
			
		} else {
			rc.Photo = ENTITYNEW("gallery");
			rc.context = "create";
			rc.PageTitle = "Add Photo";
			
			/*for (site in rc.SiteSettings) {
			rc.siteName = "#rereplace(rc.site.getname(), "&trade;", "","ALL")#";
	        rc.siteName = "#rereplace(rc.siteName, " ", "-","ALL")#;
			}*/
		}   
	}
	
	void function save(required struct rc){
		/*GalleryIMG = FileUpload('#expandPath('./globals/gallery/')#', photofile, 'unique');
		WriteDump(var='#galleryIMG#');*/
		
	}
	
	void function delete(required struct rc){
		 rc.Photo = entityLoadByPK( 'gallery', rc.photoid );
		 
		 transaction{
			 entityDelete(rc.Photo);
			 ORMFLUSH();
		 }
		 
		 variables.fw.redirect("gallery");
	}
}