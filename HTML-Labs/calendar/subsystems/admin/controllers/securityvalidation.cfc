component accessors = true extends = "abstract" {

	// ------------------------ DEPENDENCY INJECTION ------------------------ //

	property name = "SecurityValidationService" setter = true getter = false;

	// ------------------------ PUBLIC METHODS ------------------------ //

	void function default(required struct rc) {
		rc.SecurityQuestions = variables.SecurityValidationService.getQuestions();		
		
	}
	
	void function maintain(required struct rc) {
		param name='rc.qID' default = 0;
		param name='rc.context' default='create';
		
		if(rc.qID NEQ '0'){
			rc.context = 'Update';
			rc.QuestionSQL = 'Select * from user_validation_questions where validation_id=:qid';
			rc.Question = new Query();
			rc.question.addParam(name="qid", value="#rc.qid#");
			rc.QuestionInfo = rc.Question.execute(sql=#rc.QuestionSQL#).getResult();
			
			
			
		} else {
			rc.QuestionInfo.validation_question = '';
			rc.QuestionInfo.validation_answers = '';
			rc.QuestionInfo.validation_active = '1';
		}
	}
	
	void function save(required struct rc){
		if(rc.context is "update"){
			
			rc.QuestionInfo = EntityLoadByPK( "UserQValidations",#rc.qid# );
		    //We Save the Current Submitted Config:
		    rc.QuestionInfo.setValidation_Question('#rc.question#');
		    rc.QuestionInfo.setValidation_Answers('#rc.validation_answers#');
		    rc.QuestionInfo.setValidation_Active('#rc.active#');
		    		    
		    
			
			message = "Validation Question Successfully Updated";
		} else {
			rc.QuestionInfo = EntityNew( "UserQValidations");
		    //We Save the Current Submitted Config:
		    rc.QuestionInfo.setValidation_Question('#rc.question#');
		    rc.QuestionInfo.setValidation_Answers('#rc.validation_answers#');
		    rc.QuestionInfo.setValidation_Active('#rc.active#');
			
			message = "Validation Question Successfully Added";
		}
		
		transaction 
		{
			entitySave(rc.QuestionInfo);
		    		    
		    ORMFLUSH();
		}
		
		/*writeDump(var='#form#', abort=true, label='Question and Answers');*/
		 location(url='#variables.fw.buildURL(action='securityvalidation', querystring='message=#message#')#');
	}
	
	void function delete(required struct rc){
		rc.QuestionInfo = EntityLoad( "UserQValidations",#rc.qid#, true );
		transaction 
		{
			EntityDelete(rc.QuestionInfo);
		    		    
		    ORMFLUSH();
		}
		
		
		message = "Validation Question Successfully Removed";
		location(url='#variables.fw.buildURL(action='securityvalidation', querystring='message=#message#')#');
	}

}
