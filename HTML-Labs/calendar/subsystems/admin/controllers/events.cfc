component accessors = true extends = "abstract" {
	
	
	void function default(required struct rc) {
		rc.events = entityload('Events',{"site_ID": '#rc.CurrentSite#'});
	}
	
	void function maintain(required struct rc) {
		param name = "rc.Id" default = 0;
		param name = "rc.context" default = "create";
		param name = "rc.pageTitle" default = "Add Events";
		
		if(structKeyExists(rc, "id") && rc.id GT "0"){
			rc.context = "update";
			rc.event = entityloadbypk('Events', rc.id);
			rc.pageTitle = "Edit Event";
		} else {
			rc.context = "create";
			rc.event = entityNew('Events');
			rc.pageTitle = "Add Event";
			
		}
		
		
	}
	
	void function save(required struct rc) {

		if( rc.context is "create" ) {
			rc.Event = entityNew( 'Events' );
			rc.Event.setTitle( '#rc.title#' );
			rc.Event.setBody( '#rc.body#' );
			rc.Event.setAllDayEvent('#rc.allDayEvent#');
			rc.Event.setStartAt( '#rc.startAt#' );
			rc.Event.setStartTime('#rc.startTime#');
			rc.Event.setEndAt( '#rc.endsAt#' );
			rc.Event.setEndTime('#rc.EndTime#');
			rc.Event.setSite_ID('#rc.CurrentSite#');
		}
		else {
			rc.Event = entityLoadByPK( 'Events', rc.id );
			rc.Event.setTitle( '#rc.title#' );
			rc.Event.setBody( '#rc.body#' );
			rc.Event.setAllDayEvent('#rc.allDayEvent#');
			rc.Event.setStartAt( '#rc.startAt#' );
			rc.Event.setStartTime('#rc.startTime#');
			rc.Event.setEndAt( '#rc.endsAt#' );
			rc.Event.setEndTime('#rc.EndTime#');
			rc.Event.setSite_ID('#rc.CurrentSite#');
		}

		transaction{
			entitySave( rc.Event );
			ORMFLUSH( );
		}

		variables.fw.redirect(action='events.default', queryString='message=evSV');

	}
		
	
	void function delete(required struct rc){
		 rc.Event = entityLoadByPK( 'Events', rc.id );
		 
		 transaction{
			 entityDelete(rc.Event);
			 ORMFLUSH();
		 }
		 
		 variables.fw.redirect("events");
	}
}

