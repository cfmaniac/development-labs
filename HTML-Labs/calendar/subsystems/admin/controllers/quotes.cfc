component accessors = true extends = "abstract"{
    property name = "QuoteService" setter = true getter = false;
	void function default( required struct rc ) {
		rc.Quotes = variables.QuoteService.getQuotes(siteid=rc.CurrentSite.siteID);
	}
	
	void function maintain(required struct rc) {
		param name = "rc.Id" default = 0;
		param name = "rc.context" default = "create";
		param name = "rc.pageTitle" default = "Add Quote";
		
		if(structKeyExists(rc, "id") && rc.id GT "0"){
			rc.context = "update";
			rc.quote = entityloadbypk('Quotes', rc.id);
			rc.pageTitle = "Edit Quote";
		} else {
			rc.context = "create";
			rc.quote = entityNew('Quotes');
			rc.pageTitle = "Add Quote";
			
		}
	}

	void function save(required struct rc) {

		if( rc.context is "create" ) {
			rc.Quote = entityNew( 'Quotes' );
			rc.Quote.setQuote( '#rc.body#' );
			rc.Quote.setSiteID('#rc.CurrentSite.siteID#');
		} else {
			rc.quote = entityloadbypk('Quotes', rc.id);
			rc.Quote.setQuote( '#rc.body#' );
			rc.Quote.setSiteID('#rc.CurrentSite.siteID#');
		}
		
		transaction{
			entitySave( rc.Quote );
			ORMFLUSH( );
		}

		variables.fw.redirect(action='quotes.default', queryString='message=quSV');
	}	
	
	void function delete(required struct rc){
		 rc.Quote = entityLoadByPK( 'Quotes', rc.id );
		 
		 transaction{
			 entityDelete(rc.Quote);
			 ORMFLUSH();
		 }
		 
		 variables.fw.redirect("quotes");
	}
}