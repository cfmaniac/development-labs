component accessors = true extends = "abstract" {
	property name = "SiteService" setter = true getter = false;
	property name = "ContentService" setter = true getter = false;

	function default( rc ) {
		//writeDump(var=hash("thingToHash", 'MD5', 'UTF-16LE'), abort=true);
		//writeDump(var=fw.buildUrl(action='.tst?hell=world'), abort=true);
		//Site Settings:
		rc.regions = getPlugin('regionHelper');
		rc.SiteSettings = variables.SiteService.getSettingsbySite(siteID=#session.SiteID#);
		
		//rc.RootPages = entityload('Page',{"site_id":rc.CurrentSite});
		rc.RootPages = variables.ContentService.getNavigation(siteID=#session.siteID#,clearCache = true);
		rc.TemplatesDir = expandPath('./subsystems/public/layouts');
		rc.SiteTemplates = DirectoryList(rc.TemplatesDir, false, 'query');
		
		
		//Module Settings:
		ModulesSQL = 'Select * from sites_modules smc
					          INNER JOIN modules mods on smc.moduleid = mods.id
					          WHERE smc.siteid = :siteID and smc.moduleActive=1 order by name';
                ModulesQuery = new Query();
                ModulesQuery.addParam(name='siteID', value='#session.siteID#');
                rc.ModulesSettings = ModulesQuery.execute(sql=ModulesSQL).getResult();
        
        IAModulesSQL = 'Select * from sites_modules smc
					          INNER JOIN modules mods on smc.moduleid = mods.id
					          WHERE smc.siteid = :siteID and smc.moduleActive=0 order by name';
                IAModulesQuery = new Query();
                IAModulesQuery.addParam(name='siteID', value='#session.siteID#');
                rc.InActiveModulesSettings = IAModulesQuery.execute(sql=IAModulesSQL).getResult();       
                
	}	
	
	function addsite( rc ) {
		rc.regions = getPlugin('regionHelper');
		rc.TemplatesDir = expandPath('./subsystems/public/layouts');
		rc.SiteTemplates = DirectoryList(rc.TemplatesDir, false, 'query');
	}
	
	
	function disableEnableModule(rc){
		EnableDisableModSQL = "update sites_modules set moduleActive=:com where moduleid=:modID and siteID=:siteID";
		ModulesQuery = new Query();
		        ModulesQuery.addParam(name='com', value='#rc.com#');
			    ModulesQuery.addParam(name='modID', value='#rc.moduleID#');
                ModulesQuery.addParam(name='siteID', value='#rc.config.SiteID#');
                rc.ModulesENDisAble = ModulesQuery.execute(sql=EnableDisableModSQL).getResult();
                if(rc.com is "1"){
	                message = 'Module Successfully Enabled'; 
                } else {
	                message = 'Module Successfully Disabled';
                }
        //variables.fw.redirect("settings", #message#);
        location(url='#variables.fw.buildURL(action='settings', querystring='message=#message#')#');        
		
		
	}
	
	function save(rc){
		//writeDump(var='#form#', abort='true');
			param name ="rc.siteId" default='#rc.CurrentSite.siteID#';
		    param name ="rc.title" default="";
		    //param name ="rc.description" default="";
		    param name="rc.isSubsite" default="0";
		    param name="rc.parentsiteID" default="0";
		    param name ="rc.forceSSL" default="0";
		    param name ="rc.domain" default="";
		    param name ="rc.active" default="";
		    param name ="rc.template_name" default="";
		    param name ="rc.homepage" default="home";
		    param name ="rc.googleanalytics" default="";
		    param name ="rc.defaultemail" default="";

		    //Enhanced Settings:
		    param name ="rc.themecolor" default="";
		    param name ="rc.copyright" default="";
		    param name ="rc.address" default="";
		    param name ="rc.address2" default="";
		    param name ="rc.city" default="";
		    param name ="rc.state" default="";
		    param name ="rc.zipcode" default="";
		    param name ="rc.FACEBOOKACCESSTOKEN" default="";
		    param name ="rc.UseFACEBOOKLogin" default="";
		    param name ="rc.FACEBOOKAPPID" default="";
		    param name ="rc.FACEBOOKAPPSECRET" default="";
		    param name ="rc.FACEBOOKURL" default="";
		    param name ="rc.GOOGLEPLUSURL" default="";
		    param name ="rc.LinkedinURL" default="";
		    param name ="rc.linkedinApiKey" default="";
		    param name ="rc.linkedinSecretKey" default="";
		    param name ="rc.TWITTERACCESSTOKEN" default="";
		    param name ="rc.TWITTERACCESSTOKENSECRET" default="";
		    param name ="rc.TWITTERCONSUMERKEY" default="";
		    param name ="rc.TWITTERCONSUMERSECRET" default="";
		    param name ="rc.TWITTERSCREENNAME" default="";
		    param name ="rc.TWITTERURL" default="";
		    param name ="rc.Internal" default="1";
		    param name ="rc.useGeolocation" default="0";
		    param name ="rc.defaultLayout" default="default";
		    param name ="rc.themecolor" default="";
		    param name ="rc.defaultRadius" default="10";
		    param name ="rc.YOUTUBEURL" default="";

		    param name ="rc.submit" default="Save Settings";
		    rc.result = {};

		    rc.Site = EntityLoadByPK( "Sites",#rc.CurrentSite.siteID# );
		    //We Save the Current Submitted Config:
		    rc.Site.setTitle('#rc.Title#');
		    rc.Site.setDomain('#rc.domain#');
		    rc.Site.setisSubsite('#rc.isSubsite#');
		    rc.Site.setparentsiteID('#rc.parentsiteID#');
		    rc.Site.setisActive('#rc.active#');
		    rc.Site.setForceSSL('#rc.ForceSSL#');
		    rc.site.setEnableSiteSearch('#rc.enableSiteSearch#');
		    rc.Site.setCopyright('#rc.copyright#');
		    rc.site.setEmail('#rc.email#');
		    
		    rc.Site.setUseInternalAnalytics('#rc.Internal#');
		    rc.Site.setgaTrackerID('#rc.GoogleAnalytics#');
		    rc.Site.setgaProfileID('#rc.gaProfileID#');
		    
		    rc.Site.setUseFACEBOOKLogin('#rc.facebookLogin#');
			rc.Site.setFACEBOOKAPPID('#rc.facebookappid#');
			rc.Site.setFACEBOOKAPPSECRET('#rc.facebookappsecret#');
			rc.Site.setFACEBOOKURL('#rc.facebookurl#');
			rc.Site.setGOOGLEPLUSURL('#rc.googleplusurl#');
			rc.Site.setLinkedInURL('#rc.linkedinURL#');
			rc.Site.setlinkedinApiKey('#rc.linkedinApiKey#');
			rc.Site.setlinkedinSecretKey('#rc.linkedinSecretKey#');
			rc.Site.setTWITTERACCESSTOKEN('#rc.twitteraccesstoken#');
			rc.Site.setTWITTERACCESSTOKENSECRET('#rc.twitteraccesstokensecret#');
			rc.Site.setTWITTERCONSUMERKEY('#rc.twitterconsumerkey#');
			rc.Site.setTWITTERCONSUMERSECRET('#rc.twitterconsumersecret#');
			rc.Site.setTWITTERSCREENNAME('#rc.twitterscreenname#');
			rc.Site.setTWITTERURL('#rc.twitterurl#');
			
			rc.Site.setAddress('#rc.address#');
			rc.Site.setAddress2('#rc.address2#');
			rc.Site.setZipcode('#rc.zipcode#');
			
			rc.Site.setdefaultLayout('#rc.defaultLayout#');
			rc.Site.sethomepage('#rc.homepage#');
			rc.Site.setthemecolor('#rc.themecolor#');
			
		    
		    rc.Site.setUpdatedAt(#dateFormat(now(), "MM/DD/YYYY")#);
		    
		    transaction 
		{
			entitySave(rc.Site);
		    //entitySave(rc.SiteSettings);
		    
		    ORMFLUSH();
		}
		message = '#urlEncodedFormat('Site Settings Successfully Updated')#';
		variables.fw.redirect("settings?message=#message#");
		    
		    
	}
	
	function saveNewSite(rc){
		//writeDump(var='#form#', abort='true');
			param name ="rc.siteId" default='0';
		    param name ="rc.title" default="";
		    param name ="rc.isSubsite" default="0";
		    param name ="rc.parentsiteID" default="0";
		    param name ="rc.forceSSL" default="0";
		    param name ="rc.domain" default="";
		    param name ="rc.active" default="";
		    param name ="rc.template_name" default="";
		    param name ="rc.homepage" default="home";
		    param name ="rc.googleanalytics" default="";
		    param name ="rc.defaultemail" default="";

		    //Enhanced Settings:
		    param name ="rc.themecolor" default="";
		    param name ="rc.copyright" default="";
		    param name ="rc.address" default="";
		    param name ="rc.address2" default="";
		    param name ="rc.city" default="";
		    param name ="rc.state" default="";
		    param name ="rc.zipcode" default="";
		    param name ="rc.FACEBOOKACCESSTOKEN" default="";
		    param name ="rc.UseFACEBOOKLogin" default="";
		    param name ="rc.FACEBOOKAPPID" default="";
		    param name ="rc.FACEBOOKAPPSECRET" default="";
		    param name ="rc.FACEBOOKURL" default="";
		    param name ="rc.GOOGLEPLUSURL" default="";
		    param name ="rc.LinkedinURL" default="";
		    param name ="rc.linkedinApiKey" default="";
		    param name ="rc.linkedinSecretKey" default="";
		    param name ="rc.TWITTERACCESSTOKEN" default="";
		    param name ="rc.TWITTERACCESSTOKENSECRET" default="";
		    param name ="rc.TWITTERCONSUMERKEY" default="";
		    param name ="rc.TWITTERCONSUMERSECRET" default="";
		    param name ="rc.TWITTERSCREENNAME" default="";
		    param name ="rc.TWITTERURL" default="";
		    param name ="rc.Internal" default="1";
		    param name ="rc.useGeolocation" default="0";
		    param name ="rc.defaultRadius" default="10";
		    param name ="rc.defaultLayout" default="default";
		    param name ="rc.themecolor" default="";
		    param name ="rc.YOUTUBEURL" default="";

		    param name ="rc.submit" default="Save Settings";
		    rc.result = {};

		    rc.Site = entityNew('Sites');
		    //We Save the Current Submitted Config:
		    rc.Site.setTitle('#rc.Title#');
		    rc.Site.setDomain('#rc.domain#');
		    rc.Site.setisActive('#rc.active#');
		    rc.Site.setisSubsite('#rc.isSubsite#');
		    rc.Site.setparentsiteID('#rc.parentsiteID#');
		    rc.Site.setForceSSL('#rc.ForceSSL#');
		    rc.site.setEnableSiteSearch('#rc.enableSiteSearch#');
		    rc.Site.setCopyright('#rc.copyright#');
		    rc.site.setEmail('#rc.email#');
		    
		    rc.Site.setUseInternalAnalytics('#rc.Internal#');
		    rc.Site.setgaTrackerID('#rc.GoogleAnalytics#');
		    rc.Site.setgaProfileID('#rc.gaProfileID#');
		    
		    rc.Site.setcopyright('#rc.copyright#');
			rc.Site.setaddress('#rc.address#');
			rc.Site.setaddress2('#rc.address2#');
			rc.Site.setcity('#rc.city#');
			rc.Site.setstate('#rc.state#');
			rc.Site.setzipcode('#rc.zipcode#');
				    
		    
		    rc.Site.setUseFACEBOOKLogin('#rc.facebookLogin#');
			rc.Site.setFACEBOOKAPPID('#rc.facebookappid#');
			rc.Site.setFACEBOOKAPPSECRET('#rc.facebookappsecret#');
			rc.Site.setFACEBOOKURL('#rc.facebookurl#');
			rc.Site.setGOOGLEPLUSURL('#rc.googleplusurl#');
			rc.Site.setLinkedInURL('#rc.linkedinURL#');
			rc.Site.setlinkedinApiKey('#rc.linkedinApiKey#');
			rc.Site.setlinkedinSecretKey('#rc.linkedinSecretKey#');
			rc.Site.setTWITTERACCESSTOKEN('#rc.twitteraccesstoken#');
			rc.Site.setTWITTERACCESSTOKENSECRET('#rc.twitteraccesstokensecret#');
			rc.Site.setTWITTERCONSUMERKEY('#rc.twitterconsumerkey#');
			rc.Site.setTWITTERCONSUMERSECRET('#rc.twitterconsumersecret#');
			rc.Site.setTWITTERSCREENNAME('#rc.twitterscreenname#');
			rc.Site.setTWITTERURL('#rc.twitterurl#');
			
			rc.Site.setdefaultLayout('#rc.defaultLayout#');
			rc.Site.setthemecolor('#rc.themecolor#');
			
		    
		    rc.Site.setCreatedAt(#dateFormat(now(), "MM/DD/YYYY")#);
		    rc.Site.setUpdatedAt(#dateFormat(now(), "MM/DD/YYYY")#);
		    
		    transaction 
		{
			entitySave(rc.Site);
		    rc.NewSiteID = rc.Site.getID();
		    
		    ORMFLUSH();
		}
		
			rc.copysettingsSite = #rc.CurrentSite.siteID#;
		    rc.siteDirectory = ReReplace(LCase(rc.title), "[^a-z0-9]{1,}", "_", "all");
		    
		    //Create Site Directory:
		    rc.SitesDirectory = expandPath('./globals/sites/');
		    if(!directoryExists(expandPath('#rc.SitesDirectory##rc.SiteDirectory#'))){
			    directoryCreate('#rc.SitesDirectory##rc.SiteDirectory#');
				directoryCreate('#rc.SitesDirectory##rc.SiteDirectory#/pages/');
				DirectoryCreate('#rc.SitesDirectory##rc.SiteDirectory#/blog/');
				directoryCreate('#rc.SitesDirectory##rc.SiteDirectory#/store/');
				directoryCreate('#rc.SitesDirectory##rc.SiteDirectory#/emaillists/');
		    }
		    
		    //Create a New Home Page for the Site:
		    rc.HomePage = EntityNew("Page");
			rc.Homepage.setancestorId(0);
			rc.Homepage.setSlug('home');
			rc.homepage.setleftValue('0');
			rc.HomePage.setRightValue('1');
			rc.HomePage.settitle('#rc.Title# Home Page');
			rc.HomePage.setcontent('#rc.Title#');
		    rc.HomePage.setmetaGenerated('1');
			rc.Homepage.setmetaTitle('#rc.Title# Home Page');
			rc.Homepage.setmetaDescription('#rc.Title#');
			rc.Homepage.setmetaKeywords('#rc.Title#');
			rc.Homepage.setsiteID('#rc.newSiteID#');
			
			transaction 
			{
				entitySave(rc.HomePage);
				rc.homePageID = rc.HomePage.getpageID();
			    ORMFLUSH();
			}
			//Copy the pages that are linked as modules:
			//We Replicate the Pages that Contain Modules (i.e., have routes)
			
		    rc.PagesSQL = "INSERT INTO pages (page_slug, page_left, page_right, page_ancestorid, page_depth, page_title, page_content, page_hasmodule, site_id)
			                Select page_slug, page_left, page_right, '#rc.homePageID#', page_depth, page_title, page_content, page_hasmodule, '#rc.newSiteID#'
			                from Pages pgs where site_id='#rc.copysettingsSite#' and page_hasmodule='1'";
			rc.NewSitePgs = new Query();
	        rc.NewsitePages = rc.NewSitePgs.execute(sql=rc.PagesSQL).getResult();
			
			
			
			//Copy Modules from Site:
			rc.ModulesSet1SQL = "INSERT INTO sites_modules (siteID, moduleid, moduleactive)
								SELECT '#rc.newSiteID#', moduleid, moduleactive
								FROM sites_modules mods where siteid='#rc.copysettingssite#'";
			rc.NewSiteModsSettings = new Query();
			rc.NewModsSet = rc.NewSiteModsSettings.execute(sql=rc.ModulesSet1SQL).getResult();
			
			//Copy Modules Settings From Site:
			rc.ModulesSetSQL = "INSERT INTO modules_settings ( module_id, module_setting_help, module_setting_key, module_setting_value, module_setting_type, module_setting_options, site_id)
								SELECT module_id, module_setting_help, module_setting_key, module_setting_value, module_setting_type, module_setting_options, '#rc.NewSiteID#'
								FROM modules_settings mods where site_id='#rc.copysettingssite#'";
			rc.NewSiteModsSettings = new Query();
			rc.NewModsSet = rc.NewSiteModsSettings.execute(sql=rc.ModulesSetSQL).getResult();
		    
		message = '#urlEncodedFormat('New Site Successfully Saved')#';
		variables.fw.redirect("settings?message=#message#");
		    
		    
	}
	
	
}
