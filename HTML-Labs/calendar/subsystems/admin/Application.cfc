component {

	// ------------------------ APPLICATION SETTINGS ------------------------ //

	this.applicationRoot = ReReplace(getDirectoryFromPath(getCurrentTemplatePath()), "admin.$", "", "all");
	this.name = ReReplace(this.applicationRoot & "_admin", "[\W]", "", "all");

	// ------------------------ CALLED WHEN PAGE rc.STARTS ------------------------ //

	function onrc.tart() {
		location(url = "../index.cfm/admin:main", addToken = "false");
	}
}
