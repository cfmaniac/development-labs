<cfoutput>
<!DOCTYPE html>
<html class="bootstrap-layout">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>#rc.config.siteName#  - Replicator Control Panel</title>
  <base href="#rc.basehref#admin/">
  <!-- Prevent the demo from appearing in search engines (REMOVE THIS) -->
  <meta name="robots" content="noindex">

  <!-- Material Design Icons  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
 
  <!-- Roboto Web Font -->
  <link href="https://fonts.googleapis.com/css?family=Lato:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en" rel="stylesheet">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
  <!-- App CSS -->
  <link type="text/css" href="layouts/assets/css/style.min.css" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="layouts/assets/css/bootstrap-datepicker.css"> <!-- Datepicker -->
  <link type="text/css" rel="stylesheet" href="layouts/assets/css/bootstrap-timepicker.css"> <!-- Timepicker -->
  <link type="text/css" rel="stylesheet" href="layouts/assets/css/bootstrap-colorpicker.css"> <!-- Colorpicker -->
  <link type="text/css" href="layouts/assets/css/custom.css" rel="stylesheet">
  <link type="text/css" href="layouts/assets/css/datatables.min.css" rel="stylesheet">
  <link type="text/css" href="layouts/assets/vendor/fileinput/fileinput.css" rel="stylesheet">
  <link rel="icon" type="image/png" href="layouts/assets/images/replicator-icon.png" />
  <script src="layouts/assets/vendor/jquery.min.js"></script>
  

</head>

<body <cfif rc.action contains "maintain">
    class="layout-container top-navbar breakpoint-1200 si-l3-md-up"
<cfelse>
    class="layout-container top-navbar si-l3-md-up si-r3-lg-up"
</cfif>
>


  <!-- Navbar -->
  <nav class="navbar navbar-light bg-white navbar-full navbar-fixed-top">

    <!-- Sidebar left toggle -->
    <button class="navbar-toggler pull-xs-left hidden-md-up" type="button" data-toggle="sidebar" data-target="##sidebarLeft"><span class="material-icons">menu</span></button>

    <!-- Sidebar right toggle -->
    <button class="navbar-toggler pull-xs-right hidden-md-up" type="button" data-toggle="sidebar" data-target="##sidebarRight"><span class="material-icons">menu</span></button>

    <!-- Brand -->
    <a class="navbar-brand first-child-md" href="#buildURL('main')#">#rc.config.siteName# </a>

    <cfinclude template="includes/header.cfm">

    <cfinclude template="includes/sidebar-left.cfm">
    <cfif rc.action DOES NOT CONTAIN "maintain">
    <cfinclude template="includes/sidebar-right.cfm">
    </cfif>

  

  <!-- Content -->
  <div class="layout-content" data-scrollable>
    <div class="container-fluid">
      #body#
    </div>
    <cfdump var=#rc.action#>
  </div>  

      

    
  <cfinclude template="includes/footer.cfm">
  

</body>


</html>
</cfoutput>