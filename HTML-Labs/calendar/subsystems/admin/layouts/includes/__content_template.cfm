<cfoutput>
<h1 class="page-heading">Overview</h1>
      <div class="row">
        <div class="col-md-8">
          <div class="card">
            <div class="card-block">
              <div id="line" style="height:220px"></div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card-primary m-b-1">
            <div class="card-block center">
              <h4 class="card-title">January</h4>
              <p class="card-subtitle">Total number of sales</p>
              <div class="material-icons display-2 m-a-1">today</div>
              <p class="h1 m-b-0">
                <strong>$132,562</strong>
              </p>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="card">
            <div class="card-block">
              <div class="media">
                <div class="media-body">
                  <strong class="text-muted">Today</strong>
                  <p class="h2 m-b-0">
                    <strong>$3,129</strong>
                  </p>
                </div>
                <div class="media-right media-middle">
                  <i class="material-icons md-48 text-muted-light">attach_money</i>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card">
            <div class="card-block">
              <div class="media">
                <div class="media-body">
                  <strong class="text-muted">This Week</strong>
                  <p class="h2 m-b-0">
                    <strong>$8,191</strong>
                  </p>
                </div>
                <div class="media-right media-middle">
                  <i class="material-icons md-48 text-muted-light">attach_money</i>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card">
            <div class="card-block">
              <div class="media">
                <div class="media-body">
                  <strong class="text-muted">This Month</strong>
                  <p class="h2 m-b-0">
                    <strong>$12,491</strong>
                  </p>
                </div>
                <div class="media-right media-middle">
                  <i class="material-icons md-48 text-muted-light">attach_money</i>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <h4 class="page-heading">Latest Stats</h4>
          <ul class="list-group list-group-sm list-group-transparent-list m-b-05">
            <li class="list-group-item">
              <div class="media">
                <div class="media-left">
                  <div class="text-muted" style="width:50px">
                    06 Jan
                  </div>
                </div>
                <div class="media-body">
                  <a href="##">Andrew Brain</a>
                </div>
                <div class="media-right">

                  <strong>$19,281</strong>

                </div>
              </div>
            </li>

            <li class="list-group-item">
              <div class="media media-middle">
                <div class="media-left">
                  <div class="text-muted" style="width:50px">
                    06 Jan
                  </div>
                </div>
                <div class="media-body">

                  <a href="##">Andrew Brain</a>

                </div>
                <div class="media-right">

                  <strong>$19,281</strong>

                </div>
              </div>
            </li>

            <li class="list-group-item">
              <div class="media">
                <div class="media-left">
                  <div class="text-muted" style="width:50px">
                    06 Jan
                  </div>
                </div>
                <div class="media-body">
                  <a href="##">Andrew Brain</a>
                </div>
                <div class="media-right">

                  <strong>$19,281</strong>

                </div>
              </div>
            </li>
            <li class="list-group-item">
              <div class="media">
                <div class="media-left">
                  <div class="text-muted" style="width:50px">
                    06 Jan
                  </div>
                </div>
                <div class="media-body">
                  <a href="##">Andrew Brain</a>
                </div>
                <div class="media-right">

                  <strong>$19,281</strong>

                </div>
              </div>
            </li>

            <li class="list-group-item">
              <div class="media">
                <div class="media-left">
                  <div class="text-muted" style="width:50px">
                    06 Jan
                  </div>
                </div>
                <div class="media-body">
                  <a href="##">Andrew Brain</a>
                </div>
                <div class="media-right">

                  <strong>$19,281</strong>

                </div>
              </div>
            </li>

          </ul>
          <a href="##" class="btn btn-primary btn-rounded btn-sm m-b-1">View All</a>

          <h4 class="page-heading"><i class="material-icons md-36">description</i> <span class="icon-text">Overdue Invoices</span></h4>
          <div class="card">
            <table class="table table-fit">
              <tbody>
                <tr>
                  <td class="text-muted" width="100">12/01/2016</td>
                  <td><a href="##">Order No. ##12791</a></td>
                  <td width="100">
                    <strong>$1,281</strong>
                  </td>
                  <td class="text-center" width="50"><a href="##" class="text-muted"><i class="fa fa-download"></i></a></td>
                </tr>
                <tr>
                  <td class="text-muted" width="100">12/01/2016</td>
                  <td><a href="##">Order No. ##12791</a></td>
                  <td width="100">
                    <strong>$11,581</strong>
                  </td>
                  <td class="text-center" width="50"><a href="##" class="text-muted"><i class="fa fa-download"></i></a></td>
                </tr>
                <tr>
                  <td class="text-muted" width="100">12/01/2016</td>
                  <td><a href="##">Order No. ##12791</a></td>
                  <td width="100">
                    <strong>$10,121</strong>
                  </td>
                  <td class="text-center" width="50"><a href="##" class="text-muted"><i class="fa fa-download"></i></a></td>
                </tr>
              </tbody>
            </table>
          </div>

        </div>
        <div class="col-md-6">
          <h4 class="page-heading"><i class="material-icons md-36">account_circle</i> <span class="icon-text">Top 3</span></h4>
          <div class="card">
            <div class="media">
              <div class="media-left">
                <img src="assets/images/people/110/guy-8.jpg" width="85" alt="guy">
              </div>
              <div class="media-body media-middle">
                <a href="##" class="card-title">Andrew Brain</a>
                <div class="text-muted-light">
                  54 Sales
                </div>
              </div>
              <div class="media-right media-middle">
                <div class="card-block h4 m-b-0">
                  $19,281
                </div>
              </div>
            </div>
          </div>

          <div class="card">
            <div class="media">
              <div class="media-left">
                <img src="assets/images/people/110/guy-5.jpg" width="85" alt="guy">
              </div>
              <div class="media-body media-middle" style="width:100%">
                <a href="##" class="card-title">Adrian Demian</a>
                <div class="text-muted-light">
                  24 Sales
                </div>
              </div>
              <div class="media-right media-middle">
                <div class="card-block h4 m-b-0">
                  $12,475
                </div>
              </div>
            </div>
          </div>

          <div class="card">
            <div class="media">
              <div class="media-left">
                <img src="assets/images/people/110/guy-6.jpg" width="85" alt="guy">
              </div>
              <div class="media-body media-middle" style="width:100%">
                <a href="##" class="card-title">Johnny Tall</a>
                <div class="text-muted-light">
                  10 Sales
                </div>
              </div>
              <div class="media-right media-middle">
                <div class="card-block h4 m-b-0">
                  $5,311
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
</cfoutput>      