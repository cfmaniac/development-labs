<cfoutput>
	<!-- Sidebar -->
  <div class="sidebar sidebar-left si-si-3 sidebar-visible-md-up sidebar-dark ls-top-navbar-md-up" id="sidebarLeft" data-scrollable>
    <div class="sidebar-user">
      <div class="media">
        <div class="media-left media-middle p-r-0">
          
        </div>
        <div class="media-body media-middle">
        <cfif StructKeyExists(rc, "CurrentUser")>
          <a href="#buildURL(action='users.maintain', querystring='userid=#rc.CurrentUser.getuserID()#')#" class="sidebar-user-name">

              <IMG SRC="/globals/users/#rc.CurrentUser.getUserID()#/avatars/#rc.CurrentUser.getUserAvatar()#">

				#rc.CurrentUser.getName()#
          </a>
          </cfif>
          <ul class="list-unstyled list-inline m-a-0">
            <li><a href="#buildURL(action='users.maintain', querystring='userid=#rc.CurrentUser.getuserID()#')#" class="text-muted"><i class="material-icons">person_outline</i></a></li>
            <li><a href="#buildURL(action = 'inbox')#" class="text-muted"><i class="material-icons">mail</i></a></li>
            <li><a href="##" class="text-muted"><i class="material-icons">settings</i></a></li>
            <li><a href="#buildURL('security/logout')#" class="text-muted"><i class="material-icons">lock</i></a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="sidebar-heading">Main Menu</div>
    <!-- Menu -->
    <ul class="sidebar-menu sm-bordered sm-active-button-bg">
      <li class="sidebar-menu-item">
        <a class="sidebar-menu-button" href="#buildURL('main')#">
          <i class="sidebar-menu-icon material-icons">home</i> Dashboard
        </a>
      </li>

      <cfloop query="#rc.SiteModules#">
        <cfif #rc.SiteModules.hasAdmin# && #rc.SiteModules.moduleActive#>
        <li class="sidebar-menu-item <cfif getSection() EQ rc.sitemodules.name> open</cfif>">
        <a class="sidebar-menu-button" <cfif rc.SiteModules.hasAdminMenu>href="##"<cfelse>href="#buildURL('#lcase(rc.SiteModules.name)#')#"</cfif>>
          <i class="fa #rc.SiteModules.icon#"></i> #rc.SiteModules.MenuLinkTitle#
        </a>
        <cfif #rc.SiteModules.hasAdminMenu#>
           #view("#lcase(rc.SiteModules.name)#/nav/nav")#
        </cfif>
        </li>
        </cfif>
      </cfloop>

      <li class="sidebar-menu-item <cfif getSection() EQ 'settings' || getSection() EQ 'stats' || getSection() EQ 'main'> open</cfif>">
        <a class="sidebar-menu-button" href="##">
          <i class="sidebar-menu-icon material-icons">tune</i> Settings
        </a>
        #view("settings/nav/nav")#
        
      </li>
      
    </ul>
    <!-- // END Menu -->

  </div>
  <!-- // END Sidebar -->
</cfoutput>