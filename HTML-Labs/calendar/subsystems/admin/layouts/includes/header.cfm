
<cfoutput>
	<!-- Menu -->
    <div class="collapse navbar-toggleable-xs" id="navbarMenu">
      <cfinclude template="topbarnav.cfm">
    </div>
    <!-- // END Menu -->
    <!-- Menu -->
    <ul class="nav navbar-nav pull-xs-right hidden-sm-down">
      <!-- Notifications dropdown -->
      <li class="nav-item dropdown">
        <a class="btn btn-default dropdown-toggle btn-circle" data-caret="false" data-toggle="dropdown" role="button" aria-haspopup="false"><i class="material-icons">notifications</i></a>
        <ul class="dropdown-menu  dropdown-menu-right notifications">
          <!---li class="dropdown-item">
            <a href="##" class="nav-link">
              <span class="media">
					<span class="media-left">
						<img src="assets/images/people/110/guy-8.jpg" alt="guy" width="35" class="img-circle">
					</span>
              <span class="media-body media-middle">
							<strong><span class="text-primary">Andrew</span> added a new photo</strong>
              <small>2 min ago</small>
              </span>
              </span>
            </a>
          </li--->
          
          
        </ul>
      </li>
      <!-- // END Notifications dropdown -->

      <!-- Notifications dropdown -->
      <li class="nav-item dropdown">
        <a class="btn btn-primary dropdown-toggle btn-circle" data-caret="false" data-toggle="dropdown" role="button" aria-haspopup="false"><i class="material-icons email">mail </i></a><cfif rc.unreadEnquiryCount> <span class="label label-info pull-right">#NumberFormat(rc.unreadEnquiryCount)#</span></cfif>
        <ul class="dropdown-menu dropdown-menu-right notifications" aria-labelledby="Preview">
          <li class="dropdown-title">Emails <cfif rc.unreadEnquiryCount> <span class="label label-info pull-right">#NumberFormat(rc.unreadEnquiryCount)#</span></cfif></li>
         
          <cfloop array="#rc.enquiries#" index="local.Enquiry">
           <li class="dropdown-item">
            <a class="nav-link" href="#buildURL(action = 'inbox.enquiry', queryString = 'messageId=#local.Enquiry.getEnquiryId()#')#">
              <span class="media">
					<span class="media-left media-middle"><i class="material-icons">mail</i> </span>
              <span class="media-body media-middle">
						<small class="pull-xs-right text-muted">#DateFormat(local.Enquiry.getCreated(), "MM/DD/YYYY")# <br>at #TimeFormat(local.Enquiry.getCreated())#</small>
						<cfif !local.Enquiry.isRead()>
						<strong>#local.Enquiry.getName()#</strong>
						<cfelse>
						#local.Enquiry.getName()#
						</cfif>
						<!---Subject Goes Here--->
					</span>
              </span>
            </a>
          </li>
					
				</cfloop>
          <li class="dropdown-action center">
            <a href="#buildURL(action = 'inbox')#">Go to inbox</a>
          </li>
        </ul>
      </li>
      <!-- // END Notifications dropdown -->
      <li class="nav-item">
        <a class="btn btn-info btn-circle" href="http://#rc.CurrentSite.siteDomain#" target="_blank" title="View Website #rc.CurrentSite.SiteName#"><i style="margin-top: 5px;" class="fa fa-desktop fa-2x"></i></a>
      </li>
      <li class="nav-item">
	      <form name="SystemSites" id="SystemSites" style="padding-top: 13px;">
	          <select name="siteID" class="form-control" onchange="window.location = '#buildURL("admin:main")#?siteID='+$(this).val();">
	              <cfloop array="#rc.Sites#" index="site">
	              <cfscript>
		              verticalSQL = 'Select * from sites where isSubsite=1 and parentSiteID = #site.getID()#';
                      rc.VerticalSites = new Query();
                      rc.SubSites = rc.VerticalSites.execute(sql=verticalSQL).getResult();
	              </cfscript>
	              <option value="#site.getID()#" <cfif #rc.CurrentSite.siteID# EQ #site.getID()#>selected="selected"</cfif>>#site.getTitle()# <cfif !site.getIsActive()>-(InActive)</cfif></option>
	              <cfif rc.subSites.recordcount GT "0">
		              <cfloop query="#rc.subsites#">
		              <option value="#ID#" <cfif #rc.CurrentSite.siteID# EQ #ID#>selected="selected"</cfif>>|--- #Title# <cfif !IsActive>-(InActive)</cfif></option>
		              
		              </cfloop>
	              </cfif>
	              
	              </cfloop>  
		          
	          </select>
          </form>
      </li>
      <li class="nav-item">
             
          
        
        
        <div class="btn-group navbar-btn">
          
          <a href="#buildURL('security/logout')#" class="btn btn-default btn-rounded">Logout</a>
          <!---a href="#BuildURL('security')#" class="btn btn-default btn-rounded">Login</a--->
          <!---a href="##" class="btn btn-orange btn-rounded">Sign Up</a--->
        </div>
      </li>
    </ul>
    <!-- // END Menu -->

  </nav>
  <!-- // END Navbar -->
</cfoutput>