<cfoutput>
<script>
 	
	$(function () {
	 <cfif rc.action is "admin:main.default" || rc.action is "admin:stats.default">
      var Usage;
      $(document).ready(function() {
        Usage = new Highcharts.Chart({
          chart: {
            renderTo: 'usage',
            type: 'line'
          },
          title: {
            text: 'Vists per Day as of #dateformat( now( ), "Full" )#'
          },
          legend: {
            enabled: false,
          },
          xAxis: {
           categories : [
                    <cfloop array="#rc.StatsDate#" index="day">
                        '#DateFormat( day, "MM/DD" )#',
                    </cfloop>
                ],
           
            dateTimeLabelFormats: {
              day: '%m-%d'   
            },
            tickmarkPlacement: 'off',
            title: {
              enabled: false
            }
          },
          yAxis: {
            title: {
              text: 'Visits / Day'
            },
            labels: {
              formatter: function() {
                return this.value;
              }
            }
          },
          tooltip: {
            formatter: function() {
              return ''+
              this.x +': '+ Highcharts.numberFormat(this.y, 0, ',') +' visits';
            }
          },
          plotOptions: {
            area: {
              stacking: 'normal',
              lineColor: '##666666',
              lineWidth: 1,
              marker: {
                lineWidth: 1,
                lineColor: '##666666'
              }
            }
          },
          series: [{
                name : 'Views',
                data : [<cfloop array="#rc.PageViews#" index="views">
                #numberFormat( views )#,
                </cfloop>]
            }, {
                name : 'Visitors',
                data : [<cfloop array="#rc.Visitors#" index="visitors">
                #numberFormat( visitors )#,
                </cfloop>]
            }]
          });
        });
        
        </cfif>
    
    <cfif rc.action is "admin:stats.default">
    $('##browsers').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Top Browsers'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Visits / Day'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b>'
        },

        series: [{
            name: 'Browers',
            colorByPoint: true,
            data: [
            <cfloop query="#rc.BrowserStats#">
            {
                name: '#AgentName#',
                <cfif #AgentName# is "Chrome">
	        fillcolor: '##FCD207',
	        color: '##FCD207',
		    <cfelseif #agentName# is "Android WebKit Browser">
		        fillcolor: '##4BB847',
		        color: '##4BB847',
		    <cfelseif #agentName# is "FireFox">
		        fillcolor: '##F28A1E',
		        color: '##F28A1E',
		    <cfelseif #agentName# is "Internet Explorer">
		        fillcolor: '##1EBBEE',
		        color: '##1EBBEE',
		    <cfelseif #agentName# is "Safari">
		        fillcolor: '##99B1C1',
		        color: '##99B1C1',
		    <cfelseif #agentName# is "Opera">
		        fillcolor: '##FCD207',
		        color: '##FCD207',          
	        <cfelse>
	            fillColor: '##c9423f',
	            color: '##c9423f',
	        </cfif>
                y: #count#
            }<cfif #rc.BrowserStats.currentrow# LT #rc.BrowserStats.recordcount#>,</cfif>
            </cfloop>
            ]
        }]
        
      });
    
    
    
    <cfif rc.CrawlerStats.recordcount GT 0>
    $('##crawlers').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Top Robots / Crawlers'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Visits / Day'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b>'
        },

        series: [{
            name: 'Browers',
            colorByPoint: true,
            data: [
            <cfloop query="#rc.CrawlerStats#">
            {
                name: '#AgentName#',
          
		        fillcolor: '##99B1C1',
		        color: '##99B1C1',
		    
                y: #count#
            }<cfif #rc.CrawlerStats.currentrow# LT #rc.CrawlerStats.recordcount#>,</cfif>
            </cfloop>
            ]
        }]
        
      });
    </cfif>
    
    <cfif rc.TrafficStats.recordcount GT 0>
    $('##toppages').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Top Pages'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Visits / Day'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b>'
        },

        series: [{
            name: 'Top 15 Pages',
            colorByPoint: true,
            data: [
            <cfloop query="#rc.TrafficStats#">
            {
                name: '#rc.TrafficStats.path#',
          
		        fillcolor: '##99B1C1',
		        color: '##99B1C1',
		    
                y: #count#
            }<cfif #rc.TrafficStats.currentrow# LT #rc.TrafficStats.recordcount#>,</cfif>
            </cfloop>
            ]
        }]
        
      });
    </cfif>
    
    
    
	</cfif>
	});
</script>
</cfoutput>