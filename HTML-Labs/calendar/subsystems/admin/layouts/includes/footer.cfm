<cfset sessionTimeout = rc.config.systemTimeout>

<!-- jQuery -->
  
  <!-- Bootstrap -->
  <script src="layouts/assets/vendor/tether.min.js"></script>
  <script src="layouts/assets/vendor/bootstrap.min.js"></script>
  
  <!-- DATEPICKER -->
  <script src="layouts/assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script> <!-- Bootstrap datepicker -->
  <script src="layouts/assets/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js"></script> <!-- Timepicker -->
  <!-- COLORPICKER -->
  <script src="layouts/assets/vendor/bootstrap-colorpicker/bootstrap-colorpicker.js"></script> <!-- Bootstrap colorpicker -->

  <!-- Timeout Dialog -->
  <!---script src="layouts/assets/vendor/bootstrap-session-timeout.js"></script--->
  <script type="text/javascript">
  //Replicator Control Panel Configs:
  
          /*var sTimeout = <cfoutput>#sessionTimeout#</cfoutput> * 60 * 1000;
          var Warntime = 1 * 60 * 1000;
          var Reditime = 2 * 60 * 1000;
             
          $.sessionTimeout({
                keepAliveUrl: '/subsystems/remote/RemoteProxy.cfc?method=keepalive',
                title: 'Your <cfoutput>#rc.Currentsite.SiteName#</cfoutput> Session is About to Expire!',
                message: 'Your Control Panel session is about to expire.',
                logoutUrl: '<cfoutput>#buildURL('security.logout')#</cfoutput>',
                redirUrl: '<cfoutput>#buildURL('security.logout')#</cfoutput>',
                warnAfter: Warntime,
                redirAfter: Reditime,
                countdownMessage: 'Redirecting in {timer} seconds.'
            });*/
          
            
  
  // delete confirmation
	$("a[title~='Delete']").click(function(){
		return confirm("Delete this item?");
	});
	
	// Toggle meta tag fields
	$("#metagenerated").click(function(){
		if (!$(this).is(":checked")) $(".metatags").slideDown();
		else $(".metatags").slideUp();
	});
	if($("#metagenerated").is(":checked")) $(".metatags").hide();
	
	// Toggle meta tag fields
	$("#metagenerated").click(function(){
		if (!$(this).is(":checked")) $(".metatags").slideDown();
		else $(".metatags").slideUp();
	});
	if($("#metagenerated").is(":checked")) $(".metatags").hide();

	//Previous Next Buttons:
	$('a[data-toggle^="tab"]').click(function(){
    var data = $(this).attr("href");
    $('a[data-toggle^="tab"]').parent("li").removeClass("active");
    $('li a[href^="'+data+'"]').parent("li").addClass("active");
    $('.tab-pane').removeClass("active");
    $(data).addClass("active");
});
	//SiteSettings:
	var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn'),
            allPrevBtn = $('.prevBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    allPrevBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

        $(".form-group").removeClass("has-error");
        prevStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
    
    /**
   * jQuery datepicker plugin wrapper for compatibility
   */
  $.fn.plusDatePicker = function () {
      if (! this.length) return;

      if (typeof $.fn.datepicker != 'undefined') {
        this.datepicker();
      }
  };
  
  $('.datepicker').plusDatePicker();
  
  /**
  * jQuery timepicker plugin wrapper for compatibility
 */
  $.fn.plusTimePicker = function () {
      if (! this.length) return;

      if (typeof $.fn.datepicker != 'undefined') {
          this.timepicker({
              minuteStep: 5,
              showInputs: false,
              disableFocus: true,
              icons: {
                  up: 'fa fa-chevron-up',
                  down: 'fa fa-chevron-down'
              }
          });
      }
  };
  
  $('.timepicker').plusTimePicker({
		minuteStep: 5,
		showInputs: false,
		disableFocus: true,
	});
  
  $('.colorpicker').colorpicker({
    component: '.input-group-addon'
  });
</script>

  <!-- AdminPlus -->
  <script src="layouts/assets/vendor/replicator.js"></script>

  <!-- App JS -->
  <script src="layouts/assets/js/main.min.js"></script>

  <!-- Charts JS -->
  <script src="layouts/assets/vendor/highcharts/highcharts.js"></script>
  <!---Stats JS--->  
  <cfinclude template="stats_js.cfm">
  
  

  

