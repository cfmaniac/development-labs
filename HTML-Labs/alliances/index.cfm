<!---Alliances Database Fix for Replicator Games Module--->
<cfquery name="getUserAlliances" datasource="replicatorx">
    Select * from user_alliances
</cfquery>
<cfdump var="#getUserAlliances#">

<cfloop query="#getUserAlliances#">

    <cfswitch expression="#getUserAlliances.User_Alliance#">
        <cfcase value="n2d-deep">
            <cfquery name="user_game_alliance" datasource="replicatorx">
                Insert into user_game_groups_members (user_ID, gamegroup_id) VALUES (#getUserAlliances.user_id#, 1)
            </cfquery>
        </cfcase>
        <cfcase value="n2d-vt">
             <cfquery name="user_game_alliance" datasource="replicatorx">
                Insert into user_game_groups_members (user_ID, gamegroup_id) VALUES (#getUserAlliances.user_id#, 3)
            </cfquery>
        </cfcase>
        <cfcase value="n2d-mar">
             <cfquery name="user_game_alliance" datasource="replicatorx">
                Insert into user_game_groups_members (user_ID, gamegroup_id) VALUES (#getUserAlliances.user_id#, 4)
            </cfquery>
        </cfcase>
        <cfcase value="n2d-inv">
             <cfquery name="user_game_alliance" datasource="replicatorx">
                Insert into user_game_groups_members (user_ID, gamegroup_id) VALUES (#getUserAlliances.user_id#, 5)
            </cfquery>
        </cfcase>
        <cfcase value="ppx-op">
             <cfquery name="user_game_alliance" datasource="replicatorx">
                Insert into user_game_groups_members (user_ID, gamegroup_id) VALUES (#getUserAlliances.user_id#, 6)
            </cfquery>
        </cfcase>
        <cfcase value="ppx-pigsx">
             <cfquery name="user_game_alliance" datasource="replicatorx">
                Insert into user_game_groups_members (user_ID, gamegroup_id) VALUES (#getUserAlliances.user_id#, 8)
            </cfquery>
        </cfcase>
        <cfcase value="ppx-dark">
             <cfquery name="user_game_alliance" datasource="replicatorx">
                Insert into user_game_groups_members (user_ID, gamegroup_id) VALUES (#getUserAlliances.user_id#, 9)
            </cfquery>
        </cfcase>
        <cfcase value="ppx-proto">
             <cfquery name="user_game_alliance" datasource="replicatorx">
                Insert into user_game_groups_members (user_ID, gamegroup_id) VALUES (#getUserAlliances.user_id#, 10)
            </cfquery>
        </cfcase>
        <cfcase value="ppx-apoc">
             <cfquery name="user_game_alliance" datasource="replicatorx">
                Insert into user_game_groups_members (user_ID, gamegroup_id) VALUES (#getUserAlliances.user_id#, 11)
            </cfquery>
        </cfcase>
        <cfcase value="ppx-n2d">
             <cfquery name="user_game_alliance" datasource="replicatorx">
                Insert into user_game_groups_members (user_ID, gamegroup_id) VALUES (#getUserAlliances.user_id#, 12)
            </cfquery>
        </cfcase>



    </cfswitch>

</cfloop>
Member Alliances have been fixed