java.util.concurrent.Future?
java.util.concurrent.Semaphore:

Future/ThreadPool API and here it is:

ExecutorService executor = Executors.newFixedThreadPool(3);

 

Object callREST() throws Exception {

Future future = executor.submit(() -> sendHttp());

return future.get();
}

So we use a threadpool with fixed number of threads to limit the number of concurrent requests!
We simply submit request as Runnable/Callable to it, pool stores them in it’s internal Queue, waiting for free thread to become available and returns Future object to us. We wait for execution to complete using Future.get() that blocks until result is available or exception is thrown.

￼

This way we can have 100 application threads invoke callREST() method in parallel, but only 3 requests will actually be sent by threadpool threads initially and other 97 will stay in queue until any of the first 3 requests finish. All our 100 application threads will initially be blocked on Future.get() awaiting result and will gradually unblock once their particular requests complete.
