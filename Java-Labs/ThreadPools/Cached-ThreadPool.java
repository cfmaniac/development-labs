pointed out in comments that in our example we use fixed thread pool which always has 3 threads even if they do nothing, while in real world we better use cached thread pool that stops threads automatically after period of inactivity and recreates them once more tasks arrive:

new ThreadPoolExecutor(

0, //min threads 

3, //max threads

60, TimeUnit.SECONDS, //stop idle thread after this period

new SynchronousQueue<Runnable>());

The name cached comes from Executors.newCachedThreadPool() which, however, doesn’t have parameter to limit max number of threads.
