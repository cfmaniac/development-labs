The term “memory leak” is used in C/C++ to describe unreachable allocated memory. In garbage collected languages, making memory unreachable is how you free it, so the C/C++ definition is not useful for java. The term “memory leak” in java is used to describe a somewhat different behavior: memory you allocate but don’t free after you are done with it. This happens by holding references to memory you don’t need. The end result is the same: memory usage grows over time and you have to restart your application every so often.

See
 Java MemLeak.class
Java NoMemLeak.class