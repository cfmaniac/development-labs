public void NoLeak()

{

	while(true) {

		// I am allocating millions of objects and never dispose them.

		// This would be a memory leak in C++.

		// java will dispose of every single one of them.

		// NO MEMORY LEAK HERE

		MyClass obj = new MyClass();

	}

}

