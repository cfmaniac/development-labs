# Email Labs #

Various Email development items

outlook-emails
=========================================================================================

Outlook.com strips all content in ANY conditional comment placed in an email template. Depending on how your logic is constructed, this can be a problem, as it will potentially remove content that you intend to be visible.

There are various articles that suggest clever modification of conditional comments resolve the problem.

THIS IS NO LONGER TRUE!

Conditional comments will be stripped regardless.

client-emails
=========================================================================================

Emails I've developed for Clients and Companies I've worked for.
	* Pall.com Outlook 03-16 Responsive Email

repsonsive-emails
=========================================================================================

Responsive Email Templates and Tests